part of './whl_app_pages.dart';

abstract class Routes {
  static const splash = '/splash';
  static const main = '/main';
  static const login = '/login';

  static const square = '/square';

  static const settingUserInfo = '/settingUserInfo';
  static const roomPage = '/roomPage';
  static const openFile = '/openFile';
  static const publish = '/publish';
  static const productDetail = '/productDetail';
  static const otherUserInfo = '/otherUserInfo';
  static const coinShop = '/coinShop';
  static const webView = '/webView';
  static const loginForPhone = '/LoginForPhonePage';
  static const surePhoneCode = '/SurePhoneCodePage';
  static const completeUserInfo = '/CompleteUserInfoPage';
  static const selectedSexPage = '/SelectedSexPage';
  static const sexOrientation = '/SexOrientationPage';
  static const loginForPassword = '/LoginForPasswordPage';
  static const forgotPassword = '/ForgotPasswordPage';
  static const improveInformation = '/ImproveInformationPage';
  static const screen = '/ScreenPage';
  static const search = '/SearchPage';
  static const heartbeatRadio = '/HeartbeatRadioPage';
  static const share = '/sharePage';
  static const qrCode = '/QrCodePage';
  static const accountSafe = '/AccountSafePage';
  static const userInfo = '/UserInfoPage';
  static const visitors = '/VisitorsPage';
  static const chatSetting = '/ChatSettingPage';
  static const sendRedGroup = '/SendRedGroupPage';
  static const sendRedPerson = '/SendRedPersonPage';
  static const complaintPage = '/ComplaintPage';
  static const topicList = '/TopicListPage';
  static const newFriend = '/NewFriendPage';
  static const newFriendSetting = '/NewFriendSettingPage';
  static const newFriendAuth = '/NewFriendAuthPage';
  static const notice = '/NoticePage';

//cave
  static const cavePage = '/cave_page';
  static const caveHomePage = '/cave_home_page';
  static const caveSearchPage = '/cave_search_page';
  static const caveDetailPage = '/cave_detail_page';
  static const caveJoinPage = '/cave_join_page';
  static const caveRedEnvelopePage = '/red_envelopes_page';
  static const createCavePage = '/create_cave_page';
  static const caveSettingPage = '/caved_setting_page';
  static const caveSetNoticePage = '/cave_set_notice_page';
  static const addCavePost = '/add_cave_post';
  static const caveGroupChat = '/cave_group_chat';
  static const caveFindPage = '/cave_find_page';
  static const caveMutualPage = '/cave_mutual_page';
  static const cavePublic = '/cave_page';

/*chat*/
  static const chatCallPage = '/chat_call_page';
  static const chatCallIngPage = '/chat_call_ing_page';
  static const chatSearchPage = '/chat_search_page';

  //mine
  static const mineVipPage = '/mine_vip_page';
  static const walletPage = '/wallet_page';
  static const withDrawIndexPage = '/with_draw_index_page';
  static const withDrawBindPage = '/with_draw_bind_page';
  static const withDrawSucPage = '/with_draw_suc_page';

  static const attentionFansPage = '/attention_fans';
  static const editDesc = '/edit_desc';
  static const editUserItem = '/edit_userInfo_item';

  static const setterPasswordPage = '/Setter_passwordPage';
  static const setterRemarkPage = '/Setter_Remark';

  static const medalWall = '/medalWall';
  static const medalWallDetail = '/medalWallDetail';
  static const medalWallApply = '/medalWallApply';
  static const personalityTest = '/personalityTest';

  static const giftWall = '/giftWall';
  static const myRanking = '/myRanking';
  static const blackList = '/blackList';
  static const editUserInfo = '/editUserInfo';
  static const userVerification = '/userVerification';
}
