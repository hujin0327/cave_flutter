import 'package:cave_flutter/pages/cave/add_cave_post/add_cave_post_view.dart';
import 'package:cave_flutter/pages/cave/cave_detail/cave_detail_view.dart';
import 'package:cave_flutter/pages/cave/cave_group_chat/whl_chat_group_room_view.dart';
import 'package:cave_flutter/pages/cave/cave_find/cave_find_view.dart';
import 'package:cave_flutter/pages/cave/cave_home/cave_home_view.dart';
import 'package:cave_flutter/pages/cave/cave_mutual/cave_page_view.dart';
import 'package:cave_flutter/pages/cave/cave_search/cave_search_view.dart';
import 'package:cave_flutter/pages/cave/cave_setting/cave_set_notice/cave_set_notice_view.dart';
import 'package:cave_flutter/pages/cave/cave_setting/cave_setting_view.dart';
import 'package:cave_flutter/pages/cave/cave_view.dart';
import 'package:cave_flutter/pages/cave/join_cave/join_cave_view.dart';
import 'package:cave_flutter/pages/cave/red_envelopes/red_envelopes_view.dart';
import 'package:cave_flutter/pages/square/square_view.dart';
import 'package:cave_flutter/pages/whl_chat/chat_call/chat_call_view.dart';
import 'package:cave_flutter/pages/whl_chat/chat_call_ing/chat_call_ing_view.dart';
import 'package:cave_flutter/pages/whl_chat/chat_search/chat_search_view.dart';
import 'package:cave_flutter/pages/whl_chat/setter_remark/setter_remark_view.dart';
import 'package:cave_flutter/pages/whl_chat/whl_chat_room/send_red_group/send_red_group_view.dart';
import 'package:cave_flutter/pages/whl_chat/whl_chat_room/send_red_person/send_red_person_view.dart';
import 'package:cave_flutter/pages/whl_chat/whl_chat_room/whl_chat_room_view.dart';
import 'package:cave_flutter/pages/whl_chat/whl_chat_room/whl_open_file/whl_open_file_view.dart';
import 'package:cave_flutter/pages/whl_home/search/search_view.dart';
import 'package:cave_flutter/pages/whl_home/whl_product_detail/whl_product_detail_view.dart';
import 'package:cave_flutter/pages/whl_home/whl_publish/whl_publish_view.dart';
import 'package:cave_flutter/pages/whl_login/forgot_password/forgot_password_view.dart';
import 'package:cave_flutter/pages/whl_login/login_for_password/login_for_password_view.dart';
import 'package:cave_flutter/pages/whl_login/login_for_phone/login_for_phone_view.dart';
import 'package:cave_flutter/pages/whl_login/login_for_phone/sure_phone_code/sure_phone_code_view.dart';
import 'package:cave_flutter/pages/whl_login/whl_login_view.dart';
import 'package:cave_flutter/pages/whl_main/whl_main_view.dart';
import 'package:cave_flutter/pages/whl_mine/attention_fans/attention_fans_view.dart';
import 'package:cave_flutter/pages/whl_mine/blacklist/blacklist_view.dart';
import 'package:cave_flutter/pages/whl_mine/cave_welfare/cave_welfare_view.dart';
import 'package:cave_flutter/pages/whl_mine/edit_userinfo/edit_userinfo_item/edit_userinfo_item_view.dart';
import 'package:cave_flutter/pages/whl_mine/edit_userinfo/edit_userinfo_view.dart';
import 'package:cave_flutter/pages/whl_mine/gift_wall/gift_wall_view.dart';
import 'package:cave_flutter/pages/whl_mine/medal_wall/medal_wall_apply.dart';
import 'package:cave_flutter/pages/whl_mine/medal_wall/medal_wall_detail_view.dart';
import 'package:cave_flutter/pages/whl_mine/mine_vip/mine_vip_view.dart';
import 'package:cave_flutter/pages/whl_mine/my_ranking/my_ranking_view.dart';
import 'package:cave_flutter/pages/whl_mine/personality_test/personality_test_view.dart';
import 'package:cave_flutter/pages/whl_mine/setting/account_safe/account_safe_view.dart';
import 'package:cave_flutter/pages/whl_mine/setting/real_name_authentication/user_verification_view.dart';
import 'package:cave_flutter/pages/whl_mine/share/qr_code/qr_code_view.dart';
import 'package:cave_flutter/pages/whl_mine/share/share_view.dart';
import 'package:cave_flutter/pages/whl_mine/whl_coin_shop/whl_coin_shop_view.dart';
import 'package:cave_flutter/pages/whl_mine/whl_other_user_info/whl_other_user_info_view.dart';
import 'package:cave_flutter/pages/whl_mine/whl_setting_user/whl_setting_user_view.dart';
import 'package:cave_flutter/pages/whl_mine/whl_wallet/whl_wallet_view.dart';
import 'package:cave_flutter/pages/whl_mine/with_draw/bind/with_draw_bind_binding.dart';
import 'package:cave_flutter/pages/whl_mine/with_draw/bind/with_draw_bind_view.dart';
import 'package:cave_flutter/pages/whl_mine/with_draw/index/with_draw_index_binding.dart';
import 'package:cave_flutter/pages/whl_mine/with_draw/index/with_draw_index_view.dart';
import 'package:cave_flutter/pages/whl_mine/with_draw/suc/with_draw_suc_binding.dart';
import 'package:cave_flutter/pages/whl_mine/with_draw/suc/with_draw_suc_view.dart';
import 'package:cave_flutter/widgets/web_view/view.dart';
import 'package:get/get.dart';

import '../pages/cave/cave_mutual/cave_mutual_view.dart';
import '../pages/cave/create_cave/create_cave_view.dart';
import '../pages/whl_chat/new_friend/new_friend_auth/new_friend_auth_view.dart';
import '../pages/whl_chat/new_friend/new_friend_setting/new_friend_setting_view.dart';
import '../pages/whl_chat/new_friend/new_friend_view.dart';
import '../pages/whl_chat/whl_chat_room/chat_setting/chat_setting_view.dart';
import '../pages/whl_chat/whl_chat_room/chat_setting/complaint/complaint_view.dart';
import '../pages/whl_chat/whl_chat_room/notice/notice_view.dart';
import '../pages/whl_home/heartbeat_radio/heartbeat_radio_view.dart';
import '../pages/whl_home/screen/screen_view.dart';
import '../pages/whl_home/topic_list/topic_list_view.dart';
import '../pages/whl_login/complete_user_info/complete_user_info_view.dart';
import '../pages/whl_login/complete_user_info/improve_information/improve_information_view.dart';
import '../pages/whl_login/complete_user_info/selected_sex/selected_sex_view.dart';
import '../pages/whl_login/complete_user_info/sex_orientation/sex_orientation_view.dart';
import '../pages/whl_login/setter_password/setter_password_view.dart';
import '../pages/whl_mine/edit_desc/edit_desc_view.dart';
import '../pages/whl_mine/medal_wall/medal_wall_view.dart';
import '../pages/whl_mine/user_info/user_info_view.dart';
import '../pages/whl_mine/visitors/visitors_view.dart';
import '../pages/whl_splash/whl_splash_view.dart';

part './whl_app_routes.dart';

abstract class WhlAppPages {
  static final pages = [
    GetPage(
      name: Routes.splash,
      page: () => WhlSplashPage(),
    ),
    GetPage(name: Routes.main, page: () => WhlMainPage()),
    GetPage(name: Routes.login, page: () => WhlLoginPage()),

    GetPage(name: Routes.square, page: () => SquarePage()),

    GetPage(name: Routes.settingUserInfo, page: () => WhlSettingUserPage()),
    GetPage(name: Routes.roomPage, page: () => WhlChatRoomPage()),
    GetPage(name: Routes.openFile, page: () => WhlOpenFilePage()),
    GetPage(name: Routes.publish, page: () => WhlPublishPage()),
    GetPage(name: Routes.productDetail, page: () => WhlProductDetailPage()),
    GetPage(name: Routes.otherUserInfo, page: () => WhlOtherUserInfoPage()),
    GetPage(name: Routes.coinShop, page: () => WhlCoinShopPage()),
    GetPage(name: Routes.webView, page: () => WhlWebViewPage()),
    GetPage(
        name: Routes.loginForPhone,
        page: () => LoginForPhonePage(),
        fullscreenDialog: true),
    GetPage(
        name: Routes.surePhoneCode,
        page: () => SurePhoneCodePage(),
        fullscreenDialog: true),
    GetPage(name: Routes.completeUserInfo, page: () => CompleteUserInfoPage()),
    GetPage(name: Routes.selectedSexPage, page: () => SelectedSexPage()),
    GetPage(name: Routes.sexOrientation, page: () => SexOrientationPage()),
    GetPage(
        name: Routes.loginForPassword,
        page: () => LoginForPasswordPage(),
        fullscreenDialog: true),
    GetPage(
        name: Routes.forgotPassword,
        page: () => ForgotPasswordPage(),
        fullscreenDialog: true),
    GetPage(
        name: Routes.improveInformation, page: () => ImproveInformationPage()),
    GetPage(name: Routes.screen, page: () => ScreenPage()),
    GetPage(name: Routes.search, page: () => SearchPage()),
    GetPage(name: Routes.heartbeatRadio, page: () => HeartbeatRadioPage()),
    GetPage(name: Routes.share, page: () => SharePage()),
    GetPage(name: Routes.qrCode, page: () => QrCodePage()),
    GetPage(name: Routes.accountSafe, page: () => AccountSafePage()),
    GetPage(name: Routes.userInfo, page: () => UserInfoPage()),
    GetPage(name: Routes.visitors, page: () => VisitorsPage()),
    GetPage(name: Routes.chatSetting, page: () => ChatSettingPage()),
    GetPage(name: Routes.sendRedPerson, page: () => SendRedPersonPage()),
    GetPage(name: Routes.sendRedGroup, page: () => SendRedGroupPage()),
    GetPage(name: Routes.complaintPage, page: () => ComplaintPage()),
    GetPage(name: Routes.topicList, page: () => TopicListPage()),
    GetPage(name: Routes.newFriend, page: () => NewFriendPage()),
    GetPage(name: Routes.newFriendSetting, page: () => NewFriendSettingPage()),
    GetPage(name: Routes.newFriendAuth, page: () => NewFriendAuthPage()),
    GetPage(name: Routes.notice, page: () => NoticePage()),

//cave
    GetPage(name: Routes.cavePage, page: () => CavePage()),
    GetPage(name: Routes.caveHomePage, page: () => Cave_homePage()),
    GetPage(name: Routes.caveSearchPage, page: () => Cave_searchPage()),
    GetPage(name: Routes.caveDetailPage, page: () => Cave_detailPage()),
    GetPage(name: Routes.caveJoinPage, page: () => Join_cavePage()),
    GetPage(name: Routes.caveRedEnvelopePage, page: () => Red_envelopesPage()),
    GetPage(name: Routes.createCavePage, page: () => Create_cavePage()),
    GetPage(name: Routes.caveSettingPage, page: () => CaveSettingPage()),
    GetPage(name: Routes.caveSetNoticePage, page: () => CaveSetNoticePage()),
    GetPage(name: Routes.addCavePost, page: () => AddCavePostPage()),
    GetPage(name: Routes.caveGroupChat, page: () => WhlChatGroupRoomPage()),
    GetPage(name: Routes.cavePublic, page: () => Cave_welfarePage()),
    GetPage(name: Routes.caveFindPage, page: () => CaveFindPage()),
    GetPage(name: Routes.caveMutualPage, page: () => CavePageView()),

/*chat*/
    GetPage(name: Routes.chatCallPage, page: () => Chat_callPage()),
    GetPage(name: Routes.chatCallIngPage, page: () => Chat_call_ingPage()),
    GetPage(name: Routes.chatSearchPage, page: () => Chat_searchPage()),

//我的
    GetPage(name: Routes.mineVipPage, page: () => MineVipPage()),
    GetPage(name: Routes.walletPage, page: () => WhlWalletPage()),
    GetPage(
        name: Routes.withDrawIndexPage,
        page: () => WithDrawIndexPage(),
        binding: WithDrawIndexBinding()),
    GetPage(
        name: Routes.withDrawBindPage,
        page: () => WithDrawBindPage(),
        binding: WithDrawBindBinding()),
    GetPage(
        name: Routes.withDrawSucPage,
        page: () => WithDrawSucPage(),
        binding: WithDrawSucBinding()),

    GetPage(name: Routes.attentionFansPage, page: () => Attention_fansPage()),
    GetPage(name: Routes.editDesc, page: () => Edit_descPage()),
    GetPage(name: Routes.editUserItem, page: () => Edit_userinfo_itemPage()),
    GetPage(name: Routes.setterPasswordPage, page: () => Setter_passwordPage()),
    GetPage(name: Routes.setterRemarkPage, page: () => Setter_remarkPage()),

    GetPage(name: Routes.medalWall, page: () => MedalWallPage()),
    GetPage(name: Routes.personalityTest, page: () => PersonalityTestPage()),

    GetPage(name: Routes.giftWall, page: () => Gift_wallPage()),
    GetPage(name: Routes.myRanking, page: () => My_rankingPage()),
    GetPage(name: Routes.blackList, page: () => BlacklistPage()),
    GetPage(name: Routes.editUserInfo, page: () => Edit_userinfoPage()),
    GetPage(name: Routes.userVerification, page: () => UserVerificationPage()),
  ];
}
