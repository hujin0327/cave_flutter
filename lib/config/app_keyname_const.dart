
class APPKeyNameConst {
  static const devicesUuid = "devices_id_uuid"; //设备Id

  //用户信息设置是否完成
  static String infoComplete = "infoComplete";
  //动态草稿
  static String dynamicDraft = "dynamicDraft";
  //登录用户
  static String allLoginAccount = "allLoginAccount";

}