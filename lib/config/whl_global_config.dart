import 'package:package_info_plus/package_info_plus.dart';

class WHLGlobalConfig {
  static late PackageInfo packageInfo;
  static String userServiceAccountId = '';
  static String userServiceAccountAvatar = '';
  static String userPlaceholderAvatar = 'icon_app_logo.png';

  //0 DEV 2 PRO
  static int baseUrlType = 0;

  //TODO:test 支付宝appId
  static const String ALIPAY_APPID = 'your alipay appId'; // 支付/登录


  static WHLGlobalConfig? _instance;

  // 私有的命名构造函数
  WHLGlobalConfig._internal();

  static WHLGlobalConfig getInstance() {
    _instance ??= WHLGlobalConfig._internal();
    return _instance!;
  }


  String getBaseUrl() {
    int baseUrlType = WHLGlobalConfig.baseUrlType;
    if(baseUrlType == 0){
      return "http://app.cave-meta.com:7633";
    }else{
      return "http://app.cave-meta.com:7600";
    }
  }

  String getImageBaseUrl() {
    int baseUrlType = WHLGlobalConfig.baseUrlType;
    if(baseUrlType == 0){
      return "http://app.cave-meta.com:9002";
    }else{
      return "http://app.cave-meta.com:9002";
    }
  }
}