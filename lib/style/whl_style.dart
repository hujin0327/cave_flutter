import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

//主题色
Color kAppThemeColor = Colors.black;
Color kAppLeftColor = kAppColor('#F2C661');
Color kAppRightColor = kAppColor('#FFA216');
Color kAppMainColor = const Color(0xFF6D4EFA);
//背景色
// Color kAppBcgColor = const Color(0xFFF9F9F9);
Color kAppBcgColor = kAppColor("#F1F5FD");
//一级文字
Color kAppTextColor = kAppColor('#000000');
//二级文字
Color kAppSubTextColor = kAppColor('#202020');
//三级文字
Color kAppSub2TextColor = kAppColor('#000000').withOpacity(0.3);

Color kAppDisabledTextColor = kAppColor('#C1C1C1');
Color kAppHitTextColor = kAppColor('#D0D0D0');

//二级文字
Color kAppTwoTextColor = kAppColor('#666666');
//三级文字
Color kAppSub3TextColor = kAppColor('#979797');

Color kAppStrokeColor = kAppColor('#DCDCDC');
//vip 背景色
Color kAppLSbgColor = kAppColor('#060709');
//阴影
Color kShadowColor = const Color(0xeef8f8f8);
//白色
Color kAppWhiteColor = const Color(0xFFFFFFFF);

///黑色
Color kAppBlackColor = const Color(0xFF333333);
//下划线
Color kAppLineColor = const Color(0xFF979797).withOpacity(0.1);



Color kAppColor(String hex) {
  try {
    String colorStr = hex.replaceAll("#", "");
    String prefix = (8 == hex.length) ? "0x" : "0xFF";
    return Color(int.parse(prefix + colorStr));
  } catch (e) {
    return Colors.white;
  }
}

Color kAppColorOpacity(String hex, double opacity) {
  try {
    String colorStr = hex.replaceAll("#", "");
    String prefix = (8 == hex.length) ? "0x" : "0xFF";
    return Color(int.parse(prefix + colorStr)).withOpacity(opacity);
  } catch (e) {
    return Colors.white;
  }
}

Divider kAppDivider = Divider(height: 1, color: kAppColor("#CCCCCC"));

Color randomColor() {
  var random = Random();
  int r = random.nextInt(255);
  int g = random.nextInt(255);
  int b = random.nextInt(255);
  return Color.fromARGB(255, r, g, b);
}

BoxDecoration bottomLineBoxDecoration = BoxDecoration(
  // borderRadius: BorderRadius.only(Radius.circular(4.w)),
  border: Border(bottom: BorderSide(color: kAppLineColor, width: 0.5)),
  color: kAppWhiteColor,
);

BoxDecoration bgShadowViewBoxDecoration({Color? bgColor, double? radiu, Color? shadowColor}) {
  return BoxDecoration(color: bgColor ?? kAppWhiteColor, borderRadius: BorderRadius.circular(radiu ?? 0), boxShadow: [
    BoxShadow(
      color: shadowColor ?? kAppSub3TextColor,
      blurRadius: 12.0,
      offset: const Offset(0.0, 5.0),
      spreadRadius: 1.0,
    )
  ]);
}

BoxDecoration radiusBoxDecoration({Color? bgColor, double? radiu}) {
  return BoxDecoration(
      borderRadius: BorderRadius.all(
        Radius.circular(radiu ?? 8.w),
      ),
      color: bgColor ?? kAppWhiteColor);
}

BoxDecoration topRadiusBoxDecoration({Color? bgColor, double? radiu}) {
  return BoxDecoration(
      borderRadius: BorderRadius.only(topLeft: Radius.circular(radiu ?? 8.w), topRight: Radius.circular(radiu ?? 8.w)),
      color: bgColor ?? kAppWhiteColor);
}

BoxDecoration bottomRadiusBoxDecoration({Color? bgColor, double? radiu}) {
  return BoxDecoration(
      borderRadius: BorderRadius.only(bottomLeft: Radius.circular(radiu ?? 8.w), bottomRight: Radius.circular(radiu ?? 8.w)),
      color: bgColor ?? kAppWhiteColor);
}

BoxDecoration leftRadiusBoxDecoration({Color? bgColor, double? radiu}) {
  return BoxDecoration(
      borderRadius: BorderRadius.only(topLeft: Radius.circular(radiu ?? 8.w), bottomLeft: Radius.circular(radiu ?? 8.w)),
      color: bgColor ?? kAppWhiteColor);
}

BoxDecoration rightRadiusBoxDecoration({Color? bgColor, double? radiu}) {
  return BoxDecoration(
      borderRadius: BorderRadius.only(bottomRight: Radius.circular(radiu ?? 8.w), topRight: Radius.circular(radiu ?? 8.w)),
      color: bgColor ?? kAppWhiteColor);
}

BoxDecoration noTopLineRadiusBoxDecoration({
  Color? bgColor,
  double? radiu,
  double? borderWidth,
  Color? borderColor,
}) {
  return BoxDecoration(
      borderRadius: BorderRadius.all(Radius.circular(radiu ?? 8.w)),
      border: Border(
          left: BorderSide(width: borderWidth ?? 1.w, color: borderColor ?? kAppBlackColor),
          right: BorderSide(width: borderWidth ?? 1.w, color: borderColor ?? kAppBlackColor),
          bottom: BorderSide(width: borderWidth ?? 1.w, color: borderColor ?? kAppBlackColor)),
      color: bgColor ?? kAppWhiteColor);
}
