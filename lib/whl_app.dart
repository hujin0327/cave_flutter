import 'package:amap_flutter_location/amap_flutter_location.dart';
import 'package:amap_flutter_map/amap_flutter_map.dart';
import 'package:cave_flutter/common/app_version.dart';
import 'package:cave_flutter/common/chat_manager.dart';
import 'package:cave_flutter/config/whl_global_config.dart';
import 'package:cave_flutter/routes/whl_app_pages.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:cave_flutter/utils/task_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:sp_util/sp_util.dart';
import 'package:flutter_localization/flutter_localization.dart';

class WhlAppView extends StatefulWidget {
  const WhlAppView({Key? key}) : super(key: key);

  @override
  State<WhlAppView> createState() => _WhlAppViewState();
}

class _WhlAppViewState extends State<WhlAppView> with WidgetsBindingObserver {
  final FlutterLocalization _localization = FlutterLocalization.instance;

  @override
  void initState() {
    super.initState();
    List<MapLocale> locales = [];
    locales = [
      const MapLocale('en', {}),
      const MapLocale('zh', {}),
    ];
    _localization.init(
      mapLocales: locales,
      initLanguageCode: 'zh',
    );
    AMapFlutterLocation.setApiKey("cc7c500d33b2ccfd0b963432d6a28615", "845bdcecb1f3e2127922387153804eae");

    // AMapFlutterLocation.setApiKey(
    //     "cc7c500d33b2ccfd0b963432d6a28615", "cc7c500d33b2ccfd0b963432d6a28615");
    AMapFlutterLocation.updatePrivacyShow(true, true);
    AMapFlutterLocation.updatePrivacyAgree(true);
    SpUtil.getInstance();
    ChatManager.getInstance();
    TaskUtils.getInstance();
    PackageInfo.fromPlatform().then((value) {
      WHLGlobalConfig.packageInfo = value;
    });
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      AppVersion.getInstance().check();
    }
  }

  @override
  Widget build(BuildContext context) {
    final botToastBuilder = BotToastInit();
    return ScreenUtilInit(
      designSize: const Size(375, 812),
      minTextAdapt: true,
      splitScreenMode: true,
      useInheritedMediaQuery: false,
      //ScreenUtil 5.8.1的bug  必须加这个属性 否则键盘弹出会遮挡输入框  fix 2023-7-20 升级到screenUtil 5.8.4后正常了
      builder: (_, child) => GetMaterialApp(
        // showPerformanceOverlay: true,//显示性能标签
        title: 'Cave',
        builder: (context, child) {
          child = botToastBuilder(context, child);
          return GestureDetector(
              onTap: () {
                hideKeyboard(context);
              },
              child: child);
        },
        transitionDuration: const Duration(milliseconds: 100),
        defaultTransition: Transition.cupertino,
        navigatorObservers: [BotToastNavigatorObserver()],
        initialRoute: Routes.splash,
        theme: ThemeData(
          useMaterial3: false,
          scaffoldBackgroundColor: kAppBcgColor,
          hintColor: kAppColor("#8F98A7"), //提示文字颜色
          colorScheme: ColorScheme(
            primary: kAppThemeColor,
            //激活状态下的颜色
            brightness: Brightness.light,
            onPrimary: Colors.transparent,
            secondary: Colors.grey,
            onSecondary: Colors.grey,
            error: Colors.red,
            onError: Colors.red,
            background: Colors.white,
            onBackground: Colors.white,
            surface: Colors.white,
            onSurface: kAppColor("#D0D8DA"), //非激活状态下的颜色
          ),
        ),
        getPages: WhlAppPages.pages,
        supportedLocales: _localization.supportedLocales,
        localizationsDelegates: _localization.localizationsDelegates,
      ),
    );
  }
}

void hideKeyboard(BuildContext? context) {
  if (null != context) {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
      FocusManager.instance.primaryFocus!.unfocus();
    }
  }
}
