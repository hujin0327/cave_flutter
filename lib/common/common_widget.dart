import 'dart:ui';

import 'package:cave_flutter/model/dynamic_model.dart';
import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/pages/cave/cave_detail/cave_detail_more_widget.dart';
import 'package:cave_flutter/pages/square/square_more_widget.dart';
import 'package:cave_flutter/pages/whl_home/whl_publish/push_top_setting/push_top_setting_sheet.dart';
import 'package:cave_flutter/pages/whl_mine/gift_wall/gift_list_view.dart';
import 'package:cave_flutter/pages/whl_mine/privacy_setting/privacy_setting_logic.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:cave_flutter/utils/whl_record_voice_utils.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:cave_flutter/utils/whl_user_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../model/topic_model.dart';
import '../pages/square/comment/comment_view.dart';
import '../routes/whl_app_pages.dart';
import '../style/whl_style.dart';
import '../widgets/brick/widget/basic_widget.dart';
import '../widgets/brick/widget/image_widget.dart';
import '../widgets/brick/widget/text_widget.dart';
import 'common_action.dart';

enum MyButtonType {
  imageInTop,
  imageInLeft,
  imageInBottom,
  imageInRight,
}

//毛玻璃效果
Widget visualEffectWidget(
    {double? width,
    double? height,
    Color? color = Colors.white,
    double? opacity,
    Widget? child}) {
  return ClipRect(
      child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
          child: Container(
            width: width,
            height: height,
            decoration: BoxDecoration(
                color: (color ?? Colors.white).withOpacity(opacity ?? 0.1)),
            child: child,
          )));
}

Widget noBorderTextField(
    {TextInputType? keyboardType,
    TextAlign? textAlign,
    Color? textColor,
    double? fontSize,
    String? hint,
    Color? hintTextColor,
    EdgeInsetsGeometry? contentPadding,
    ValueChanged<String>? onChanged,
    VoidCallback? onEditingComplete,
    ValueChanged<String>? onSubmitted}) {
  return TextField(
    keyboardType: keyboardType ?? TextInputType.text,
    textAlign: textAlign ?? TextAlign.start,
    decoration: InputDecoration(
      border: InputBorder.none,
      contentPadding: contentPadding ?? EdgeInsets.zero,
      isDense: true,
      hintText: hint ?? '',
      hintStyle: TextStyle(
          color: hintTextColor ?? kAppSub3TextColor,
          fontSize: fontSize ?? 14.sp),
    ),
    style: TextStyle(
        color: textColor ?? kAppTextColor, fontSize: fontSize ?? 14.sp),
    onChanged: onChanged,
    onSubmitted: onSubmitted,
    onEditingComplete: onEditingComplete,
  );
}

Widget noBorderCTextField(
    {TextEditingController? controller,
    TextInputType? keyboardType,
    TextAlign? textAlign,
    Color? textColor,
    double? fontSize,
    FontWeight? fontWeight,
    String? hint,
    Color? hintTextColor,
    FontWeight? hintFontWeight,
    bool? obscureText,
    bool? readOnly,
    int? maxLine,
    int? maxLength,
    List<TextInputFormatter>? inputFormatters,
    EdgeInsetsGeometry? contentPadding,
    ValueChanged<String>? onChanged,
    VoidCallback? onEditingComplete,
    ValueChanged<String>? onSubmitted}) {
  return TextField(
    keyboardType: keyboardType ?? TextInputType.text,
    textAlign: textAlign ?? TextAlign.start,
    controller: controller,
    obscureText: obscureText ?? false,
    maxLines: maxLine ?? 1,
    maxLength: maxLength,
    inputFormatters: inputFormatters,
    readOnly: readOnly ?? false,
    decoration: InputDecoration(
        isCollapsed: true,
        hintText: hint ?? "",
        contentPadding: contentPadding ?? EdgeInsets.zero,
        border: const OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.transparent,
          ),
        ),
        enabledBorder: const OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.transparent,
          ),
        ),
        disabledBorder: const OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.transparent,
          ),
        ),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.transparent,
          ),
        ),
        hintStyle: TextStyle(
            color: hintTextColor ?? kAppSub3TextColor,
            fontWeight: hintFontWeight,
            fontSize: fontSize ?? 14.sp)),
    style: TextStyle(
        color: textColor ?? kAppTextColor,
        fontSize: fontSize ?? 14.sp,
        fontWeight: fontWeight),
    onChanged: onChanged,
    onSubmitted: onSubmitted,
    onEditingComplete: onEditingComplete,
  );
}

Widget leftRightDetailVisitView(
  String title, {
  EdgeInsetsGeometry? margin,
  Color? titleColor,
  double? titleFont,
  Widget? titleWidget,
  FontWeight? titleFontWeight,
  String? value = "",
  Color? valueColor,
  double? valueFont,
  FontWeight? valueFontWeight,
  bool? hasRedView = false,
  String? rightImageName = "",
  double? imageWidth,
  double? imageHeight,
  Widget? rightWidget,
  Color? bgColor,
  GestureTapDownCallback? onTapDown,
  GestureTapUpCallback? onTapUp,
  GestureTapCallback? onTap,
  GestureLongPressCallback? onLongPress,
  GestureTapCancelCallback? onTapCancel,
}) {
  value ??= "";
  return UIRow(
    margin: margin,
    color: bgColor ?? kAppWhiteColor,
    onTap: onTap,
    onTapDown: onTapDown,
    onTapUp: onTapUp,
    onLongPress: onLongPress,
    onTapCancel: onTapCancel,
    children: [
      Container(
        constraints: BoxConstraints(
          maxWidth: value.isEmpty
              ? ScreenUtil().screenWidth - 32.w - 30.w
              : ScreenUtil().screenWidth / 2.0,
        ),
        child: titleWidget ??
            Text(
              title,
              style: TextStyle(
                color: titleColor ?? kAppSubTextColor,
                fontSize: titleFont ?? 16.sp,
                fontWeight: titleFontWeight,
              ),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
      ),
      if (hasRedView!)
        UIText(
          text: "*",
          textColor: kAppColor("#F44436"),
          margin: EdgeInsets.only(left: 4, bottom: 12.w),
        ),
      Expanded(
        child: UIText(
          shrinkWrap: false,
          margin: EdgeInsets.only(
            left: 5.w,
          ),
          text: value,
          textAlign: TextAlign.right,
          textColor: valueColor ?? kAppSub3TextColor,
          fontSize: valueFont ?? 16.sp,
          fontWeight: valueFontWeight,
          overflow: TextOverflow.ellipsis,
        ),
      ),
      if (rightWidget != null) rightWidget,
      if ((rightImageName ?? "").isNotEmpty)
        UIImage(
          margin: EdgeInsets.only(left: 5.w),
          assetImage: rightImageName,
          width: imageWidth ?? 24.w,
          height: imageHeight ?? 24.w,
        )
    ],
  );
}

Widget UIButton(
  String title, {
  EdgeInsetsGeometry? margin,
  Decoration? decoration,
  Color? titleColor,
  double? titleFont,
  AlignmentGeometry? alignment,
  FontWeight? titleFontWeight,
  String? assetImageName = "",
  String? assetPlaceHolder = "",
  String? httpImage = "",
  double? imageWidth,
  double? imageHeight,
  double? imageRadius,
  Color? imageColor,
  BoxFit? imageFit,
  double? imageTitleSpace,
  MyButtonType? buttonType = MyButtonType.imageInLeft,
  Color? bgColor,
  bool? isSmall = false,
  GestureTapDownCallback? onTapDown,
  GestureTapUpCallback? onTapUp,
  GestureTapCallback? onTap,
  GestureLongPressCallback? onLongPress,
  GestureTapCancelCallback? onTapCancel,
}) {
  if (buttonType == MyButtonType.imageInLeft ||
      buttonType == MyButtonType.imageInRight) {
    return UIRow(
      margin: margin,
      color: bgColor ?? Colors.transparent,
      decoration: decoration,
      mainAxisSize: MainAxisSize.min,
      onTap: onTap,
      onTapDown: onTapDown,
      onTapUp: onTapUp,
      onLongPress: onLongPress,
      onTapCancel: onTapCancel,
      // mainAxisAlignment: MainAxisAlignment.center,
      children: [
        if (buttonType == MyButtonType.imageInLeft)
          UIImage(
            margin: EdgeInsets.only(right: imageTitleSpace ?? 8.w),
            assetImage: assetImageName,
            httpImage: httpImage,
            assetPlaceHolder: assetPlaceHolder,
            imageColor: imageColor,
            width: imageWidth ?? 24.w,
            height: imageHeight ?? 24.w,
            radius: imageRadius ?? 0.w,
            fit: imageFit,
          ),
        isSmall!
            ? Text(
                title,
                style: TextStyle(
                  color: titleColor ?? kAppSubTextColor,
                  fontSize: titleFont ?? 16.sp,
                  fontWeight: titleFontWeight,
                ),
                overflow: TextOverflow.ellipsis,
              )
            : Expanded(
                child: Text(
                title,
                style: TextStyle(
                  color: titleColor ?? kAppSubTextColor,
                  fontSize: titleFont ?? 16.sp,
                  fontWeight: titleFontWeight,
                ),
                overflow: TextOverflow.ellipsis,
              )),
        if (buttonType == MyButtonType.imageInRight)
          UIImage(
            margin: EdgeInsets.only(left: imageTitleSpace ?? 8.w),
            assetImage: assetImageName,
            httpImage: httpImage,
            assetPlaceHolder: assetPlaceHolder,
            imageColor: imageColor,
            width: imageWidth ?? 24.w,
            height: imageHeight ?? 24.w,
            radius: imageRadius ?? 0.w,
            fit: imageFit,
          )
      ],
    );
  } else {
    return UIColumn(
      margin: margin,
      color: bgColor ?? Colors.transparent,
      decoration: decoration,
      onTap: onTap,
      onTapDown: onTapDown,
      onTapUp: onTapUp,
      onLongPress: onLongPress,
      onTapCancel: onTapCancel,
      children: [
        if (buttonType == MyButtonType.imageInTop)
          UIImage(
            assetImage: assetImageName,
            assetPlaceHolder: assetPlaceHolder,
            httpImage: httpImage,
            imageColor: imageColor,
            width: imageWidth ?? 24.w,
            height: imageHeight ?? 24.w,
            radius: imageRadius ?? 0.w,
            fit: imageFit,
          ),
        Container(
          margin: buttonType == MyButtonType.imageInTop
              ? EdgeInsets.only(top: imageTitleSpace ?? 8.w)
              : EdgeInsets.only(bottom: imageTitleSpace ?? 8.w),
          child: Text(
            title,
            style: TextStyle(
              color: titleColor ?? kAppSubTextColor,
              fontSize: titleFont ?? 16.sp,
              fontWeight: titleFontWeight,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ),
        if (buttonType == MyButtonType.imageInBottom)
          UIImage(
            assetImage: assetImageName,
            assetPlaceHolder: assetPlaceHolder,
            httpImage: httpImage,
            imageColor: imageColor,
            width: imageWidth ?? 24.w,
            height: imageHeight ?? 24.w,
            radius: imageRadius ?? 0.w,
            fit: imageFit,
          )
      ],
    );
  }
}

/// 动态数据来源
enum DynamicDataSource { Square, Cave, Collection }

Widget commonSquareView(
    {required DynamicDataSource source,
    DynamicModel? model,
    EdgeInsetsGeometry? margin,
    EdgeInsetsGeometry? padding,
    BorderRadiusGeometry? borderRadius,
    dynamic logic,
    //  是否Cave管理员
    bool isCaveMaster = false,
    bool? isCollect = false,
    Function(int type)? callBack}) {
  // String getImageBaseUrl = WHLGlobalConfig.getInstance().getImageBaseUrl();
  WhlRecordVoiceUtils recordVoiceUtils = WhlRecordVoiceUtils();
  recordVoiceUtils.initRecord();

  //  0未播放 1播放中 3暂停
  String audioStatus = '0';

  String? vipPath = getVipIcon(model?.user?.vip, model?.user?.rechargeType);
  List<PrivacySettingModel>? vacies = model?.user?.vacies;
  PrivacySettingModel? hide_location =
      vacies?.firstWhere((v) => v.configKey == 'hide_location');
  String? positionStr;
  if (hide_location?.isOpen == 1) {
    positionStr = model?.user?.time;
  } else {
    positionStr =
        '${(model?.province ?? '') + (model?.city ?? '')} ·${model?.user?.time ?? ''} ';
  }
  return UIColumn(
    margin: margin ?? EdgeInsets.only(bottom: 6.h),
    padding: padding ??
        EdgeInsets.only(
          left: 11.w,
          right: 11.w,
          top: 18.h,
          bottom: 15.h,
        ),
    decoration: BoxDecoration(
      borderRadius: borderRadius ?? BorderRadius.circular(16.w),
      gradient: LinearGradient(
        colors: [
          model?.topOrder == 4 ? kAppColor('#FFFAC7') : Colors.white,
          Colors.white,
          Colors.white
        ],
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
      ),
    ),
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      model?.topOrder == 4
          ? Row(
              children: [
                UIText(
                  // text: '（8/100）',
                  text: ' ',
                  textColor: kAppTextColor,
                  fontSize: 11.sp,
                  startDrawable: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      UIImage(
                        assetImage: 'icon_square_top.gif',
                        width: 46.w,
                      ),
                      UIImage(
                        assetImage: 'top_text.png',
                        width: 25.w,
                      )
                    ],
                  ),
                  margin: EdgeInsets.only(bottom: 12.w),
                )
              ],
            )
          : Container(),
      Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          UIImage(
            margin: EdgeInsets.only(right: 9.w),
            httpImage: model?.avatar?.toImageUrl(),
            assetPlaceHolder: 'icon_app_logo.png',
            width: 45.w,
            height: 45.w,
            disabledHttpBigImage: true,
            radius: 45.w,
            onTap: () {
              Get.toNamed(Routes.userInfo, arguments: {
                "accountId": model?.accountId,
              });
            },
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        UIText(
                          text: model?.user?.nickName,
                          textColor: kAppTextColor,
                          fontSize: 16.sp,
                          fontWeight: FontWeight.bold,
                        ),
                        if (model?.user?.levelImage != null)
                          UIImage(
                            margin: EdgeInsets.only(left: 4.w),
                            httpImage: model?.user?.levelImage?.toImageUrl(),
                            disabledHttpBigImage: true,
                            width: 18.w,
                            height: 18.w,
                          ),
                        if (vipPath != null)
                          UIImage(
                            margin: EdgeInsets.only(left: 4.w),
                            assetImage: vipPath,
                            disabledHttpBigImage: true,
                            width: 35.w,
                          ),
                        (model?.user?.medal == true &&
                                model?.user?.medalImage != null)
                            ? UIImage(
                                httpImage:
                                    model?.user?.medalImage?.toImageUrl(),
                                width: 82.w,
                                margin: EdgeInsets.only(left: 4.w),
                              )
                            : Container(),
                        // UIText(
                        //   padding: EdgeInsets.symmetric(
                        //       horizontal: 3.w, vertical: 2.w),
                        //   margin: EdgeInsets.only(left: 4.w),
                        //   text: '霸道总裁',
                        //   alignment: Alignment.center,
                        //   textColor: kAppWhiteColor,
                        //   gradientStartColor: kAppColor("#B246F7"),
                        //   gradientEndColor: kAppColor("#B246F7"),
                        //   fontSize: 8.sp,
                        //   radius: 2.w,
                        //   color: kAppColor("#F4EEFF"),
                        // ),
                        Expanded(child: Container()),
                        model?.hot == 1
                            ? UIImage(
                                margin: EdgeInsets.only(left: 9.w),
                                assetImage: 'icon_square_hot.png',
                                width: 20.w,
                                height: 20.w,
                              )
                            : Container(),
                        if (isCollect != true)
                          UIContainer(
                            child: Icon(Icons.more_vert,
                                color: kAppColor('#9E9E9E')),
                            onTap: () {
                              if (source == DynamicDataSource.Cave) {
                                Get.bottomSheet(
                                  CavePostMoreWidget(
                                    dynamicId: model?.id ?? "",
                                    isCaveMaster: isCaveMaster,
                                    isMe: model?.accountId ==
                                        WhlUserUtils.getUserInfo().accountId,
                                    callBack: (int type) {
                                      if (type == 5) {
                                        model!.essence = true;
                                        if (callBack != null) {
                                          callBack(type);
                                        }
                                      }
                                    },
                                  ),
                                );
                              } else {
                                Get.bottomSheet(
                                  square_more_widget(
                                    dynamicId: model?.id ?? "",
                                    isMe: model?.accountId ==
                                        WhlUserUtils.getUserInfo().accountId,
                                    callBack: callBack,
                                  ),
                                );
                              }
                            },
                          ),
                      ],
                    ),
                    SizedBox(
                      height: 4.w,
                    ),
                    Row(
                      children: [
                        UIText(
                          padding: EdgeInsets.only(left: 8.w, right: 7.w),
                          height: 18.w,
                          alignment: Alignment.center,
                          text: '${model?.user?.age ?? 0}岁',
                          textColor: getGenderColor(model?.user?.gender),
                          fontSize: 11.sp,
                          color: kAppColor('#F6F6F6'),
                          radius: 6.w,
                          marginDrawable: 3.w,
                          startDrawable: UIImage(
                            assetImage: getGenderIcon(model?.user?.gender),
                            height: 10.w,
                          ),
                        ),
                        UIText(
                          margin: EdgeInsets.only(left: 4.w),
                          padding: EdgeInsets.only(left: 8.w, right: 7.w),
                          height: 18.w,
                          alignment: Alignment.center,
                          text: model?.user?.sexualOrientation ?? "未知",
                          textColor: kAppColor('#444444'),
                          fontSize: 11.sp,
                          color: kAppColor('#F6F6F6'),
                          radius: 6.w,
                        ),
                        UIText(
                          margin: EdgeInsets.only(left: 4.w),
                          padding: EdgeInsets.only(left: 8.w, right: 7.w),
                          height: 18.w,
                          alignment: Alignment.center,
                          text: model?.user?.funRole ?? "未知",
                          textColor: kAppColor('#444444'),
                          fontSize: 11.sp,
                          color: kAppColor('#F6F6F6'),
                          radius: 6.w,
                        ),
                        Expanded(child: Container()),
                        UIText(
                          text: '${model?.readNum ?? 0}',
                          textColor: kAppColor('#C1C1C1'),
                          fontSize: 10.sp,
                          marginDrawable: 4.w,
                          startDrawable: UIImage(
                            assetImage: 'icon_eye.png',
                            width: 13.w,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                UIText(
                  margin: EdgeInsets.only(top: 10.w),
                  text: model?.content,
                  textColor: kAppColor('#2D2D2D'),
                  fontSize: 15.sp,
                  shrinkWrap: false,
                ),
                model?.audioUrl != null
                    ? UIContainer(
                        child: UIRow(
                          margin: EdgeInsets.only(top: 10.w, bottom: 10.w),
                          radius: 40.w,
                          mainAxisSize: MainAxisSize.min,
                          height: 33.w,
                          color: Colors.black,
                          onTap: () {
                            if (audioStatus == '0' || audioStatus == '3') {
                              recordVoiceUtils.startPlayer(
                                  model?.audioUrl?.toImageUrl() ?? "",
                                  (callBack) {
                                audioStatus = '1';
                                if (callBack == -1) {
                                  MyToast.show('播放失败');
                                  audioStatus = '0';
                                } else if (callBack == 0) {
                                  audioStatus = '1';
                                }
                              });
                            } else {
                              recordVoiceUtils.stopPlayer();
                              audioStatus = '3';
                            }
                          },
                          children: [
                            audioStatus == '1'
                                ? UIContainer(
                                    margin: EdgeInsets.only(
                                      left: 15.w,
                                      right: 10.w,
                                    ),
                                    width: 12.w,
                                    height: 12.w,
                                    color: Colors.white)
                                : UIImage(
                                    margin: EdgeInsets.only(
                                      left: 15.w,
                                      right: 10.w,
                                    ),
                                    assetImage: 'icon_voice_play.png',
                                    width: 12.w,
                                  ),
                            UIImage(
                              assetImage: 'icon_voice_voice.png',
                              width: 22.w,
                            ),
                            UIText(
                              margin: EdgeInsets.only(left: 15.w, right: 15.w),
                              text: '${model?.audioTime ?? 0}s',
                              textColor: Colors.white,
                              fontSize: 12.sp,
                              fontWeight: FontWeight.bold,
                            ),
                          ],
                        ),
                      )
                    : Container(),
                if (model?.urls?.first.imageUrl?.isNotEmpty ?? false)
                  UIImage(
                    onTap: () {
                      WhlApi.editReadNum.post({'id': model?.id}).then((value) {
                        model!.readNum = (model.readNum ?? 0) + 1;
                        logic.update();
                      });
                    },
                    margin: EdgeInsets.only(top: 13.w),
                    httpImage: model?.urls?.first.imageUrl?.toImageUrl(),
                    assetPlaceHolder: 'icon_app_logo.png',
                    width: 166.w,
                    alignment: Alignment.centerLeft,
                    height: 166.w,
                    fit: BoxFit.cover,
                    radius: 6.w,
                  ),
                // UIText(
                //   text: '${hide_location?.isOpen}',
                // ),
                if (positionStr != null)
                  Container(
                    margin: EdgeInsets.only(top: 11.w),
                    child: RichText(
                      text: TextSpan(
                          text: positionStr,
                          style: TextStyle(
                              fontSize: 10.sp, color: kAppColor('#C1C1C1')),
                          children: [
                            TextSpan(
                                text: source != DynamicDataSource.Cave &&
                                        model?.caveId != null
                                    ? '· 来自 ${model?.caveName ?? ''}'
                                    : '',
                                style: TextStyle(
                                    fontSize: 10.sp, color: kAppTextColor))
                          ]),
                    ),
                  ),
                if ((model?.talks ?? []).isNotEmpty)
                  UIContainer(
                    margin: EdgeInsets.only(top: 15.h, bottom: 7.h),
                    height: 26.h,
                    child: ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (BuildContext context, int index) {
                        TopicModel topicModel = model!.talks![index];
                        return UIText(
                          margin: EdgeInsets.only(left: index == 0 ? 0 : 8.w),
                          padding: EdgeInsets.symmetric(horizontal: 10.w),
                          text: '#${topicModel.talkName}',
                          textColor: kAppBlackColor,
                          color: kAppColor('#F8F8F8'),
                          fontSize: 11.sp,
                          alignment: Alignment.center,
                          radius: 13.h,
                        );
                      },
                      itemCount: model?.talks?.length,
                    ),
                  ),
                UIRow(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  margin: EdgeInsets.only(top: 21.w),
                  children: [
                    // UIImage(
                    //   assetImage: 'icon_square_share.png',
                    //   width: 20.w,
                    //   imageColor: model?.setting?.notForward == 1
                    //       ? kAppSub3TextColor.withOpacity(0.5)
                    //       : kAppColor("#ADADAD"),
                    //   onTap: () {
                    //     if (model?.setting?.notComment == 1) {
                    //       MyToast.show("该动态禁止转发");
                    //       return;
                    //     }
                    //     Get.toNamed(Routes.publish,
                    //         arguments: {"model": model, "isForward": true});
                    //   },
                    // ),
                    UIImage(
                      assetImage: 'icon_square_fly_grey.png',
                      width: 20.w,
                      onTap: () {
                        if (callBack != null) {
                          callBack(100);
                        }
                      },
                    ),
                    UIImage(
                      assetImage: 'icon_square_gift.png',
                      width: 20.w,
                      onTap: () {
                        sendGiftDialog(model!.accountId!);
                      },
                    ),
                    UIText(
                      text: '${model?.commentNum ?? 0}',
                      textColor: model?.setting?.notComment == 1
                          ? kAppSub3TextColor.withOpacity(0.5)
                          : kAppColor("#ADADAD"),
                      fontSize: 14.sp,
                      marginDrawable: 3.w,
                      startDrawable: UIImage(
                        assetImage: 'icon_square_comment.png',
                        width: 20.w,
                        imageColor: model?.setting?.notComment == 1
                            ? kAppSub3TextColor.withOpacity(0.5)
                            : kAppColor("#ADADAD"),
                      ),
                      onTap: () {
                        if (model?.setting?.notComment == 1) {
                          MyToast.show("该动态禁止评论");
                          return;
                        }
                        Get.bottomSheet(
                          CommentWidget(
                            dynamicId: model?.id ?? "",
                            source: source,
                          ),
                          isScrollControlled: true,
                        ).then((value) {
                          if (model?.commentNum != value['commentNum']) {
                            model?.commentNum = value['commentNum'];
                            logic.update();
                          }
                        });
                      },
                    ),
                    UIText(
                      text: '${model?.goodNum ?? 0}',
                      textColor: kAppColor('#ADADAD'),
                      fontSize: 14.sp,
                      marginDrawable: 3.w,
                      startDrawable: UIImage(
                        assetImage: model?.currentZan == true
                            ? 'icon_square_isLike.png'
                            : 'icon_square_like.png',
                        width: 20.w,
                      ),
                      onTap: () {
                        dianZanDynamicRequest(model!, dataSource: source,
                            callback: (isSuccess) {
                          if (isSuccess && logic != null) {
                            logic.update();
                          }
                        });
                      },
                    ),
                  ],
                )
              ],
            ),
          )
        ],
      )
    ],
  );
}

// 弹出送礼
void sendGiftDialog(String accountId) {
  Get.bottomSheet(
    GiftListPage(
      accountId: accountId,
    ),
    isScrollControlled: true,
  ).then((value) {
    // if (model?.commentNum != value['commentNum']) {
    //   model?.commentNum = value['commentNum'];
    //   logic.update();
    // }
  });
}
