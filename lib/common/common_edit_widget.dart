import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../style/whl_style.dart';
import '../widgets/brick/widget/basic_widget.dart';
import '../widgets/brick/widget/image_widget.dart';
import '../widgets/brick/widget/text_widget.dart';

Widget myInputItemView({
  String? title,
  EdgeInsetsGeometry? margin,
  EdgeInsetsGeometry? padding,
  Color? titleColor,
  double? titleFont,
  double? height,
  FontWeight? titleFontWeight,
  String? hintString = "请输入",
  String? text = "",
  Color? textColor,
  double? textFont,
  FontWeight? textFontWeight,
  double? textFieldRadius,
  bool? hasRedView = false,
  Color? bgColor,
  Color? textFillColor,
  bool? canEdit = true,
  bool? isMoreLine = false,
  int? maxLength,
  GestureTapDownCallback? onTapDown,
  GestureTapUpCallback? onTapUp,
  GestureTapCallback? onTap,
  GestureLongPressCallback? onLongPress,
  GestureTapCancelCallback? onTapCancel,
  ValueChanged<String>? onChanged,
  ValueChanged<String>? onEditComplete,
  TextEditingController? controller,
  FocusNode? focusNode,
}) {
  controller ??= TextEditingController();
  return UIRow(
    padding: padding ?? EdgeInsets.symmetric(horizontal: 20.w),
    crossAxisAlignment: CrossAxisAlignment.start,
    margin: margin,
    color: bgColor,
    // onChange:
    onTap: onTap,
    onTapDown: onTapDown,
    onTapUp: onTapUp,
    onLongPress: onLongPress,
    onTapCancel: onTapCancel,
    children: [
      if (title != null && title.isNotEmpty)
        UIText(
          margin: EdgeInsets.only(right: 14.w, top: 13.h),
          text: title,
          textColor: titleColor ?? kAppSubTextColor,
          fontSize: titleFont ?? 13.sp,
          fontWeight: titleFontWeight,
        ),
      if (hasRedView!)
        UIText(
          text: "*",
          textColor: kAppColor("#F44436"),
          margin: EdgeInsets.only(left: 4, top: 10.w),
        ),
      Expanded(
        child: UIContainer(
          height: height ?? (isMoreLine == true ? 110.h : 40.h),
          // margin: ,
          radius: textFieldRadius ?? 7.w,
          clipBehavior: Clip.hardEdge,
          constraints:
              (isMoreLine == true ? BoxConstraints(minHeight: 48.w) : null),
          child: UIColumn(
            color: textFillColor ?? kAppColor("#FAFAFA"),
            children: [
              Expanded(
                child: TextField(
                  controller: controller,
                  maxLength: maxLength,
                  onChanged: (value) {
                    if (onChanged != null) onChanged(value);
                  },
                  onEditingComplete: () {
                    if (onEditComplete != null) {
                      onEditComplete(controller!.value.text);
                    }
                  },
                  onSubmitted: (text) {
                    FocusScope.of(Get.context!).requestFocus(FocusNode());
                  },
                  textInputAction: TextInputAction.done,
                  maxLines: (isMoreLine == true ? null : 1),
                  style: TextStyle(
                      fontSize: textFont ?? 13.sp,
                      color: textColor ?? kAppSubTextColor,
                      fontWeight: textFontWeight),
                  decoration: InputDecoration(
                      counterText: "",
                      border: InputBorder.none,
                      contentPadding: const EdgeInsets.only(right: 8, left: 8),
                      hintText: canEdit == true ? (hintString ?? "请输入") : null,
                      hintMaxLines: 5,
                      hintStyle: TextStyle(
                          fontSize: textFont ?? 13.sp,
                          color: kAppSub3TextColor),
                      fillColor: textFillColor ?? kAppColor("#FAFAFA"),
                      filled: true),
                  enabled: canEdit ?? false,
                  focusNode: focusNode,
                ),
              ),
            ],
          ),
        ),
      ),
    ],
  );
}

Widget mySelectTypeView({
  String? title,
  EdgeInsetsGeometry? margin,
  EdgeInsetsGeometry? padding,
  Color? titleColor,
  double? titleFont,
  FontWeight? titleFontWeight,
  bool? hasRedView = false,
  Color? bgColor,
  double? spacing = 0.0,
  double? runSpacing = 0.0,
  List<Widget>? selectWidgets,
  GestureTapDownCallback? onTapDown,
  GestureTapUpCallback? onTapUp,
  GestureTapCallback? onTap,
  GestureLongPressCallback? onLongPress,
  GestureTapCancelCallback? onTapCancel,
}) {
  return UIRow(
    padding: padding ?? EdgeInsets.symmetric(horizontal: 20.w),
    crossAxisAlignment: CrossAxisAlignment.start,
    margin: margin,
    color: bgColor,
    onTap: onTap,
    onTapDown: onTapDown,
    onTapUp: onTapUp,
    onLongPress: onLongPress,
    onTapCancel: onTapCancel,
    children: [
      if (title != null && title.isNotEmpty)
        UIText(
          margin: EdgeInsets.only(right: 14.w,top: 11.h),
          text: title,
          textColor: titleColor ?? kAppSubTextColor,
          fontSize: titleFont ?? 13.sp,
          fontWeight: titleFontWeight,
        ),
      if (hasRedView!)
        UIText(
          text: "*",
          textColor: kAppColor("#F44436"),
          margin: EdgeInsets.only(left: 4, top: 10.w),
        ),
      UIContainer(
        child: Wrap(
          spacing: spacing??0.0,
          runSpacing: runSpacing??0.0,
          children: selectWidgets ?? [],
        ),
      )
      // Expanded(
      //   child: ,
      // ),
    ],
  );
}


Widget mySelectItemView(
    String title, {
      EdgeInsetsGeometry? margin,
      EdgeInsetsGeometry? padding,
      Color? titleColor,
      double? titleFont,
      Widget? titleWidget,
      FontWeight? titleFontWeight,
      String? value = "",
      Color? valueColor,
      double? valueFont,
      Alignment? valueAlignment,
      String? hintString = "请选择",
      FontWeight? valueFontWeight,
      bool? hasRedView = false,
      String? rightImageName = "",
      double? imageWidth,
      double? imageHeight,
      Widget? rightWidget,
      Color? imageColor,
      Color? bgColor,
      Color? valueBgColor,
      GestureTapDownCallback? onTapDown,
      GestureTapUpCallback? onTapUp,
      GestureTapCallback? onTap,
      GestureLongPressCallback? onLongPress,
      GestureTapCancelCallback? onTapCancel,
    }) {
  value ??= "";
  return UIRow(
    margin: margin,
    padding: padding ?? EdgeInsets.symmetric(horizontal: 20.w),
    crossAxisAlignment: CrossAxisAlignment.start,
    color: bgColor ?? kAppWhiteColor,
    onTap: onTap,
    onTapDown: onTapDown,
    onTapUp: onTapUp,
    onLongPress: onLongPress,
    onTapCancel: onTapCancel,
    children: [
      Container(
        margin: EdgeInsets.only(right: 14.w,top: 11.h),
        constraints: BoxConstraints(
          maxWidth: value.isEmpty
              ? ScreenUtil().screenWidth - 32.w - 30.w
              : ScreenUtil().screenWidth / 2.0,
        ),
        child: titleWidget ?? Text(
          title,
          style: TextStyle(
            color: titleColor ?? kAppSubTextColor,
            fontSize: titleFont ?? 13.sp,
            fontWeight: titleFontWeight,
          ),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
      ),
      if (hasRedView!)
        UIText(
          text: "*",
          textColor: kAppColor("#F44436"),
          margin: EdgeInsets.only(left: 4, top: 10.w),
        ),
      Expanded(
        child: UIRow(
          padding: EdgeInsets.symmetric(horizontal: 14.w),
          color: valueBgColor??kAppColor("#FAFAFA"),
          children: [
            Expanded(
              child: UIText(
                shrinkWrap: false,
                margin: EdgeInsets.only(
                    top: 12.w,
                    bottom: 12.w,
                    right: rightImageName!.isNotEmpty ? 0.w : 10.w),
                text: value.isNotEmpty?value:hintString,
                alignment: valueAlignment,
                textColor: value.isNotEmpty?valueColor ?? kAppSub3TextColor:kAppSub3TextColor,
                fontSize: valueFont ?? 12.sp,
                fontWeight: valueFontWeight,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            if (rightWidget != null) rightWidget,
            if (rightImageName!.isNotEmpty)
              UIImage(
                margin: EdgeInsets.only(left: 5.w),
                assetImage: rightImageName,
                width: imageWidth ?? 24.w,
                height: imageHeight ?? 24.w,
                imageColor: imageColor,
              )
          ],
        ),
      ),

    ],
  );
}
