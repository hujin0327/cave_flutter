import 'package:floor/floor.dart';

@entity
class WhlProductBean {
  @primaryKey
  int? id;
  String? title;
  String? describe;
  String? type;
  String? cover;
  int? likeNum;
  bool? isLike;
  bool? isFollowed;
  int? followNum;
  List<String>? details;
  String? userId;
  String? nickname;
  String? avatar;
  String? avatarMapPath;
  int? gender;
  int? age;
  String? country;
  String? status;
  int? callCoins;
  String? unit;
  bool? isFriend;
  bool? isMultiple;
  String? about;
  List<WhlCommentModel>? commentList;
  String? extra;
  WhlProductBean(
      {this.id,
      this.title,
      this.describe,
      this.type,
      this.cover,
      this.likeNum,
      this.isLike,
      this.isFollowed,
      this.followNum,
      this.details,
      this.userId,
      this.nickname,
      this.avatar,
      this.avatarMapPath,
      this.gender,
      this.age,
      this.country,
      this.status,
      this.callCoins,
      this.unit,
      this.isFriend,
      this.isMultiple,
      this.about,
      this.commentList,
        this.extra
      });

  WhlProductBean.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    describe = json['describe'];
    type = json['type'];
    cover = json['cover'];
    likeNum = json['likeNum'];
    isLike = json['isLike'];
    isFollowed = json['isFollowed'];
    followNum = json['followNum'];
    details = json['details']?.cast<String>();
    userId = json['userId'];
    nickname = json['nickname'];
    avatar = json['avatar'];
    avatarMapPath = json['avatarMapPath'];
    gender = json['gender'];
    age = json['age'];
    country = json['country'];
    status = json['status'];
    callCoins = json['callCoins'];
    unit = json['unit'];
    isFriend = json['isFriend'];
    isMultiple = json['isMultiple'];
    about = json['about'];
    if (json['commentList'] != null) {
      commentList = <WhlCommentModel>[];
      json['commentList'].forEach((v) {
        commentList!.add(WhlCommentModel.fromJson(v));
      });
    }
    extra = json['extra'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['title'] = title;
    data['describe'] = describe;
    data['type'] = type;
    data['cover'] = cover;
    data['likeNum'] = likeNum;
    data['isLike'] = isLike;
    data['isFollowed'] = isFollowed;
    data['followNum'] = followNum;
    data['details'] = details;
    data['userId'] = userId;
    data['nickname'] = nickname;
    data['avatar'] = avatar;
    data['avatarMapPath'] = avatarMapPath;
    data['gender'] = gender;
    data['age'] = age;
    data['country'] = country;
    data['status'] = status;
    data['callCoins'] = callCoins;
    data['unit'] = unit;
    data['isFriend'] = isFriend;
    data['isMultiple'] = isMultiple;
    data['about'] = about;
    if (commentList != null) {
      data['commentList'] = commentList!.map((v) => v.toJson()).toList();
    }
    data['extra'] = extra;
    return data;
  }
}

class WhlCommentModel {
  String? userId;
  String? nickname;
  String? avatar;
  bool? isLike;
  String? content;
  int? time;

  WhlCommentModel({this.userId, this.nickname, this.avatar, this.isLike, this.content, this.time});

  WhlCommentModel.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    nickname = json['nickname'];
    avatar = json['avatar'];
    isLike = json['isLike'];
    content = json['content'];
    time = json['time'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['userId'] = userId;
    data['nickname'] = nickname;
    data['avatar'] = avatar;
    data['isLike'] = isLike;
    data['content'] = content;
    data['time'] = time;
    return data;
  }
}
