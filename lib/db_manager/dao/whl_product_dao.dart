import 'package:floor/floor.dart';

import '../bean/whl_product_bean.dart';

@dao
abstract class WhlProductDao {
  //增
  @Insert()
  Future<void> insertProduct(WhlProductBean productBean);

  //删
  @delete
  Future<void> deleteProduct(WhlProductBean productBean);

  //改
  @update
  Future<void> updateProduct(WhlProductBean productBean);

  //查
  @Query('SELECT * FROM WhlProductBean')
  Future<List<WhlProductBean>?> findAllProducts();

  //删除所有
  @Query('DELETE FROM WhlProductBean')
  Future<void> deleteAllProducts();

  @Query('SELECT * FROM WhlProductBean where type = :type')
  Future<List<WhlProductBean>?> findProductByType(String type);

  @Query('SELECT * FROM WhlProductBean where userId = :userId')
  Future<List<WhlProductBean>?> findProductByUserId(String userId);
}
