import 'dart:async';
import 'package:cave_flutter/db_manager/dao/whl_product_dao.dart';
import 'package:floor/floor.dart';
import '../bean/whl_product_bean.dart';
import 'app_database.dart';
import 'package:sqflite/sqflite.dart' as sqflite;
// **************************************************************************
// FloorGenerator
// **************************************************************************

// ignore: avoid_classes_with_only_static_members
class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  WhlProductDao? _productDaoInstance;

  Future<sqflite.Database> open(
    String path,
    List<Migration> migrations, [
    Callback? callback,
  ]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
        await callback?.onConfigure?.call(database);
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `WhlProductBean` (`id` INTEGER, `title` TEXT, `describe` TEXT, `type` TEXT, `cover` TEXT, `likeNum` INTEGER, `isLike` INTEGER, `isFollowed` INTEGER, `followNum` INTEGER, `details` TEXT, `userId` TEXT, `nickname` TEXT, `avatar` TEXT, `avatarMapPath` TEXT, `gender` INTEGER, `age` INTEGER, `country` TEXT, `status` TEXT, `callCoins` INTEGER, `unit` TEXT, `isFriend` INTEGER, `isMultiple` INTEGER, `about` TEXT, `commentList` TEXT, `extra` TEXT, PRIMARY KEY (`id`))');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  WhlProductDao get productDao {
    return _productDaoInstance ??= _$WhlProductDao(database, changeListener);
  }
}

class _$WhlProductDao extends WhlProductDao {
  _$WhlProductDao(
    this.database,
    this.changeListener,
  )   : _queryAdapter = QueryAdapter(database),
        _whlProductBeanInsertionAdapter = InsertionAdapter(
            database,
            'WhlProductBean',
            (WhlProductBean item) => <String, Object?>{
                  'id': item.id,
                  'title': item.title,
                  'describe': item.describe,
                  'type': item.type,
                  'cover': item.cover,
                  'likeNum': item.likeNum,
                  'isLike': item.isLike == null ? null : (item.isLike! ? 1 : 0),
                  'isFollowed': item.isFollowed == null
                      ? null
                      : (item.isFollowed! ? 1 : 0),
                  'followNum': item.followNum,
                  'details': _listStringConverter.encode(item.details),
                  'userId': item.userId,
                  'nickname': item.nickname,
                  'avatar': item.avatar,
                  'avatarMapPath': item.avatarMapPath,
                  'gender': item.gender,
                  'age': item.age,
                  'country': item.country,
                  'status': item.status,
                  'callCoins': item.callCoins,
                  'unit': item.unit,
                  'isFriend':
                      item.isFriend == null ? null : (item.isFriend! ? 1 : 0),
                  'isMultiple': item.isMultiple == null
                      ? null
                      : (item.isMultiple! ? 1 : 0),
                  'about': item.about,
                  'commentList':
                      _commentModelConverter.encode(item.commentList),
                  'extra': item.extra
                }),
        _whlProductBeanUpdateAdapter = UpdateAdapter(
            database,
            'WhlProductBean',
            ['id'],
            (WhlProductBean item) => <String, Object?>{
                  'id': item.id,
                  'title': item.title,
                  'describe': item.describe,
                  'type': item.type,
                  'cover': item.cover,
                  'likeNum': item.likeNum,
                  'isLike': item.isLike == null ? null : (item.isLike! ? 1 : 0),
                  'isFollowed': item.isFollowed == null
                      ? null
                      : (item.isFollowed! ? 1 : 0),
                  'followNum': item.followNum,
                  'details': _listStringConverter.encode(item.details),
                  'userId': item.userId,
                  'nickname': item.nickname,
                  'avatar': item.avatar,
                  'avatarMapPath': item.avatarMapPath,
                  'gender': item.gender,
                  'age': item.age,
                  'country': item.country,
                  'status': item.status,
                  'callCoins': item.callCoins,
                  'unit': item.unit,
                  'isFriend':
                      item.isFriend == null ? null : (item.isFriend! ? 1 : 0),
                  'isMultiple': item.isMultiple == null
                      ? null
                      : (item.isMultiple! ? 1 : 0),
                  'about': item.about,
                  'commentList':
                      _commentModelConverter.encode(item.commentList),
                  'extra': item.extra
                }),
        _whlProductBeanDeletionAdapter = DeletionAdapter(
            database,
            'WhlProductBean',
            ['id'],
            (WhlProductBean item) => <String, Object?>{
                  'id': item.id,
                  'title': item.title,
                  'describe': item.describe,
                  'type': item.type,
                  'cover': item.cover,
                  'likeNum': item.likeNum,
                  'isLike': item.isLike == null ? null : (item.isLike! ? 1 : 0),
                  'isFollowed': item.isFollowed == null
                      ? null
                      : (item.isFollowed! ? 1 : 0),
                  'followNum': item.followNum,
                  'details': _listStringConverter.encode(item.details),
                  'userId': item.userId,
                  'nickname': item.nickname,
                  'avatar': item.avatar,
                  'avatarMapPath': item.avatarMapPath,
                  'gender': item.gender,
                  'age': item.age,
                  'country': item.country,
                  'status': item.status,
                  'callCoins': item.callCoins,
                  'unit': item.unit,
                  'isFriend':
                      item.isFriend == null ? null : (item.isFriend! ? 1 : 0),
                  'isMultiple': item.isMultiple == null
                      ? null
                      : (item.isMultiple! ? 1 : 0),
                  'about': item.about,
                  'commentList':
                      _commentModelConverter.encode(item.commentList),
                  'extra': item.extra
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<WhlProductBean> _whlProductBeanInsertionAdapter;

  final UpdateAdapter<WhlProductBean> _whlProductBeanUpdateAdapter;

  final DeletionAdapter<WhlProductBean> _whlProductBeanDeletionAdapter;

  @override
  Future<List<WhlProductBean>?> findAllProducts() async {
    return _queryAdapter.queryList('SELECT * FROM WhlProductBean',
        mapper: (Map<String, Object?> row) => WhlProductBean(
            id: row['id'] as int?,
            title: row['title'] as String?,
            describe: row['describe'] as String?,
            type: row['type'] as String?,
            cover: row['cover'] as String?,
            likeNum: row['likeNum'] as int?,
            isLike: row['isLike'] == null ? null : (row['isLike'] as int) != 0,
            isFollowed: row['isFollowed'] == null
                ? null
                : (row['isFollowed'] as int) != 0,
            followNum: row['followNum'] as int?,
            details: _listStringConverter.decode(row['details'] as String),
            userId: row['userId'] as String?,
            nickname: row['nickname'] as String?,
            avatar: row['avatar'] as String?,
            avatarMapPath: row['avatarMapPath'] as String?,
            gender: row['gender'] as int?,
            age: row['age'] as int?,
            country: row['country'] as String?,
            status: row['status'] as String?,
            callCoins: row['callCoins'] as int?,
            unit: row['unit'] as String?,
            isFriend:
                row['isFriend'] == null ? null : (row['isFriend'] as int) != 0,
            isMultiple: row['isMultiple'] == null
                ? null
                : (row['isMultiple'] as int) != 0,
            about: row['about'] as String?,
            commentList:
                _commentModelConverter.decode(row['commentList'] as String),
            extra: row['extra'] as String?));
  }

  @override
  Future<void> deleteAllProducts() async {
    await _queryAdapter.queryNoReturn('DELETE FROM WhlProductBean');
  }

  @override
  Future<List<WhlProductBean>?> findProductByType(String type) async {
    return _queryAdapter.queryList(
        'SELECT * FROM WhlProductBean where type = ?1',
        mapper: (Map<String, Object?> row) => WhlProductBean(
            id: row['id'] as int?,
            title: row['title'] as String?,
            describe: row['describe'] as String?,
            type: row['type'] as String?,
            cover: row['cover'] as String?,
            likeNum: row['likeNum'] as int?,
            isLike: row['isLike'] == null ? null : (row['isLike'] as int) != 0,
            isFollowed: row['isFollowed'] == null
                ? null
                : (row['isFollowed'] as int) != 0,
            followNum: row['followNum'] as int?,
            details: _listStringConverter.decode(row['details'] as String),
            userId: row['userId'] as String?,
            nickname: row['nickname'] as String?,
            avatar: row['avatar'] as String?,
            avatarMapPath: row['avatarMapPath'] as String?,
            gender: row['gender'] as int?,
            age: row['age'] as int?,
            country: row['country'] as String?,
            status: row['status'] as String?,
            callCoins: row['callCoins'] as int?,
            unit: row['unit'] as String?,
            isFriend:
                row['isFriend'] == null ? null : (row['isFriend'] as int) != 0,
            isMultiple: row['isMultiple'] == null
                ? null
                : (row['isMultiple'] as int) != 0,
            about: row['about'] as String?,
            commentList:
                _commentModelConverter.decode(row['commentList'] as String),
            extra: row['extra'] as String?),
        arguments: [type]);
  }

  @override
  Future<List<WhlProductBean>?> findProductByUserId(String userId) async {
    return _queryAdapter.queryList(
        'SELECT * FROM WhlProductBean where userId = ?1',
        mapper: (Map<String, Object?> row) => WhlProductBean(
            id: row['id'] as int?,
            title: row['title'] as String?,
            describe: row['describe'] as String?,
            type: row['type'] as String?,
            cover: row['cover'] as String?,
            likeNum: row['likeNum'] as int?,
            isLike: row['isLike'] == null ? null : (row['isLike'] as int) != 0,
            isFollowed: row['isFollowed'] == null
                ? null
                : (row['isFollowed'] as int) != 0,
            followNum: row['followNum'] as int?,
            details: _listStringConverter.decode(row['details'] as String),
            userId: row['userId'] as String?,
            nickname: row['nickname'] as String?,
            avatar: row['avatar'] as String?,
            avatarMapPath: row['avatarMapPath'] as String?,
            gender: row['gender'] as int?,
            age: row['age'] as int?,
            country: row['country'] as String?,
            status: row['status'] as String?,
            callCoins: row['callCoins'] as int?,
            unit: row['unit'] as String?,
            isFriend:
                row['isFriend'] == null ? null : (row['isFriend'] as int) != 0,
            isMultiple: row['isMultiple'] == null
                ? null
                : (row['isMultiple'] as int) != 0,
            about: row['about'] as String?,
            commentList:
                _commentModelConverter.decode(row['commentList'] as String),
            extra: row['extra'] as String?),
        arguments: [userId]);
  }

  @override
  Future<void> insertProduct(WhlProductBean productBean) async {
    await _whlProductBeanInsertionAdapter.insert(
        productBean, OnConflictStrategy.abort);
  }

  @override
  Future<void> updateProduct(WhlProductBean productBean) async {
    await _whlProductBeanUpdateAdapter.update(
        productBean, OnConflictStrategy.abort);
  }

  @override
  Future<void> deleteProduct(WhlProductBean productBean) async {
    await _whlProductBeanDeletionAdapter.delete(productBean);
  }
}

// ignore_for_file: unused_element
final _listStringConverter = ListStringConverter();
final _commentModelConverter = CommentModelConverter();
