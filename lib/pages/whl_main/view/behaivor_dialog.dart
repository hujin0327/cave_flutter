import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../widgets/brick/brick.dart';
import '../../whl_home/sign_in_dialog/sign_in_dialog_view.dart';

class BehaviorDialog extends StatelessWidget {
  const BehaviorDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(
        child: Wrap(
          children: [
            UIColumn(
              width: double.maxFinite,
              radius: 30.w,
              margin: EdgeInsets.symmetric(horizontal: 34.w),
              color: Colors.white,
              children: [
                UIImage(
                  margin: EdgeInsets.only(top: 22.h),
                  assetImage: 'icon_behaivor_safe.png',
                  width: 49.w,
                ),
                UIText(
                  text: 'Cave平台行为规范',
                  fontSize: 17.sp,
                  textColor: kAppTextColor,
                  fontWeight: FontWeight.bold,
                  margin: EdgeInsets.only(top: 16.h, bottom: 14.h),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 25.w),
                  child: RichText(
                      text: TextSpan(text: '尊敬的用户，应', style: TextStyle(fontSize: 11.sp, color: kAppColor('#666666')), children: [
                    TextSpan(text: '《互联网信息服务管理办法》', style: TextStyle(fontSize: 11.sp, color: Colors.black, fontWeight: FontWeight.bold)),
                    const TextSpan(text: '要求，'),
                    TextSpan(
                      text: 'Cave平台禁止发布、传播以下内容：',
                      style: TextStyle(fontSize: 11.sp, color: Colors.black, fontWeight: FontWeight.bold),
                    ),
                    const TextSpan(
                        text: '\n（一）反对宪法所确定的基本原则的；'
                            '\n（二）危害国家安全，泄露国家秘密，颠覆国家政权，破坏国家统一的；'
                            '\n（三）损害国家荣誉和利益的；'
                            '\n（四）煽动民族仇恨、民族歧视，破坏民族团结的；'
                            '\n（五）破坏国家宗教政策，宣扬邪教和封建迷信的；'
                            '\n（六）散布谣言，扰乱社会秩序，破坏社会稳定的；'
                            '\n（七）散布淫秽、色情、赌博、暴力、凶杀、恐怖或者教唆犯罪的；'
                            '\n（八）侮辱或者诽谤他人，侵害他人合法权益的；'
                            '\n（九）含有法律、行政法规禁止的其他内容的。'
                            'Cave倡导'),
                    TextSpan(text: '绿色文明交友，营造和谐、正能量', style: TextStyle(fontSize: 11.sp, color: Colors.black, fontWeight: FontWeight.bold)),
                    const TextSpan(text: '的交友环境，官方有权对违反上述内容的用户予以禁言或封禁处理，感谢您的配合!')
                  ])),
                ),
                UIRow(
                  margin: EdgeInsets.symmetric(horizontal: 100.w, vertical: 20.h),
                  children: [
                    Expanded(
                      child: UISolidButton(
                        height: 42.h,
                        text: '我已知晓',
                        color: Colors.black,
                        onTap: () {
                          Get.back();
                          // Get.dialog(SignInDialogView());
                        },
                      ),
                    ),
                  ],
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
