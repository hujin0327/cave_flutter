import 'package:cave_flutter/common/app_version.dart';
import 'package:cave_flutter/common/chat_rtc_manager.dart';
import 'package:cave_flutter/pages/cave/cave_home/cave_home_logic.dart';
import 'package:cave_flutter/pages/whl_chat/whl_chat_view.dart';
import 'package:cave_flutter/pages/whl_explore/whl_explore_view.dart';
import 'package:cave_flutter/pages/whl_mine/setting/setting_view.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/whl_bottombar.dart';
import 'package:cave_flutter/widgets/whl_keep_alive_widget.dart';
import 'package:floating_overlay/floating_overlay.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import '../../widgets/brick/brick.dart';

// import '../../widgets/curved_navigation_bar/curved_navigation_bar.dart';
import '../../widgets/curved_navigation_bar_1/curved_navigation_bar.dart';
import '../../widgets/curved_navigation_bar_1/curved_navigation_bar_item.dart';
import '../cave/cave_home/cave_home_view.dart';
import '../square/square_view.dart';
import '../whl_chat/chat_call/small_video_call_widget.dart';
import '../whl_chat/whl_chat_logic.dart';
import '../whl_home/whl_home_view.dart';
import '../whl_mine/whl_mine_view.dart';
import 'whl_main_logic.dart';

class WhlMainPage extends StatelessWidget {
  WhlMainPage({Key? key}) : super(key: key);

  final logic = Get.put(WhlMainLogic());

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      child: Scaffold(
        key: logic.globalKey,
        backgroundColor: Colors.white,
        body: FloatingOverlay(
          controller: logic.overlayController,
          floatingChild: Hero(
            tag: "videoCall",
            child: UIContainer(
              width: 130.w,
              height: 170.w,
              padding: EdgeInsets.all(15.w),
              alignment: Alignment.topRight,
              child: SmallCallWidget(),
            ),
          ),
          child: GetBuilder<WhlMainLogic>(builder: (logic) {
            return IndexedStack(
              index: logic.selectedIndex,
              children: [
                WhlKeepAliveWidget(child: WhlHomePage()),
                WhlKeepAliveWidget(child: SquarePage()),
                WhlKeepAliveWidget(child: Cave_homePage()),
                WhlKeepAliveWidget(child: WhlChatPage()),
                WhlKeepAliveWidget(child: WhlMinePage()),
              ],
            );
          }),
        ),
        bottomNavigationBar: Container(
          padding: EdgeInsets.only(bottom: 0.h),
          child: GetBuilder<WhlMainLogic>(builder: (logic) {
            return CurvedNavigationBar(
              backgroundColor: logic.selectedIndex == 2
                  ? kAppColor('#F8F8F8')
                  : kAppColor('#F8F8F8'),
              color: logic.selectedIndex == 2 ? Colors.black : Colors.black,
              buttonBackgroundColor: kAppColor('#FFE800'),
              items: logic.bottomList.map((e) {
                int index = logic.bottomList.indexOf(e);
                return CurvedNavigationBarItem(
                    child: GetBuilder<WhlMainLogic>(builder: (logic) {
                      return UIImage(
                        assetImage: e.icon,
                        width: 27.w,
                        height: 27.w,
                        imageColor: logic.selectedIndex == index
                            ? kAppTextColor
                            : Colors.white.withOpacity(0.3),
                      );
                    }),
                    label: e.title,
                    labelStyle: TextStyle(
                        fontSize: 10.sp,
                        color: logic.selectedIndex == index
                            ? Colors.white
                            : Colors.white.withOpacity(0.3)));
              }).toList(),
              onTap: (index) {
                AppVersion.getInstance().check();

                if (index == 3) {
                  final chatLogic = Get.find<WhlChatLogic>();
                  chatLogic.onGetConversationList();
                } else if (index == 2) {
                  final caveLogic = Get.find<Cave_homeLogic>();
                  caveLogic.onInit();
                }
                logic.selectedIndex = index;
                logic.update();
              },
            );
          }),
        ),
        endDrawer: SettingPage(),
      ),
    );
  }
}
