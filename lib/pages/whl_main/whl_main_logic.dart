import 'package:cave_flutter/common/app_version.dart';
import 'package:cave_flutter/common/chat_manager.dart';
import 'package:cave_flutter/model/whl_user_info_model.dart';
import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/utils/whl_user_utils.dart';
import 'package:floating_overlay/floating_overlay.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import '../../common/common_action.dart';
import '../../routes/whl_app_pages.dart';
import '../../widgets/curved_navigation_bar/curved_navigation_bar.dart';
import 'view/behaivor_dialog.dart';

class WhlBottomBarModel {
  String icon;
  String activeIcon;
  String? title;

  WhlBottomBarModel(this.icon, this.activeIcon, {this.title});
}

class WhlMainLogic extends GetxController {
  GlobalKey<CurvedNavigationBarState> bottomNavigationKey = GlobalKey();
  List<WhlBottomBarModel> bottomList = [
    WhlBottomBarModel('icon_tab_0_default.png', 'icon_tab_0_select.png',
        title: '交友'),
    WhlBottomBarModel('icon_tab_1_default.png', 'icon_tab_1_select.png',
        title: '广场'),
    WhlBottomBarModel('icon_tab_2_default.png', 'icon_tab_2_select.png',
        title: 'Cave'),
    WhlBottomBarModel('icon_tab_3_default.png', 'icon_tab_3_select.png',
        title: '消息'),
    WhlBottomBarModel('icon_tab_3_default.png', 'icon_tab_3_select.png',
        title: '我的'),
  ];
  int selectedIndex = 0;
  GlobalKey<ScaffoldState> globalKey = GlobalKey();

  final overlayController = FloatingOverlayController.absoluteSize(
    maxSize: Size(200.w, 260.w),
    minSize: Size(130.w, 170.w),
    start:
        Offset(ScreenUtil().screenWidth - 130.w, ScreenUtil().statusBarHeight),
    constrained: true,
  );

  doClickBottomBar(i) {
    selectedIndex = i;
    update();
  }

  @override
  void onInit() {
    super.onInit();
    Future.delayed(Duration(milliseconds: 200)).then((value) {
      Get.dialog(BehaviorDialog());
      AppVersion.getInstance().check();
    });
    getIMToken();
    getUserInfo();
  }

  getIMToken() {
    WhlApi.getIMToken.get({}).then((value) {
      if (value.isSuccess()) {
        String token = value.data["token"] ?? "";
        String account = value.data["acid"] ?? "";
        ChatManager.getInstance().loginIM(account, token);
        print("account = $account token = $token");
      } else {
        ChatManager.getInstance().loginIM("", "");
      }
    });
  }

  onGetDataList() async {}

  showSmVideoView() {
    overlayController.show();
  }

  hideSmVideoView() {
    overlayController.hide();
  }

  openDrawer() {
    globalKey.currentState?.openEndDrawer();
  }
}
