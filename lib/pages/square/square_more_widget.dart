import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/pages/whl_chat/whl_chat_room/chat_setting/complaint/complaint_view.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../widgets/brick/widget/basic_widget.dart';

class square_more_widget extends StatefulWidget {
  String dynamicId = "";
  bool? isMe = false;
  bool? isCollect = false;
  Function(int type)? callBack;

  square_more_widget(
      {Key? key, this.dynamicId = "", this.isCollect, this.isMe, this.callBack})
      : super(key: key);

  @override
  State<square_more_widget> createState() => _square_more_widgetState();
}

class _square_more_widgetState extends State<square_more_widget> {
  List data = [
    {"title": "收藏", "id": 0},
    {"title": "举报", "id": 1}
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if (widget.isMe == true) {
      data = [
        {"title": "置顶", "id": 2},
        {"title": "编辑", "id": 3},
        {"title": "删除", "id": 4},
        {"title": "收藏", "id": 0}
      ];
    }
  }

  collectRequest() {
    WhlApi.squareCollect.post({"id": widget.dynamicId}).then((value) {
      if (value.isSuccess()) {
        MyToast.show("收藏成功");
        Get.back();
        setState(() {
          widget.isCollect = true;
        });
      }
    });
  }

  deleteRequest() {
    WhlApi.squareDelete.post({"id": widget.dynamicId}).then((value) {
      if (value.isSuccess()) {
        MyToast.show("删除成功");
        Get.back();
        if (widget.callBack != null) {
          widget.callBack!(4);
        }
      }
    });
  }

  toTopRequest() {
    WhlApi.squareToTop.post({"id": widget.dynamicId}).then((value) {
      if (value.isSuccess()) {
        MyToast.show("置顶成功");
        Get.back();
        if (widget.callBack != null) {
          widget.callBack!(2);
        }
      }
    });
  }

  updateData() {
    if (widget.isCollect == true && widget.isMe != true) {
      data = [
        {"title": "收藏", "id": 0},
        {"title": "举报", "id": 1}
      ];
    }
  }

  @override
  Widget build(BuildContext context) {
    updateData();
    return SafeArea(
      child: UIContainer(
        // color: const Color.fromRGBO(255, 255, 255, 0.3),
        decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(14), topRight: Radius.circular(14))),
        clipBehavior: Clip.hardEdge,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            //为了防止控件溢出
            Flexible(
                child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Flexible(
                      child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: data.length,
                    itemBuilder: (context, index) {
                      Map dic = data[index];
                      String title = dic["title"] ?? "";
                      String imageName = dic["imageName"] ?? "";
                      int id = dic["id"] ?? 0;
                      return Column(
                        children: <Widget>[
                          ListTile(
                              onTap: () {},
                              title: UIText(
                                text: title,
                                alignment: Alignment.center,
                                textColor: kAppTwoTextColor,
                                fontSize: 16.sp,
                                marginDrawable: 2.w,
                                startDrawable: imageName.isNotEmpty
                                    ? UIImage(
                                        // margin: EdgeInsets.only(right: 2.w),
                                        assetImage: imageName,
                                        width: 20.w,
                                        height: 20.w,
                                      )
                                    : null,
                                onTap: () {
                                  if (id == 1) {
                                    Navigator.pop(context);
                                    Get.to(() => ComplaintPage(), arguments: {
                                      'title': '广场举报',
                                      'categoryId': '1'
                                    });
                                  } else if (id == 0) {
                                    collectRequest();
                                  } else if (id == 2) {
                                    toTopRequest();
                                  } else if (id == 3) {
                                    Get.back();
                                    if (widget.callBack != null) {
                                      widget.callBack!(3);
                                    }
                                  } else if (id == 4) {
                                    deleteRequest();
                                  }
                                },
                              )),
                          index == data.length - 1
                              ? Container(
                                  color: kAppColor("#F7F7F7"),
                                  height: 12,
                                )
                              : Divider(
                                  height: 1.w,
                                  color: kAppLineColor,
                                ),
                        ],
                      );
                    },
                  )),
                ],
              ),
            )),
            GestureDetector(
              child: Container(
                height: 54,
                width: double.infinity,
                alignment: Alignment.center,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(0)),
                ),
                child: Text('取消',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 16.sp, color: kAppSubTextColor)),
              ),
              onTap: () {
                Navigator.pop(context);
              },
            )
          ],
        ),
      ),
    );
  }
}
