import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../model/topic_model.dart';
import 'topic_list_logic.dart';

class TopicListPage extends StatelessWidget {
  TopicListPage({Key? key}) : super(key: key);

  final logic = Get.put(TopicListLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: WhlAppBar(
        centerTitle: '话题广场',
        backgroundColor: Colors.white,
        isShowBottomLine: false,
      ),
      body: GetBuilder<TopicListLogic>(builder: (logic) {
        return CustomScrollView(
          slivers: [
            SliverList(
                delegate: SliverChildBuilderDelegate((context, index) {
                  TopicModel model = logic.listData[index];
                  return IntrinsicHeight(
                    child: UIRow(
                      margin: EdgeInsets.only(top: 11.w, left: 15.w, right: 15
                          .w),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.05),
                          offset: Offset(0, 1.w),
                          blurRadius: 16.w,
                          spreadRadius: 0.w,
                        )
                      ],
                      children: [
                        UIImage(
                          httpImage: model.imageUrl?.toImageUrl(),
                          assetPlaceHolder: 'icon_app_logo.png',
                          width: 105.w,
                          height: 105.w,
                          radius: 12.w,
                        ),
                        Expanded(
                          child: UIColumn(
                            margin: EdgeInsets.only(left: 10.w),
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              UIText(
                                text: '#${model.talkName}',
                                fontSize: 15.sp,
                                textColor: Colors.black,
                                fontWeight: FontWeight.bold,
                              ),
                              UIText(text: model.talkBrief,
                                  fontSize: 12.sp,
                                  textColor: kAppColor('#9E9E9E')),
                              Expanded(child: Container()),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  UIText(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 14.w, vertical: 8.w),
                                    text: '参与话题',
                                    textColor: Colors.white,
                                    fontSize: 13.sp,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                    radius: 24.w,
                                    onTap: (){
                                      logic.confirmTopic(model);
                                    },
                                  ),
                                ],
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  );
                }, childCount: logic.listData.length))
          ],
        );
      }),
    );
  }
}
