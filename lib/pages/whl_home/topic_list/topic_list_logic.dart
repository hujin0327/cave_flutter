import 'package:cave_flutter/network/whl_api.dart';
import 'package:get/get.dart';

import '../../../model/topic_model.dart';

class TopicListLogic extends GetxController {

  List listData = [];

  @override
  void onInit() {
    super.onInit();
    loadTopicData();
  }

  loadTopicData(){
    WhlApi.getTopicList.get({},withLoading: true).then((value) {
      if (value.isSuccess()) {
        listData = value.data.map((e) => TopicModel.fromJson(e)).toList();
        update();
      }
    });
  }

  confirmTopic(TopicModel model) {
    Get.back(result: model);
  }
}
