import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/pages/whl_home/whl_product_detail/whl_product_detail_logic.dart';
import 'package:cave_flutter/pages/whl_mine/whl_mine_logic.dart';
import 'package:cave_flutter/routes/whl_app_pages.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:get/get.dart';

class WhlSendGifSheetViewLogic extends GetxController {
  List<Map<String, dynamic>> giftMapList = [
    {'icon_gift_0.png': '39'},
    {'icon_gift_1.png': '69'},
    {'icon_gift_2.png': '99'},
  ];

  doClickGiftItem(Map<String, dynamic> map) async {
    // selectedGift = map.keys.first;
    // update();
    WhlMineLogic mineLogic = Get.find();
    int coin = int.parse(map.values.first);
    // if (coin > (mineLogic.userInfoModel.availableCoins ?? 0)) {
    //   MyToast.show('Insufficient balance of coins, please recharge');
    //   Get.toNamed(Routes.coinShop);
    //   return;
    // }
    BotToast.showLoading();
    Map<String, dynamic> params = {'outlay': map.values.first, 'source': 'iap'};
    // ResponseData responseData = await WhlApi.consumeCoin.post(params);
    // if (responseData.isSuccess()) {
    //   MyToast.show('Send Successfully');
    //   mineLogic.onGetUserInfo();
    //   Get.back();
    //   WhlProductDetailLogic logic = Get.find();
    //   logic.selectedGift = map.keys.first;
    //   logic.onStartSendGiftAnimation();
    // }
    // BotToast.closeAllLoading();
  }
}
