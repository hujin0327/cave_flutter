import 'package:cave_flutter/routes/whl_app_pages.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../whl_mine/whl_mine_logic.dart';
import 'whl_send_gif_sheet_view_logic.dart';

class WhlSendGifSheetViewPage extends StatefulWidget {
  WhlSendGifSheetViewPage({Key? key}) : super(key: key);

  @override
  State<WhlSendGifSheetViewPage> createState() => _WhlSendGifSheetViewPageState();
}

class _WhlSendGifSheetViewPageState extends State<WhlSendGifSheetViewPage> {
  final logic = Get.put(WhlSendGifSheetViewLogic());

  @override
  void dispose() {
    Get.delete<WhlSendGifSheetViewLogic>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return UIColumn(
      onTap: () {
        Get.back();
      },
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        GetBuilder<WhlMineLogic>(builder: (logic) {
          return UIColumn(
            crossAxisAlignment: CrossAxisAlignment.end,
            width: 375.w,
            height: 260.h,
            padding: EdgeInsets.only(bottom: ScreenUtil().bottomBarHeight),
            color: Colors.white,
            mainAxisSize: MainAxisSize.min,
            topRightRadius: 12.w,
            topLeftRadius: 12.w,
            children: [
              UIRow(
                margin: EdgeInsets.only(top: 25.h, right: 20.w),
                mainAxisAlignment: MainAxisAlignment.end,
                mainAxisSize: MainAxisSize.min,
                gradientStartColor: kAppColor('#FFEDAF'),
                gradientEndColor: kAppColor('#FFEDAF'),
                gradientBegin: Alignment.topCenter,
                gradientEnd: Alignment.bottomCenter,
                padding: EdgeInsets.only(left: 7.w, right: 9.w, top: 4.w, bottom: 4.w),
                radius: 2.w,
                color: Colors.white.withOpacity(0.3),
                children: [
                  UIImage(
                    assetImage: 'icon_coin.png',
                    width: 24.w,
                  ),
                  UIText(
                    margin: EdgeInsets.only(left: 8.w, right: 6.w),
                    // text: logic.userInfoModel.availableCoins.toString(),
                    textColor: kAppTextColor,
                    fontSize: 16.sp,
                    fontWeight: FontWeight.bold,
                  ),
                  Icon(Icons.keyboard_arrow_right)
                ],
                onTap: () {
                  Get.toNamed(Routes.coinShop);
                },
              ),
              UIRow(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                margin: EdgeInsets.only(top: 42.h, left: 20.w, right: 20.w),
                children: this.logic.giftMapList.map((e) {
                  Map<String,dynamic> map = e;
                  return UIColumn(
                    children: [
                      UIImage(
                        assetImage: map.keys.first,
                        width: 86.w,
                      ),
                      UIText(
                        text: map.values.first,
                        textColor: kAppTextColor,
                        fontSize: 14.sp,
                        fontWeight: FontWeight.bold,
                        marginDrawable: 4.w,
                        startDrawable: UIImage(
                          assetImage: 'icon_coin.png',
                          width: 20.w,
                        ),
                      )
                    ],
                    onTap: () {
                      this.logic.doClickGiftItem(map);
                    },
                  );
                }).toList(),
              )
            ],
          );
        }),
      ],
    );
  }
}
