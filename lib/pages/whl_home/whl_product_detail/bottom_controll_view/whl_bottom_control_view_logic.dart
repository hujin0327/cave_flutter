import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'package:cave_flutter/whl_app.dart';
import 'package:cave_flutter/db_manager/bean/whl_product_bean.dart';
import 'package:cave_flutter/pages/whl_chat/whl_chat_room/record_sheet_view/whl_record_sheet_view.dart';
import 'package:cave_flutter/pages/whl_chat/whl_chat_room/whl_chat_room_logic.dart';
import 'package:cave_flutter/pages/whl_home/whl_product_detail/whl_product_detail_logic.dart';
import 'package:cave_flutter/pages/whl_home/whl_product_detail/whl_product_detail_view.dart';
import 'package:cave_flutter/pages/whl_home/whl_product_detail/whl_send_gif_sheet_view/whl_send_gif_sheet_view_view.dart';
import 'package:cave_flutter/utils/whl_db_utils.dart';

import 'package:cave_flutter/utils/whl_permission_util.dart';
import 'package:cave_flutter/utils/whl_rc_im_utils.dart';
import 'package:cave_flutter/utils/whl_record_voice_utils.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:cave_flutter/utils/whl_user_utils.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class WhlDetailBottomLogic extends GetxController {
  TextEditingController inputTextController = TextEditingController();
  FocusNode focusNode = FocusNode();
  bool isShowEmoji = false;
  bool isShowVoice = false;
  bool isShowMore = false;
  bool showKeyBoard = false;
  WhlProductDetailLogic logic = Get.find();

  doClickSend(String text) async {
    if (text.isEmpty) {
      MyToast.show('Send message cannot be empty');
      return;
    }
    inputTextController.clear();
    BotToast.showLoading();
    await Future.delayed(Duration(milliseconds: Random().nextInt(2000)));
    BotToast.closeAllLoading();
    logic.productBean.commentList ??= [];
    logic.productBean.commentList?.add(WhlCommentModel(
      userId: WhlUserUtils.getId(),
      nickname: WhlUserUtils.getNickName(),
      avatar: WhlUserUtils.getAvatar(),
      content: text,
      time: DateTime.now().millisecondsSinceEpoch,
    ));
    logic.update();
    WhlDBUtils.instance.productDao.updateProduct(logic.productBean);
    Future.delayed(Duration(milliseconds: 200)).then((value) {
      logic.onScrollToBottom();
    });
  }

  doClickGift() {
    Get.bottomSheet(WhlSendGifSheetViewPage(), isScrollControlled: true);
  }

  doClickMore() {
    isShowMore = !isShowMore;
    isShowVoice = false;
    hideKeyboard(Get.context);
    isShowEmoji = false;
    update();
  }

  doClickEmoji() {
    isShowEmoji = !isShowEmoji;
    isShowVoice = false;
    hideKeyboard(Get.context);
    isShowMore = false;
    update();
  }

  resetAllStatus() {
    isShowEmoji = false;
    isShowVoice = false;
    isShowMore = false;
    hideKeyboard(Get.context);
    update();
  }
}
