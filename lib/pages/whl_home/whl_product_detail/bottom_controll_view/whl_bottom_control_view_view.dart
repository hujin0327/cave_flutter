import 'dart:math';

import 'package:cave_flutter/pages/whl_chat/whl_chat_room/whl_chat_room_logic.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/whl_current_limiting_util.dart';
import 'package:cave_flutter/widgets/TextSpanField/special_text/whl_special_text_span_builder.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:extended_text_field/extended_text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'whl_bottom_control_view_logic.dart';

class WhlDetailBottomView extends StatefulWidget {
  WhlDetailBottomView({Key? key}) : super(key: key);

  @override
  State<WhlDetailBottomView> createState() => _WhlDetailBottomViewState();
}

class _WhlDetailBottomViewState extends State<WhlDetailBottomView> {
  late WhlDetailBottomLogic logic = Get.put(WhlDetailBottomLogic());

  @override
  void dispose() {
    // Get.delete<BottomControlViewLogic>();
    super.dispose();
    // Get.delete<BottomControlViewLogic>();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      child: UIRow(
        padding: EdgeInsets.symmetric(vertical: 13.h),
        decoration: BoxDecoration(color: Colors.white, border: Border(top: BorderSide(color: kAppColor('#E5E5E5')))),
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          UIImage(
            assetImage: 'icon_gift.png',
            width: 41.w,
            margin: EdgeInsets.only(left: 16.w),
            onTap: () {
              logic.doClickGift();
            },
          ),
          Expanded(
            child: UIContainer(
              color: kAppColor('#F2F2F2'),
              margin: EdgeInsets.only(right: 24.w, left: 20.w),
              padding: EdgeInsets.only(left: 12.w, right: 10.w),
              radius: 50.w,
              height: 36.h,
              child: buildInputWidget(context),
            ),
          ),
          UIText(
            padding: EdgeInsets.symmetric(horizontal: 12.w, vertical: 8.h),
            margin: EdgeInsets.only(right: 20.w),
            text: 'Send',
            textColor: Colors.white,
            fontSize: 16.sp,
            radius: 50.w,
            alignment: Alignment.center,
            gradientStartColor: kAppLeftColor,
            gradientEndColor: kAppRightColor,
            onTap: () {
              logic.doClickSend(logic.inputTextController.text);
            },
          )
        ],
      ),
    );
  }

  buildInputWidget(context) {
    return ExtendedTextField(
        controller: logic.inputTextController,
        textAlign: TextAlign.left,
        focusNode: logic.focusNode,
        textCapitalization: TextCapitalization.sentences,
        specialTextSpanBuilder: WhlSpecialTextSpanBuilder(
          showAtBackground: false,
        ),
        enabled: true,
        decoration: InputDecoration(
          hintText: 'Please enter message...',
          hintStyle: TextStyle(color: kAppColor('#CCCCCC'), fontSize: 10.sp),
          border: const OutlineInputBorder(borderSide: BorderSide.none),
          contentPadding: const EdgeInsets.all(0),
        ),
        style: TextStyle(fontSize: 15.sp, color: kAppTextColor),
        textInputAction: TextInputAction.send,
        // keyboardType: TextInputType.multiline,
        onSubmitted: (value) {
          // FocusScope.of(context).requestFocus(logic.focusNode);
          logic.doClickSend(value);
        },
        onTap: () {
          print('onTap');
          logic.logic.onScrollToBottom();
        });
  }
}
