import 'dart:math';

import 'package:cave_flutter/config/whl_base_controller.dart';
import 'package:cave_flutter/db_manager/bean/whl_product_bean.dart';
import 'package:cave_flutter/pages/whl_chat/whl_chat_logic.dart';
import 'package:cave_flutter/pages/whl_explore/whl_explore_logic.dart';
import 'package:cave_flutter/pages/whl_home/whl_home_logic.dart';
import 'package:cave_flutter/pages/whl_mine/whl_other_user_info/whl_other_user_info_logic.dart';
import 'package:cave_flutter/utils/whl_db_utils.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../routes/whl_app_pages.dart';

class WhlProductDetailLogic extends WhlBaseController with GetTickerProviderStateMixin {
  WhlProductBean productBean = Get.arguments;
  ScrollController scrollController = ScrollController();
  double giftAlpha = 0;
  bool giftVisible = false;
  AnimationController? animationController;
  Animation? animation;
  String? selectedGift;
  @override
  onReady() {
    super.onReady();
  }

  onScrollToBottom() {
    Future.delayed(Duration(milliseconds: 500)).then((value) {
      double minTo = min(280.h, scrollController.position.maxScrollExtent);
      scrollController.animateTo(minTo, duration: Duration(milliseconds: 300), curve: Curves.ease);
    });
  }

  doClickFollow() {
    WhlHomeLogic logic = Get.find();
    logic.doClickFollow(productBean);
    update();
  }

  doClickCancelFollow() {
    WhlHomeLogic logic = Get.find();
    logic.doClickCancelFollow(productBean);
    update();
  }

  doClickProductLike(WhlProductBean model) {
    WhlExploreLogic logic = Get.find();
    logic.doClickLike(model);
    productBean.isLike = model.isLike;
    update();
  }

  doClickCommentLike(WhlCommentModel model) {
    model.isLike = model.isLike == true ? false : true;
    update();
    WhlDBUtils.instance.productDao.updateProduct(productBean);
  }

  doClickMore() {
    onShowReportBottomSheet(productBean.userId ?? '');
  }

  doClickChat() {
    WhlChatLogic logic = Get.find();
  }

  doClickAvatar() {
    if (Get.isRegistered<WhlOtherUserInfoLogic>()) {
      WhlOtherUserInfoLogic logic = Get.find();
      logic.productBean = productBean;
      logic.update();
      Get.until((route) => Get.currentRoute == Routes.otherUserInfo);
    } else {
      Get.toNamed(Routes.otherUserInfo, arguments: productBean);
    }
  }

  onStartSendGiftAnimation() {
    giftVisible = true;
    giftAlpha = 0;
    update();
    animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 1500));
    animation = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(parent: animationController!, curve: Curves.easeInOut));
    animation!.addListener(() {
      giftAlpha = animation!.value;
      update();
      if (giftAlpha == 1) {
        Future.delayed(Duration(milliseconds: 500)).then((value) {
          animationController!.reverse();
          Future.delayed(Duration(milliseconds: 1500)).then((value) {
            giftVisible = false;
            giftAlpha = 0;
            update();
          });
        });
      }
    });
    animationController!.forward();
  }
}
