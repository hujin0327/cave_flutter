import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/routes/whl_app_pages.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import '../../model/base_model.dart';
import '../../model/user_model.dart';
import '../../style/whl_style.dart';
import '../../widgets/brick/widget/basic_widget.dart';
import '../../widgets/brick/widget/image_widget.dart';
import '../../widgets/brick/widget/text_widget.dart';

class UserRank {
  String? userId;
  String? avatar;
  String? nickName;
  String? value;
  String? accountId;
  UserRank(
      {this.userId, this.avatar, this.accountId, this.nickName, this.value});
  UserRank.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    avatar = json['avatar'];
    nickName = json['nickName'];
    value = json['value'];
    accountId = json['accountId'];
  }
}

class RankListWidget extends StatefulWidget {
  const RankListWidget({super.key});

  @override
  State<RankListWidget> createState() => _RankListWidgetState();
}

class _RankListWidgetState extends State<RankListWidget> {
  List<BaseModel> rankTypeList = [
    BaseModel(id: '1', title: '财富榜', content: "财富值"),
    BaseModel(id: '2', title: '魅力榜', content: "魅力值"),
    BaseModel(id: '3', title: '灵魂契约', content: "契约值"),
  ];
  int selectedRankTypeIndex = 0;

  List<UserRank> listData = [];

  @override
  void initState() {
    super.initState();
    loadData();
  }

  loadData() {
    WhlApi.homeRankg.get({"type": selectedRankTypeIndex + 1}).then((value) {
      if (value.isSuccess()) {
        listData = List<UserRank>.from((value.data ?? []).map((e) {
          return UserRank.fromJson(e);
        }).toList());
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return UIColumn(
      margin: EdgeInsets.symmetric(horizontal: 13.w).copyWith(bottom: 20.h),
      color: Colors.white,
      radius: 22.w,
      boxShadow: [
        BoxShadow(
          offset: const Offset(0, 0),
          color: kAppColor('#221A38').withOpacity(0.02),
          blurRadius: 30,
        )
      ],
      children: [
        UIRow(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          height: 50.w,
          children: rankTypeList.map((e) {
            int index = rankTypeList.indexOf(e);
            return UIColumn(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                UIText(
                  text: e.title,
                  textColor: selectedRankTypeIndex == index
                      ? kAppTextColor
                      : kAppTextColor.withOpacity(0.7),
                  fontSize: 16.sp,
                  fontWeight: selectedRankTypeIndex == index
                      ? FontWeight.bold
                      : FontWeight.normal,
                ),
                UIContainer(
                  margin: EdgeInsets.only(top: 5.w),
                  width: 12.w,
                  height: 3.w,
                  radius: 3.w,
                  color: selectedRankTypeIndex == index
                      ? kAppTextColor
                      : Colors.transparent,
                )
              ],
              onTap: () {
                setState(() {
                  listData.clear();
                  selectedRankTypeIndex = index;
                  loadData();
                });
              },
            );
          }).toList(),
        ),
        Expanded(
          child: CustomScrollView(
            slivers: [
              SliverList(
                  delegate: SliverChildBuilderDelegate((context, index) {
                UserRank model = listData[index];
                BaseModel baseModel = rankTypeList[selectedRankTypeIndex];
                return UIRow(
                  gradientStartColor: kAppColor('#FEFEFE').withOpacity(0.3),
                  gradientEndColor: kAppColor('#FEF2F2').withOpacity(0.3),
                  gradientBegin: Alignment.topCenter,
                  gradientEnd: Alignment.bottomCenter,
                  height: 88.w,
                  padding: EdgeInsets.symmetric(horizontal: 13.w),
                  children: [
                    if (index < 3)
                      UIImage(
                        margin: EdgeInsets.only(right: 9.w),
                        assetImage: 'icon_rank_$index.png',
                        width: 28.w,
                      )
                    else
                      UIText(
                        margin: EdgeInsets.only(left: 4.w, right: 16.w),
                        text: '${index + 1}.',
                        textColor: kAppColor('#666666'),
                        fontSize: 18.sp,
                        fontWeight: FontWeight.bold,
                      ),
                    if (selectedRankTypeIndex == 2)
                      Stack(
                        clipBehavior: Clip.none,
                        children: [
                          for (int i = 0; i < 2; i++)
                            UIImage(
                              disabledHttpBigImage: true,
                              onTap: () {},
                              httpImage: model.avatar?.toImageUrl(),
                              assetPlaceHolder: 'icon_app_logo.png',
                              margin: EdgeInsets.only(left: i * 20.w),
                              strokeColor: Colors.white,
                              strokeWidth: 1.w,
                              width: 42.w,
                              height: 42.w,
                              radius: 22.w,
                            )
                        ],
                      )
                    else
                      UIImage(
                        disabledHttpBigImage: true,
                        onTap: () {
                          Get.toNamed(Routes.userInfo, arguments: {
                            "accountId": model.accountId ?? model.userId,
                          });
                        },
                        httpImage: model.avatar?.toImageUrl(),
                        assetPlaceHolder: 'icon_app_logo.png',
                        width: 42.w,
                        height: 42.w,
                        radius: 22.w,
                      ),
                    Expanded(
                      child: UIText(
                        margin: EdgeInsets.only(left: 10.w),
                        text: model.nickName,
                        textColor: kAppTextColor,
                        fontSize: 14.sp,
                        alignment: Alignment.centerLeft,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    UIText(
                      text: '${baseModel.content}: ${model.value}',
                      textColor: kAppColor('#EF4848'),
                      fontSize: 11.sp,
                      fontWeight: FontWeight.w500,
                    )
                  ],
                );
              }, childCount: listData.length))
            ],
          ),
        )
      ],
    );
  }
}
