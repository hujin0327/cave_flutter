import 'package:cave_flutter/style/whl_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../widgets/brick/brick.dart';
import 'search_logic.dart';

class SearchPage extends StatelessWidget {
  SearchPage({Key? key}) : super(key: key);

  final logic = Get.put(SearchLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: UIColumn(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          UIRow(
            margin: EdgeInsets.only(
                top: ScreenUtil().statusBarHeight + 18.w,
                left: 18.w,
                right: 18.w),
            children: [
              Expanded(
                child: UIContainer(
                  height: 44.w,
                  radius: 30.w,
                  color: kAppColor('#F6F6F6'),
                  child: TextField(
                    onSubmitted: (key) {
                      logic.saveHistory(key);
                    },
                    decoration: InputDecoration(
                      // prefixIcon: Icon(Icons.search),
                      hintText: '搜索',
                      hintStyle: TextStyle(
                        fontSize: 13.sp,
                        color: kAppColor('#9C9C9C'),
                      ),
                      prefixIconConstraints: BoxConstraints(
                        // maxHeight: 20.h,
                        // maxWidth: 20.w,
                        minHeight: 20.h,
                        minWidth: 20.w,
                      ),
                      prefixIcon: UIImage(
                        margin: EdgeInsets.only(left: 18.w, right: 6.w),
                        assetImage: 'icon_search.png',
                        width: 18.w,
                        height: 18.w,
                      ),
                      border: const OutlineInputBorder(
                        borderSide: BorderSide.none,
                      ),
                      contentPadding: EdgeInsets.zero,
                    ),
                  ),
                ),
              ),
              UIText(
                margin: EdgeInsets.only(left: 10.w),
                text: '取消',
                textColor: kAppColor('#9C9C9C'),
                fontSize: 16.sp,
                onTap: () {
                  Get.back();
                },
              )
            ],
          ),
          UIRow(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            margin: EdgeInsets.only(top: 18.w, left: 18.w, right: 18.w),
            children: [
              UIText(
                text: '历史记录',
                textColor: kAppTextColor,
                fontSize: 14.sp,
                fontWeight: FontWeight.bold,
              ),
              UIImage(
                assetImage: 'icon_search_delete.png',
                width: 20.w,
                onTap: () {
                  logic.clearHistory();
                },
              )
            ],
          ),
          GetBuilder<SearchLogic>(builder: (logic) {
            return UIWrap(
              padding: EdgeInsets.symmetric(horizontal: 14.w),
              textDirection: TextDirection.ltr,
              crossAxisAlignment: WrapCrossAlignment.start,
              margin: EdgeInsets.only(top: 11.w),
              wrapAlignment: WrapAlignment.start,
              runSpacing: 11.w,
              spacing: 11.w,
              children: logic.list.map((String e) {
                return UIText(
                  onTap: () {
                    Get.back(result: e);
                  },
                  padding:
                      EdgeInsets.symmetric(horizontal: 12.w, vertical: 4.w),
                  text: e,
                  strokeWidth: 1.w,
                  strokeColor: kAppColor('#F6F6F6'),
                  textColor: kAppTextColor,
                  fontSize: 14.sp,
                  radius: 100.w,
                );
              }).toList(),
            );
          }),
        ],
      ),
    );
  }
}
