import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../common/common_widget.dart';
import '../../../../style/whl_style.dart';
import '../../../../widgets/brick/widget/basic_widget.dart';
import '../../../../widgets/brick/widget/text_widget.dart';

class PushTopEditDialog extends StatefulWidget {
  const PushTopEditDialog({super.key, required this.interval, required this.onTap});

  final String interval;
  final Function(String interval) onTap;

  @override
  State<PushTopEditDialog> createState() => _PushTopEditDialogState();
}

class _PushTopEditDialogState extends State<PushTopEditDialog> {
  TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    _controller.text = widget.interval;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(
        child: Wrap(
          children: [
            UIColumn(
              width: double.maxFinite,
              radius: 30.w,
              margin: EdgeInsets.symmetric(horizontal: 34.w),
              color: Colors.white,
              children: [
                UIText(
                  text: '自动推订间隔时长设置',
                  fontSize: 17.sp,
                  textColor: Colors.black,
                  fontWeight: FontWeight.bold,
                  margin: EdgeInsets.only(top: 20.h),
                ),
                Container(
                  margin: EdgeInsets.only(top: 31.h, left: 20.w, right: 20.w),
                  child: Row(
                    children: [
                      UIText(
                        text: '时间间隔',
                        textColor: Colors.black,
                        fontSize: 14.sp,
                        fontWeight: FontWeight.bold,
                      ),
                      Expanded(
                          child: UIContainer(
                        margin: EdgeInsets.only(left: 10.w),
                        padding: EdgeInsets.only(left: 10.w, right: 10.w),
                        height: 39.h,
                        radius: 7.w,
                        alignment: Alignment.centerLeft,
                        strokeWidth: 0.5.w,
                        strokeColor: kAppColor('#DADADA'),
                        child: noBorderCTextField(
                          controller: _controller,
                          keyboardType: TextInputType.number,
                          textAlign: TextAlign.left,
                          hint: '请输入',
                          textColor: Colors.black,
                          hintTextColor: kAppColor('#C1C1C1'),
                          fontSize: 14.sp,
                          hintFontWeight: FontWeight.normal,
                        ),
                      )),
                    ],
                  ),
                ),
                UIText(
                  onTap: () {
                    if (_controller.text.isNotEmpty) {
                      widget.onTap(_controller.text);
                      Get.back();
                    } else {
                      BotToast.showText(text: '请输入时间间隔');
                    }
                  },
                  width: double.infinity,
                  alignment: Alignment.center,
                  margin: EdgeInsets.only(
                      top: 30.h, left: 25.w, right: 25.w, bottom: 20.h),
                  height: 43.h,
                  text: '确定',
                  fontWeight: FontWeight.bold,
                  fontSize: 16.sp,
                  color: Colors.black,
                  textColor: Colors.white,
                  radius: 99.h,
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
