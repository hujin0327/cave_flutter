import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../common/common_widget.dart';
import '../../../../widgets/brick/widget/basic_widget.dart';
import '../../../../widgets/scale/scale_numberpicker_wrapper.dart';
import 'push_top_setting_logic.dart';

// ignore: must_be_immutable
class PushTopSettingSheet extends StatelessWidget {
  final String? id;
  final DynamicDataSource? dataSource;
  PushTopSettingSheet({super.key, this.id, this.dataSource});

  late PushTopSettingLogic logic;

  @override
  Widget build(BuildContext context) {
    logic = Get.put(PushTopSettingLogic(id: id, dataSource: dataSource));
    return GetBuilder<PushTopSettingLogic>(builder: (logic) {
      return UIContainer(
        padding:
            EdgeInsets.only(left: 17.w, right: 17.w, top: 16.w, bottom: 45.w),
        color: Colors.white,
        topRightRadius: 30.w,
        topLeftRadius: 30.w,
        child: ListView(
          shrinkWrap: true,
          children: [
            UIText(
              alignment: Alignment.center,
              text: '推顶卡',
              textColor: Colors.black,
              fontSize: 16.sp,
              fontWeight: FontWeight.bold,
            ),
            UIText(
              margin: EdgeInsets.only(top: 5.h),
              alignment: Alignment.center,
              text: '在热门、同城、话题、我的关注中推顶',
              textColor: kAppColor('#B7B7B7'),
              fontSize: 13.sp,
            ),
            _countWidget(),
            // SizedBox(
            //   height: 31.h,
            // ),
            // UIRow(
            //   mainAxisAlignment: MainAxisAlignment.center,
            //   children: [
            //     UIText(
            //       text: '自动推订间隔时长',
            //       textColor: Colors.black,
            //       fontSize: 14.sp,
            //       fontWeight: FontWeight.bold,
            //     ),
            //     UIText(
            //       text: '（分钟）',
            //       textColor: kAppColor('#9E9E9E'),
            //       fontSize: 10.sp,
            //     ),
            //   ],
            // ),
            // _intervalSetting(context),
            if (logic.cardCount > 0)
              UIText(
                onTap: logic.onCommit,
                width: double.infinity,
                alignment: Alignment.center,
                margin: EdgeInsets.only(top: 20.h),
                height: 53.h,
                text: '使用1张推顶卡',
                fontWeight: FontWeight.bold,
                fontSize: 16.sp,
                color: Colors.black,
                textColor: Colors.white,
                radius: 99.h,
              )
            // else
            //   UIText(
            //     onTap: (){},
            //     width: double.infinity,
            //     alignment: Alignment.center,
            //     margin: EdgeInsets.only(top: 20.h),
            //     height: 53.h,
            //     text: '获取推顶卡',
            //     fontWeight: FontWeight.bold,
            //     fontSize: 16.sp,
            //     color: Colors.black,
            //     textColor: Colors.white,
            //     radius: 99.h,
            //   )
          ],
        ),
      );
    });
  }

  ///设置使用数量
  _countWidget() {
    return Container(
      margin: EdgeInsets.only(top: 31.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          if (logic.cardCount > 0)
            UIText(
              text: '您还有${logic.cardCount}张推顶卡',
              textColor: Colors.black,
              fontSize: 14.sp,
              fontWeight: FontWeight.bold,
            )
          else
            UIText(
              text: '您没有推顶卡',
              textColor: Colors.black,
              fontSize: 14.sp,
              fontWeight: FontWeight.bold,
            ),
          // Expanded(
          //   child: UIContainer(
          //     margin: EdgeInsets.only(left: 10.w),
          //     padding: EdgeInsets.only(left: 10.w, right: 10.w),
          //     height: 39.h,
          //     radius: 7.w,
          //     alignment: Alignment.centerLeft,
          //     strokeWidth: 0.5.w,
          //     strokeColor: kAppColor('#DADADA'),
          //     child: noBorderCTextField(
          //       controller: logic.countController,
          //       textAlign: TextAlign.left,
          //       keyboardType: TextInputType.number,
          //       hint: '请输入',
          //       textColor: Colors.black,
          //       hintTextColor: kAppColor('#C1C1C1'),
          //       fontSize: 14.sp,
          //       hintFontWeight: FontWeight.normal,
          //     ),
          //   ),
          // ),
          // UIText(
          //   text: '（剩余${logic.cardCount}张）',
          //   textColor: kAppColor('#FF0000'),
          //   fontSize: 10.sp,
          // ),
        ],
      ),
    );
  }

  ///间隔时长设置
  _intervalSetting(BuildContext context) {
    return UIColumn(
      margin: EdgeInsets.only(top: 13.h),
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        GetBuilder<PushTopSettingLogic>(
            id: PushTopSettingLogic.intervalTitleId,
            builder: (s) {
              return UIRow(
                onTap: logic.onTapInterval,
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  UIText(
                    text: logic.interval,
                    textColor: Colors.black,
                    fontSize: 32.sp,
                    fontWeight: FontWeight.bold,
                  ),
                  SizedBox(
                    width: 4.w,
                  ),
                  UIImage(
                    margin: EdgeInsets.only(bottom: 8.h),
                    assetImage: 'ic_edit_icon.png',
                    width: 10.w,
                  )
                ],
              );
            }),
        SizedBox(
          height: 15.h,
        ),
        GetBuilder<PushTopSettingLogic>(builder: (s) {
          return ScaleNumberPickerWrapper(
            key: logic.pickerKey,
            initialValue: double.parse(logic.interval).toInt() *
                PushTopSettingLogic.intervalScale,
            minValue: 1 * PushTopSettingLogic.intervalScale,
            maxValue: 10 * PushTopSettingLogic.intervalScale,
            step: 1,
            widgetWidth: MediaQuery.of(context).size.width.round() - 30,
            subGridCountPerGrid: 10,
            subGridWidth: 10,
            onSelectedChanged: (value) {
              s.onUpdateIntervalTitle(
                  (value / PushTopSettingLogic.intervalScale).toString());
            },
            scaleTransformer: (value) {
              return formatIntegerStr(
                  value / PushTopSettingLogic.intervalScale);
            },
            numberPickerHeight: 90.h.toInt(),
          );
        })
      ],
    );
  }
}

///去掉整数后的小数点和0
///1.0 -> "1"
///1.2 -> "1.2"
String formatIntegerStr(num number) {
  int intNumber = number.truncate();

  //是整数
  if (intNumber == number) {
    return intNumber.toString();
  } else {
    return number.toString();
  }
}
