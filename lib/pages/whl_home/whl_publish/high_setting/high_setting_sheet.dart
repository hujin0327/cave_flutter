import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_switch_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'high_setting_logic.dart';
import 'package:cave_flutter/pages/whl_home/whl_publish/whl_publish_logic.dart';

class HighSettingSheet extends StatelessWidget {
  HighSettingSheet({Key? key}) : super(key: key);

  final logic = Get.put(HighSettingLogic());
  final publishLogic = Get.put(WhlPublishLogic());

  @override
  Widget build(BuildContext context) {
    return UIContainer(
      padding:
          EdgeInsets.only(left: 24.w, right: 13.w, top: 31.w, bottom: 30.w),
      color: Colors.white,
      topRightRadius: 30.w,
      topLeftRadius: 30.w,
      child: ListView(
        children: getAllRowView(),
        shrinkWrap: true,
      ),
    );
  }

  List<Widget> getAllRowView() {
    List<Widget> items = [];
    final pushLogic = Get.find<WhlPublishLogic>();
    for (Map<String, dynamic> element in highSettingData) {
      int type = element["type"] ?? 0;
      String title = element["name"] ?? "";
      int vip = element["vip"] ?? 0;
      String key = element["key"] ?? "";
      if (type == 1) {
        items.add(UIRow(
          onTap: () {
            logic.onClickKey(element).then((res) {
              if (res != null) {
                publishLogic.topCardNum = res['num'];
              }
            });
          },
          margin: EdgeInsets.only(bottom: 20.w),
          children: [
            UIText(
              text: title,
              textColor: kAppColor('#2D2D2D'),
              fontSize: 14.sp,
              fontWeight: FontWeight.bold,
            ),
            Expanded(child: Container()),
            Icon(Icons.keyboard_arrow_right, color: kAppSub2TextColor),
          ],
        ));
      } else {
        items.add(UIRow(
          margin: EdgeInsets.only(bottom: 20.w),
          children: [
            UIText(
              text: title,
              textColor: kAppColor('#2D2D2D'),
              fontSize: 14.sp,
              fontWeight: FontWeight.bold,
              marginDrawable: 7.w,
              endDrawable: vip > 0
                  ? UIImage(
                      assetImage: 'icon_vip1.png',
                      height: 15.w,
                    )
                  : null,
            ),
            Expanded(child: Container()),
            WhlSwitch(
              openColor: Colors.white,
              bgColor: kAppColor('#EDEDED'),
              color: Colors.white,
              openBgColor: kAppThemeColor,
              // inactiveTrackColor: Color.fromRGBO(237, 237, 237, 1),
              value: pushLogic.highSetterData[key] == 1 ? true : false,
              width: 50.w,
              height: 26.w,
              onChanged: (val) {
                if (key.isNotEmpty) {
                  pushLogic.highSetterData[key] = val ? 1 : 0;
                }
              },
            )
          ],
        ));
      }
    }
    return items;
  }
}
