import 'package:get/get.dart';
import '../push_top_setting/push_top_setting_sheet.dart';

List<Map<String, dynamic>> highSettingData = [
  {
    "id": "59c6a127b93611ee83cf000c292789e3",
    "name": "广场推顶(消耗1张置顶卡)",
    "vip": 0,
    "key": "pushTop"
  },
  {
    "id": "6f745058b93611ee83cf000c292789e3",
    "name": "是否推荐",
    "vip": 2,
    "key": "recommend"
  },
  {
    "id": "79fb4ea5b93611ee83cf000c292789e3",
    "name": "定时发送",
    "vip": 2,
    "key": "send"
  },
  {
    "id": "86c38c69b93611ee83cf000c292789e3",
    "name": "不允许匿名评论",
    "vip": 2,
    "key": "notAnonymousComment"
  },
  {
    "id": "9784ce4cb93611ee83cf000c292789e3",
    "name": "评论隐藏",
    "vip": 1,
    "key": "hideComment"
  },
  {
    "id": "bd00e6cdb93611ee83cf000c292789e3",
    "name": "禁止评论",
    "vip": 1,
    "key": "notComment"
  },
  {
    "id": "c3ffb3c6b93611ee83cf000c292789e3",
    "name": "禁止下载",
    "vip": 1,
    "key": "notDownload"
  },
  {
    "id": "cc10b0e2b93611ee83cf000c292789e3",
    "name": "禁止转发",
    "vip": 1,
    "key": "notForward"
  },
  {
    "id": "d581b7beb93611ee83cf000c292789e3",
    "name": "添加水印",
    "vip": 0,
    "key": "watermark"
  }
];

class HighSettingLogic extends GetxController {
  ///点击事件（目前type = 1 的时候）
  onClickKey(Map element) {
    Get.back();
    String key = element['key'];
    if (key == 'pushTop') {
      return Get.bottomSheet(PushTopSettingSheet(), isScrollControlled: true);
    }
  }
}
