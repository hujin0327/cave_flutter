import 'package:amap_flutter_map/amap_flutter_map.dart';
import 'package:amap_flutter_base/amap_flutter_base.dart';
import 'package:amap_search_fluttify/amap_search_fluttify.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/search_widget.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../common/map_const_config.dart';
import 'map_search_logic.dart';

class Map_searchPage extends StatefulWidget {

  String latitude = '';
  String longitude = '';
  Map_searchPage({Key? key, required this.latitude, required this.longitude}) : super(key: key);

  @override
  State<Map_searchPage> createState() => _MapSearchPageState();
}

class _MapSearchPageState extends State<Map_searchPage> {

  final logic = Get.put(Map_searchLogic());

  @override
  void initState() {
    super.initState();
    logic.latLng = LatLng(double.parse(widget.latitude), double.parse(widget.longitude));
  }

  // 输入位置搜索
  void doSearch(String keyword) async {
    // 执行搜索
    print('用户输入的内容是：$keyword');
    final List<Poi> poiList = await logic.doSearch(keyword);
    // 处理搜索结果
    setState(() {
      logic.poiList = poiList;
    });
  }

  void onItemTapped(Poi poi) {
    print('选择了新的地点POI：${poi}');
    setState(() {
      logic.poiItemClicked(poi);
      logic.mapController.moveCamera(CameraUpdate.newLatLng(logic.latLng));
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: kAppWhiteColor,
      appBar: WhlAppBar(
        backgroundColor: kAppWhiteColor,
        centerTitle: "城市漫游",
        rightWidget: UIText(
          margin: EdgeInsets.only(right: 18.w),
          text: "确定",
          alignment: Alignment.center,
          color: kAppBlackColor,
          textColor: kAppWhiteColor,
          height: 30.w,
          width: 64.w,
          radius: 15.w,
          onTap: () {
            logic.saveNewPoi(callback: (isSuccess) {
              if (isSuccess) {
                Navigator.pop(context, logic.poi.title.toString());
              }
            });
          },
        ),
      ),
      body: UIStack(
        alignment: Alignment.topCenter,
        children: [
          UIContainer(
            height: ScreenUtil().screenHeight/2.0-ScreenUtil().statusBarHeight,
            child: AMapWidget(
              apiKey: MapConstConfig.amapApiKeys,
              privacyStatement: MapConstConfig.amapPrivacyStatement,
              initialCameraPosition: CameraPosition(target: logic.latLng, zoom: 13),
              myLocationStyleOptions: MyLocationStyleOptions(true),
              onMapCreated: logic.onMapCreated,
            ),
          ),
          UIColumn(
            margin: EdgeInsets.only(top: ScreenUtil().screenHeight/2.0-40.w-ScreenUtil().statusBarHeight),
            topLeftRadius: 30.w,
            topRightRadius: 30.w,
            color: kAppWhiteColor,
            children: [
              UIContainer(margin: EdgeInsets.only(left: 15.w,right: 15.w,top: 19.w,bottom: 5.w),child: SearchWidget(logic.editingController,bgColor: kAppColor("#F6F6F6"),
                searchCallback: (value) {
                  doSearch(value);
                },),),
              Expanded(child: ListView.builder(itemBuilder: (context,index) {
                Poi poi = logic.poiList[index];
                return UIColumn(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  padding: EdgeInsets.only(top: 14.h,left: 14.w,right: 14.w),
                  children: [
                    UIText(
                      margin: EdgeInsets.only(bottom: 6.h),
                      text: poi.title,
                      fontSize: 15.sp,
                      textColor: kAppBlackColor,
                      onTap: () {
                        onItemTapped(poi);
                      },
                    ),
                    UIText(
                      text: "${poi.distance}km | ${poi.address}",
                      fontSize: 12.sp,
                      textColor: kAppColor("#9C9C9C"),
                      onTap: () {
                        onItemTapped(poi);
                      },
                    ),
                    UIContainer(
                      margin: EdgeInsets.only(top: 14.h),
                      color: kAppStrokeColor,
                      height: 1.h,
                    )
                  ],
                );
              },itemCount: logic.poiList.length,)),
            ],
          )
        ],
      ),
    );
  }
}
