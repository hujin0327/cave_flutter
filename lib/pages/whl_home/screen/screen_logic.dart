import 'package:bot_toast/bot_toast.dart';
import 'package:cave_flutter/network/whl_api.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../whl_login/complete_user_info/selected_sex/selected_sex_logic.dart';

class ScreenLogic extends GetxController {
  List<SexModel> sexList = [
    SexModel(
        value: 0,
        defaultIcon: 'icon_sex_man_0.png',
        selectedIcon: 'icon_sex_man_1.png',
        name: 'Male'),
    SexModel(
        value: 1,
        defaultIcon: 'icon_sex_woman_0.png',
        selectedIcon: 'icon_sex_woman_1.png',
        name: 'Female'),
    SexModel(
        defaultIcon: 'icon_sex_lgbtq_0.png',
        selectedIcon: 'icon_sex_lgbtq_1.png',
        name: 'LGBTQ',
        value: 2),
  ];
  int selectedSexIndex = 0;
  RangeValues ageRange = const RangeValues(18, 24);
  List<SexModel> firstSexList = [
    SexModel(name: '不限', describe: ''),
  ];

  List<SexModel> secondSexList = [
    SexModel(name: '不限', describe: ''),
  ];

  List<SexModel> thirdSexList = [
    SexModel(name: '不限', describe: ''),
  ];
  List<SexModel> forthSexList = [
    SexModel(name: '不限', describe: ''),
  ];
  List<SexModel> fiveSexList = [
    SexModel(name: '不限', describe: ''),
  ];

  Map<String, dynamic> nearbyQueryParams = {};
  SexModel? soulFirst;
  SexModel? soulSec;
  SexModel? soulThree;
  @override
  void onInit() {
    super.onInit();
    getAll();
    var argument = Get.arguments;
    if (argument != null && argument['nearbyQueryParams'] != null) {
      nearbyQueryParams = argument['nearbyQueryParams'];
    }
  }

  submit() {
    if (soulThree != null &&
        soulThree!.value != null &&
        soulThree!.value != '') {
      nearbyQueryParams['souls'] = soulThree!.value;
    }
    nearbyQueryParams['startAge'] = ageRange.start.toInt();
    nearbyQueryParams['endAge'] = ageRange.end.toInt();

    Get.back(result: nearbyQueryParams);
  }

  getAll() {
    BotToast.showLoading();
    Future.wait([
      WhlApi.getSexQXList.get({}),
      WhlApi.getQQJSList.get({}),
      WhlApi.getMySoul.get({})
    ]).then((value) {
      if (value[0].isSuccess()) {
        //  如果有选中项，就不选不限
        bool defaultSelected = true;
        firstSexList = List<SexModel>.from(
          value[0].data.map(
            (c) {
              bool selected = c['id'] == nearbyQueryParams['sexualOrientation'];
              if (defaultSelected && selected) {
                defaultSelected = false;
              }
              return SexModel(
                value: c['id'] ?? '',
                name: c['lable'] ?? '',
                describe: c['note'] ?? '',
                isSelected: selected,
              );
            },
          ),
        ).toList();
        firstSexList.insert(
          0,
          SexModel(value: '', name: '不限', isSelected: defaultSelected),
        );
      }
      if (value[1].isSuccess()) {
        //  如果有选中项，就不选不限
        bool defaultSelected = true;
        secondSexList = List<SexModel>.from(
          value[1].data.map(
            (c) {
              bool selected = c['id'] == nearbyQueryParams['funRole'];
              if (defaultSelected && selected) {
                defaultSelected = false;
              }
              return SexModel(
                value: c['id'] ?? '',
                name: c['lable'] ?? '',
                describe: c['note'] ?? '',
                isSelected: selected,
              );
            },
          ),
        ).toList();
        secondSexList.insert(
          0,
          SexModel(
            value: '',
            name: '不限',
            isSelected: defaultSelected,
          ),
        );
      }
      if (value[2].isSuccess()) {
        try {
          thirdSexList = [SexModel(value: '', name: '不限', isSelected: true)] +
              List<SexModel>.from(
                value[2].data.map(
                      (c) => SexModel(
                        value: c['id'] ?? '',
                        name: c['lable'] ?? '',
                        describe: c['note'] ?? '',
                        children: [
                              SexModel(value: '', name: '不限', isSelected: true)
                            ] +
                            List<SexModel>.from(
                              c['child']?.map(
                                (child) => SexModel(
                                  value: child['id'] ?? '',
                                  name: child['lable'] ?? '',
                                  describe: child['note'] ?? '',
                                  children: [
                                        SexModel(
                                          value: '',
                                          name: '不限',
                                          isSelected: true,
                                        )
                                      ] +
                                      List<SexModel>.from(
                                        child['child']?.map(
                                          (cchild) => SexModel(
                                            value: cchild['id'] ?? '',
                                            name: cchild['lable'] ?? '',
                                            describe: cchild['note'] ?? '',
                                          ),
                                        ),
                                      ).toList(),
                                ),
                              ),
                            ).toList(),
                      ),
                    ),
              ).toList();
        } catch (errpr) {}
      }
      BotToast.closeAllLoading();
      update();
    }, onError: (error) {
      BotToast.closeAllLoading();
    });
  }

  doClickGender(SexModel model) {
    if (model.value == nearbyQueryParams['gender']) {
      nearbyQueryParams['gender'] = null;
    } else {
      nearbyQueryParams['gender'] = model.value;
    }
    update();
  }

  doClickFirstItem(SexModel model) {
    nearbyQueryParams['sexualOrientation'] = model.value;
    update();
  }

  doClickSecondItem(SexModel model) {
    nearbyQueryParams['funRole'] = model.value;
    update();
  }

  doClickThirdItem(SexModel model) {
    soulFirst = model;
    update();
  }

  doClickForthItem(SexModel model) {
    soulSec = model;
    update();
  }

  doClickFiveItem(SexModel model) {
    soulThree = model;
    update();
  }
}
