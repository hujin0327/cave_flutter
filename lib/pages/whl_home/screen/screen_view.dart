import 'package:cave_flutter/pages/whl_login/complete_user_info/selected_sex/selected_sex_logic.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../widgets/brick/brick.dart';
import 'screen_logic.dart';

class ScreenPage extends StatelessWidget {
  ScreenPage({Key? key}) : super(key: key);

  final logic = Get.put(ScreenLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: UIColumn(
          margin: EdgeInsets.only(
            top: ScreenUtil().statusBarHeight + 12.h,
            bottom: 120.h,
          ),
          children: [
            Row(children: [
              UIContainer(
                padding: EdgeInsets.only(left: 20.w, right: 20.w),
                child: Icon(Icons.close, size: 20.w),
                onTap: () {
                  Get.back();
                },
              ),
              Expanded(
                child: UIText(
                  text: '匹配偏好',
                  textColor: kAppTextColor,
                  alignment: Alignment.center,
                  fontSize: 16.sp,
                  fontWeight: FontWeight.bold,
                ),
              ),
              UIContainer(
                  padding: EdgeInsets.only(left: 20.w, right: 20.w),
                  child: Icon(
                    Icons.close,
                    size: 20.w,
                    color: Colors.white,
                  )),
            ]),
            UIText(
              margin: EdgeInsets.only(left: 20.w, top: 28.w),
              alignment: Alignment.centerLeft,
              text: '想认识的性别',
              textColor: kAppTextColor,
              fontSize: 13.sp,
            ),
            UIRow(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              margin: EdgeInsets.symmetric(horizontal: 22.w, vertical: 17.w),
              children: logic.sexList.map((SexModel e) {
                return UIStack(
                  children: [
                    UIColumn(
                      mainAxisAlignment: MainAxisAlignment.center,
                      width: 85.w,
                      height: 75.w,
                      radius: 15.w,
                      strokeColor: kAppTextColor,
                      strokeWidth: 1.w,
                      children: [
                        UIText(
                          text: e.name,
                          textColor: kAppTextColor,
                          fontSize: 16.sp,
                          fontWeight: FontWeight.bold,
                        ),
                        UIImage(
                          margin: EdgeInsets.only(top: 3.w),
                          assetImage: e.selectedIcon,
                          width: 30.w,
                        )
                      ],
                    ),
                    GetBuilder<ScreenLogic>(builder: (logic) {
                      return Visibility(
                        visible: (logic.nearbyQueryParams['gender'] != e.value),
                        child: UIContainer(
                          width: 85.w,
                          height: 75.w,
                          radius: 15.w,
                          color: Colors.white.withOpacity(0.8),
                        ),
                      );
                    })
                  ],
                  onTap: () {
                    logic.doClickGender(e);
                  },
                );
              }).toList(),
            ),
            UIText(
              margin: EdgeInsets.only(left: 20.w, top: 11.w),
              alignment: Alignment.centerLeft,
              text: '想认识的年龄',
              textColor: kAppTextColor,
              fontSize: 13.sp,
            ),
            GetBuilder<ScreenLogic>(builder: (logic) {
              return Column(
                children: [
                  Stack(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 10.w, right: 10.w),
                        child: SliderTheme(
                          data: SliderThemeData(
                            activeTrackColor: kAppBlackColor,
                            activeTickMarkColor: kAppBlackColor,
                            rangeThumbShape: IndicatorRangeSliderThumbShape(
                              logic.ageRange.start.toInt(),
                              logic.ageRange.end.toInt(),
                            ),
                          ),
                          child: RangeSlider(
                            values: logic.ageRange,
                            min: 18,
                            max: 100,
                            activeColor: kAppBlackColor,
                            inactiveColor: Colors.grey.withOpacity(0.1),
                            onChanged: (value) {
                              logic.ageRange = value;
                              logic.update();
                            },
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              );
            }),
            UIText(
              margin: EdgeInsets.only(left: 20.w, top: 28.w),
              alignment: Alignment.centerLeft,
              text: '想认识性取向',
              textColor: kAppTextColor,
              fontSize: 13.sp,
            ),
            Container(
              margin: EdgeInsets.only(left: 23.w, top: 28.h),
              height: 32.h,
              alignment: Alignment.centerLeft,
              child: GetBuilder<ScreenLogic>(builder: (logic) {
                return ListView.builder(
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    SexModel model = logic.firstSexList[index];
                    bool selected = model.value ==
                        (logic.nearbyQueryParams['sexualOrientation'] ?? '');
                    return UIText(
                      padding:
                          EdgeInsets.symmetric(horizontal: 19.w, vertical: 7.h),
                      text: model.name,
                      fontSize: 13.sp,
                      textColor: selected ? Colors.white : kAppTextColor,
                      color: selected ? Colors.black : Colors.white,
                      margin: EdgeInsets.only(right: 24.w),
                      radius: 66.w,
                      strokeWidth: 1.w,
                      strokeColor:
                          selected ? kAppTextColor : kAppColor('#D9D9D9'),
                      onTap: () {
                        logic.doClickFirstItem(model);
                      },
                    );
                  },
                  itemCount: logic.firstSexList.length,
                );
              }),
            ),
            UIText(
              margin: EdgeInsets.only(left: 20.w, top: 28.w),
              alignment: Alignment.centerLeft,
              text: '想认识情趣角色',
              textColor: kAppTextColor,
              fontSize: 13.sp,
            ),
            Container(
              margin: EdgeInsets.only(left: 23.w, top: 28.h),
              height: 32.h,
              alignment: Alignment.centerLeft,
              child: GetBuilder<ScreenLogic>(builder: (logic) {
                return ListView.builder(
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    SexModel model = logic.secondSexList[index];
                    bool isSelected = model.value ==
                        (logic.nearbyQueryParams['funRole'] ?? '');
                    return UIText(
                      padding:
                          EdgeInsets.symmetric(horizontal: 19.w, vertical: 7.h),
                      text: model.name,
                      fontSize: 13.sp,
                      textColor: isSelected ? Colors.white : kAppTextColor,
                      color: isSelected ? Colors.black : Colors.white,
                      margin: EdgeInsets.only(right: 24.w),
                      radius: 66.w,
                      strokeWidth: 1.w,
                      strokeColor:
                          isSelected ? kAppTextColor : kAppColor('#D9D9D9'),
                      onTap: () {
                        logic.doClickSecondItem(model);
                      },
                    );
                  },
                  itemCount: logic.secondSexList.length,
                );
              }),
            ),
            UIText(
              margin: EdgeInsets.only(left: 20.w, top: 28.w),
              alignment: Alignment.centerLeft,
              text: '灵魂标签',
              textColor: kAppTextColor,
              fontSize: 13.sp,
            ),
            Container(
              margin: EdgeInsets.only(left: 23.w, top: 28.h),
              height: 32.h,
              alignment: Alignment.centerLeft,
              child: GetBuilder<ScreenLogic>(builder: (logic) {
                return ListView.builder(
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    SexModel model = logic.thirdSexList[index];
                    bool isSelected = model.value == logic.soulFirst?.value ||
                        (model.value == '' && logic.soulFirst == null);
                    return UIText(
                      padding:
                          EdgeInsets.symmetric(horizontal: 19.w, vertical: 7.h),
                      text: model.name,
                      fontSize: 13.sp,
                      textColor: isSelected ? Colors.white : kAppTextColor,
                      color: isSelected ? Colors.black : Colors.white,
                      margin: EdgeInsets.only(right: 24.w),
                      radius: 66.w,
                      strokeWidth: 1.w,
                      strokeColor:
                          isSelected ? kAppTextColor : kAppColor('#D9D9D9'),
                      onTap: () {
                        logic.doClickThirdItem(model);
                      },
                    );
                  },
                  itemCount: logic.thirdSexList.length,
                );
              }),
            ),
            GetBuilder<ScreenLogic>(builder: (logic) {
              if (logic.soulFirst != null && logic.soulFirst!.value != '') {
                List<SexModel> children = logic.soulFirst?.children ?? [];
                return Container(
                  key: Key('source_${logic.soulFirst?.value}'),
                  margin: EdgeInsets.only(left: 23.w, top: 28.h),
                  height: 32.h,
                  alignment: Alignment.centerLeft,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      SexModel model = children[index];
                      bool isSelected = model.value == logic.soulSec?.value ||
                          (model.value == '' && logic.soulSec == null);
                      return UIText(
                        padding: EdgeInsets.symmetric(
                          horizontal: 19.w,
                          vertical: 7.h,
                        ),
                        text: model.name,
                        fontSize: 13.sp,
                        textColor: isSelected ? Colors.white : kAppTextColor,
                        color: isSelected ? Colors.black : Colors.white,
                        margin: EdgeInsets.only(right: 24.w),
                        radius: 66.w,
                        strokeWidth: 1.w,
                        strokeColor:
                            isSelected ? kAppTextColor : kAppColor('#D9D9D9'),
                        onTap: () {
                          logic.doClickForthItem(model);
                        },
                      );
                    },
                    itemCount: children.length,
                  ),
                );
              }
              return const SizedBox();
            }),
            GetBuilder<ScreenLogic>(builder: (logic) {
              if (logic.soulSec != null && logic.soulSec!.value != '') {
                List<SexModel> children = logic.soulSec?.children ?? [];
                return Container(
                  key: Key('source_${logic.soulSec?.value}'),
                  margin: EdgeInsets.only(left: 23.w, top: 28.h),
                  height: 32.h,
                  alignment: Alignment.centerLeft,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      SexModel model = children[index];
                      bool isSelected = model.value == logic.soulThree?.value ||
                          (model.value == '' && logic.soulThree == null);
                      return UIText(
                        padding: EdgeInsets.symmetric(
                            horizontal: 19.w, vertical: 7.h),
                        text: model.name,
                        fontSize: 13.sp,
                        textColor: isSelected ? Colors.white : kAppTextColor,
                        color: isSelected ? Colors.black : Colors.white,
                        margin: EdgeInsets.only(right: 24.w),
                        radius: 66.w,
                        strokeWidth: 1.w,
                        strokeColor:
                            isSelected ? kAppTextColor : kAppColor('#D9D9D9'),
                        onTap: () {
                          logic.doClickFiveItem(model);
                        },
                      );
                    },
                    itemCount: children.length,
                  ),
                );
              }
              return const SizedBox();
            }),
          ],
        ),
      ),
      bottomSheet: UISolidButton(
        margin: EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.w),
        text: '完成',
        color: Colors.black,
        onTap: () {
          logic.submit();
        },
      ),
    );
  }
}

class IndicatorRangeSliderThumbShape<T> extends RangeSliderThumbShape {
  IndicatorRangeSliderThumbShape(this.start, this.end);

  @override
  Size getPreferredSize(bool isEnabled, bool isDiscrete) {
    // 这里根据设计图或者代码自行计算
    return const Size(15, 40);
  }

  T start;
  T end;
  TextPainter labelTextPainter = TextPainter()
    ..textDirection = TextDirection.ltr;

  @override
  void paint(
    PaintingContext context,
    Offset center, {
    required Animation<double> activationAnimation,
    required Animation<double> enableAnimation,
    bool? isDiscrete,
    bool? isEnabled,
    bool? isOnTop,
    TextDirection? textDirection,
    required SliderThemeData sliderTheme,
    Thumb? thumb,
    bool? isPressed,
  }) {
    final Canvas canvas = context.canvas;
    final Paint strokePaint = Paint()
      ..color = sliderTheme.thumbColor ?? Colors.yellow
      ..strokeWidth = 3.0
      ..style = PaintingStyle.stroke;
    canvas.drawCircle(center, 7.5, Paint()..color = Colors.black);
    canvas.drawCircle(center, 7.5, strokePaint);
    if (thumb == null) {
      return;
    }
    // 以下就是在thumb下添加一个自定义labels
    final value = thumb == Thumb.start ? start : end;
    labelTextPainter.text = TextSpan(
      text: value.toString(),
      style: const TextStyle(fontSize: 14, color: Colors.black),
    );
    labelTextPainter.layout();
    labelTextPainter.paint(
      canvas,
      center.translate(
        -labelTextPainter.width / 2,
        labelTextPainter.height / 2,
      ),
    );
  }
}
