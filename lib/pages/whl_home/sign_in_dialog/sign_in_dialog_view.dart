import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'sign_in_dialog_logic.dart';

class SignInDialogView extends StatefulWidget {
  const SignInDialogView({Key? key}) : super(key: key);

  @override
  State<SignInDialogView> createState() => _SignInDialogViewState();
}

class _SignInDialogViewState extends State<SignInDialogView> {
  final logic = Get.put(SignInDialogLogic());

  @override
  void dispose() {
    Get.delete<SignInDialogLogic>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: UIColumn(
        color: Colors.black.withOpacity(0.3),
        padding: EdgeInsets.only(top: 122.h,left: 13.w,right: 13.w),
        children: [
          Stack(children: [
            UIImage(
              assetImage: 'img_bg_signIn.png',
              width: 350.w,
              height: 390.w,
              fit: BoxFit.fill,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                UIText(
                  margin: EdgeInsets.only(top: 20.w, left: 15.w),
                  text: '每日签到',
                  textColor: kAppColor('#080B16'),
                  fontSize: 36.sp,
                  fontWeight: FontWeight.bold,
                  marginDrawable: 10.w,
                  endDrawable: UIImage(
                    assetImage: 'icon_signIn_complete.png',
                    width: 30.w,
                    height: 30.w,
                  )
                ),
                UIText(
                  margin: EdgeInsets.only(left: 16.w,top: 3.w),
                  text: '已连续签到1天',
                  textColor: Colors.white,
                  fontSize: 13.sp,
                ),
                UIContainer(
                  margin: EdgeInsets.symmetric(horizontal: 9.w),
                  color: Colors.white,
                  radius: 8.w,
                  child: UIWrap(
                    margin: EdgeInsets.only(left: 15.w, right: 15.w, top: 11.h, bottom: 11.h),
                    spacing: 5.w,
                    runSpacing: 6.w,
                    children: [
                      for (int i = 0; i < 6; i++)
                        UIColumn(
                          width: 70.w,
                          height: 90.w,
                          radius: 8.w,
                          color: kAppColor('#E0FFF8'),
                          children: [
                            UIText(
                              margin: EdgeInsets.only(top: 8.w, bottom: 1.5.w),
                              text: '已签到',
                              textColor: kAppColor('#0BC296'),
                              fontSize: 13.sp,
                            ),
                            UIImage(
                              assetImage: 'icon_signIn_coin.png',
                              width: 35.w,
                              height: 35.w,
                            ),
                            UIText(
                              margin: EdgeInsets.only(top: 3.h),
                              text: '抽奖1次',
                              textColor: kAppColor('#0BC296'),
                              fontSize: 11.sp,
                            ),
                          ],
                        ),
                      UIColumn(
                        radius: 8.w,
                        width: 147.w,
                        height: 91.w,
                        color: kAppColor('#FEF9ED'),
                        children: [
                          UIText(
                            width: double.maxFinite,
                            color: kAppColor('#FFFDCF'),
                            text: '开启加速升级模式',
                            textColor: kAppColor('#FF8D1A'),
                            fontSize: 13.sp,
                          ),
                          UIImage(
                            assetImage: 'icon_signIn_gift_bag.png',
                            width: 35.w,
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                UISolidButton(
                  margin: EdgeInsets.only(left: 46.w, right: 46.w, top: 17.w),
                  text: '立即签到',
                  fontSize: 17.sp,
                  height: 52.w,
                ),
                UIRow(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  margin: EdgeInsets.only(top: 15.w, left: 56.w, right: 56.w),
                  children: [
                    UIText(
                      text: '已连续签到2天',
                      textColor: kAppColor('#0F194D').withOpacity(0.5),
                      fontSize: 11.sp,
                      // fontWeight: FontWeight.bold,
                    ),
                  ],
                )
              ],
            )
          ]),
          UIImage(
            margin: EdgeInsets.only(top: 25.w),
            assetImage: 'icon_signIn_close.png',
            width: 39.w,
            onTap: () {
              Get.back();
            },
          )
        ],
      ),
    );
  }
}
