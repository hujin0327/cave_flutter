class SoulInteractionItemModel {
  final String? funRole;
  final String? gender;
  final num? distance;
  final String? nickName;
  final List<dynamic>? album;
  final bool? onlineStatus;
  final String? avatar;
  final String? aboutMe;
  final String? sexualOrientation;
  final String? accountId;
  final bool? medal;
  final num? id;
  final num? age;

  SoulInteractionItemModel({
    this.funRole,
    this.gender,
    this.distance,
    this.nickName,
    this.album,
    this.onlineStatus,
    this.avatar,
    this.aboutMe,
    this.sexualOrientation,
    this.accountId,
    this.medal,
    this.id,
    this.age,
  });

  factory SoulInteractionItemModel.fromJson(Map<String, dynamic> json) {
    return SoulInteractionItemModel(
      funRole: json['funRole'],
      gender: json['gender'],
      distance: json['distance'],
      nickName: json['nickName'],
      album: json['album'],
      onlineStatus: json['onlineStatus'],
      avatar: json['avatar'],
      aboutMe: json['aboutMe'],
      sexualOrientation: json['sexualOrientation'],
      accountId: json['accountId'],
      medal: json['medal'],
      id: json['id'],
      age: json['age'],
    );
  }

  Map<String, dynamic> toJson() => {
        'funRole': funRole,
        'gender': gender,
        'distance': distance,
        'nickName': nickName,
        'album': album,
        'onlineStatus': onlineStatus,
        'avatar': avatar,
        'aboutMe': aboutMe,
        'sexualOrientation': sexualOrientation,
        'accountId': accountId,
        'medal': medal,
        'id': id,
        'age': age,
      };
}
