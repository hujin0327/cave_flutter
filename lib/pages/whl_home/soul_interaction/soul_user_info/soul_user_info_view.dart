import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cave_flutter/common/common_action.dart';
import 'package:cave_flutter/common/common_widget.dart';
import 'package:cave_flutter/common/sliver_header_delegate.dart';
import 'package:cave_flutter/model/dynamic_model.dart';
import 'package:cave_flutter/model/image_model.dart';
import 'package:cave_flutter/pages/square/comment/comment_view.dart';
import 'package:cave_flutter/pages/square/square_more_widget.dart';
import 'package:cave_flutter/pages/whl_home/soul_interaction/soul_user_info/soul_user_info_logic.dart';
import 'package:cave_flutter/pages/whl_mine/user_info/add_friend_dialog/add_friend_dialog_view.dart';
import 'package:cave_flutter/routes/whl_app_pages.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:cave_flutter/utils/whl_user_utils.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/refresh_widget.dart';
import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';

class SoulUserInfoPage extends StatelessWidget {
  late String accountId;
  SoulUserInfoPage({
    super.key,
    String? accountId,
  }) {
    this.accountId = accountId ?? '';
  }

  final logic = Get.put(SoulUserInfoLogic());

  @override
  Widget build(BuildContext context) {
    logic.accountId = accountId;
    logic.isMine = accountId == WhlUserUtils.getUserInfo().accountId;
    if (!logic.isMine) {
      logic.addVisitLog();
    }

    logic.getUserDynamicsList(isRefresh: true);
    logic.getUserDetail();

    final double statusBarHeight = MediaQuery.of(context).padding.top;
    final double pinnedHeaderHeight = statusBarHeight + kToolbarHeight;
    // WhlUserInfoModel myUserInfo = WhlUserUtils.getUserInfo();

    return MediaQuery.removePadding(
      removeTop: true,
      removeBottom: true,
      context: context,
      child: Scaffold(
        backgroundColor: kAppColor('#323232'),
        body: GetBuilder<SoulUserInfoLogic>(builder: (logic) {
          String? backgroundImageUrl = logic.userModel?.avatar?.toImageUrl();
          if ((logic.userModel?.albumList ?? []).isNotEmpty) {
            ImageModel model = logic.userModel!.albumList!.first;
            backgroundImageUrl = model.imageUrl?.toImageUrl();
          }
          return Stack(
            children: [
              UIImage(
                httpImage: backgroundImageUrl,
                width: double.infinity,
                height: 733.h,
                fit: BoxFit.fitHeight,
              ),
              MyRefresh(
                  controller: logic.refreshController,
                  onLoad: logic.selectedIndex == 1
                      ? () {
                          logic.getUserDynamicsList();
                        }
                      : null,
                  childBuilder: (context, physics) {
                    return CustomScrollView(
                      controller: logic.scrollController,
                      physics: physics,
                      slivers: [
                        SliverAppBar(
                          pinned: true,
                          expandedHeight: ScreenUtil().statusBarHeight,
                          floating: false,
                        ),
                        SliverAppBar(
                            pinned: true,
                            expandedHeight: 0,
                            floating: false,
                            // backgroundColor: Colors.transparent,
                            flexibleSpace: FlexibleSpaceBar(
                              // collapseMode: CollapseMode.pin,
                              background: Visibility(
                                visible: logic.isShowBar,
                                child: UIRow(
                                  padding: EdgeInsets.only(
                                    left: 18.w,
                                  ),
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  // mainAxisAlignment: MainAxisAlignment.start,
                                  color: kAppColor('#252525').withOpacity(0.21),
                                  height: 40.h,
                                  children: [
                                    UIContainer(
                                      child: const Icon(
                                        Icons.arrow_back_ios,
                                        color: Colors.white,
                                      ),
                                      onTap: () {
                                        Get.back();
                                      },
                                    ),
                                    UIImage(
                                      httpImage:
                                          logic.userModel?.avatar?.toImageUrl(),
                                      assetPlaceHolder: 'icon_app_logo.png',
                                      width: 30.w,
                                      height: 30.w,
                                      radius: 30.w,
                                    ),
                                    UIText(
                                      text:
                                          '${logic.userModel?.nickName ?? ""} · ${logic.userModel?.age ?? 0}岁 · ${logic.userModel?.sexualOrientation ?? "未知"} · ${logic.userModel?.funRole ?? "未知"} · ${logic.isMine ? "" : "0.0km"}',
                                      textColor: Colors.white,
                                      fontSize: 12.sp,
                                      // fontWeight: FontWeight.bold,
                                    )
                                  ],
                                ),
                              ),
                            )),
                        SliverPersistentHeader(
                            delegate: MySliverHeaderDelegate(
                          maxHeight: 585.h,
                          minHeight: 585.h,
                          child: Stack(
                            alignment: Alignment.topCenter,
                            children: [
                              Column(
                                children: [
                                  Container(height: 300.h),
                                  Stack(
                                    clipBehavior: Clip.none,
                                    children: [
                                      UIContainer(
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 15.w),
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(20.w),
                                          child: BackdropFilter(
                                            filter: ImageFilter.blur(
                                                sigmaY: 44, sigmaX: 44),
                                            child: Stack(
                                              clipBehavior: Clip.none,
                                              children: [
                                                UIColumn(
                                                  // margin: EdgeInsets.only(top: 400.h),
                                                  color: kAppColor('#A1A1A1')
                                                      .withOpacity(0.12),
                                                  width: double.infinity,
                                                  height: 250.w,
                                                  children: [
                                                    UIRow(
                                                      margin: EdgeInsets.only(
                                                          right: 25.w,
                                                          top: 12.w),
                                                      mainAxisAlignment:
                                                          MainAxisAlignment.end,
                                                      children: [
                                                        UIText(
                                                          text: '关注',
                                                          textColor: Colors
                                                              .white
                                                              .withOpacity(
                                                                  0.68),
                                                          fontSize: 11.sp,
                                                          marginDrawable: 5.w,
                                                          topDrawable: UIText(
                                                            text: '3120',
                                                            textColor:
                                                                Colors.white,
                                                            fontSize: 17.sp,
                                                            fontWeight:
                                                                FontWeight.w500,
                                                          ),
                                                        ),
                                                        UIText(
                                                          margin: EdgeInsets
                                                              .symmetric(
                                                                  horizontal:
                                                                      20.w),
                                                          text: '粉丝',
                                                          textColor: Colors
                                                              .white
                                                              .withOpacity(
                                                                  0.68),
                                                          fontSize: 11.sp,
                                                          marginDrawable: 5.w,
                                                          topDrawable: UIText(
                                                            text: '1.2w',
                                                            textColor:
                                                                Colors.white,
                                                            fontSize: 17.sp,
                                                            fontWeight:
                                                                FontWeight.w500,
                                                          ),
                                                        ),
                                                        UIText(
                                                          text: 'Cave',
                                                          textColor: Colors
                                                              .white
                                                              .withOpacity(
                                                                  0.68),
                                                          fontSize: 11.sp,
                                                          marginDrawable: 5.w,
                                                          topDrawable: UIText(
                                                            text: '8',
                                                            textColor:
                                                                Colors.white,
                                                            fontSize: 17.sp,
                                                            fontWeight:
                                                                FontWeight.w500,
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                  ],
                                                ),
                                                UIColumn(
                                                  margin: EdgeInsets.only(
                                                      top: 60.w, left: 15.w),
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Row(
                                                      children: [
                                                        UIText(
                                                          text: logic.userModel
                                                              ?.nickName,
                                                          textColor:
                                                              Colors.white,
                                                          fontSize: 22.sp,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                        // UIImage(
                                                        //   margin:
                                                        //       EdgeInsets.only(
                                                        //           left: 4.w),
                                                        //   assetImage:
                                                        //       'icon_level.png',
                                                        //   width: 30.w,
                                                        // ),
                                                        // UIImage(
                                                        //   margin:
                                                        //       EdgeInsets.only(
                                                        //           left: 4.w),
                                                        //   assetImage:
                                                        //       'icon_svip.png',
                                                        //   width: 51.w,
                                                        // )
                                                      ],
                                                    ),
                                                    UIRow(
                                                      margin: EdgeInsets.only(
                                                          top: 4.w),
                                                      children: [
                                                        UIText(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 2.w,
                                                                  right: 5.w),
                                                          text:
                                                              'ID:${logic.accountId}',
                                                          textColor:
                                                              Colors.white,
                                                          fontSize: 10.sp,
                                                        ),
                                                        UIText(
                                                          padding: EdgeInsets
                                                              .symmetric(
                                                                  horizontal:
                                                                      6.w,
                                                                  vertical:
                                                                      2.w),
                                                          color: kAppColor(
                                                                  '#6A6000')
                                                              .withOpacity(
                                                                  0.21),
                                                          text:
                                                              '财富值${logic.userModel?.wealth ?? 0}',
                                                          textColor: kAppColor(
                                                              '#FFE800'),
                                                          fontSize: 11.sp,
                                                          radius: 6.w,
                                                        ),
                                                        UIText(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 5.w),
                                                          padding: EdgeInsets
                                                              .symmetric(
                                                                  horizontal:
                                                                      6.w,
                                                                  vertical:
                                                                      2.w),
                                                          color: kAppColor(
                                                                  '#7F2020')
                                                              .withOpacity(
                                                                  0.24),
                                                          text:
                                                              '魅力值${logic.userModel?.charm ?? 0}',
                                                          textColor: kAppColor(
                                                              '#FF73A0'),
                                                          fontSize: 11.sp,
                                                          radius: 6.w,
                                                        ),
                                                      ],
                                                    ),
                                                    UIRow(
                                                      margin: EdgeInsets.only(
                                                          top: 16.w,
                                                          bottom: 14.w),
                                                      children: [
                                                        UIText(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  right: 5.w),
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 8.w,
                                                                  right: 7.w,
                                                                  top: 3.w,
                                                                  bottom: 3.w),
                                                          alignment:
                                                              Alignment.center,
                                                          text:
                                                              '${logic.userModel?.age ?? 0}岁',
                                                          color: kAppColor(
                                                                  '#A06075')
                                                              .withOpacity(
                                                                  0.16),
                                                          textColor:
                                                              getGenderColor(
                                                                  logic
                                                                      .userModel
                                                                      ?.gender),
                                                          fontSize: 10.sp,
                                                          strokeColor:
                                                              getGenderColor(
                                                                  logic
                                                                      .userModel
                                                                      ?.gender),
                                                          strokeWidth: 1.w,
                                                          radius: 18.w,
                                                          marginDrawable: 3.w,
                                                          startDrawable:
                                                              UIImage(
                                                            assetImage:
                                                                getGenderIcon(logic
                                                                    .userModel
                                                                    ?.gender),
                                                            height: 10.w,
                                                          ),
                                                        ),
                                                        RichText(
                                                            text: TextSpan(
                                                                text: '/ ',
                                                                style:
                                                                    TextStyle(
                                                                  color: kAppColor(
                                                                      '#979797'),
                                                                  fontSize:
                                                                      10.sp,
                                                                ),
                                                                children: [
                                                              TextSpan(
                                                                text: logic
                                                                        .userModel
                                                                        ?.sexualOrientation ??
                                                                    '未知',
                                                                style:
                                                                    TextStyle(
                                                                  color: kAppColor(
                                                                          '#D9D9D9')
                                                                      .withOpacity(
                                                                          0.7),
                                                                  fontSize:
                                                                      10.sp,
                                                                ),
                                                              ),
                                                              TextSpan(
                                                                text: ' / ',
                                                                style:
                                                                    TextStyle(
                                                                  color: kAppColor(
                                                                      '#979797'),
                                                                  fontSize:
                                                                      10.sp,
                                                                ),
                                                              ),
                                                              TextSpan(
                                                                text: logic
                                                                        .userModel
                                                                        ?.funRole ??
                                                                    "未知",
                                                                style:
                                                                    TextStyle(
                                                                  color: kAppColor(
                                                                          '#D9D9D9')
                                                                      .withOpacity(
                                                                          0.7),
                                                                  fontSize:
                                                                      10.sp,
                                                                ),
                                                              ),
                                                            ])),
                                                        UIText(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 8.w),
                                                          padding: EdgeInsets
                                                              .symmetric(
                                                                  horizontal:
                                                                      6.w,
                                                                  vertical:
                                                                      5.w),
                                                          radius: 6.w,
                                                          color: Colors.white
                                                              .withOpacity(
                                                                  0.25),
                                                          text: '查看安全词',
                                                          textColor:
                                                              Colors.white,
                                                          fontSize: 8.sp,
                                                        )
                                                      ],
                                                    ),
                                                    UIRow(
                                                      margin: EdgeInsets.only(
                                                          right: 16.w),
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        UIText(
                                                          text:
                                                              '杭州 · 在线 · ${logic.userModel?.distance ?? '未知'}km',
                                                          textColor:
                                                              Colors.white,
                                                          fontSize: 12.sp,
                                                          marginDrawable: 5.w,
                                                          startDrawable:
                                                              UIImage(
                                                            assetImage:
                                                                'icon_location_white.png',
                                                            width: 12.w,
                                                          ),
                                                        ),
                                                        UIText(
                                                          text:
                                                              '加入Cave${logic.userModel?.joinCaveDays ?? 0}天',
                                                          textColor: kAppColor(
                                                                  '#D9D9D9')
                                                              .withOpacity(0.7),
                                                          fontSize: 10.sp,
                                                        )
                                                      ],
                                                    ),
                                                    UIRow(
                                                      margin: EdgeInsets.only(
                                                          top: 7.w,
                                                          bottom: 7.w),
                                                      children: [
                                                        for (int i = 0;
                                                            i < 5;
                                                            i++)
                                                          UIImage(
                                                            assetImage:
                                                                'icon_rank_0.png',
                                                            width: 57.w,
                                                          )
                                                      ],
                                                    )
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Positioned(
                                        left: 30.w,
                                        top: -24.h,
                                        child: UIImage(
                                          httpImage: logic.userModel?.avatar
                                              ?.toImageUrl(),
                                          assetPlaceHolder: 'icon_app_logo.png',
                                          width: 76.w,
                                          height: 76.w,
                                          radius: 76.w,
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )),
                        SliverPersistentHeader(
                            pinned: true,
                            delegate: MySliverHeaderDelegate(
                              maxHeight: 50.w,
                              minHeight: 50.w,
                              child: Container(
                                color: kAppColor('#252525'),
                                padding: EdgeInsets.symmetric(vertical: 10.w),
                                child: TabBar(
                                  controller: logic.tabController,
                                  labelColor: Colors.white,
                                  labelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      color: kAppColor('#151718'),
                                      fontWeight: FontWeight.bold),
                                  unselectedLabelStyle: TextStyle(
                                      fontSize: 14.sp,
                                      color: Colors.white.withOpacity(0.7)),
                                  indicatorColor: Colors.white,
                                  indicatorSize: TabBarIndicatorSize.label,
                                  indicatorWeight: 2.0,
                                  isScrollable: false,
                                  unselectedLabelColor:
                                      Colors.white.withOpacity(0.7),
                                  tabs: const <Tab>[
                                    Tab(text: '资料'),
                                    Tab(text: '动态'),
                                    Tab(text: '相册')
                                  ],
                                ),
                              ),
                            )),
                        buildContextWidget(),
                        SliverToBoxAdapter(
                          child: Container(
                              height: 110.w, color: kAppColor('#323232')),
                        )
                      ],
                    );
                  }
                  // child:

                  ),
              // GetBuilder<UserInfoLogic>(builder: (logic) {
              //   return Visibility(
              //     visible: !logic.isScrolling,
              //     child: ClipRRect(
              //       child: BackdropFilter(
              //         filter: ImageFilter.blur(sigmaY: 44, sigmaX: 44),
              //         child: Column(
              //           mainAxisSize: MainAxisSize.min,
              //           children: [
              //             UIRow(
              //               padding: EdgeInsets.only(
              //                   left: 18.w,
              //                   top: ScreenUtil().statusBarHeight + 10.h),
              //               crossAxisAlignment: CrossAxisAlignment.center,
              //               color: kAppColor('#252525').withOpacity(0.21),
              //               height: ScreenUtil().statusBarHeight + 40.h,
              //               children: [
              //                 UIContainer(
              //                   child: const Icon(
              //                     Icons.arrow_back_ios,
              //                     color: Colors.white,
              //                   ),
              //                   onTap: () {
              //                     Get.back();
              //                   },
              //                 ),
              //                 UIImage(
              //                   httpImage:
              //                       logic.userModel?.avatar?.toImageUrl(),
              //                   assetPlaceHolder: 'icon_app_logo.png',
              //                   width: 30.w,
              //                   height: 30.w,
              //                   radius: 30.w,
              //                 ),
              //                 UIText(
              //                   text:
              //                       '${logic.userModel?.nickName ?? ""} · ${logic.userModel?.age ?? 0}岁 · ${logic.userModel?.sexualOrientation ?? "未知"} · ${logic.userModel?.funRole ?? "未知"} · ${logic.isMine ? "" : "0.0km"}',
              //                   textColor: Colors.white,
              //                   fontSize: 12.sp,
              //                   // fontWeight: FontWeight.bold,
              //                 )
              //               ],
              //             ),
              //             Container(
              //               color: kAppColor('#252525').withOpacity(0.21),
              //               padding: EdgeInsets.only(bottom: 10.w),
              //               child: buildTabBarView(logic),
              //             ),
              //           ],
              //         ),
              //       ),
              //     ),
              //   );
              // }),
              GetBuilder<SoulUserInfoLogic>(builder: (logic) {
                return Visibility(
                  visible: !logic.isScrolling && !logic.isMine,
                  child: Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: ClipRRect(
                      child: BackdropFilter(
                        filter: ImageFilter.blur(sigmaY: 44, sigmaX: 44),
                        child: UIRow(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          padding: EdgeInsets.only(
                              left: 46.w, right: 46.w, top: 24.w),
                          height: 110.w,
                          children: [
                            Expanded(
                              child: UIStrokeButton(
                                textColor: Colors.white,
                                strokeColor: Colors.white,
                                text: '关注',
                                onTap: () {
                                  logic.attentionRequest();
                                },
                              ),
                            ),
                            SizedBox(width: 23.w),
                            Expanded(
                              child: UISolidButton(
                                text: '+ 加好友',
                                color: kAppColor('#FFF754'),
                                textColor: kAppTextColor,
                                onTap: () {
                                  print('加好友参数--------');
                                  print(logic.userModel?.id.toString());
                                  Get.dialog(AddFriendDialog(),
                                      arguments: logic.userModel);
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              }),
            ],
          );
        }),
      ),
    );
  }

  Widget buildContextWidget() {
    return GetBuilder<SoulUserInfoLogic>(builder: (logic) {
      if (logic.selectedIndex == 1) {
        return SliverList(
            delegate: SliverChildBuilderDelegate((context, index) {
          DynamicModel model = logic.listData[index];
          return buildDynamicItem(model);
        }, childCount: logic.listData.length));
      } else if (logic.selectedIndex == 0) {
        return SliverList(
            delegate: SliverChildListDelegate([
          SingleChildScrollView(
            physics: const NeverScrollableScrollPhysics(),
            child: UIColumn(
              color: kAppColor('#323232'),
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 15.w),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20.w),
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaY: 44, sigmaX: 44),
                      child: UIColumn(
                        radius: 20.w,
                        color: kAppColor('#E9E9E9').withOpacity(0.12),
                        // margin: EdgeInsets.symmetric(horizontal: 15.w),
                        width: double.infinity,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          UIText(
                            margin: EdgeInsets.only(left: 14.w, top: 20.w),
                            text: '基础资料',
                            textColor: kAppColor('#E9E9E9'),
                            fontSize: 17.sp,
                            fontWeight: FontWeight.bold,
                          ),
                          UIWrap(
                            runAlignment: WrapAlignment.start,
                            wrapAlignment: WrapAlignment.start,
                            crossAxisAlignment: WrapCrossAlignment.start,
                            margin: EdgeInsets.symmetric(
                                horizontal: 14.w, vertical: 18.w),
                            runSpacing: 7.w,
                            spacing: 8.w,
                            children:
                                (logic.userModel?.baseInfo ?? []).map((e) {
                              return UIText(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 15.w, vertical: 7.w),
                                text: e,
                                textColor: Colors.white,
                                fontSize: 12.sp,
                                strokeColor: Colors.white,
                                strokeWidth: 1.w,
                                radius: 30.w,
                              );
                            }).toList(),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  margin:
                      EdgeInsets.symmetric(horizontal: 15.w, vertical: 20.w),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20.w),
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaY: 44, sigmaX: 44),
                      child: UIColumn(
                        radius: 20.w,
                        color: kAppColor('#E9E9E9').withOpacity(0.12),
                        // margin: EdgeInsets.symmetric(horizontal: 15.w),
                        width: double.infinity,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          UIText(
                            margin: EdgeInsets.only(left: 14.w, top: 20.w),
                            text: '关于我',
                            textColor: kAppColor('#E9E9E9'),
                            fontSize: 17.sp,
                            fontWeight: FontWeight.bold,
                          ),
                          UIText(
                            margin: EdgeInsets.only(top: 18.w, bottom: 40.w),
                            padding: EdgeInsets.symmetric(horizontal: 15.w),
                            text: logic.userModel?.aboutMe ?? "",
                            textColor: kAppColor('#D5D5D5'),
                            shrinkWrap: false,
                            fontSize: 13.sp,
                            fontWeight: FontWeight.bold,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 15.w),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20.w),
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaY: 44, sigmaX: 44),
                      child: UIColumn(
                        radius: 20.w,
                        color: kAppColor('#E9E9E9').withOpacity(0.12),
                        // margin: EdgeInsets.symmetric(horizontal: 15.w),
                        width: double.infinity,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          UIText(
                            margin: EdgeInsets.only(left: 14.w, top: 20.w),
                            text: '礼物展馆',
                            textColor: kAppColor('#E9E9E9'),
                            fontSize: 17.sp,
                            fontWeight: FontWeight.bold,
                            marginDrawable: 5.w,
                            endDrawable: UIText(
                              text: '已点亮12/36',
                              textColor: kAppColor('#A7A7A7'),
                              fontSize: 10.sp,
                            ),
                          ),
                          UIRow(
                            margin: EdgeInsets.symmetric(vertical: 15.w),
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              for (int i = 0; i < 5; i++)
                                UIImage(
                                  assetImage: 'icon_gift_0.png',
                                  width: 54.w,
                                )
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  margin:
                      EdgeInsets.symmetric(horizontal: 15.w, vertical: 24.w),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20.w),
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaY: 44, sigmaX: 44),
                      child: UIColumn(
                        radius: 20.w,
                        color: kAppColor('#E9E9E9').withOpacity(0.12),
                        // margin: EdgeInsets.symmetric(horizontal: 15.w),
                        width: double.infinity,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          UIText(
                            margin: EdgeInsets.only(left: 14.w, top: 20.w),
                            text: '灵魂标签',
                            textColor: kAppColor('#E9E9E9'),
                            fontSize: 17.sp,
                            fontWeight: FontWeight.bold,
                          ),
                          UIWrap(
                            runAlignment: WrapAlignment.start,
                            wrapAlignment: WrapAlignment.start,
                            crossAxisAlignment: WrapCrossAlignment.start,
                            margin: EdgeInsets.symmetric(
                                horizontal: 14.w, vertical: 18.w),
                            runSpacing: 7.w,
                            spacing: 8.w,
                            children:
                                (logic.userModel?.soulTagList ?? []).map((e) {
                              return UIText(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 15.w, vertical: 7.w),
                                text: e.label,
                                textColor: Colors.white,
                                fontSize: 12.sp,
                                strokeColor: Colors.white,
                                strokeWidth: 1.w,
                                radius: 30.w,
                              );
                            }).toList(),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ]));
      } else {
        return SliverList(
            delegate: SliverChildListDelegate([
          Container(
            color: kAppColor('#323232'),
            child: SingleChildScrollView(
              physics: ClampingScrollPhysics(),
              child: Container(
                padding: EdgeInsets.all(10),
                child: MasonryGridView.count(
                  // 展示几列
                  crossAxisCount: 2,
                  // 元素总个数
                  itemCount: logic.userModel != null
                      ? logic.userModel!.albumList?.length
                      : 0,
                  // 单个子元素
                  itemBuilder: (BuildContext context, int index) {
                    ImageModel? imageModel = logic.userModel!.albumList?[index];
                    return ClipRRect(
                      borderRadius: BorderRadius.circular(22.w),
                      child: CachedNetworkImage(
                        imageUrl: (imageModel?.imageUrl ?? "").toImageUrl(),
                        fit: BoxFit.fitWidth,
                      ),
                    );
                  },
                  // 纵向元素间距
                  mainAxisSpacing: 10,
                  // 横向元素间距
                  crossAxisSpacing: 10,
                  //本身不滚动，让外面的singlescrollview来滚动
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true, //收缩，让元素宽度自适应
                ),
              ),
            ),
          ),
        ]));
      }
    });
  }

  TabBar buildTabBarView(SoulUserInfoLogic logic) {
    return TabBar(
      controller: logic.tabController,
      labelColor: Colors.white,
      labelStyle: TextStyle(
          fontSize: 16.sp,
          color: kAppColor('#151718'),
          fontWeight: FontWeight.bold),
      unselectedLabelStyle:
          TextStyle(fontSize: 14.sp, color: Colors.white.withOpacity(0.7)),
      indicatorColor: Colors.white,
      indicatorSize: TabBarIndicatorSize.label,
      indicatorWeight: 2.0,
      isScrollable: false,
      unselectedLabelColor: Colors.white.withOpacity(0.7),
      tabs: const <Tab>[Tab(text: '资料'), Tab(text: '动态'), Tab(text: '相册')],
    );
  }

  Widget buildDynamicItem(DynamicModel model) {
    return Container(
      color: kAppColor('#323232'),
      child: UIRow(
        margin: EdgeInsets.only(bottom: 24.w, left: 15.w, right: 15.w),
        padding:
            EdgeInsets.only(left: 16.w, right: 14.w, top: 20.w, bottom: 25.w),
        gradientStartColor: kAppColor('#473D3E'),
        gradientEndColor: kAppColor('#584848'),
        radius: 20.w,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          UIImage(
            margin: EdgeInsets.only(right: 9.w),
            httpImage: model.avatar?.toImageUrl(),
            assetPlaceHolder: 'icon_app_logo.png',
            width: 45.w,
            height: 45.w,
            radius: 22.5.w,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                UIRow(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            UIText(
                              text: model.user?.nickName ?? "",
                              textColor: Colors.white,
                              fontSize: 16.sp,
                              fontWeight: FontWeight.bold,
                            ),
                            UIText(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 4.w, vertical: 3.w),
                              margin: EdgeInsets.only(left: 4.w),
                              text: 'Lv${model.user?.level ?? "0"}',
                              alignment: Alignment.center,
                              textColor: kAppTextColor,
                              fontSize: 8.sp,
                              radius: 7.w,
                              color: kAppColor("#F4EEFF"),
                            ),
                            // UIImage(
                            //   margin: EdgeInsets.only(left: 4.w),
                            //   assetImage: 'icon_svip.png',
                            //   width: 51.w,
                            // )
                          ],
                        ),
                        Row(
                          children: [
                            UIText(
                              padding: EdgeInsets.only(left: 8.w, right: 7.w),
                              height: 18.w,
                              alignment: Alignment.center,
                              text: '${model?.user?.age ?? 0}岁',
                              textColor: getGenderColor(model.user?.gender),
                              fontSize: 11.sp,
                              color: kAppColor('#F6F6F6').withOpacity(0.11),
                              radius: 6.w,
                              marginDrawable: 3.w,
                              startDrawable: UIImage(
                                assetImage: getGenderIcon(model.user?.gender),
                                height: 10.w,
                              ),
                            ),
                            UIText(
                              margin: EdgeInsets.only(left: 4.w),
                              padding: EdgeInsets.only(left: 8.w, right: 7.w),
                              height: 18.w,
                              alignment: Alignment.center,
                              text: model.user?.sexualOrientation ?? "未知",
                              textColor: Colors.white,
                              fontSize: 11.sp,
                              color: kAppColor('#F6F6F6').withOpacity(0.11),
                              radius: 6.w,
                            ),
                            UIText(
                              margin: EdgeInsets.only(left: 4.w),
                              padding: EdgeInsets.only(left: 8.w, right: 7.w),
                              height: 18.w,
                              alignment: Alignment.center,
                              text: model.user?.funRole ?? "未知",
                              textColor: Colors.white,
                              fontSize: 11.sp,
                              color: kAppColor('#F6F6F6').withOpacity(0.11),
                              radius: 6.w,
                            ),
                          ],
                        ),
                      ],
                    ),
                    Expanded(child: Container()),
                    UIImage(
                      assetImage: 'icon_square_top.gif',
                      width: 20.w,
                      height: 20.w,
                    ),
                    UIImage(
                      margin: EdgeInsets.only(left: 9.w, right: 4.w),
                      assetImage: 'icon_square_hot.png',
                      width: 20.w,
                      height: 20.w,
                    ),
                    UIContainer(
                      child: Icon(Icons.more_vert, color: kAppColor('#9E9E9E')),
                      onTap: () {
                        Get.bottomSheet(square_more_widget(
                          dynamicId: model.id ?? "",
                          isMe: model.accountId ==
                              WhlUserUtils.getUserInfo().accountId,
                          callBack: (type) {
                            if (type == 2) {
                              logic.getUserDynamicsList(isRefresh: true);
                            } else if (type == 3) {
                              logic.toEditSquarePage(model);
                            } else if (type == 4) {
                              logic.listData.remove(model);
                              logic.update();
                            }
                          },
                        ));
                      },
                    )
                  ],
                ),
                UIText(
                  margin: EdgeInsets.only(top: 10.w),
                  text: model.content ?? "",
                  textColor: Colors.white,
                  fontSize: 15.sp,
                  shrinkWrap: false,
                ),
                if (model?.urls?.first.imageUrl?.isNotEmpty ?? false)
                  UIImage(
                    margin: EdgeInsets.only(top: 13.w),
                    httpImage: model?.urls?.first.imageUrl?.toImageUrl(),
                    assetPlaceHolder: 'icon_app_logo.png',
                    width: 166.w,
                    alignment: Alignment.centerLeft,
                    height: 166.w,
                    fit: BoxFit.cover,
                    radius: 6.w,
                  ),
                Container(
                  margin: EdgeInsets.only(top: 11.w),
                  child: RichText(
                    text: TextSpan(
                        text: '浙江杭州 ·15分钟前发布 · 来自 ',
                        style: TextStyle(
                            fontSize: 10.sp, color: kAppColor('#C1C1C1')),
                        children: [
                          TextSpan(
                              text: 'LES',
                              style: TextStyle(
                                  fontSize: 10.sp, color: Colors.white))
                        ]),
                  ),
                ),
                UIRow(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  margin: EdgeInsets.only(top: 21.w),
                  children: [
                    UIImage(
                      assetImage: 'icon_square_share.png',
                      width: 20.w,
                      imageColor: model.setting?.notForward == 1
                          ? kAppSub3TextColor.withOpacity(0.5)
                          : kAppColor("#ADADAD"),
                      onTap: () {
                        if (model.setting?.notComment == 1) {
                          MyToast.show("该动态禁止转发");
                          return;
                        }
                        Get.toNamed(Routes.publish,
                            arguments: {"model": model, "isForward": true});
                      },
                    ),
                    UIImage(
                      assetImage: 'icon_square_fly_grey.png',
                      width: 20.w,
                    ),
                    UIImage(
                      assetImage: 'icon_square_gift.png',
                      width: 20.w,
                    ),
                    UIText(
                      text: '${model.commentNum ?? 0}',
                      textColor: model.setting?.notComment == 1
                          ? kAppSub3TextColor.withOpacity(0.5)
                          : kAppColor("#ADADAD"),
                      fontSize: 14.sp,
                      marginDrawable: 3.w,
                      startDrawable: UIImage(
                        assetImage: 'icon_square_comment.png',
                        width: 20.w,
                        imageColor: model.setting?.notComment == 1
                            ? kAppSub3TextColor.withOpacity(0.5)
                            : kAppColor("#ADADAD"),
                      ),
                      onTap: () {
                        if (model.setting?.notComment == 1) {
                          MyToast.show("该动态禁止评论");
                          return;
                        }
                        Get.bottomSheet(
                            CommentWidget(
                              dynamicId: model.id ?? "",
                            ),
                            isScrollControlled: true);
                      },
                    ),
                    UIText(
                      text: '${model.goodNum ?? 0}',
                      textColor: kAppColor('#ADADAD'),
                      fontSize: 14.sp,
                      marginDrawable: 3.w,
                      startDrawable: UIImage(
                        assetImage: model?.currentZan == true
                            ? 'icon_square_isLike.png'
                            : 'icon_square_like.png',
                        width: 20.w,
                      ),
                      onTap: () {
                        dianZanDynamicRequest(model!, callback: (isSuccess) {
                          if (isSuccess && logic != null) {
                            logic.update();
                          }
                        });
                      },
                    ),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
