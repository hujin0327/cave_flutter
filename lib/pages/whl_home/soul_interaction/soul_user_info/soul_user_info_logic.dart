import 'package:cave_flutter/model/dynamic_model.dart';
import 'package:cave_flutter/model/user_model.dart';
import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/routes/whl_app_pages.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/whl_current_limiting_util.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class SoulUserInfoLogic extends GetxController
    with GetSingleTickerProviderStateMixin {
  late TabController tabController;
  ScrollController scrollController = ScrollController();
  EasyRefreshController refreshController = EasyRefreshController(
      controlFinishLoad: true, controlFinishRefresh: true);
  bool isScrolling = false; //
  bool isShowBar = false; //
  int selectedIndex = 0;

  String accountId = "";
  // String userId = "";
  List listData = [];
  int size = 10;
  int current = 1;

  UserModel? userModel;
  // OtherUserModel? otherUserModel;
  bool isMine = false;

  @override
  void onInit() {
    super.onInit();
    tabController =
        TabController(length: 3, vsync: this, initialIndex: selectedIndex);
    tabController.addListener(() {
      selectedIndex = tabController.index;
      update();
      if (tabController.index == 0) {
        // getListIn(true);
      } else {
        // getListOut(true);
      }
    });
    scrollController.addListener(() {
      isScrolling = true;
      isShowBar = scrollController.offset > 200.h;
      WhlCurrentLimitingUtil.debounce(() {
        print('11111111111111111');
        isScrolling = false;
        update();
      });
      update();
    });
    // getMyUserInfo();
  }

  @override
  void onClose() {
    tabController.dispose();
    super.onClose();
  }

  getUserDetail() {
    WhlApi.getPersonDetail
        .get({"accountId": accountId}, withLoading: true).then((value) {
      if (value.isSuccess()) {
        userModel = UserModel.fromJson(value.data);
        update();
      }
    });
  }

  // getMyUserInfo() {
  //   WhlApi.myUserInformation.get({}, withLoading: true).then((value) {
  //     if (value.isSuccess()) {
  //       otherUserModel = OtherUserModel.fromJson(value.data);
  //       update();
  //     }
  //   });
  // }

  getUserDynamicsList({bool isRefresh = false}) async {
    if (isRefresh) {
      current = 1;
    } else {
      current++;
    }
    Map<String, dynamic> param = {
      "size": size,
      "current": current,
      "accountId": accountId
    };

    ResponseData responseData = await WhlApi.getUserDynamicsList.get(param);
    if (responseData.isSuccess()) {
      List records = responseData.data["records"];
      List<DynamicModel> modelList =
          records.map((e) => DynamicModel.fromJson(e)).toList();
      if (isRefresh) {
        listData = modelList;
        refreshController.finishRefresh();
        refreshController.resetFooter();
      } else {
        listData.addAll(modelList);
        refreshController.finishLoad();
      }
      if (records.length < size) {
        refreshController.finishLoad(IndicatorResult.noMore);
      }
      update();
    } else {
      current--;
      refreshController.finishRefresh();
      refreshController.finishLoad();
      update();
    }
  }

  addVisitLog() {
    WhlApi.addVisit.get({"accountId": accountId}).then((value) {
      if (value.isSuccess()) {
        print("添加访问记录成功");
      }
    });
  }

  attentionRequest() {
    '${WhlApi.userFollow}$accountId'.get({}, withLoading: true).then((value) {
      if (value.isSuccess()) {
        MyToast.show("关注成功");
      }
    });
  }

  Future<void> toEditSquarePage(DynamicModel model) async {
    print(111);
    var a = await Get.toNamed(Routes.publish, arguments: {"model": model});
    if (a != null) {
      getUserDynamicsList(isRefresh: true);
    }
  }
}
