import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/pages/whl_home/soul_interaction/soul_interaction_item_model.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SoulInteractionLogic extends GetxController {
  final PageController pageController = PageController();
  List<SoulInteractionItemModel> dataSource = [];

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    var arguments = Get.arguments;
    if (arguments != null) {
      // accountId = arguments['accountId'];
    }

    requestData();
    // pageController.addListener(() {});
  }

  /// 加载指定页的数据
  void requestData() {
    // var params = {'current': page, 'size': pageSize, 'accountId': accountId};
    WhlApi.getSoulPeoples.get({}).then((value) {
      if (value.isSuccess()) {
        dataSource.clear();
        Map data = value.data;
        if (data["list"] != null) {
          for (var element in data["list"]) {
            dataSource.add(SoulInteractionItemModel.fromJson(element));
          }
        }
      } else {
        MyToast.show(value.msg ?? "获取失败");
      }
      print('【cave】dataSource: $dataSource');
      update();
    });
  }
}
