import 'package:cave_flutter/pages/whl_home/soul_interaction/soul_interaction_logic.dart';
import 'package:cave_flutter/pages/whl_home/soul_interaction/soul_user_info/soul_user_info_view.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SoulInteractionPage extends StatelessWidget {
  SoulInteractionPage({super.key});
  final logic = Get.put(SoulInteractionLogic());
  // ScreenUtil().statusBarHeight
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppWhiteColor,
      body: GetBuilder<SoulInteractionLogic>(builder: (logic) {
        return PageView(
          controller: logic.pageController,
          children: logic.dataSource.map((e) {
            return UIColumn(
              color: Colors.red,
              children: [
                Expanded(
                    child: SoulUserInfoPage(
                  accountId: e.accountId,
                )),
              ],
            );
          }).toList(),
          onPageChanged: (int page) {
            print('【cave】page: $page');
          },
        );
      }),
    );
  }
}
