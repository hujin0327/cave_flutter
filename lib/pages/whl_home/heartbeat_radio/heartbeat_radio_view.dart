import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../style/whl_style.dart';
import 'heartbeat_radio_logic.dart';

class HeartbeatRadioPage extends StatelessWidget {
  HeartbeatRadioPage({Key? key}) : super(key: key);

  final logic = Get.put(HeartbeatRadioLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Align(
            alignment: Alignment.topRight,
            child: UIImage(
              assetImage: 'img_bg_heartbeat_radio.png',
              width: 213.w,
            ),
          ),
          Column(
            children: [
              UIText(
                margin: EdgeInsets.only(top: 5.w + ScreenUtil().statusBarHeight, left: 23.w),
                text: '心动电台',
                textColor: kAppTextColor,
                alignment: Alignment.centerLeft,
                fontSize: 22.sp,
                fontWeight: FontWeight.bold,
              ),
              Expanded(
                  child: CustomScrollView(
                slivers: [
                  SliverPadding(
                    padding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 12.w),
                    sliver: SliverGrid(
                      delegate: SliverChildBuilderDelegate((context, index) {
                        return UIStack(
                          radius: 15.w,
                          children: [
                            UIImage(
                              assetImage: 'icon_app_logo.png',
                              width: 168.w,
                              fit: BoxFit.fill,
                            ),
                            Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    UIText(
                                      margin: EdgeInsets.only(top: 8.w, right: 8.w),
                                      padding: EdgeInsets.symmetric(horizontal: 7.w, vertical: 2.w),
                                      text: '情感',
                                      textColor: kAppTextColor,
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                      radius: 6.w,
                                    ),
                                  ],
                                ),
                                Expanded(child: Container()),
                                UIText(
                                  margin: EdgeInsets.only(left: 12.w),
                                  text: 'INJT闲聊',
                                  textColor: Colors.white,
                                  alignment: Alignment.centerLeft,
                                  fontSize: 15.sp,
                                  fontWeight: FontWeight.bold,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    UIStack(
                                      margin: EdgeInsets.only(left: 11.w, bottom: 12.w, top: 4.w),
                                      alignment: Alignment.centerLeft,
                                      clipBehavior: Clip.none,
                                      children: [
                                        for (int i = 0; i < 4; i++)
                                          UIImage(
                                            assetImage: 'icon_app_logo.png',
                                            width: 20.w,
                                            margin: EdgeInsets.only(left: i * 10.w),
                                            strokeColor: Colors.white,
                                            strokeWidth: 1.w,
                                            radius: 21.w,
                                          )
                                      ],
                                    ),
                                    UIText(
                                      margin: EdgeInsets.only(right: 13.w),
                                      text: '88',
                                      textColor: Colors.white,
                                      fontSize: 13.sp,
                                      fontWeight: FontWeight.bold,
                                    )
                                  ],
                                )
                              ],
                            )
                          ],
                        );
                      }, childCount: 20),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2, mainAxisSpacing: 10.w, crossAxisSpacing: 10.w, childAspectRatio: 1),
                    ),
                  )
                ],
              ))
            ],
          )
        ],
      ),
    );
  }
}
