import 'package:cave_flutter/config/whl_global_config.dart';
import 'package:cave_flutter/routes/whl_app_pages.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:video_player/video_player.dart';

import 'whl_login_logic.dart';

class WhlLoginPage extends StatelessWidget {
  WhlLoginPage({Key? key}) : super(key: key);

  final logic = Get.put(WhlLoginLogic());

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      child: Scaffold(
        // backgroundColor: kAppBlackColor,
        body: GetBuilder<WhlLoginLogic>(builder: (logic) {
          return Stack(
            alignment: Alignment.topCenter,
            children: [
              Center(
                child: logic.videoPlayerController.value.isInitialized
                    ?
                AspectRatio(
                  aspectRatio: ScreenUtil().screenWidth/ScreenUtil().screenHeight,
                  child: VideoPlayer(logic.videoPlayerController),
                )
                    : const UIImage(
                  assetImage: 'img_bg_home.png',
                  fit: BoxFit.cover,
                  width: double.infinity,
                  height: double.infinity,
                ),
              ),
              UIContainer(
                color: Colors.black.withOpacity(0.3),
                width: double.infinity,
                height: double.infinity,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  UIImage(
                    margin: EdgeInsets.only(top: 180.h),
                    assetImage: 'icon_app_logo.png',
                    width: 90.w,
                    height: 90.w,
                    radius: 24.w,
                  ),
                  UIText(
                    margin: EdgeInsets.only(top: 16.h),
                    text: '拥抱每一种人格',
                    fontSize: 14.sp,
                    textColor: Colors.white,
                  ),
                  // UIText(
                  //   margin: EdgeInsets.only(top: 183.h),
                  //   text: '136****3311',
                  //   textColor: Colors.white,
                  //   fontSize: 22.sp,
                  //   fontWeight: FontWeight.bold,
                  // ),
                  UISolidButton(
                    margin: EdgeInsets.only(top: 210.h, left: 45.w, right: 45.w),
                    text: '一键登录',
                    height: 46.h,
                    color: Colors.black,
                    onTap: () {
                      logic.doClickFastLogin();
                    },
                  ),
                  UIRow(
                    margin: EdgeInsets.symmetric(
                        horizontal: 73.w, vertical: 11.h),
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      UIText(
                        text: '密码登录',
                        textColor: Colors.white.withOpacity(0.6),
                        fontSize: 13.sp,
                        onTap: () {
                          Get.toNamed(Routes.loginForPassword);
                        },
                      ),
                      UIText(
                        text: '其他手机号登录',
                        textColor: Colors.white.withOpacity(0.6),
                        fontSize: 13.sp,
                        onTap: () {
                          Get.toNamed(Routes.loginForPhone);
                        },
                      ),
                    ],
                  ),
                  UIRow(
                    margin: EdgeInsets.only(top: 20.h, left: 80.w, right: 80.w),
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: logic.loginTypeList.map((e) {
                      return UIImage(
                        assetImage: e.icon,
                        width: 47.w,
                        height: 47.w,
                        onTap: (){
                          logic.thirdLogin();
                        },
                      );
                    }).toList(),
                  ),
                  Expanded(child: Container()),
                  UIRow(
                    width: double.infinity,
                    margin: EdgeInsets.only(
                        bottom: ScreenUtil().bottomBarHeight + 15.h),
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GetBuilder<WhlLoginLogic>(builder: (logic) {
                        return UIImage(
                          assetImage: logic.agreePrivateStatus
                              ? 'icon_select.png'
                              : 'icon_select_nor.png',
                          padding: EdgeInsets.only(
                              top: 10.w, bottom: 10.w, right: 5.w, left: 15.w),
                          width: 31.w,
                          height: 31.w,
                          onTap: () {
                            logic.doClickSelect();
                          },
                        );
                      }),
                      RichText(
                          maxLines: 2,
                          text: TextSpan(children: [
                            TextSpan(
                              text: '已阅读并同意',
                              style: TextStyle(
                                fontSize: 10.sp,
                                color: kAppColor('#D9D9D9'),
                              ),
                            ),
                            TextSpan(
                              text: ' 用户协议、',
                              style: TextStyle(
                                fontSize: 10.sp,
                                color: Colors.white.withOpacity(0.9),
                                // decoration: TextDecoration.underline,
                              ),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  logic.doClickTermsOfUser();
                                },
                            ),
                            TextSpan(
                              text: ' 隐身政策、',
                              style: TextStyle(
                                fontSize: 10.sp,
                                color: Colors.white.withOpacity(0.9),
                                // decoration: TextDecoration.underline,
                              ),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  logic.doClickPrivacyPolicy();
                                },
                            ),
                            TextSpan(
                              text: ' 中国移动认证服务协议',
                              style: TextStyle(
                                fontSize: 10.sp,
                                color: Colors.white.withOpacity(0.9),
                                // decoration: TextDecoration.underline,
                              ),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  logic.doClickPrivacyPolicy();
                                },
                            )
                          ])),
                    ],
                  )
                ],
              )
            ],
          );
        }),
      ),
    );
  }
}
