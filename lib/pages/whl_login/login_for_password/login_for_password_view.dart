import 'package:cave_flutter/pages/whl_login/login_for_phone/login_for_phone_logic.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../routes/whl_app_pages.dart';
import '../../../widgets/brick/brick.dart';
import 'login_for_password_logic.dart';

class LoginForPasswordPage extends StatelessWidget {
  LoginForPasswordPage({Key? key}) : super(key: key);

  final logic = Get.put(LoginForPasswordLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white.withOpacity(0.5),
      body: Container(
        margin: EdgeInsets.only(top: 52.h),
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(30.w), topRight: Radius.circular(30.w)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                UIContainer(
                  padding: EdgeInsets.only(top: 18.h, left: 20.w, right: 20.w),
                  child: const Icon(Icons.arrow_back_ios),
                  onTap: () {
                    Get.back();
                  },
                ),
                UIText(
                  padding: EdgeInsets.only(top: 18.h, left: 20.w, right: 20.w),
                  text: '使用验证码登录',
                  textColor: kAppColor('#9C9C9C'),
                  fontSize: 16.sp,
                  onTap: () {
                    logic.doClickLoginForPhone();
                  },
                )
              ],
            ),
            UIText(
              margin: EdgeInsets.only(top: 44.h, left: 30.w),
              text: '手机密码登录',
              textColor: kAppTextColor,
              fontSize: 30.sp,
              fontWeight: FontWeight.bold,
              // marginDrawable: 11.h,
              // bottomDrawable: UIText(
              //   text: '未注册绑定的手机号将自动注册',
              //   textColor: kAppColor('#7E7E7E'),
              //   fontSize: 15.sp,
              // ),
            ),
            UIRow(
              margin: EdgeInsets.only(top: 68.h, left: 34.w, right: 39.w),
              children: [
                UIText(
                  padding: EdgeInsets.symmetric(horizontal: 25.w),
                  radius: 18.w,
                  alignment: Alignment.center,
                  height: 53.h,
                  text: '+86',
                  textColor: kAppTextColor,
                  fontSize: 14.sp,
                  fontWeight: FontWeight.bold,
                  color: kAppColor('#F7F7F9'),
                ),
                SizedBox(width: 7.w),
                Expanded(
                    child: UIContainer(
                  radius: 18.w,
                  height: 53.h,
                  color: kAppColor('#F7F7F9'),
                  alignment: Alignment.centerLeft,
                  child: TextField(
                    inputFormatters: [LengthLimitingTextInputFormatter(11)],
                    controller: logic.phoneController,
                    keyboardType: TextInputType.number,
                    maxLines: 1,
                    autofocus: false,
                    style: TextStyle(fontSize: 14.sp, color: kAppTextColor, fontWeight: FontWeight.bold),
                    decoration: InputDecoration(
                        border: const OutlineInputBorder(borderSide: BorderSide.none),
                        hintText: '手机号码',
                        hintStyle: TextStyle(color: kAppColor('#C1C1C1'), fontWeight: FontWeight.bold, fontSize: 14.sp),
                        contentPadding: EdgeInsets.only(left: 20.w),
                        suffixIcon: GestureDetector(
                          onTap: () {
                            logic.phoneController.clear();
                          },
                          child: Icon(
                            Icons.close,
                            color: kAppColor('#C1C1C1'),
                          ),
                        )),
                    onChanged: (value) {
                      logic.onCheckBtnEnable();
                    },
                  ),
                ))
              ],
            ),
            UIContainer(
              margin: EdgeInsets.only(top: 18.h, left: 34.w, right: 39.w),
              radius: 18.w,
              height: 53.h,
              color: kAppColor('#F7F7F9'),
              alignment: Alignment.centerLeft,
              child: TextField(
                inputFormatters: [LengthLimitingTextInputFormatter(20)],
                controller: logic.passwordController,
                keyboardType: TextInputType.visiblePassword,
                maxLines: 1,
                autofocus: false,
                obscureText: true,
                style: TextStyle(fontSize: 14.sp, color: kAppTextColor, fontWeight: FontWeight.bold),
                decoration: InputDecoration(
                    border: const OutlineInputBorder(borderSide: BorderSide.none),
                    hintText: '密码',
                    hintStyle: TextStyle(color: kAppColor('#C1C1C1'), fontWeight: FontWeight.bold, fontSize: 14.sp),
                    contentPadding: EdgeInsets.only(left: 20.w)),
                onChanged: (value) {
                  logic.onCheckBtnEnable();
                },
              ),
            ),
            GetBuilder<LoginForPasswordLogic>(builder: (logic) {
              return UISolidButton(
                margin: EdgeInsets.only(top: 41.h, left: 38.w, right: 38.w, bottom: 21.h),
                text: '登录',
                height: 46.h,
                color: logic.isBtnEnable ? Colors.black : kAppColor('#D9D9D9'),
                onTap: () {
                  logic.doClickLogin();
                },
              );
            }),
            UIText(
              alignment: Alignment.center,
              text: '忘记密码?',
              textColor: kAppTextColor,
              fontSize: 14.sp,
              fontWeight: FontWeight.bold,
              onTap: () {
                Get.toNamed(Routes.forgotPassword);
              },
            ),
            Expanded(child: Container()),
            UIRow(
              width: double.infinity,
              margin: EdgeInsets.only(bottom: ScreenUtil().bottomBarHeight + 20.h),
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GetBuilder<LoginForPasswordLogic>(builder: (logic) {
                  return UIImage(
                    assetImage: logic.agreePrivateStatus ? 'icon_select.png' : 'icon_select_nor.png',
                    padding: EdgeInsets.only(top: 10.w,bottom: 10.w,right: 5.w,left: 15.w),
                    imageColor: !logic.agreePrivateStatus ? kAppColor('#979797') : null,
                    width: 31.w,
                    height: 31.w,
                    onTap: () {
                      logic.doClickSelect();
                    },
                  );
                }),
                RichText(
                    maxLines: 2,
                    text: TextSpan(children: [
                      TextSpan(
                        text: '已阅读并同意',
                        style: TextStyle(
                          fontSize: 10.sp,
                          color: kAppColor('#9E9E9E'),
                        ),
                      ),
                      TextSpan(
                        text: ' 用户协议、',
                        style: TextStyle(
                          fontSize: 10.sp,
                          color: kAppTextColor,
                          fontWeight: FontWeight.bold,
                          // decoration: TextDecoration.underline,
                        ),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            logic.doClickTermsOfUser();
                          },
                      ),
                      TextSpan(
                        text: ' 隐身政策',
                        style: TextStyle(
                          fontSize: 10.sp,
                          color: kAppTextColor,
                          fontWeight: FontWeight.bold,
                          // decoration: TextDecoration.underline,
                        ),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            logic.doClickPrivacyPolicy();
                          },
                      ),
                    ])),
              ],
            )
          ],
        ),
      ),
    );
  }
}
