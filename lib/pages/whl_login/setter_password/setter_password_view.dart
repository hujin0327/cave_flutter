import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../common/common_widget.dart';
import '../../../style/whl_style.dart';
import '../../../widgets/brick/widget/basic_widget.dart';
import '../../../widgets/brick/widget/text_widget.dart';
import '../../../widgets/whl_app_bar.dart';
import 'setter_password_logic.dart';

class Setter_passwordPage extends StatelessWidget {
  final logic = Get.put(Setter_passwordLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppWhiteColor,
      appBar: WhlAppBar(
          backgroundColor: kAppWhiteColor,
          centerTitle: "重设密码",
          isShowBottomLine: false),
      body: UIColumn(
        children: [
          getInputWidget("设置新密码", logic.newPasswordController),
          getInputWidget("确认密码", logic.cPasswordController),
          UIRow(
            margin: EdgeInsets.only(
                left: ScreenUtil().screenWidth - 260.w - 20.w, top: 15.h),
            children: [
              Expanded(
                  child: UIText(
                    text: "8-20位，包含字母、数字",
                    textColor: kAppSub3TextColor,
                    fontSize: 11.sp,
                  )),
            ],
          ),
          Expanded(child: Container()),
          UIText(
            margin: EdgeInsets.symmetric(horizontal: 20.w, vertical: 60.h),
            text: "确定",
            textColor: kAppWhiteColor,
            fontSize: 16.sp,
            fontWeight: FontWeight.bold,
            color: kAppBlackColor,
            height: 52.h,
            radius: 26.h,
            alignment: Alignment.center,
            onTap: () {
              logic.onConfirm();
            },
          )
        ],
      ),
    );
  }

  getInputWidget(String title, TextEditingController controller) {
    return UIRow(
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      margin: EdgeInsets.only(top: 9.h),
      children: [
        Expanded(
            child: UIText(
              text: title,
              textColor: kAppBlackColor,
              fontSize: 13.sp,
              fontWeight: FontWeight.bold,
            )),
        UIContainer(
          width: 260.w,
          height: 50.h,
          radius: 18.w,
          color: kAppColor("#F7F7F9"),
          child: noBorderCTextField(
              controller: controller,
              contentPadding:
              EdgeInsets.only(top: 15.h, bottom: 15.h, left: 15.w),
              hint: "请输入",
              obscureText: true,
              hintTextColor: kAppColor("#C1C1C1"),
              textColor: kAppSubTextColor),
        )
      ],
    );
  }
}
