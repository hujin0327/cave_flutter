import 'package:cave_flutter/utils/whl_user_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../../../network/whl_api.dart';
import '../../../network/whl_result_data.dart';
import '../../../utils/whl_toast_utils.dart';

class Setter_passwordLogic extends GetxController {

  TextEditingController newPasswordController = TextEditingController();
  TextEditingController cPasswordController = TextEditingController();

  String authCode = "";
  String phone = "";
  @override
  void onInit(){
    super.onInit();
    var a = Get.arguments;
    if (a != null) {
      phone = a["phone"];
      authCode = a["authCode"];
    }
  }

  onSavePassword() async {
    Map<String, dynamic> params = {"password": newPasswordController.text,"phone": phone,};
    ResponseData responseData = await WhlApi.saveNaPassword.post(params);
    if (responseData.isSuccess()) {
      MyToast.show("重置密码成功，请重新登录");
      WhlUserUtils.loginOut();
    }
    // Map<String,dynamic> params = {
    //   "authCode": authCode,
    //   "phone": phone,
    //   "password": newPasswordController.text
    // };
    // ResponseData responseData = await WhlApi.forgetPassword.post(params,withLoading: true);
    // if (responseData.isSuccess()){
    //   MyToast.show("重置密码成功，请重新登录");
    //   WhlUserUtils.loginOut();
    // }
  }

  void onConfirm() {
    if (newPasswordController.text.isEmpty) {
      MyToast.show("请输入新密码");
      return;
    }
    if (cPasswordController.text.isEmpty) {
      MyToast.show("请确认新密码");
      return;
    }
    if (newPasswordController.text != cPasswordController.text){
      MyToast.show("确认密码与新密码不匹配");
      return;
    }
    onSavePassword();
  }
}
