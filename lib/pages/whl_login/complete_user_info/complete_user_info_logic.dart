import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/routes/whl_app_pages.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../../../utils/whl_avatar_utils.dart';

class CompleteUserInfoLogic extends GetxController {
  TextEditingController nickNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  String? birthDay;
  String? filePath;
  String? avatarUrl;

  doClickNext() {
    if ((filePath ?? "").isEmpty) {
      MyToast.show('请选择头像');
      return;
    } else if ((avatarUrl ?? "").isEmpty) {
      MyToast.show('请等待头像上传完成');
      return;
    }
    if (nickNameController.text.isEmpty) {
      MyToast.show('请输入昵称');
      return;
    }
    if (birthDay == null) {
      MyToast.show('请选择生日');
      return;
    }
    if (passwordController.text.isEmpty) {
      MyToast.show('请输入密码');
      return;
    }
    if (passwordController.text.length < 6) {
      MyToast.show('密码长度不能小于6位');
      return;
    }
    Map<String,dynamic> param = {
      "avatar": avatarUrl,
      "birthDate": birthDay,
      "nickName": nickNameController.text,
      "password": passwordController.text
    };
    Get.toNamed(Routes.selectedSexPage,arguments: param);
    // onSavePassword();
  }

  selectPhoto() {
    AvatarUtils.onChoosePhoto(Get.context!, onSelectEnd: (path) {
      avatarUrl = null;
      filePath = path;
      update();
    }, onSuccess: (url) {
      avatarUrl = url;
      if (url.isEmpty) {
        filePath = null;
      }
      update();
    });
    update();
  }

  onSavePassword() async {
    Map<String, dynamic> params = {"password": passwordController.text};
    ResponseData responseData = await WhlApi.savePassword.post(params);
    if (responseData.isSuccess()) {
      print('保存密码成功');
    }
  }
}
