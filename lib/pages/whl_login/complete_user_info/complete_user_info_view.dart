import 'dart:io';

import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/extension/datetime_extension.dart';
import 'package:cave_flutter/utils/whl_picker_util.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../widgets/brick/brick.dart';
import 'complete_user_info_logic.dart';

class CompleteUserInfoPage extends StatelessWidget {
  CompleteUserInfoPage({Key? key}) : super(key: key);

  final logic = Get.put(CompleteUserInfoLogic());

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          children: [
            UIContainer(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
              padding: EdgeInsets.only(top: 18.h, left: 20.w, right: 20.w),
              child: const Icon(Icons.arrow_back_ios),
              onTap: () {
                Get.back();
              },
            ),
            Expanded(
              child: SingleChildScrollView(
                child: GetBuilder<CompleteUserInfoLogic>(builder: (logic) {
                  return UIColumn(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      UIText(
                        margin: EdgeInsets.only(top: 44.h, left: 30.w),
                        text: 'Hi，来完善资料吧',
                        alignment: Alignment.centerLeft,
                        textColor: kAppTextColor,
                        fontSize: 30.sp,
                        fontWeight: FontWeight.bold,
                      ),
                      UIText(
                        margin: EdgeInsets.only(top: 11.h, left: 30.w),
                        text: '认真填写，给自己选个最酷的昵称和头像',
                        textColor: kAppColor('#7E7E7E'),
                        fontSize: 15.sp,
                      ),
                      (logic.filePath ?? "").isNotEmpty
                          ? UIStack(
                              margin: EdgeInsets.only(top: 42.h, bottom: 17.h),
                              children: [
                                UIImage(
                                  width: 110.w,
                                  height: 110.w,
                                  radius: 55.w,
                                  image: Image.file(
                                    File(logic.filePath!),
                                    fit: BoxFit.cover,
                                  ),
                                  onTap: () {
                                    logic.selectPhoto();
                                  },
                                  clipBehavior: Clip.hardEdge,
                                ),
                                if ((logic.avatarUrl ?? "").isEmpty)
                                  Positioned(
                                    left: 0,
                                    right: 0,
                                    top: 0,
                                    bottom: 0,
                                    child: UIContainer(
                                      color: kAppBlackColor.withAlpha(50),
                                      radius: 55.w,
                                      alignment: Alignment.topCenter,
                                      child: Center(
                                        child: CupertinoActivityIndicator(
                                          radius: 10,
                                          color: kAppWhiteColor,
                                        ),
                                      ),
                                    ),
                                  )
                              ],
                            )
                          : UIImage(
                              margin: EdgeInsets.only(top: 42.h, bottom: 17.h),
                              width: 110.w,
                              height: 110.w,
                              assetImage: 'icon_upload_avatar.png',
                              onTap: () {
                                logic.selectPhoto();
                              },
                            ),
                      UIColumn(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        margin: EdgeInsets.symmetric(horizontal: 53.w),
                        children: [
                          UIText(
                            text: '昵称',
                            textColor: kAppTextColor,
                            fontSize: 12.sp,
                          ),
                          UIContainer(
                            margin: EdgeInsets.only(top: 9.h),
                            radius: 18.w,
                            strokeColor: kAppColor('#DDDDDD'),
                            strokeWidth: 1,
                            child: TextField(
                              inputFormatters: [LengthLimitingTextInputFormatter(20)],
                              controller: logic.nickNameController,
                              keyboardType: TextInputType.text,
                              maxLines: 1,
                              autofocus: false,
                              style: TextStyle(fontSize: 14.sp, color: kAppTextColor, fontWeight: FontWeight.bold),
                              decoration: InputDecoration(
                                  border: const OutlineInputBorder(borderSide: BorderSide.none),
                                  hintText: '输入昵称',
                                  hintStyle: TextStyle(color: kAppColor('#C1C1C1'), fontWeight: FontWeight.bold, fontSize: 14.sp),
                                  contentPadding: EdgeInsets.only(left: 20.w, top: 15.h, bottom: 15.h)),
                            ),
                          ),
                          UIText(
                            margin: EdgeInsets.only(top: 28.h),
                            text: '生日 （填写真实生日，匹配好友更合拍）',
                            textColor: kAppTextColor,
                            fontSize: 12.sp,
                          ),
                          GetBuilder<CompleteUserInfoLogic>(builder: (logic) {
                            return UIText(
                              width: double.maxFinite,
                              margin: EdgeInsets.only(top: 9.h),
                              radius: 18.w,
                              padding: EdgeInsets.only(left: 20.w, top: 15.h, bottom: 15.h),
                              strokeColor: kAppColor('#DDDDDD'),
                              strokeWidth: 1,
                              text: logic.birthDay ?? '选择生日',
                              fontSize: 14.sp,
                              fontWeight: FontWeight.bold,
                              textColor: logic.birthDay != null ? kAppTextColor : kAppColor('#C1C1C1'),
                              onTap: () {
                                WhlPickerUtil.showPickerDateTime(context, 7, (picker, selected) {
                                  DateTime givenTime = DateTime.parse(picker.adapter.text);
                                  DateTime currentTime = DateTime.now();

                                  Duration duration = currentTime.difference(givenTime);
                                  num years = duration.inDays / 365;
                                  if (years < 18) {
                                    MyToast.show('年龄不能低于18岁');
                                    return;
                                  }
                                  logic.birthDay = DateTime.parse(picker.adapter.text).toDateStringWithFormat(KDateTimeYMD);
                                  logic.update();
                                }, maxDateTime: DateTime.now());
                              },
                            );
                          }),
                          UIText(
                            margin: EdgeInsets.only(top: 28.h),
                            text: '设置密码',
                            textColor: kAppTextColor,
                            fontSize: 12.sp,
                          ),
                          UIContainer(
                            margin: EdgeInsets.only(top: 9.h),
                            radius: 18.w,
                            strokeColor: kAppColor('#DDDDDD'),
                            strokeWidth: 1,
                            child: TextField(
                              inputFormatters: [LengthLimitingTextInputFormatter(16)],
                              controller: logic.passwordController,
                              keyboardType: TextInputType.visiblePassword,
                              maxLines: 1,
                              obscureText: true,
                              style: TextStyle(fontSize: 14.sp, color: kAppTextColor, fontWeight: FontWeight.bold),
                              decoration: InputDecoration(
                                  border: const OutlineInputBorder(borderSide: BorderSide.none),
                                  hintText: '输入登录密码',
                                  hintStyle: TextStyle(color: kAppColor('#C1C1C1'), fontWeight: FontWeight.bold, fontSize: 14.sp),
                                  contentPadding: EdgeInsets.only(left: 20.w, top: 15.h, bottom: 15.h)),
                            ),
                          ),
                        ],
                      ),
                      UIRow(
                        margin: EdgeInsets.only(top: 53.h, right: 45.w),
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          UIText(
                            radius: 50.w,
                            padding: EdgeInsets.only(left: 26.w, right: 22.w, top: 9.h, bottom: 9.h),
                            color: kAppTextColor,
                            text: '1/4 ',
                            fontSize: 16.sp,
                            textColor: Colors.white,
                            endDrawable: UIImage(
                              assetImage: 'icon_complete_info_arrow_right.png',
                              height: 14.h,
                            ),
                            onTap: () {
                              logic.doClickNext();
                            },
                          ),
                        ],
                      )
                    ],
                  );
                }),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
