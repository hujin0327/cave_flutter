import 'package:cave_flutter/model/tag_model.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/brick/widget/basic_widget.dart';
import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../routes/whl_app_pages.dart';
import '../../../../style/whl_style.dart';
import '../../../../widgets/brick/widget/text_widget.dart';
import 'improve_information_logic.dart';

class ImproveInformationPage extends StatelessWidget {
  ImproveInformationPage({Key? key}) : super(key: key);

  final logic = Get.put(ImproveInformationLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          children: [
            Column(
              children: [
                UIContainer(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
                  padding: EdgeInsets.only(top: 18.h, left: 20.w, right: 20.w),
                  child: const Icon(Icons.arrow_back_ios),
                  onTap: () {
                    Get.back();
                  },
                ),
                UIText(
                  margin: EdgeInsets.only(top: 18.h, left: 23.w),
                  text: '编辑你的灵魂代码吧',
                  alignment: Alignment.centerLeft,
                  textColor: kAppTextColor,
                  fontSize: 30.sp,
                  fontWeight: FontWeight.bold,
                ),
                UIText(
                  margin: EdgeInsets.only(top: 11.h, left: 23.w),
                  text: '这是开启灵魂代码的必要条件',
                  alignment: Alignment.centerLeft,
                  textColor: kAppColor('#7E7E7E'),
                  fontSize: 15.sp,
                ),
                GetBuilder<ImproveInformationLogic>(builder: (logic) {
                  return Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(left: 23.w, top: 25.h),
                    height: 32.h,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        bool isSelected =
                            logic.selectedFir?.id == logic.tagList[index].id;
                        return UIText(
                          padding: EdgeInsets.symmetric(horizontal: 19.w),
                          text: logic.tagList[index].label,
                          fontSize: 13.sp,
                          textColor: isSelected ? Colors.white : kAppTextColor,
                          color: isSelected ? Colors.black : Colors.white,
                          margin: EdgeInsets.only(right: 5.w),
                          height: 32.h,
                          radius: 16.h,
                          strokeWidth: 1.w,
                          alignment: Alignment.center,
                          strokeColor:
                              isSelected ? kAppTextColor : kAppColor('#D9D9D9'),
                          onTap: () {
                            logic.selectedFir = logic.tagList[index];
                            logic.update();
                          },
                        );
                      },
                      itemCount: logic.tagList.length,
                    ),
                  );
                }),
                GetBuilder<ImproveInformationLogic>(builder: (logic) {
                  if (logic.selectedFir == null) {
                    return SizedBox();
                  }
                  return Container(
                    margin: EdgeInsets.only(left: 23.w, top: 11.h),
                    height: 32.h,
                    alignment: Alignment.centerLeft,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        TagModel current = logic.selectedFir!.child![index];
                        bool isSelected = logic.selectedFir!.child![index].id ==
                            logic.selectedSec?.id;
                        return UIText(
                          padding: EdgeInsets.symmetric(
                            horizontal: 19.w,
                          ),
                          textStyleHeight: 1.8.h,
                          height: 32.h,
                          textAlign: TextAlign.center,
                          text: logic.selectedFir!.child![index].label,
                          fontSize: 13.sp,
                          textColor: kAppTextColor,
                          color:
                              isSelected ? kAppColor('#F0F0F0') : Colors.white,
                          margin: EdgeInsets.only(right: 5.w),
                          radius: 16.h,
                          strokeWidth: 1.w,
                          strokeColor: isSelected
                              ? kAppColor('#F0F0F0')
                              : kAppColor('#D9D9D9'),
                          onTap: () {
                            logic.selectedSec = current;
                            logic.update();
                          },
                        );
                      },
                      itemCount: logic.selectedFir!.child!.length,
                    ),
                  );
                }),
              ],
            ),
            Expanded(
              child: Stack(
                children: [
                  GetBuilder<ImproveInformationLogic>(builder: (logic) {
                    if (logic.selectedSec == null) {
                      return SizedBox();
                    }
                    // if (logic.selectedSec == null ||
                    //     logic.selectedSec!.child == null ||
                    //     logic.selectedSec!.child!.isEmpty) {
                    //   return const Center(
                    //     child: UIText(
                    //       text: '无可选择的灵魂代码',
                    //     ),
                    //   );
                    // }
                    return CustomScrollView(
                      slivers: [
                        SliverList(
                          delegate: SliverChildBuilderDelegate(
                            (context, index) {
                              TagModel tagModel =
                                  logic.selectedSec!.child![index];
                              bool hasSelected =
                                  logic.myTags.any((c) => c.id == tagModel.id);
                              return UIRow(
                                margin: EdgeInsets.only(
                                    top: 20.h, left: 22.w, right: 20.w),
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  UIText(
                                    text: tagModel.label,
                                    textColor: kAppTextColor,
                                    fontSize: 15.sp,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  UIImage(
                                    assetImage: hasSelected
                                        ? 'icon_add.png'
                                        : 'icon_add_none.png',
                                    width: 20.w,
                                    onTap: () {
                                      logic.addTag(tagModel);
                                    },
                                  )
                                ],
                              );
                            },
                            childCount: logic.selectedSec!.child!.length,
                          ),
                        ),
                        SliverToBoxAdapter(
                          child: SizedBox(
                            height: 43.h,
                          ),
                        )
                      ],
                    );
                  }),
                  Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: UIContainer(
                      gradientStartColor: Colors.white.withOpacity(0.69),
                      gradientEndColor: Colors.white,
                      gradientBegin: Alignment.topCenter,
                      gradientEnd: Alignment.bottomCenter,
                      height: 43.h,
                      color: kAppColor('#F0F0F0'),
                    ),
                  )
                ],
              ),
            ),
            GetBuilder<ImproveInformationLogic>(builder: (logic) {
              return UISolidButton(
                margin: EdgeInsets.only(
                    bottom: 90.h, top: 8.h, left: 38.w, right: 38.w),
                text: logic.isSetter ? "确认" : '开始进入你的Cave吧',
                color: Colors.black,
                onTap: () {
                  logic.confirmAction();
                },
              );
            })
          ],
        ));
  }
}
