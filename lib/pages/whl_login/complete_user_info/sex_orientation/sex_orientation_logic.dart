import 'package:get/get.dart';

import '../../../../network/whl_api.dart';
import '../../../../routes/whl_app_pages.dart';
import '../selected_sex/selected_sex_logic.dart';
import 'fun_role_sheet/fun_role_sheet_view.dart';

class SexOrientationLogic extends GetxController {
  List<SexModel> firstSexList = [
    // SexModel(name: '主流', isSelected: true, describe: '异性恋',value: 0),
    // SexModel(name: 'Les', children: [
    //   SexModel(name: 'T', describe: 'T: 一般指打扮比较男性化的女生', isSelected: true,value: 1),
    //   SexModel(name: 'F', describe: 'F: 一般指打扮比较女性化的女生',value: 2),
    //   SexModel(name: 'H', describe: 'H: 两者都有，或者中性打扮的女生',value: 3),
    // ], describe: 'T: 一般指打扮比较男性化的女生', value: 101
    // ),
    // SexModel(name: 'Gay', children: [
    //   SexModel(name: 'BTM', describe: 'BTM: 在男同性恋中，代表受', isSelected: true,value: 4),
    //   SexModel(name: 'TP', describe: 'TP: 代表可攻可受，即可以扮演攻或受的角色。',value: 5),
    //   SexModel(name: 'Vers', describe: 'Vers: 在男同性恋中，代表攻。',value: 6),
    // ],describe:'BTM: 在男同性恋中，代表受',value: 102),
  ];
  List<SexModel> sexOrientationList = [
  //   SexModel(name: '双性恋', describe: '对男女均可产生情感和性吸引',value: 7),
  //   SexModel(name: '泛性恋', describe: '对任何性别、性取向、属性都能产生爱慕和性吸引',value: 8),
  //   SexModel(name: '智性恋', describe: '是以智商标准作为恋爱的取向，会受到他人高智商影响而产生性吸引',value: 9),
  //   SexModel(name: '颜性恋', describe: '指不论对方性别，只要对方颜值高，就能发展爱情关系的人',value: 10),
  //   SexModel(name: '性单恋', describe: '是指感受到浪漫爱情但不希望得到对方回应的一种浪漫倾向，当自己爱慕的对象同样对自己产生浪漫或爱情时，性单恋者反而会感到不适，开始疏远对方，他们并不希望与对方建立关系',value: 11),
  //   SexModel(name: '半性恋', describe: '只有跟很亲密的人才能产生的情感',value: 12),
  //   SexModel(name: '无性恋', describe: '是指一些不具有性欲望或者宣称自己没有性取向的人，即不会对男性或女性任一性别表现出性爱慕，即缺乏性冲动',value: 13),
  //   SexModel(name: 'Four Love', describe: '生理性别与社会性别互为颠倒的异性之间所发生的爱恋关系，即：一方生理性别为男，社会性别为女，性向为女。另一方生理性别为女，社会性别为男，性向为男。两方之间所发生的爱恋关系就叫作第四爱。',value: 14),
  ];
  bool isShowSwitch = true;
  bool openSocialIsolation = false;
  bool hasInteresting = true;
  SexModel? selectedModel;
  SexModel? selectedRole;
  int showSubType = 0;

  doClickItem(SexModel model) {
    sexOrientationList.forEach((element) {
      element.isSelected = false;
    });
    model.isSelected = true;
    selectedModel = model;
    showSubType = 0;
    if (model.children.isNotEmpty) {
      showSubType = firstSexList.indexOf(model);
    }
    if (selectedRole == null && model.children.isEmpty && openSocialIsolation){
      openSocialIsolation = false;
      isShowSwitch = false;
      update();
      Future.delayed(Duration(milliseconds: 200))
          .then((value) {
        isShowSwitch = true;
        update();
      });
    }else {
      update();
    }
  }

  changeOpenSocialIsolation(){
    if (selectedRole == null && (selectedModel?.children??[]).isEmpty && openSocialIsolation){
      openSocialIsolation = false;
      isShowSwitch = false;
      update();
      Future.delayed(Duration(milliseconds: 200))
          .then((value) {
        isShowSwitch = true;
        update();
      });
    }
  }

  doClickSubItem(SexModel model) {
    selectedModel?.children.forEach((element) {
      element.isSelected = false;
    });
    model.isSelected = true;
    selectedModel?.describe = model.describe;
    update();
  }

  doClickNext() {
    String sexOValue = selectedModel?.value;
    if ((selectedModel?.children??[]).isNotEmpty) {
      selectedModel?.children.forEach((element) {
        if (element.isSelected) {
          sexOValue = element.value;
        }
      });
    }
    Map<String,dynamic> param = {
      "sexParam":{
        "sexual_orientation":sexOValue,
        "fun_role":selectedRole?.value,
        "open_social_isolation":openSocialIsolation
      },
      "userInfo":Get.arguments
    };
    Get.toNamed(Routes.improveInformation,arguments: param);
  }

  @override
  void onInit() {
    super.onInit();

    update();

    getSexQXData();
  }

  getSexQXData(){
    WhlApi.getSexQXList.get({}).then((value) {
      List data = value.data;
      for (var e in data) {
        SexModel model = SexModel(name: e["lable"],value: e["id"],describe: e["note"]);
        List child = e["child"]??[];
        if (child.isNotEmpty) {
          model.children = child.map((subE) {
            SexModel subModel = SexModel(name: subE["lable"],value: subE["id"],describe: subE["note"]);
            if (subE == child.first) {
              subModel.isSelected = true;
            }
            return subModel;
          }).toList();
        }
        if (model.name == "主流"||model.value == "0accb046ff274ebf810722d393ea602c") {
          firstSexList.insert(0, model);
        }else if (model.children.isNotEmpty) {
          firstSexList.add(model);
        }else {
          sexOrientationList.add(model);
        }
      }
      if (firstSexList.isNotEmpty) {
        selectedModel = firstSexList[0];
      }
      update();
    });
  }


  showInterestingSheetView(){
    Get.bottomSheet(FunRoleSheetView(), isScrollControlled: true).then((value) {
      if (value != null) {
        print("1 --- $value");
        selectedRole = value;
      } else {
        print("2 --- $value");
        selectedRole = null;
        hasInteresting = false;
        update();
        Future.delayed(Duration(milliseconds: 200))
            .then((value) {
          hasInteresting = true;
          update();
        });
      }
    });
  }
}
