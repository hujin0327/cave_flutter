import 'package:get/get.dart';

import '../../../../../network/whl_api.dart';
import '../../selected_sex/selected_sex_logic.dart';

class FunRoleSheetLogic extends GetxController {
  List<SexModel> sexList = [
    // SexModel(name: 'Sado', describe: '扮演强势、霸道的角色',value: 0),
    // SexModel(name: 'Maso', describe: '扮演柔弱、被动的角色',value: 1),
    // SexModel(name: 'Dom', describe: '扮演支配者的角色',value: 2),
    // SexModel(name: 'Sub', describe: '扮演臣服者的角色',value: 3),
    // SexModel(name: 'Switch', describe: '双属性（与双性恋不同），它指的是即可扮演Sado/Dom，也可以扮演Maso/Sub',value: 4),
    // SexModel(name: 'CD', describe: '男扮女装，指男生对物品的一种特殊喜好（与性别认知障碍不同）',value: 5),
  ];
  int selectedSex = 0;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();

    getQQJSData();
  }

  getQQJSData(){
    WhlApi.getQQJSList.get({}).then((value) {
      List data = value.data;
      sexList = data.map((e) {
        SexModel model = SexModel(name: e["lable"],value: e["id"],describe: e["note"]);
        return model;
      }).toList();
      update();
    });
  }
}
