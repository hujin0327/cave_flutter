import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../../style/whl_style.dart';
import '../../../../../widgets/brick/brick.dart';
import '../../selected_sex/selected_sex_logic.dart';
import 'fun_role_sheet_logic.dart';

class FunRoleSheetView extends StatefulWidget {
  const FunRoleSheetView({Key? key}) : super(key: key);

  @override
  State<FunRoleSheetView> createState() => _FunRoleSheetViewState();
}

class _FunRoleSheetViewState extends State<FunRoleSheetView> {
  final logic = Get.put(FunRoleSheetLogic());

  @override
  void dispose() {
    super.dispose();
    Get.delete<FunRoleSheetLogic>();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<FunRoleSheetLogic>(
        builder: (logic) {
          return UIColumn(
            topRightRadius: 20.w,
            topLeftRadius: 20.w,
            height: 700.h,
            color: Colors.white,
            width: double.maxFinite,
            children: [
              Expanded(
                child: CustomScrollView(
                  physics: const NeverScrollableScrollPhysics(),
                  slivers: [
                    SliverToBoxAdapter(
                      child: Column(
                        children: [
                          UIRow(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            margin: EdgeInsets.only(top: 20.h,
                                left: 20.w,
                                right: 20.w,
                                bottom: 22.h),
                            children: [
                              UIText(
                                text: '取消',
                                textColor: kAppColor('#C1C1C1'),
                                fontSize: 15.sp,
                                onTap: () {
                                  Get.back();
                                },
                              ),
                              UIText(
                                text: '确认',
                                textColor: kAppTextColor,
                                fontSize: 15.sp,
                                fontWeight: FontWeight.bold,
                                onTap: () {
                                  Get.back(
                                      result: logic.sexList[logic.selectedSex]);
                                },
                              ),
                            ],
                          ),
                          UIText(
                            text: '小众性别在社会通常会受到外界歧视 我们应该尊重、包容弱势多元群体💗',
                            textColor: kAppColor('#979797'),
                            fontSize: 14.sp,
                            width: 230.w,
                            maxLines: 2,
                            shrinkWrap: false,
                          ),
                        ],
                      ),
                    ),
                    SliverPadding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 25.w, vertical: 39.h),
                      sliver: SliverGrid(
                          delegate: SliverChildBuilderDelegate((context,
                              index) {
                            SexModel model = logic.sexList[index];
                            return buildDottedBorderView(model);
                          }, childCount: logic.sexList.length),
                          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 3, childAspectRatio: 1)),
                    ),
                    if (logic.sexList.isNotEmpty)
                      SliverToBoxAdapter(
                        child: GetBuilder<FunRoleSheetLogic>(builder: (logic) {
                          SexModel selectedModel = logic.sexList[logic
                              .selectedSex];
                          return Column(
                            children: [
                              UIText(
                                text: '什么是${selectedModel.name}?',
                                textColor: kAppTextColor,
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w500,
                                marginDrawable: 3.w,
                                startDrawable: UIImage(
                                  assetImage: 'icon_sex_tips.png',
                                  width: 24.w,
                                ),
                              ),
                              UIText(
                                margin: EdgeInsets.only(
                                    top: 6.h, left: 46.w, right: 46.w),
                                text: selectedModel.describe,
                                textColor: kAppColor('#666666'),
                                fontSize: 13.sp,
                                shrinkWrap: false,
                                textAlign: TextAlign.center,
                              )
                            ],
                          );
                        }),
                      )
                  ],
                ),
              ),
            ],
          );
        });
  }

  buildDottedBorderView(SexModel sexModel) {
    return GetBuilder<FunRoleSheetLogic>(builder: (logic) {
      bool isSelected = logic.sexList.indexOf(sexModel) == logic.selectedSex;
      return UIColumn(
        children: [
          Stack(
            children: [
              if (isSelected)
                GetBuilder<SelectedSexLogic>(builder: (logic) {
                  return RotationTransition(
                    turns: logic.animationController,
                    child: DottedBorder(
                      padding: EdgeInsets.zero,
                      borderType: BorderType.Circle,
                      color: Colors.black,
                      radius: const Radius.circular(5000),
                      strokeWidth: 2,
                      child: SizedBox(
                        width: 88.w,
                        height: 88.w,
                      ),
                    ),
                  );
                }),
              UIText(
                width: 88.w,
                height: 88.w,
                alignment: Alignment.center,
                text: sexModel.name,
                textColor: kAppTextColor,
                fontSize: 16.sp,
                fontWeight: FontWeight.bold,
                strokeWidth: 1.w,
                strokeColor: kAppColor('#DCDCDC'),
                radius: 5000,
              ),
            ],
          ),
        ],
        onTap: () {
          logic.selectedSex = logic.sexList.indexOf(sexModel);
          logic.update();
        },
      );
    });
  }
}
