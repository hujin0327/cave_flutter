import 'package:cave_flutter/pages/whl_login/complete_user_info/selected_sex/selected_sex_logic.dart';
import 'package:cave_flutter/pages/whl_login/complete_user_info/sex_orientation/view/social_isolation_dialog.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:cave_flutter/widgets/whl_switch_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../style/whl_style.dart';
import '../../../../widgets/brick/brick.dart';
import 'fun_role_sheet/fun_role_sheet_view.dart';
import 'sex_orientation_logic.dart';

class SexOrientationPage extends StatelessWidget {
  SexOrientationPage({Key? key}) : super(key: key);

  final logic = Get.put(SexOrientationLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: GetBuilder<SexOrientationLogic>(builder: (logic) {
          return CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: Column(
                  children: [
                    UIContainer(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.only(
                          top: ScreenUtil().statusBarHeight),
                      padding:
                      EdgeInsets.only(top: 18.h, left: 20.w, right: 20.w),
                      child: const Icon(Icons.arrow_back_ios),
                      onTap: () {
                        Get.back();
                      },
                    ),
                    UIText(
                      margin: EdgeInsets.only(top: 18.h, left: 30.w),
                      text: 'So，你的性取向是？',
                      alignment: Alignment.centerLeft,
                      textColor: kAppTextColor,
                      fontSize: 30.sp,
                      fontWeight: FontWeight.bold,
                    ),
                    UIText(
                      margin: EdgeInsets.only(top: 11.h, left: 30.w),
                      text: '这是开启灵魂代码的必要条件',
                      alignment: Alignment.centerLeft,
                      textColor: kAppColor('#7E7E7E'),
                      fontSize: 15.sp,
                    ),
                  ],
                ),
              ),
              SliverToBoxAdapter(
                child: GetBuilder<SexOrientationLogic>(builder: (logic) {
                  return UIColumn(
                    children: [
                      UIRow(
                        margin: EdgeInsets.only(
                            left: 43.w,
                            right: 43.w,
                            top: 43.w,
                            bottom: logic.showSubType > 0 ? 4.w : 30.w),
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: logic.firstSexList.map((model) {
                          return UIText(
                            text: model.name,
                            textColor: logic.selectedModel == model
                                ? Colors.white
                                : kAppTextColor,
                            alignment: Alignment.center,
                            fontSize: 13.sp,
                            fontWeight: FontWeight.bold,
                            width: 80.w,
                            height: 35.w,
                            radius: 35.w,
                            color: logic.selectedModel == model
                                ? Colors.black
                                : Colors.white,
                            strokeWidth: 1.w,
                            strokeColor: logic.selectedModel == model
                                ? Colors.black
                                : kAppColor('#DCDCDC'),
                            onTap: () {
                              logic.doClickItem(model);
                            },
                          );
                        }).toList(),
                      ),
                      if (logic.showSubType > 0)
                        Container(
                          margin: EdgeInsets.only(bottom: 15.w,
                              left: logic.showSubType == 2 ? 150.w : 0),
                          width: 155.w,
                          child: UIStack(
                            alignment: Alignment.center,
                            children: [
                              UIImage(
                                width: 155.w,
                                height: 48.w,
                                assetImage: "login_sexOBg",
                              ),
                              UIRow(
                                padding: EdgeInsets.symmetric(horizontal: 10.w),
                                margin: EdgeInsets.only(top: 8.w),
                                children: (logic.selectedModel?.children ?? [])
                                    .map((subModel) {
                                  return UIText(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 5.w),
                                    text: subModel.name,
                                    textColor: subModel.isSelected
                                        ? Colors.white
                                        : kAppTextColor,
                                    alignment: Alignment.center,
                                    fontSize: 10.sp,
                                    width: 35.w,
                                    height: 18.w,
                                    radius: 9.w,
                                    color: subModel.isSelected
                                        ? Colors.black
                                        : Colors.white,
                                    onTap: () {
                                      logic.doClickSubItem(subModel);
                                    },
                                  );
                                }).toList(),
                              )
                            ],
                          ),
                        )
                    ],
                  );
                }),
              ),
              SliverPadding(
                padding: EdgeInsets.symmetric(horizontal: 43.w),
                sliver: SliverGrid(
                    delegate: SliverChildBuilderDelegate((context, index) {
                      SexModel model = logic.sexOrientationList[index];
                      return GetBuilder<SexOrientationLogic>(builder: (logic) {
                        return UIText(
                          text: model.name,
                          textColor: logic.selectedModel == model
                              ? Colors.white
                              : kAppTextColor,
                          alignment: Alignment.center,
                          fontSize: 13.sp,
                          fontWeight: FontWeight.bold,
                          width: 80.w,
                          height: 35.w,
                          radius: 35.w,
                          color: logic.selectedModel == model
                              ? Colors.black
                              : Colors.white,
                          strokeWidth: 1.w,
                          strokeColor: logic.selectedModel == model
                              ? Colors.black
                              : kAppColor('#DCDCDC'),
                          onTap: () {
                            logic.doClickItem(model);
                          },
                        );
                      });
                    }, childCount: logic.sexOrientationList.length),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                        crossAxisSpacing: 24.w,
                        mainAxisSpacing: 30.w,
                        childAspectRatio: 80 / 35)),
              ),
              SliverToBoxAdapter(
                child: GetBuilder<SexOrientationLogic>(builder: (logic) {
                  return UIRow(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    margin: EdgeInsets.only(left: 27.w, right: 36.w, top: 22.w),
                    children: [
                      UIImage(
                        assetImage: 'icon_sex_tips.png',
                        width: 24.w,
                      ),
                      Expanded(
                        child: UIText(
                          margin: EdgeInsets.only(left: 6.w),
                          text: logic.selectedModel?.describe ?? '',
                          textColor: kAppColor('#C1C1C1'),
                          fontSize: 12.sp,
                          shrinkWrap: false,
                        ),
                      ),
                    ],
                  );
                }),
              ),
              SliverToBoxAdapter(
                child: UIColumn(
                  margin: EdgeInsets.only(top: 35.h),
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        UIText(
                          width: 140.w,
                          text: '是否有情趣角色喜好',
                          textColor: kAppTextColor,
                          fontSize: 12.sp,
                          fontWeight: FontWeight.bold,
                          startDrawable: UIContainer(
                            margin: EdgeInsets.only(right: 4.w),
                            color: kAppColor('#FFF754'),
                            width: 7.w,
                            height: 7.w,
                            radius: 7.w,
                            strokeColor: kAppTextColor,
                            strokeWidth: 1.w,
                          ),
                        ),
                        GetBuilder<SexOrientationLogic>(builder: (logic) {
                          return Visibility(
                              visible: logic.hasInteresting,
                              child: WhlSwitch(
                                value: false,
                                color: Colors.white,
                                openBgColor: kAppTextColor,
                                openBorderColor: kAppTextColor,
                                openColor: Colors.white,
                                bgColor: kAppColor('#D9D9D9'),
                                onChanged: (value) {
                                  if (value) {
                                    logic.showInterestingSheetView();
                                  } else {
                                    logic.selectedRole = null;
                                    print("23456");
                                    logic.changeOpenSocialIsolation();
                                  }
                                },
                              ));
                        }),
                      ],
                    ),
                    SizedBox(height: 26.h),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        UIText(
                          width: 140.w,
                          text: '是否打开社交隔离系统',
                          textColor: kAppTextColor,
                          fontSize: 12.sp,
                          fontWeight: FontWeight.bold,
                          startDrawable: UIContainer(
                            margin: EdgeInsets.only(right: 4.w),
                            color: kAppColor('#FFF754'),
                            width: 7.w,
                            height: 7.w,
                            radius: 7.w,
                            strokeColor: kAppTextColor,
                            strokeWidth: 1.w,
                          ),
                        ),
                        GetBuilder<SexOrientationLogic>(builder: (logic) {
                          return Visibility(
                            visible: logic.isShowSwitch,
                            child: WhlSwitch(
                              value: false,
                              color: Colors.white,
                              openBgColor: kAppTextColor,
                              openColor: Colors.white,
                              openBorderColor: kAppTextColor,
                              bgColor: kAppColor('#D9D9D9'),
                              onChanged: (value) {
                                if (value) {
                                  if (logic.selectedRole == null &&
                                      (logic.selectedModel?.children??[]).isEmpty) {
                                    logic.openSocialIsolation = false;
                                    logic.isShowSwitch = false;
                                    logic.update();
                                    Future.delayed(Duration(milliseconds: 200))
                                        .then((value) {
                                      logic.isShowSwitch = true;
                                      logic.update();
                                    });
                                    MyToast.show(
                                        "有情趣角色喜好用户才能打开社交隔离系统");
                                    return;
                                  }
                                  Get.dialog(SocialIsolationDialog(
                                    onTagAgree: () {
                                      logic.openSocialIsolation = true;
                                    },
                                    onTapReject: () {
                                      logic.openSocialIsolation = false;
                                      logic.isShowSwitch = false;
                                      logic.update();
                                      Future.delayed(
                                          Duration(milliseconds: 200))
                                          .then((value) {
                                        logic.isShowSwitch = true;
                                        logic.update();
                                      });
                                    },
                                  ));
                                } else {
                                  logic.openSocialIsolation = false;
                                }
                              },
                            ),
                          );
                        }),
                      ],
                    ),
                    UIRow(
                      margin: EdgeInsets.only(top: 42.h, right: 45.w),
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        UIText(
                          radius: 50.w,
                          padding: EdgeInsets.only(
                              left: 26.w, right: 22.w, top: 9.h, bottom: 9.h),
                          color: kAppTextColor,
                          text: '3/4 ',
                          fontSize: 16.sp,
                          textColor: Colors.white,
                          endDrawable: UIImage(
                            assetImage: 'icon_complete_info_arrow_right.png',
                            height: 14.h,
                          ),
                          onTap: () {
                            logic.doClickNext();
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              )
            ],
          );
        }));
  }
}
