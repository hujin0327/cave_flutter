import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../../style/whl_style.dart';
import '../../../../../widgets/brick/brick.dart';
import '../../../../../widgets/whl_button.dart';

class SocialIsolationDialog extends StatelessWidget {
  const SocialIsolationDialog({super.key, required this.onTagAgree, required this.onTapReject});

  final Function() onTagAgree;
  final Function() onTapReject;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(
        child: Wrap(
          children: [
            UIColumn(
              width: double.maxFinite,
              radius: 30.w,
              margin: EdgeInsets.symmetric(horizontal: 34.w),
              gradientStartColor: kAppColor('#FFFEF1'),
              gradientEndColor: Colors.white,
              gradientEnd: Alignment.bottomCenter,
              gradientBegin: Alignment.topCenter,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                UIText(
                  text: '友情提醒',
                  fontSize: 21.sp,
                  textColor: kAppTextColor,
                  alignment: Alignment.center,
                  fontWeight: FontWeight.bold,
                  margin: EdgeInsets.only(top: 22.h, bottom: 15.h),
                ),
                UIText(
                  margin: EdgeInsets.symmetric(horizontal: 23.w),
                  text: '打开社交隔离系统，您将只会出现在相同情感社区动态、附近的人里只能相同取向才可看到～',
                  textColor: kAppTextColor,
                  fontSize: 13.5.sp,
                  textAlign: TextAlign.center,
                  fontWeight: FontWeight.w500,
                  shrinkWrap: false,
                ),
                UIText(
                  margin: EdgeInsets.only(left: 23.w, top: 16.h),
                  text: '优点：可不被不必要的人打扰，沉浸在自己的社交圈子里',
                  textColor: kAppColor('#7E7E7E'),
                  fontSize: 11.sp,
                  shrinkWrap: false,
                  maxLines: 2,
                  startDrawable: UIContainer(
                    margin: EdgeInsets.only(right: 4.w),
                    color: kAppColor('#FFF754'),
                    width: 7.w,
                    height: 7.w,
                    radius: 7.w,
                    strokeColor: kAppTextColor,
                    strokeWidth: 1.w,
                  ),
                ),
                UIText(
                  margin: EdgeInsets.only(left: 23.w),
                  text: '缺点：可能会降低您的社交概率',
                  textColor: kAppColor('#7E7E7E'),
                  fontSize: 11.sp,
                  startDrawable: UIContainer(
                    margin: EdgeInsets.only(right: 4.w),
                    color: kAppColor('#FFF754'),
                    width: 7.w,
                    height: 7.w,
                    radius: 7.w,
                    strokeColor: kAppTextColor,
                    strokeWidth: 1.w,
                  ),
                ),
                UIRow(
                  margin: EdgeInsets.symmetric(horizontal: 30.w, vertical: 19.h),
                  children: [
                    Expanded(
                      child: UISolidButton(
                        height: 42.h,
                        text: '我再想想',
                        textColor: kAppColor('#979797'),
                        color: kAppColor('#F1F0F5'),
                        onTap: () {
                          Get.back();
                          onTapReject();
                        },
                      ),
                    ),
                    SizedBox(width: 30.w),
                    Expanded(
                      child: UISolidButton(
                        height: 42.h,
                        text: '确认打开',
                        color: Colors.black,
                        onTap: () {
                          Get.back();
                          onTagAgree();
                        },
                      ),
                    ),
                  ],
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
