import 'package:cave_flutter/config/whl_global_config.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../widgets/brick/brick.dart';
import 'selected_sex_logic.dart';

class SelectedSexPage extends StatelessWidget {
  SelectedSexPage({Key? key}) : super(key: key);

  final logic = Get.put(SelectedSexLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: GetBuilder<SelectedSexLogic>(builder: (logic) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              UIContainer(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
                padding: EdgeInsets.only(top: 18.h, left: 20.w, right: 20.w),
                child: const Icon(Icons.arrow_back_ios),
                onTap: () {
                  Get.back();
                },
              ),
              UIText(
                margin: EdgeInsets.only(top: 18.h, left: 30.w),
                text: '你的性别是？',
                alignment: Alignment.centerLeft,
                textColor: kAppTextColor,
                fontSize: 30.sp,
                fontWeight: FontWeight.bold,
              ),
              UIText(
                margin: EdgeInsets.only(top: 11.h, left: 30.w),
                text: '这是开启灵魂代码的必要条件',
                alignment: Alignment.centerLeft,
                textColor: kAppColor('#7E7E7E'),
                fontSize: 15.sp,
              ),
              if (logic.sexList.isNotEmpty)
                UIRow(
                  margin: EdgeInsets.only(top: 42.h),
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    buildDottedBorderView(logic.sexList[0]),
                    SizedBox(width: 40.w),
                    buildDottedBorderView(logic.sexList[1]),
                  ],
                ),
              SizedBox(height: 22.h),
              if (logic.sexList.isNotEmpty)
              buildDottedBorderView(logic.sexList[2]),
              UIRow(
                margin: EdgeInsets.only(top: 130.h, right: 45.w),
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  UIText(
                    radius: 50.w,
                    padding: EdgeInsets.only(
                        left: 26.w, right: 22.w, top: 9.h, bottom: 9.h),
                    color: kAppTextColor,
                    text: '2/4 ',
                    fontSize: 16.sp,
                    textColor: Colors.white,
                    endDrawable: UIImage(
                      assetImage: 'icon_complete_info_arrow_right.png',
                      height: 14.h,
                    ),
                    onTap: () {
                      logic.doClickNext();
                    },
                  ),
                ],
              ),
            ],
          );
        }));
  }

  buildDottedBorderView(SexModel sexModel) {
    return GetBuilder<SelectedSexLogic>(builder: (logic) {
      bool isSelected = logic.sexList.indexOf(sexModel) == logic.selectedIndex;
      return UIColumn(
        children: [
          Stack(
            children: [
              UIContainer(
                width: 122.w,
                height: 122.w,
                radius: 5000,
                strokeColor: kAppColor('#DCDCDC'),
                strokeWidth: 1.w,
                child: Center(
                  child: UIImage(
                    httpImage: (isSelected ? sexModel.selectedIcon : sexModel
                        .defaultIcon)?.toImageUrl(),
                    assetPlaceHolder: WHLGlobalConfig.userPlaceholderAvatar,
                    height: 55.h,
                  ),
                ),
              ),
              if (isSelected)
                RotationTransition(
                  turns: logic.animationController,
                  child: DottedBorder(
                    padding: EdgeInsets.zero,
                    borderType: BorderType.Circle,
                    color: Colors.black,
                    radius: const Radius.circular(5000),
                    strokeWidth: 2,
                    child: SizedBox(
                      width: 122.w,
                      height: 122.w,
                    ),
                  ),
                )
            ],
          ),
          UIText(
            margin: EdgeInsets.only(top: 22.h),
            text: sexModel.name,
            textColor: isSelected ? kAppTextColor : kAppColor('#C1C1C1'),
            fontSize: 16.sp,
            fontWeight: isSelected ? FontWeight.bold : FontWeight.normal,
          )
        ],
        onTap: () {
          logic.selectedIndex = logic.sexList.indexOf(sexModel);
          logic.update();
        },
      );
    });
  }
}
