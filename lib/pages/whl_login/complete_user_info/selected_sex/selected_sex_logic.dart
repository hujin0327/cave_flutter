import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/pages/whl_login/complete_user_info/selected_sex/selected_lgbtq/selected_lgbtq_view.dart';
import 'package:cave_flutter/routes/whl_app_pages.dart';
import 'package:flutter/animation.dart';
import 'package:get/get.dart';

class SexModel {
  String? name;
  String? describe;
  String? defaultIcon;
  String? selectedIcon;
  bool isSelected = false;
  var value;
  List<SexModel> children;

  SexModel(
      {this.name,
      this.describe,
      this.defaultIcon,
      this.selectedIcon,
      this.isSelected = false,
      this.value,
      this.children = const []});
}

class SelectedSexLogic extends GetxController
    with GetSingleTickerProviderStateMixin {
  int selectedIndex = 0;
  late AnimationController animationController;
  late Animation<double> animation;
  List<SexModel> sexList = [];
  // [
  //   SexModel(defaultIcon: 'icon_sex_man_0.png', selectedIcon: 'icon_sex_man_1.png', name: '男生',value: 0),
  //   SexModel(defaultIcon: 'icon_sex_woman_0.png', selectedIcon: 'icon_sex_woman_1.png', name: '女生',value: 1),
  //   SexModel(defaultIcon: 'icon_sex_lgbtq_0.png', selectedIcon: 'icon_sex_lgbtq_1.png', name: '我是 LGBTQ'),
  // ];

  @override
  void onClose() {
    animationController.dispose();
    super.onClose();
  }

  @override
  void onInit() {
    super.onInit();
    animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 10000),
    );
    animationController.addListener(() {
      if (animationController.status == AnimationStatus.completed) {
        startAnimation();
        //结束了
      } else if (animationController.status == AnimationStatus.forward) {
        // print('forward');
      } else if (animationController.status == AnimationStatus.reverse) {
        // print('reverse');
      }
      update();
    });
    //初始位置
    animation = Tween<double>(begin: 0, end: 0).animate(
      CurvedAnimation(
        parent: animationController,
        curve: const Interval(
          0,
          1.0,
          curve: Curves.easeInToLinear,
        ),
      ),
    );
    startAnimation();

    getSexData();
  }

  getSexData() {
    WhlApi.getGenderList.get({"type": 1}).then((value) {
      if (value.isSuccess()) {
        List data = value.data;
        sexList = data.map((e) {
          SexModel model = SexModel(
              defaultIcon: e["imageUrl"],
              selectedIcon: e["imageUrl"],
              name: e["lable"],
              value: e["id"]);
          return model;
        }).toList();
        update();
      }
    });
  }

  void startAnimation() {
    animation = Tween<double>(begin: 0, end: 3).animate(CurvedAnimation(
      parent: animationController,
      curve: const Interval(
        0,
        1.0,
        curve: Curves.ease,
      ),
    ));
    animationController.reset();
    animationController.forward();
  }

  doClickNext() {
    if (selectedIndex == 2) {
      Get.bottomSheet(SelectedLgbtqSheet(), isScrollControlled: true)
          .then((value) {
        print(value);
        if (value != null) {
          Map<String, dynamic> param = Get.arguments ?? {};
          param["gender"] = value;
          Get.toNamed(Routes.sexOrientation, arguments: param);
        }
      });
    } else {
      SexModel model = sexList[selectedIndex];
      Map<String, dynamic> param = Get.arguments ?? {};
      param["gender"] = model.value;
      Get.toNamed(Routes.sexOrientation, arguments: param);
    }
  }
}
