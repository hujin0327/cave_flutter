import 'package:get/get.dart';

import '../../../../../network/whl_api.dart';
import '../selected_sex_logic.dart';

class SelectedLgbtqLogic extends GetxController {
  List<SexModel> sexList = [
    // SexModel(defaultIcon: 'icon_sex_lgbtq_common_0_0.png', selectedIcon: 'icon_sex_lgbtq_common_0_1.png', name: '中性',describe: '喜欢打扮比较酷的女生或者喜欢打扮比较优雅的男性',value: 2),
    // SexModel(defaultIcon: 'icon_sex_lgbtq_common_1_0.png', selectedIcon: 'icon_sex_lgbtq_common_1_1.png', name: '跨性别男性',describe: '跨性别男性是指性别认同部分为男性的人群，但拥有女性身体特征，心理性别为男性，例如les-T',value: 3),
    // SexModel(defaultIcon: 'icon_sex_lgbtq_common_2_0.png', selectedIcon: 'icon_sex_lgbtq_common_2_1.png', name: '跨性别女性',describe: '跨性别女性是指性别认同部分为女性的人群，但拥有男性身体特征，心理性别为女性，例如TS',value: 4),
    // SexModel(defaultIcon: 'icon_sex_lgbtq_common_3_0.png', selectedIcon: 'icon_sex_lgbtq_common_3_1.png', name: '偏男两性人',describe: '偏男两性人是指性别与男性相似但不完全符合男性的人群，基因或染色体性别不完全为男性，例如克氏综合征，染色体表达成了XXY等情况。这些人在社会上面临着来自传统性别二元制的歧视和排斥，需要我们尊重他们的身份和权利，为他们争取平等和包容的环境。',value: 5),
    // SexModel(defaultIcon: 'icon_sex_lgbtq_common_5_0.png', selectedIcon: 'icon_sex_lgbtq_common_5_1.png', name: '偏女两性人',describe: '偏女两性人性别通常指的是在身体上被归类为女性，基因或染色体性别不完全为女性，例如超雄综合征，染色体的表达成了XXX等情况。这些人在社会上面临着来自传统性别二元制的歧视和排斥，需要我们尊重他们的身份和权利，为他们争取平等和包容的环境。',value: 6),
    // SexModel(defaultIcon: 'icon_sex_lgbtq_common_4_0.png', selectedIcon: 'icon_sex_lgbtq_common_4_1.png', name: '无性别',describe: '无性别又称精神无性别者，是一种性别酷儿或性别认同，认为自己没有准确性别的人群，可以是男生也可以是女生，甚至是一件物品，或者拒绝任何性别，没有有感觉到自己有任何强烈性别归属的人。',value: 7),
  ];
  int selectedSex = 0;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    getSexData();
  }

  getSexData() {
    WhlApi.getGenderList.get({"type": 1}).then((value) {
      List data = value.data;
      sexList = data.map((e) {
        SexModel model = SexModel(
            defaultIcon: e["imageUrl"],
            selectedIcon: e["imageUrl"],
            name: e["lable"],
            value: e["id"],
            describe: e["note"]);
        return model;
      }).toList();
      update();
    });
  }
}
