import 'package:cave_flutter/pages/whl_login/complete_user_info/selected_sex/selected_sex_logic.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../../config/whl_global_config.dart';
import 'selected_lgbtq_logic.dart';

class SelectedLgbtqSheet extends StatefulWidget {
  const SelectedLgbtqSheet({Key? key}) : super(key: key);

  @override
  State<SelectedLgbtqSheet> createState() => _SelectedLgbtqSheetState();
}

class _SelectedLgbtqSheetState extends State<SelectedLgbtqSheet> {
  final logic = Get.put(SelectedLgbtqLogic());

  @override
  void dispose() {
    super.dispose();
    Get.delete<SelectedLgbtqLogic>();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SelectedLgbtqLogic>(
        builder: (logic) {
          return UIColumn(
            topRightRadius: 20.w,
            topLeftRadius: 20.w,
            height: 700.h,
            color: Colors.white,
            width: double.maxFinite,
            children: [
              Expanded(
                child: CustomScrollView(
                  physics: const NeverScrollableScrollPhysics(),
                  slivers: [
                    SliverToBoxAdapter(
                      child: Column(
                        children: [
                          UIRow(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            margin: EdgeInsets.only(top: 20.h,
                                left: 20.w,
                                right: 20.w,
                                bottom: 22.h),
                            children: [
                              UIText(
                                text: '取消',
                                textColor: kAppColor('#C1C1C1'),
                                fontSize: 15.sp,
                                onTap: () {
                                  Get.back();
                                },
                              ),
                              UIText(
                                text: '确认',
                                textColor: kAppTextColor,
                                fontSize: 15.sp,
                                fontWeight: FontWeight.bold,
                                onTap: () {
                                  SexModel selectedModel = logic.sexList[logic
                                      .selectedSex];
                                  Get.back(result: selectedModel.value);
                                },
                              ),
                            ],
                          ),
                          UIText(
                            text: '小众性别在社会通常会受到外界歧视 我们应该尊重、包容弱势多元群体💗',
                            textColor: kAppColor('#979797'),
                            fontSize: 14.sp,
                            width: 230.w,
                            maxLines: 2,
                            shrinkWrap: false,
                          ),
                        ],
                      ),
                    ),
                    SliverPadding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 25.w, vertical: 39.h),
                      sliver: SliverGrid(
                          delegate: SliverChildBuilderDelegate((context,
                              index) {
                            SexModel model = logic.sexList[index];
                            return buildDottedBorderView(model);
                          }, childCount: logic.sexList.length),
                          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 3, childAspectRatio: 0.75)),
                    ),
                    if(logic.sexList.isNotEmpty)
                    SliverToBoxAdapter(
                      child: GetBuilder<SelectedLgbtqLogic>(builder: (logic) {
                        SexModel selectedModel = logic.sexList[logic
                            .selectedSex];
                        return Column(
                          children: [
                            UIText(
                              text: '什么是${selectedModel.name}',
                              textColor: kAppTextColor,
                              fontSize: 14.sp,
                              fontWeight: FontWeight.w500,
                              marginDrawable: 3.w,
                              startDrawable: UIImage(
                                assetImage: 'icon_sex_tips.png',
                                width: 24.w,
                              ),
                            ),
                            UIText(
                              margin: EdgeInsets.only(
                                  top: 6.h, left: 46.w, right: 46.w),
                              text: selectedModel.describe,
                              textColor: kAppColor('#666666'),
                              fontSize: 13.sp,
                              shrinkWrap: false,
                              textAlign: TextAlign.center,
                            )
                          ],
                        );
                      }),
                    )
                  ],
                ),
              ),
            ],
          );
        });
  }

  buildDottedBorderView(SexModel sexModel) {
    return GetBuilder<SelectedLgbtqLogic>(builder: (logic) {
      bool isSelected = logic.sexList.indexOf(sexModel) == logic.selectedSex;
      return UIColumn(
        children: [
          Stack(
            children: [
              UIContainer(
                width: 88.w,
                height: 88.w,
                radius: 5000,
                strokeColor: kAppColor('#DCDCDC'),
                strokeWidth: 1.w,
                child: Center(
                  child: UIImage(
                    httpImage: (isSelected ? sexModel.selectedIcon : sexModel
                        .defaultIcon)?.toImageUrl(),
                    assetPlaceHolder: WHLGlobalConfig.userPlaceholderAvatar,
                    height: 40.h,
                  ),
                ),
              ),
              if (isSelected)
                GetBuilder<SelectedSexLogic>(builder: (logic) {
                  return RotationTransition(
                    turns: logic.animationController,
                    child: DottedBorder(
                      padding: EdgeInsets.zero,
                      borderType: BorderType.Circle,
                      color: Colors.black,
                      radius: const Radius.circular(5000),
                      strokeWidth: 2,
                      child: SizedBox(
                        width: 88.w,
                        height: 88.w,
                      ),
                    ),
                  );
                })
            ],
          ),
          UIText(
            margin: EdgeInsets.only(top: 18.h),
            text: sexModel.name,
            textColor: isSelected ? kAppTextColor : kAppColor('#C1C1C1'),
            fontSize: 16.sp,
            fontWeight: isSelected ? FontWeight.bold : FontWeight.normal,
          )
        ],
        onTap: () {
          logic.selectedSex = logic.sexList.indexOf(sexModel);
          logic.update();
        },
      );
    });
  }
}
