import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../style/whl_style.dart';
import '../../../widgets/brick/brick.dart';
import 'forgot_password_logic.dart';

class ForgotPasswordPage extends StatelessWidget {
  ForgotPasswordPage({Key? key}) : super(key: key);

  final logic = Get.put(ForgotPasswordLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white.withOpacity(0.5),
      body: Container(
        margin: EdgeInsets.only(top: 52.h),
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(30.w), topRight: Radius.circular(30.w)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UIContainer(
              padding: EdgeInsets.only(top: 18.h, left: 20.w, right: 20.w),
              child: const Icon(Icons.arrow_back_ios),
              onTap: () {
                Get.back();
              },
            ),
            UIText(
              margin: EdgeInsets.only(top: 44.h, left: 30.w),
              text: '手机验证码登录',
              textColor: kAppTextColor,
              fontSize: 30.sp,
              fontWeight: FontWeight.bold,
              marginDrawable: 11.h,
              bottomDrawable: UIText(
                text: '未注册绑定的手机号将自动注册',
                textColor: kAppColor('#7E7E7E'),
                fontSize: 15.sp,
              ),
            ),
            UIRow(
              margin: EdgeInsets.only(top: 37.h, left: 34.w, right: 39.w),
              children: [
                UIText(
                  padding: EdgeInsets.symmetric(horizontal: 25.w),
                  radius: 18.w,
                  alignment: Alignment.center,
                  height: 53.h,
                  text: '+86',
                  textColor: kAppTextColor,
                  fontSize: 14.sp,
                  fontWeight: FontWeight.bold,
                  color: kAppColor('#F7F7F9'),
                ),
                SizedBox(width: 7.w),
                Expanded(
                    child: UIContainer(
                  radius: 18.w,
                  height: 53.h,
                  color: kAppColor('#F7F7F9'),
                  alignment: Alignment.centerLeft,
                  child: TextField(
                    inputFormatters: [LengthLimitingTextInputFormatter(11)],
                    controller: logic.phoneController,
                    keyboardType: TextInputType.number,
                    maxLines: 1,
                    autofocus: false,
                    style: TextStyle(fontSize: 14.sp, color: kAppTextColor, fontWeight: FontWeight.bold),
                    decoration: InputDecoration(
                        border: const OutlineInputBorder(borderSide: BorderSide.none),
                        hintText: '手机号码',
                        hintStyle: TextStyle(color: kAppColor('#C1C1C1'), fontWeight: FontWeight.bold, fontSize: 14.sp),
                        contentPadding: EdgeInsets.only(left: 20.w),
                        suffixIcon: GestureDetector(
                          onTap: () {
                            logic.phoneController.clear();
                          },
                          child: Icon(
                            Icons.close,
                            color: kAppColor('#C1C1C1'),
                          ),
                        )),
                  ),
                ))
              ],
            ),
            GetBuilder<ForgotPasswordLogic>(builder: (logic) {
              return UISolidButton(
                margin: EdgeInsets.only(top: 54.h, left: 38.w, right: 38.w, bottom: 21.h),
                text: '获取验证码${logic.countDown == 60 ? '' : '${logic.countDown}S'}',
                height: 46.h,
                color: logic.countDown == 60 ? Colors.black : kAppColor('#D9D9D9'),
                onTap: () {
                  logic.doClickSendPhoneCode();
                },
              );
            }),
          ],
        ),
      ),
    );
  }
}
