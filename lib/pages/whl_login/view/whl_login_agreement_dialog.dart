import 'package:cave_flutter/config/whl_global_config.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import '../../../style/whl_style.dart';

class WhlLoginAgreementDialog extends StatelessWidget {
  const WhlLoginAgreementDialog({super.key, required this.onTagAgree});

  final Function() onTagAgree;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(
        child: Wrap(
          children: [
            UIColumn(
              width: double.maxFinite,
              radius: 30.w,
              margin: EdgeInsets.symmetric(horizontal: 34.w),
              color: Colors.white,
              children: [
                UIText(
                  text: '服务条款和隐私协议',
                  fontSize: 17.sp,
                  textColor: kAppTextColor,
                  fontWeight: FontWeight.bold,
                  margin: EdgeInsets.only(top: 25.h, bottom: 6.h),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 40.w),
                  child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(text: '若需要注册或登录Cave，请先阅读并同意', style: TextStyle(fontSize: 11.sp, color: kAppColor('#7E7E7E')), children: [
                        TextSpan(text: ' 服务条款、 ', style: TextStyle(fontSize: 11.sp, color: Colors.black, fontWeight: FontWeight.bold)),
                        TextSpan(text: ' 隐私协议 ', style: TextStyle(fontSize: 11.sp, color: Colors.black, fontWeight: FontWeight.bold)),
                      ])),
                ),
                UIRow(
                  margin: EdgeInsets.symmetric(horizontal: 30.w, vertical: 19.h),
                  children: [
                    Expanded(
                      child: UISolidButton(
                        height: 42.h,
                        text: '取消',
                        textColor: kAppColor('#979797'),
                        color: kAppColor('#F1F0F5'),
                        onTap: () {
                          Get.back();
                        },
                      ),
                    ),
                    SizedBox(width: 30.w),
                    Expanded(
                      child: UISolidButton(
                        height: 42.h,
                        text: '同意',
                        color: Colors.black,
                        onTap: () {
                          Get.back();
                          onTagAgree();
                        },
                      ),
                    ),
                  ],
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
