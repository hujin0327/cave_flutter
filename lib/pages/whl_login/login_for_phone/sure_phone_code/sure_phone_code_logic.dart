import 'dart:ffi';

import 'package:bot_toast/bot_toast.dart';
import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/network/whl_network_utils.dart';
import 'package:cave_flutter/pages/whl_login/setter_password/setter_password_view.dart';
import 'package:cave_flutter/routes/whl_app_pages.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../../../../common/common_action.dart';
import '../../../../utils/whl_toast_utils.dart';

class SurePhoneCodeLogic extends GetxController {
  String msgCode = '';
  String phone = "";
  bool isForget = false;
  FocusNode focusNode = FocusNode();

  @override
  void onInit() {
    super.onInit();

    var a = Get.arguments;
    if (a != null) {
      phone = a["phone"] ?? '';
      isForget = a["isForget"] ?? false;
    }
    //弹起键盘
    Future.delayed(const Duration(milliseconds: 100), () {
      focusNode.requestFocus();
    });
  }

  onSurePhoneCode() async {
    // Get.toNamed(Routes.completeUserInfo);
    // return;

    if (isForget) {
      Map<String, dynamic> params = {
        "authCode": msgCode,
        "phone": phone,
      };
      ResponseData responseData =
          await WhlApi.forgetPassword.post(params, withLoading: true);
      if (responseData.isSuccess()) {
        Get.toNamed(Routes.setterPasswordPage, arguments: params);
      }
    } else {
      Map<String, dynamic> params = {"authCode": msgCode, "phone": phone};
      BotToast.showLoading();
      ResponseData responseData = await WhlApi.loginForCode.post(params);
      if (responseData.isSuccess()) {
        Map dic = responseData.data;
        String token = dic["token"];
        WhlNetWorkUtils.setAccessToken(token);
        getUserInfo(
            isLogin: true,
            callback: (isSuccess) {
              BotToast.closeAllLoading();
              if (!isSuccess) {
                MyToast.show("获取用户信息失败");
              }
            });
        // Get.toNamed(Routes.completeUserInfo);
      }
    }
  }
}
