import 'package:cave_flutter/pages/whl_login/login_for_phone/login_for_phone_logic.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../style/whl_style.dart';
import '../../../../widgets/brick/brick.dart';
import '../../forgot_password/forgot_password_logic.dart';
import 'sure_phone_code_logic.dart';

class SurePhoneCodePage extends StatelessWidget {
  SurePhoneCodePage({Key? key}) : super(key: key);

  final logic = Get.put(SurePhoneCodeLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white.withOpacity(0.5),
      body: UIContainer(
        margin: EdgeInsets.only(top: 52.h),
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(30.w), topRight: Radius.circular(30.w)),
        ),
        onTap: () {},
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UIContainer(
              padding: EdgeInsets.only(top: 18.h, left: 20.w, right: 20.w),
              child: const Icon(Icons.arrow_back_ios),
              onTap: () {
                Get.back();
              },
            ),
            UIText(
              margin: EdgeInsets.only(top: 44.h, left: 30.w),
              text: '确认验证码',
              alignment: Alignment.centerLeft,
              textColor: kAppTextColor,
              fontSize: 30.sp,
              fontWeight: FontWeight.bold,
            ),
            UIText(
              margin: EdgeInsets.only(top: 11.h, left: 30.w),
              text: '请输入6位数字验证码，已发送至 \n${logic.phone}',
              textColor: kAppColor('#7E7E7E'),
              fontSize: 15.sp,
            ),
            UIStack(
              margin: EdgeInsets.only(top: 38.h),
              children: [
                UIRow(
                  margin: EdgeInsets.only(left: 34.w, right: 34.w),
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    for (int i = 0; i < 6; i++)
                      GetBuilder<SurePhoneCodeLogic>(builder: (logic) {
                        return UIContainer(
                          width: 40.w,
                          height: 40.w,
                          alignment: Alignment.center,
                          color: kAppColor('#F7F7F9'),
                          radius: 5.w,
                          strokeWidth: 1,
                          strokeColor: logic.msgCode.length >= i ? kAppTextColor : kAppColor('#F7F7F9'),
                          child: logic.msgCode.length > i
                              ? UIText(
                                  text: logic.msgCode.substring(i, i + 1),
                                  fontSize: 24.sp,
                                  textColor: kAppTextColor,
                                )
                              : null,
                        );
                      }),
                  ],
                ),
                Opacity(
                  opacity: 0,
                  child: TextField(
                    controller: TextEditingController(text: logic.msgCode),
                    onChanged: (value) {
                      logic.msgCode = value;
                      logic.update();
                      if (value.length == 6) {
                        logic.onSurePhoneCode();
                      }
                    },
                    focusNode: logic.focusNode,
                    keyboardType: TextInputType.number,
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly, LengthLimitingTextInputFormatter(6)],
                    decoration: InputDecoration(
                      hintText: '请输入验证码',
                      hintStyle: TextStyle(color: kAppColor('#7E7E7E'), fontSize: 15.sp),
                      contentPadding: EdgeInsets.only(left: 30.w, right: 30.w, top: 20.h, bottom: 20.h),
                      enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: kAppColor('#F7F7F9'))),
                      focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: kAppColor('#F7F7F9'))),
                    ),
                  ),
                ),
              ],
            ),
            if (Get.isRegistered<LoginForPhoneLogic>())
              GetBuilder<LoginForPhoneLogic>(builder: (logic) {
                return UIText(
                  margin: EdgeInsets.only(top: 60.h),
                  alignment: Alignment.center,
                  text: '重新发送${logic.countDown != 60 ? '(${logic.countDown}s)' : ''}',
                  textColor: kAppColor('#686868'),
                  fontSize: 15.sp,
                  fontWeight: FontWeight.bold,
                  onTap: () {
                    logic.onSendPhoneCode();
                  },
                );
              })
            else
              GetBuilder<ForgotPasswordLogic>(builder: (logic) {
                return UIText(
                  margin: EdgeInsets.only(top: 60.h),
                  alignment: Alignment.center,
                  text: '重新发送${logic.countDown != 60 ? '(${logic.countDown}s)' : ''}',
                  textColor: kAppColor('#686868'),
                  fontSize: 15.sp,
                  fontWeight: FontWeight.bold,
                  onTap: () {
                    logic.onSendPhoneCode();
                  },
                );
              }),
            // DottedBorder(
            //   borderType: BorderType.RRect,
            //   radius: Radius.circular(233),
            //   padding: EdgeInsets.all(6),
            //   child: Container(height: 100,width: 100,),
            // ),
          ],
        ),
      ),
    );
  }
}
