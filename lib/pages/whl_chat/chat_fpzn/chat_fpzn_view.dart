import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../style/whl_style.dart';
import '../../../widgets/brick/widget/image_widget.dart';
import 'chat_fpzn_logic.dart';

class Chat_fpznPage extends StatelessWidget {
  Chat_fpznPage({Key? key}) : super(key: key);

  final logic = Get.put(Chat_fpznLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          MediaQuery.removePadding(
            context: context,
            removeTop: true,
            child: ListView(
              children: [
                UIImage(
                  width: ScreenUtil().screenWidth,
                  fit: BoxFit.fitWidth,
                  assetImage: "chat_fpzn",
                )
              ],
            ),
          ),
          UIImage(
            margin: EdgeInsets.only(
                top: ScreenUtil().statusBarHeight + 10.w, left: 10.w),
            padding: EdgeInsets.all(5.w),
            assetImage: "icon_back_black",
            width: 30.w,
            height: 30.w,
            onTap: () {
              Get.back();
            },
          ),
        ],
      ),
    );
  }
}
