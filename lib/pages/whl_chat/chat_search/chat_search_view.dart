import 'package:cave_flutter/config/whl_global_config.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:nim_core/nim_core.dart';

import '../../../style/whl_style.dart';
import '../../../utils/whl_date_utils.dart';
import '../../../widgets/search_widget.dart';
import '../../../widgets/whl_app_bar.dart';
import 'chat_search_logic.dart';

class Chat_searchPage extends StatelessWidget {
  Chat_searchPage({Key? key}) : super(key: key);

  final logic = Get.put(Chat_searchLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppWhiteColor,
      appBar: WhlAppBar(
        backgroundColor: kAppWhiteColor,
        isBack: false,
        isShowBottomLine: false,
        centerWidget: UIContainer(
          margin: EdgeInsets.only(right: 45.w),
          child: SearchWidget(
            logic.editingController,
            bgColor: kAppColor("#F6F6F6"),
            searchCallback: (value) {
              logic.startSearch();
            },
          ),
        ),
        rightWidget: UIText(
          margin: EdgeInsets.only(right: 15.w),
          text: "取消",
          textColor: kAppSub3TextColor,
          fontSize: 16.sp,
          onTap: () {
            Get.back();
          },
        ),
      ),
      body: UIColumn(
        color: kAppColor("#F8F8F8"),
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          UIText(
            margin: EdgeInsets.only(left: 16.w, top: 20.h, bottom: 10.h),
            text: "搜索结果",
            textColor: kAppBlackColor,
            fontSize: 14.sp,
            fontWeight: FontWeight.bold,
          ),
          Expanded(
              child: UIContainer(
            color: kAppWhiteColor,
            child: GetBuilder<Chat_searchLogic>(builder: (logic) {
              return ListView.builder(
                itemBuilder: (context, index) {
                  NIMMessage message = logic.searchData![index];
                  NIMUser? user = null;
                  if (index < logic.userInfo.length) {
                    user = logic.userInfo[index];
                  }
                  return getSearchResultItem(message, user);
                },
                itemCount: logic.searchData?.length,
              );
            }),
          ))
        ],
      ),
    );
  }

  getSearchResultItem(NIMMessage message, NIMUser? user) {
    String context = message.content ?? "";
    String timeStr = WhlDateUtils.getTimeStringFromWechat(message.timestamp, isShowTime: true);
    Map<String, dynamic> ext = message.remoteExtension ?? {};
    String nickName = ext["nickName"] ?? "";
    if (user != null && nickName.isEmpty) {
      print('【cave】nick: ${user.nick}; userId: ${user.userId}; ext: ${user.ext}');
      nickName = user.nick ?? "";
    }

    String avatar = ext["avatar"] ?? "";
    NimCore.instance.userService.getUserInfo(message.fromAccount ?? '');
    return UIContainer(
      decoration: bottomLineBoxDecoration,
      child: UIRow(
        children: [
          UIImage(
            margin: EdgeInsets.only(left: 18.w, right: 10.w),
            httpImage: avatar.toImageUrl(),
            assetPlaceHolder: WHLGlobalConfig.userPlaceholderAvatar,
            width: 44.w,
            height: 44.w,
            radius: 22.w,
            fit: BoxFit.cover,
            clipBehavior: Clip.hardEdge,
          ),
          Expanded(
            child: UIColumn(
              padding: EdgeInsets.symmetric(vertical: 15.h),
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                UIText(
                  margin: EdgeInsets.only(bottom: 5.h),
                  textColor: kAppSub3TextColor,
                  fontSize: 12.sp,
                  text: nickName,
                ),
                RichText(
                    text: TextSpan(
                        children: context.getTextSpanList(
                  searchContent: logic.editingController.text,
                  selectFontColor: kAppColor("#1E80C8"),
                )))
              ],
            ),
          ),
          UIText(
            margin: EdgeInsets.only(right: 18.w, left: 10.w),
            textColor: kAppSub3TextColor,
            fontSize: 13.sp,
            text: timeStr,
          )
        ],
      ),
    );
  }
}
