import 'package:cave_flutter/model/user_model.dart';
import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/pages/whl_chat/whl_chat_logic.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../whl_chat_room/whl_chat_room_logic.dart';

class Setter_remarkLogic extends GetxController {
  TextEditingController textEditingController = TextEditingController();

  UserModel? userModel;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    var a = Get.arguments;
    if (a != null) {
      userModel = a["userModel"];
      textEditingController.text = userModel?.remark ?? "";
      update();
    }
  }

  void onConfirm() {
    /*
    if (textEditingController.text.isEmpty) {
      MyToast.show("请输入");
      return;
    }
    */

    WhlApi.saveRemark
        .get({"remark": textEditingController.text.isNotEmpty ? textEditingController.text : null, "tid": userModel?.acid}).then((value) {
      if (value.isSuccess()) {
        userModel?.remark = textEditingController.text;
        WhlChatRoomLogic roomLogic = Get.find<WhlChatRoomLogic>();
        WhlChatLogic chatLogic = Get.find<WhlChatLogic>();
        roomLogic.update();
        chatLogic.update();
        Get.back(result: true);
      }
    });
  }
}
