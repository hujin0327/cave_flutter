import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../common/common_widget.dart';
import '../../../style/whl_style.dart';
import '../../../widgets/brick/widget/basic_widget.dart';
import '../../../widgets/brick/widget/text_widget.dart';
import '../../../widgets/whl_app_bar.dart';
import 'setter_remark_logic.dart';

class Setter_remarkPage extends StatelessWidget {
  final logic = Get.put(Setter_remarkLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppWhiteColor,
      appBar: WhlAppBar(
        backgroundColor: kAppWhiteColor,
        centerTitle: '设置备注',
        isShowBottomLine: true,
      ),
      body: UIColumn(
          crossAxisAlignment: CrossAxisAlignment.start,
          color: kAppWhiteColor,
          padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 25.w),
          children: [
            noBorderCTextField(
              controller: logic.textEditingController,
              hint: "请输入",
              fontSize: 16.sp,
            ),
            Expanded(child: UIContainer()),
            UIText(
              margin: EdgeInsets.symmetric(horizontal: 20.w, vertical: 60.h),
              text: "确定",
              textColor: kAppWhiteColor,
              fontSize: 16.sp,
              fontWeight: FontWeight.bold,
              color: kAppBlackColor,
              height: 52.h,
              radius: 26.h,
              alignment: Alignment.center,
              onTap: () {
                logic.onConfirm();
              },
            )
          ]),
    );
  }
}
