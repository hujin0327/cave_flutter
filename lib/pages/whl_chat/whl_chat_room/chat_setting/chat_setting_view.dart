import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:cave_flutter/widgets/whl_row_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:nim_core/nim_core.dart';

import 'chat_setting_logic.dart';

class ChatSettingPage extends StatelessWidget {
  ChatSettingPage({Key? key}) : super(key: key);

  final logic = Get.put(ChatSettingLogic());
  EdgeInsets padding =
      EdgeInsets.only(left: 17.w, right: 13.w, top: 13.w, bottom: 13.w);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ChatSettingLogic>(builder: (logic) {
      return Scaffold(
        backgroundColor: kAppColor('#FAFAFA'),
        appBar: WhlAppBar(
          centerTitle: logic.centerTitle,
          backgroundColor: kAppColor('#FAFAFA'),
          isShowBottomLine: false,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              if (logic.sessionType == NIMSessionType.p2p)
                MyRowItem(
                  margin: EdgeInsets.only(top: 8.w),
                  color: Colors.white,
                  padding: padding,
                  title: '设置备注',
                  value: logic.userModel?.remark,
                  onTap: () {
                    logic.toSetterRemarkPage();
                  },
                ),
              if (logic.sessionType == NIMSessionType.team)
                MyRowItem(
                    margin: EdgeInsets.only(top: 8.w),
                    color: Colors.white,
                    padding: padding,
                    title: '设置群昵称',
                    value: logic.groupNickname,
                    onTap: () {
                      logic.setTeamNickname();
                    }),
              // if (logic.sessionType == NIMSessionType.team) buildMemerContainer(),
              Column(
                children: [
                  MyRowItem(
                    margin: EdgeInsets.only(top: 8.w),
                    color: Colors.white,
                    padding: padding,
                    title: '消息置顶',
                    isShowSwitch: logic.isShowSwitch,
                    isSwitchOn: logic.isTop,
                    onChanged: () {
                      logic.changeTopState();
                    },
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 20.w),
                    height: 0.5.w,
                    color: kAppColor('#F2F0F0'),
                  ),
                  MyRowItem(
                    color: Colors.white,
                    padding: padding,
                    title: '消息免打扰',
                    isShowSwitch: logic.isShowSwitch,
                    isSwitchOn: logic.isMute,
                    onChanged: () {
                      logic.changeMuteState();
                    },
                  ),
                ],
              ),
              Column(
                children: [
                  MyRowItem(
                    margin: EdgeInsets.only(top: 8.w),
                    color: Colors.white,
                    padding: padding,
                    title: '聊天背景',
                    onTap: () {
                      MyToast.show('暂未开放');
                    },
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 20.w),
                    height: 0.5.w,
                    color: kAppColor('#F2F0F0'),
                  ),
                  MyRowItem(
                    onTap: () {
                      MyToast.show('暂未开放');
                    },
                    color: Colors.white,
                    padding: padding,
                    title: '聊天气泡',
                  ),
                ],
              ),
              Column(
                children: [
                  MyRowItem(
                    margin: EdgeInsets.only(top: 8.w),
                    color: Colors.white,
                    padding: padding,
                    title: '查找聊天记录',
                    onTap: () {
                      logic.goSearchPage();
                    },
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 20.w),
                    height: 0.5.w,
                    color: kAppColor('#F2F0F0'),
                  ),
                  MyRowItem(
                    color: Colors.white,
                    padding: padding,
                    title: '清空本地聊天记录',
                    onTap: () {
                      logic.deleteLocMessage();
                    },
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 20.w),
                    height: 0.5.w,
                    color: kAppColor('#F2F0F0'),
                  ),
                  MyRowItem(
                    color: Colors.white,
                    padding: padding,
                    title: '清空本地及服务器聊天记录',
                    onTap: () {
                      logic.deleteLocAndServiceMessage();
                    },
                  ),
                ],
              ),
              if (logic.sessionType == NIMSessionType.p2p) // 只有单聊才有加入黑名单
                MyRowItem(
                  margin: EdgeInsets.only(top: 8.w),
                  color: Colors.white,
                  padding: padding,
                  title: '加入黑名单',
                  isShowSwitch: true,
                  isSwitchOn: logic.isBlack,
                  onChanged: () {
                    logic.changeBlackAction();
                  },
                ),
              MyRowItem(
                margin: EdgeInsets.only(top: 8.w),
                color: Colors.white,
                padding: padding,
                title: '一键举报',
                onTap: () {
                  logic.doClickComplaint();
                },
              ),
            ],
          ),
        ),
      );
    });
  }

  Widget buildMemerContainer() {
    List<Widget> memberWidgets = [];

    int index = 0;
    int maxCount = 4;
    for (NIMUser item in logic.members ?? []) {
      if (index >= maxCount) {
        break;
      }

      var memberWidget = UIColumn(
        children: [
          UIImage(
            // assetImage: "icon_home_top_2",
            httpImage: item.avatar,
            width: 42.w,
            height: 42.w,
            radius: 31.w,
            fit: BoxFit.cover,
          ),
          UIText(
            text: item.nick,
            fontSize: 12.sp,
            textColor: kAppSub2TextColor,
            fontWeight: FontWeight.normal,
          ),
        ],
      );

      memberWidgets.add(memberWidget);
      index++;
    }

    return UIRow(
      children: [
        Expanded(
          child: UIContainer(
            height: 112.w,
            margin: EdgeInsets.only(top: 8.w),
            color: Colors.white,
            padding: padding,
            child: UIColumn(
              children: [
                UIRow(
                  children: [
                    UIText(
                      text: '群聊成员',
                      fontSize: 15.sp,
                      textColor: kAppColor('#444444'),
                      fontWeight: FontWeight.normal,
                    ),
                    const Expanded(
                      child: UIContainer(),
                    ),
                    UIRow(
                      onTap: () {},
                      children: [
                        UIText(
                          text: '查看全部群聊成员',
                          fontSize: 12.sp,
                          textColor: kAppSub2TextColor,
                          fontWeight: FontWeight.normal,
                        ),
                        Icon(
                          Icons.keyboard_arrow_right,
                          color: kAppSub2TextColor,
                          size: 20.w,
                        ),
                      ],
                    )
                  ],
                ),
                UIRow(
                  children: memberWidgets.isNotEmpty
                      ? memberWidgets
                      : [const UIContainer()],
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
