import 'dart:io';

import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'complaint_logic.dart';

class ComplaintPage extends StatelessWidget {
  ComplaintPage({Key? key}) : super(key: key);

  final logic = Get.put(ComplaintLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: WhlAppBar(
        centerTitle: '',
        backgroundColor: Colors.white,
        isShowBottomLine: false,
      ),
      body: GetBuilder<ComplaintLogic>(builder: (logic) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UIRow(
              margin: EdgeInsets.only(left: 20.w, right: 15.w),
              padding: EdgeInsets.symmetric(vertical: 8.h),
              decoration: BoxDecoration(border: Border(bottom: BorderSide(width: 0.5.w, color: kAppColor('#D9D9D9')))),
              children: [
                UIText(
                  text: '违规项目:',
                  textColor: kAppColor('#9E9E9E'),
                  fontSize: 15.sp,
                ),
                UIText(
                  text: logic.title,
                  textColor: kAppTextColor,
                  fontSize: 15.sp,
                )
              ],
            ),
            UIText(
              margin: EdgeInsets.only(top: 20.w, left: 18.w),
              text: '举报类型',
              textColor: kAppTextColor,
              alignment: Alignment.centerLeft,
              fontSize: 16.sp,
              fontWeight: FontWeight.bold,
              startDrawable: UIText(
                text: '*',
                textColor: kAppColor('#DA0042'),
                fontSize: 16.sp,
                fontWeight: FontWeight.bold,
              ),
            ),
            UIWrap(
              margin: EdgeInsets.only(top: 18.w, left: 18.w),
              runSpacing: 12.w,
              spacing: 15.w,
              children: logic.reportTypes.map((e) {
                bool isSelected = logic.selectedTypeId == e["id"];
                return UIText(
                  padding: EdgeInsets.symmetric(horizontal: 21.w, vertical: 9.w),
                  radius: 24.w,
                  text: e["name"],
                  textColor: isSelected ? Colors.white : kAppTextColor,
                  fontSize: 13.sp,
                  color: isSelected ? kAppTextColor : Colors.transparent,
                  fontWeight: FontWeight.bold,
                  strokeWidth: 1.w,
                  strokeColor: kAppColor('#DCDCDC'),
                  onTap: () {
                    logic.selectedTypeId = e['id'] ?? '';
                    logic.update();
                  },
                );
              }).toList(),
            ),
            UIText(
              margin: EdgeInsets.only(top: 20.w, left: 18.w),
              text: '举报内容',
              textColor: kAppTextColor,
              alignment: Alignment.centerLeft,
              fontSize: 16.sp,
              fontWeight: FontWeight.bold,
              startDrawable: UIText(
                text: '*',
                textColor: kAppColor('#DA0042'),
                fontSize: 16.sp,
                fontWeight: FontWeight.bold,
              ),
            ),
            UIContainer(
              strokeColor: kAppColor('#DADADA'),
              strokeWidth: 0.5.w,
              margin: EdgeInsets.only(left: 16.w, right: 16.w, top: 18.w),
              radius: 16.w,
              child: TextField(
                controller: logic.contentController,
                maxLines: 5,
                decoration: InputDecoration(
                  hintText: '请补充详细的说明...',
                  hintStyle: TextStyle(color: kAppColor('#979797'), fontSize: 13.sp),
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 15.w),
                ),
              ),
            ),
            UIText(
              margin: EdgeInsets.only(top: 20.w, left: 18.w),
              text: '上传证据',
              textColor: kAppTextColor,
              alignment: Alignment.centerLeft,
              fontSize: 16.sp,
              fontWeight: FontWeight.bold,
            ),
            GetBuilder<ComplaintLogic>(builder: (logic) {
              if (logic.imagePath != null) {
                return Stack(
                  alignment: Alignment.topRight,
                  children: [
                    UIContainer(
                      margin: EdgeInsets.only(left: 16.w, top: 13.h),
                      width: 112.w,
                      height: 112.w,
                      strokeWidth: 1.w,
                      strokeColor: kAppColor('#D9D9D9'),
                      radius: 16.w,
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(16.w), child: Image.file(File(logic.imagePath!), fit: BoxFit.cover)),
                      onTap: () {
                        logic.doClickImageAsset();
                      },
                    ),
                    UIContainer(
                      padding: EdgeInsets.only(right: 4.w, top: 18.w),
                      child: UIImage(
                        assetImage: 'icon_close_circle.png',
                        width: 20.w,
                      ),
                      onTap: () {
                        logic.imagePath == null;
                        logic.update();
                      },
                    )
                  ],
                );
              }
              return UIContainer(
                margin: EdgeInsets.only(left: 16.w, top: 13.h),
                color: kAppColor('#F3F3F3'),
                width: 112.w,
                height: 112.w,
                alignment: Alignment.center,
                radius: 16.w,
                child: UIText(
                  text: '+',
                  textColor: Colors.white,
                  fontSize: 30.sp,
                  fontWeight: FontWeight.bold,
                ),
                onTap: () {
                  logic.doClickImageAsset();
                },
              );
            }),
            UISolidButton(
              margin: EdgeInsets.only(top: 60.w, left: 16.w, right: 16.w),
              text: '提交',
              fontSize: 16.sp,
              textColor: Colors.white,
              onTap: () {
                logic.onConfirm();
              },
            )
          ],
        );
      }),
    );
  }
}
