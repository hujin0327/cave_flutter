import 'package:cave_flutter/pages/whl_chat/whl_chat_room/chat_setting/chat_setting_logic.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:nim_core/nim_core.dart';

class ChatSetGroupNameLogic extends GetxController {
  String groupId = '';
  String groupNickname = '';
  TextEditingController textEditingController = TextEditingController();

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    var a = Get.arguments;
    if (a != null) {
      groupId = a["groupId"];
      groupNickname = a["groupNickname"];
      textEditingController.text = groupNickname;
      update();
    }
  }

  void onConfirm() {
    NimCore.instance.teamService.updateMyTeamNick(groupId, textEditingController.text);
    MyToast.show("设置成功");
    ChatSettingLogic logic = Get.find<ChatSettingLogic>();
    logic.groupNickname = textEditingController.text;
    logic.update();
    Get.back();
  }
}
