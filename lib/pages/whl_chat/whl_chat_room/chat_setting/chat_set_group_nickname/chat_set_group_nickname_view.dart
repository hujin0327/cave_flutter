import 'package:cave_flutter/common/common_widget.dart';
import 'package:cave_flutter/pages/whl_chat/whl_chat_room/chat_setting/chat_set_group_nickname/chat_set_group_nickname_logic.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/widget/basic_widget.dart';
import 'package:cave_flutter/widgets/brick/widget/text_widget.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class ChatSetGroupNamePage extends StatelessWidget {
  ChatSetGroupNamePage({super.key});
  final logic = Get.put(ChatSetGroupNameLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppWhiteColor,
      appBar: WhlAppBar(
        backgroundColor: kAppWhiteColor,
        centerTitle: '设置群昵称',
        isShowBottomLine: true,
      ),
      body: UIColumn(
          crossAxisAlignment: CrossAxisAlignment.start,
          color: kAppWhiteColor,
          padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 25.w),
          children: [
            noBorderCTextField(
              controller: logic.textEditingController,
              hint: "请输入群昵称",
              fontSize: 16.sp,
            ),
            const Expanded(child: UIContainer()),
            UIText(
              margin: EdgeInsets.symmetric(horizontal: 20.w, vertical: 60.h),
              text: "确定",
              textColor: kAppWhiteColor,
              fontSize: 16.sp,
              fontWeight: FontWeight.bold,
              color: kAppBlackColor,
              height: 52.h,
              radius: 26.h,
              alignment: Alignment.center,
              onTap: () {
                logic.onConfirm();
              },
            )
          ]),
    );
  }
}
