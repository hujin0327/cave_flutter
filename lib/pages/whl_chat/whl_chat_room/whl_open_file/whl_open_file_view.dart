import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:open_app_file/open_app_file.dart';

import 'whl_open_file_logic.dart';

class WhlOpenFilePage extends StatelessWidget {
  WhlOpenFilePage({Key? key}) : super(key: key);

  final logic = Get.put(WhlOpenFileLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppColor('#F7F8FA'),
      appBar: WhlAppBar(
        centerTitle: '',
        backgroundColor: Colors.white,
      ),
      body: UIColumn(
        crossAxisAlignment: CrossAxisAlignment.center,
        width: 375.w,
        children: [
          UIContainer(
            margin: EdgeInsets.only(top: 186.h),
            width: 106.w,
            height: 106.w,
            color: Colors.white,
            radius: 12.w,
            child: UIImage(
              assetImage: 'icon_chat_tool_file.png',
              width: 50.w,
              imageColor: kAppSub2TextColor,
            ),
          ),
          UIText(
            margin: EdgeInsets.only(top: 10.h),
            // text: logic.message.name ?? '',
            textColor: kAppSub2TextColor,
            fontSize: 14.sp,
          ),
          Expanded(child: Container()),
          UIText(
            margin: EdgeInsets.only(left: 57.w,right: 57.w,bottom: ScreenUtil().bottomBarHeight + 20.h),
            padding: EdgeInsets.symmetric(vertical: 16.h),
            text: 'Open',
            textColor: Colors.white,
            fontSize: 18.sp,
            alignment: Alignment.center,
            radius: 100.w,
            gradientStartColor: kAppLeftColor,
            gradientEndColor: kAppRightColor,
            onTap: () async {
              // String downloadPath = logic.message.local ?? '';
              // OpenResult re = await OpenAppFile.open(downloadPath);
              // if (re.type == ResultType.done) {
              //   print('打开成功');
              // } else {
              //   print('打开失败');
              // }
            },
          )
        ],
      ),
    );
  }
}
