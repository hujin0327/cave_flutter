import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:cave_flutter/whl_app.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

enum SendRedGroupType {
  lucky,
  normal,
  exclusive,
}

class SendRedGroupPageLogic extends GetxController {
  TextEditingController numController = TextEditingController();
  TextEditingController descController = TextEditingController();
  TextEditingController moneyController = TextEditingController();
  TextEditingController allMoneyController = TextEditingController();

  TextEditingController sendCountController = TextEditingController(text: '1');
  TextEditingController timeIntervalController = TextEditingController(text: '1');
  FocusNode numFocusNode = FocusNode();
  bool isExceedAmount = false;
  double maxAmount = 200;
  String money = '0.00';

  @override
  onInit() {
    super.onInit();
    //弹起键盘
    Future.delayed(const Duration(milliseconds: 100), () {
      numFocusNode.requestFocus();
    });
  }

  onClickSendBtn() {
    hideKeyboard(Get.context);
    if (allMoneyController.text.isEmpty) {
      MyToast.show("请输入总金额");
      return;
    }
    if (double.parse(allMoneyController.text) <= 0) {
      MyToast.show('总金额必须大于0');
      return;
    }
    if (moneyController.text.isEmpty) {
      MyToast.show("请输入单个金额");
      return;
    }
    if (double.parse(moneyController.text) <= 0) {
      MyToast.show('单个金额必须大于0');
      return;
    }
  }

  onClickSelectMember() {
    hideKeyboard(Get.context);
  }

  //总金额输入框监听
  onChangedMoney(String value) {
    if (moneyController.text.isNotEmpty && double.parse(moneyController.text) > maxAmount) {
      isExceedAmount = true;
    } else {
      isExceedAmount = false;
    }

    money =
        (int.parse(numController.text.isEmpty ? '0' : numController.text) * double.parse(moneyController.text.isEmpty ? '0' : moneyController.text))
            .toStringAsFixed(2);
    update();
  }
}
