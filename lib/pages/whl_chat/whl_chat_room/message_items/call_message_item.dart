import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:nim_core/nim_core.dart';

import '../../../../common/chat_manager.dart';
import '../../../../style/whl_style.dart';
import '../../../../widgets/brick/widget/basic_widget.dart';
import '../../../../widgets/brick/widget/text_widget.dart';
import '../../whl_chat_logic.dart';

class CallMessageItem extends StatelessWidget {
  // final RCIMIWMessage messageModel;
  // final int index;
  NIMMessage message;

  CallMessageItem({super.key, required this.message});

  final logic = Get.find<WhlChatLogic>();

  @override
  Widget build(BuildContext context) {
    // RCIMIWTextMessage textMessage = messageModel as RCIMIWTextMessage;
    // String? translatedText;
    // if (messageModel.extra?.isNotEmpty == true) {
    //   Map extra = jsonDecode(messageModel.extra!);
    //   translatedText = extra['translatedText'];
    // } else {
    //   translatedText = WhlUserUtils.getTranslatedText(textMessage.text ?? '');
    // }
    bool isSend = message.messageDirection == NIMMessageDirection.outgoing;
    String text = "";
    Map<String, dynamic>? a = message.messageAttachment?.toMap();
    int customType = a?["customType"] ?? 0;
    int actionType = a?["actionType"] ?? 0;
    print(a);
    print("cell show");
    print(customType);
    if (actionType != -1) {
      if (customType == MyCustomMsgType.videoCall.tag) {
        text = "视频通话";
      } else {
        text = "语音通话";
      }
    } else {
      bool isReceive = a?["isReceive"] ?? false;
      int? callTime = a?["callTime"];
      if (customType == MyCustomMsgType.videoCall.tag) {
        if (isReceive) {
          text = isSend?"已拒绝":"对方已拒绝";
        } else if (callTime != null) {
          int time = callTime ?? 0;
          text =
              '视频通话时长 ${(time ~/ 60).toString().padLeft(2, '0')}:${(time % 60).toString().padLeft(2, '0')}';
        } else {
          text = "已取消";
        }
      } else {
        if (isReceive) {
          text = isSend?"已拒绝":"对方已拒绝";
        } else if (callTime != null) {
          int time = callTime ?? 0;
          text =
              '语音通话时长 ${(time ~/ 60).toString().padLeft(2, '0')}:${(time % 60).toString().padLeft(2, '0')}';
        } else {
          text = "已取消";
        }
      }
    }

    return UIColumn(
      crossAxisAlignment: CrossAxisAlignment.start,
      margin: EdgeInsets.only(bottom: 20.h),
      children: [
        UIContainer(
          padding: EdgeInsets.symmetric(horizontal: 14.w, vertical: 12.h),
          // gradientStartColor: messageModel.senderUserId == WhlUserUtils.getId() ? kAppColor('#FFEC45') : Colors.white,
          // gradientEndColor: messageModel.senderUserId == WhlUserUtils.getId() ? kAppColor('#FFEC45') : Colors.white,
          color: isSend ? kAppTextColor : kAppColor('#F5F5F5'),
          radius: 8.w,
          constraints:
              BoxConstraints(maxWidth: Get.width - 180.w, minHeight: 40.h),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              UIText(
                text: text,
                fontSize: 14.sp,
                textColor: isSend ? kAppWhiteColor : kAppSubTextColor,
              )
              // if (translatedText != null)
              //   Container(
              //     margin: EdgeInsets.only(top: 10.h),
              //     padding: EdgeInsets.only(top: 10.h),
              //     decoration: BoxDecoration(border: Border(top: BorderSide(width: 0.5.w, color: kAppColor('#CCCCCC')))),
              //     child: Text(
              //       translatedText,
              //       style: TextStyle(fontSize: 14.sp, color: kAppSubTextColor),
              //     ),
              //   )
            ],
          ),
        ),
        // if (messageModel.senderUserId != WhlUserUtils.getId() && !WhlUserUtils.getIsAutoTranslate() && translatedText == null)
        //   UIText(
        //     margin: EdgeInsets.only(top: 6.h),
        //     text: 'Translate',
        //     fontSize: 11.sp,
        //     textColor: kAppSubTextColor,
        //     marginDrawable: 4.w,
        //     endDrawable: UIImage(
        //       assetImage: 'icon_chat_translate.png',
        //       width: 12.w,
        //       imageColor: kAppSubTextColor,
        //     ),
        //     onTap: () {
        //       logic.onTranslateForChatRoom(messageModel, isManual: true);
        //     },
        //   )
      ],
    );
  }
}
