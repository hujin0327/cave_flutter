import 'package:cave_flutter/pages/whl_chat/whl_chat_room/bottom_controll_view/whl_bottom_control_view_logic.dart';
import 'package:cave_flutter/pages/whl_chat/whl_chat_room/whl_chat_room_logic.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/whl_rc_im_utils.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:cave_flutter/utils/whl_user_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:nim_core/nim_core.dart';

import '../../../../common/chat_manager.dart';
import '../../../../widgets/brick/brick.dart';
import '../../../../widgets/whl_audio_visualizer_widget.dart';

class WhlVoiceMessageItem extends StatelessWidget {
  const WhlVoiceMessageItem({super.key, required this.message});

  final NIMMessage message;

  @override
  Widget build(BuildContext context) {
    bool isSend = message.messageDirection == NIMMessageDirection.outgoing;
    Map dic = message.messageAttachment?.toMap()??{};
    print("load 111111");
    print(dic);
    String path = dic["path"]??"";
    String url = dic["url"]??"";
    return UIContainer(
      margin: EdgeInsets.only(bottom: 20.h),
      padding: EdgeInsets.symmetric(horizontal: 14.w, vertical: 12.h),
      gradientStartColor: isSend ? kAppColor('#FFEC45') : Colors.white,
      gradientEndColor: isSend ? kAppColor('#FFEC45') : Colors.white,
      radius: 8.w,
      // constraints: BoxConstraints(maxWidth: Get.width - 180.w, minHeight: 40.h),
      width: 108.w,
      height: 40.h,
      child: Row(
        children: [
          UIImage(
            margin: EdgeInsets.only(right: 17.w),
            assetImage: "icon_voice.png",
            width: 20.w,
            height: 20.w,
          ),
          GetBuilder<WhlChatRoomLogic>(
            builder: (logic) {
              if (logic.playingModel == message) {
                return Expanded(
                  child: AudioVisualizerWidget(
                    colors: [kAppTextColor, kAppTextColor, kAppTextColor, kAppTextColor],
                    duration: const [900, 700, 600, 800, 500],
                    barCount: 10,
                  ),
                );
              } else {
                return const UIImage(
                  assetImage: 'icon_voice_ wave.png',
                );
              }
            },
            id: 'isPlaying',
          ),
        ],
      ),
      onTap: () async {
        WhlChatRoomLogic roomLogic = Get.find<WhlChatRoomLogic>();
        WhlBottomControlViewLogic logic = Get.find<WhlBottomControlViewLogic>();
        if (roomLogic.playingModel == message) {
          roomLogic.playingModel = null;
          logic.recordVoiceUtils.stopPlayer();
          roomLogic.update(['isPlaying']);
        } else {
          // WhlRCIMUtils.engine.downloadMediaMessage(messageModel,
          //     listener: RCIMIWDownloadMediaMessageListener(onMediaMessageDownloaded: (int? code, RCIMIWMediaMessage? message) {
          //   // if (co == 100) {
          //     roomLogic.update(['isPlaying']);
          //   // }
          // }));
          // NIMResult<void> loadResult = await NimCore.instance.messageService.downloadAttachment(message: message, thumb: true);
          // Map dic = loadResult.toMap();
          // print("load");
          // print(dic);
          if (path.isEmpty&& url.isEmpty){
            MyToast.show('Play failed');
            return;
          }
          logic.recordVoiceUtils.startPlayer(path.isNotEmpty ?path: url, (callBack) {
            if (callBack == -1) {
              MyToast.show('Play failed');
            } else if (callBack == 0) {
              roomLogic.playingModel = null;
              roomLogic.update(['isPlaying']);
            }
          });
          roomLogic.playingModel = message;
          roomLogic.update(['isPlaying']);
        }
      },
    );
  }
}
