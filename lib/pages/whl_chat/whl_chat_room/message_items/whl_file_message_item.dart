import 'package:cave_flutter/routes/whl_app_pages.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:nim_core/nim_core.dart';

import '../../../../common/chat_manager.dart';

class WhlFileMessageItem extends StatelessWidget {
  const WhlFileMessageItem({super.key, required this.message});

  final NIMMessage message;

  @override
  Widget build(BuildContext context) {
    bool isSend = message.messageDirection == NIMMessageDirection.outgoing;
    return IntrinsicHeight(
      child: UIRow(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        margin: EdgeInsets.only(bottom: 10.h),
        padding: EdgeInsets.symmetric(horizontal: 18.w, vertical: 9.h),
        width: 235.w,
        // height: 95.w,
        color: Colors.white,
        radius: 8.w,
        children: [
          UIImage(
            assetImage: 'icon_chat_tool_file.png',
            width: 38.w,
            height: 38.w,
            imageColor: kAppSub2TextColor,
          ),
          Expanded(
            child: UIColumn(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              margin: EdgeInsets.only(left: 10.w),
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                UIText(
                  // text: message.name ?? '',
                  fontSize: 15.sp,
                  textColor: kAppTextColor,
                  fontWeight: FontWeight.w500,
                  maxLines: 2,
                  shrinkWrap: false,
                ),
                UIText(
                  // text: toFileSize(message.size ?? 0),
                  fontSize: 12.sp,
                  textColor: kAppSub2TextColor,
                  fontWeight: FontWeight.w400,
                  maxLines: 1,
                  shrinkWrap: false,
                ),
              ],
            ),
          ),
        ],
        onTap: (){
          Get.toNamed(Routes.openFile,arguments: message);
        },
      ),
    );
  }

  String toFileSize(int fileSize) {
    if (fileSize < 1024) {
      return "${fileSize}B";
    } else if (fileSize < 1024 * 1024) {
      return "${(fileSize / 1024).toStringAsFixed(2)}KB";
    } else if (fileSize < 1024 * 1024 * 1024) {
      return "${(fileSize / 1024 / 1024).toStringAsFixed(2)}MB";
    } else {
      return "${(fileSize / 1024 / 1024 / 1024).toStringAsFixed(2)}GB";
    }
  }
}
