import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../style/whl_style.dart';
import 'notice_logic.dart';

class NoticePage extends StatelessWidget {
  NoticePage({Key? key}) : super(key: key);

  final logic = Get.put(NoticeLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: WhlAppBar(
        centerTitle: '通知',
        backgroundColor: Colors.white,
        isShowBottomLine: false,
      ),
      body: CustomScrollView(
        slivers: [
          SliverList(delegate: SliverChildBuilderDelegate((context, index){
            return IntrinsicHeight(
              child: UIRow(
                padding: EdgeInsets.symmetric(horizontal: 18.w, vertical: 13.w),
                children: [
                  UIImage(
                    httpImage: '',
                    assetPlaceHolder: 'icon_app_logo.png',
                    width: 42.w,
                    height: 42.w,
                    radius: 21.w,
                    margin: EdgeInsets.only(right: 13.w),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      UIText(
                         text: 'Austin 对你的动态点赞',
                         textColor: kAppTextColor,
                         fontSize: 14.sp,
                         fontWeight: FontWeight.bold,
                      ),
                      UIText(
                         text: '5分钟前',
                         textColor: kAppColor('#C1C1C1'),
                         fontSize: 13.sp,
                      )
                    ],
                  )
                ],
              ),
            );
          },childCount: 10))
        ],
      ),
    );
  }
}
