import 'package:flutter/cupertino.dart';

import 'package:get/get.dart';

import '../../../../utils/whl_toast_utils.dart';
import '../../../../whl_app.dart';

class SendRedPersonPageLogic extends GetxController {
  TextEditingController descController = TextEditingController();
  TextEditingController moneyController = TextEditingController();
  // String tag = Get.arguments['tag'];
  FocusNode moneyFocusNode = FocusNode();
  bool isExceedAmount = false; //是否超过限制金额
  double maxAmount = 3000; //最大金额
  String money = '0.00';
  @override
  void onInit() {
    super.onInit();
    //弹起键盘
    Future.delayed(const Duration(milliseconds: 100), () {
      moneyFocusNode.requestFocus();
    });
  }

  onClickSendBtn() {
    hideKeyboard(Get.context);
    if (moneyController.text.isEmpty) {
      MyToast.show("请输入金额");
      return;
    }
    if (double.parse(moneyController.text) <= 0) {
      MyToast.show('金额必须大于0');
      return;
    }
    if (isExceedAmount) return;
    String desc = descController.text;
  }

  //总金额输入框监听
  onChangedMoney(String value) {
    //艾新屋 限制总金额3000
    if (value.isNotEmpty && double.parse(value) > maxAmount) {
      isExceedAmount = true;
    } else {
      isExceedAmount = false;
    }
    money = value;
    update();
  }
}
