import 'package:cave_flutter/style/whl_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../widgets/TextSpanField/money_text_input_formatter.dart';
import '../../../../widgets/brick/brick.dart';
import '../../../../widgets/whl_button.dart';
import '../../../../widgets/whl_white_app_bar.dart';
import 'send_red_person_logic.dart';

class SendRedPersonPage extends StatelessWidget {
  SendRedPersonPage({Key? key}) : super(key: key);

  final logic = Get.put(SendRedPersonPageLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppColor('#F3F3F3'),
      body: SafeArea(
        top: false,
        child: Stack(
          children: [
            UIImage(
              assetImage: 'img_bg_red_pkg_top.png',
              width: double.maxFinite,
              height: 160.h,
              fit: BoxFit.fill,
            ),
            UIColumn(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              margin: EdgeInsets.only(top: 106.h, left: 13.w, right: 13.w),
              width: double.maxFinite,
              radius: 30.w,
              color: kAppColor('#FEFAFB'),
              padding: EdgeInsets.only(top: 27.h, left: 18.w, right: 18.w),
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    UIRow(
                      padding: EdgeInsets.only(left: 13.w, right: 19.w, top: 5.w, bottom: 5.w),
                      color: Colors.white,
                      radius: 10.w,
                      children: [
                        UIText(
                          text: 'C币金额',
                          textColor: kAppColor('#E93323'),
                          fontSize: 16.sp,
                          fontWeight: FontWeight.bold,
                        ),
                        Expanded(
                          child: TextField(
                            textAlign: TextAlign.right,
                            controller: logic.moneyController,
                            focusNode: logic.moneyFocusNode,
                            keyboardType: const TextInputType.numberWithOptions(decimal: true),
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(RegExp("[0-9.]")), //只允许输入小数
                              MoneyTextInputFormatter(),
                            ],
                            decoration: InputDecoration(
                              hintText: '0.00',
                              hintStyle: TextStyle(
                                color: kAppColor('#BBBBBB'),
                                fontSize: 16.sp,
                              ),
                              border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                              ),
                              contentPadding: EdgeInsets.zero,
                            ),
                            style: TextStyle(
                              color: kAppTextColor,
                              fontSize: 16.sp,
                            ),
                            onChanged: (value) {
                              logic.onChangedMoney(value);
                            },
                          ),
                        ),
                        UIText(
                          margin: EdgeInsets.only(left: 6.w),
                          text: '币',
                          textColor: kAppColor('#E93323'),
                          fontSize: 16.sp,
                          fontWeight: FontWeight.bold,
                        ),
                      ],
                    ),
                    UIContainer(
                      margin: EdgeInsets.only(top: 10.h, bottom: 16.h),
                      radius: 10.w,
                      height: 136.w,
                      alignment: Alignment.center,
                      color: Colors.white,
                      child: TextField(
                          controller: logic.descController,
                          decoration: InputDecoration(
                            hintText: '恭喜发财，大吉大利',
                            hintStyle: TextStyle(color: kAppColor('#BBBBBB'), fontSize: 16.sp),
                            border: InputBorder.none,
                          ),
                          textInputAction: TextInputAction.send,
                          // maxLength: 12,
                          // inputFormatters: [
                          //   LengthLimitingTextInputFormatter(12),
                          //   // NoSpaceTextInoutFormatter(),
                          // ],
                          onSubmitted: (value) {
                            logic.onClickSendBtn();
                          },
                          style: TextStyle(
                            fontSize: 16.sp,
                            color: kAppTextColor,
                          ),
                          textAlign: TextAlign.center),
                    ),
                    GetBuilder<SendRedPersonPageLogic>(builder: (logic) {
                      return RichText(
                          text: TextSpan(
                              text: logic.money.isEmpty ? '0.00' : logic.money,
                              style: TextStyle(fontSize: 46.sp, color: kAppColor('#E93323')),
                              children: [TextSpan(text: '币', style: TextStyle(fontSize: 18.sp))]));
                    })
                  ],
                ),
                UISolidButton(
                  margin: EdgeInsets.only(top: 15.w, bottom: 45.w, left: 82.w, right: 82.w),
                  height: 58.w,
                  text: '发红包',
                  color: kAppColor('#EE8499'),
                  fontSize: 16.sp,
                  radius: 58.w,
                  onTap: () {
                    logic.onClickSendBtn();
                  },
                ),
                // UIText(
                //   margin: EdgeInsets.only(top: 20.h),
                //   text: '本服务由${kAppConfig().appName()}提供',
                //   textColor: kAppColor('#C9C9C9'),
                //   fontSize: 12.sp,
                //   alignment: Alignment.center,
                // )
              ],
            ),
            UIColumn(
              width: double.infinity,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const WhlWhiteAppBar('发红包'),
              ],
            ),
            // GetBuilder<SendRedPersonPageLogic>(builder: (logic) {
            //   return Visibility(
            //     visible: (logic.isExceedAmount),
            //     child: UIText(
            //       margin: EdgeInsets.only(top: ScreenUtil().statusBarHeight + 40.h),
            //       color: kAppColor('#FFD09A'),
            //       width: double.infinity,
            //       height: 30.h,
            //       alignment: Alignment.center,
            //       text: '红包金额不得超过${logic.maxAmount.toStringAsFixed(0)}',
            //       textColor: kAppTextColor,
            //       fontSize: 14.sp,
            //     ),
            //   );
            // }),
            Align(
              alignment: Alignment.bottomCenter,
              child: UIText(
                 text: '共发出0个红包，累计0.00元',
                 textColor: kAppColor('#444444'),
                 fontSize: 14.sp,
                startDrawable: UIImage(
                  assetImage: 'icon_red_pkg_flag.png',
                  width: 23.w,
                ),
                endDrawable: UIImage(
                  assetImage: 'icon_red_pkg_flag.png',
                  width: 23.w,
                )
              ),
            )
          ],
        ),
      ),
    );
  }
}
