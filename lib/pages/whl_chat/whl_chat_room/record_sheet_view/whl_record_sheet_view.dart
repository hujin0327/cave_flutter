import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../widgets/whl_audio_visualizer_widget.dart';
import 'whl_record_sheet_logic.dart';

class WhlRecordSheetPage extends StatefulWidget {
  WhlRecordSheetPage({Key? key}) : super(key: key);

  @override
  State<WhlRecordSheetPage> createState() => _WhlRecordSheetPageState();
}

class _WhlRecordSheetPageState extends State<WhlRecordSheetPage> {
  final logic = Get.put(WhlRecordSheetLogic());

  @override
  void dispose() {
    Get.delete<WhlRecordSheetLogic>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<WhlRecordSheetLogic>(builder: (logic) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          if (logic.isRecording)
            UIText(
              margin: EdgeInsets.only(bottom: 15.w),
              text: logic.isMoveOut?'松手取消':'松手发送',
              textColor: Colors.white,
              fontSize: 14.sp,
              marginDrawable: 11.w,
              topDrawable: UIImage(
                assetImage: logic.isMoveOut ? 'icon_chat_record_cancel_move.png' : 'icon_chat_record_cancel.png',
                width: 60.w,
                height: 60.w,
              ),
            ),
          UIColumn(
            padding: EdgeInsets.only(bottom: ScreenUtil().bottomBarHeight),
            color: logic.isMoveOut ? kAppTextColor.withOpacity(0.7) : Colors.white,
            mainAxisSize: MainAxisSize.min,
            topRightRadius: 12.w,
            topLeftRadius: 12.w,
            children: [
              UIRow(
                margin: EdgeInsets.only(
                  top: 20.w,
                ),
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  if (logic.isRecording)
                    Container(
                      width: 60.w,
                      child: AudioVisualizerWidget(
                        colors: [kAppColor('#E1E2E8'), kAppColor('#E1E2E8'), kAppColor('#E1E2E8'), kAppColor('#E1E2E8')],
                        duration: const [900, 700, 600, 800, 500],
                        barCount: 8,
                      ),
                    ),
                  UIText(
                    text: logic.timeText,
                    textColor: logic.isMoveOut ? Colors.white : kAppTextColor,
                    fontSize: 18.sp,
                    fontWeight: FontWeight.bold,
                  ),
                  if (logic.isRecording)
                    Container(
                      width: 60.w,
                      child: AudioVisualizerWidget(
                        colors: [kAppColor('#E1E2E8'), kAppColor('#E1E2E8'), kAppColor('#E1E2E8'), kAppColor('#E1E2E8')],
                        duration: const [900, 700, 600, 800, 500],
                        barCount: 8,
                      ),
                    ),
                ],
              ),
              GetBuilder<WhlRecordSheetLogic>(builder: (logic) {
                return UIText(
                  margin: EdgeInsets.only(top: 3.h, bottom: 11.h),
                  text: logic.isRecording ? '' : '按住说话',
                  textColor: kAppTextColor,
                  fontSize: 14.sp,
                  fontWeight: FontWeight.bold,
                );
              }),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  UIStack(
                    alignment: Alignment.center,
                    margin: EdgeInsets.symmetric(horizontal: 24.w),
                    clipBehavior: Clip.none,
                    children: [
                      // UIImage(
                      //   assetImage: 'icon_chat_record_record.png',
                      //   width: 100.w,
                      //   height: 100.w,
                      // ),
                      UIContainer(
                        strokeWidth: 3.w,
                        strokeColor: kAppColor('#F5F6F9'),
                        width: 96.w,
                        height: 96.w,
                        radius: 50.w,
                      ),

                      GetBuilder<WhlRecordSheetLogic>(builder: (logic) {
                        if (logic.isRecording) {
                          return UIContainer(
                            width: 66.w,
                            height: 66.w,
                            color: kAppColor('#E1E2E8'),
                            radius: 43.w,
                          );
                        } else {
                          return UIContainer(
                            width: 86.w,
                            height: 86.w,
                            color: kAppTextColor,
                            radius: 43.w,
                          );
                        }
                      }),
                      UIImage(
                        assetImage: 'icon_chat_record_record.png',
                        width: 32.w,
                      ),
                      GetBuilder<WhlRecordSheetLogic>(builder: (logic) {
                        return SizedBox(
                          width: 96.w,
                          height: 96.w,
                          child: CircularProgressIndicator(
                            value: logic.recordIngTime / logic.maxRecordTime,
                            valueColor: AlwaysStoppedAnimation<Color>(logic.isMoveOut ? kAppColor('#FF5656') : kAppTextColor),
                            strokeWidth: 3.w,
                          ),
                        );
                      }),
                    ],
                    onLongPress: () {
                      logic.startRecording();
                    },
                    onLongPressEnd: (e) {
                      logic.stopRecording();
                      // Get.back();
                    },
                    onLongPressMoveUpdate: (e) {
                      bool oldMove = logic.isMoveOut;
                      if (e.localPosition.dy < -120) {
                        logic.isMoveOut = true;
                      } else {
                        logic.isMoveOut = false;
                      }
                      if (oldMove != logic.isMoveOut) {
                        logic.update(['audioRecord']);
                      }
                    },
                  ),
                ],
              ),
            ],
          ),
        ],
      );
    });
  }
}
