import 'package:azlistview/azlistview.dart';
import 'package:cave_flutter/config/whl_global_config.dart';
import 'package:cave_flutter/model/user_model.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../model/friend_model.dart';
import '../../../routes/whl_app_pages.dart';
import '../../../style/whl_style.dart';
import '../../../widgets/brick/brick.dart';
import 'contact_logic.dart';

class ContactPage extends StatelessWidget {
  ContactPage({Key? key}) : super(key: key);

  final logic = Get.put(ContactLogic());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ContactLogic>(builder: (logic) {
      return AzListView(
        data: logic.friendList,
        itemCount: logic.friendList.length,
        itemBuilder: (BuildContext context, int index) {
          UserModel model = logic.friendList[index];
          if (index == 0) {
            return Column(
              children: [
                buildSearchView(),
              ],
            );
          }
          return buildItemView(model);
        },
        susItemHeight: 36,
        susItemBuilder: (BuildContext context, int index) {
          UserModel model = logic.friendList[index];
          if (index == 0) return Container();
          return UIText(
            width: Get.width,
            // margin: EdgeInsets.symmetric(horizontal: 15.w),
            padding: EdgeInsets.only(left: 15.w),
            // radius: 18.w,
            height: 40.h,
            // gradientStartColor: kAppColorModel().rightColor.withOpacity(0.1),
            // gradientEndColor: kAppColorModel().rightColor.withOpacity(0.03),
            alignment: Alignment.centerLeft,
            text: model.initial ?? '',
            textColor: Colors.black,
            fontSize: 13.sp,
            fontWeight: FontWeight.w500,
          );
        },
        indexBarData: SuspensionUtil.getTagIndexList(logic.friendList),
        indexBarOptions: IndexBarOptions(
          needRebuild: true,
          color: Colors.transparent,
          downDecoration: BoxDecoration(
            color: kAppColor("#8F847A").withOpacity(0.1),
            borderRadius: BorderRadius.circular(20.w),
          ),
          // localImages: [logic.imgFavorite], //local images.
        ),
      );
    });
  }

  buildSearchView() {
    return UIRow(
        margin:
            EdgeInsets.only(top: 14.w, left: 18.w, right: 18.w, bottom: 15.w),
        color: Colors.white,
        height: 44.h,
        radius: 30.h,
        children: [
          UIImage(
            margin: EdgeInsets.only(left: 18.w),
            assetImage: 'icon_search.png',
            width: 15.w,
            height: 15.w,
          ),
          Expanded(
              child: TextField(
            controller: logic.searchController,
            onSubmitted: (value) {
              // logic.doClickSearch();
            },
            textInputAction: TextInputAction.search,
            style: TextStyle(fontSize: 14.sp, color: kAppTextColor),
            decoration: InputDecoration(
              hintText: '搜索',
              hintStyle:
                  TextStyle(fontSize: 14.sp, color: kAppColor('#9C9C9C')),
              contentPadding: EdgeInsets.only(left: 10.w, right: 10.w),
              border: OutlineInputBorder(borderSide: BorderSide.none),
            ),
          ))
        ]);
  }

  Column buildItemView(UserModel model) {
    return Column(
      children: [
        UIRow(
          padding: EdgeInsets.symmetric(horizontal: 18.w, vertical: 10.h),
          children: [
            UIImage(
              httpImage: (model.avatar ?? "").toImageUrl(),
              assetPlaceHolder: WHLGlobalConfig.userPlaceholderAvatar,
              width: 42.w,
              height: 42.w,
              radius: 24,
              fit: BoxFit.cover,
              margin: EdgeInsets.only(right: 14.w),
            ),
            UIText(
              text: model.nickName ?? '',
              fontSize: 14.sp,
              fontWeight: FontWeight.bold,
              textColor: kAppTextColor,
              marginDrawable: 4.w,
              endDrawable: true
                  ? UIImage(
                      assetImage: 'icon_vip1.png',
                      width: 30.w,
                    )
                  : null,
            ),
            if (false)
              UIImage(
                margin: EdgeInsets.only(left: 4.w),
                assetImage: 'icon_good_number.png',
                width: 16.w,
              ),
            Expanded(child: Container()),
          ],
          onTap: () {
            logic.getImAcid(model);
            // Get.toNamed(Routes.roomPage, arguments: {'showName': 'showName',"toAccountId":model.accountId})?.then((value) {
            //   // update();
            // });
            // logic.doClickItem(model);
          },
        ),
        Container(
          margin: EdgeInsets.only(left: 70.w),
          height: 0.5.w,
          color: kAppColor('#EBEBEB'),
        )
      ],
    );
  }
}
