import 'package:azlistview/azlistview.dart';
import 'package:cave_flutter/model/user_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:lpinyin/lpinyin.dart';

import '../../../model/friend_model.dart';
import '../../../network/whl_api.dart';
import '../../../routes/whl_app_pages.dart';

class ContactLogic extends GetxController {
  TextEditingController searchController = TextEditingController();

  List<UserModel> friendList = [];
  List<UserModel> selectedList = [];

  @override
  void onInit() {
    super.onInit();
    onGetFriendList();
  }

  onGetFriendList(){
    WhlApi.friendList.get({}).then((value) {
      if (value.isSuccess()) {
        List records = value.data["records"];
        List<UserModel> modelList =
        records.map((e) => UserModel.fromJson(e)).toList();
        friendList = modelList;
        _handleList(friendList);
        update();
      }else {

        update();
      }
    });
  }

  void _handleList(List<UserModel> list) {
    if (list.isNotEmpty) {
      for (int i = 0, length = list.length; i < length; i++) {
        try {
          String pinyin = PinyinHelper.getPinyinE((list[i].nickName ?? ''));
          String tag = pinyin.substring(0, 1).toUpperCase();
          list[i].initial = pinyin;
          if (RegExp("[A-Z]").hasMatch(tag)) {
            list[i].initial = tag;
          } else {
            list[i].initial = "#";
          }
        } catch (e) {
          list[i].initial = "#";
        }
      }
      SuspensionUtil.sortListBySuspensionTag(list);
      // show sus tag.
      SuspensionUtil.setShowSuspensionStatus(friendList);
      friendList.insert(0, UserModel(accountId: '↑', initial: '↑', nickName: ''));
      update();
    } else {
      friendList = [];
      update();
    }
  }
  
  getImAcid(UserModel userModel){
    WhlApi.getChatAcid.get({"accountId":userModel.accountId}).then((value) {
      if (value.isSuccess()) {

        Get.toNamed(Routes.roomPage, arguments: {'showName': 'showName',"toAccountId":value.data,"userModel":userModel});
      }
    });
  }
}
