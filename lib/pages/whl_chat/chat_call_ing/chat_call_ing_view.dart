import 'package:cave_flutter/common/chat_rtc_manager.dart';
import 'package:cave_flutter/common/common_widget.dart';
import 'package:cave_flutter/config/whl_global_config.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/ls_love_progress_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../routes/whl_app_pages.dart';
import '../../../widgets/brick/widget/image_widget.dart';
import '../../../widgets/ripple_animated_widget.dart';
import 'chat_call_ing_logic.dart';

class Chat_call_ingPage extends StatelessWidget {
  Chat_call_ingPage({Key? key}) : super(key: key);

  final logic = Get.put(Chat_call_ingLogic());

  @override
  Widget build(BuildContext context) {
    return PopScope(
        canPop: false,
        onPopInvoked: (canPop) {
          // print(canPop);
          if (canPop) {
            return;
          }
          logic.backAction();
        },
        child: Scaffold(
          body: GetBuilder<Chat_call_ingLogic>(builder: (logic) {
            return Stack(
              children: [
                UIImage(
                  httpImage:logic.userModel?.avatar?.toImageUrl(),
                  assetPlaceHolder: WHLGlobalConfig.userPlaceholderAvatar,
                  width: ScreenUtil().screenWidth,
                  height: ScreenUtil().screenHeight,
                  fit: BoxFit.cover,
                ),
                visualEffectWidget(
                    width: ScreenUtil().screenWidth,
                    height: ScreenUtil().screenHeight,
                    color: kAppColor("#393939"),
                    opacity: 0.77),
                UIColumn(
                  children: [
                    UIRow(
                      margin: EdgeInsets.only(
                          top: ScreenUtil().statusBarHeight + 10.w, left: 10.w),
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        UIImage(
                          padding: EdgeInsets.all(5.w),
                          assetImage: "icon_back_black",
                          imageColor: kAppWhiteColor,
                          width: 30.w,
                          height: 30.w,
                          onTap: () {
                            logic.backAction();
                            // Get.back();
                          },
                        ),
                        Expanded(
                            child: UIText(
                          margin: EdgeInsets.only(left: 10.w, right: 40.w),
                          height: 30.w,
                          text: logic.isCall
                              ? "等待对方接受邀请..."
                              : logic.isVideo
                                  ? "邀请你视频通话..."
                                  : "邀请你语音通话...",
                          fontSize: 16.sp,
                          textColor: kAppWhiteColor,
                          alignment: Alignment.center,
                        ))
                      ],
                    ),
                    UIStack(
                      margin: EdgeInsets.only(top: 64.h, bottom: 17.h),
                      alignment: Alignment.center,
                      children: [
                        UIContainer(
                          width: 230.w,
                          height: 230.w,
                          child: RippleAnimatedWidget(
                            controller: logic.animationController,
                          ),
                        ),
                        UIImage(
                          httpImage:logic.userModel?.avatar?.toImageUrl(),
                          assetPlaceHolder: WHLGlobalConfig.userPlaceholderAvatar,
                          width: 118.w,
                          height: 118.w,
                          radius: 59.w,
                          clipBehavior: Clip.hardEdge,
                          fit: BoxFit.cover,
                        ),
                      ],
                    ),
                    UIText(
                      text: logic.userModel?.nickName,
                      fontSize: 20.sp,
                      textColor: kAppWhiteColor,
                      alignment: Alignment.center,
                    ),
                    Expanded(child: UIContainer()),
                    if (!logic.isCall)
                      UIColumn(
                        padding: EdgeInsets.symmetric(horizontal: 10.w),
                        children: [
                          UIContainer(
                            padding: EdgeInsets.symmetric(
                                horizontal: 15.w, vertical: 8.h),
                            color: kAppWhiteColor.withOpacity(0.72),
                            height: 40.h,
                            radius: 20.h,
                            child: Text(
                              "千里感情一线牵，珍惜咱们这段缘～",
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  color: kAppBlackColor,
                                  overflow: TextOverflow.ellipsis),
                            ),
                          ),
                          UIRow(
                            margin: EdgeInsets.only(top: 30.h, bottom: 100.h),
                            padding: EdgeInsets.symmetric(horizontal: 25.w),
                            children: [
                              Expanded(
                                  child: UIButton("取消",
                                      buttonType: MyButtonType.imageInTop,
                                      titleFont: 14.sp,
                                      titleColor: kAppWhiteColor,
                                      assetImageName: "chat_callGD",
                                      imageWidth: 70.w,
                                      imageHeight: 70.w,
                                      imageTitleSpace: 13.h, onTap: () {
                                logic.backAction();
                              })),
                              Expanded(
                                  child: UIButton("接听",
                                      buttonType: MyButtonType.imageInTop,
                                      titleFont: 14.sp,
                                      titleColor: kAppWhiteColor,
                                      assetImageName: "chat_callJT",
                                      imageWidth: 70.w,
                                      imageHeight: 70.w,
                                      imageTitleSpace: 13.h, onTap: () {
                                        logic.joinRoom(isJT:true);
                              }))
                            ],
                          ),
                        ],
                      ),
                    if (logic.isCall)
                      UIButton("取消",
                          margin: EdgeInsets.only(top: 30.h, bottom: 100.h),
                          titleFont: 14.sp,
                          titleColor: kAppWhiteColor,
                          buttonType: MyButtonType.imageInTop,
                          assetImageName: "chat_callGD",
                          imageWidth: 70.w,
                          imageHeight: 70.w,
                          imageTitleSpace: 15.h, onTap: () {
                            logic.backAction();
                      }),
                  ],
                ),
              ],
            );
          }),
        ));
  }
}
