import 'package:cave_flutter/config/whl_global_config.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../style/whl_style.dart';
import '../../../../widgets/brick/brick.dart';
import '../../../../widgets/whl_button.dart';
import 'new_friend_auth_logic.dart';

class NewFriendAuthPage extends StatelessWidget {
  NewFriendAuthPage({Key? key}) : super(key: key);

  final logic = Get.put(NewFriendAuthLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: WhlAppBar(
        centerTitle: '好友验证',
        backgroundColor: Colors.white,
        isShowBottomLine: false,
      ),
      body: GetBuilder<NewFriendAuthLogic>(builder: (logic) {
        return Column(
          children: [
            UIRow(
              margin: EdgeInsets.only(left: 18.w, top: 15.w),
              children: [
                UIImage(
                  httpImage: logic.applyModel?.avatar?.toImageUrl(),
                  assetPlaceHolder: WHLGlobalConfig.userPlaceholderAvatar,
                  width: 42.w,
                  height: 42.w,
                  radius: 21.w,
                  margin: EdgeInsets.only(right: 13.w),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          UIText(
                            text: logic.applyModel?.nickName,
                            textColor: kAppTextColor,
                            fontSize: 14.sp,
                            fontWeight: FontWeight.bold,
                            marginDrawable: 3.w,
                            endDrawable: UIImage(
                              assetImage: 'icon_level.png',
                              width: 32.w,
                            ),
                          ),
                          UIImage(
                            assetImage: 'icon_add_friend_favorite.png',
                            width: 68.w,
                          )
                        ],
                      ),
                      UIRow(
                        margin: EdgeInsets.only(top: 6.w),
                        children: [
                          UIText(
                            width: 50.w,
                            height: 18.w,
                            alignment: Alignment.center,
                            text: '21岁',
                            color: kAppColor('#FCEDF2'),
                            textColor: kAppColor('#FF73A0'),
                            fontSize: 11.sp,
                            radius: 6.w,
                            marginDrawable: 3.w,
                            startDrawable: UIImage(
                              assetImage: 'icon_sex_woman_1.png',
                              height: 10.w,
                            ),
                          ),
                          UIText(
                            margin: EdgeInsets.only(left: 4.w, right: 4.w),
                            text: '无性恋',
                            textColor: kAppColor('#444444'),
                            constraints: BoxConstraints(minWidth: 50.w),
                            fontSize: 11.sp,
                            radius: 6.w,
                            alignment: Alignment.center,
                            color: kAppColor('#F6F6F6'),
                            height: 18.w,
                            padding: EdgeInsets.symmetric(horizontal: 8.w),
                          ),
                          UIText(
                            margin: EdgeInsets.only(left: 4.w, right: 4.w),
                            text: 'CD',
                            textColor: kAppColor('#444444'),
                            fontSize: 11.sp,
                            alignment: Alignment.center,
                            constraints: BoxConstraints(minWidth: 50.w),
                            radius: 6.w,
                            color: kAppColor('#F6F6F6'),
                            height: 18.w,
                            padding: EdgeInsets.symmetric(horizontal: 8.w),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
            UIRow(
              margin: EdgeInsets.only(top: 22.w, left: 16.w, right: 16.w),
              padding: EdgeInsets.all(15.w),
              height: 175.w,
              radius: 16.w,
              crossAxisAlignment: CrossAxisAlignment.start,
              strokeColor: kAppColor('#DADADA'),
              strokeWidth: 0.5.w,
              children: [
                UIText(
                  text: '对方: ',
                  textColor: kAppColor('#979797'),
                  fontSize: 13.sp,
                  fontWeight: FontWeight.bold,
                ),
                Expanded(
                  child: UIText(
                    text: '你好啊～能认识下吗？',
                    textColor: kAppTextColor,
                    fontSize: 13.sp,
                    fontWeight: FontWeight.bold,
                  ),
                )
              ],
            ),
            UIRow(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              margin: EdgeInsets.only(top: 46.w, left: 75.w, right: 75.w),
              children: [
                UISolidButton(
                  text: '拒绝',
                  width: 80.w,
                  height: 35.w,
                  textColor: kAppColor('#C1C1C1'),
                  fontSize: 13.sp,
                  color: kAppColor('#F4F5F8'),
                  onTap: (){
                    logic.friendHandle(0);
                  },
                ),
                UISolidButton(
                  text: '同意',
                  width: 80.w,
                  height: 35.w,
                  fontSize: 13.sp,
                  onTap: (){
                    logic.friendHandle(1);
                  },
                ),
              ],
            )
          ],
        );
      }),
    );
  }
}
