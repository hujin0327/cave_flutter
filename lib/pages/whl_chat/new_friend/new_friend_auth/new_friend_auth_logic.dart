import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:get/get.dart';

import '../../../../model/apply_model.dart';
import '../../contact/contact_logic.dart';

class NewFriendAuthLogic extends GetxController {
  ApplyModel? applyModel;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();

    applyModel = Get.arguments;
    update();
  }

  friendHandle(int type){
    if (applyModel == null) {
      return;
    }
    WhlApi.applyFriendHandle.post({"applyId":applyModel!.applyId,"operate":type == 1?"ACCEPTED":"DECLINED"}).then((value) {
      if (value.isSuccess()) {
        if (type == 1) {
          final contactLogic = Get.find<ContactLogic>();
          contactLogic.onGetFriendList();
          MyToast.show("添加好友成功");
        }else {
          MyToast.show("已拒绝");
        }
      }
    });
  }
}
