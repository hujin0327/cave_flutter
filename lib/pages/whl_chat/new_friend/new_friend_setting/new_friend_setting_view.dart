import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:cave_flutter/widgets/whl_row_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'new_friend_setting_logic.dart';

class NewFriendSettingPage extends StatelessWidget {
  NewFriendSettingPage({Key? key}) : super(key: key);

  final logic = Get.put(NewFriendSettingLogic());

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = EdgeInsets.symmetric(horizontal: 18.w, vertical: 13.w);
    return Scaffold(
      backgroundColor: kAppColor('#FAFAFA'),
      appBar: WhlAppBar(
        centerTitle: '新朋友设置',
        backgroundColor: kAppColor('#FAFAFA'),
        isShowBottomLine: false,
      ),
      body: Column(
        children: [
          UIColumn(
            color: Colors.white,
            margin: EdgeInsets.only(top: 8.w),
            children: [
              MyRowItem(
                decoration: BoxDecoration(border: Border(bottom: BorderSide(color: kAppColor('#E5E5E5'), width: 0.5.w))),
                padding: padding,
                title: '无需验证直接通过',
                isShowSwitch: true,
                isSwitchOn: true,
              ),
              MyRowItem(
                padding: padding,
                title: '关注后可直接通过',
                isShowSVip: true,
                isShowSwitch: true,
                isSwitchOn: true,
              )
            ],
          ),
          UIColumn(
            color: Colors.white,
            margin: EdgeInsets.only(top: 8.w),
            children: [
              MyRowItem(
                decoration: BoxDecoration(border: Border(bottom: BorderSide(color: kAppColor('#E5E5E5'), width: 0.5.w))),
                padding: padding,
                title: '需要验证才可通过',
                isShowSwitch: true,
                isSwitchOn: true,
              ),
              MyRowItem(
                padding: padding,
                title: '设置礼物才可通过',
                isShowSVip: true,
              )
            ],
          ),
          UIColumn(
            color: Colors.white,
            margin: EdgeInsets.only(top: 8.w),
            children: [
              MyRowItem(
                decoration: BoxDecoration(border: Border(bottom: BorderSide(color: kAppColor('#E5E5E5'), width: 0.5.w))),
                padding: padding,
                title: '只接受实名认证用户的申请（需实名）',
                isShowSwitch: true,
                isSwitchOn: true,
              ),
            ],
          ),
          UIColumn(
            color: Colors.white,
            margin: EdgeInsets.only(top: 8.w),
            children: [
              MyRowItem(
                decoration: BoxDecoration(border: Border(bottom: BorderSide(color: kAppColor('#E5E5E5'), width: 0.5.w))),
                padding: padding,
                title: '拒绝任何人添加好友',
                isShowSVip: true,
                isShowSwitch: true,
                isSwitchOn: true,
              ),
            ],
          ),
          UIColumn(
            color: Colors.white,
            margin: EdgeInsets.only(top: 8.w),
            children: [
              MyRowItem(
                decoration: BoxDecoration(border: Border(bottom: BorderSide(color: kAppColor('#E5E5E5'), width: 0.5.w))),
                padding: padding,
                title: '仅接受主流群体的好友申请',
                isShowSwitch: true,
                isSwitchOn: true,
              ),
            ],
          )
        ],
      ),
    );
  }
}
