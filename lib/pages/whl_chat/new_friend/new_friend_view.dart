import 'package:cave_flutter/config/whl_global_config.dart';
import 'package:cave_flutter/model/apply_model.dart';
import 'package:cave_flutter/routes/whl_app_pages.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'new_friend_logic.dart';

class NewFriendPage extends StatelessWidget {
  NewFriendPage({Key? key}) : super(key: key);

  final logic = Get.put(NewFriendLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: WhlAppBar(
        centerTitle: '新朋友',
        backgroundColor: Colors.white,
        isShowBottomLine: false,
        rightWidget: UIImage(
          margin: EdgeInsets.only(right: 20.w),
          assetImage: 'icon_chat_room_more.png',
          width: 28.w,
          height: 28.w,
          onTap: () {
            Get.toNamed(Routes.newFriendSetting);
          },
        ),
      ),
      body: GetBuilder<NewFriendLogic>(builder: (logic) {
        return CustomScrollView(
          slivers: [
            SliverList(
                delegate: SliverChildBuilderDelegate((context, index) {
                  ApplyModel model = logic.dataList[index];
                  return UIColumn(
                    padding: EdgeInsets.symmetric(
                        horizontal: 18.w, vertical: 12.w),
                    children: [
                      UIRow(
                        children: [
                          UIImage(
                            httpImage: model.avatar?.toImageUrl(),
                            assetPlaceHolder: WHLGlobalConfig.userPlaceholderAvatar,
                            width: 42.w,
                            height: 42.w,
                            radius: 21.w,
                            margin: EdgeInsets.only(right: 13.w),
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    UIText(
                                      text: model.nickName,
                                      textColor: kAppTextColor,
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.bold,
                                      marginDrawable: 3.w,
                                      endDrawable: UIImage(
                                        assetImage: 'icon_level.png',
                                        width: 32.w,
                                      ),
                                    ),
                                    UIImage(
                                      assetImage: 'icon_add_friend_favorite.png',
                                      width: 68.w,
                                    )
                                  ],
                                ),
                                UIRow(
                                  margin: EdgeInsets.only(top: 6.w),
                                  children: [
                                    UIText(
                                      width: 50.w,
                                      height: 18.w,
                                      alignment: Alignment.center,
                                      text: '21岁',
                                      color: kAppColor('#FCEDF2'),
                                      textColor: kAppColor('#FF73A0'),
                                      fontSize: 11.sp,
                                      radius: 6.w,
                                      marginDrawable: 3.w,
                                      startDrawable: UIImage(
                                        assetImage: 'icon_sex_woman_1.png',
                                        height: 10.w,
                                      ),
                                    ),
                                    UIText(
                                      margin: EdgeInsets.only(
                                          left: 4.w, right: 4.w),
                                      text: '无性恋',
                                      textColor: kAppColor('#444444'),
                                      constraints: BoxConstraints(
                                          minWidth: 50.w),
                                      fontSize: 11.sp,
                                      radius: 6.w,
                                      alignment: Alignment.center,
                                      color: kAppColor('#F6F6F6'),
                                      height: 18.w,
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 8.w),
                                    ),
                                    UIText(
                                      margin: EdgeInsets.only(
                                          left: 4.w, right: 4.w),
                                      text: 'CD',
                                      textColor: kAppColor('#444444'),
                                      fontSize: 11.sp,
                                      alignment: Alignment.center,
                                      constraints: BoxConstraints(
                                          minWidth: 50.w),
                                      radius: 6.w,
                                      color: kAppColor('#F6F6F6'),
                                      height: 18.w,
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 8.w),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                          UISolidButton(
                            width: 80.w,
                            height: 35.w,
                            text: '待验证',
                            fontSize: 13.sp,
                            onTap: () {
                              Get.toNamed(Routes.newFriendAuth,arguments: model);
                            },
                          )
                        ],
                      ),
                      UIRow(
                        margin: EdgeInsets.only(top: 18.w),
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          UIText(
                            text: '你好啊～能认识下吗？',
                            textColor: kAppColor('#C1C1C1'),
                            fontSize: 13.sp,
                          ),
                          UIText(
                            margin: EdgeInsets.only(right: 20.w),
                            text: '06-30',
                            textColor: kAppColor('#C1C1C1'),
                            fontSize: 13.sp,
                          ),
                        ],
                      )
                    ],
                  );
                }, childCount: logic.dataList.length))
          ],
        );
      }),
    );
  }
}
