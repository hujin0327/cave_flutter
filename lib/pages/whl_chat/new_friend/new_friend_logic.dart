import 'package:cave_flutter/model/apply_model.dart';
import 'package:cave_flutter/network/whl_api.dart';
import 'package:get/get.dart';

class NewFriendLogic extends GetxController {

  List<ApplyModel> dataList = [];

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();

    getListData();
  }

  getListData(){
    WhlApi.applyFriendList.get({}).then((value) {
      if (value.isSuccess()) {
        List records = value.data["records"];
        List<ApplyModel> modelList =
        records.map((e) => ApplyModel.fromJson(e)).toList();
        dataList = modelList;
        // if (isRefresh) {
        //   listData = modelList;
        //   refreshController.finishRefresh();
        //   refreshController.resetFooter();
        // } else {
        //   listData.addAll(modelList);
        //   refreshController.finishLoad();
        // }
        // if (records.length < size) {
        //   refreshController.finishLoad(IndicatorResult.noMore);
        // }
        update();
      }else {
        // current--;
        // refreshController.finishRefresh();
        // refreshController.finishLoad();
        update();
      }
    });
  }
}
