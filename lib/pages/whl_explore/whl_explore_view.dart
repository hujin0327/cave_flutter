import 'dart:io';

import 'package:cave_flutter/db_manager/bean/whl_product_bean.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../widgets/brick/brick.dart';
import 'whl_explore_logic.dart';

class WhlExplorePage extends StatelessWidget {
  WhlExplorePage({Key? key}) : super(key: key);

  final logic = Get.put(WhlExploreLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(children: [
      const UIImage(
        assetImage: 'img_bg_home.png',
        width: double.infinity,
        height: double.infinity,
        fit: BoxFit.fill,
      ),
      Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        UIImage(
          margin: EdgeInsets.only(top: ScreenUtil().statusBarHeight + 8.h, left: 15.w, bottom: 17.h),
          assetImage: 'txt_app_name.png',
          height: 30.h,
        ),
        buildTypeListView(),
        Expanded(
          child: GetBuilder<WhlExploreLogic>(builder: (logic) {
            if (logic.productList.isEmpty) return Container();
            return CustomScrollView(
              slivers: [
                SliverPadding(
                  padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 10.h),
                  sliver: SliverGrid(
                      delegate: SliverChildBuilderDelegate((context, index) {
                        WhlProductBean model = logic.productList[index];
                        return buildListItemView(model);
                      }, childCount: logic.productList.length),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2, crossAxisSpacing: 10.w, mainAxisSpacing: 10.w, childAspectRatio: 166 / 202)),
                ),
              ],
            );
          }),
        )
      ])
    ]));
  }

  UIContainer buildListItemView(WhlProductBean model) {
    return UIContainer(
      color: Colors.white,
      radius: 4.w,
      child: UIColumn(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (model.cover?.contains('img_art') == true)
            UIImage(
              assetImage: model.cover,
              height: 148.w,
              width: double.maxFinite,
              fit: BoxFit.cover,
              radius: 4.w,
            )
          else
            UIContainer(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(4.w),
                child: Image.file(
                  File(model.cover ?? ''),
                  fit: BoxFit.cover,
                  height: 148.w,
                  width: double.maxFinite,
                ),
              ),
            ),
          UIText(
            margin: EdgeInsets.only(left: 10.w, top: 8.h, right: 10.w),
            text: model.title,
            textColor: kAppTextColor,
            fontSize: 12.sp,
            shrinkWrap: false,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            fontWeight: FontWeight.bold,
          ),
          UIRow(
            margin: EdgeInsets.only(top: 4.h, left: 10.w, right: 10.w),
            children: [
              UIImage(
                httpImage: model.avatar,
                width: 16.w,
                height: 16.w,
                radius: 16.w,
              ),
              Expanded(
                child: UIText(
                  margin: EdgeInsets.only(left: 5.w, right: 5.w),
                  text: model.nickname,
                  textColor: kAppSub2TextColor,
                  fontSize: 10.sp,
                  shrinkWrap: false,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              UIImage(
                assetImage: model.isLike == false ? 'icon_like_no.png' : 'icon_like.png',
                width: 16.w,
                height: 16.w,
                onTap: () {
                  logic.doClickLike(model);
                },
              )
            ],
          )
        ],
      ),
      onTap: () {
        logic.doClickItem(model);
      },
    );
  }

  buildTypeListView() {
    return UIContainer(
        height: 28.h,
        margin: EdgeInsets.only(bottom: 10.h),
        child: ListView.builder(
          padding: EdgeInsets.only(left: 16.w),
          scrollDirection: Axis.horizontal,
          itemCount: logic.typeList.length,
          itemBuilder: (context, index) {
            return GetBuilder<WhlExploreLogic>(builder: (logic) {
              return UIContainer(
                margin: EdgeInsets.only(right: 20.w),
                padding: EdgeInsets.symmetric(horizontal: 16.w),
                alignment: Alignment.center,
                color: Colors.white,
                radius: 22.w,
                gradientStartColor: logic.selectedIndex == index ? kAppLeftColor : null,
                gradientEndColor: logic.selectedIndex == index ? kAppRightColor : null,
                child: UIText(
                  text: logic.typeList[index],
                  fontSize: 16.sp,
                  textColor: logic.selectedIndex == index ? Colors.white : kAppSub2TextColor,
                  fontWeight: logic.selectedIndex == index ? FontWeight.bold : FontWeight.normal,
                ),
                onTap: () {
                  logic.doClickType(index);
                },
              );
            });
          },
        ));
  }
}
