import 'package:cave_flutter/network/dio_log/dio_log.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../widgets/whl_button.dart';
import 'whl_splash_logic.dart';

class WhlSplashPage extends StatefulWidget {
  WhlSplashPage({Key? key}) : super(key: key);

  @override
  State<WhlSplashPage> createState() => _WhlSplashPageState();
}

class _WhlSplashPageState extends State<WhlSplashPage> {
  final logic = Get.put(WhlSplashLogic());

  @override
  void initState() {
    super.initState();
    // showDebugBtn(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(alignment: Alignment.topCenter, children: [
      const UIImage(
        assetImage: 'img_bg_home.png',
        fit: BoxFit.cover,
        width: double.infinity,
        height: double.infinity,
      ),
      UIContainer(
        color: Colors.black.withOpacity(0.3),
        width: double.infinity,
        height: double.infinity,
      ),
      Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
        UIImage(
          margin: EdgeInsets.only(top: 180.h),
          assetImage: 'icon_app_logo.png',
          width: 90.w,
          height: 90.w,
          radius: 24.w,
        ),
        UIText(
          margin: EdgeInsets.only(top: 16.h),
          text: '拥抱每一种人格',
          fontSize: 14.sp,
          textColor: Colors.white,
        ),

        UISolidButton(
          margin: EdgeInsets.only(top: 318.h, left: 45.w, right: 45.w),
          text: '加入Cave',
          height: 46.h,
          color: Colors.black,
          onTap: () {
          },
        ),
      ])
    ]));
  }
}
