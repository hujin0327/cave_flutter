import 'package:cave_flutter/config/whl_global_config.dart';
import 'package:cave_flutter/routes/whl_app_pages.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/droppable_card_widget.dart';
import 'package:cave_flutter/widgets/refresh_widget.dart';
import 'package:extended_nested_scroll_view/extended_nested_scroll_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barrage/flutter_barrage.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../common/common_widget.dart';
import '../../../common/sliver_header_delegate.dart';
import '../../../model/dynamic_model.dart';
import '../../../style/whl_style.dart';
import '../../../widgets/custom_tab_widget.dart';
import 'cave_detail_logic.dart';

class Cave_detailPage extends StatelessWidget {
  Cave_detailPage({Key? key}) : super(key: key);

  GlobalKey myGlobalKey = GlobalKey();
  final logic = Get.put(Cave_detailLogic());

  String getImageBaseUrl = WHLGlobalConfig.getInstance().getImageBaseUrl();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppColor("#F6F6F6"),
      body: GetBuilder<Cave_detailLogic>(builder: (logic) {
        return MyRefresh(
          controller: logic.refreshController,
          onRefresh: () {
            logic.loadSquareData(isRefresh: true);
          },
          onLoad: () {
            logic.loadSquareData();
          },
          child: ExtendedNestedScrollView(
            key: myGlobalKey,
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return [
                SliverAppBar(
                  pinned: true,
                  expandedHeight: 332.w,
                  backgroundColor: kAppColor("#F6F6F6"),
                  flexibleSpace: UIStack(
                    color: kAppColor("#F6F6F6"),
                    children: [
                      UIImage(
                        // assetImage: "icon_home_top_2",
                        httpImage:
                            '$getImageBaseUrl${logic.caveDetail.caveImg}',

                        width: ScreenUtil().screenWidth,
                        height: 220.w,
                        fit: BoxFit.cover,
                      ),
                      visualEffectWidget(
                          width: ScreenUtil().screenWidth,
                          height: 220.w,
                          color: kAppBlackColor),
                      UIImage(
                        margin: EdgeInsets.only(
                            top: ScreenUtil().statusBarHeight + 10.w,
                            left: 10.w),
                        padding: EdgeInsets.all(5.w),
                        assetImage: "icon_back_black",
                        imageColor: kAppWhiteColor,
                        width: 30.w,
                        height: 30.w,
                        onTap: () {
                          Get.back();
                        },
                      ),
                      if (logic.caveRole > 0)
                        Positioned(
                            right: 20.w,
                            top: ScreenUtil().statusBarHeight + 10.w,
                            child: UIImage(
                              assetImage: "cave_more",
                              imageColor: kAppWhiteColor,
                              width: 30.w,
                              height: 30.w,
                              onTap: () {
                                // logic.starBarrage();
                                // Get.to(() => CaveSettingPage());
                                Get.toNamed(Routes.caveSettingPage, arguments: {
                                  'caveId': logic.caveId,
                                  'model': logic.caveDetail
                                });
                              },
                            )),
                      Positioned(
                          left: 0,
                          right: 0,
                          top: ScreenUtil().statusBarHeight + 50.w,
                          height: 80.w,
                          child: BarrageWall(
                            child: UIContainer(),
                            controller: logic.barrageWallController,
                            width: ScreenUtil().screenWidth,
                            height: 80.w,
                            speed: 5,
                            maxBulletHeight: 35.w,
                          )),
                      UIContainer(
                        margin: EdgeInsets.only(
                            top: ScreenUtil().statusBarHeight +
                                50.w +
                                80.w +
                                8.w),
                        color: kAppColor("#F6F6F6"),
                        topRightRadius: 30.w,
                        width: ScreenUtil().screenWidth,
                        height: 120.w,
                      ),
                      caveInfoView(
                        margin: EdgeInsets.only(
                            top: ScreenUtil().statusBarHeight + 50.w + 80.w,
                            bottom: 5.w),
                      )
                    ],
                  ),
                ),
                SliverPersistentHeader(
                    pinned: true,
                    delegate: MySliverHeaderDelegate(
                      maxHeight: 50.w,
                      minHeight: 50.w,
                      child: UIContainer(
                        color: kAppColor("#F6F6F6"),
                        alignment: Alignment.centerLeft,
                        child: CustomTabWidget(
                          tabs: logic.tabList,
                          selectedIndex: 0,
                          onTabSelected: (int value) {
                            logic.selectedTabIndex = value;
                            logic.loadSquareData(isRefresh: true);
                            logic.update();
                          },
                        ),
                      ),
                    )),
                // SliverToBoxAdapter(
                //   child: MediaQuery.removePadding(
                //     removeTop: true,
                //     context: context,
                //     child: ,
                //   ),
                // )
              ];
            },
            body: MediaQuery.removePadding(
              removeTop: true,
              context: context,
              child: ListView.builder(
                itemBuilder: (context, index) {
                  DynamicModel model = logic.listData[index];

                  return UIContainer(
                    padding: EdgeInsets.symmetric(horizontal: 10.w),
                    child: Stack(
                      // clipBehavior: Clip.none,
                      children: [
                        commonSquareView(
                          model: model,
                          isCaveMaster: logic.caveRole == 3,
                          source: DynamicDataSource.Cave,
                          logic: logic,
                          callBack: (type) {
                            if (type == 5) {
                              logic.update();
                            } else if (type == 3) {
//
                            } else if (type == 4) {
                              logic.listData.remove(model);
                              logic.update();
                            } else if (type == 100) {
                              logic.pushTopSetting(model);
                            }
                          },
                        ),
                        if (model.essence == true)
                          Positioned(
                            right: -14.w,
                            top: -8.h,
                            child: Transform.rotate(
                              angle: -3.14 * 450,
                              child: UIText(
                                alignment: Alignment.center,
                                width: 60.w,
                                padding: EdgeInsets.only(
                                  left: 16.w,
                                  top: 6.h,
                                  bottom: 2.h,
                                ),
                                // height: 30.h,
                                text: '精华',
                                fontSize: 10.sp,
                                textColor: kAppWhiteColor,
                                color: kAppColor('#DA4456'),
                              ),
                            ),
                          ),
                      ],
                    ),
                  );
                },
                physics: const NeverScrollableScrollPhysics(),
                itemCount: logic.listData.length,
                shrinkWrap: true,
              ),
            ),
          ),
        );
      }),
      floatingActionButton: GetBuilder<Cave_detailLogic>(builder: (logic) {
        bool hasPostRole = logic.caveRole == 3 &&
                logic.masterPerDetailModel.masterPost == 1 ||
            logic.caveRole == 1 && logic.masterPerDetailModel.adminPost == 1 ||
            (logic.masterPerDetailModel.memberPost == 1 && logic.caveRole == 2);
        if (hasPostRole) {
          return UIRow(
            padding:
                EdgeInsets.only(left: 8.w, right: 18.w, top: 7.w, bottom: 7.w),
            mainAxisAlignment: MainAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            margin: EdgeInsets.only(bottom: 15.w),
            color: kAppColor('#FFE800'),
            radius: 100.w,
            children: [
              UIImage(
                assetImage: 'icon_square_send.png',
                width: 28.w,
                height: 28.w,
              ),
              UIText(
                text: '发动态',
                textColor: kAppTextColor,
                fontSize: 13.sp,
                fontWeight: FontWeight.bold,
              )
            ],
            onTap: () {
              // Get.toNamed(Routes.publish);
              logic.toSendPage();
            },
          );
        }
        return const SizedBox();
      }),
    );
  }

  Widget caveInfoView({EdgeInsetsGeometry? margin}) {
    return UIColumn(
      margin: margin,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UIImage(
              margin: EdgeInsets.only(left: 12.w),
              httpImage: '$getImageBaseUrl${logic.caveDetail.caveImg}',
              // assetImage: 'icon_home_top_2.png',
              width: 75.w,
              height: 75.w,
              radius: 6.w,
              fit: BoxFit.cover,
            ),
            Expanded(
              child: UIColumn(
                margin: EdgeInsets.only(top: 14.w, left: 9.w),
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Container(
                        constraints: BoxConstraints(
                          maxWidth: ScreenUtil().screenWidth - 180.w,
                        ),
                        child: Text(
                          logic.caveDetail.caveName ?? '',
                          style: TextStyle(
                              color: kAppTextColor,
                              fontSize: 17.sp,
                              fontWeight: FontWeight.bold,
                              overflow: TextOverflow.ellipsis),
                        ),
                      ),
                      UIImage(
                        assetImage:
                            'icon_cave_level_${logic.caveDetail.level ?? '1'}.png',
                        margin: EdgeInsets.only(left: 4.w),
                        width: 20.w,
                        height: 20.h,
                      ),
                      if (logic.caveDetail.caveType == "1")
                        UIText(
                          padding: EdgeInsets.symmetric(
                              horizontal: 7.w, vertical: 2.w),
                          margin: EdgeInsets.only(left: 4.w, right: 10.w),
                          text: '官方',
                          textColor: kAppWhiteColor,
                          alignment: Alignment.center,
                          fontSize: 10.sp,
                          radius: 9.w,
                          gradientStartColor: kAppColor("#41B3E4"),
                          gradientEndColor: kAppColor("#1C97EF"),
                        ),
                      // UIText(
                      //   padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 3.w),
                      //   margin: EdgeInsets.only(left: 4.w),
                      //   text: 'Lv${ logic.caveDetail.level ?? '1'}',
                      //   alignment: Alignment.center,
                      //   textColor: kAppTextColor,
                      //   fontSize: 8.sp,
                      //   radius: 7.w,
                      //   color: kAppColor("#FFE9B6"),
                      // ),
                      // UIText(
                      //   padding: EdgeInsets.symmetric(
                      //       horizontal: 7.w, vertical: 2.w),
                      //   margin: EdgeInsets.only(left: 4.w, right: 10.w),
                      //   text: logic.caveDetail.caveType == '1' ? '官方' : '个人',
                      //   textColor: kAppWhiteColor,
                      //   alignment: Alignment.center,
                      //   fontSize: 10.sp,
                      //   radius: 9.w,
                      //   gradientStartColor: kAppColor("#41B3E4"),
                      //   gradientEndColor: kAppColor("#1C97EF"),
                      // ),

                      Expanded(child: Container()),
                    ],
                  ),
                  Row(
                    children: [
                      Expanded(
                          child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          UIText(
                            margin: EdgeInsets.symmetric(vertical: 3.w),
                            text:
                                "CaveID:${logic.caveDetail.caveAccountId ?? ''}",
                            fontSize: 11.sp,
                            textColor: kAppSub3TextColor,
                          ),
                          UIText(
                            text:
                                "${logic.caveDetail.postNum ?? '0'}条帖子   ${logic.caveDetail.essencePostNum ?? '0'}条精华",
                            fontSize: 11.sp,
                            textColor: kAppTwoTextColor,
                          )
                        ],
                      )),
                      UIRow(
                        padding: EdgeInsets.symmetric(horizontal: 8.w),
                        mainAxisSize: MainAxisSize.min,
                        margin: EdgeInsets.only(right: 16.w, top: 8.w),
                        color: kAppColor('#FFE800'),
                        radius: 13.w,
                        height: 26.w,
                        children: [
                          Icon(
                            Icons.people,
                            size: 12.w,
                          ),
                          UIText(
                            margin: EdgeInsets.only(left: 4.w),
                            text:
                                '进入群组（${logic.caveDetail.userNum ?? '0'}/${logic.caveDetail.userNumMax ?? '0'}）',
                            textColor: kAppTextColor,
                            fontSize: 9.sp,
                          )
                        ],
                        onTap: () {
                          logic.toCaveGroup();
                        },
                      )
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
        UIRow(
          margin:
              EdgeInsets.only(top: 16.w, bottom: 10.w, left: 10.w, right: 10.w),
          padding: EdgeInsets.symmetric(
            horizontal: 10.w,
          ),
          color: kAppColor("#FBFBFB"),
          radius: 12.w,
          height: 40.w,
          children: [
            UIText(
              text: 'Cave主：',
              textColor: kAppSub3TextColor,
              fontSize: 13.sp,
            ),
            UIImage(
              margin: EdgeInsets.only(right: 5.w, left: 3.w),
              httpImage: '$getImageBaseUrl${logic.caveDetail.masterImg}',
              width: 18.w,
              height: 18.w,
              radius: 9.w,
              fit: BoxFit.cover,
            ),
            Expanded(
              child: UIText(
                text: logic.caveDetail.masterName ?? '',
                textColor: kAppSub3TextColor,
                fontSize: 13.sp,
              ),
            ),
            UIText(
              margin: EdgeInsets.only(left: 15.w),
              text: '已创建${logic.createDays}天',
              textColor: kAppSub3TextColor,
              fontSize: 12.sp,
            )
          ],
        ),
        UIContainer(
          height: 55.w,
          margin: EdgeInsets.symmetric(horizontal: 10.w),
          child: DroppableCardWidget(
            // isAuto: true,
            children: [
              createAnnouncementCardView(index: 0),
              createAnnouncementCardView(index: 1),
              createAnnouncementCardView(index: 2),
              createAnnouncementCardView(index: 3),
              createAnnouncementCardView(index: 4),
              createAnnouncementCardView(index: 5),
            ],
          ),
        )
      ],
    );
  }

  createAnnouncementCardView({int? index}) {
    return UIRow(
      padding: EdgeInsets.symmetric(horizontal: 10.w),
      height: 45.w,
      decoration: bgShadowViewBoxDecoration(
          radiu: 10.w,
          bgColor: kAppWhiteColor,
          shadowColor: kAppBlackColor.withOpacity(0.04)),
      onTap: () {},
      children: [
        UIText(
          width: 28.w,
          height: 16.w,
          radius: 2.w,
          text: "公告",
          alignment: Alignment.center,
          fontSize: 10.sp,
          textColor: kAppWhiteColor,
          color: kAppColor("#1F1F1F"),
        ),
        Expanded(
            child: UIText(
          margin: EdgeInsets.symmetric(horizontal: 5.w),
          fontSize: 11.sp,
          textColor: kAppTwoTextColor,
          text: logic.caveDetail.announcements ?? '',
          overflow: TextOverflow.ellipsis,
          shrinkWrap: false,
        )),
        UIImage(
          assetImage: "vip_down",
          imageColor: kAppSub3TextColor,
          width: 10.w,
          height: 10.w,
        )
      ],
    );
  }
}
