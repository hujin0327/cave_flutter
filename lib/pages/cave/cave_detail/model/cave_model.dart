class CaveModel {
  int? id;
  String? tid;
  String? caveName = "";
  String? caveImg;
  String? caveType = '0';
  String? createDate;
  String? announcements = "";
  int? essencePostNum;
  num? joinPrice;
  String? masterImg;
  String? masterName;
  int? level;
  int? payJoin;
  int? postNum = 0;
  int? userNum = 0;
  int? userNumMax = 0;
  String? caveAccountId;
  List<dynamic>? usersInfos = [];

  CaveModel({
    this.id,
    this.tid,
    this.caveImg,
    this.caveName,
    this.caveType,
    this.createDate,
    this.announcements,
    this.essencePostNum,
    this.level,
    this.joinPrice,
    this.masterImg,
    this.masterName,
    this.payJoin,
    this.postNum,
    this.userNum,
    this.userNumMax,
    this.usersInfos,
    this.caveAccountId,
  });

  CaveModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    tid = json['tid'];
    caveImg = json['caveImg'];
    caveName = json['caveName'];
    caveType = json['caveType'];
    createDate = json['createDate'];
    announcements = json['announcements'];
    essencePostNum = json['essencePostNum'];
    level = json['level'];
    joinPrice = json['joinPrice'];
    masterImg = json['masterImg'];
    masterName = json['masterName'];
    payJoin = json['payJoin'];
    postNum = json['postNum'] ?? 0;
    userNum = json['userNum'] ?? 0;
    userNumMax = json['userNumMax'] ?? 0;
    usersInfos = json['usersInfos'];
    caveAccountId = json['caveAccountId'];
  }
}
