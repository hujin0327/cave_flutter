import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import '../../../../config/whl_global_config.dart';
import '../../../../model/user_model.dart';
import '../../../../routes/whl_app_pages.dart';
import 'find_dialog_logic.dart';

class FindDialogView extends StatefulWidget {

  bool isSuccess;
  UserModel? userModel;
  UserModel? otherUserModel;
  final Function()? onTapRetry;

  FindDialogView({
    super.key,
    required this.isSuccess,
    this.userModel,
    this.otherUserModel,
    this.onTapRetry
  });

  @override
  State<FindDialogView> createState() => _FindDialogViewState();
}

class _FindDialogViewState extends State<FindDialogView> {

  final logic = Get.put(FindDialogLogic());
  
  @override
  void dispose() {
    Get.delete<FindDialogLogic>();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: UIColumn(
        color: Colors.black.withOpacity(0.3),
        padding: EdgeInsets.only(top: 160.h,left: 4.w,right: 4.w),
        children: [
          UIColumn(
            color: Colors.white,
            radius: 30.w,
            children: [
              UIRow(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  UIImage(
                    margin: EdgeInsets.only(right: 23.w,top: 19.w),
                    assetImage: 'icon_find_close.png',
                    width: 16.w,
                    height: 16.h,
                    onTap: () {
                      Get.back();
                    },
                  ),
                ],
              ),
              UIImage(
                assetImage: widget.isSuccess ? 'icon_find_success.png' : 'icon_find_fail.png',
                width: 104.w,
                height: 28.h,
              ),
              Stack(children: [
                UIColumn(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  padding: EdgeInsets.only(top: 30.h,bottom: 30.h,),
                  children: [
                    UIContainer(
                      margin: EdgeInsets.symmetric(horizontal: 9.w),
                      color: Colors.white,
                      radius: 8.w,
                      child: UIRow(
                        mainAxisAlignment: MainAxisAlignment.center,
                        margin: EdgeInsets.only(left: 15.w, right: 15.w, top: 11.h, bottom: 11.h),
                        alignment: Alignment.center,
                        children: [
                            UIColumn(
                              radius: 8.w,
                              children: [
                                ClipOval(
                                  child: UIImage(
                                    httpImage: widget.userModel?.avatar?.toImageUrl(),
                                    assetPlaceHolder: WHLGlobalConfig.userPlaceholderAvatar,
                                    width: 68.w,
                                    height: 68.h,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                UIText(
                                  margin: EdgeInsets.only(top: 16.h, bottom: 1.5.h),
                                  text: widget.userModel?.nickName,
                                  textColor: Colors.black,
                                  fontSize: 12.sp,
                                  fontWeight: FontWeight.bold,
                                ),
                              ],
                            ),
                          if(widget.isSuccess)
                              UIImage(
                                assetImage: 'icon_bind_line.png',
                                width: 127.w,
                                height: 19.w,
                              ),
                          if(widget.isSuccess)
                              UIColumn(
                              radius: 8.w,
                              children: [
                                ClipOval(
                                  child:  UIImage(
                                    httpImage: widget.otherUserModel?.avatar?.toImageUrl(),
                                    assetPlaceHolder: WHLGlobalConfig.userPlaceholderAvatar,
                                    width: 68.w,
                                    height: 68.h,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                UIText(
                                  margin: EdgeInsets.only(top: 16.h, bottom: 1.5.h),
                                  text: widget.otherUserModel?.nickName,
                                  textColor: Colors.black,
                                  fontSize: 12.sp,
                                  fontWeight: FontWeight.bold,
                                ),
                              ],
                            ),
                        ],
                      ),
                    ),
                  ],
                )
              ]),
              UISolidButton(
                margin: EdgeInsets.only(left: 46.w, right: 46.w, top: 30.h, bottom: 10.h),
                text: widget.isSuccess ? '去聊聊吧' : '再试一次',
                fontSize: 16.sp,
                height: 52.w,
                onTap: () {
                  Get.back();
                  debugPrint('cesfyhjjnj'+widget.otherUserModel!.accountId.toString());
                  if(widget.isSuccess) {
                    Get.toNamed(Routes.roomPage, arguments: {
                      'showName': 'showName',
                      "toAccountId": widget.otherUserModel?.accountId,
                      "userModel": widget.otherUserModel,
                    });
                  } else {
                    //通知刷新
                    widget.onTapRetry!();
                  }
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
