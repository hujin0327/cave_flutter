import 'package:flutter/material.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import '../../../model/user_model.dart';
import '../../../network/whl_api.dart';
import '../../../utils/whl_user_utils.dart';
import '../../../widgets/brick/widget/image_widget.dart';
import '../../../widgets/brick/widget/text_widget.dart';
import 'find_dialog/find_dialog_view.dart';
import 'cave_find_logic.dart';

class CaveFindPage extends StatefulWidget {

  const CaveFindPage({super.key});

  @override
  _CaveFindPageState createState() => _CaveFindPageState();
}

class _CaveFindPageState extends State<CaveFindPage> with SingleTickerProviderStateMixin{

  final logic = Get.put(CaveFindLogic());
  late AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 2000));
    Future.delayed(Duration.zero, () {
      _animationController.repeat();
    });
    loadData();
  }

  loadData () {
    WhlApi.soulFriend.get({}).then((value) {
      if (value.isSuccess()) {
        // //暂停动画
        _animationController.stop(canceled: true);
        // //成功弹窗
        var otherUserModel = UserModel.fromJson(value.data['targetUser']);
        var userModel = UserModel.fromJson(value.data['sourceUser']);
        // otherUserModel.accountId = otherUserModel.userNetim?.netimId;
        // userModel.accountId = userModel.userNetim?.netimId;
        Get.dialog(FindDialogView(
            isSuccess: true,
            userModel: userModel,
            otherUserModel: otherUserModel,
        ));
      } else{
        //失败弹窗
        var userModel = UserModel.fromJson(value.data['sourceUser']);
        Get.dialog(FindDialogView(
            isSuccess: false,
            userModel: userModel,
            onTapRetry: () {
              Future.delayed(Duration.zero, () {
                _animationController.repeat();
              });
              loadData();
            }
        ));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppColor('#FAFAFA'),
      appBar: WhlAppBar(
        centerTitle: '灵魂速配',
        backgroundColor: kAppColor('#FAFAFA'),
        isShowBottomLine: false,
      ),
      body: GetBuilder<CaveFindLogic>(builder: (logic){
        return Column(
          children: [
           Padding(
             padding: EdgeInsets.all(50.w),
             child:  Center(
               child: Stack(
                 alignment:Alignment.center,
                 children: [
                   ClipOval(
                     child: UIImage(
                       width: 338.w,
                       height: 338.h,
                       assetImage: "icon_find_bg.png",
                     ),
                   ),
                   buildTransition(),
                   ClipOval(
                     child: UIImage(
                       width: 90.w,
                       height: 60.h,
                       assetImage: "icon_logo_mini.png",
                     ),
                   ),
                 ],
             ),
           ),),
            UIText(
              margin: EdgeInsets.only(
                  top: 20.h, left: 20.w, right: 20.w, bottom: 43.h),
              height: 54.h,
              radius: 27.h,
              text: "正在为你寻找匹配契合灵魂的TA……",
              fontSize: 14.sp,
              textColor: kAppBlackColor,
              alignment: Alignment.center,
            ),
            UIText(
              margin: EdgeInsets.only(
                  top: 20.h, left: 20.w, right: 20.w, bottom: 43.h),
              height: 33.h,
              width: 111.h,
              radius: 31.h,
              text: "取消匹配",
              fontSize: 12.sp,
              textColor: kAppColor('#070704'),
              color: kAppColor('#eeecf3'),
              alignment: Alignment.center,
              onTap: () {
                Get.back();
              },
            )
          ],
        );
    }),
    );
  }

  RotationTransition buildTransition () {
    return RotationTransition(
      //动画控制器
      turns: _animationController,
      //圆形裁剪
      child: ClipOval(
        //扫描渐变
        child: Container(
          width: 180.w,
          height: 180.h,
          decoration: BoxDecoration(
            //扫描渐变
            gradient: SweepGradient(colors: [
              kAppColor('#65EBD1 ').withOpacity(0.1),
              kAppColor('#65EBD1 ').withOpacity(0.3),
            ]),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    //销毁
    _animationController.dispose();
    super.dispose();
  }

}