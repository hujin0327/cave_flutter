import 'dart:ui';

import 'package:cave_flutter/widgets/3DBall/xball_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../routes/whl_app_pages.dart';
import 'cave_logic.dart';

class CavePage extends StatelessWidget {
  CavePage({Key? key}) : super(key: key);

  final logic = Get.put(CaveLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: CustomScrollView(
        physics: ClampingScrollPhysics(),
        slivers: [
          SliverToBoxAdapter(
            child: ListView(
              physics: ClampingScrollPhysics(),
              shrinkWrap: true,
              children: [
                Container(
                  height: Get.width,
                  child: XBallView(
                    mediaQueryData: MediaQuery.of(context),
                    keywords: ['户外', '球类运动', '养动植物', '线下游戏', '户外', '球类运动', '养动植物', '线下游戏', '户外', '球类运动', '养动植物', '线下游戏'],
                    highlight: [],
                    onClickAction: (Point model) {
                      Get.toNamed(Routes.caveHomePage);
                      // AppNavigator.startFriendInfo(info: UserInfo(userID: model.uid,nickname: model.name));
                    },
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
