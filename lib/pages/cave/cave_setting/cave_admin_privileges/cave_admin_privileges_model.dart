class CaveAdminPrivilegesModel {
  int removeMember;
  int prohibit;
  int caveId;
  int editCaveImg;
  int setPostEssence;
  int groupProhibit;
  int editCaveName;
  int setGuest;
  int delSquare;
  int editCaveNotice;

  CaveAdminPrivilegesModel({
    this.removeMember = 0,
    this.prohibit = 0,
    this.caveId = 0,
    this.editCaveImg = 0,
    this.setPostEssence = 0,
    this.groupProhibit = 0,
    this.editCaveName = 0,
    this.setGuest = 0,
    this.delSquare = 0,
    this.editCaveNotice = 0,
  });

  factory CaveAdminPrivilegesModel.fromJson(Map<String, dynamic> json) {
    return CaveAdminPrivilegesModel(
      removeMember: json['removeMember'] ?? 0,
      prohibit: json['prohibit'] ?? 0,
      caveId: json['caveId'] ?? 0,
      editCaveImg: json['editCaveImg'] ?? 0,
      setPostEssence: json['setPostEssence'] ?? 0,
      groupProhibit: json['groupProhibit'] ?? 0,
      editCaveName: json['editCaveName'] ?? 0,
      setGuest: json['setGuest'] ?? 0,
      delSquare: json['delSquare'] ?? 0,
      editCaveNotice: json['editCaveNotice'] ?? 0,
    );
  }

  Map<String, dynamic> toJson() => {
        'removeMember': removeMember,
        'prohibit': prohibit,
        'caveId': caveId,
        'editCaveImg': editCaveImg,
        'setPostEssence': setPostEssence,
        'groupProhibit': groupProhibit,
        'editCaveName': editCaveName,
        'setGuest': setGuest,
        'delSquare': delSquare,
        'editCaveNotice': editCaveNotice,
      };
}
