import 'package:cave_flutter/pages/cave/cave_setting/cave_admin_privileges/cave_admin_privileges_logic.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_row_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class CaveAdminPrivilegesView extends StatelessWidget {
  CaveAdminPrivilegesView({Key? key}) : super(key: key);
  final logic = Get.put(CaveAdminPrivilegesLogic());

  // body: GetBuilder<CaveSettingLogic>(builder: (logic) {}

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = EdgeInsets.only(left: 0.w, right: 0.w, top: 13.w, bottom: 13.w);
    return UIContainer(
      padding: EdgeInsets.only(left: 16.w, right: 16.w, top: 28.w, bottom: 30.w),
      color: Colors.white,
      topRightRadius: 32.w,
      topLeftRadius: 32.w,
      child: GetBuilder<CaveAdminPrivilegesLogic>(builder: (logic) {
        return ListView(
          shrinkWrap: true,
          children: [
            MyRowItem(
              color: Colors.white,
              padding: padding,
              title: '编辑Cave头像',
              onChanged: () {
                logic.editData(0);
              },
              isShowSwitch: true,
              isSwitchOn: logic.model?.editCaveName == 1,
            ),
            MyRowItem(
              color: Colors.white,
              padding: padding,
              title: '编辑Cave昵称',
              onChanged: () {
                logic.editData(1);
              },
              isShowSwitch: true,
              isSwitchOn: logic.model?.editCaveName == 1,
            ),
            MyRowItem(
              color: Colors.white,
              padding: padding,
              title: '编辑Cave公告',
              onChanged: () {
                logic.editData(2);
              },
              isShowSwitch: true,
              isSwitchOn: logic.model?.editCaveNotice == 1,
            ),
            MyRowItem(
              color: Colors.white,
              padding: padding,
              title: '删除广场动态',
              onChanged: () {
                logic.editData(3);
              },
              isShowSwitch: true,
              isSwitchOn: logic.model?.delSquare == 1,
            ),
            MyRowItem(
              color: Colors.white,
              padding: padding,
              title: '设置精华帖',
              onChanged: () {
                logic.editData(4);
              },
              isShowSwitch: true,
              isSwitchOn: logic.model?.setPostEssence == 1,
            ),
            MyRowItem(
              color: Colors.white,
              padding: padding,
              title: '设置嘉宾',
              onChanged: () {
                logic.editData(5);
              },
              isShowSwitch: true,
              isSwitchOn: logic.model?.setGuest == 1,
            ),
            MyRowItem(
              color: Colors.white,
              padding: padding,
              title: '禁言/解除禁言',
              onChanged: () {
                logic.editData(6);
              },
              isShowSwitch: true,
              isSwitchOn: logic.model?.prohibit == 1,
            ),
            MyRowItem(
              color: Colors.white,
              padding: padding,
              title: '群组全体禁言',
              onChanged: () {
                logic.editData(7);
              },
              isShowSwitch: true,
              isSwitchOn: logic.model?.groupProhibit == 1,
            ),
            MyRowItem(
              color: Colors.white,
              padding: padding,
              title: '移除成员',
              onChanged: () {
                logic.editData(8);
              },
              isShowSwitch: true,
              isSwitchOn: logic.model?.removeMember == 1,
            ),
            UIContainer(
              margin: EdgeInsets.only(bottom: 30.w),
            )
          ],
        );
      }),
    );
  }
}
