import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/pages/cave/cave_setting/cave_admin_privileges/cave_admin_privileges_model.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:get/get.dart';

class CaveAdminPrivilegesLogic extends GetxController {
  CaveAdminPrivilegesModel? model;

  @override
  void onInit() {
    super.onInit();
    /*
    var arguments = Get.arguments;
    if (arguments != null) {
      model = arguments['model'];
    }
    */
  }

  void reloadDate(CaveAdminPrivilegesModel adminPrivilegesModel) {
    model = adminPrivilegesModel;
    update();
  }

  void editData(int type) async {
    var params = {"caveId": model?.caveId ?? ''};
    switch (type) {
      case 0:
        params['editCaveImg'] = model?.editCaveImg == 1 ? 0 : 1;
        break;
      case 1:
        params['editCaveName'] = model?.editCaveName == 1 ? 0 : 1;
        break;
      case 2:
        params['editCaveNotice'] = model?.editCaveNotice == 1 ? 0 : 1;
        break;
      case 3:
        params['delSquare'] = model?.delSquare == 1 ? 0 : 1;
        break;
      case 4:
        params['setPostEssence'] = model?.setPostEssence == 1 ? 0 : 1;
        break;
      case 5:
        params['setGuest'] = model?.setGuest == 1 ? 0 : 1;
        break;
      case 6:
        params['prohibit'] = model?.prohibit == 1 ? 0 : 1;
        break;
      case 7:
        params['groupProhibit'] = model?.groupProhibit == 1 ? 0 : 1;
        break;
      case 8:
        params['removeMember'] = model?.removeMember == 1 ? 0 : 1;
        break;

      default:
        break;
    }

    ResponseData responseData = await WhlApi.saveCavePer.post(params);
    if (responseData.isSuccess()) {
      switch (type) {
        case 0:
          model?.editCaveImg = model?.editCaveImg == 1 ? 0 : 1;
          break;
        case 1:
          model?.editCaveName = model?.editCaveName == 1 ? 0 : 1;
          break;
        case 2:
          model?.editCaveNotice = model?.editCaveNotice == 1 ? 0 : 1;
          break;
        case 3:
          model?.delSquare = model?.delSquare == 1 ? 0 : 1;
          break;
        case 4:
          model?.setPostEssence = model?.setPostEssence == 1 ? 0 : 1;
          break;
        case 5:
          model?.setGuest = model?.setGuest == 1 ? 0 : 1;
          break;
        case 6:
          model?.prohibit = model?.prohibit == 1 ? 0 : 1;
          break;
        case 7:
          model?.groupProhibit = model?.groupProhibit == 1 ? 0 : 1;
          break;
        case 8:
          model?.removeMember = model?.removeMember == 1 ? 0 : 1;
          break;
        default:
          break;
      }
    } else {
      MyToast.show(responseData.msg ?? '获取群主设置信息失败');
    }
    update();
  }
}
