class CaveMasterPerDetailModel {
  int masterPost;
  int memberMoneyPost;
  int adminPost;
  int caveId;
  int memberPost;
  int groupProhibit;
  int id;

  CaveMasterPerDetailModel({
    this.masterPost = 0,
    this.memberMoneyPost = 0,
    this.adminPost = 0,
    this.caveId = 0,
    this.memberPost = 0,
    this.groupProhibit = 0,
    this.id = 0,
  });

  factory CaveMasterPerDetailModel.fromJson(Map<String, dynamic> json) {
    return CaveMasterPerDetailModel(
      masterPost: json['masterPost'],
      memberMoneyPost: json['memberMoneyPost'],
      adminPost: json['adminPost'],
      caveId: json['caveId'],
      memberPost: json['memberPost'],
      groupProhibit: json['groupProhibit'],
      id: json['id'],
    );
  }

  Map<String, dynamic> toJson() => {
        'masterPost': masterPost,
        'memberMoneyPost': memberMoneyPost,
        'adminPost': adminPost,
        'caveId': caveId,
        'memberPost': memberPost,
        'groupProhibit': groupProhibit,
        'id': id,
      };
}
