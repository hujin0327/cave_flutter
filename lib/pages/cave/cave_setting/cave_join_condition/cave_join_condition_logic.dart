import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/pages/cave/cave_setting/cave_join_condition/cave_join_condition_model.dart';
import 'package:cave_flutter/pages/cave/cave_setting/cave_setting_logic.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CaveJoinConditionLogic extends GetxController {
  TextEditingController editingController = TextEditingController();
  CaveJoinConditionModel model = CaveJoinConditionModel();

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  void reloadData(CaveJoinConditionModel joinConditionModel) {
    model = joinConditionModel;
    editingController.text = model.joinPrice > 0 ? '${joinConditionModel.joinPrice}' : '';
    update();
  }

  void onConfirm() async {
    num joinPrice = 0;
    if (editingController.text.isNotEmpty) {
      joinPrice = num.parse(editingController.text);
    }

    Map<String, dynamic> params = model.toJson();
    params["joinPrice"] = joinPrice;
    params["payJoin"] = joinPrice > 0 ? 1 : 0;

    ResponseData responseData = await WhlApi.saveCaveJoinCondition.post(params, withLoading: true);
    if (responseData.isSuccess()) {
      MyToast.show("设置成功");
      model.joinPrice = joinPrice;
      CaveSettingLogic logic = Get.find<CaveSettingLogic>();
      logic.update();
      Get.back();
    } else {
      MyToast.show(responseData.msg ?? "设置失败");
    }
  }
}
