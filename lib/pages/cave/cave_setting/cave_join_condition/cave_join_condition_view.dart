import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_row_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../common/common_widget.dart';
import 'cave_join_condition_logic.dart';

class CaveJoinConditionView extends StatelessWidget {
  CaveJoinConditionView({Key? key}) : super(key: key);

  final logic = Get.put(CaveJoinConditionLogic());

  // body: GetBuilder<CaveSettingLogic>(builder: (logic) {}

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = EdgeInsets.only(left: 0.w, right: 0.w, top: 13.w, bottom: 13.w);

    return UIContainer(
      padding: EdgeInsets.only(left: 16.w, right: 16.w, top: 28.w, bottom: 30.w),
      color: Colors.white,
      topRightRadius: 32.w,
      topLeftRadius: 32.w,
      child: GetBuilder<CaveJoinConditionLogic>(builder: (logic) {
        return ListView(
          shrinkWrap: true,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                UIText(
                  text: '设置加入Cave金额',
                  fontSize: 16.sp,
                  textColor: Colors.black,
                  fontWeight: FontWeight.bold,
                )
              ],
            ),
            _inputItem(hint: '请输入加入金额', title: '加入金额', controller: logic.editingController),
            MyRowItem(
              color: Colors.white,
              padding: padding,
              title: '只接受实名认证用户加入',
              onChanged: () {
                logic.model.idCardUser = logic.model.idCardUser == 1 ? 0 : 1;
              },
              isShowSwitch: true,
              isSwitchOn: logic.model.idCardUser == 1,
            ),
            MyRowItem(
              color: Colors.white,
              padding: padding,
              title: '需要VIP以上才可加入',
              onChanged: () {
                logic.model.vipUser = logic.model.vipUser == 1 ? 0 : 1;
              },
              isShowSwitch: true,
              isSwitchOn: logic.model.vipUser == 1,
            ),
            MyRowItem(
              color: Colors.white,
              padding: padding,
              title: '需要SVIP以上才可加入',
              onChanged: () {
                logic.model.svipUser = logic.model.svipUser == 1 ? 0 : 1;
              },
              isShowSwitch: true,
              isSwitchOn: logic.model.svipUser == 1,
            ),
            MyRowItem(
              color: Colors.white,
              padding: padding,
              title: '需要星钻以上才可加入',
              onChanged: () {
                logic.model.starUser = logic.model.starUser == 1 ? 0 : 1;
              },
              isShowSwitch: true,
              isSwitchOn: logic.model.starUser == 1,
            ),
            MyRowItem(
              color: Colors.white,
              padding: padding,
              title: '需要黑金以上才可加入',
              onChanged: () {
                logic.model.blackGoldUser = logic.model.blackGoldUser == 1 ? 0 : 1;
              },
              isShowSwitch: true,
              isSwitchOn: logic.model.blackGoldUser == 1,
            ),
            UIText(
              margin: EdgeInsets.only(left: 20.w, right: 20.w, top: 30.w, bottom: 10.w),
              text: "确定",
              textColor: kAppWhiteColor,
              fontSize: 16.sp,
              fontWeight: FontWeight.bold,
              color: kAppBlackColor,
              height: 52.h,
              radius: 26.h,
              alignment: Alignment.center,
              onTap: () {
                logic.onConfirm();
              },
            )
          ],
        );
      }),
    );
  }

  _inputItem({required String title, required String hint, required TextEditingController controller}) {
    return UIContainer(
      height: 61.h,
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          UIText(
            text: title,
            textColor: Colors.black,
            fontWeight: FontWeight.bold,
            fontSize: 14.sp,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ConstrainedBox(
                  constraints: BoxConstraints(minWidth: 170.w, maxWidth: 170.w),
                  child: IntrinsicWidth(
                    child: noBorderCTextField(
                      controller: controller,
                      textAlign: TextAlign.right,
                      hint: hint,
                      textColor: kAppSubTextColor,
                      hintTextColor: kAppSub3TextColor,
                      fontSize: 14.sp,
                      fontWeight: FontWeight.bold,
                      hintFontWeight: FontWeight.normal,
                      keyboardType: TextInputType.number,
                      contentPadding: EdgeInsets.only(top: 4.h),
                    ),
                  ))
            ],
          )
        ],
      ),
    );
  }
}
