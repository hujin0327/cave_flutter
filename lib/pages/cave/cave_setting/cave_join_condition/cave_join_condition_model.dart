class CaveJoinConditionModel {
  int svipUser;
  int idCardUser;
  int starUser;
  int blackGoldUser;
  int caveId;
  int vipUser;
  num joinPrice;
  String genders;
  int payJoin;

  CaveJoinConditionModel({
    this.svipUser = 0,
    this.idCardUser = 0,
    this.starUser = 0,
    this.blackGoldUser = 0,
    this.caveId = 0,
    this.vipUser = 0,
    this.joinPrice = 0,
    this.genders = '',
    this.payJoin = 0,
  });

  factory CaveJoinConditionModel.fromJson(Map<String, dynamic> json) {
    return CaveJoinConditionModel(
      svipUser: json['svipUser'] ?? 0,
      idCardUser: json['idCardUser'] ?? 0,
      starUser: json['starUser'] ?? 0,
      blackGoldUser: json['blackGoldUser'] ?? 0,
      caveId: json['caveId'] ?? 0,
      vipUser: json['vipUser'] ?? 0,
      joinPrice: json['joinPrice'] ?? 0,
      genders: json['genders'] ?? '',
      payJoin: json['payJoin'] ?? 0,
    );
  }

  Map<String, dynamic> toJson() => {
        'svipUser': svipUser,
        'idCardUser': idCardUser,
        'starUser': starUser,
        'blackGoldUser': blackGoldUser,
        'caveId': caveId,
        'vipUser': vipUser,
        'joinPrice': joinPrice,
        'genders': genders,
        'payJoin': payJoin,
      };
}
