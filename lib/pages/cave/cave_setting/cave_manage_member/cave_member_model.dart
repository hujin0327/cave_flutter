class CaveMemberModel {
  int id;
  int uid;
  String nickName;
  String avatar;
  int isGuest;
  int isMaster;
  int isAdmin;
  int prohibit;

  CaveMemberModel({
    this.id = 0,
    this.uid = 0,
    this.isMaster = 0,
    this.nickName = '',
    this.isGuest = 0,
    this.avatar = '',
    this.isAdmin = 0,
    this.prohibit = 0,
  });

  factory CaveMemberModel.fromJson(Map<String, dynamic> json) {
    return CaveMemberModel(
      id: json['id'] ?? 0,
      uid: json['uid'] ?? 0,
      isGuest: json['isGuest'] ?? 0,
      isMaster: json['isMaster'] ?? 0,
      nickName: json['nickName'] ?? '',
      avatar: json['avatar'] ?? '',
      isAdmin: json['isAdmin'] ?? 0,
      prohibit: json['prohibit'] ?? 0,
    );
  }

  Map<String, dynamic> toJson() => {
        'uid': uid,
        'isGuest': isGuest,
        'isMaster': isMaster,
        'nickName': nickName,
        'avatar': avatar,
        'isAdmin': isAdmin,
        'prohibit': prohibit,
      };
}
