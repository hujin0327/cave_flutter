import 'package:cave_flutter/config/whl_global_config.dart';
import 'package:cave_flutter/pages/cave/cave_setting/cave_manage_member/cave_manage_member_logic.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/whl_app.dart';
import 'package:cave_flutter/widgets/brick/widget/basic_widget.dart';
import 'package:cave_flutter/widgets/brick/widget/image_widget.dart';
import 'package:cave_flutter/widgets/brick/widget/text_widget.dart';
import 'package:cave_flutter/widgets/refresh_widget.dart';
import 'package:cave_flutter/widgets/search_widget.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:cave_flutter/widgets/whl_row_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'cave_member_model.dart';

class CaveManageMemberPage extends StatelessWidget {
  CaveManageMemberPage({Key? key}) : super(key: key);
  final getImageBaseUrl = WHLGlobalConfig.getInstance().getImageBaseUrl();
  final logic = Get.put(CaveManageMemberLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppWhiteColor,
      appBar: WhlAppBar(
        centerTitle: "成员管理",
        backgroundColor: kAppWhiteColor,
      ),
      body: GetBuilder<CaveManageMemberLogic>(builder: (logic) {
        return UIColumn(children: [
          UIContainer(
            padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.w),
            // decoration: BoxDecoration(border: Border(bottom: BorderSide(color: kAppColor('#E5E5E5'), width: 0.5.w))),
            child: SearchWidget(
              logic.editingController,
              bgColor: kAppColor("#F6F6F6"),
              searchCallback: (searchText) {
                hideKeyboard(context);
                logic.onSearch(searchText);
              },
              onClear: () {
                hideKeyboard(context);
                logic.onClear();
              },
            ),
          ),
          Expanded(
            child: MyRefresh(
              controller: logic.refreshController,
              onRefresh: () {
                logic.pullRefreshData();
              },
              onLoad: () {
                logic.loadMoreData();
              },
              child: ListView.builder(
                itemBuilder: (context, index) {
                  return createListItem(logic.memberList[index]);
                },
                itemCount: logic.memberList.length,
              ),
            ),
          )
        ]);
      }),
    );
  }

  Widget createListItem(CaveMemberModel item) {
    return UIRow(
      margin: EdgeInsets.only(top: 15.h),
      padding: EdgeInsets.symmetric(horizontal: 18.w),
      children: [
        UIImage(
          // assetImage: "icon_home_top_2",
          httpImage: '$getImageBaseUrl${item.avatar}',
          width: 54.w,
          height: 54.w,
          radius: 27.w,
          fit: BoxFit.cover,
        ),
        Expanded(
          child: UIContainer(
            padding: EdgeInsets.symmetric(horizontal: 10.w),
            child: UIText(
              margin: EdgeInsets.only(top: 2.h),
              text: item.nickName ?? '',
              fontSize: 16.sp,
              fontWeight: FontWeight.bold,
              textColor: kAppBlackColor,
            ),
          ),
        ),
        buildSettingButton(item),
      ],
    );
  }

  Widget buildSettingButton(CaveMemberModel item) {
    if (item.isMaster == 1 || // 成员是群主
        (item.isAdmin == 1 && logic.caveRole != 3) || // 成员是管理员且当前用户非群主
        (logic.caveRole == 2 && // 当前用户身份是管理员且没有相关权限
            logic.adminPrivilegesModel?.setGuest == 0 &&
            logic.adminPrivilegesModel?.prohibit == 0 &&
            logic.adminPrivilegesModel?.removeMember == 0)) {
      return const UIContainer();
    }

    return UIText(
      radius: 18.w,
      strokeWidth: 1.w,
      strokeColor: kAppStrokeColor,
      width: 80.w,
      height: 36.w,
      text: "设置",
      alignment: Alignment.center,
      fontSize: 13.sp,
      textColor: kAppBlackColor,
      onTap: () {
        Get.bottomSheet(
          UIContainer(
              padding: EdgeInsets.only(left: 16.w, right: 16.w, top: 28.w, bottom: 30.w),
              color: Colors.white,
              topRightRadius: 32.w,
              topLeftRadius: 32.w,
              child: ListView(
                shrinkWrap: true,
                children: buildSettingItems(item),
              )),
        );
      },
    );
  }

  List<Widget> buildSettingItems(CaveMemberModel item) {
    EdgeInsets padding = EdgeInsets.only(left: 0.w, right: 0.w, top: 8.w, bottom: 8.w);
    List<Widget> items = [];

    var setAdmin = MyRowItem(
      color: Colors.white,
      padding: EdgeInsets.only(left: 0.w, right: 0.w, top: 0.w, bottom: 8.w),
      title: '设置管理员',
      onChanged: () {
        logic.setAdmin(item);
      },
      isShowSwitch: true,
      isSwitchOn: item.isAdmin == 1,
    );

    var setGuest = MyRowItem(
      color: Colors.white,
      padding: padding,
      title: '设置嘉宾',
      onChanged: () {
        logic.setGuest(item);
      },
      isShowSwitch: true,
      isSwitchOn: item.isGuest == 1,
    );

    var setProhibit = MyRowItem(
      color: Colors.white,
      padding: padding,
      title: '禁言/解除禁言',
      onChanged: () {
        logic.setProhibit(item);
      },
      isShowSwitch: true,
      isSwitchOn: item.prohibit == 1,
    );

    var removeMember = UIText(
        margin: EdgeInsets.only(left: 18.w, right: 18.w, top: 20.w),
        text: "移除成员",
        textColor: kAppWhiteColor,
        fontSize: 16.sp,
        fontWeight: FontWeight.bold,
        color: kAppColor("#FF5858"),
        height: 52.h,
        radius: 26.h,
        alignment: Alignment.center,
        onTap: () {
          logic.removeMember(item);
        });

    switch (logic.caveRole) {
      case 2:
        {
          if (logic.adminPrivilegesModel?.setGuest == 1) {
            items.add(setGuest);
          }

          if (logic.adminPrivilegesModel?.prohibit == 1) {
            items.add(setProhibit);
          }

          if (logic.adminPrivilegesModel?.removeMember == 1) {
            items.add(removeMember);
          }
        }
        break;

      case 3:
        items = [setAdmin, setGuest, setProhibit, removeMember];
        break;
      default:
        items = [const UIContainer()];
        break;
    }

    return items;
  }
}
