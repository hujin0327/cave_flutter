import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/widget/basic_widget.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:cave_flutter/widgets/whl_row_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'cave_setting_logic.dart';

class CaveSettingPage extends StatelessWidget {
  CaveSettingPage({Key? key}) : super(key: key);

  final logic = Get.put(CaveSettingLogic());

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = EdgeInsets.only(left: 17.w, right: 13.w, top: 13.w, bottom: 13.w);
    return Scaffold(
      backgroundColor: kAppColor('#FAFAFA'),
      appBar: WhlAppBar(
        centerTitle: 'Cave设置',
        backgroundColor: kAppColor('#FAFAFA'),
        isShowBottomLine: false,
      ),
      body: GetBuilder<CaveSettingLogic>(builder: (logic) {
        return Column(
          children: [
            Expanded(
              child: ListView(
                children: buildListItem(),
              ),
            )
          ],
        );
      }),
    );
  }

  List<Widget> buildListItem() {
    EdgeInsets padding = EdgeInsets.only(left: 17.w, right: 13.w, top: 13.w, bottom: 13.w);
    List<Widget> items = [];

    var editAvatar = MyRowItem(
        color: Colors.white,
        padding: padding,
        title: '编辑Cave头像',
        onTap: () {
          logic.selectPhoto();
        });

    var editNickname = MyRowItem(
        color: Colors.white,
        padding: padding,
        title: '编辑Cave昵称',
        value: logic.model?.caveName ?? '',
        onTap: () {
          logic.goSetNickname();
        });

    var editNotice = MyRowItem(
        color: Colors.white,
        padding: padding,
        title: '编辑Cave公告',
        onTap: () {
          logic.goSetNotice();
        });

    var editjoinCondition = MyRowItem(
        margin: EdgeInsets.only(top: 8.w),
        color: Colors.white,
        padding: padding,
        title: '设置加入Cave金额',
        value: logic.joinConditionModel.joinPrice > 0 ? '${logic.joinConditionModel.joinPrice}C币' : '',
        onTap: () {
          logic.presentJoinConditionView();
        });

    var manageMember = MyRowItem(
        margin: EdgeInsets.only(top: 8.w),
        color: Colors.white,
        padding: padding,
        title: '成员管理',
        onTap: () {
          logic.goManageMemberPage();
        });

    var adminPrivileges = MyRowItem(
        color: Colors.white,
        padding: padding,
        title: '管理员权限设置',
        onTap: () {
          logic.presentCaveAdminPrivilegesView();
        });

    var groupProhibit = MyRowItem(
        margin: EdgeInsets.only(top: 8.w),
        color: Colors.white,
        padding: padding,
        title: '群组全体禁言',
        isShowSwitch: true,
        isSwitchOn: logic.masterPerDetailModel?.groupProhibit == 1,
        onChanged: () {
          logic.saveCavePer(0);
        });

    var masterPost = MyRowItem(
        color: Colors.white,
        padding: padding,
        title: '仅Cave群主发帖',
        isShowSwitch: true,
        isSwitchOn: logic.masterPerDetailModel?.masterPost == 1,
        onChanged: () {
          logic.saveCavePer(1);
        });

    var adminPost = MyRowItem(
        color: Colors.white,
        padding: padding,
        title: '允许管理员发帖',
        isShowSwitch: true,
        isSwitchOn: logic.masterPerDetailModel?.adminPost == 1,
        onChanged: () {
          logic.saveCavePer(2);
        });

    var memberPost = MyRowItem(
        color: Colors.white,
        padding: padding,
        title: '允许Cave成员发帖',
        isShowSwitch: true,
        isSwitchOn: logic.masterPerDetailModel?.memberPost == 1,
        onChanged: () {
          logic.saveCavePer(3);
        });

    var memberMoneyPost = MyRowItem(
        color: Colors.white,
        padding: padding,
        title: '允许Cave成员发布付费贴',
        isShowSwitch: true,
        isSwitchOn: logic.masterPerDetailModel?.memberMoneyPost == 1,
        onChanged: () {
          logic.saveCavePer(4);
        });

    var disbandCave = MyRowItem(
        margin: EdgeInsets.only(top: 8.w, bottom: 40.w),
        color: Colors.white,
        padding: padding,
        title: '解散Cave',
        onTap: () {
          logic.disbandCave();
        });

    var exitCave = MyRowItem(
        color: Colors.white,
        padding: padding,
        title: '退出Cave',
        onTap: () {
          logic.exitCave();
        });

    switch (logic.caveRole) {
      case 0:
        {
          items = [const UIContainer()];
        }
        break;

      case 1:
        {
          // 嘉宾
          items = [exitCave];
        }
        break;

      case 2:
        {
          // 管理员
          if (logic.adminPrivilegesModel.editCaveImg == 1) {
            items.add(editAvatar);
          }

          if (logic.adminPrivilegesModel.editCaveName == 1) {
            items.add(editNickname);
          }

          if (logic.adminPrivilegesModel.editCaveNotice == 1) {
            items.add(editNotice);
          }

          items.add(manageMember);

          if (logic.adminPrivilegesModel.groupProhibit == 1) {
            items.add(groupProhibit);
          }

          items.add(exitCave);
        }
        break;

      case 3:
        {
          // 群主
          items = [
            editAvatar,
            editNickname,
            editNotice,
            editjoinCondition,
            manageMember,
            adminPrivileges,
            groupProhibit,
            masterPost,
            adminPost,
            memberPost,
            memberMoneyPost,
            disbandCave
          ];
        }
        break;

      default:
        break;
    }

    return items;
  }
}
