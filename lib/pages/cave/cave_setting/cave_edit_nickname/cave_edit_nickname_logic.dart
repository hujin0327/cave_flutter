import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/pages/cave/cave_detail/cave_detail_logic.dart';
import 'package:cave_flutter/pages/cave/cave_detail/model/cave_model.dart';
import 'package:cave_flutter/pages/cave/cave_setting/cave_setting_logic.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CaveEditNicknameLogic extends GetxController {
  int caveId = 0;
  CaveModel? model;
  TextEditingController textEditingController = TextEditingController();

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();

    var arguments = Get.arguments;
    if (arguments != null) {
      caveId = arguments['caveId'];
      model = arguments['model'];
    }

    textEditingController.text = model?.caveName ?? '';
  }

  void onConfirm() async {
    if (textEditingController.text.isEmpty) {
      MyToast.show("请输入Cave昵称");
      return;
    }

    ResponseData responseData = await WhlApi.applyCave.post({
      "id": caveId,
      "caveName": textEditingController.text,
    }, withLoading: true);
    if (responseData.isSuccess()) {
      // responseData.data;
      // Get.toNamed(Routes.setterPasswordPage, arguments: params);
      model?.caveName = textEditingController.text;
      MyToast.show("编辑成功");
      final CaveSettingLogic logic = Get.find<CaveSettingLogic>();
      logic.update();

      final Cave_detailLogic detailLogic = Get.find<Cave_detailLogic>();
      detailLogic.update();

      Get.back();
    } else {
      MyToast.show(responseData.msg ?? "编辑失败");
    }
  }
}
