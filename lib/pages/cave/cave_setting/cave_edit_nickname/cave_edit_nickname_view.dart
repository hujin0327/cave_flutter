import 'package:cave_flutter/common/common_widget.dart';
import 'package:cave_flutter/pages/cave/cave_setting/cave_edit_nickname/cave_edit_nickname_logic.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/widget/basic_widget.dart';
import 'package:cave_flutter/widgets/brick/widget/text_widget.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class CaveEditNicknamePage extends StatelessWidget {
  CaveEditNicknamePage({Key? key}) : super(key: key);

  final logic = Get.put(CaveEditNicknameLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppWhiteColor,
      appBar: WhlAppBar(
        backgroundColor: kAppWhiteColor,
        centerTitle: '编辑Cave昵称',
        isShowBottomLine: true,
      ),
      body: UIColumn(
          crossAxisAlignment: CrossAxisAlignment.start,
          color: kAppWhiteColor,
          padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 0.w),
          children: [
            UIContainer(
              padding: EdgeInsets.only(top: 16.w, bottom: 16.w),
              decoration: BoxDecoration(border: Border(bottom: BorderSide(color: kAppColor('#E5E5E5'), width: 0.5.w))),
              child: noBorderCTextField(
                controller: logic.textEditingController,
                hint: "请输入Cave昵称",
                fontSize: 16.sp,
              ),
            ),
            Expanded(child: UIContainer()),
            UIText(
              margin: EdgeInsets.symmetric(horizontal: 20.w, vertical: 60.h),
              text: "确定",
              textColor: kAppWhiteColor,
              fontSize: 16.sp,
              fontWeight: FontWeight.bold,
              color: kAppBlackColor,
              height: 52.h,
              radius: 26.h,
              alignment: Alignment.center,
              onTap: () {
                logic.onConfirm();
              },
            )
          ]),
    );
  }
}
