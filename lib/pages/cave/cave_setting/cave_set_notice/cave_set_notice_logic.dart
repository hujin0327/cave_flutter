import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/pages/cave/cave_detail/cave_detail_logic.dart';
import 'package:cave_flutter/pages/cave/cave_detail/model/cave_model.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CaveSetNoticeLogic extends GetxController {
  int caveId = 0;
  CaveModel? model;
  TextEditingController textEditController = TextEditingController();

  @override
  void onInit() {
    super.onInit();

    var arguments = Get.arguments;
    if (arguments != null) {
      caveId = arguments['caveId'];
      model = arguments['model'];
    }
    textEditController.text = model?.announcements ?? '';
  }

  void setNotice() async {
    /*
    if (textEditController.text.isEmpty) {
      MyToast.show("请输入公告内容");
      return;
    }*/

    ResponseData responseData =
        await WhlApi.saveCaveAnnouncements.post({"id": caveId, 'content': textEditController.text}, withLoading: true);
    if (responseData.isSuccess()) {
      MyToast.show("编辑成功");
      model?.announcements = textEditController.text;
      Cave_detailLogic logic = Get.find<Cave_detailLogic>();
      logic.update();
      Get.back();
    } else {
      MyToast.show(responseData.msg ?? "编辑失败");
    }
  }
}
