import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/widget/text_widget.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'cave_set_notice_logic.dart';

class CaveSetNoticePage extends StatelessWidget {
  CaveSetNoticePage({Key? key}) : super(key: key);

  final logic = Get.put(CaveSetNoticeLogic());

  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = EdgeInsets.only(left: 17.w, right: 13.w, top: 13.w, bottom: 13.w);

    return Scaffold(
        backgroundColor: kAppColor('#ffffff'),
        appBar: WhlAppBar(
          centerTitle: 'Cave公告',
          backgroundColor: kAppColor('#ffffff'),
          isShowBottomLine: false,
        ),
        body: Padding(
          padding: EdgeInsets.all(16.w),
          child: Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Container(
              decoration: BoxDecoration(
                color: const Color(0xFFFAFAFA),
                borderRadius: BorderRadius.circular(4.0),
              ),
              child: SizedBox(
                height: 275.0,
                child: TextField(
                  controller: logic.textEditController,
                  maxLines: null,
                  decoration: const InputDecoration(
                    hintText: '请输入公告内容',
                    border: InputBorder.none,
                    isDense: true,
                    contentPadding: EdgeInsets.all(8.0),
                  ),
                ),
              ),
            ),
            UIText(
              onTap: () {
                logic.setNotice();
              },
              width: double.infinity,
              alignment: Alignment.center,
              margin: EdgeInsets.only(top: 12.h, bottom: 12.h),
              height: 53.h,
              text: '发布',
              fontWeight: FontWeight.bold,
              fontSize: 16.sp,
              color: Colors.black,
              textColor: Colors.white,
              radius: 99.h,
            )
          ]),
        ));
  }
}
