import 'package:cave_flutter/routes/whl_app_pages.dart';
import 'package:extended_nested_scroll_view/extended_nested_scroll_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../common/common_widget.dart';
import '../../../common/sliver_header_delegate.dart';
import '../../../style/whl_style.dart';
import '../../../widgets/brick/widget/basic_widget.dart';
import '../../../widgets/brick/widget/image_widget.dart';
import '../../../widgets/brick/widget/text_widget.dart';
import '../../../widgets/refresh_widget.dart';
import 'package:cave_flutter/config/whl_global_config.dart';

import 'join_cave_logic.dart';

class Join_cavePage extends StatelessWidget {
  Join_cavePage({Key? key}) : super(key: key);

  final logic = Get.put(Join_caveLogic());

  String getImageBaseUrl = WHLGlobalConfig.getInstance().getImageBaseUrl();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<Join_caveLogic>(builder: (logic) {
        return UIColumn(
          gradientStartColor: kAppColor("#8E8E8E").withOpacity(0.6),
          gradientEndColor: kAppColor("#CFCFCF"),
          children: [
            Expanded(
                child: MyRefresh(
              controller: logic.refreshController,
              onRefresh: () {},
              // onLoad: () {},
              child: MediaQuery.removePadding(
                removeTop: true,
                context: context,
                child: ListView(
                  children: [
                    UIStack(
                      // color: kAppColor("#F6F6F6"),
                      children: [
                        UIImage(
                          // assetImage: "icon_home_top_2",
                          httpImage: !logic.caveDetail.caveImg
                                  .toString()
                                  .startsWith('http')
                              ? '$getImageBaseUrl${logic.caveDetail.caveImg}'
                              : logic.caveDetail.caveImg,
                          width: ScreenUtil().screenWidth,
                          height: 220.w,
                          fit: BoxFit.cover,
                        ),
                        visualEffectWidget(
                            width: ScreenUtil().screenWidth,
                            height: 220.w,
                            color: kAppBlackColor),
                        UIImage(
                          margin: EdgeInsets.only(
                              top: ScreenUtil().statusBarHeight + 10.w,
                              left: 10.w),
                          padding: EdgeInsets.all(5.w),
                          assetImage: "icon_back_black",
                          imageColor: kAppWhiteColor,
                          width: 30.w,
                          height: 30.w,
                          onTap: () {
                            Get.back();
                          },
                        ),
                        UIContainer(
                          color: kAppColor("#FEFEFE").withOpacity(0.78),
                          radius: 14.w,
                          margin: EdgeInsets.only(
                              left: 15.w, right: 15.w, top: 110.w),
                          width: ScreenUtil().screenWidth,
                          height: 250.w,
                        ),
                        caveInfoView(
                          margin: EdgeInsets.only(
                              top: 102.w, bottom: 5.w, left: 15.w),
                        )
                      ],
                    ),
                    UIContainer(
                      color: kAppColor("#FEFEFE").withOpacity(0.78),
                      constraints: BoxConstraints(
                        minHeight: 200.h,
                      ),
                      radius: 14.w,
                      margin:
                          EdgeInsets.only(left: 15.w, right: 15.w, top: 10.w),
                      padding: EdgeInsets.symmetric(
                          horizontal: 13.w, vertical: 19.h),
                      width: ScreenUtil().screenWidth,
                      child: UIColumn(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          UIText(
                            margin: EdgeInsets.only(bottom: 15.h),
                            width: 28.w,
                            height: 16.w,
                            radius: 2.w,
                            text: "公告",
                            alignment: Alignment.center,
                            fontSize: 10.sp,
                            textColor: kAppWhiteColor,
                            color: kAppColor("#1F1F1F"),
                          ),
                          Text(
                            logic.caveDetail?.announcements ?? '暂无公告',
                            style: TextStyle(
                              color: kAppTwoTextColor,
                              fontSize: 12.sp,
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            )),
            UIRow(
              margin: EdgeInsets.only(
                  left: 19.w, right: 19.w, top: 28.h, bottom: 40.h),
              height: 54.h,
              color: kAppBlackColor,
              radius: 27.h,
              children: [
                logic.needPay == true
                    ? Expanded(
                        child: UIContainer(
                        child: Center(
                          child: Text.rich(TextSpan(
                              text: "¥${logic.caveDetail.joinPrice ?? '0'}C币",
                              style: TextStyle(
                                  color: kAppColor("#F7E1C1"),
                                  fontSize: 16.sp,
                                  fontWeight: FontWeight.bold),
                              children: [
                                TextSpan(
                                    text: " 永久加入",
                                    style: TextStyle(
                                      color: kAppWhiteColor,
                                      fontSize: 11.sp,
                                    ))
                              ])),
                        ),
                      ))
                    : Expanded(
                        child: UIText(
                        text: "免费加入",
                        textColor: kAppColor("#F7E1C1"),
                        fontSize: 16.sp,
                        alignment: Alignment.center,
                      )),
                UIText(
                  height: 54.h,
                  width: 124.w,
                  radius: 27.h,
                  text: "申请加入",
                  textColor: kAppBlackColor,
                  color: kAppColor("#FFE800"),
                  fontSize: 16.sp,
                  alignment: Alignment.center,
                  onTap: () {
                    // logic.needPay = !(logic.needPay??false);
                    // logic.update();
                    logic.applyJoin(context);
                    // Get.toNamed(Routes.caveRedEnvelopePage);
                  },
                )
              ],
            )
          ],
        );
      }),
    );
  }

  Widget caveInfoView({EdgeInsetsGeometry? margin}) {
    // return GetBuilder<Join_caveLogic>(builder: (logic) {

    List<Widget>? userInfoWidget = logic.caveDetail.usersInfos?.map((e) {
      return Expanded(
        child: columProgressView(
          title: e['title'],
          progress: e['num'],
          rate: e['rate']?.toString(),
        ),
      );
    }).toList();
    return UIColumn(
      margin: margin,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UIImage(
                margin: EdgeInsets.only(left: 12.w),
                // assetImage: 'icon_home_top_2.png',
                httpImage:
                    !logic.caveDetail.caveImg.toString().startsWith('http')
                        ? '$getImageBaseUrl${logic.caveDetail.caveImg}'
                        : logic.caveDetail.caveImg,
                width: 75.w,
                height: 75.w,
                radius: 6.w,
                fit: BoxFit.cover),
            Expanded(
              child: UIColumn(
                margin: EdgeInsets.only(top: 14.w, left: 9.w),
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Container(
                        constraints: BoxConstraints(
                          maxWidth: ScreenUtil().screenWidth - 200.w,
                        ),
                        child: Text(
                          logic.caveDetail.caveName ?? '',
                          style: TextStyle(
                              color: kAppTextColor,
                              fontSize: 17.sp,
                              fontWeight: FontWeight.bold,
                              overflow: TextOverflow.ellipsis),
                        ),
                      ),
                      // UIText(
                      //   padding: EdgeInsets.symmetric(
                      //       horizontal: 4.w, vertical: 3.w),
                      //   margin: EdgeInsets.only(left: 4.w),
                      //   text: 'Lv2',
                      //   alignment: Alignment.center,
                      //   textColor: kAppTextColor,
                      //   fontSize: 8.sp,
                      //   radius: 7.w,
                      //   color: kAppColor("#FFE9B6"),
                      // ),
                      UIImage(
                        assetImage:
                            'icon_cave_level_${logic.caveDetail.level ?? '1'}.png',
                        margin: EdgeInsets.only(left: 4.w),
                        width: 20.w,
                        height: 20.h,
                      ),
                      if (logic.caveDetail.caveType == "1")
                        UIText(
                          padding: EdgeInsets.symmetric(
                              horizontal: 7.w, vertical: 2.w),
                          margin: EdgeInsets.only(left: 4.w, right: 10.w),
                          text: '官方',
                          textColor: kAppWhiteColor,
                          alignment: Alignment.center,
                          fontSize: 10.sp,
                          radius: 9.w,
                          gradientStartColor: kAppColor("#41B3E4"),
                          gradientEndColor: kAppColor("#1C97EF"),
                        ),
                      Expanded(child: Container()),
                    ],
                  ),
                  UIRow(
                    children: [
                      UIText(
                        margin: EdgeInsets.symmetric(vertical: 3.w),
                        text: "CaveID:${logic.caveDetail.id ?? ''}  |",
                        fontSize: 11.sp,
                        textColor: kAppSub3TextColor,
                      ),
                      // UIImage(
                      //   margin: EdgeInsets.symmetric(horizontal: 4.w),
                      //   assetImage: "",
                      //   width: 11.w,
                      //   height: 10.w,
                      // ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 4.w),
                        child: Icon(
                          Icons.people,
                          size: 12.w,
                          color: kAppColor("#21415F"),
                        ),
                      ),
                      UIText(
                        text:
                            "群组（${logic.caveDetail.userNum}/${logic.caveDetail.userNumMax})",
                        fontSize: 11.sp,
                        textColor: kAppColor("#21415F"),
                      )
                    ],
                  ),
                  UIText(
                    text:
                        "${logic.caveDetail.postNum}条帖子   ${logic.caveDetail.essencePostNum}条精华",
                    fontSize: 11.sp,
                    textColor: kAppTwoTextColor,
                  ),
                ],
              ),
            )
          ],
        ),
        UIRow(
          margin: EdgeInsets.only(top: 14.w, left: 4.w, right: 20.w),
          padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 7.w),
          height: 52.w,
          decoration: BoxDecoration(
              border: Border(
                  top: BorderSide(color: kAppLineColor, width: 1.w),
                  bottom: BorderSide(color: kAppLineColor, width: 1.w))),
          children: [
            UIText(
              text: 'Cave主：',
              textColor: kAppTextColor,
              fontSize: 14.sp,
            ),
            UIImage(
              margin: EdgeInsets.only(right: 5.w, left: 3.w),
              // assetImage: 'icon_home_top_2.png',
              httpImage:
                  !logic.caveDetail.masterImg.toString().startsWith('http')
                      ? '$getImageBaseUrl${logic.caveDetail.masterImg}'
                      : logic.caveDetail.masterImg,
              width: 38.w,
              height: 38.w,
              radius: 19.w,
            ),
            Expanded(
              child: UIText(
                text: logic.caveDetail.masterName,
                textColor: kAppTextColor,
                fontSize: 14.sp,
              ),
            ),
            UIText(
              margin: EdgeInsets.only(left: 15.w),
              text: '已创建${logic.createDays}天',
              textColor: kAppTextColor,
              fontSize: 12.sp,
            )
          ],
        ),
        UIColumn(
          crossAxisAlignment: CrossAxisAlignment.start,
          padding: EdgeInsets.symmetric(horizontal: 14.w, vertical: 11.w),
          children: [
            UIText(
              text: 'Cave成员信息',
              textColor: kAppTextColor,
              fontSize: 12.sp,
            ),
            UIRow(
              children: userInfoWidget ?? [],
            )
          ],
        )
      ],
    );
    // });
  }

  Widget columProgressView(
      {String? title, int? progress = 0, int? total = 0, String? rate}) {
    double _rate = (double.tryParse(rate ?? '0') ?? 0) / 100;
    return UIContainer(
      margin: EdgeInsets.only(top: 11.w, right: 25.w),
      height: 64.w,
      child: UIRow(
        // mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          UIStack(
            alignment: Alignment.bottomCenter,
            children: [
              UIContainer(
                width: 14.w,
                height: 64.w,
                radius: 7.w,
                color: kAppColor("#F5EDD3"),
              ),
              UIContainer(
                width: 14.w,
                height: 64.w * _rate,
                radius: 7.w,
                color: kAppColor("#FED122"),
              )
            ],
          ),
          UIColumn(
            margin: EdgeInsets.only(left: 9.w),
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              UIText(
                text: title,
                textColor: kAppTextColor,
                fontSize: 12.sp,
              ),
              UIText(
                margin: EdgeInsets.only(top: 2.w),
                text: "$progress人",
                textColor: kAppSub3TextColor,
                fontSize: 10.sp,
              ),
              Expanded(
                  child: UIText(
                text: rate,
                textColor: kAppTextColor,
                fontSize: 14.sp,
                alignment: Alignment.bottomLeft,
              )),
            ],
          )
        ],
      ),
    );
  }
}
