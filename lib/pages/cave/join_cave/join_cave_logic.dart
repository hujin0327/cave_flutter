import 'package:cave_flutter/pages/cave/cave_detail/model/cave_model.dart';
import 'package:easy_refresh/easy_refresh.dart';
import 'package:extended_nested_scroll_view/extended_nested_scroll_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';

import 'package:get/get.dart';

import 'package:cave_flutter/network/whl_api.dart';

class Join_caveLogic extends GetxController {
  EasyRefreshController refreshController = EasyRefreshController(
      controlFinishRefresh: true, controlFinishLoad: true);
  GlobalKey<ExtendedNestedScrollViewState> myGlobalKey =
      GlobalKey<ExtendedNestedScrollViewState>();

  bool? needPay = false;

  int caveId = 0;

  CaveModel caveDetail = CaveModel();

  String createDays = '';

  @override
  void onInit() {
    super.onInit();
    var arguments = Get.arguments;
    if (arguments != null) {
      caveId = arguments['id'];
    }
    getCaveDetail(caveId);
    update();
  }

  getCaveDetail(int id) async {
    ResponseData responseData = await WhlApi.getCaveDetail.get({'caveId': id});
    if (responseData.isSuccess()) {
      caveDetail = CaveModel.fromJson(responseData.data);
      if (responseData.data['payJoin'] != null) {
        if (responseData.data['payJoin'] == 1) {
          needPay = true;
        } else {
          needPay = false;
        }
      }
    }
    update();
    getDateDays();
  }

  getDateDays() {
    DateTime givenTime = DateTime.parse(caveDetail.createDate.toString());
    DateTime currentTime = DateTime.now();

    Duration duration = currentTime.difference(givenTime);

    createDays = duration.inDays.toString();
    update();
  }

  applyJoin(BuildContext context) async {
    ResponseData responseData =
        await WhlApi.applyJoinCave.get({'caveId': caveId, 'desc': ''});

    if (responseData.isSuccess()) {
      MyToast.show('申请成功');
      Navigator.pop(context);
    }
  }
}
