import 'package:flutter/cupertino.dart';
import 'package:cave_flutter/network/whl_api.dart';

import 'package:get/get.dart';

class Cave_searchLogic extends GetxController {

  TextEditingController editingController = TextEditingController();

  List<dynamic> searchResult = [];
  List<dynamic> recommondList = [];

  @override
  void onInit() {
    super.onInit();
  }

  onSearch(String text) async {
    print(text);

    ResponseData responseData = await WhlApi.searchCave.post({
      'caveName': text
    });

    if (responseData.isSuccess()) {
      searchResult = responseData.data['records'];
      print(responseData.data['records'].toString());
    }

    update();
  }
}
