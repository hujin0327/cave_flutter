import 'package:cave_flutter/common/list_view_group.dart';
import 'package:cave_flutter/config/whl_global_config.dart';
import 'package:cave_flutter/routes/whl_app_pages.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/search_widget.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'cave_search_logic.dart';

class Cave_searchPage extends StatelessWidget {
  Cave_searchPage({Key? key}) : super(key: key);

  final logic = Get.put(Cave_searchLogic());

  String getImageBaseUrl = WHLGlobalConfig.getInstance().getImageBaseUrl();

  @override
  Widget build(BuildContext context) {
    ListViewGroupHandler groupHandler = getGroupHandler();

    return Scaffold(
      backgroundColor: kAppWhiteColor,
      appBar: WhlAppBar(
        backgroundColor: kAppWhiteColor,
        isBack: false,
        isShowBottomLine: false,
        centerWidget: UIContainer(
          margin: EdgeInsets.only(right: 45.w),
          child: SearchWidget(logic.editingController, bgColor: kAppColor("#F6F6F6"), searchCallback: (searchText) {
            logic.onSearch(searchText);
            logic.update();
            print(searchText);
          }),
        ),
        rightWidget: UIText(
          margin: EdgeInsets.only(right: 15.w),
          text: "取消",
          textColor: kAppSub3TextColor,
          fontSize: 16.sp,
          onTap: () {
            Get.back();
          },
        ),
      ),
      body: GetBuilder<Cave_searchLogic>(builder: (logic) {
        return ListView.builder(
          itemBuilder: (context, index) {
            return groupHandler.cellAtIndex(index);
          },
          itemCount: groupHandler.allItemCount,
        );
      }),
    );
  }

  ListViewGroupHandler getGroupHandler() {
    return ListViewGroupHandler(
        numberOfSections: 2,
        numberOfRowsInSection: (section) {
          return section == 1 ? logic.recommondList.length : logic.searchResult.length;
        },
        cellForRowAtIndexPath: (indexPath) {
          if (indexPath.section == 0) {
            return createListItem(logic.searchResult[indexPath.row]);
          } else {
            return createListItem(logic.recommondList[indexPath.row]);
          }
        },
        headerForSection: (section) {
          if (section == 0) {
            return logic.searchResult.length > 0
                ? UIText(
                    margin: EdgeInsets.only(top: 18.h, left: 18.w),
                    text: "搜索结果",
                    fontSize: 14.sp,
                    fontWeight: FontWeight.bold,
                    textColor: kAppBlackColor,
                  )
                : Container();
          } else {
            return logic.recommondList.length > 0
                ? UIText(
                    margin: EdgeInsets.only(top: 18.h, left: 18.w),
                    text: "相关Cave推荐",
                    fontSize: 14.sp,
                    fontWeight: FontWeight.bold,
                    textColor: kAppBlackColor,
                  )
                : Container();
          }
        });
  }

  Widget createListItem(dynamic item) {
    return UIRow(
      margin: EdgeInsets.only(top: 15.h),
      padding: EdgeInsets.symmetric(horizontal: 18.w),
      children: [
        UIImage(
          // assetImage: "icon_home_top_2",
          httpImage: !item['caveImg'].toString().startsWith('http') ? '$getImageBaseUrl${item['caveImg']}' : item['caveImg'],
          width: 54.w,
          height: 54.w,
          radius: 27.w,
          fit: BoxFit.cover,
          onTap: () {
            if (item['join'] == true) {
              Get.toNamed(Routes.caveGroupChat, 
                arguments: {
                  'id': item['id'],
                  'tid': item['tid'],
                  'type': item['caveType'],
                  'caveName': item['caveName'],
                  'showName':'showName',
                });
              // Get.toNamed(Routes.caveDetailPage, arguments: {'id': item['id']});
            } else {
              Get.toNamed(Routes.caveJoinPage, arguments: {'id': item['id']});
            }
          },
        ),
        Expanded(
            child: UIColumn(
          padding: EdgeInsets.symmetric(horizontal: 10.w),
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UIText(
              margin: EdgeInsets.only(top: 2.h),
              text: item['caveName'] ?? '',
              fontSize: 16.sp,
              fontWeight: FontWeight.bold,
              textColor: kAppBlackColor,
            ),
            UIText(
              margin: EdgeInsets.only(top: 8.h),
              text: "",
              fontSize: 14.sp,
              textColor: kAppTwoTextColor,
            )
          ],
        )),
        UIText(
          radius: 18.w,
          strokeWidth: 1.w,
          strokeColor: kAppStrokeColor,
          width: 80.w,
          height: 36.w,
          text: "进入",
          alignment: Alignment.center,
          fontSize: 13.sp,
          textColor: kAppBlackColor,
          onTap: () {
            if (item['join'] == true) {
              Get.toNamed(Routes.caveDetailPage, arguments: {'id': item['id']});
            } else {
              Get.toNamed(Routes.caveJoinPage, arguments: {'id': item['id']});
            }
          },
        )
      ],
    );
  }
}
