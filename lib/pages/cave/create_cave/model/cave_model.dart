class CaveModel {
    int? id;
    String caveName = "";
    String? caveImg;
    int caveType = 0;
    String? caveCategories = "";
    String? caveCategoriesDesc = "";
    String? content;
    List<String>? qualificationImages;


    CaveModel({
      this.id,
      this.caveName = "",
      this.caveImg,
      this.caveType = 0,
      this.caveCategories = "",
      this.caveCategoriesDesc = "",
      this.content,
      this.qualificationImages
    });
}
