import 'package:cave_flutter/common/common_edit_widget.dart';
import 'package:cave_flutter/common/upload_photo_widget.dart';
import 'package:cave_flutter/model/base_source_model.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:cave_flutter/widgets/whl_row_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:cave_flutter/config/whl_global_config.dart';

import '../../../common/list_view_group.dart';

import 'create_cave_logic.dart';

class Create_cavePage extends StatelessWidget {
  Create_cavePage({Key? key}) : super(key: key);

  final logic = Get.put(Create_caveLogic());

  String getImageBaseUrl = WHLGlobalConfig.getInstance().getImageBaseUrl();

  @override
  Widget build(BuildContext context) {
    ListViewGroupHandler groupHandler = getGroupHandler();
    return Scaffold(
      backgroundColor: kAppColor("#FDFDFD"),
      appBar: WhlAppBar(
        backgroundColor: kAppColor("#FDFDFD"),
        isShowBottomLine: false,
      ),
      body: Column(
        children: [
          Expanded(
            child: GetBuilder<Create_caveLogic>(builder: (logic) {
              return ListView.builder(
                itemBuilder: (context, index) {
                  return groupHandler.cellAtIndex(index);
                },
                itemCount: groupHandler.allItemCount,
                shrinkWrap: true,
              );
            }),
          ),
          UIText(
            margin: EdgeInsets.only(
                top: 20.h, left: 20.w, right: 20.w, bottom: 43.h),
            height: 54.h,
            radius: 27.h,
            text: "提交",
            fontSize: 16.sp,
            textColor: kAppWhiteColor,
            color: kAppBlackColor,
            alignment: Alignment.center,
            onTap: () {
              // print(logic.dataSource[5].datas.toString());
              logic.createCave(context);
            },
          )
        ],
      ),
    );
  }

  ListViewGroupHandler getGroupHandler() {
    return ListViewGroupHandler(
      numberOfRowsInSection: (section) {
        return logic.dataSource.length;
      },
      cellForRowAtIndexPath: (indexPath) {
        BaseItemModel model = logic.dataSource[indexPath.row];
        return createListItem(model);
      },
    );
  }

  Widget createListItem(BaseItemModel model) {
    if (model.itemType == BaseItemTypeENUM.ENUM_headerCell) {
      return createHeaderCell(model);
    } else if (model.itemType == BaseItemTypeENUM.ENUM_InputCell) {
      return UIRow(
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        margin: EdgeInsets.only(top: 20.w),
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          UIText(
            margin: EdgeInsets.only(right: 14.w, top: 13.h),
            text: 'Cave名称',
            textColor: kAppSubTextColor,
            fontSize: 13.sp,
          ),
          Expanded(
              child: UIContainer(
            height: 40.w,
            radius: 7.w,
            alignment: Alignment.center,
            clipBehavior: Clip.hardEdge,
            child: TextField(
              controller: logic.caveNameController,
              textInputAction: TextInputAction.done,
              style: TextStyle(fontSize: 13.sp, color: kAppSubTextColor),
              decoration: InputDecoration(
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.only(right: 8.w, left: 8.w),
                  hintText: '请输入Cave名称',
                  hintStyle:
                      TextStyle(fontSize: 13.sp, color: kAppSub3TextColor),
                  fillColor: kAppColor('#FAFAFA'),
                  filled: true),
            ),
          ))
        ],
      );
    } else if (model.itemType == BaseItemTypeENUM.ENUM_MoreInputCell) {
      return logic.caveModel?.caveType == 1
          ? UIRow(
              padding: EdgeInsets.symmetric(horizontal: 20.w),
              margin: EdgeInsets.only(top: 10.w),
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                UIText(
                  margin: EdgeInsets.only(right: 14.w, top: 13.h),
                  text: '申请内容',
                  textColor: kAppSubTextColor,
                  fontSize: 13.sp,
                ),
                Expanded(
                    child: UIContainer(
                  height: 110.w,
                  radius: 7.w,
                  alignment: Alignment.center,
                  clipBehavior: Clip.hardEdge,
                  constraints: BoxConstraints(maxHeight: 96.w),
                  child: TextField(
                    maxLines: 6,
                    controller: logic.contentController,
                    textInputAction: TextInputAction.done,
                    style: TextStyle(fontSize: 13.sp, color: kAppSubTextColor),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.only(right: 8.w, left: 8.w),
                      hintText: '请详细说明创建的目的，如何完善自己的cave，未来发展方向等。',
                      hintStyle:
                          TextStyle(fontSize: 13.sp, color: kAppSub3TextColor),
                      fillColor: kAppColor('#FAFAFA'),
                      filled: true,
                    ),
                  ),
                ))
              ],
            )
          : Container();
    } else if (model.itemType == BaseItemTypeENUM.ENUM_SelectCell) {
      return mySelectItemView(model.title ?? "",
          margin: EdgeInsets.only(top: 20.h),
          hintString: model.hitString,
          value: model.value,
          rightImageName: model.imageName,
          imageHeight: 10.w,
          imageWidth: 10.w,
          imageColor: kAppTwoTextColor, onTap: () {
        Get.bottomSheet(logic.caveModel?.caveType == 1
            ? openOfficialCategory(model)
            : openPersonCategory(model));
      });
    } else if (model.itemType == BaseItemTypeENUM.ENUM_SelectTypeCell) {
      return createSelectTypeView(model);
    } else if (model.itemType == BaseItemTypeENUM.ENUM_AddCell) {
      return logic.caveModel?.caveType == 1
          ? createUploadPhotoView(model)
          : Container();
    }
    return Container();
  }

  Widget createHeaderCell(BaseItemModel model) {
    return UIColumn(
      children: [
        logic.avatarUrl == ''
            ? UIImage(
                width: 72.w,
                height: 72.w,
                assetImage: "icon_home_top_2",
                radius: 36.w,
                fit: BoxFit.cover,
                onTap: () {
                  logic.selectPhoto();
                },
              )
            : UIImage(
                width: 72.w,
                height: 72.w,
                httpImage: '$getImageBaseUrl${logic.avatarUrl!}',
                radius: 36.w,
                fit: BoxFit.cover,
                onTap: () {
                  logic.selectPhoto();
                },
              ),
        UIText(
          margin: EdgeInsets.only(top: 12.h, bottom: 20.h),
          text: model.title,
          fontSize: 14.sp,
          textColor: kAppSubTextColor,
        ),
        UIText(
          text: model.hitString,
          fontSize: 10.sp,
          textColor: kAppSub3TextColor,
        ),
      ],
    );
  }

  Widget createSelectTypeView(BaseItemModel model) {
    return mySelectTypeView(
      margin: EdgeInsets.only(top: 20.h),
      title: model.title,
      spacing: 16.w,
      runSpacing: 10.w,
      selectWidgets: [
        for (int i = 0; i < model.datas!.length; i++)
          UIText(
            padding: EdgeInsets.all(12.w),
            alignment: Alignment.center,
            radius: 7.w,
            color: kAppColor("#FAFAFA"),
            text: model.datas![i]['text'],
            fontSize: 12.sp,
            strokeWidth:
                model.datas![i]['value'] == logic.caveModel?.caveType ? 1.w : 0,
            strokeColor: model.datas![i]['value'] == logic.caveModel?.caveType
                ? Colors.black
                : Colors.transparent,
            textColor: kAppSub3TextColor,
            onTap: () {
              logic.caveModel?.caveType = model.datas![i]['value'];
              logic.update();
            },
          ),
      ],
    );
  }

  Widget createUploadPhotoView(BaseItemModel model) {
    return UIColumn(
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      crossAxisAlignment: CrossAxisAlignment.start,
      margin: EdgeInsets.only(top: 20.h),
      children: [
        UIRow(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(right: 14.w),
              child: Text(
                model.title ?? "",
                style: TextStyle(
                  color: kAppSubTextColor,
                  fontSize: 13.sp,
                ),
              ),
            ),
            Expanded(
                child: UIText(
              text: model.hitString,
              textColor: kAppSub3TextColor,
              fontSize: 10.sp,
              shrinkWrap: false,
            )),
          ],
        ),
        UIContainer(
          margin: EdgeInsets.only(left: 68.w, top: 14.h, bottom: 30.h),
          child: UploadPhotoWidget(
              photoData: model.datas,
              photoChangeCallBack: (List<dynamic> list) {
                print('change');
                print(list.toString());
              }),
        )
      ],
    );
  }

  Widget openPersonCategory(BaseItemModel model) {
    return UIContainer(
        padding:
            EdgeInsets.only(left: 16.w, right: 16.w, top: 34.w, bottom: 30.w),
        color: Colors.white,
        topRightRadius: 30.w,
        topLeftRadius: 30.w,
        child: logic.personCategory.length > 0
            ? ListView.builder(
                scrollDirection: Axis.vertical,
                itemBuilder: (BuildContext context, int index) {
                  return UIRow(
                    onTap: () {
                      logic.caveModel?.caveCategories =
                          logic.personCategory[index]['id'].toString();
                      logic.caveModel?.caveCategoriesDesc =
                          logic.personCategory[index]['name'];
                      model.value = logic.personCategory[index]['name'];
                      logic.update();
                      Navigator.pop(context);
                    },
                    decoration: const BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                      width: 1.0,
                      color: Colors.black12,
                    ))),
                    children: [
                      UIContainer(
                        height: 40.w,
                        child: UIText(
                            alignment: Alignment.center,
                            text: logic.personCategory[index]['name']),
                      )
                    ],
                  );
                },
                itemCount: logic.personCategory.length)
            : Container());
  }

  Widget openOfficialCategory(BaseItemModel model) {
    return UIContainer(
        padding:
            EdgeInsets.only(left: 16.w, right: 16.w, top: 34.w, bottom: 30.w),
        color: Colors.white,
        topRightRadius: 30.w,
        topLeftRadius: 30.w,
        child: logic.officialCategory.length > 0
            ? ListView.builder(
                scrollDirection: Axis.vertical,
                itemBuilder: (BuildContext context, int index) {
                  return UIRow(
                    onTap: () {
                      logic.caveModel?.caveCategories =
                          logic.officialCategory[index]['id'].toString();
                      logic.caveModel?.caveCategoriesDesc =
                          logic.officialCategory[index]['name'];
                      model.value = logic.officialCategory[index]['name'];
                      logic.update();
                      Navigator.pop(context);
                    },
                    decoration: const BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                      width: 1.0,
                      color: Colors.black12,
                    ))),
                    children: [
                      UIContainer(
                        height: 40.w,
                        child: UIText(
                            alignment: Alignment.center,
                            text: logic.officialCategory[index]['name']),
                      )
                    ],
                  );
                },
                itemCount: logic.officialCategory.length)
            : Container());
  }

  // Widget openOfficialCategory(BaseItemModel model) {
  //   return GetBuilder<Create_caveLogic>(builder: (logic) {
  //     return UIContainer(
  //       padding: EdgeInsets.only(left: 0.w, right: 0.w, top: 34.w, bottom: 0.w),
  //       color: Colors.white,
  //       topRightRadius: 30.w,
  //       topLeftRadius: 30.w,
  //       child: logic.officialCategory.length > 0
  //           ? UIRow(
  //               children: [
  //                 Container(
  //                   width: 100.w,
  //                   child: ListView.builder(
  //                     itemBuilder: (BuildContext context, int index) {
  //                       return UIRow(
  //                         padding: EdgeInsets.only(
  //                             left: 16.w, right: 0.w, top: 0.w, bottom: 0.w),
  //                         color: logic.officialCategoryActive ==
  //                                 logic.officialCategory[index]['id']
  //                             ? kAppColor('#F4F4F4')
  //                             : Colors.white,
  //                         onTap: () {
  //                           if (logic.officialCategory[index]['id'] ==
  //                               logic.officialCategoryActive) {
  //                             return;
  //                           }

  //                           logic.officialCategoryActive =
  //                               logic.officialCategory[index]['id'].toString();
  //                           logic.getOfficialSecondCategory();
  //                           logic.update();

  //                           // print(logic.officialCategoryActive);
  //                           // print(logic.officialSecondCategory.toString());
  //                         },
  //                         children: [
  //                           UIContainer(
  //                             height: 40.w,
  //                             child: UIText(
  //                                 textColor: logic.officialCategoryActive ==
  //                                         logic.officialCategory[index]['id']
  //                                     ? kAppColor('#000000')
  //                                     : kAppColor('#9C9C9C'),
  //                                 alignment: Alignment.center,
  //                                 text: logic.officialCategory[index]['name']),
  //                           )
  //                         ],
  //                       );
  //                     },
  //                     itemCount: logic.officialCategory.length,
  //                   ),
  //                 ),
  //                 Expanded(
  //                   child: UIContainer(
  //                       padding: EdgeInsets.only(
  //                           left: 19.w, right: 19.w, top: 0.w, bottom: 0.w),
  //                       color: kAppColor('#F4F4F4'),
  //                       child: ListView.builder(
  //                           itemBuilder: (BuildContext context, int index) {
  //                             return UIRow(
  //                               onTap: () {
  //                                 logic.caveModel?.caveCategories = logic
  //                                     .officialSecondCategory[index]['id']
  //                                     .toString();
  //                                 logic.caveModel?.caveCategoriesDesc = logic
  //                                     .officialSecondCategory[index]['name'];
  //                                 model.value = logic
  //                                     .officialSecondCategory[index]['name'];
  //                                 logic.update();
  //                                 Navigator.pop(context);
  //                               },
  //                               children: [
  //                                 UIContainer(
  //                                   height: 40.w,
  //                                   child: UIText(
  //                                       alignment: Alignment.center,
  //                                       textColor: logic.caveModel
  //                                                   ?.caveCategories ==
  //                                               logic.officialSecondCategory[
  //                                                   index]['id']
  //                                           ? kAppColor('#000000')
  //                                           : kAppColor('#9C9C9C'),
  //                                       text:
  //                                           logic.officialSecondCategory[index]
  //                                               ['name']),
  //                                 )
  //                               ],
  //                             );
  //                           },
  //                           itemCount: logic.officialSecondCategory.length)),
  //                 )
  //               ],
  //             )
  //           : Container(),
  //     );
  //   });
  // }
}
