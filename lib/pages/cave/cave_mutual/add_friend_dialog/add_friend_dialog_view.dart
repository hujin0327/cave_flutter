import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../widgets/brick/brick.dart';
import 'add_friend_dialog_logic.dart';

class AddFriendDialog extends StatefulWidget {
  AddFriendDialog({Key? key}) : super(key: key);

  @override
  State<AddFriendDialog> createState() => _AddFriendDialogState();
}

class _AddFriendDialogState extends State<AddFriendDialog> {
  final logic = Get.put(AddFriendDialogLogic());

  @override
  void dispose() {
    super.dispose();
    Get.delete<AddFriendDialogLogic>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: GetBuilder<AddFriendDialogLogic>(builder: (logic) {
        return Stack(
          children: [
            UIWrap(
              margin: EdgeInsets.only(top: 133.h, left: 31.w, right: 31.w),
              runAlignment: WrapAlignment.center,
              wrapAlignment: WrapAlignment.center,
              color: Colors.white,
              radius: 22.w,
              children: [
                UIRow(
                  margin: EdgeInsets.only(top: 57.h),
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    UIText(
                      text: logic.userModel != null
                          ? logic.userModel!.nickName
                          : 'Tom',
                      textColor: kAppTextColor,
                      fontSize: 22.sp,
                      fontWeight: FontWeight.bold,
                    ),
                    UIImage(
                      margin: EdgeInsets.only(left: 4.w),
                      assetImage: 'icon_level.png',
                      width: 30.w,
                    ),
                    // UIImage(
                    //   margin: EdgeInsets.only(left: 4.w),
                    //   assetImage: 'icon_svip.png',
                    //   width: 51.w,
                    // )
                  ],
                ),
                UIRow(
                  mainAxisAlignment: MainAxisAlignment.center,
                  margin: EdgeInsets.only(top: 2.w, bottom: 14.w),
                  children: [
                    UIText(
                      margin: EdgeInsets.only(right: 5.w),
                      padding: EdgeInsets.only(
                          left: 6.w, right: 5.w, top: 3.w, bottom: 2.w),
                      alignment: Alignment.center,
                      text: '21岁',
                      textColor: kAppColor('#FF73A0'),
                      fontSize: 8.sp,
                      strokeColor: kAppColor('#FF73A0').withOpacity(0.5),
                      strokeWidth: 1.w,
                      radius: 18.w,
                      marginDrawable: 1.w,
                      startDrawable: UIImage(
                        assetImage: 'icon_sex_woman_1.png',
                        height: 10.w,
                      ),
                    ),
                    RichText(
                        text: TextSpan(
                            text: '/ ',
                            style: TextStyle(
                              color: kAppColor('#979797'),
                              fontSize: 10.sp,
                            ),
                            children: [
                              TextSpan(
                                text: '双性恋',
                                style: TextStyle(
                                  color: kAppTextColor,
                                  fontSize: 10.sp,
                                ),
                              ),
                              TextSpan(
                                text: ' / ',
                                style: TextStyle(
                                  color: kAppColor('#979797'),
                                  fontSize: 10.sp,
                                ),
                              ),
                              TextSpan(
                                text: 'Maso',
                                style: TextStyle(
                                  color: kAppTextColor,
                                  fontSize: 10.sp,
                                ),
                              ),
                            ])),
                  ],
                ),
                UIContainer(
                  margin: EdgeInsets.only(
                      left: 21.w, right: 21.w, bottom: 20.w),
                  radius: 10.w,
                  strokeColor: kAppTextColor,
                  strokeWidth: 1.w,
                  child: TextField(
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(
                          left: 15.w, top: 15.w, right: 10.w, bottom: 10.w),
                      hintText: '可编辑30个字...',
                      hintStyle: TextStyle(
                          color: kAppSub2TextColor, fontSize: 14.sp),
                      border: const OutlineInputBorder(
                          borderSide: BorderSide.none),
                    ),
                    style: TextStyle(color: kAppTextColor, fontSize: 12.sp),
                    maxLength: 30,
                    maxLines: 5,
                    onChanged: (str) {
                      // logic.onCheckPostBtnEnable();
                    },
                  ),
                ),
                GetBuilder<AddFriendDialogLogic>(builder: (logic) {
                  return Visibility(
                    visible: logic.isShowGift,
                    child: UIContainer(
                      margin: EdgeInsets.only(left: 21.w, bottom: 20.w),
                      height: 87.w,
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: [
                          for (int i = 0; i < 8; i++)
                            GetBuilder<AddFriendDialogLogic>(builder: (logic) {
                              return UIStack(
                                children: [
                                  UIColumn(
                                    strokeColor: logic.selectedIndex == i
                                        ? kAppColor('#FFE800')
                                        : Colors.white,
                                    strokeWidth: 1.5.w,
                                    radius: 10.w,
                                    width: 72.w,
                                    margin: EdgeInsets.only(right: 10.w),
                                    children: [
                                      UIImage(
                                        assetImage: 'icon_gift_1.png',
                                        width: 45.w,
                                      ),
                                      UIText(
                                        text: '心动礼物',
                                        textColor: kAppTextColor,
                                        fontSize: 12.sp,
                                      ),
                                      UIText(
                                        text: '15洞币',
                                        textColor: kAppTextColor,
                                        fontSize: 12.sp,
                                      )
                                    ],
                                  ),
                                  if (logic.selectedIndex == i) UIImage(
                                      assetImage: 'icon_gift_selected.png',
                                      width: 25.w)
                                ],
                                onTap: () {
                                  logic.selectedIndex = i;
                                  logic.update();
                                },
                              );
                            })
                        ],
                      ),
                    ),
                  );
                }),
                UIRow(
                  margin: EdgeInsets.only(
                      left: 24.w, right: 24.w, bottom: 25.w),
                  children: [
                    Expanded(
                      child: UISolidButton(
                        height: 50.w,
                        text: '直接申请',
                        fontSize: 16.sp,
                        onTap: (){
                          logic.addFriendRequest();
                        },
                      ),
                    ),
                    SizedBox(width: 8.w),
                    Expanded(
                      child: UISolidButton(
                        color: kAppColor('#FFE800'),
                        height: 50.w,
                        text: logic.isShowGift ? '发送礼物' : '心动礼物',
                        textColor: kAppTextColor,
                        fontSize: 16.sp,
                        onTap: () {
                          logic.doClickGift();
                        },
                      ),
                    )
                  ],
                )
              ],
            ),
            Align(
              alignment: Alignment.topCenter,
              child: UIImage(
                margin: EdgeInsets.only(top: 96.w),
                httpImage: '',
                assetImage: 'icon_app_logo.png',
                width: 90.w,
                radius: 45.w,
                height: 90.w,
                strokeColor: Colors.white,
                strokeWidth: 3.w,
              ),
            ),
            Align(
              alignment: Alignment.topRight,
              child: UIContainer(
                margin: EdgeInsets.only(top: 150.w, right: 50.w),
                child: Icon(Icons.close, color: kAppColor('#D9D9D9')),
                onTap: () {
                  Get.back();
                },
              ),
            )
          ],
        );
      }),
    );
  }
}
