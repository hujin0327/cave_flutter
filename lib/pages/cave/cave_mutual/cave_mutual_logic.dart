import 'package:cave_flutter/model/other_user_model.dart';
import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/utils/whl_current_limiting_util.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:cave_flutter/utils/whl_user_utils.dart';
import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../model/dynamic_model.dart';
import '../../../model/user_model.dart';
import '../../../routes/whl_app_pages.dart';

import 'package:cave_flutter/style/whl_style.dart';


class CaveMutualoLogic extends GetxController with GetSingleTickerProviderStateMixin{

  UserModel account;
  List listData = [];
  int size = 10;
  int current = 1;

  UserModel? userModel;

  CaveMutualoLogic(this.account);

  @override
  void onInit() {
    super.onInit();
    debugPrint('Initial value is:accountId: ${account.accountId}');
    // getUserDynamicsList(isRefresh: true, accountId: account.accountId);
    // getUserDetail(accountId);
  }

  getGenderIcon(gender) {
    String icon = '';
    if (gender == '0' ||
        gender == '4' ||
        gender == '5') {
      icon = 'icon_sex_man_1.png';
    }

    if (gender == '1' ||
        gender == '3' ||
        gender == '6') {
      icon = 'icon_sex_woman_1';
    }

    if (gender == '2' || gender == '7') {
      icon = 'icon_sex_lgbtq_common_0_0';
    }

    return icon;
  }

  getStatusIcon(status) {
    String icon = '';
    if (status == 1) {
      icon = 'icon_auth_status_2.png';
    }

    if (status == 2) {
      icon = 'icon_auth_status_3.png';
    }

    if (status == 3) {
      icon = 'icon_auth_status_1.png';
    }

    return icon;
  }

  Color getGenderColor() {
    String color = '';
    if (userModel?.gender == '0' ||
        userModel?.gender == '4' ||
        userModel?.gender == '5') {
      color = '#33A9FF';
    }

    if (userModel?.gender == '1' ||
        userModel?.gender == '3' ||
        userModel?.gender == '6') {
      color = '#FF73A0';
    }

    if (userModel?.gender == '2' || userModel?.gender == '7') {
      color = '#000000';
    }
    return kAppColor(color);
  }


  getUserDetail(accountId){
    debugPrint('getUserDetail: $accountId');
    WhlApi.getPersonDetail.get({"accountId": accountId},withLoading: true).then((value) {
      if (value.isSuccess()) {
        userModel = UserModel.fromJson(value.data);
        
        update();
      }
    });
  }


  addVisitLog(){
    WhlApi.addVisit.get({"accountId": account.accountId}).then((value) {
      if (value.isSuccess()) {
        print("添加访问记录成功");
      }
    });
  }

  attentionRequest(){
    '${WhlApi.userFollow}$account.accountId'.get({},withLoading: true).then((value) {
      if (value.isSuccess()) {
        MyToast.show("关注成功");
      }
    });
  }


  setFriendStart () {
    WhlApi.setFriendStart.get({"friendId": account.id, "userId": WhlUserUtils.getUserInfo().userId}).then((value) {
      if (value.isSuccess()) {
        print("交互成功");
        MyToast.show("添加好友成功");
      }
    });
  }

  setAccount (UserModel account) {
    account = account;
  }

}
