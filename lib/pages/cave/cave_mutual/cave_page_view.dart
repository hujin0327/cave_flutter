import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../model/user_model.dart';
import '../../../network/whl_api.dart';
import 'cave_mutual_view.dart';

class CavePageView extends StatefulWidget {

  const CavePageView({super.key});

  @override
  State<CavePageView> createState() => _CavePageViewState();
}

class _CavePageViewState extends State<CavePageView> {

  List listData = [];
  List<Widget> pages = [];
  late PageController _pageController;
  late int _currentPage;

  @override
  void initState() {
    super.initState();
    _currentPage = 0;
    _pageController = PageController(initialPage: _currentPage);
    loadData();
  }

  loadData () {
    WhlApi.soulPeoples.get({}).then((value) {
        List dataList = [];
        if (value.isSuccess()){
          if (value.data != null) {
            List data = value.data['list'];
            dataList = data.map((e) => UserModel.fromJson(e)).toList();
            setState(() {
              listData = dataList;
            });
          }
        }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        PageView.builder(
          scrollDirection: Axis.horizontal,
          controller: _pageController,
          itemCount: listData.length,
          itemBuilder: (BuildContext context, int index) {
            debugPrint('indexitemBuilder'+listData[index].accountId.toString());
            return CaveMutualPage(listData[index], key: Key("mutual${listData[index].accountId}"));
          },
          onPageChanged: (int page) {
            debugPrint('indexonPageChanged'+listData[page].accountId.toString());
            if (page != _currentPage) {
              // 更新tag值
              if (mounted) {
                setState(() {
                  _currentPage = page;
                });
              }
            }
          },
        ),
        Positioned(
            top: 10.h,
            right: 0,
            left: 0,
            height: 80.h,
            child: UIRow(
              color: Colors.transparent,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              margin: EdgeInsets.only(right: 10.w, left: 10.w),
              children: [
                UIImage(
                  assetImage: 'icon_back_gray.png',
                  width: 24.w,
                  height: 24.h,
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
                UIRow(
                    children: [
                      for (int i = 0; i < listData.length; i++)
                        _currentPage == i ?
                           UIContainer(
                            width: 19,
                            height: 4,
                            margin: EdgeInsets.all(1.5),
                            decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(8.r),
                              ),
                          ):
                          UIContainer(
                            width: 4,
                            height: 4,
                            margin: const EdgeInsets.all(1.5),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: _currentPage == i
                                    ? Colors.amberAccent
                                    : const Color.fromRGBO(0, 0, 0, 0.23)),
                          )
                    ]
                ),
                SizedBox(),
              ],
            )
        ),
      ],
    );
  }

  getAllRowView(data){
    List<Widget> children = [];
    for (int i = 0; i < data.length; ++i) {
      children.add(CaveMutualPage(data[i]));
    }
    setState(() {
      pages = children;
    });
  }


}
