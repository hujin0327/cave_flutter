import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cave_flutter/model/dynamic_model.dart';
import 'package:cave_flutter/model/image_model.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import '../../../common/common_action.dart';
import '../../../common/common_widget.dart';
import '../../../common/sliver_header_delegate.dart';
import '../../../model/user_model.dart';
import '../../../network/whl_api.dart';
import '../../../network/whl_result_data.dart';
import '../../../routes/whl_app_pages.dart';
import '../../../utils/whl_current_limiting_util.dart';
import '../../../utils/whl_toast_utils.dart';
import '../../../utils/whl_user_utils.dart';
import '../../../widgets/refresh_widget.dart';
import '../../square/comment/comment_view.dart';
import '../../square/square_more_widget.dart';
import 'cave_mutual_logic.dart';

class CaveMutualPage extends StatefulWidget {
  UserModel account;
  CaveMutualPage(this.account, {Key? key}) : super(key: key);

  @override
  State<CaveMutualPage> createState() => _CaveMutualPageState();
}

class _CaveMutualPageState extends State<CaveMutualPage>
    with SingleTickerProviderStateMixin {
  bool get wantKeepAlive => true;
  late CaveMutualoLogic logic;
  late TabController tabController;
  int selectedIndex = 0;
  List listData = [];
  int size = 10;
  int current = 1;
  EasyRefreshController refreshController = EasyRefreshController(
      controlFinishLoad: true, controlFinishRefresh: true);
  ScrollController scrollController = ScrollController();
  bool isScrolling = false; //
  bool isShowBar = false; //

  @override
  void initState() {
    super.initState();
    print('@@@@22${widget.account.accountId.toString()}');
    getUserDynamicsList(isRefresh: true, accountId: widget.account.accountId);
    tabController =
        TabController(length: 3, vsync: this, initialIndex: selectedIndex);
    tabController.addListener(() {
      selectedIndex = tabController.index;
      debugPrint('tabController value is: ${widget.account.accountId}');
      getUserDynamicsList(isRefresh: true, accountId: widget.account.accountId);
    });
    scrollController.addListener(() {
      isScrolling = true;
      isShowBar = scrollController.offset > 200.h;
      WhlCurrentLimitingUtil.debounce(() {
        print('11111111111111111');
        setState(() {
          isScrolling = false;
        });
      });
    });
  }

  getUserDynamicsList({bool isRefresh = false, accountId}) async {
    if (isRefresh) {
      current = 1;
    } else {
      current++;
    }
    Map<String, dynamic> param = {
      "size": size,
      "current": current,
      "accountId": accountId
    };
    ResponseData responseData = await WhlApi.getUserDynamicsList.get(param);
    if (responseData.isSuccess()) {
      List records = responseData.data["records"];
      List<DynamicModel> modelList =
          records.map((e) => DynamicModel.fromJson(e)).toList();
      List newListData = listData;
      if (isRefresh) {
        newListData = modelList;
        refreshController.finishRefresh();
        refreshController.resetFooter();
      } else {
        newListData.addAll(modelList);
        refreshController.finishLoad();
      }
      if (records.length < size) {
        refreshController.finishLoad(IndicatorResult.noMore);
      }
      setState(() {
        listData = newListData;
      });
    } else {
      current--;
      refreshController.finishRefresh();
      refreshController.finishLoad();
    }
  }

  Future<void> toEditSquarePage(DynamicModel model) async {
    print(111);
    var a = await Get.toNamed(Routes.publish, arguments: {"model": model});
    if (a != null) {
      debugPrint(
          'getUserDynamicsList-toEditSquarePage value is: ${widget.account.accountId}');
      getUserDynamicsList(isRefresh: true);
    }
  }

  @override
  void dispose() {
    super.dispose();
    tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    logic = Get.put(CaveMutualoLogic(widget.account));
    final double statusBarHeight = MediaQuery.of(context).padding.top;
    final double pinnedHeaderHeight = statusBarHeight + kToolbarHeight;
    // WhlUserInfoModel myUserInfo = WhlUserUtils.getUserInfo();
    return MediaQuery.removePadding(
      removeTop: true,
      removeBottom: true,
      context: context,
      child: Scaffold(
        backgroundColor: kAppColor('#323232'),
        body: GetBuilder<CaveMutualoLogic>(builder: (logic) {
          String? backgroundImageUrl = widget.account.avatar?.toImageUrl();
          if ((widget.account.albumList ?? []).isNotEmpty) {
            ImageModel model = widget.account.albumList!.first;
            backgroundImageUrl = model.imageUrl?.toImageUrl();
          }
          print('@@@@11${logic.userModel?.accountId.toString()}');

          if (logic.userModel?.accountId.toString() !=
              widget.account.accountId.toString()) {
            // logic.initTabController();
            logic.setAccount(widget.account);
            //   logic.getUserDynamicsList(isRefresh: true, accountId: account.accountId);
          }
          return Stack(
            children: [
              UIImage(
                httpImage: backgroundImageUrl,
                width: ScreenUtil().screenWidth,
                height: ScreenUtil().screenHeight,
                fit: BoxFit.fitHeight,
              ),
              MyRefresh(
                  controller: refreshController,
                  onLoad: selectedIndex == 1
                      ? () {
                          getUserDynamicsList(
                              isRefresh: true,
                              accountId: widget.account.accountId);
                        }
                      : null,
                  childBuilder: (context, physics) {
                    return CustomScrollView(
                      key: Key("mutual${widget.account.accountId}"),
                      controller: scrollController,
                      physics: physics,
                      slivers: [
                        SliverPersistentHeader(
                            delegate: MySliverHeaderDelegate(
                          maxHeight: ScreenUtil().screenHeight,
                          minHeight: ScreenUtil().screenHeight,
                          child: Stack(
                            alignment: Alignment.bottomCenter,
                            children: [
                              Positioned(
                                bottom: 0,
                                left: 0,
                                width: ScreenUtil().screenWidth,
                                child: Column(
                                  children: [
                                    Container(height: 400.h),
                                    Stack(
                                      clipBehavior: Clip.none,
                                      children: [
                                        UIImage(
                                          assetImage: 'icon_card_bg.png',
                                          width: double.infinity,
                                          height: 428.h,
                                          fit: BoxFit.fitHeight,
                                        ),
                                        UIContainer(
                                          padding: EdgeInsets.only(top: 240.h),
                                          margin: EdgeInsets.symmetric(
                                              horizontal: 15.w),
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(20.w),
                                            child: UIContainer(
                                              color: const Color.fromRGBO(
                                                  161, 161, 161, 0.12),
                                              child: Stack(
                                                clipBehavior: Clip.none,
                                                children: [
                                                  UIColumn(
                                                    margin: EdgeInsets.only(
                                                        top: 14.w, left: 15.w),
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Row(
                                                        verticalDirection:
                                                            VerticalDirection
                                                                .down,
                                                        children: [
                                                          UIText(
                                                            text: widget.account
                                                                .nickName, //widget.account.nickName,
                                                            textColor:
                                                                Colors.white,
                                                            fontSize: 22.sp,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                          ),
                                                          if (widget.account
                                                                  .status !=
                                                              0)
                                                            UIImage(
                                                              assetImage: logic
                                                                  .getStatusIcon(
                                                                      widget
                                                                          .account
                                                                          .status),
                                                              width: 24.w,
                                                              height: 24.h,
                                                              padding: EdgeInsets
                                                                  .only(
                                                                      left: 7.w,
                                                                      top: 4.h),
                                                              alignment: Alignment
                                                                  .bottomLeft,
                                                            ),
                                                        ],
                                                      ),
                                                      UIRow(
                                                        padding: EdgeInsets
                                                            .symmetric(
                                                                horizontal: 0.w,
                                                                vertical: 11.w),
                                                        margin: EdgeInsets.only(
                                                            right: 16.w),
                                                        verticalDirection:
                                                            VerticalDirection
                                                                .down,
                                                        children: [
                                                          UIText(
                                                            text:
                                                                '${widget.account.distance}',
                                                            textColor:
                                                                kAppColor(
                                                                    '#FFE800'),
                                                            fontSize: 16.sp,
                                                            marginDrawable: 5.w,
                                                          ),
                                                          UIText(
                                                            text: 'km',
                                                            textColor:
                                                                Colors.white,
                                                            fontSize: 13.sp,
                                                            marginDrawable: 5.w,
                                                            padding:
                                                                EdgeInsets.only(
                                                                    left: 4.w),
                                                          ),
                                                          UIText(
                                                            text:
                                                                "${widget.account.province}${widget.account.city}",
                                                            textColor:
                                                                Colors.white,
                                                            fontSize: 13.sp,
                                                            marginDrawable: 5.w,
                                                            padding:
                                                                EdgeInsets.only(
                                                                    left: 8.w),
                                                          ),
                                                        ],
                                                      ),
                                                      Padding(
                                                        padding: EdgeInsets
                                                            .symmetric(
                                                                horizontal: 0.w,
                                                                vertical: 11.w),
                                                        child: Visibility(
                                                          visible: widget
                                                                  .account
                                                                  .soulTagList!
                                                                  .isNotEmpty
                                                              ? true
                                                              : false,
                                                          child: Wrap(
                                                            spacing: 8.0,
                                                            runSpacing: 8.0,
                                                            children: [
                                                              UIImage(
                                                                assetImage:
                                                                    'icon_special.png',
                                                                height: 10.w,
                                                              ),
                                                              Wrap(
                                                                spacing: 8.0,
                                                                runSpacing: 8.0,
                                                                children: [
                                                                  for (int i =
                                                                          0;
                                                                      i <
                                                                          widget
                                                                              .account
                                                                              .soulTagList!
                                                                              .length;
                                                                      i++)
                                                                    UIText(
                                                                      text: widget
                                                                          .account
                                                                          .soulTagList?[
                                                                              i]
                                                                          .label,
                                                                      textColor:
                                                                          Colors
                                                                              .white,
                                                                      fontSize:
                                                                          14.sp,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    ),
                                                                ],
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Positioned(
                                  right: 0,
                                  bottom: 136.h,
                                  child: UIColumn(
                                    margin: EdgeInsets.only(
                                        top: 14.w, left: 15.w, right: 15.w),
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      GestureDetector(
                                        child: UIImage(
                                          assetImage: "icon_love.png",
                                          padding: EdgeInsets.all(5.w),
                                          width: 55.w,
                                          height: 55.w,
                                        ),
                                        onTap: () {
                                          // MyToast.show("添加好友");
                                          logic.setFriendStart();
                                        },
                                      ),
                                      UIImage(
                                        assetImage: 'icon_flower.png',
                                        padding: EdgeInsets.all(5.w),
                                        width: 55.w,
                                        height: 55.w,
                                        onTap: () {
                                          // MyToast.show("赠送礼物");
                                          sendGiftDialog(
                                              widget.account.accountId!);
                                        },
                                      ),
                                    ],
                                  )),
                            ],
                          ),
                        )),
                        SliverPersistentHeader(
                            pinned: true,
                            delegate: MySliverHeaderDelegate(
                              maxHeight: 50.w,
                              minHeight: 50.w,
                              child: Container(
                                color: kAppColor('#252525'),
                                padding: EdgeInsets.symmetric(vertical: 10.w),
                                child: TabBar(
                                  controller: tabController,
                                  labelColor: Colors.white,
                                  labelStyle: TextStyle(
                                      fontSize: 16.sp,
                                      color: kAppColor('#151718'),
                                      fontWeight: FontWeight.bold),
                                  unselectedLabelStyle: TextStyle(
                                      fontSize: 14.sp,
                                      color: Colors.white.withOpacity(0.7)),
                                  indicatorColor: Colors.white,
                                  indicatorSize: TabBarIndicatorSize.label,
                                  indicatorWeight: 1.0,
                                  isScrollable: false,
                                  unselectedLabelColor:
                                      Colors.white.withOpacity(0.7),
                                  tabs: const <Tab>[
                                    Tab(text: '资料'),
                                    Tab(text: '动态'),
                                    Tab(text: '相册')
                                  ],
                                ),
                              ),
                            )),
                        buildContextWidget(),
                        SliverToBoxAdapter(
                          child: Container(
                              height: 110.w, color: kAppColor('#323232')),
                        )
                      ],
                    );
                  }),
            ],
          );
        }),
      ),
    );
  }

  Widget buildContextWidget() {
    return GetBuilder<CaveMutualoLogic>(builder: (logic) {
      if (selectedIndex == 1) {
        return GetBuilder<CaveMutualoLogic>(builder: (logic) {
          return SliverList(
              key: Key("mutual${widget.account.accountId}"),
              delegate: SliverChildBuilderDelegate((context, index) {
                DynamicModel model = listData[index];
                return buildDynamicItem(model);
              }, childCount: listData.length));
        });
      } else if (selectedIndex == 0) {
        return SliverList(
            delegate: SliverChildListDelegate([
          SingleChildScrollView(
            physics: const NeverScrollableScrollPhysics(),
            key: Key("mutual${widget.account.accountId}"),
            child: UIColumn(
              color: kAppColor('#323232'),
              children: [
                Container(
                  margin:
                      EdgeInsets.symmetric(horizontal: 15.w, vertical: 23.w),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20.w),
                    child: UIContainer(
                      color: const Color.fromRGBO(161, 161, 161, 0.12),
                      child: UIColumn(
                        radius: 20.w,
                        color: kAppColor('#E9E9E9').withOpacity(0.12),
                        // margin: EdgeInsets.symmetric(horizontal: 15.w),
                        width: double.infinity,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          UIText(
                            margin: EdgeInsets.only(left: 14.w, top: 20.w),
                            text: '基础资料',
                            textColor: kAppColor('#E9E9E9'),
                            fontSize: 17.sp,
                            fontWeight: FontWeight.bold,
                          ),
                          UIWrap(
                            runAlignment: WrapAlignment.start,
                            wrapAlignment: WrapAlignment.start,
                            crossAxisAlignment: WrapCrossAlignment.start,
                            margin: EdgeInsets.symmetric(
                                horizontal: 14.w, vertical: 18.w),
                            runSpacing: 7.w,
                            spacing: 8.w,
                            children: (widget.account.baseInfo ?? []).map((e) {
                              return e != null
                                  ? UIText(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 15.w, vertical: 7.w),
                                      text: e ?? '',
                                      textColor: Colors.white,
                                      fontSize: 12.sp,
                                      strokeColor: Colors.white,
                                      strokeWidth: 1.w,
                                      radius: 30.w,
                                    )
                                  : Container();
                            }).toList(),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  margin:
                      EdgeInsets.symmetric(horizontal: 15.w, vertical: 20.w),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20.w),
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaY: 44, sigmaX: 44),
                      child: UIColumn(
                        radius: 20.w,
                        color: kAppColor('#E9E9E9').withOpacity(0.12),
                        width: double.infinity,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          UIText(
                            margin: EdgeInsets.only(left: 14.w, top: 20.w),
                            text: '关于我',
                            textColor: kAppColor('#E9E9E9'),
                            fontSize: 17.sp,
                            fontWeight: FontWeight.bold,
                          ),
                          UIText(
                            margin: EdgeInsets.only(top: 18.w, bottom: 40.w),
                            padding: EdgeInsets.symmetric(horizontal: 15.w),
                            text: widget.account.aboutMe ?? "",
                            textColor: kAppColor('#D5D5D5'),
                            shrinkWrap: false,
                            fontSize: 13.sp,
                            fontWeight: FontWeight.bold,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 15.w),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20.w),
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaY: 44, sigmaX: 44),
                      child: UIColumn(
                        radius: 20.w,
                        color: kAppColor('#E9E9E9').withOpacity(0.12),
                        width: double.infinity,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          UIText(
                            margin: EdgeInsets.only(left: 14.w, top: 20.w),
                            text: '礼物展馆',
                            textColor: kAppColor('#E9E9E9'),
                            fontSize: 17.sp,
                            fontWeight: FontWeight.bold,
                            marginDrawable: 5.w,
                            endDrawable: UIText(
                              text: '已点亮12/36',
                              textColor: kAppColor('#A7A7A7'),
                              fontSize: 10.sp,
                            ),
                          ),
                          UIRow(
                            margin: EdgeInsets.symmetric(vertical: 15.w),
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              for (int i = 0; i < 5; i++)
                                UIImage(
                                  assetImage: 'icon_gift_0.png',
                                  width: 54.w,
                                )
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  margin:
                      EdgeInsets.symmetric(horizontal: 15.w, vertical: 24.w),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20.w),
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaY: 44, sigmaX: 44),
                      child: UIColumn(
                        radius: 20.w,
                        color: kAppColor('#E9E9E9').withOpacity(0.12),
                        width: double.infinity,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          UIText(
                            margin: EdgeInsets.only(left: 14.w, top: 20.w),
                            text: '灵魂标签',
                            textColor: kAppColor('#E9E9E9'),
                            fontSize: 17.sp,
                            fontWeight: FontWeight.bold,
                          ),
                          UIWrap(
                            runAlignment: WrapAlignment.start,
                            wrapAlignment: WrapAlignment.start,
                            crossAxisAlignment: WrapCrossAlignment.start,
                            margin: EdgeInsets.symmetric(
                                horizontal: 14.w, vertical: 18.w),
                            runSpacing: 7.w,
                            spacing: 8.w,
                            children:
                                (widget.account.soulTagList ?? []).map((e) {
                              return UIText(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 15.w, vertical: 7.w),
                                text: e.label,
                                textColor: Colors.white,
                                fontSize: 12.sp,
                                strokeColor: Colors.white,
                                strokeWidth: 1.w,
                                radius: 30.w,
                              );
                            }).toList(),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ]));
      } else {
        return SliverList(
            key: Key("mutual${widget.account.accountId}"),
            delegate: SliverChildListDelegate([
              Container(
                color: kAppColor('#323232'),
                child: SingleChildScrollView(
                  key: Key("mutual${widget.account.accountId}"),
                  physics: ClampingScrollPhysics(),
                  child: Container(
                    padding: EdgeInsets.all(10),
                    child: MasonryGridView.count(
                      // 展示几列
                      crossAxisCount: 2,
                      // 元素总个数
                      itemCount: widget.account.album?.length,
                      // 单个子元素
                      itemBuilder: (BuildContext context, int index) {
                        return ClipRRect(
                          borderRadius: BorderRadius.circular(22.w),
                          child: CachedNetworkImage(
                            imageUrl:
                                (widget.account.album![index]!.toString() ?? "")
                                    .toImageUrl(),
                            fit: BoxFit.fitWidth,
                          ),
                        );
                      },
                      // 纵向元素间距
                      mainAxisSpacing: 10,
                      // 横向元素间距
                      crossAxisSpacing: 10,
                      //本身不滚动，让外面的singlescrollview来滚动
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true, //收缩，让元素宽度自适应
                    ),
                  ),
                ),
              ),
            ]));
      }
    });
  }

  Widget buildDynamicItem(DynamicModel model) {
    return GetBuilder<CaveMutualoLogic>(builder: (logic) {
      return Container(
        key: Key("mutual${widget.account.accountId}"),
        color: kAppColor('#323232'),
        child: UIRow(
          key: Key("mutual${widget.account.accountId}"),
          margin: EdgeInsets.only(bottom: 24.w, left: 15.w, right: 15.w),
          padding:
              EdgeInsets.only(left: 16.w, right: 14.w, top: 20.w, bottom: 25.w),
          gradientStartColor: kAppColor('#473D3E'),
          gradientEndColor: kAppColor('#584848'),
          radius: 20.w,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UIImage(
              margin: EdgeInsets.only(right: 9.w),
              httpImage: model.avatar?.toImageUrl(),
              assetPlaceHolder: 'icon_app_logo.png',
              width: 45.w,
              height: 45.w,
              radius: 22.5.w,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  UIRow(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              UIText(
                                text: model.user?.nickName ?? "",
                                textColor: Colors.white,
                                fontSize: 16.sp,
                                fontWeight: FontWeight.bold,
                              ),
                              UIText(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 4.w, vertical: 3.w),
                                margin: EdgeInsets.only(left: 4.w),
                                text: 'Lv${model.user?.level ?? "0"}',
                                alignment: Alignment.center,
                                textColor: kAppTextColor,
                                fontSize: 8.sp,
                                radius: 7.w,
                                color: kAppColor("#F4EEFF"),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              UIText(
                                padding: EdgeInsets.only(left: 8.w, right: 7.w),
                                height: 18.w,
                                alignment: Alignment.center,
                                text: '${model.user?.age ?? 0}岁',
                                textColor: logic.getGenderColor(),
                                fontSize: 11.sp,
                                color: kAppColor('#F6F6F6').withOpacity(0.11),
                                radius: 6.w,
                                marginDrawable: 3.w,
                                startDrawable: UIImage(
                                  assetImage: logic
                                      .getGenderIcon(widget.account.gender),
                                  height: 10.w,
                                ),
                              ),
                              UIText(
                                margin: EdgeInsets.only(left: 4.w),
                                padding: EdgeInsets.only(left: 8.w, right: 7.w),
                                height: 18.w,
                                alignment: Alignment.center,
                                text: model.user?.sexualOrientation ?? "未知",
                                textColor: Colors.white,
                                fontSize: 11.sp,
                                color: kAppColor('#F6F6F6').withOpacity(0.11),
                                radius: 6.w,
                              ),
                              UIText(
                                margin: EdgeInsets.only(left: 4.w),
                                padding: EdgeInsets.only(left: 8.w, right: 7.w),
                                height: 18.w,
                                alignment: Alignment.center,
                                text: model.user?.funRole ?? "未知",
                                textColor: Colors.white,
                                fontSize: 11.sp,
                                color: kAppColor('#F6F6F6').withOpacity(0.11),
                                radius: 6.w,
                              ),
                            ],
                          ),
                        ],
                      ),
                      Expanded(child: Container()),
                      UIImage(
                        assetImage: 'icon_square_top.gif',
                        width: 20.w,
                        height: 20.w,
                      ),
                      UIImage(
                        margin: EdgeInsets.only(left: 9.w, right: 4.w),
                        assetImage: 'icon_square_hot.png',
                        width: 20.w,
                        height: 20.w,
                      ),
                      UIContainer(
                        child:
                            Icon(Icons.more_vert, color: kAppColor('#9E9E9E')),
                        onTap: () {
                          Get.bottomSheet(square_more_widget(
                            dynamicId: model.id ?? "",
                            isMe: model.accountId ==
                                WhlUserUtils.getUserInfo().accountId,
                            callBack: (type) {
                              if (type == 2) {
                                debugPrint(
                                    'getUserDynamicsList-square_more_widget value is: ${widget.account.accountId}');
                                getUserDynamicsList(
                                    isRefresh: true,
                                    accountId: widget.account.accountId);
                              } else if (type == 3) {
                                toEditSquarePage(model);
                              } else if (type == 4) {
                                listData.remove(model);
                                setState(() {
                                  listData;
                                });
                              }
                            },
                          ));
                        },
                      )
                    ],
                  ),
                  UIText(
                    margin: EdgeInsets.only(top: 10.w),
                    text: model.content ?? "",
                    textColor: Colors.white,
                    fontSize: 15.sp,
                    shrinkWrap: false,
                  ),
                  if (model?.urls?.first.imageUrl?.isNotEmpty ?? false)
                    UIImage(
                      margin: EdgeInsets.only(top: 13.w),
                      httpImage: model?.urls?.first.imageUrl?.toImageUrl(),
                      assetPlaceHolder: 'icon_app_logo.png',
                      width: 166.w,
                      alignment: Alignment.centerLeft,
                      height: 166.w,
                      fit: BoxFit.cover,
                      radius: 6.w,
                    ),
                  Container(
                    margin: EdgeInsets.only(top: 11.w),
                    child: RichText(
                      text: TextSpan(
                          text: '浙江杭州 ·15分钟前发布 · 来自 ',
                          style: TextStyle(
                              fontSize: 10.sp, color: kAppColor('#C1C1C1')),
                          children: [
                            TextSpan(
                                text: 'LES',
                                style: TextStyle(
                                    fontSize: 10.sp, color: Colors.white))
                          ]),
                    ),
                  ),
                  UIRow(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    margin: EdgeInsets.only(top: 21.w),
                    children: [
                      UIImage(
                        assetImage: 'icon_square_share.png',
                        width: 20.w,
                        imageColor: model.setting?.notForward == 1
                            ? kAppSub3TextColor.withOpacity(0.5)
                            : kAppColor("#ADADAD"),
                        onTap: () {
                          if (model.setting?.notComment == 1) {
                            MyToast.show("该动态禁止转发");
                            return;
                          }
                          Get.toNamed(Routes.publish,
                              arguments: {"model": model, "isForward": true});
                        },
                      ),
                      UIImage(
                        assetImage: 'icon_square_fly_grey.png',
                        width: 20.w,
                      ),
                      UIImage(
                        assetImage: 'icon_square_gift.png',
                        width: 20.w,
                      ),
                      UIText(
                        text: '${model.commentNum ?? 0}',
                        textColor: model.setting?.notComment == 1
                            ? kAppSub3TextColor.withOpacity(0.5)
                            : kAppColor("#ADADAD"),
                        fontSize: 14.sp,
                        marginDrawable: 3.w,
                        startDrawable: UIImage(
                          assetImage: 'icon_square_comment.png',
                          width: 20.w,
                          imageColor: model.setting?.notComment == 1
                              ? kAppSub3TextColor.withOpacity(0.5)
                              : kAppColor("#ADADAD"),
                        ),
                        onTap: () {
                          if (model.setting?.notComment == 1) {
                            MyToast.show("该动态禁止评论");
                            return;
                          }
                          Get.bottomSheet(
                              CommentWidget(
                                dynamicId: model.id ?? "",
                              ),
                              isScrollControlled: true);
                        },
                      ),
                      UIText(
                        text: '${model.goodNum ?? 0}',
                        textColor: kAppColor('#ADADAD'),
                        fontSize: 14.sp,
                        marginDrawable: 3.w,
                        startDrawable: UIImage(
                          assetImage: model.currentZan == true
                              ? 'icon_square_isLike.png'
                              : 'icon_square_like.png',
                          width: 20.w,
                        ),
                        onTap: () {
                          dianZanDynamicRequest(model!,
                              dataSource: DynamicDataSource.Cave,
                              callback: (isSuccess) {
                            if (isSuccess && logic != null) {
                              logic.update();
                            }
                          });
                        },
                      ),
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      );
    });
  }
}
