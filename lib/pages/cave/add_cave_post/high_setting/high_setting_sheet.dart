import 'package:cave_flutter/common/common_action.dart';
import 'package:cave_flutter/pages/whl_home/whl_publish/high_setting/high_setting_logic.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_switch_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'package:cave_flutter/pages/cave/add_cave_post/add_cave_post_logic.dart';

class HighSettingSheet extends StatelessWidget {
  HighSettingSheet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return UIContainer(
      padding:
          EdgeInsets.only(left: 24.w, right: 13.w, top: 31.w, bottom: 30.w),
      color: Colors.white,
      topRightRadius: 30.w,
      topLeftRadius: 30.w,
      child: ListView(
        children: getAllRowView(),
        shrinkWrap: true,
      ),
    );
  }

  List<Widget> getAllRowView() {
    List<Widget> items = [];
    final pushLogic = Get.find<AddCavePostLogic>();
    List<Map<String, dynamic>> list = List<Map<String, dynamic>>.from([
      {
        "id": "1234a127b93611ee83cf000c292789e3",
        "name": "同步到广场",
        "vip": 0,
        "key": "syncSquare",
      },
    ]);
    List<Map<String, dynamic>> newList = list + highSettingData;
    for (Map<String, dynamic> element in newList) {
      int type = element["type"] ?? 0;
      String title = element["name"] ?? "";
      int vip = element["vip"] ?? 0;
      String key = element["key"] ?? "";
      if (type == 1) {
        items.add(UIRow(
          margin: EdgeInsets.only(bottom: 20.w),
          children: [
            UIText(
              text: title,
              textColor: kAppColor('#2D2D2D'),
              fontSize: 14.sp,
              fontWeight: FontWeight.bold,
            ),
            Expanded(child: Container()),
            Icon(Icons.keyboard_arrow_right, color: kAppSub2TextColor),
          ],
        ));
      } else {
        String? vipPath = getVipIcon(vip, 0);
        items.add(
          UIRow(
            margin: EdgeInsets.only(bottom: 20.w),
            children: [
              UIText(
                text: title,
                textColor: kAppColor('#2D2D2D'),
                fontSize: 14.sp,
                fontWeight: FontWeight.bold,
                marginDrawable: 7.w,
                endDrawable: vipPath != null
                    ? UIImage(
                        assetImage: vipPath,
                        height: 15.w,
                      )
                    : null,
              ),
              Expanded(child: Container()),
              WhlSwitch(
                openColor: Colors.white,
                bgColor: kAppColor('#EDEDED'),
                color: Colors.white,
                openBgColor: kAppThemeColor,
                // inactiveTrackColor: Color.fromRGBO(237, 237, 237, 1),
                value: pushLogic.highSetterData[key] == 1 ? true : false,
                width: 50.w,
                height: 26.w,
                onChanged: (val) {
                  if (key.isNotEmpty) {
                    pushLogic.highSetterData[key] = val ? 1 : 0;
                  }
                },
              )
            ],
          ),
        );
      }
    }
    return items;
  }
}
