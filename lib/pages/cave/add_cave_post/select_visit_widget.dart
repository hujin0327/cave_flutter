import 'package:cave_flutter/common/common_widget.dart';
import 'package:cave_flutter/model/base_source_model.dart';
import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class SelectVisitWidget extends StatefulWidget {
  BaseItemModel? oldSelectModel;

  SelectVisitWidget({super.key, this.oldSelectModel});

  @override
  State<SelectVisitWidget> createState() => _SelectVisitWidgetState();
}

class _SelectVisitWidgetState extends State<SelectVisitWidget> {
  List dataList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadVisitData();
  }

  loadVisitData() {
    WhlApi.getBrowseList.get({}, withLoading: true).then((value) {
      if (value.isSuccess()) {
        List data = value.data;
        List modelList = [];
        for (Map element in data) {
          String name = element["name"];
          BaseItemModel model =
              BaseItemModel(title: name, value: element["id"]);
          if (name.contains("指定")) {
            model.imageName = "home_right";
            model.otherBool = true;
          }
          if (widget.oldSelectModel != null &&
              widget.oldSelectModel!.value == model.value) {
            model.isSelect = true;
          }
          modelList.add(model);
        }
        setState(() {
          dataList = modelList;
        });
      }
    });
  }

  tapItem(BaseItemModel model) {
    setState(() {
      for (var element in dataList) {
        element.isSelect = false;
      }
      model.isSelect = true;
      if (model.otherBool == true) {
      } else {
        Get.back(result: model);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return UIColumn(
      padding:
          EdgeInsets.only(left: 24.w, top: 26.h, right: 12.w, bottom: 26.h),
      radius: 30.w,
      color: kAppWhiteColor,
      children: [
        UIText(
          text: "浏览权限",
          textColor: kAppSubTextColor,
          fontSize: 14.sp,
        ),
        Expanded(
          child: ListView.builder(
            itemBuilder: (context, index) {
              BaseItemModel model = dataList[index];
              return leftRightDetailVisitView(model.title ?? "",
                  margin: EdgeInsets.only(top: 20.h),
                  rightImageName: (model.imageName ?? "").isNotEmpty
                      ? model.imageName
                      : model.isSelect == true
                          ? "icon_select.png"
                          : null,
                  titleColor: model.isSelect == true?kAppSubTextColor:kAppSub3TextColor,
                  titleFont: 14.sp, onTap: () {
                tapItem(model);
              });
            },
            itemCount: dataList.length,
            shrinkWrap: true,
          ),
        )
      ],
    );
  }
}
