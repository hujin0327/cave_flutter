import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../style/whl_style.dart';
import 'sure_exit_publish_logic.dart';

class SureExitPublishSheet extends StatelessWidget {
  SureExitPublishSheet({Key? key}) : super(key: key);

  final logic = Get.put(SureExitPublishLogic());

  @override
  Widget build(BuildContext context) {
    return UIWrap(
      color: Colors.white,
      topLeftRadius: 30.w,
      topRightRadius: 30.w,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            GestureDetector(
                child: Container(
                    padding: EdgeInsets.only(top: 20.w, right: 20.w),
                    child: Icon(Icons.close)),
              onTap: (){
                  Get.back();
              },
            ),
          ],
        ),
        UIText(
          margin: EdgeInsets.only(top: 14.w, bottom: 38.w),
          alignment: Alignment.center,
          text: '没发完就走了，要不要存个草稿？',
          textColor: kAppTextColor,
          fontSize: 16.sp,
          fontWeight: FontWeight.bold,
        ),
        UIRow(
          margin: EdgeInsets.symmetric(horizontal: 42.w),
          children: [
            Expanded(
                child: UISolidButton(
              text: '不保存',
              fontSize: 16.sp,
              onTap: () {
                Get.back(result: false);
              },
            )),
            SizedBox(
              width: 23.w,
            ),
            Expanded(
                child: UISolidButton(
              text: '保存草稿',
              fontSize: 16.sp,
              color: kAppColor('#FFE800'),
              onTap: () {
                Get.back(result: true);
              },
            )),
          ],
        ),
        Container(
          height: 20.w + ScreenUtil().bottomBarHeight,
        ),
      ],
    );
  }
}
