import 'dart:io';

import 'package:cave_flutter/model/topic_model.dart';
import 'package:cave_flutter/pages/cave/add_cave_Post/high_setting/high_setting_sheet.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import './add_cave_post_logic.dart';

class AddCavePostPage extends StatelessWidget {
  AddCavePostPage({Key? key}) : super(key: key);

  final logic = Get.put(AddCavePostLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: GetBuilder<AddCavePostLogic>(builder: (logic) {
        return PopScope(
          canPop: false,
          onPopInvoked: (canPop) {
            print(canPop);
            if (canPop) {
              return;
            }
            logic.backAction();
          },
          child: UIColumn(
            margin: EdgeInsets.only(top: ScreenUtil().statusBarHeight + 10.w),
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  UIContainer(
                    margin: EdgeInsets.only(left: 18.w),
                    alignment: Alignment.centerLeft,
                    width: 70.w,
                    child: const Icon(
                      Icons.close,
                      color: Colors.black,
                    ),
                    onTap: () {
                      // Get.back();
                      logic.backAction();
                    },
                  ),
                  UIText(
                    text: logic.dynamicModel != null
                        ? logic.isForward
                            ? '分享与转发'
                            : "编辑动态"
                        : '发布新动态',
                    textColor: kAppTextColor,
                    fontSize: 16.sp,
                    fontWeight: FontWeight.bold,
                  ),
                  UIRow(
                    margin: EdgeInsets.only(right: 18.w),
                    color: logic.isBtnEnable
                        ? kAppBlackColor
                        : kAppColor('#F1F1F1'),
                    alignment: Alignment.center,
                    width: 70.w,
                    height: 37.w,
                    radius: 26.w,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      UIImage(
                        assetImage: 'icon_publish.png',
                        width: 13.w,
                        imageColor: logic.isBtnEnable
                            ? kAppWhiteColor
                            : kAppColor('#888888'),
                      ),
                      UIText(
                        text: '发布',
                        textColor: logic.isBtnEnable
                            ? kAppWhiteColor
                            : kAppColor('#888888'),
                        fontSize: 13.sp,
                      )
                    ],
                    onTap: () {
                      logic.sendDynamicRequest(context);
                    },
                  ),
                ],
              ),
              Visibility(
                visible: (logic.recordVoiceModel != null),
                child: UIRow(
                  margin: EdgeInsets.only(left: 18.w, top: 10.w),
                  radius: 40.w,
                  mainAxisSize: MainAxisSize.min,
                  height: 33.w,
                  color: Colors.black,
                  children: [
                    UIImage(
                      margin: EdgeInsets.only(left: 15.w, right: 10.w),
                      assetImage: 'icon_voice_play.png',
                      width: 12.w,
                    ),
                    UIImage(
                      assetImage: 'icon_voice_voice.png',
                      width: 22.w,
                    ),
                    UIText(
                      margin: EdgeInsets.only(left: 15.w),
                      text: '${logic.recordVoiceModel?.time.toString()}″',
                      textColor: Colors.white,
                      fontSize: 12.sp,
                      fontWeight: FontWeight.bold,
                    ),
                    UIImage(
                      margin: EdgeInsets.symmetric(horizontal: 10.w),
                      assetImage: 'icon_voice_close.png',
                      width: 20.w,
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 20.w),
                child: TextField(
                  controller: logic.titleController,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(left: 18.w),
                    hintText: '分享我的Cave生活...',
                    hintStyle:
                        TextStyle(color: kAppColor('#CACACA'), fontSize: 16.sp),
                    border:
                        const OutlineInputBorder(borderSide: BorderSide.none),
                  ),
                  onChanged: (value) {
                    logic.onCheckPostBtnEnable();
                  },
                  style: TextStyle(color: kAppTextColor, fontSize: 16.sp),
                  maxLines: 8,
                ),
              ),
              if (logic.isForward)
                IntrinsicHeight(
                  child: UIRow(
                    color: kAppColor('#F8F8F8'),
                    margin: EdgeInsets.symmetric(
                      horizontal: 18.w,
                    ),
                    children: [
                      UIImage(
                        assetImage: 'icon_app_logo.png',
                        width: 85.w,
                      ),
                      Expanded(
                        child: UIColumn(
                          margin: EdgeInsets.only(
                              left: 10.w, top: 10.w, bottom: 10.w),
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            UIText(
                              text: logic.dynamicModel?.content ?? '',
                              textColor: kAppTextColor,
                              fontSize: 16.sp,
                              fontWeight: FontWeight.bold,
                            ),
                            UIText(
                              text: '这是转发的内容详情文字，只是做展示不 可点击，只能看只能看只能看只能看只...',
                              textColor: kAppColor('#979797'),
                              fontSize: 13.sp,
                              fontWeight: FontWeight.bold,
                              shrinkWrap: false,
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              GetBuilder<AddCavePostLogic>(builder: (logic) {
                if (logic.imagePath != null || logic.imageModel != null) {
                  return Stack(
                    alignment: Alignment.topRight,
                    children: [
                      UIContainer(
                        margin: EdgeInsets.only(left: 16.w, top: 13.h),
                        width: 112.w,
                        height: 112.w,
                        strokeWidth: 1.w,
                        strokeColor: kAppColor('#D9D9D9'),
                        radius: 16.w,
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(16.w),
                            child: logic.imageModel != null
                                ? UIImage(
                                    httpImage: logic.imageModel!.imageUrl
                                        ?.toImageUrl(),
                                    fit: BoxFit.cover)
                                : Image.file(File(logic.imagePath!),
                                    fit: BoxFit.cover)),
                        onTap: () {
                          logic.doClickImageAsset();
                        },
                      ),
                      UIImage(
                        padding: EdgeInsets.only(right: 4.w, top: 18.w),
                        assetImage: 'icon_close_circle.png',
                        width: 28.w,
                        onTap: () {
                          logic.deleteImage();
                        },
                      )
                    ],
                  );
                }
                return UIContainer(
                  margin: EdgeInsets.only(left: 16.w, top: 13.h),
                  color: kAppColor('#F3F3F3'),
                  width: 112.w,
                  height: 112.w,
                  alignment: Alignment.center,
                  radius: 16.w,
                  child: UIText(
                    text: '+',
                    textColor: Colors.white,
                    fontSize: 30.sp,
                    fontWeight: FontWeight.bold,
                  ),
                  onTap: () {
                    logic.doClickImageAsset();
                  },
                );
              }),
              UIText(
                margin: EdgeInsets.only(left: 25.w, top: 15.w),
                text: logic.address,
                textColor: kAppColor('#C1C1C1'),
                fontSize: 11.sp,
                fontWeight: FontWeight.bold,
                startDrawable: UIImage(
                  assetImage: 'icon_publish_location.png',
                  width: 23.w,
                ),
              ),
              Expanded(child: Container()),
              UIRow(
                margin: EdgeInsets.only(left: 20.w),
                height: 30.w,
                children: [
                  UIText(
                    padding: EdgeInsets.symmetric(horizontal: 10.w),
                    height: 30.w,
                    text: '话题 >',
                    alignment: Alignment.center,
                    textColor: kAppColor('#444444'),
                    fontSize: 12.sp,
                    fontWeight: FontWeight.bold,
                    strokeColor: Colors.black,
                    strokeWidth: 1.w,
                    radius: 20.w,
                    onTap: () {
                      logic.toTopicPage();
                    },
                  ),
                  Expanded(
                    child: GetBuilder<AddCavePostLogic>(builder: (logic) {
                      return ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (BuildContext context, int index) {
                          TopicModel model = logic.selectTopicList[index];
                          return UIText(
                            margin: EdgeInsets.only(left: 8.w),
                            padding: EdgeInsets.symmetric(horizontal: 10.w),
                            text: '#${model.talkName}',
                            textColor: kAppSub3TextColor,
                            color: kAppColor('#F8F8F8'),
                            fontSize: 12.sp,
                            alignment: Alignment.center,
                            fontWeight: FontWeight.bold,
                            radius: 20.w,
                          );
                        },
                        itemCount: logic.selectTopicList.length,
                      );
                    }),
                  ),
                ],
              ),
              UIRow(
                margin: EdgeInsets.only(left: 16.w, top: 21.w, right: 13.w),
                children: [
                  UIImage(
                    margin: EdgeInsets.only(right: 20.w),
                    assetImage: 'icon_publish_image_1.png',
                    width: 30.w,
                    onTap: () {
                      logic.selectPhoto();
                    },
                  ),
                  UIImage(
                    margin: EdgeInsets.only(right: 20.w),
                    assetImage: 'icon_publish_camera_1.png',
                    width: 30.w,
                    onTap: () {
                      logic.takePhoto();
                    },
                  ),
                  UIImage(
                    margin: EdgeInsets.only(right: 20.w),
                    assetImage: 'icon_publish_record_1.png',
                    width: 30.w,
                    onTap: () {
                      logic.doClickVoiceRecord();
                    },
                  ),
                  // UIImage(
                  //   margin: EdgeInsets.only(right: 20.w),
                  //   assetImage: 'icon_publish_location_0.png',
                  //   width: 30.w,
                  // ),
                  // UIImage(
                  //   assetImage: 'icon_publish_at_1.png',
                  //   width: 30.w,
                  // ),
                  Expanded(child: Container()),
                  GetBuilder<AddCavePostLogic>(builder: (logic) {
                    return UIText(
                      padding: EdgeInsets.only(left: 11.w),
                      height: 32.w,
                      alignment: Alignment.center,
                      text: logic.selectVisitModel?.title ?? "请选择可见范围",
                      textColor: kAppTextColor,
                      strokeColor: kAppColor('#D9D9D9'),
                      strokeWidth: 1.w,
                      fontSize: 13.sp,
                      fontWeight: FontWeight.bold,
                      radius: 20.w,
                      endDrawable: Icon(Icons.arrow_drop_down_sharp),
                      onTap: () {
                        logic.showSelectVisitView();
                      },
                    );
                  })
                ],
              ),
              UIRow(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                margin: EdgeInsets.only(bottom: ScreenUtil().bottomBarHeight),
                padding: EdgeInsets.symmetric(horizontal: 18.w, vertical: 20.w),
                children: [
                  UIText(
                    text: '高级设置',
                    textColor: kAppColor('#444444'),
                    fontSize: 13.sp,
                  ),
                  Icon(
                    Icons.arrow_forward_ios,
                    size: 15,
                  )
                ],
                onTap: () {
                  Get.bottomSheet(HighSettingSheet(), isScrollControlled: true);
                },
              )
            ],
          ),
        );
      }),
    );

    // return Scaffold(
    //   backgroundColor: Colors.white,
    //   appBar: WhlAppBar(
    //     centerTitle: '',
    //     backgroundColor: Colors.white,
    //     isShowBottomLine: false,
    //   ),
    //   body: SingleChildScrollView(
    //     child: UIColumn(
    //       onTap: () {
    //         hideKeyboard(context);
    //       },
    //       crossAxisAlignment: CrossAxisAlignment.start,
    //       children: [
    //         GetBuilder<WhlPublishLogic>(builder: (logic) {
    //           if (logic.imagePath != null) {
    //             return UIContainer(
    //               margin: EdgeInsets.only(left: 16.w, top: 8.h),
    //               width: 140.w,
    //               height: 158.w,
    //               child: ClipRRect(borderRadius: BorderRadius.circular(4.w), child: Image.file(File(logic.imagePath!), fit: BoxFit.cover)),
    //               onTap: () {
    //                 logic.doClickImageAsset();
    //               },
    //             );
    //           }
    //           return UIContainer(
    //             margin: EdgeInsets.only(left: 16.w, top: 8.h),
    //             color: kAppColor('#F0F0F0'),
    //             width: 140.w,
    //             height: 158.w,
    //             radius: 4.w,
    //             child: Icon(
    //               Icons.add_circle_outlined,
    //               color: kAppColor('#CCCCCC'),
    //             ),
    //             onTap: () {
    //               logic.doClickImageAsset();
    //             },
    //           );
    //         }),
    //         UIRow(
    //           margin: EdgeInsets.only(right: 16.w, left: 16.w, top: 24.h, bottom: 40.h),
    //           children: logic.typeList.map((e) {
    //             int index = logic.typeList.indexOf(e);
    //             return GetBuilder<WhlPublishLogic>(builder: (logic) {
    //               return UIContainer(
    //                 margin: EdgeInsets.only(right: 20.w),
    //                 decoration: BoxDecoration(
    //                     image: DecorationImage(
    //                   image: AssetImage(
    //                       logic.selectedIndex == index ? 'assets/images/img_bg_publish_select.png' : 'assets/images/img_bg_publish_default.png'),
    //                   fit: BoxFit.fill,
    //                 )),
    //                 padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 1.h),
    //                 child: UIText(
    //                   text: e,
    //                   textColor: logic.selectedIndex == index ? Colors.white : kAppSubTextColor,
    //                   fontSize: 12.sp,
    //                 ),
    //                 onTap: () {
    //                   logic.selectedIndex = index;
    //                   logic.update();
    //                 },
    //               );
    //             });
    //           }).toList(),
    //         ),
    //         UIContainer(
    //           margin: EdgeInsets.only(left: 16.w, right: 16.w),
    //           color: kAppColor('#F8F8F8'),
    //           height: 32.h,
    //           radius: 4.w,
    //           child: TextField(
    //             controller: logic.titleController,
    //             decoration: InputDecoration(
    //               contentPadding: EdgeInsets.only(left: 10.w),
    //               hintText: 'Type your title.',
    //               hintStyle: TextStyle(color: kAppSub2TextColor, fontSize: 12.sp),
    //               border: const OutlineInputBorder(borderSide: BorderSide.none),
    //             ),
    //             style: TextStyle(color: kAppTextColor, fontSize: 12.sp),
    //             onChanged: (str) {
    //               logic.onCheckPostBtnEnable();
    //             },
    //           ),
    //         ),
    //         UIContainer(
    //           margin: EdgeInsets.only(left: 16.w, right: 16.w, top: 20.h),
    //           color: kAppColor('#F8F8F8'),
    //           height: 132.h,
    //           radius: 4.w,
    //           child: TextField(
    //             controller: logic.describeController,
    //             decoration: InputDecoration(
    //               contentPadding: EdgeInsets.only(left: 10.w, top: 10.w, right: 10.w, bottom: 10.w),
    //               hintText: 'Start your creation...',
    //               hintStyle: TextStyle(color: kAppSub2TextColor, fontSize: 12.sp),
    //               border: const OutlineInputBorder(borderSide: BorderSide.none),
    //             ),
    //             style: TextStyle(color: kAppTextColor, fontSize: 12.sp),
    //             maxLength: 600,
    //             maxLines: 5,
    //             onChanged: (str) {
    //               logic.onCheckPostBtnEnable();
    //             },
    //           ),
    //         ),
    //         GetBuilder<WhlPublishLogic>(builder: (logic) {
    //           return UIRow(
    //             height: 48.h,
    //             width: double.infinity,
    //             margin: EdgeInsets.only(top: 45.h, left: 122.w, right: 122.w),
    //             mainAxisAlignment: MainAxisAlignment.center,
    //             decoration: BoxDecoration(
    //               border: Border.all(color: Colors.white, width: 2.w),
    //               borderRadius: BorderRadius.circular(30.w),
    //               gradient: LinearGradient(
    //                   colors: [logic.isBtnEnable ? kAppLeftColor : kAppColor('#CCCCCC'), logic.isBtnEnable ? kAppRightColor : kAppColor('#CCCCCC')],
    //                   begin: Alignment.centerLeft,
    //                   end: Alignment.centerRight),
    //             ),
    //             children: [
    //               Icon(
    //                 Icons.camera_alt,
    //                 color: Colors.white,
    //                 size: 18.w,
    //               ),
    //               UIText(
    //                 margin: EdgeInsets.only(left: 8.w),
    //                 text: 'Post',
    //                 textColor: Colors.white,
    //                 fontSize: 14.sp,
    //                 fontWeight: FontWeight.bold,
    //               )
    //             ],
    //             onTap: () async {
    //               logic.doClickPost();
    //             },
    //           );
    //         })
    //       ],
    //     ),
    //   ),
    // );
  }
}
