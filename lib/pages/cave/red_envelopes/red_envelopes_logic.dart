import 'package:get/get.dart';
import 'package:cave_flutter/network/whl_api.dart';

class Red_envelopesLogic extends GetxController {
  int caveId = 0;

  @override
  void onInit() {
    super.onInit();
    var arguments = Get.arguments;
    if (arguments != null) {
      caveId = arguments['caveId'];
    }
  }
}
