import 'package:cave_flutter/routes/whl_app_pages.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../common/common_widget.dart';
import '../../../style/whl_style.dart';
import 'red_envelopes_logic.dart';

class Red_envelopesPage extends StatelessWidget {
  Red_envelopesPage({Key? key}) : super(key: key);

  final logic = Get.put(Red_envelopesLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: UIColumn(
        children: [
          UIStack(
            children: [
              UIImage(
                width: ScreenUtil().screenWidth,
                height: 160.h,
                assetImage: "red_bg",
                fit: BoxFit.fill,
              ),
              UIImage(
                margin: EdgeInsets.only(
                    top: ScreenUtil().statusBarHeight + 10.w, left: 10.w),
                padding: EdgeInsets.all(5.w),
                assetImage: "icon_back_black",
                imageColor: kAppWhiteColor,
                width: 30.w,
                height: 30.w,
                onTap: () {
                  Get.back();
                },
              ),
              Center(
                child: UIText(
                  margin:
                      EdgeInsets.only(top: ScreenUtil().statusBarHeight + 10.w),
                  text: "发群红包",
                  fontSize: 16.sp,
                  textColor: kAppWhiteColor,
                ),
              ),
              UIColumn(
                margin: EdgeInsets.only(
                    left: 13.w,
                    right: 13.w,
                    top: ScreenUtil().statusBarHeight + 62.h),
                padding: EdgeInsets.symmetric(horizontal: 15.w),
                radius: 30.w,
                clipBehavior: Clip.hardEdge,
                color: kAppColor("#FEFAFB"),
                children: [
                  UIRow(
                    margin: EdgeInsets.only(top: 27.h),
                    padding: EdgeInsets.symmetric(horizontal: 11.w),
                    color: kAppWhiteColor,
                    radius: 10.w,
                    height: 52.h,
                    children: [
                      UIText(
                        margin: EdgeInsets.only(right: 50.w),
                        text: "红包个数",
                        fontSize: 16.sp,
                        textColor: kAppColor("#E93323"),
                      ),
                      Expanded(
                          child: UIContainer(
                        child: noBorderTextField(
                          keyboardType: TextInputType.number,
                          textAlign: TextAlign.right,
                          hint: "0",
                          textColor: kAppSubTextColor,
                          hintTextColor: kAppSub3TextColor,
                          fontSize: 16.sp,
                          contentPadding: EdgeInsets.only(top: 4.h),
                        ),
                      )),
                      UIText(
                        margin: EdgeInsets.only(left: 5.w),
                        text: "个",
                        fontSize: 16.sp,
                        textColor: kAppColor("#E93323"),
                      ),
                    ],
                  ),
                  UIRow(
                    margin: EdgeInsets.only(top: 10.h),
                    padding: EdgeInsets.symmetric(horizontal: 11.w),
                    color: kAppWhiteColor,
                    radius: 10.w,
                    height: 52.h,
                    children: [
                      UIText(
                        margin: EdgeInsets.only(right: 50.w),
                        text: "C币金额",
                        fontSize: 16.sp,
                        textColor: kAppColor("#E93323"),
                      ),
                      Expanded(
                          child: UIContainer(
                        child: noBorderTextField(
                          keyboardType: TextInputType.number,
                          textAlign: TextAlign.right,
                          hint: "0.00",
                          textColor: kAppSubTextColor,
                          hintTextColor: kAppSub3TextColor,
                          fontSize: 16.sp,
                          contentPadding: EdgeInsets.only(top: 4.h),
                        ),
                      )),
                      UIText(
                        margin: EdgeInsets.only(left: 5.w),
                        text: "币",
                        fontSize: 16.sp,
                        textColor: kAppColor("#E93323"),
                      ),
                    ],
                  ),
                  UIContainer(
                    height: 74.h,
                    radius: 10.w,
                    margin: EdgeInsets.only(top: 10.h),
                    color: kAppWhiteColor,
                    child: Center(
                      child: noBorderTextField(
                        textAlign: TextAlign.center,
                        hint: "恭喜发财 大吉大利",
                        textColor: kAppSubTextColor,
                        hintTextColor: kAppSub3TextColor,
                        fontSize: 16.sp,
                        // contentPadding: EdgeInsets.only(top: 4.h),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 16.h),
                    child: Text.rich(TextSpan(
                      text: "0.00",
                      style: TextStyle(
                          fontSize: 46.sp,
                          color: kAppColor("#E93323"),
                          fontWeight: FontWeight.bold),
                      children: [
                        TextSpan(
                            text: "币",
                            style: TextStyle(
                                fontSize: 18.sp,
                                color: kAppColor("#E93323"),
                                ))
                      ],
                    )),
                  ),
                  UIText(
                    height: 58.h,
                    width: 155.w,
                    radius: 29.h,
                    margin: EdgeInsets.only(bottom: 45.h),
                    text: "发送红包",
                    textColor: kAppWhiteColor,
                    fontSize: 16.sp,
                    color: kAppColor("#EE8499"),
                    alignment: Alignment.center,
                    onTap: (){
                      Get.toNamed(Routes.createCavePage);
                    },
                  )
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}
