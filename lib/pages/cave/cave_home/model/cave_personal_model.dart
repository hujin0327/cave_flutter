class PersonalCategory {
  String? classId;
  String? className;
  List<CaveModel>? caves = [];

  PersonalCategory({
    this.classId,
    this.className,
    this.caves
  });

}

class CaveModel {
    int? id;
    String? caveName = "";
    String? caveImg;



    CaveModel({
      this.id,
      this.caveName = "",
      this.caveImg,
     
    });
}
