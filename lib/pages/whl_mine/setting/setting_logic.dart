import 'package:cave_flutter/model/base_model.dart';
import 'package:cave_flutter/pages/whl_mine/about_me/about_me_view.dart';
import 'package:cave_flutter/pages/whl_mine/blacklist/blacklist_view.dart';
import 'package:cave_flutter/pages/whl_mine/friend_setting/friend_setting_view.dart';
import 'package:cave_flutter/pages/whl_mine/gift_wall/gift_wall_view.dart';
import 'package:cave_flutter/pages/whl_mine/my_ranking/my_ranking_view.dart';
import 'package:cave_flutter/pages/whl_mine/privacy_setting/privacy_setting_view.dart';
import 'package:cave_flutter/pages/whl_mine/setter_call_price/setter_call_price_view.dart';
import 'package:cave_flutter/pages/whl_mine/setting/mine_contact/mine_contact_view.dart';
import 'package:cave_flutter/routes/whl_app_pages.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:get/get.dart';

import '../collection/collection_view.dart';

enum SettingType {
  attire,
  collection,
  medal,
  gift,
  black,
  rank,
  secret,
  account,
  friend,
  notice,
  video,
  share,
  cave,
  contact,
  about,
}

class SettingLogic extends GetxController {
  List<BaseModel> firstList = [
    BaseModel(
        icon: 'icon_setting_attire.png',
        title: '我的装扮',
        settingType: SettingType.attire),
    BaseModel(
        icon: 'icon_setting_collection.png',
        title: '我的收藏',
        settingType: SettingType.collection),
    BaseModel(
        icon: 'icon_setting_medal.png',
        title: '勋章墙',
        settingType: SettingType.medal),
    BaseModel(
        icon: 'icon_setting_gift.png',
        title: '礼物墙',
        settingType: SettingType.gift),
    BaseModel(
        icon: 'icon_setting_rank.png',
        title: '排行榜',
        settingType: SettingType.rank),
    BaseModel(
        icon: 'icon_setting_black.png',
        title: '小黑屋',
        settingType: SettingType.black),
  ];
  List<BaseModel> secondList = [
    BaseModel(
        icon: 'icon_setting_secret.png',
        title: '隐私管理',
        settingType: SettingType.secret),
  ];
  List<BaseModel> thirdList = [
    BaseModel(
        icon: 'icon_setting_account.png',
        title: '账号与安全',
        settingType: SettingType.account),
    BaseModel(
        icon: 'icon_setting_friend_add.png',
        title: '好友申请设置',
        settingType: SettingType.friend),
    BaseModel(
        icon: 'icon_setting_notice.png',
        title: '消息与通知',
        settingType: SettingType.notice),
    BaseModel(
        icon: 'icon_setting_video_chat.png',
        title: '视频语音连线设置',
        settingType: SettingType.video),
  ];
  List<BaseModel> fourthList = [
    BaseModel(
        icon: 'icon_setting_share.png',
        title: '分享Cave App',
        settingType: SettingType.share),
    BaseModel(
        icon: 'icon_setting_cave.png',
        title: '共建Cave',
        settingType: SettingType.cave),
    BaseModel(
        icon: 'icon_setting_contact_us.png',
        title: '联系我们',
        settingType: SettingType.contact),
    BaseModel(
        icon: 'icon_setting_about_us.png',
        title: '关于我们',
        settingType: SettingType.about),
  ];

  doClickItem(BaseModel model) {
    if (model.settingType == SettingType.account) {
      Get.toNamed(Routes.accountSafe);
    } else if (model.settingType == SettingType.collection) {
      Get.to(() => CollectionPage());
    } else if (model.settingType == SettingType.gift) {
      Get.to(() => Gift_wallPage());
    } else if (model.settingType == SettingType.about) {
      Get.to(() => About_mePage());
    } else if (model.settingType == SettingType.share) {
      Get.toNamed(Routes.share);
    } else if (model.settingType == SettingType.video) {
      Get.to(() => Setter_call_pricePage());
    } else if (model.settingType == SettingType.black) {
      Get.to(() => BlacklistPage());
    } else if (model.settingType == SettingType.rank) {
      Get.to(() => My_rankingPage());
    } else if (model.settingType == SettingType.secret) {
      Get.to(() => PrivacySettingPage());
    } else if (model.settingType == SettingType.friend) {
      Get.to(() => FriendSettingPage());
    } else if (model.settingType == SettingType.medal) {
      Get.toNamed(Routes.medalWall);
    } else if (model.settingType == SettingType.contact) {
      Get.to(() => MineContactPage());
    } else {
      MyToast.show('该功能暂未开放');
    }
  }
}
