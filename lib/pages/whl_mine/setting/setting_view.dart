import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/whl_user_utils.dart';
import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:cave_flutter/widgets/whl_row_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../widgets/brick/brick.dart';
import 'setting_logic.dart';

class SettingPage extends StatelessWidget {
  SettingPage({Key? key}) : super(key: key);

  final logic = Get.put(SettingLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: UIContainer(
        color: kAppColor('#F6F6F6'),
        margin: EdgeInsets.only(left: 62.w),
        child: SingleChildScrollView(
          child: UIColumn(
            crossAxisAlignment: CrossAxisAlignment.start,
            padding: EdgeInsets.only(top: ScreenUtil().statusBarHeight, left: 16.w, right: 16.w, bottom: ScreenUtil().bottomBarHeight),
            width: 314.w,
            children: [
              UIText(
                text: '关于我的',
                textColor: kAppColor('#9C9C9C'),
                fontSize: 14.sp,
              ),
              UIColumn(
                margin: EdgeInsets.only(top: 14.w),
                padding: EdgeInsets.symmetric(vertical: 5.w),
                color: Colors.white,
                radius: 12.w,
                children: logic.firstList.map((e) {
                  return MyRowItem(
                    padding: EdgeInsets.symmetric(vertical: 13.w, horizontal: 14.w),
                    title: e.title,
                    titleFontWeight: FontWeight.w500,
                    titleColor: kAppTextColor,
                    icon: e.icon,
                    showArrow: false,
                    onTap: () {
                      logic.doClickItem(e);
                    },
                  );
                }).toList(),
              ),
              UIText(
                margin: EdgeInsets.only(top: 24.w),
                text: 'Cave服务',
                textColor: kAppColor('#9C9C9C'),
                fontSize: 14.sp,
              ),
              UIColumn(
                margin: EdgeInsets.only(top: 14.w),
                color: Colors.white,
                radius: 12.w,
                children: logic.secondList.map((e) {
                  return MyRowItem(
                    padding: EdgeInsets.symmetric(vertical: 13.w, horizontal: 14.w),
                    title: e.title,
                    titleFontWeight: FontWeight.w500,
                    titleColor: kAppTextColor,
                    icon: e.icon,
                    showArrow: false,
                    onTap: () {
                      logic.doClickItem(e);
                    },
                  );
                }).toList(),
              ),
              UIText(
                margin: EdgeInsets.only(top: 24.w),
                text: '其他',
                textColor: kAppColor('#9C9C9C'),
                fontSize: 14.sp,
              ),
              UIColumn(
                margin: EdgeInsets.only(top: 14.w),
                color: Colors.white,
                radius: 12.w,
                children: logic.thirdList.map((e) {
                  return MyRowItem(
                    padding: EdgeInsets.symmetric(vertical: 13.w, horizontal: 14.w),
                    title: e.title,
                    titleFontWeight: FontWeight.w500,
                    titleColor: kAppTextColor,
                    icon: e.icon,
                    showArrow: false,
                    onTap: () {
                      logic.doClickItem(e);
                    },
                  );
                }).toList(),
              ),
              UIColumn(
                margin: EdgeInsets.only(top: 10.w),
                color: Colors.white,
                radius: 12.w,
                children: logic.fourthList.map((e) {
                  return MyRowItem(
                    padding: EdgeInsets.symmetric(vertical: 13.w, horizontal: 14.w),
                    title: e.title,
                    titleFontWeight: FontWeight.w500,
                    titleColor: kAppTextColor,
                    icon: e.icon,
                    showArrow: false,
                    onTap: () {
                      logic.doClickItem(e);
                    },
                  );
                }).toList(),
              ),
              UISolidButton(
                margin: EdgeInsets.only(top: 77.h),
                text: '退出',
                onTap: () {
                  WhlUserUtils.loginOut();
                },
              ),
              UIContainer(
                height: 44.w,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
