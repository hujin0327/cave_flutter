import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/utils/task_utils.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:cave_flutter/utils/whl_user_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'user_verification_model.dart';

class UserVerificationLogic extends GetxController {
  TextEditingController nameController = TextEditingController();
  TextEditingController idController = TextEditingController();
  UserVerificationModel? userVerificationModel;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    loadData();
  }

  void loadData() async {
    ResponseData responseData =
        await WhlApi.myUserVerification.get({}, withLoading: true);
    if (responseData.isSuccess()) {
      // responseData.data;
      // Get.toNamed(Routes.setterPasswordPage, arguments: params);
      if (responseData.data != null) {
        userVerificationModel =
            UserVerificationModel.fromJson(responseData.data);
      }
      nameController.text = userVerificationModel?.realName ?? '';
      idController.text = userVerificationModel?.identityNumber ?? '';
      update();
    } else {
      MyToast.show(responseData.msg ?? "获取实名信息失败");
    }
  }

  void onConfirm() async {
    if (nameController.text.isEmpty) {
      MyToast.show('请输入真实姓名');
      return;
    }

    if (idController.text.length != 18) {
      MyToast.show('请输入18位身份证号');
      return;
    }

    // identityType 证件类型 ID_card：身份在，passport：护照，driver_license：驾驶证
    ResponseData responseData = await WhlApi.userVerification.post({
      "identityNumber": idController.text,
      "identityType": "ID_card",
      "realName": nameController.text,
      "userId": WhlUserUtils.getUserInfo().userId
    }, withLoading: true);
    if (responseData.isSuccess()) {
      // Get.toNamed(Routes.setterPasswordPage, arguments: params);
      MyToast.show("实名认证成功");
      userVerificationModel = UserVerificationModel(
          realName: nameController.text, identityNumber: idController.text);
      TaskUtils.complateTask(3, type: 1);
      update();
    } else {
      MyToast.show(responseData.msg ?? "实名认证失败");
    }
  }
}
