class UserVerificationModel {
  final String? realName;
  final String? identityNumber;
  final String? identityType;
  final String? verificationTime;

  UserVerificationModel({
    this.realName,
    this.identityNumber,
    this.identityType,
    this.verificationTime,
  });

  factory UserVerificationModel.fromJson(Map<String, dynamic> json) {
    return UserVerificationModel(
      realName: json['realName'],
      identityNumber: json['identityNumber'],
      identityType: json['identityType'],
      verificationTime: json['verificationTime'],
    );
  }

  Map<String, dynamic> toJson() => {
        'realName': realName,
        'identityNumber': identityNumber,
        'identityType': identityType,
        'verificationTime': verificationTime,
      };
}
