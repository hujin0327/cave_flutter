import 'package:cave_flutter/common/common_widget.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/widget/basic_widget.dart';
import 'package:cave_flutter/widgets/brick/widget/text_widget.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'verify_mobile_logic.dart';

class VerifyMobilePage extends StatelessWidget {
  VerifyMobilePage({super.key});
  final logic = Get.put(VerifyMobileLogic());
  // ScreenUtil().statusBarHeight
  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = EdgeInsets.symmetric(horizontal: 0.w, vertical: 13.w);
    return Scaffold(
      backgroundColor: kAppWhiteColor,
      appBar: WhlAppBar(
        centerTitle: "忘记密码",
        backgroundColor: kAppWhiteColor,
      ),
      body: GetBuilder<VerifyMobileLogic>(builder: (logic) {
        return UIColumn(
            crossAxisAlignment: CrossAxisAlignment.start,
            padding: EdgeInsets.only(top: 0.w, left: 26.w, right: 26.w, bottom: 0.w),
            children: [
              // 手机号输入框
              UIRow(
                // padding: EdgeInsets.symmetric(horizontal: 20.w),
                margin: EdgeInsets.only(top: 20.h),
                children: [
                  UIContainer(
                    margin: EdgeInsets.only(right: 26.w),
                    width: 234.w,
                    height: 50.h,
                    radius: 18.w,
                    color: kAppColor("#F7F7F9"),
                    child: noBorderCTextField(
                        controller: logic.phoneNumberController,
                        contentPadding: EdgeInsets.only(top: 15.h, bottom: 15.h, left: 15.w),
                        hint: "请输入手机号",
                        obscureText: false,
                        readOnly: true,
                        hintTextColor: kAppHitTextColor,
                        textColor: kAppSubTextColor),
                  ),
                  if (logic.counting)
                    Expanded(
                        child: UIText(
                      alignment: Alignment.center,
                      text: '${logic.countDown}s',
                      textColor: kAppDisabledTextColor,
                      fontSize: 11.sp,
                      fontWeight: FontWeight.bold,
                      textAlign: TextAlign.center,
                    ))
                  else
                    Expanded(
                        child: UIText(
                      alignment: Alignment.center,
                      text: logic.resend ? "再次发送" : "发送验证码",
                      textColor: kAppBlackColor,
                      fontSize: 11.sp,
                      fontWeight: FontWeight.bold,
                      textAlign: TextAlign.center,
                      onTap: () {
                        logic.doClickSendPhoneCode();
                      },
                    ))
                ],
              ),
              // 验证码输入框
              UIContainer(
                // padding: EdgeInsets.symmetric(horizontal: 20.w),
                margin: EdgeInsets.only(top: 20.h),
                height: 50.h,
                radius: 18.w,
                color: kAppColor("#F7F7F9"),
                child: noBorderCTextField(
                    controller: logic.verificationCodeController,
                    contentPadding: EdgeInsets.only(top: 15.h, bottom: 15.h, left: 15.w),
                    hint: "请输入验证码",
                    obscureText: false,
                    hintTextColor: kAppHitTextColor,
                    textColor: kAppSubTextColor),
              ),
              const Expanded(child: UIContainer()),
              UIText(
                margin: EdgeInsets.symmetric(horizontal: 0.w, vertical: 60.h),
                text: "确定",
                textColor: kAppWhiteColor,
                fontSize: 16.sp,
                fontWeight: FontWeight.bold,
                color: kAppBlackColor,
                height: 52.h,
                radius: 26.h,
                alignment: Alignment.center,
                onTap: () {
                  logic.onConfirm();
                },
              )
            ]);
      }),
    );
  }
}
