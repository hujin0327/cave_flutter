import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class Update_passwordLogic extends GetxController {
  TextEditingController oldPasswordController = TextEditingController();
  TextEditingController newPasswordController = TextEditingController();
  TextEditingController cPasswordController = TextEditingController();

  onConfirm() {
    if (oldPasswordController.text.isEmpty) {
      MyToast.show("请输入旧密码");
      return;
    }
    if (newPasswordController.text.isEmpty) {
      MyToast.show("请输入新密码");
      return;
    }
    if (cPasswordController.text.isEmpty) {
      MyToast.show("请确认新密码");
      return;
    }
    if (newPasswordController.text != cPasswordController.text) {
      MyToast.show("确认密码与新密码不匹配");
      return;
    }
    WhlApi.updatePassword
        .post({"password": newPasswordController.text, "oldpassword": oldPasswordController.text}, withLoading: true).then((value) {
      if (value.isSuccess()) {
        MyToast.show("修改密码成功");
      }
    });
  }
}
