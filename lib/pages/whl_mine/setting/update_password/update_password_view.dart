import 'package:cave_flutter/common/common_widget.dart';
import 'package:cave_flutter/pages/whl_mine/setting/verify_mobile/verify_mobile_view.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'update_password_logic.dart';

class Update_passwordPage extends StatelessWidget {
  Update_passwordPage({Key? key}) : super(key: key);

  final logic = Get.put(Update_passwordLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppWhiteColor,
      appBar: WhlAppBar(backgroundColor: kAppWhiteColor, centerTitle: "修改密码", isShowBottomLine: false),
      body: UIColumn(
        children: [
          getInputWidget("原密码", logic.oldPasswordController),
          getInputWidget("设置新密码", logic.newPasswordController),
          getInputWidget("确认密码", logic.cPasswordController),
          UIRow(
            margin: EdgeInsets.only(left: ScreenUtil().screenWidth - 260.w - 20.w, top: 15.h),
            children: [
              Expanded(
                  child: UIText(
                text: "8-20位，包含字母、数字",
                textColor: kAppSub3TextColor,
                fontSize: 11.sp,
              )),
              UIText(
                margin: EdgeInsets.only(right: 28.w),
                text: "忘记密码",
                textColor: kAppSubTextColor,
                fontSize: 11.sp,
                onTap: () {
                  // Get.toNamed(Routes.forgotPassword);
                  Get.to(() => VerifyMobilePage());
                },
              )
            ],
          ),
          Expanded(child: Container()),
          UIText(
            margin: EdgeInsets.symmetric(horizontal: 20.w, vertical: 60.h),
            text: "确定",
            textColor: kAppWhiteColor,
            fontSize: 16.sp,
            fontWeight: FontWeight.bold,
            color: kAppBlackColor,
            height: 52.h,
            radius: 26.h,
            alignment: Alignment.center,
            onTap: () {
              logic.onConfirm();
            },
          )
        ],
      ),
    );
  }

  getInputWidget(String title, TextEditingController controller) {
    return UIRow(
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      margin: EdgeInsets.only(top: 9.h),
      children: [
        Expanded(
            child: UIText(
          text: title,
          textColor: kAppBlackColor,
          fontSize: 13.sp,
          fontWeight: FontWeight.bold,
        )),
        UIContainer(
          width: 260.w,
          height: 50.h,
          radius: 18.w,
          color: kAppColor("#F7F7F9"),
          child: noBorderCTextField(
              controller: controller,
              contentPadding: EdgeInsets.only(top: 15.h, bottom: 15.h, left: 15.w),
              hint: "请输入",
              obscureText: true,
              hintTextColor: kAppColor("#C1C1C1"),
              textColor: kAppSubTextColor),
        )
      ],
    );
  }
}
