import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/widget/basic_widget.dart';
import 'package:cave_flutter/widgets/brick/widget/text_widget.dart';
import 'package:cave_flutter/widgets/empty_widget.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'shipping_address_list_logic.dart';

class ShippingAddressListPage extends StatelessWidget {
  ShippingAddressListPage({super.key});
  final logic = Get.put(ShippingAddressListLogic());
  // ScreenUtil().statusBarHeight
  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = EdgeInsets.symmetric(horizontal: 0.w, vertical: 13.w);
    return Scaffold(
      backgroundColor: kAppWhiteColor,
      appBar: WhlAppBar(
        centerTitle: "我的收货地址",
        backgroundColor: kAppWhiteColor,
      ),
      body: GetBuilder<ShippingAddressListLogic>(builder: (logic) {
        List<Widget> items = [];
        for (ShippingAddressItemModel item in logic.items) {
          items.add(UIRow(
            onTap: () {
              logic.editItem(item);
            },
            children: [
              Expanded(
                  child: UIContainer(
                margin: EdgeInsets.only(
                  top: 18.w,
                ),
                radius: 16.w,
                strokeColor: kAppColor('#DADADA'),
                strokeWidth: 0.5.w,
                // color: kAppColor("#F7F7F9"),
                child: UIColumn(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.w),
                  children: [
                    UIRow(
                      children: [
                        UIText(
                          text: item.name,
                          textColor: kAppTextColor,
                          fontSize: 14.sp,
                        ),
                        UIText(
                          margin: EdgeInsets.only(left: 10.w),
                          text: item.phone,
                          textColor: kAppSubTextColor,
                          fontSize: 14.sp,
                        ),
                        if (item.isDefault == 1)
                          UIText(
                            padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 1.w),
                            margin: EdgeInsets.only(left: 10.w),
                            radius: 4.w,
                            text: '默认',
                            textColor: Colors.white,
                            color: kAppTextColor,
                            fontSize: 12.sp,
                          ),
                      ],
                    ),
                    UIText(
                      margin: EdgeInsets.only(top: 5.w),
                      text: item.receiveAddress,
                      textColor: kAppSub2TextColor,
                      fontSize: 14.sp,
                    ),
                    UIRow(
                      mainAxisAlignment: MainAxisAlignment.end,
                      margin: EdgeInsets.only(top: 5.w),
                      children: [
                        UIText(
                          text: '删除',
                          textColor: kAppTextColor,
                          fontSize: 14.sp,
                          onTap: () {
                            logic.deleteItem(item);
                          },
                        ),
                      ],
                    )
                  ],
                ),
              ))
            ],
          ));
        }

        return UIColumn(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: UIColumn(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  padding: EdgeInsets.only(top: 0.w, left: 16.w, right: 16.w, bottom: 0.w),
                  children: items.isNotEmpty
                      ? items
                      : [
                          MyEmptyWidget(
                            margin: EdgeInsets.only(top: 200.h),
                          )
                        ],
                ),
              ),
            ),
            UIText(
              margin: EdgeInsets.symmetric(horizontal: 16.w, vertical: 60.h),
              text: "添加收货地址",
              textColor: kAppWhiteColor,
              fontSize: 16.sp,
              fontWeight: FontWeight.bold,
              color: kAppBlackColor,
              height: 52.h,
              radius: 26.h,
              alignment: Alignment.center,
              onTap: () {
                logic.addItem();
              },
            )
          ],
        );
      }),
    );
  }
}
