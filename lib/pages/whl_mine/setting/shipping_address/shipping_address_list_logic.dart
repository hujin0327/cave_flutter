import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/pages/whl_mine/setting/shipping_address/edit_shipping_address_view.dart';
import 'package:cave_flutter/utils/whl_dialog_util.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:get/get.dart';

class ShippingAddressListLogic extends GetxController {
  List<ShippingAddressItemModel> items = [];

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    requestData();
  }

  void requestData() async {
    ResponseData responseData = await WhlApi.getUserReceiveAddress.post({}, withLoading: true);
    if (responseData.isSuccess()) {
      // responseData.data;
      // Get.toNamed(Routes.setterPasswordPage, arguments: params);
      items.clear();
      for (var item in responseData.data ?? []) {
        items.add(ShippingAddressItemModel.fromJson(item));
      }
      update();
    } else {
      MyToast.show(responseData.msg ?? "获取收货地址信息失败");
    }
  }

  void addItem() {
    Get.to(() => EditShippingAddressPage());
  }

  void editItem(ShippingAddressItemModel item) {
    Get.to(() => EditShippingAddressPage(), arguments: {'item': item});
  }

  void deleteItem(ShippingAddressItemModel item) {
    WhlDialogUtil.showConfirmDialog(
      '',
      description: '确认删除当前收货地址？',
      sureString: '确定',
      cancelString: '取消',
      sureAction: () {},
    );
  }
}

class ShippingAddressItemModel {
  int id = 0;
  int isDefault = 0;
  String name = '';
  String phone = '';
  String receiveAddress = '';

  ShippingAddressItemModel({
    this.id = 0,
    this.isDefault = 0,
    this.name = '',
    this.phone = '',
    this.receiveAddress = '',
  });

  ShippingAddressItemModel.fromJson(Map<String, dynamic> json) {
    id = json['id'] ?? 0;
    isDefault = json['isDefault'] ?? 0;
    name = json['name'] ?? '';
    phone = json['phone'] ?? '';
    receiveAddress = json['receiveAddress'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['isDefault'] = isDefault;
    data['name'] = name;
    data['phone'] = phone;
    data['receiveAddress'] = receiveAddress;
    return data;
  }
}
