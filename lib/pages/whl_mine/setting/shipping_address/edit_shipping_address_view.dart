import 'package:cave_flutter/common/common_widget.dart';
import 'package:cave_flutter/pages/whl_mine/setting/shipping_address/edit_shipping_address_logic.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/widget/basic_widget.dart';
import 'package:cave_flutter/widgets/brick/widget/image_widget.dart';
import 'package:cave_flutter/widgets/brick/widget/text_widget.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class EditShippingAddressPage extends StatelessWidget {
  EditShippingAddressPage({super.key});
  final logic = Get.put(EditShippingAddressLogic());
  // ScreenUtil().statusBarHeight
  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = EdgeInsets.symmetric(horizontal: 0.w, vertical: 13.w);
    return GetBuilder<EditShippingAddressLogic>(builder: (logic) {
      return Scaffold(
        backgroundColor: kAppWhiteColor,
        appBar: WhlAppBar(
          centerTitle: logic.item == null ? '添加收货地址' : '编辑收货地址',
          backgroundColor: kAppWhiteColor,
        ),
        body: UIColumn(
          crossAxisAlignment: CrossAxisAlignment.start,
          padding: EdgeInsets.only(top: 20.w, left: 26.w, right: 26.w, bottom: 0.w),
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: UIColumn(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    UIText(
                      text: "姓名",
                      textColor: kAppColor("#444343"),
                      fontSize: 14.sp,
                      fontWeight: FontWeight.bold,
                    ),
                    UIContainer(
                      margin: EdgeInsets.only(
                        top: 10.h,
                      ),
                      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: kAppColor('#CAC9C9'), width: 0.5.w))),
                      height: 40.h,
                      // color: kAppColor("#F7F7F9"),
                      child: noBorderCTextField(
                          controller: logic.nameController,
                          hint: "请填写收件人姓名",
                          obscureText: false,
                          hintTextColor: kAppHitTextColor,
                          fontSize: 16.sp,
                          textColor: kAppSubTextColor),
                    ),
                    UIContainer(
                      margin: EdgeInsets.only(top: 30.w),
                      child: UIText(
                        text: "手机号",
                        textColor: kAppColor("#444343"),
                        fontSize: 14.sp,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    UIContainer(
                      // padding: EdgeInsets.symmetric(horizontal: 20.w),
                      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: kAppColor('#CAC9C9'), width: 0.5.w))),
                      margin: EdgeInsets.only(top: 10.h),
                      height: 40.h,
                      // color: kAppColor("#F7F7F9"),
                      child: noBorderCTextField(
                          controller: logic.phoneController,
                          hint: "请填写收件人手机号",
                          obscureText: false,
                          fontSize: 16.sp,
                          hintTextColor: kAppHitTextColor,
                          textColor: kAppSubTextColor),
                    ),
                    UIContainer(
                      margin: EdgeInsets.only(top: 30.w),
                      child: UIText(
                        text: "收件地址",
                        textColor: kAppColor("#444343"),
                        fontSize: 14.sp,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    UIContainer(
                      strokeColor: kAppColor('#DADADA'),
                      strokeWidth: 0.5.w,
                      margin: EdgeInsets.only(top: 18.w),
                      radius: 16.w,
                      child: TextField(
                        controller: logic.addressController,
                        maxLines: 5,
                        // fontSize: 14.sp,
                        decoration: InputDecoration(
                          hintText: '请填写收件地址',
                          hintStyle: TextStyle(color: kAppColor('#CCCCCC'), fontSize: 14.sp),
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 15.w),
                        ),
                      ),
                    ),
                    UIRow(
                      padding: EdgeInsets.symmetric(vertical: 30.w),
                      onTap: () {
                        logic.setDefault();
                      },
                      children: [
                        UIImage(
                          padding: EdgeInsets.all(3.w),
                          margin: EdgeInsets.only(right: 10.w),
                          assetImage: logic.isDefault == 1 ? "icon_select" : "vip_select_no",
                          width: 24.w,
                          height: 24.w,
                        ),
                        UIText(
                          text: "设为默认",
                          textColor: kAppSubTextColor,
                          fontSize: 14.sp,
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
            UIText(
              margin: EdgeInsets.only(top: 30.h, bottom: 60.h),
              text: "确认",
              textColor: kAppWhiteColor,
              fontSize: 15.sp,
              color: kAppBlackColor,
              height: 52.h,
              radius: 26.h,
              alignment: Alignment.center,
              onTap: () {
                logic.onConfirm();
              },
            ),
          ],
        ),
      );
    });
  }
}
