import 'package:cave_flutter/model/base_model.dart';
import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/pages/whl_mine/setting/bind_alipay/bind_alipay_view.dart';
import 'package:cave_flutter/pages/whl_mine/setting/bind_platform/bind_platform_logic.dart';
import 'package:cave_flutter/pages/whl_mine/setting/bind_platform/bind_platform_view.dart';
import 'package:cave_flutter/pages/whl_mine/setting/change_bind_mobile/change_bind_mobile_view.dart';
import 'package:cave_flutter/pages/whl_mine/setting/real_name_authentication/user_verification_view.dart';
import 'package:cave_flutter/pages/whl_mine/setting/shipping_address/shipping_address_list_view.dart';
import 'package:cave_flutter/pages/whl_mine/setting/update_password/update_password_view.dart';
import 'package:cave_flutter/pages/whl_mine/switch_account/switch_account_view.dart';
import 'package:cave_flutter/utils/whl_dialog_util.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:cave_flutter/utils/whl_user_utils.dart';
import 'package:get/get.dart';

enum AccountSafeType {
  changePhone,
  resetPassword,
  address,
  certification,
  alipay,
  wechat,
  qq,
  weibo,
  switchAccount,
  logout,
}

class AccountSafeLogic extends GetxController {
  List<BaseModel> list = [
    BaseModel(
        title: '修改绑定手机号',
        subTitle: (WhlUserUtils.getUserInfo().phone ?? '').length == 11 ? WhlUserUtils.getUserInfo().phone!.replaceRange(3, 7, "****") : '',
        accountSafeType: AccountSafeType.changePhone),
    BaseModel(title: '重置密码', accountSafeType: AccountSafeType.resetPassword),
    BaseModel(title: '我的收货地址', accountSafeType: AccountSafeType.address),
    BaseModel(title: '实名认证', accountSafeType: AccountSafeType.certification),
    BaseModel(title: '绑定支付宝', accountSafeType: AccountSafeType.alipay),
    BaseModel(title: '绑定微信', accountSafeType: AccountSafeType.wechat),
    BaseModel(title: '绑定QQ', accountSafeType: AccountSafeType.qq),
    BaseModel(title: '绑定微博', accountSafeType: AccountSafeType.weibo),
    BaseModel(title: '切换账号', accountSafeType: AccountSafeType.switchAccount),
    BaseModel(title: '注销账号', accountSafeType: AccountSafeType.logout),
  ];

  void itemAction(AccountSafeType? safeType) {
    switch (safeType) {
      case AccountSafeType.changePhone:
        Get.to(() => ChangeBindMobilePage());
        break;
      case AccountSafeType.resetPassword:
        Get.to(() => Update_passwordPage());
        break;

      case AccountSafeType.address:
        Get.to(() => ShippingAddressListPage());
        break;

      case AccountSafeType.switchAccount:
        Get.to(() => Switch_accountPage());
        break;

      case AccountSafeType.certification:
        Get.to(() => UserVerificationPage());
        break;
      case AccountSafeType.logout:
        {
          WhlDialogUtil.showConfirmDialog("确定注销当前账号？", description: "注销成功后，账号将在30天后被释放", sureString: "确定", cancelString: "取消",
              sureAction: () {
            userLogout();
          });
        }
        break;
      case AccountSafeType.wechat:
        Get.to(() => BindPlatformPage(), arguments: {'platform': BindPlatformType.wechat});
        break;

      case AccountSafeType.qq:
        Get.to(() => BindPlatformPage(), arguments: {'platform': BindPlatformType.qq});
        break;

      case AccountSafeType.weibo:
        Get.to(() => BindPlatformPage(), arguments: {'platform': BindPlatformType.microblog});
        break;

      case AccountSafeType.alipay:
        Get.to(() => BindAlipayPage());
        break;

      default:
        break;
    }
  }

  void userLogout() async {
    ResponseData responseData = await WhlApi.userLogout.post({}, withLoading: true);
    if (responseData.isSuccess()) {
      MyToast.show("注销账号成功");
      WhlUserUtils.loginOut();
    } else {
      MyToast.show(responseData.msg ?? "注销账号失败");
    }
  }
}
