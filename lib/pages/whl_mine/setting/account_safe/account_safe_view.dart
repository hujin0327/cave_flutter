import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:cave_flutter/widgets/whl_row_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../widgets/brick/widget/basic_widget.dart';
import 'account_safe_logic.dart';

class AccountSafePage extends StatelessWidget {
  AccountSafePage({Key? key}) : super(key: key);

  final logic = Get.put(AccountSafeLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: WhlAppBar(
        centerTitle: '账号与安全',
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        child: UIColumn(
          margin: EdgeInsets.only(top: 9.w),
          children: logic.list.map((e) {
            return MyRowItem(
              padding: EdgeInsets.symmetric(horizontal: 18.w, vertical: 12.w),
              title: e.title,
              titleColor: kAppColor('#444444'),
              titleFontWeight: FontWeight.w500,
              value: e.subTitle,
              onTap: (){
                logic.itemAction(e.accountSafeType);
              },
            );
          }).toList(),
        ),
      ),
    );
  }
}
