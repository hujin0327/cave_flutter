class BindAlipayModel {
  final String? realName;
  final String? payAccount;

  BindAlipayModel({
    this.realName,
    this.payAccount,
  });

  factory BindAlipayModel.fromJson(Map<String, dynamic> json) {
    return BindAlipayModel(
      realName: json['realName'],
      payAccount: json['payAccount'],
    );
  }

  Map<String, dynamic> toJson() => {
        'realName': realName,
        'payAccount': payAccount,
      };
}
