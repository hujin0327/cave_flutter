import 'package:cave_flutter/common/common_widget.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/widget/basic_widget.dart';
import 'package:cave_flutter/widgets/brick/widget/text_widget.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'bind_alipay_logic.dart';

class BindAlipayPage extends StatelessWidget {
  BindAlipayPage({super.key});
  final logic = Get.put(BindAlipayLogic());
  // ScreenUtil().statusBarHeight
  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = EdgeInsets.symmetric(horizontal: 0.w, vertical: 13.w);
    return Scaffold(
      backgroundColor: kAppWhiteColor,
      appBar: WhlAppBar(
        centerTitle: "绑定支付宝",
        backgroundColor: kAppWhiteColor,
      ),
      body: GetBuilder<BindAlipayLogic>(builder: (logic) {
        return UIColumn(
            crossAxisAlignment: CrossAxisAlignment.start,
            padding: EdgeInsets.only(top: 20.w, left: 26.w, right: 26.w, bottom: 0.w),
            children: [
              UIText(
                text: "真实姓名",
                textColor: kAppColor("#444343"),
                fontSize: 14.sp,
                fontWeight: FontWeight.bold,
              ),
              UIContainer(
                margin: EdgeInsets.only(
                  top: 10.h,
                ),
                decoration: BoxDecoration(border: Border(bottom: BorderSide(color: kAppColor('#CAC9C9'), width: 0.5.w))),
                height: 70.h,
                // color: kAppColor("#F7F7F9"),
                child: noBorderCTextField(
                    controller: logic.realNameController,
                    contentPadding: EdgeInsets.only(top: 15.h, bottom: 15.h, left: 0.w),
                    hint: "请填写您本人的真实姓名",
                    obscureText: false,
                    hintTextColor: kAppHitTextColor,
                    fontSize: 16.sp,
                    textColor: kAppSubTextColor),
              ),
              UIContainer(
                  margin: EdgeInsets.only(top: 30.w),
                  child: UIText(
                    text: "支付宝账号",
                    textColor: kAppColor("#444343"),
                    fontSize: 14.sp,
                    fontWeight: FontWeight.bold,
                  )),
              UIContainer(
                // padding: EdgeInsets.symmetric(horizontal: 20.w),
                decoration: BoxDecoration(border: Border(bottom: BorderSide(color: kAppColor('#CAC9C9'), width: 0.5.w))),
                margin: EdgeInsets.only(top: 10.h),
                height: 70.h,
                // color: kAppColor("#F7F7F9"),
                child: noBorderCTextField(
                    controller: logic.payAccountController,
                    contentPadding: EdgeInsets.only(top: 15.h, bottom: 15.h, left: 0.w),
                    hint: "请填写要绑定的支付宝账号",
                    obscureText: false,
                    fontSize: 16.sp,
                    hintTextColor: kAppHitTextColor,
                    textColor: kAppSubTextColor),
              ),
              const Expanded(child: UIContainer()),
              UIText(
                margin: EdgeInsets.only(top: 60.h, bottom: 60.h),
                text: "绑定",
                textColor: kAppWhiteColor,
                fontSize: 15.sp,
                color: kAppBlackColor,
                height: 52.h,
                radius: 26.h,
                alignment: Alignment.center,
                onTap: () {
                  logic.onConfirm();
                },
              ),

              /*
              UIText(
                margin: EdgeInsets.only(top: 20.h, bottom: 40.h),
                // text: "本次识别由网易云盾提供",
                textColor: kAppColor("#9F9F9F"),
                fontSize: 11.sp,
                alignment: Alignment.center,
                onTap: () {
                  logic.onConfirm();
                },
              )*/
            ]);
      }),
    );
  }
}
