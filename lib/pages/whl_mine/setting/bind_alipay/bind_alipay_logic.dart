import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/pages/whl_mine/setting/bind_alipay/bind_alipay_model.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:cave_flutter/utils/whl_user_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BindAlipayLogic extends GetxController {
  TextEditingController realNameController = TextEditingController();
  TextEditingController payAccountController = TextEditingController();
  BindAlipayModel? bindAlipayModel;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    loadData();
  }

  void loadData() async {
    ResponseData responseData = await WhlApi.myBindBindPayment.get({}, withLoading: true);
    if (responseData.isSuccess()) {
      // responseData.data;
      // Get.toNamed(Routes.setterPasswordPage, arguments: params);
      if (responseData.data != null) {
        bindAlipayModel = BindAlipayModel.fromJson(responseData.data);
      }
      realNameController.text = bindAlipayModel?.realName ?? '';
      payAccountController.text = bindAlipayModel?.payAccount ?? '';
      update();
    } else {
      MyToast.show(responseData.msg ?? "获取实名信息失败");
    }
  }

  void onConfirm() async {
    if (realNameController.text.isEmpty) {
      MyToast.show('请输入真实姓名');
      return;
    }

    if (payAccountController.text.isEmpty) {
      MyToast.show('请输入支付宝账号');
      return;
    }

    // identityType 证件类型 ID_card：身份在，passport：护照，driver_license：驾驶证
    ResponseData responseData = await WhlApi.bindBindPayment.post({
      "payAccount": payAccountController.text,
      "realName": realNameController.text,
      "userId": WhlUserUtils.getUserInfo().userId,
    }, withLoading: true);
    if (responseData.isSuccess()) {
      // Get.toNamed(Routes.setterPasswordPage, arguments: params);
      MyToast.show("实名认证成功");
      Get.back();
    } else {
      MyToast.show(responseData.msg ?? "实名认证失败");
    }
  }
}
