import 'package:cave_flutter/common/common_widget.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../style/whl_style.dart';
import '../../../../widgets/brick/widget/basic_widget.dart';
import '../../../../widgets/brick/widget/text_widget.dart';
import 'bind_platform_logic.dart';

class BindPlatformPage extends StatelessWidget {
  BindPlatformPage({Key? key}) : super(key: key);

  final logic = Get.put(BindPlatformLogic());

  @override
  Widget build(BuildContext context) {
    String platform = '';
    switch (logic.platform) {
      case BindPlatformType.wechat:
        platform = '微信';
        break;
      case BindPlatformType.qq:
        platform = 'QQ';
        break;
      case BindPlatformType.microblog:
        platform = '微博';
        break;
      default:
        break;
    }

    return Scaffold(
      backgroundColor: kAppWhiteColor,
      appBar: WhlAppBar(
        backgroundColor: kAppWhiteColor,
        centerTitle: '绑定$platform',
        isShowBottomLine: true,
      ),
      body: UIColumn(
          crossAxisAlignment: CrossAxisAlignment.start,
          color: kAppWhiteColor,
          padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 25.w),
          children: [
            noBorderCTextField(
              controller: logic.textEditingController,
              hint: "请输入${platform}账号",
              fontSize: 16.sp,
            ),
            const Expanded(child: UIContainer()),
            UIText(
              margin: EdgeInsets.symmetric(horizontal: 20.w, vertical: 60.h),
              text: "确定",
              textColor: kAppWhiteColor,
              fontSize: 16.sp,
              fontWeight: FontWeight.bold,
              color: kAppBlackColor,
              height: 52.h,
              radius: 26.h,
              alignment: Alignment.center,
              onTap: () {
                logic.onConfirm();
              },
            )
          ]),
    );
  }
}
