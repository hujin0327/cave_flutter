import 'package:cave_flutter/model/base_source_model.dart';
import 'package:cave_flutter/model/whl_user_info_model.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:cave_flutter/utils/whl_user_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../../../../network/whl_api.dart';
import '../../whl_mine_logic.dart';

enum BindPlatformType {
  wechat,
  qq,
  microblog,
}

class BindPlatformLogic extends GetxController {
  TextEditingController textEditingController = TextEditingController();
  BindPlatformType platform = BindPlatformType.wechat;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    var a = Get.arguments;
    if (a != null) {
      platform = a["platform"];
      // textEditingController.text = editModel.value ?? "";
      update();
    }
  }

  saveUserInfo(BaseItemModel model) {
    Map<String, dynamic> param = {model.requestKey ?? "": model.requestValue ?? model.value};
    WhlApi.saveUserBaseInfo.post(param, withLoading: true).then((value) {
      if (value.isSuccess()) {
        MyToast.show("保存成功");
        WhlUserInfoModel infoModel = WhlUserUtils.getUserInfo();
        infoModel.nickname = textEditingController.text;
        WhlUserUtils.saveUserInfo(infoModel);
        Get.back(result: true);
        final mineLogic = Get.find<WhlMineLogic>();
        mineLogic.update();
      }
    });
  }

  saveUserConfig(BaseItemModel model) {
    Map<String, dynamic> param = {model.requestKey ?? "": model.requestValue ?? model.value};
    WhlApi.saveUserInformation.post(param, withLoading: true).then((value) {
      if (value.isSuccess()) {
        MyToast.show("保存成功");
        Get.back(result: true);
      }
    });
  }

  void onConfirm() async {
    String platformName = '';
    var params = {};
    switch (platform) {
      case BindPlatformType.wechat:
        {
          platformName = '微信';
          params['wechat'] = textEditingController.text;
        }
        break;
      case BindPlatformType.qq:
        platformName = 'QQ';
        params['qq'] = textEditingController.text;
        break;
      case BindPlatformType.microblog:
        platformName = '微博';
        params['microblog'] = textEditingController.text;
        break;
      default:
        break;
    }

    if (textEditingController.text.isNotEmpty) {
      ResponseData responseData = await WhlApi.userBind.post(params, withLoading: true);
      if (responseData.isSuccess()) {
        MyToast.show("绑定成功");
        Get.back();
      } else {
        MyToast.show(responseData.msg ?? "绑定失败");
      }
    } else {
      MyToast.show("请输入${platformName}账号");
    }
  }
}
