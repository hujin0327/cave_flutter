import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MineContactPage extends StatelessWidget {
  const MineContactPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: kAppWhiteColor,
        appBar: WhlAppBar(
          centerTitle: "联系我们",
          backgroundColor: kAppWhiteColor,
        ),
        body: UIColumn(
          width: double.infinity,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            UIImage(
              margin: EdgeInsets.only(top: 60.h),
              height: 70.w,
              width: 70.w,
              radius: 10.w,
              assetImage: "icon_app_logo",
            ),
            UIText(
              margin: EdgeInsets.only(top: 16.h),
              text: "Cave",
              fontSize: 19.sp,
              textAlign: TextAlign.center,
              textColor: kAppSubTextColor,
              fontWeight: FontWeight.bold,
            ),
            UIText(
              margin: EdgeInsets.only(top: 16.h),
              text: "联系电话：137xxxx0011",
              fontSize: 16.sp,
              textAlign: TextAlign.center,
              textColor: kAppSubTextColor,
              fontWeight: FontWeight.bold,
            ),
          ],
        ));
  }
}
