import 'package:cave_flutter/network/whl_api.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../model/attention_model.dart';
import '../../../utils/whl_toast_utils.dart';

class Attention_fansLogic extends GetxController with GetSingleTickerProviderStateMixin{

  List titleList = ["关注","粉丝","Cave"];
  late TabController tabController;

  List attentionList = [];
  List fansList = [];
  List<dynamic> caveList = [];

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    int index = Get.arguments??0;
    tabController = TabController(length: titleList.length, vsync: this);
    tabController.index = index;
    loadData();
    update();
  }

  loadData(){
    if (tabController.index == 0) {
      if (attentionList.isEmpty) {
        attentionListRequest();
      }
    }else if (tabController.index == 1) {
      if (fansList.isEmpty) {
        fansListRequest();
      }
    } else if (tabController.index == 2) {
      if (caveList.isEmpty) {
        caveListRequest();
      }
    }
  }

  attentionListRequest(){
    WhlApi.getFollowList.get({}).then((value){
      if (value.isSuccess()) {
        List records = value.data["records"];
        attentionList = records.map((e) => AttentionUserModel.fromJson(e)).toList();
        update();
      }
    });
  }

  fansListRequest(){
    WhlApi.getFansList.get({}).then((value){
      if (value.isSuccess()) {
        Map data = value.data??{};
        List records = data["records"];
        fansList = records.map((e) => AttentionUserModel.fromJson(e)).toList();
        update();
      }
    });
  }

  caveListRequest() async {
    ResponseData responseData = await WhlApi.myCave.get({});
    
    if (responseData.isSuccess()) {
      print('我的cave-------------');

      caveList = responseData.data;
      update();
    }
  
  }

  attentionRequest(AttentionUserModel model){
    '${WhlApi.userFollow}${model.accountId}'.get({},withLoading: true).then((value) {
      if (value.isSuccess()) {
        MyToast.show("关注成功");
        model.mutualFollow = true;
        update();
      }
    });
  }

  unAttentionRequest(AttentionUserModel model){
    '${WhlApi.userUnfollow}${model.accountId}'.get({},withLoading: true).then((value) {
      if (value.isSuccess()) {
        MyToast.show("已取消关注");
        if (tabController.index == 0) {
          attentionList.remove(model);
        }else {
          model.mutualFollow = false;
        }
        update();
      }
    });
  }
  
  List<Tab> getTabs(){
    List<Tab> tabs = [];
    for (var element in titleList) {
      tabs.add(Tab(text: element,));
    }
    return tabs;
  }
}
