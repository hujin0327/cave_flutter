import 'package:cave_flutter/config/whl_global_config.dart';
import 'package:cave_flutter/model/attention_model.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/empty_widget.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:cave_flutter/routes/whl_app_pages.dart';

import 'package:get/get.dart';

import 'attention_fans_logic.dart';

class Attention_fansPage extends StatelessWidget {
  Attention_fansPage({Key? key}) : super(key: key);

  final logic = Get.put(Attention_fansLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppWhiteColor,
      appBar: WhlAppBar(
        backgroundColor: kAppWhiteColor,
        centerWidget: TabBar(
          controller: logic.tabController,
          tabs: logic.getTabs(),
          labelColor: kAppBlackColor,
          labelStyle: TextStyle(
              fontSize: 16.sp,
              fontWeight: FontWeight.bold
          ),
          indicatorColor: Colors.transparent,
          unselectedLabelColor: kAppTwoTextColor,
          unselectedLabelStyle: TextStyle(
            fontSize: 16.sp,
          ),
          onTap: (index) {
            logic.loadData();
          },
        ),
      ),
      body: GetBuilder<Attention_fansLogic>(builder: (logic) {
        return TabBarView(controller: logic.tabController, children: [
          getAttentionFansListView(),
          getAttentionFansListView(isFans: true),
          getCaveListView()
        ],);
      }),
    );
  }

  Widget getAttentionFansListView({bool isFans = false}) {
    List data = isFans ? logic.fansList : logic.attentionList;
    if (data.isEmpty) {
      return MyEmptyWidget();
    }
    return ListView.builder(itemBuilder: (context, index) {
      return getAttentionItem(data[index], isFans: isFans);
    },
      itemCount: isFans ? logic.fansList.length : logic.attentionList.length,
    );
  }

  Widget getCaveListView() {

  String getImageBaseUrl = WHLGlobalConfig.getInstance().getImageBaseUrl();

    return ListView.builder(
      itemBuilder: (context, index) {
        return UIRow(
          padding: EdgeInsets.fromLTRB(18.w, 12.w, 18.w, 12.w),
          children: [
            UIImage(
              margin: EdgeInsets.only(right: 10.w),

              width: 54.w,
              height: 54.w,
              radius: 27.w,
              httpImage:  getImageBaseUrl + logic.caveList[index]['caveImg'],
              assetPlaceHolder: WHLGlobalConfig.userPlaceholderAvatar,
              clipBehavior: Clip.hardEdge,
              fit: BoxFit.cover,
              onTap: () => {
                Get.toNamed(Routes.caveDetailPage, 
                      arguments: {
                      'id': logic.caveList[index]['id'],

                  })

              },
            ),
            Expanded(

              child: UIColumn(
                onTap: () => {
                   Get.toNamed(Routes.caveGroupChat, 
                      arguments: {
                      'id': logic.caveList[index]['id'],
                      'tid': logic.caveList[index]['tid'],
                      'type': logic.caveList[index]['caveType'],
                      'caveName': logic.caveList[index]['caveName'],

                      'showName':'showName',
                  })
                },
                children: [
                  UIRow(
                    margin: EdgeInsets.only(bottom: 8.w),
                    children: [
                      UIText(
                        text: logic.caveList[index]['caveName'],
                        textColor: Colors.black,
                        fontSize: 16.sp,
                      ),
                      UIText(
                        text: logic.caveList[index]['caveType'] == '1' ? '官方' : '个人',
                        padding: EdgeInsets.symmetric(horizontal: 8.w, vertical: 0.w),
                        margin: EdgeInsets.only(left: 8.w, right: 10.w),
                        textColor: kAppColor("#C1C1C1"),
                        strokeWidth:1,
                        strokeColor: kAppColor("#C1C1C1"),
                        alignment: Alignment.center,
                        fontSize: 12.sp,
                        radius: 9.w,
                        color: kAppColor('#ffffff'),
                      )
                    ],
                  ),
                  UIRow(
                    width: double.infinity,
                    height: 20.w,
                    children: [
                      Expanded(child: UIText(
                        text: '里斯：有上海的朋友吗？加个好友...',
                        textColor: kAppColor("#666666"),
                        fontSize: 12.sp,
                      ),),
                      
                      UIText(
                        text: '12:20',
                        textColor: kAppColor("#C1C1C1"),
                        fontSize: 12.sp,
                      ),
                    ],
                  )
                ],
              ),
            )
          ],
        );
      },
      itemCount: logic.caveList.length,
    );
  }

  Widget getAttentionItem(AttentionUserModel model, {bool isFans = false}) {
    return UIRow(
      padding: EdgeInsets.symmetric(horizontal: 18.w, vertical: 12.h),
      children: [
        UIImage(
          width: 42.w,
          height: 42.w,
          radius: 21.w,
          httpImage: model.avatar?.toImageUrl(),
          assetPlaceHolder: WHLGlobalConfig.userPlaceholderAvatar,
          clipBehavior: Clip.hardEdge,
          fit: BoxFit.cover,
        ),
        Expanded(
          child: UIText(
            margin: EdgeInsets.only(left: 14.w),
            text: model.nickName,
            textColor: kAppBlackColor,
            fontSize: 14.sp,
          ),
        ),
        UIText(
          // padding: EdgeInsets.symmetric(horizontal: 14.w),
          margin: EdgeInsets.only(left: 10.w),
          text: model.mutualFollow == true ? "互相关注" : isFans
              ? "回关"
              :"已关注",
          alignment: Alignment.center,
          textColor: isFans ? kAppSubTextColor : kAppStrokeColor,
          height: 36.w,
          width: 80.w,
          radius: 18.w,
          strokeColor: kAppStrokeColor,
          strokeWidth: 1.w,
          onTap: (){
            if (isFans){
              if (model.mutualFollow != true) {
                logic.attentionRequest(model);
              }else {
                logic.unAttentionRequest(model);
              }
            }else {
              logic.unAttentionRequest(model);
            }

          },
        )
      ],
    );
  }
}
