import 'package:cave_flutter/config/whl_global_config.dart';
import 'package:cave_flutter/pages/whl_mine/task/model/task_model.dart';
import 'package:cave_flutter/pages/whl_mine/user_info/user_info_logic.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/widget/basic_widget.dart';
import 'package:cave_flutter/widgets/brick/widget/image_widget.dart';
import 'package:cave_flutter/widgets/brick/widget/text_widget.dart';
import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:cave_flutter/widgets/whl_switch_plus.dart';
import 'package:cave_flutter/widgets/whl_white_app_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';

import 'task_logic.dart';

class TaskPage extends StatelessWidget {
  TaskPage({Key? key}) : super(key: key);

  final userLogic = Get.put(UserInfoLogic());
  final taskLogic = Get.put(TaskLogic());
  @override
  Widget build(BuildContext context) {
    // /app/user-sign/getSignInfo
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
            child: UIContainer(
              width: ScreenUtil().screenWidth,
              height: 228.h,
              decoration: BoxDecoration(
                color: kAppColor("#1B1B1B"),
                borderRadius: BorderRadius.circular(30.r),
              ),
              // ),
            ),
          ),
          GetBuilder<TaskLogic>(builder: (logic) {
            return UIColumn(
              padding: EdgeInsets.symmetric(horizontal: 18.w),
              children: [
                const WhlWhiteAppBar(
                  '每日任务',
                ),
                Expanded(
                  child: ListView(
                    padding: EdgeInsets.only(bottom: 18.h),
                    children: [
                      UIRow(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          UIImage(
                            margin: EdgeInsets.only(right: 10.w),
                            httpImage:
                                logic.userLevelModel.avatar?.toImageUrl(),
                            assetPlaceHolder:
                                WHLGlobalConfig.userPlaceholderAvatar,
                            width: 67.w,
                            height: 67.w,
                            radius: 34.w,
                            strokeColor: Colors.white,
                            strokeWidth: 2.w,
                          ),
                          Expanded(
                            child: UIColumn(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                UIText(
                                  text: logic.userLevelModel.nickName,
                                  textColor: Colors.white,
                                  fontSize: 18.sp,
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 4),
                                  child: LinearProgressIndicator(
                                    minHeight: 6.0.w,
                                    backgroundColor:
                                        Colors.white.withOpacity(0),
                                    value: (taskLogic
                                            .userLevelModel.experience!) /
                                        (taskLogic.userLevelModel.upEperience!),
                                    borderRadius: BorderRadius.circular(8),
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                      kAppColor('#FFF754'),
                                    ),
                                  ),
                                ),
                                UIText(
                                  text:
                                      '距离升级还差${((taskLogic.userLevelModel.upEperience ?? 0) - (taskLogic.userLevelModel.experience ?? 0))}点',
                                  fontSize: 14.sp,
                                  textColor: kAppColorOpacity('#FFFFFF', 0.7),
                                )
                              ],
                            ),
                          ),
                          // UIImage(
                          //   margin: EdgeInsets.only(left: 4.w),
                          //   // assetImage: 'icon_svip.png',
                          //   httpImage:
                          //       userLogic.userModel?.levelImage?.toImageUrl(),

                          //   width: 18.w,
                          //   height: 18.w,
                          // )
                          UIText(
                            padding: EdgeInsets.only(left: 40.w),
                            fontSize: 18.sp,
                            text: 'LV.${taskLogic.userLevelModel.level}',
                            textColor: Colors.white,
                          ),
                        ],
                      ),
                      UIContainer(
                        margin: EdgeInsets.only(top: 30.h),
                        padding: EdgeInsets.only(
                          top: 16.h,
                          left: 16.w,
                          right: 16.w,
                          bottom: 16.h,
                        ),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8.r),
                        ),
                        width: ScreenUtil().screenWidth,
                        child: UIColumn(children: [
                          UIRow(
                            children: [
                              Expanded(
                                child: UIText(
                                  text:
                                      '已经连续签到${logic.signModel.signDay ?? 0}天',
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16.sp,
                                ),
                              ),
                              UIText(
                                text: '签到提醒',
                                textColor: kAppColor('#9F9F9F'),
                                fontSize: 14.sp,
                                margin: EdgeInsets.only(right: 12.w),
                              ),
                              WhlSwitch(
                                key: Key(
                                    "signRemind_${logic.signModel.signRemind}"),
                                value: logic.signModel.signRemind == true,
                                width: 50.w,
                                height: 26.w,
                                onChanged: (bool value) =>
                                    {logic.saveDate(value)},
                                openColor: Colors.white,
                                bgColor: kAppColor('#EDEDED'),
                                color: Colors.white,
                                openBgColor: kAppThemeColor,
                              )
                            ],
                          ),
                          UIWrap(
                            margin: EdgeInsets.only(top: 20.h),
                            runSpacing: 10.h,
                            children: [
                              UIRow(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: logic.signList1.map((e) {
                                  return Expanded(
                                    child: signBox(context, e),
                                  );
                                }).toList(),
                              ),
                              UIRow(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  ...logic.signList2.map((e) {
                                    return Expanded(
                                      child: signBox(context, e),
                                    );
                                  }).toList(),
                                  Expanded(
                                    flex: 2,
                                    child: UIContainer(
                                      child: Column(
                                        children: [
                                          UIContainer(
                                            height: 22.h,
                                            width: double.infinity,
                                            alignment: Alignment.center,
                                            color: kAppColorOpacity(
                                              '#FFFCC3',
                                              0.8,
                                            ),
                                            child: UIText(
                                              text: '开启加速升级模式',
                                              fontSize: 12.sp,
                                              textColor: kAppColor('#3D3D3D'),
                                            ),
                                          ),
                                          UIContainer(
                                            height: 40,
                                            color: kAppColor('#F0F0F0'),
                                            child: UIImage(
                                              padding: EdgeInsets.symmetric(
                                                  vertical: 4.h),
                                              height: 32.h,
                                              fit: BoxFit.fitHeight,
                                              width: double.infinity,
                                              assetImage: 'task_sign_gift.png',
                                            ),
                                          ),
                                          UIContainer(
                                            margin:
                                                const EdgeInsets.only(top: 2),
                                            alignment: Alignment.center,
                                            height: 20.h,
                                            width: double.infinity,
                                            color: kAppColor('#F5F7F9'),
                                            child: UIText(
                                              text: '经验+300%',
                                              fontSize: 12.sp,
                                              textColor: kAppColor('#E14541'),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ]),
                      ),
                      UIContainer(
                        child: UIColumn(
                          children: [
                            taskList(context, '每日任务', logic.dailyTasks),
                            taskList(context, '每日随机任务', logic.randomTasks),
                            taskList(context, '固定任务', logic.fixedTasks)
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            );
          })
        ],
      ),
    );
  }

  Widget signBox(BuildContext context, SignDto signDto) {
    return UIContainer(
      onTap: () {
        if (signDto.todaySign && signDto.signDay == signDto.day ||
            !signDto.todaySign && signDto.signDay < signDto.day) {
          taskLogic.saveSign(signDto);
        }
      },
      margin: EdgeInsets.only(right: 10.w),
      child: Column(
        children: [
          UIContainer(
            height: 22.h,
            width: double.infinity,
            alignment: Alignment.center,
            color: kAppColorOpacity(
              '#FFFCC3',
              0.8,
            ),
            child: UIText(
              text: '第${signDto.day}天',
              fontSize: 12.sp,
              textColor: kAppColor('#3D3D3D'),
            ),
          ),
          UIContainer(
            color: signDto.signed ? kAppColor('#FFE800') : kAppColor('#F0F0F0'),
            child: UIImage(
              height: 40.h,
              width: double.infinity,
              assetImage: 'task_sign_exp.png',
            ),
          ),
          UIContainer(
            margin: const EdgeInsets.only(top: 2),
            alignment: Alignment.center,
            height: 20.h,
            width: double.infinity,
            color: kAppColor('#F5F7F9'),
            child: UIText(
              text: '经验+${signDto.experience}',
              textColor: kAppColor('#909193'),
              fontSize: 12.sp,
            ),
          )
        ],
      ),
    );
  }

  Widget taskList(BuildContext context, String title, List<TaskModel> tasks) {
    return UIContainer(
      margin: EdgeInsets.only(top: 12.h),
      padding: EdgeInsets.symmetric(
        horizontal: 16.w,
      ).copyWith(
        top: 18.h,
        bottom: 10.h,
      ),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8.r),
      ),
      child: UIColumn(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          UIText(
            text: title,
            fontSize: 16.sp,
            fontWeight: FontWeight.bold,
            margin: EdgeInsets.only(bottom: 18.h),
          ),
          if (tasks.isEmpty)
            UIText(
              text: '今日暂无任务哦',
              textColor: kAppColor('#979797'),
            )
          else
            ListView.separated(
              shrinkWrap: true,
              physics: ClampingScrollPhysics(),
              itemBuilder: (context, int index) {
                TaskModel task = tasks[index];
                return UIColumn(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    UIRow(
                      children: [
                        Expanded(
                          child: RichText(
                            text: TextSpan(
                                text: task.taskName,
                                style: TextStyle(
                                    color: kAppBlackColor, fontSize: 12.sp),
                                children: [
                                  TextSpan(
                                    text: '　+${task.rewardExperience}经验',
                                    style: TextStyle(
                                      color: kAppColor('#E14541'),
                                    ),
                                  )
                                ]),
                          ),
                        ),
                        task.schedule == 1
                            ? UIContainer(
                                width: 64.w,
                                height: 26.h,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8.r),
                                  color: kAppColor('#E8E8E8'),
                                ),
                                child: UIText(
                                  text: '已完成',
                                  textColor: kAppColor('#9F9F9F'),
                                ),
                              )
                            : UIContainer(
                                onTap: () {
                                  if (task.router != null &&
                                      task.router!.isNotEmpty) {
                                    Get.toNamed(task.router!);
                                  }
                                },
                                width: 64.w,
                                height: 26.h,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8.r),
                                  color: kAppColor('#FFE800'),
                                ),
                                child: UIText(
                                  text: '去完成',
                                ),
                              )
                      ],
                    ),
                    // UIText(
                    //   text: '前往社区点赞任意动态',
                    //   textColor: kAppColor('#979797'),
                    //   fontSize: 10.sp,
                    // ),
                  ],
                );
              },
              separatorBuilder: (context, int index) {
                return const Divider();
              },
              itemCount: tasks.length,
            )
        ],
      ),
    );
  }
}
