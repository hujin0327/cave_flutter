import 'package:cave_flutter/model/base_source_model.dart';
import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/pages/whl_mine/privacy_setting/privacy_setting_logic.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

import 'model/sign_model.dart';
import 'model/task_model.dart';

class SignDto {
  bool todaySign;
  //  已签到天数
  int signDay;
  //  当前第几天
  int day;
  //  是否已签到
  bool signed;
  //  当天经验值
  int experience;
  SignDto({
    required this.day,
    required this.todaySign,
    required this.signDay,
    required this.signed,
    required this.experience,
  });
}

class TaskLogic extends GetxController {
  List<TaskModel> dailyTasks = [];
  List<TaskModel> randomTasks = [];
  List<TaskModel> fixedTasks = [];
  SignModel signModel = SignModel();

  List<SignDto> signList1 = List<SignDto>.from([]);
  List<SignDto> signList2 = List<SignDto>.from([]);

  UserLevelModel userLevelModel = UserLevelModel(
    experience: 0,
    upEperience: 100,
    level: 0,
  );
  @override
  void onInit() {
    super.onInit();
    signList1 = [
      SignDto(
        day: 1,
        experience: 10,
        signDay: 0,
        signed: false,
        todaySign: false,
      ),
      SignDto(
        day: 2,
        experience: 20,
        signDay: 0,
        signed: false,
        todaySign: false,
      ),
      SignDto(
        day: 3,
        experience: 30,
        signDay: 0,
        signed: false,
        todaySign: false,
      ),
      SignDto(
        day: 4,
        experience: 40,
        signDay: 0,
        signed: false,
        todaySign: false,
      )
    ];
    signList2 = [
      SignDto(
        day: 5,
        experience: 50,
        signDay: 0,
        signed: false,
        todaySign: false,
      ),
      SignDto(
        day: 6,
        experience: 60,
        signDay: 0,
        signed: false,
        todaySign: false,
      )
    ];
    getSignInfo();
    getUserLevel();
    getTaskList();
  }

  void getUserLevel() {
    WhlApi.userLevel.get({}).then((value) {
      if (value.isSuccess()) {
        userLevelModel = UserLevelModel.fromJson(value.data);
        update();
      }
    });
  }

  void saveDate(bool isOpen) {
    WhlApi.saveMyPrivacy.post({
      "configKey": 'sign_remind',
      // ignore: unrelated_type_equality_checks
      "isOpen": isOpen ? 1 : 0,
      "privacyType": 3
    }).then((value) {
      if (value.isSuccess()) {
        signModel.signRemind = isOpen;
      } else {
        MyToast.show(value.msg ?? "编辑失败");
      }
      update();
    });
  }

  /// 获取任务列表
  getTaskList() async {
    ResponseData responseData = await WhlApi.getTask.get({});
    if (responseData.isSuccess()) {
      dailyTasks = List<TaskModel>.from(responseData.data['dailyTasks']
          .map((c) => TaskModel.fromJson(c))
          .toList());
      randomTasks = List<TaskModel>.from(responseData.data['randomTasks']
          .map((c) => TaskModel.fromJson(c))
          .toList());
      fixedTasks = List<TaskModel>.from(responseData.data['fixedTasks']
          .map((c) => TaskModel.fromJson(c))
          .toList());
      update();
    }
  }

  Future<bool> saveSign(SignDto signDto) async {
    ResponseData responseData = await WhlApi.saveSign.post({});
    if (responseData.isSuccess()) {
      signDto.signed = true;
      signDto.todaySign = true;
      signModel.signDay = signModel.signDay! + 1;
      update();
      //  签到成功，重新获取签到信息
      getSignInfo();
      getUserLevel();
      MyToast.show('签到成功');
      return Future.value(true);
    } else {
      if (responseData.msg != null) {
        MyToast.show(responseData.msg!);
      }
    }

    return Future.value(false);
  }

  getSignInfo() async {
    ResponseData responseData = await WhlApi.getSignInfo.post({});
    if (responseData.isSuccess()) {
      signModel = SignModel.fromJson(responseData.data);
      int signDay = signModel.signDay ?? 0;
      int m = signDay ~/ 6;
      signList1.clear();
      signList2.clear();
      for (int i = 1; i < 7; i++) {
        int day = (i * m + i);
        if (signList1.length < 4) {
          signList1.add(
            SignDto(
              todaySign: signModel.todaySign ?? false,
              signDay: signDay,
              day: day,
              signed: day <= signDay,
              experience: getExperience(i, signModel) ?? 0,
            ),
          );
        } else {
          signList2.add(
            SignDto(
              todaySign: signModel.todaySign ?? false,
              signDay: signDay,
              day: day,
              signed: day <= signDay,
              experience: getExperience(i, signModel) ?? 0,
            ),
          );
        }
      }
      print(signModel);
      update();
    }
  }

  int? getExperience(int day, SignModel signModel) {
    switch (day) {
      case 1:
        return signModel.oneExperience;
      case 2:
        return signModel.twoExperience;
      case 3:
        return signModel.threeExperience;
      case 4:
        return signModel.fourExperience;
      case 5:
        return signModel.fiveExperience;
      case 6:
        return signModel.sixExperience;
      default:
        return signModel.sevenAfterExperience;
    }
  }
}

class UserLevelModel {
  int? userId;
  String? avatar;
  String? accountId;
  String? nickName;
  int? level;
  int? experience;
  int? upEperience;

  UserLevelModel(
      {this.userId,
      this.avatar,
      this.accountId,
      this.nickName,
      this.level,
      this.experience,
      this.upEperience});

  UserLevelModel.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    avatar = json['avatar'];
    accountId = json['accountId'];
    nickName = json['nickName'];
    level = json['level'];
    experience = json['experience'];
    upEperience = json['upEperience'];
  }
}
