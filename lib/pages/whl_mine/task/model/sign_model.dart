class SignModel {
  int? oneExperience;
  int? twoExperience;
  int? threeExperience;
  int? fourExperience;
  int? fiveExperience;
  int? sixExperience;
  int? evenExperience;
  int? sevenAfterExperience;
  int? signDay;
  bool? todaySign;
  bool? signRemind;

  SignModel({
    this.signRemind,
    this.oneExperience,
    this.twoExperience,
    this.threeExperience,
    this.fourExperience,
    this.fiveExperience,
    this.sixExperience,
    this.evenExperience,
    this.sevenAfterExperience,
    this.signDay,
    this.todaySign,
  });

  factory SignModel.fromJson(Map<String, dynamic> json) => SignModel(
      oneExperience: json['oneExperience'],
      twoExperience: json['twoExperience'],
      threeExperience: json['threeExperience'],
      fourExperience: json['fourExperience'],
      fiveExperience: json['fiveExperience'],
      sixExperience: json['sixExperience'],
      evenExperience: json['evenExperience'],
      sevenAfterExperience: json['sevenAfterExperience'],
      signDay: json['signDay'],
      todaySign: json['todaySign'],
      signRemind: json['signRemind']);

  Map<String, dynamic> toJson() => {
        'signRemind': signRemind,
        "oneExperience": oneExperience,
        "twoExperience": twoExperience,
        "threeExperience": threeExperience,
        "fourExperience": fourExperience,
        "fiveExperience": fiveExperience,
        "sixExperience": sixExperience,
        "evenExperience": evenExperience,
        "sevenAfterExperience": sevenAfterExperience,
        "signDay": signDay,
        "todaySign": todaySign,
      };
}
