class TaskModel {
  int? taskId;
  int? userId;
  String? taskName;
  int? type;
  int? schedule;
  int? rewardExperience;
  String? router;
  var rewardVip;
  var rewardCoin;

  TaskModel({
    this.taskId,
    this.userId,
    this.taskName,
    this.type,
    this.schedule,
    this.rewardExperience,
    this.router,
    this.rewardVip,
    this.rewardCoin,
  });

  factory TaskModel.fromJson(Map<String, dynamic> json) => TaskModel(
        taskId: json['taskId'],
        userId: json['userId'],
        taskName: json['taskName'],
        type: json['type'],
        schedule: json['schedule'],
        rewardExperience: json['rewardExperience'],
        rewardVip: json['rewardVip'],
        rewardCoin: json['rewardCoin'],
        router: json['router'],
      );

  Map<String, dynamic> toJson() => {
        "taskId": taskId,
        "userId": userId,
        "taskName": taskName,
        "type": type,
        "schedule": schedule,
        "rewardExperience": rewardExperience,
        "rewardVip": rewardVip,
        "rewardCoin": rewardCoin,
        "router": router,
      };
}
