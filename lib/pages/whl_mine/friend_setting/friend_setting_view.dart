import 'package:cave_flutter/pages/whl_mine/friend_setting/friend_setting_logic.dart';
import 'package:cave_flutter/pages/whl_mine/privacy_setting/privacy_setting_logic.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/widget/basic_widget.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:cave_flutter/widgets/whl_row_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class FriendSettingPage extends StatelessWidget {
  FriendSettingPage({super.key});
  final logic = Get.put(FriendSettingLogic());
  // ScreenUtil().statusBarHeight
  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = EdgeInsets.symmetric(horizontal: 16.w, vertical: 13.w);
    return Scaffold(
      backgroundColor: kAppColor('#FAFAFA'),
      appBar: WhlAppBar(
        centerTitle: "好友申请设置",
        backgroundColor: kAppWhiteColor,
      ),
      body: GetBuilder<FriendSettingLogic>(builder: (logic) {
        List<Widget> friendList = List<Widget>.from(([]));
        logic.friendList.forEach((e) {
          friendList.add(MyRowItem(
            decoration: BoxDecoration(
              color: kAppWhiteColor,
              border: Border(
                bottom: BorderSide(color: kAppColor('#E5E5E5'), width: 0.5.w),
              ),
            ),
            padding: padding,
            title: e.configName,
            isShowSVip: e.condition == 2,
            isShowSwitch: true,
            isSwitchOn: e.isOpen == 1,
            onChanged: () {
              logic.saveDate(e);
            },
          ));
          // if (e.spacer == true) {
          //   friendList.add(UIContainer(
          //     height: 10.w,
          //   ));
          // }
        });
        return SingleChildScrollView(
          child: UIColumn(
            crossAxisAlignment: CrossAxisAlignment.start,
            padding: EdgeInsets.only(
              top: 0.w,
              bottom: 0.w,
            ),
            children: friendList,
          ),
        );
      }),
    );
  }
}
