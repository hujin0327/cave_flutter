import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/pages/whl_mine/privacy_setting/privacy_setting_logic.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:get/get.dart';

class FriendSettingLogic extends GetxController {
  List<PrivacySettingModel> friendList = [];

  @override
  void onInit() {
    super.onInit();
    friendList = [
      // FriendSettingModel(configName: '无需验证直接通过'),
      // FriendSettingModel(configName: '关注后可通过', condition: 2, spacer: true),
      // // FriendSettingModel(configName: '需要验证才可通过'),
      // FriendSettingModel(configName: '需设置礼物才可通过', condition: 2, spacer: true),
      // FriendSettingModel(configName: '只接受实名认证用户的申请（需实名）', spacer: true),
      // FriendSettingModel(configName: '拒绝任何人添加好友', condition: 2),
      // FriendSettingModel(configName: '仅接受主流群体的好友申请'),
    ];
    loadData();
  }

  Future<void> loadData() async {
    ResponseData value = await WhlApi.myPrivacy.get({'type': 2});
    if (value.isSuccess()) {
      if (value.isSuccess()) {
        List data = value.data;
        List<PrivacySettingModel> allModels = [];
        for (var element in data) {
          allModels.add(PrivacySettingModel.fromJson(element));
        }
        friendList = allModels;
      } else {
        if ((value.msg ?? "").isNotEmpty) {
          MyToast.show(value.msg!);
        }
      }
      update();
    }
  }

  void saveDate(PrivacySettingModel item) {
    WhlApi.saveMyPrivacy.post({
      "configKey": item.configKey,
      "isOpen": item.isOpen == 0 ? 1 : 0,
      "privacyType": item.privacyType
    }).then((value) {
      if (value.isSuccess()) {
        item.isOpen = item.isOpen == 0 ? 1 : 0;
      } else {
        MyToast.show("编辑失败");
      }
      update();
    });
  }
}
