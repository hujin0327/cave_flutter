import 'package:cave_flutter/model/whl_coin_goods_model.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:cave_flutter/widgets/whl_white_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../whl_mine_logic.dart';
import 'whl_coin_shop_logic.dart';

class WhlCoinShopPage extends StatelessWidget {
  WhlCoinShopPage({Key? key}) : super(key: key);

  final logic = Get.put(WhlCoinShopLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppColor('#F6F6F6'),
      body: Stack(
        children: [
          const UIImage(
            assetImage: 'img_bg_coin_shop.png',
          ),
          WhlWhiteAppBar(
            'Coin Shop',
            actions: [
              UIImage(
                padding: EdgeInsets.only(right: 20.w),
                assetImage: 'icon_coin_shop_service.png',
                width: 44.w,
                onTap: () {
                  logic.doClickService();
                },
              )
            ],
          ),
          UIColumn(
            margin: EdgeInsets.only(top: ScreenUtil().statusBarHeight + 65.h),
            children: [
              UIStack(
                margin: EdgeInsets.only(left: 45.w, right: 45.w, bottom: 10.h),
                children: [
                  UIImage(
                    assetImage: 'img_bg_coin_shop_top.png',
                    width: double.maxFinite,
                    height: 122.h,
                  ),
                  UIColumn(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    padding: EdgeInsets.symmetric(horizontal: 25.w, vertical: 20.h),
                    children: [
                      GetBuilder<WhlMineLogic>(builder: (logic) {
                        return UIText(
                          // text: logic.userInfoModel.availableCoins.toString(),
                          textColor: kAppColor('#A96E15'),
                          fontSize: 32.sp,
                          fontWeight: FontWeight.bold,
                        );
                      }),
                      UIText(
                        margin: EdgeInsets.only(top: 25.h),
                        text: 'My Coins',
                        textColor: kAppColor('#A96E15'),
                        fontSize: 20.sp,
                        fontWeight: FontWeight.w500,
                      )
                    ],
                  )
                ],
              ),
              Expanded(
                child: CustomScrollView(
                  slivers: [
                    GetBuilder<WhlCoinShopLogic>(builder: (logic) {
                      return SliverList(
                          delegate: SliverChildBuilderDelegate((context, index) {
                        return buildListItem(logic.goodsList[index]);
                      }, childCount: logic.goodsList.length));
                    })
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  buildListItem(WhlCoinGoodsModel model) {
    return UIStack(
      alignment: Alignment.topRight,
      children: [
        IntrinsicHeight(
          child: UIRow(
            margin: EdgeInsets.only(left: 20.w, right: 20.w, bottom: 12.h),
            padding: EdgeInsets.symmetric(vertical: 14.h, horizontal: 16.w),
            color: Colors.white,
            radius: 8.w,
            strokeWidth: 0.5.w,
            strokeColor: kAppColor('#CCCCCC'),
            children: [
              UIImage(
                margin: EdgeInsets.only(right: 8.w),
                assetImage: 'icon_coin.png',
                width: 40.w,
                height: 40.w,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  UIText(
                    text: model.exchangeCoin.toString(),
                    textColor: kAppColor('#523E0B'),
                    fontSize: 14.sp,
                    fontWeight: FontWeight.bold,
                  ),
                  if ((model.discount ?? 0) > 0)
                    UIText(
                      text: '${(model.discount ?? 0) * 100}% off',
                      textColor: kAppColor('#FFC657'),
                      fontSize: 9.sp,
                    )
                ],
              ),
              Expanded(child: Container()),
              UIText(
                text: '\$${model.price?.toStringAsFixed(2) ?? ''}',
                textColor: kAppTextColor,
                fontSize: 18.sp,
                fontWeight: FontWeight.bold,
              ),
              if (model.price != model.originalPrice)
                UIText(
                  margin: EdgeInsets.only(left: 8.w),
                  text: '\$${model.originalPrice?.toStringAsFixed(2)}',
                  textColor: kAppSub2TextColor,
                  fontSize: 10.sp,
                  textDecoration: TextDecoration.lineThrough,
                )
            ],
          ),
        ),
        if (model.tags?.isNotEmpty ?? false)
          if (model.tags == 'Hot')
            UIImage(
              margin: EdgeInsets.only(right: 20.w),
              assetImage: 'icon_shop_hot.png',
              height: 20.h,
            )
          else
            UIText(
              margin: EdgeInsets.only(right: 20.w),
              padding: EdgeInsets.symmetric(horizontal: 12.w, vertical: 3.h),
              bottomLeftRadius: 8.w,
              topRightRadius: 8.w,
              text: model.tags ?? '',
              textColor: Colors.white,
              fontSize: 12.sp,
              gradientStartColor: kAppLeftColor,
              gradientEndColor: kAppRightColor,
            )
      ],
      onTap: () {
        logic.doClickItem(model);
      },
    );
  }
}
