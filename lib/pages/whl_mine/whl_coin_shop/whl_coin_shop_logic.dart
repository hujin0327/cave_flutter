import 'package:cave_flutter/config/whl_base_controller.dart';
import 'package:cave_flutter/model/whl_coin_goods_model.dart';
import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/pages/whl_mine/whl_mine_logic.dart';
import 'package:cave_flutter/utils/whl_inapp_purchase_util.dart';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter_inapp_purchase/flutter_inapp_purchase.dart';
import 'package:get/get.dart';

class WhlCoinShopLogic extends WhlBaseController {
  List<WhlCoinGoodsModel> goodsList = [];

  @override
  void onInit() {
    super.onInit();
    onGetCoinShopList();
  }

  onGetCoinShopList() async {
    Map<String, dynamic> params = {'isIncludeSubscription': false, 'payChannel': 'IAP'};
    // ResponseData responseData = await WhlApi.coinShopList.post(params);
    // if (responseData.isSuccess()) {
    //   print('responseData.data = ${responseData.data}');
    //   List list = responseData.data;
    //   goodsList = list.map((e) => WhlCoinGoodsModel.fromJson(e)).toList();
    //   update();
    // }
  }

  doClickItem(WhlCoinGoodsModel model) async {
    BotToast.showLoading();
    Map<String, dynamic> params = {
      'entry': '',
      'source': '',
      'goodsCode': model.code,
      'payChannel': 'IAP',
    };
    // ResponseData responseData = await WhlApi.createOrder.post(params);
    // if (responseData.isSuccess()) {
    //   print('responseData.data = ${responseData.data}');
    //   WhlInAppPurchaseModel purchaseModel = WhlInAppPurchaseModel(
    //     productId: model.code,
    //     orderNo: responseData.data['orderNo'],
    //     tradeNo: responseData.data['tradeNo'],
    //     successBlock: () {
    //       WhlMineLogic logic = Get.find();
    //       logic.onGetUserInfo();
    //     },
    //   );
    //   WhlInAppPurchaseUtil().getProduct(purchaseModel);
      // WhlInAppPurchaseUtil.checkOrderForService(null);
    // } else {
    //   BotToast.closeAllLoading();
    // }
  }
}
