import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/utils/task_utils.dart';
import 'package:get/get.dart';

import '../../../model/gift_model.dart';

class Gift_wallLogic extends GetxController {
  List listData = [];
  int hasCount = 0;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    TaskUtils.complateTask(15, type: 2);

    getGiftListRequest();
  }

  getGiftListRequest() {
    WhlApi.myGiftList.get({}).then((value) {
      if (value.isSuccess()) {
        List data = value.data;
        listData = data.map((e) => GiftModel.fromJson(e)).toList();
        checkHasCount();
        update();
      }
    });
  }

  checkHasCount() {
    hasCount = 0;
    for (var element in listData) {
      if (element.isReceive == true) {
        hasCount++;
      }
    }
  }
}
