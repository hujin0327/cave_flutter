import 'package:cave_flutter/config/whl_global_config.dart';
import 'package:cave_flutter/model/gift_model.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:cave_flutter/utils/whl_user_utils.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'gift_wall_logic.dart';

class Gift_wallPage extends StatelessWidget {
  Gift_wallPage({Key? key}) : super(key: key);

  final logic = Get.put(Gift_wallLogic());

  @override
  Widget build(BuildContext context) {
    int rowItemCount = 4;
    double itemWidth =
        (ScreenUtil().screenWidth - 5.w * (rowItemCount - 1) - 20.w) /
            rowItemCount;
    return Scaffold(
      backgroundColor: kAppBlackColor,
      // appBar: WhlAppBar(
      //   centerTitle: "礼物墙",
      // ),
      body: UIColumn(
        children: [
          UIRow(
            margin: EdgeInsets.only(
                top: ScreenUtil().statusBarHeight + 10.w,
                left: 10.w,
                right: 10.w),
            children: [
              UIImage(
                padding: EdgeInsets.all(5.w),
                assetImage: "icon_back_black",
                imageColor: kAppWhiteColor,
                width: 30.w,
                height: 30.w,
                onTap: () {
                  Get.back();
                },
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(right: 30.w),
                  child: Center(
                    child: Text("礼物墙",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 16.sp,
                            color: Colors.white,
                            fontWeight: FontWeight.bold)),
                  ),
                ),
              )
            ],
          ),
          UIImage(
            margin: EdgeInsets.only(top: 16.h, bottom: 3.h),
            httpImage: WhlUserUtils.getAvatar().toImageUrl(),
            assetPlaceHolder: WHLGlobalConfig.userPlaceholderAvatar,
            width: 64.w,
            height: 64.w,
            radius: 32.w,
            fit: BoxFit.cover,
            clipBehavior: Clip.hardEdge,
          ),
          UIText(
            text: WhlUserUtils.getNickName(),
            textColor: kAppWhiteColor,
            fontSize: 15.sp,
          ),
          GetBuilder<Gift_wallLogic>(builder: (logic) {
            return UIContainer(
              margin: EdgeInsets.only(top: 37.h),
              child: Text.rich(TextSpan(
                  text: "已点亮",
                  style:
                      TextStyle(color: kAppColor("#F7D08D"), fontSize: 12.sp),
                  children: [
                    TextSpan(
                        text: "  ${logic.hasCount}",
                        style: TextStyle(
                            color: kAppColor("#F7D08D"), fontSize: 12.sp)),
                    TextSpan(
                        text: "/${logic.listData.length}",
                        style: TextStyle(
                            color: kAppWhiteColor.withOpacity(0.66),
                            fontSize: 12.sp)),
                  ])),
            );
          }),
          GetBuilder<Gift_wallLogic>(builder: (logic) {
            return Expanded(
                child: Container(
              margin: EdgeInsets.symmetric(horizontal: 10.w),
              child: GridView.builder(
                shrinkWrap: true,
                // physics: const NeverScrollableScrollPhysics(),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 4,
                  mainAxisSpacing: 5.w,
                  crossAxisSpacing: 5.w,
                  childAspectRatio: itemWidth / (itemWidth + 20),
                ),
                itemBuilder: (context, index) {
                  GiftModel model = logic.listData[index];
                  return getGiftItem(model);
                },
                itemCount: logic.listData.length,
              ),
            ));
          }),
        ],
      ),
    );
  }

  getGiftItem(GiftModel model) {
    return Stack(
      alignment: Alignment.topCenter,
      children: [
        if ((model.count ?? 0) > 0)
          Positioned(
              right: 0,
              top: 0,
              height: 10,
              child: UIText(
                padding: EdgeInsets.symmetric(horizontal: 4.w),
                alignment: Alignment.center,
                color: kAppColor("#F7D08D"),
                textColor: kAppSubTextColor,
                height: 10.w,
                radius: 5.w,
                text: "${model.count}",
                fontSize: 9.sp,
              )),
        UIColumn(
          children: [
            UIImage(
              padding: EdgeInsets.only(
                  top: 15.w, left: 10.w, right: 10.w, bottom: 5.w),
              // assetImage: "icon_rank_1",
              assetPlaceHolder: WHLGlobalConfig.userPlaceholderAvatar,
              httpImage: model.imageUrl?.toImageUrl(),
              aspectRatio: 1.0,
              // imageColor: model.isReceive == true?kAppColor("#F7D08D"):kAppWhiteColor.withOpacity(0.66),
            ),
            UIText(
              text: model.giftName,
              textColor: model.isReceive == true
                  ? kAppColor("#F7D08D")
                  : kAppWhiteColor.withOpacity(0.66),
              fontSize: 12.sp,
            )
          ],
        )
      ],
    );
  }
}
