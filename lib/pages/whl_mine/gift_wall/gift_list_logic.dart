import 'package:cave_flutter/model/gift_model.dart';
import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/pages/whl_mine/whl_wallet/model/whl_wallet_model.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

class GiftListLogic extends GetxController {
  List<GiftModel> listData = [];
  late String accountId;
  GiftModel? selectedGift;
  WhlWalletModel? walletModel;
  GiftListLogic(this.accountId);
  @override
  void onInit() {
    super.onInit();
    walletModel = WhlWalletModel();

    getGiftList();
    getWithdrawAmount();
  }

  //  获取可提现金额
  getWithdrawAmount() async {
    ResponseData response = await WhlApi.withdrawAmount.get({});
    if (response.isSuccess() && response.data != null) {
      WhlWalletModel data = WhlWalletModel.fromJson(response.data);
      walletModel?.balance = data.balance;
      walletModel?.unCanWithDrawCoin = data.unCanWithDrawCoin;
      walletModel?.withdrawal = data.withdrawal;
      walletModel?.withdrawalAmount = data.withdrawalAmount;
      walletModel?.walletId = data.walletId;
      update();
    }
  }

  getGiftList() {
    //  这个接口应该要换成礼物列表
    WhlApi.myGiftList.get({}).then((value) {
      if (value.isSuccess()) {
        listData = List<GiftModel>.from(
            value.data['list'].map((e) => GiftModel.fromJson(e))).toList();
      }
    });
  }

  selectGift(GiftModel model) {
    selectedGift = model;
    update();
  }

  giveGift() {
    if (selectedGift != null) {
      if ((walletModel?.balance ?? 0) >= selectedGift!.giftPrice!) {
        WhlApi.giveGift.post({
          'count': 1,
          'giftId': selectedGift?.id,
          'userId': accountId
        }).then((value) {
          MyToast.show('赠送成功');
          getWithdrawAmount();
        });
      } else {
        MyToast.show('可用余额不足');
      }
    } else {
      MyToast.show('请选择要赠送的礼物');
    }
  }
}
