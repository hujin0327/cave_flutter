import 'package:cave_flutter/model/gift_model.dart';
import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/pages/whl_mine/gift_wall/gift_list_logic.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

/// 赠送礼物列表
class GiftListPage extends StatefulWidget {
  final String accountId;
  const GiftListPage({super.key, required this.accountId});

  @override
  State<GiftListPage> createState() => _GiftListPageState();
}

class _GiftListPageState extends State<GiftListPage> {
  late GiftListLogic logic;
  @override
  void initState() {
    logic = Get.put(GiftListLogic(widget.accountId));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return UIContainer(
      height: 340.h,
      decoration: BoxDecoration(
        color: kAppWhiteColor,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(30.r),
          topLeft: Radius.circular(30.r),
        ),
        gradient: LinearGradient(
          colors: [kAppColor('#FFFCE1'), Colors.white, Colors.white],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      child: UIColumn(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          UIRow(
            children: [
              UIText(
                margin: EdgeInsets.all(20.w),
                text: '礼物列表',
                fontSize: 16.sp,
              ),
              Spacer(),
              UIImage(
                assetImage: 'search_delete.png',
                width: 20.w,
                margin: EdgeInsets.only(right: 20.w),
                onTap: () {
                  Get.back();
                },
              ),
            ],
          ),
          Expanded(
            child: GetBuilder<GiftListLogic>(builder: (logic) {
              return GridView.builder(
                padding: EdgeInsets.symmetric(horizontal: 12.w, vertical: 12.h),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 4,
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  childAspectRatio: 0.9,
                ),
                itemCount: logic.listData.length,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  GiftModel giftModel = logic.listData[index];
                  return UIColumn(
                    onTap: () {
                      logic.selectGift(giftModel);
                    },
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(8.r),
                      ),
                      border: Border.all(
                        width: 2.w,
                        color: logic.selectedGift?.id == giftModel.id
                            ? kAppBlackColor
                            : Colors.transparent,
                      ),
                    ),
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      UIImage(
                        // assetImage: 'svip_medal.png',
                        httpImage: giftModel.imageUrl?.toImageUrl(),
                        width: 55.w,
                        height: 55.h,
                        disabledHttpBigImage: true,
                      ),
                      UIText(
                        text: giftModel.giftName,
                        fontSize: 12,
                      ),
                      UIRow(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          UIText(
                            text: '${giftModel.giftPrice}',
                            fontSize: 12,
                          ),
                          UIImage(
                            assetImage: 'icon_signIn_coin.png',
                            width: 20.w,
                            // padding: EdgeInsets.only(top: 4.h),
                          ),
                        ],
                      )
                    ],
                  );
                },
              );
            }),
          ),
          GetBuilder<GiftListLogic>(
            builder: (logic) {
              return UIRow(
                padding: EdgeInsets.symmetric(horizontal: 12.w, vertical: 12.h),
                children: [
                  UIText(
                    text: '余额：${logic.walletModel?.balance ?? '0'}',
                  ),
                  UIImage(
                    padding: EdgeInsets.only(top: 4.h),
                    assetImage: 'icon_signIn_coin.png',
                    width: 20.w,
                  ),
                  const Spacer(),
                  if (logic.selectedGift != null)
                    UISolidButton(
                      onTap: () {
                        logic.giveGift();
                      },
                      color: kAppBlackColor,
                      textColor: kAppWhiteColor,
                      height: 37.h,
                      width: 73.w,
                      text: '赠送',
                    )
                  else
                    SizedBox(
                      height: 37.h,
                    )
                ],
              );
            },
          ),
        ],
      ),
    );
  }
}
