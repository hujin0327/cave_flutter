import 'package:cave_flutter/network/whl_api.dart';
import 'package:easy_refresh/easy_refresh.dart';
import 'package:get/get.dart';

import '../../../model/dynamic_model.dart';

class CollectionLogic extends GetxController {
  List listData = [];
  EasyRefreshController refreshController = EasyRefreshController(
      controlFinishLoad: true, controlFinishRefresh: true);

  @override
  void onInit() {
    super.onInit();
    loadCollectionData();
  }

  loadCollectionData() {
    WhlApi.myCollect.get({}).then((value) {
      if (value.isSuccess()) {
        List records = value.data["records"];
        List<DynamicModel> modelList =
            records.map((e) => DynamicModel.fromJson(e)).toList();
        listData = modelList;
        update();
      }
      refreshController.finishRefresh();
    });
  }
}
