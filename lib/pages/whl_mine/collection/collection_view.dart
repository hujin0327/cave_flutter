import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/empty_widget.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../common/common_widget.dart';
import '../../../model/dynamic_model.dart';
import '../../../widgets/refresh_widget.dart';
import 'collection_logic.dart';

class CollectionPage extends StatelessWidget {
  CollectionPage({Key? key}) : super(key: key);

  final logic = Get.put(CollectionLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppWhiteColor,
      appBar: WhlAppBar(
        backgroundColor: kAppWhiteColor,
        centerTitle: "我的收藏",
      ),
      body: GetBuilder<CollectionLogic>(builder: (logic) {
        return MyRefresh(
          controller: logic.refreshController,
          child: logic.listData.isEmpty
              ? ListView(
                  children: [
                    MyEmptyWidget(
                      margin: EdgeInsets.only(top: 200.h),
                    )
                  ],
                )
              : CustomScrollView(
                  slivers: [
                    SliverPadding(
                      padding: EdgeInsets.only(left: 12.w, right: 10.w),
                      sliver: SliverList(
                          delegate:
                              SliverChildBuilderDelegate((context, index) {
                        DynamicModel model = logic.listData[index];
                        return commonSquareView(
                            source: DynamicDataSource.Collection,
                            model: model,
                            logic: logic,
                            isCollect: true);
                      }, childCount: logic.listData.length)),
                    )
                  ],
                ),
          onRefresh: () {
            logic.loadCollectionData();
          },
        );
      }),
    );
  }
}
