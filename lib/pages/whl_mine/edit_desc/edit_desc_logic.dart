import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class Edit_descLogic extends GetxController {
  TextEditingController editingController = TextEditingController();

  @override
  void onInit() {
    super.onInit();

    String value = Get.arguments ?? "";
    editingController.text = value;
  }

  saveUserConfig() {
    WhlApi.saveUserInformation.post({"introduction": editingController.text},
        withLoading: true).then((value) {
      if (value.isSuccess()) {
        MyToast.show("保存成功");
        Get.back(result: editingController.text);
      }
    });
  }

  void onConfirm() {
    if (editingController.text.isEmpty) {
      MyToast.show("请输入个人介绍");
      return;
    }
    saveUserConfig();
  }
}
