import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../common/common_widget.dart';
import 'edit_desc_logic.dart';

class Edit_descPage extends StatelessWidget {
  Edit_descPage({Key? key}) : super(key: key);

  final logic = Get.put(Edit_descLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppWhiteColor,
      appBar: WhlAppBar(
        backgroundColor: kAppWhiteColor,
        isShowBottomLine: false,
      ),
      body: UIColumn(
        children: [
          UIColumn(
            radius: 20.w,
            // height: 374.h,
            crossAxisAlignment: CrossAxisAlignment.start,
            color: kAppColor("#F9F9F9"),
            margin: EdgeInsets.only(left: 17.w, right: 17.w, top: 22.h),
            children: [
              UIText(
                margin: EdgeInsets.only(left: 25.w, top: 32.h, bottom: 22.h),
                text: "关于你的自我介绍",
                fontSize: 16.sp,
                textColor: kAppBlackColor,
              ),
              UIContainer(
                margin: EdgeInsets.symmetric(horizontal: 25.w),
                height: 1.h,
                color: kAppStrokeColor,
              ),
              UIContainer(
                height: 105,
                child: noBorderCTextField(
                    controller: logic.editingController,
                    hint: "Eg：你的爱好、属性、星座、兴趣...",
                    hintTextColor: kAppSub3TextColor,
                    maxLine: 100,
                    // maxLength: 256,
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 10.h, horizontal: 25.w),
                    onChanged: (value) {
                      if (value.length > 256) {
                        logic.editingController.text = value.substring(0, 256);
                      }
                      logic.update();
                    }),
              ),
              UIStack(
                alignment: Alignment.bottomRight,
                children: [
                  UIImage(
                    assetImage: "mine_editBg",
                    width: 120.w,
                    height: 165.w,
                  ),
                  GetBuilder<Edit_descLogic>(builder: (logic) {
                    return UIText(
                      margin: EdgeInsets.only(bottom: 27.h, right: 23.w),
                      text: "${logic.editingController.text.length}/256",
                      textColor: kAppSub3TextColor,
                      fontSize: 12.sp,
                    );
                  })
                ],
              )
            ],
          ),
          const Expanded(child: UIContainer()),
          UIText(
            margin: EdgeInsets.symmetric(horizontal: 20.w, vertical: 60.h),
            text: "确定",
            textColor: kAppWhiteColor,
            fontSize: 16.sp,
            fontWeight: FontWeight.bold,
            color: kAppBlackColor,
            height: 52.h,
            radius: 26.h,
            alignment: Alignment.center,
            onTap: () {
              logic.onConfirm();
            },
          )
        ],
      ),
    );
  }
}
