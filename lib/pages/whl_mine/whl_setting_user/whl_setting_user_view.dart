import 'package:cave_flutter/pages/whl_mine/whl_mine_logic.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/whl_avatar_utils.dart';

import 'package:cave_flutter/utils/whl_picker_util.dart';
import 'package:cave_flutter/widgets/whl_bottom_action_sheet.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'whl_setting_user_logic.dart';

class WhlSettingUserPage extends StatelessWidget {
  WhlSettingUserPage({Key? key}) : super(key: key);

  final logic = Get.put(WhlSettingUserLogic());
  final mineLogic = Get.find<WhlMineLogic>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: WhlAppBar(
        backgroundColor: Colors.white,
        centerTitle: '',
        isShowBottomLine: false,
        rightWidget: UIText(
          margin: EdgeInsets.only(right: 18.w),
          text: 'Save',
          textColor: kAppThemeColor,
          fontSize: 18.sp,
          fontWeight: FontWeight.w500,
          onTap: () {
            logic.doClickSave();
          },
        ),
      ),
      body: UIColumn(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        margin: EdgeInsets.only(top: 32.h, left: 18.w, right: 18.w),
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GetBuilder<WhlMineLogic>(builder: (logic) {
            return Center(
              child: GestureDetector(
                child: Stack(
                  alignment: Alignment.bottomRight,
                  children: [
                    UIImage(
                      httpImage: logic.userInfoModel.avatar,
                      width: 108.w,
                      height: 108.w,
                      radius: 100.w,
                      color: Colors.grey.withOpacity(0.8),
                    ),
                    UIImage(
                      assetImage: 'icon_avatar_take.png',
                      width: 32.w,
                    )
                  ],
                ),
                onTap: () {
                  WhlBottomActionSheet.show( ['Take a photo', 'Choose from album'], callBack: (index) {
                    if (index == 0) {
                      AvatarUtils.onCameraPicker(context, onSuccess: (url) {
                        logic.userInfoModel.avatar = url;
                        logic.update();
                      });
                    } else {
                      AvatarUtils.onChoosePhoto(context, onSuccess: (url) {
                        logic.userInfoModel.avatar = url;
                        logic.update();
                      });
                    }
                  });
                },
              ),
            );
          }),
          UIText(
            margin: EdgeInsets.only(top: 48.h),
            text: 'Nick Name',
            textColor: kAppSub2TextColor,
            fontSize: 12.sp,
          ),
          TextField(
            controller: logic.nickNameController,
            decoration: InputDecoration(
              hintText: 'Please enter your nickname',
              hintStyle: TextStyle(
                color: kAppSub2TextColor,
                fontSize: 14.sp,
              ),
              border: InputBorder.none,
            ),
            inputFormatters: [LengthLimitingTextInputFormatter(15)],
          ),
          kAppDivider,
          UIText(
            margin: EdgeInsets.only(top: 24.h),
            text: 'Date of Birth',
            textColor: kAppSub2TextColor,
            fontSize: 12.sp,
          ),
          GetBuilder<WhlMineLogic>(builder: (logic) {
            return UIText(
              padding: EdgeInsets.symmetric(vertical: 10.h),
              text: logic.userInfoModel.birthDate,
              textColor: kAppTextColor,
              fontSize: 14.sp,
              onTap: () {
                WhlPickerUtil.showPickerDateTime(context, 7, (picker, selected) {
                  logic.userInfoModel.birthDate = picker.adapter.text.substring(0, 10);
                  logic.update();
                },
                    maxDateTime: DateTime(DateTime.now().year - 18, DateTime.now().month, DateTime.now().day),
                    minDateTime: DateTime(DateTime.now().year - 100));
              },
            );
          }),
          kAppDivider
        ],
      ),
    );
  }
}
