import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/pages/whl_mine/whl_mine_logic.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class WhlSettingUserLogic extends GetxController {
  TextEditingController nickNameController = TextEditingController();
  WhlMineLogic logic = Get.find();

  @override
  void onInit() {
    super.onInit();
    nickNameController.text = logic.userInfoModel.nickname ?? '';
  }

  doClickSave() async {
    if (nickNameController.text.isEmpty) {
      MyToast.show('Please enter nickname');
      return;
    }
    Map<String, dynamic> params = {
      'nickname': nickNameController.text,
      'birthday': logic.userInfoModel.birthDate,
    };
    ResponseData responseData = await WhlApi.saveUserBaseInfo.post(params);
    if (responseData.isSuccess()) {
      MyToast.show('Save Success');
      Get.back();
    }
  }
}
