import 'package:cave_flutter/model/base_source_model.dart';
import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/extension/datetime_extension.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MineVipLogic extends GetxController
    with GetSingleTickerProviderStateMixin {
  List<MyVipModel> vipList = [
    MyVipModel(
      vipType: 1,
      vipTypeName: "VIP",
      startColor: kAppColor("#FFE95A"),
      endColor: kAppColor("#FFCF41"),
      bgImage: "vip_card_bg.png",
      vipImage: "vip_tag.png",
      vipMedalImage: "vip_medal.png",
      vipInfo: [
        {"image": "vip_zsbs", "title": "VIP专属标识"},
        {"image": "vip_wxyh", "title": "无限次右滑"},
        {"image": "vip_yssz", "title": "隐私设置"},
        {"image": "vip_dtzd", "title": "动态置顶"},
        {"image": "vip_fsst", "title": "发送闪图"},
        {"image": "vip_wzmy", "title": "位置漫游"},
        {"image": "vip_hysx", "title": "好友筛选"},
        {"image": "vip_yxbg", "title": "优先曝光"}
      ],
    ),
    MyVipModel(
      vipType: 2,
      vipTypeName: "SVIP",
      startColor: kAppColor("#3E270C"),
      endColor: kAppColor("#B08837"),
      bgImage: "svip_card_bg.png",
      vipImage: "svip_tag.png",
      vipMedalImage: "svip_medal.png",
      vipInfo: [
        {"image": "svip_zsbs", "title": "SVIP专属标识"},
        {"image": "svip_gjys", "title": "高级隐私设置"},
        {"image": "svip_gjhy", "title": "高级好友筛选"},
        {"image": "svip_gjzl", "title": "高级资料展示"},
        {"image": "svip_gjzlbj", "title": "高级资料编辑"},
        {"image": "svip_dttj", "title": "动态推荐"},
        {"image": "svip_dtds", "title": "动态定时"},
        {"image": "svip_bnm", "title": "不匿名评论"}
      ],
    ),
    MyVipModel(
      vipType: 3,
      vipTypeName: "星钻",
      startColor: kAppColor("#FBE1FF"),
      endColor: kAppColor("#FFEBF1"),
      bgImage: "xzvip_card_bg.png",
      vipImage: "xzvip_tag.png",
      vipMedalImage: "xzvip_medal.png",
      vipInfo: [
        {"image": "xzvip_zsnc", "title": "星钻专属昵称"},
        {"image": "xzvip_zsbs", "title": "星钻专属标识"},
        {"image": "xzvip_zstxk", "title": "星钻专属头像框"},
        {"image": "xzvip_zsxz", "title": "星钻专属勋章"},
        {"image": "xzvip_ltqp", "title": "星钻聊天气泡"},
        {"image": "xzvip_xzlw", "title": "星钻礼物"},
        {"image": "xzvip_glyqx", "title": "超级管理员权限"},
        {"image": "xzvip_gcqx", "title": "超级广场权限"}
      ],
    ),
    MyVipModel(
      vipType: 4,
      vipTypeName: "私人专属",
      startColor: kAppColor("#FFE95A"),
      endColor: kAppColor("#FFCF41"),
      vipInfo: [
        {"image": "srvip_smrtq", "title": "神秘人特权"},
        {"image": "srvip_dzzsx", "title": "定制专属心"},
        {"image": "srvip_bxltd", "title": "不限量推顶卡"},
        {"image": "srvip_hjnc", "title": "黑金昵称"},
        {"image": "srvip_hjbs", "title": "黑金标识"},
        {"image": "srvip_hjtxk", "title": "黑金头像框"},
        {"image": "srvip_hjxz", "title": "黑金勋章"},
        {"image": "srvip_hjltqp", "title": "黑金聊天气泡"}
      ],
    ),
  ];

  List<VipPriceModel> vipPriceList = [];

  List payTypeList = [
    {"title": "支付宝", "image": "vip_alipay", "key": "1"},
    {"title": "微信", "image": "vip_wechat", "key": "2"}
  ];

  late TabController tbc;
  Color? vipColor;
  String selectPayType = "";
  VipPriceModel? currentSelectModel;
  bool isShow = false;
  int tabIndex = 0;

  @override
  void onInit() {
    super.onInit();
    tbc = TabController(length: vipList.length, vsync: this);
    getMyMember();
    getVipList();
  }

  getMyMember() async {
    ResponseData responseData = await WhlApi.myMember.post({});
    if (responseData.isSuccess()) {
      responseData.data.forEach((item) {
        MyVipModel? myVip =
            vipList.firstWhereOrNull((vip) => vip.vipType == item['vipType']);
        if (myVip != null) {
          myVip.openTime = DateTime.parse(item['openTime'])
              .toDateStringWithFormat(KDateTimeYMD);
          myVip.closeTime = DateTime.parse(item['closeTime'])
              .toDateStringWithFormat(KDateTimeYMD);
        }
      });
      update();
    }
  }

  getVipList() async {
    ResponseData responseData = await WhlApi.vipList.get({});
    if (responseData.isSuccess()) {
      Map<int, String> titleMap = {
        1: '包月会员',
        2: '包季会员',
        3: '包年会员',
        4: '连续包月会员',
        5: '连续包季会员',
        6: '连续包年会员',
      };
      vipPriceList.clear();
      responseData.data.forEach((item) {
        int rechargeType = item['rechargeType'];
        double originAmount = double.parse("${item['amount']}");
        double amount = double.parse("${item['amount']}");
        double? oneDownRate = double.tryParse("${item['oneDownRate']}");
        double? twoDownRate = double.tryParse("${item['twoDownRate']}");
        double? monthlyAmount;
        if (item['rechargeType'] < 4 && oneDownRate != null) {
          amount = amount - (amount * (oneDownRate / 10000));
        } else if (item['rechargeType'] > 3 && twoDownRate != null) {
          amount = amount - (amount * (twoDownRate / 10000));
        }
        switch (rechargeType) {
          case 1:
          case 4:
            monthlyAmount = amount;
            break;
          case 2:
          case 5:
            monthlyAmount = amount / 3;
            break;
          case 3:
          case 6:
            monthlyAmount = amount / 12;
            break;
        }

        vipPriceList.add(
          VipPriceModel(
            id: item['id'],
            title: titleMap[item['rechargeType']],
            vipType: item['vipType'],
            rechargeType: rechargeType,
            originAmount: originAmount,
            amount: amount,
            monthlyAmount: monthlyAmount,
          ),
        );
      });
      update();
    }
  }

  indexChange(int index) {
    tabIndex = index;
    if (index == 0) {
      vipColor = kAppColor("#FDE975");
    } else if (index == 1) {
      vipColor = kAppColor("#FF6219");
    } else if (index == 2) {
      vipColor = kAppColor("#FADBFA");
    } else if (index == 3) {
      vipColor = kAppColor("#F6BE6F");
    }
    isShow = false;
    update();
  }

  selectVipPrice(VipPriceModel model) {
    currentSelectModel = model;
    update();
  }

  openMember() {
    if (currentSelectModel != null) {
      WhlApi.openMember.post({
        'caveVipSetId': currentSelectModel!.id,
        'source': 1,
        // 'rechargeType': currentSelectModel!.rechargeType,
        'vipType': currentSelectModel?.vipType
      }).then((value) {
        getMyMember();
      });
    } else {
      MyToast.show('请选择充值会员类型');
    }
  }
}

class VipPriceModel {
  String? id;
  String? title;
  int? vipType;
  //  现价
  double? amount;
  double? originAmount;
  //  每月价格
  double? monthlyAmount;
  int? rechargeType;

  VipPriceModel(
      {this.id,
      this.title,
      this.vipType,
      this.amount,
      this.originAmount,
      this.rechargeType,
      this.monthlyAmount});
}

class MyVipModel {
  String? id;
  int? amount;
  String? caveVipSetId;
  String? closeTime;
  String? openTime;
  int? rechargeType;
  int? status;
  int? userId;
  int? vipType;
  String? vipTypeName;
  Color? startColor;
  Color? endColor;
  String? bgImage;
  String? vipImage;
  String? vipMedalImage;
  List<dynamic>? vipInfo;

  MyVipModel({
    this.id,
    this.amount,
    this.caveVipSetId,
    this.closeTime,
    this.openTime,
    this.rechargeType,
    this.status,
    this.userId,
    this.vipType,
    this.vipTypeName,
    this.startColor,
    this.endColor,
    this.vipInfo,
    this.bgImage,
    this.vipImage,
    this.vipMedalImage,
  });

  MyVipModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    amount = json['amount'];
    caveVipSetId = json['caveVipSetId'];
    closeTime = json['closeTime'];
    openTime = json['openTime'];
    rechargeType = json['rechargeType'];
    status = json['status'];
    userId = json['userId'];
    vipType = json['vipType'];
    vipTypeName = json['vipTypeName'];
  }
}
