import 'dart:math';

import 'package:cave_flutter/model/base_source_model.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'mine_vip_logic.dart';
import 'dart:ui' as UI;

class MineVipPage extends StatelessWidget {
  MineVipPage({Key? key}) : super(key: key);

  final logic = Get.put(MineVipLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: WhlAppBar(
        isShowBottomLine: false,
        backgroundColor: kAppLSbgColor,
        centerWidget: UIContainer(
          // margin: EdgeInsets.only(left: 48.w, right: 15.w),
          width: MediaQuery.of(context).size.width,
          color: kAppLSbgColor,
          child: GetBuilder<MineVipLogic>(
            builder: (logic) {
              return DefaultTabController(
                length: logic.vipList.length,
                child: TabBar(
                  controller: logic.tbc,
                  labelColor: logic.vipColor ?? kAppColor("#FDE975"),
                  labelStyle: TextStyle(
                    fontSize: 15.sp,
                  ),
                  // indicatorColor: logic.vipColor ?? kAppColor("#66A2F8"),
                  indicatorSize: TabBarIndicatorSize.label,
                  // indicatorWeight: 3.0,
                  indicator: UnderlineTabIndicator(
                    borderRadius: BorderRadius.circular(1.5),
                    borderSide: BorderSide(
                      color: logic.vipColor ?? kAppColor("#FDE975"),
                      width: 3,
                    ),
                  ),
                  isScrollable: true,
                  unselectedLabelColor: kAppSub3TextColor,
                  unselectedLabelStyle: TextStyle(
                    fontSize: 16.sp,
                  ),
                  tabs: logic.vipList.map((e) {
                    return Tab(
                      text: e.vipTypeName,
                    );
                  }).toList(),
                  onTap: (index) {
                    logic.indexChange(index);
                  },
                ),
              );
            },
          ),
        ),
      ),
      bottomNavigationBar: confirmView(),
      body: TabBarView(
        controller: logic.tbc,
        children: logic.vipList.map((e) {
          return GetBuilder<MineVipLogic>(builder: (logic) {
            if (e.vipType == 4) {
              return blackVIPView(e);
            } else {
              return ListView(
                children: [
                  vipInfoView(e),
                  getDownView(e),
                ],
              );
            }
          });
        }).toList(),
      ),
    );
  }

  Widget vipInfoView(MyVipModel e) {
    return UIContainer(
      decoration: BoxDecoration(
        color: kAppColor("#060709"),
      ),
      width: ScreenUtil().screenWidth,
      padding: EdgeInsets.only(top: 16.h),
      child: UIStack(
        clipBehavior: Clip.none,
        color: kAppColor("#060709"),
        children: [
          UIImage(
            width: double.infinity,
            margin: EdgeInsets.symmetric(horizontal: 30.w),
            assetImage: e.bgImage,
          ),
          Positioned(
            top: 30.h,
            left: 28.w,
            child: UIColumn(
              margin: EdgeInsets.symmetric(horizontal: 30.w),
              mainAxisSize: MainAxisSize.min,
              children: [
                UIImage(
                  assetImage: e.vipImage,
                  width: 61.w,
                ),
                UIText(
                  margin: EdgeInsets.only(top: 8.w),
                  width: 120.w,
                  text: e.closeTime != null ? "${e.closeTime}到期" : '未开通',
                  textColor: e.endColor, //  kAppColor("#FCB124"),
                  fontSize: 12.sp,
                ),
              ],
            ),
          ),
          Positioned(
            top: -40.h,
            right: 56.w,
            child: UIImage(
              assetImage: e.vipMedalImage,
            ),
          ),
          Positioned(
            top: 120.h,
            width: ScreenUtil().screenWidth,
            child: UIImage(
              margin: EdgeInsets.symmetric(horizontal: 16.w),
              assetImage: "vip_bg.png",
              // imageColor: kAppWhiteColor, //.withOpacity(0.8),
              // imageColor: kAppDisabledTextColor,
            ),
          ),
          vipInfoGridView(e),
        ],
      ),
    );
  }

  Widget vipInfoGridView(MyVipModel vipModel, {EdgeInsetsGeometry? margin}) {
    return UIContainer(
        margin: margin ?? EdgeInsets.only(top: 150.h, left: 20.w, right: 20.w),
        child: UIColumn(
          children: [
            UIText(
              margin: EdgeInsets.only(bottom: 10.h),
              textColor: kAppWhiteColor,
              text: "VIP权益",
              alignment: Alignment.center,
              fontWeight: FontWeight.w400,
              fontSize: 13.sp,
            ),
            GridView.builder(
              physics: NeverScrollableScrollPhysics(),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 4,
                mainAxisSpacing: 10.w,
                crossAxisSpacing: 10.w,
                childAspectRatio: 60.0 / 60,
              ),
              itemBuilder: (context, index) {
                Map dic = vipModel.vipInfo![index];
                return UIColumn(
                  children: [
                    UIImage(
                      width: 48.w,
                      height: 48.w,
                      assetImage: dic["image"],
                    ),
                    UIText(
                      margin: EdgeInsets.only(top: 5.w, left: 0, right: 0),
                      text: dic["title"],
                      alignment: Alignment.center,
                      textColor: kAppSub3TextColor,
                      fontSize: 11.sp,
                    )
                  ],
                );
              },
              itemCount: logic.isShow
                  ? vipModel.vipInfo!.length
                  : min(vipModel.vipInfo!.length, 4),
              shrinkWrap: true,
            ),
          ],
        ));
  }

  Widget getDownView(MyVipModel myVipModel) {
    return UIStack(
      children: [
        UIContainer(
          color: kAppColor("#060709"),
          width: ScreenUtil().screenWidth,
          height: 60.h,
        ),
        UIContainer(
          margin: EdgeInsets.only(top: 0.w),
          clipBehavior: Clip.hardEdge,
          decoration: topRadiusBoxDecoration(
              bgColor: kAppColor("#F1F5FD"), radiu: 30.w),
          child: UIColumn(
            // mainAxisSize: MainAxisSize.min,
            children: [
              Center(
                child: UIImage(
                  // margin: EdgeInsets.symmetric(vertical: 5.h),
                  padding: EdgeInsets.all(5.w),
                  assetImage: "vip_down",
                  imageColor: kAppColor("#CCCCCC"),
                  width: 30.w,
                  height: 30.w,
                  onTap: () {
                    logic.isShow = !logic.isShow;
                    logic.update();
                  },
                ),
              ),
              priceGridView(myVipModel),
              //  暂时注释掉对接支付相关的内容
              // payTypeView(),
            ],
          ),
        )
      ],
    );
  }

  Widget confirmView() {
    return GetBuilder<MineVipLogic>(builder: (logic) {
      if (logic.tabIndex == 3) {
        return SizedBox();
      }
      return UIContainer(
        padding: EdgeInsets.only(left: 21.w, right: 21.w, bottom: 25.w),
        decoration:
            topRadiusBoxDecoration(bgColor: kAppWhiteColor, radiu: 30.w),
        width: ScreenUtil().screenWidth,
        child: UIText(
          margin: EdgeInsets.symmetric(horizontal: 21.w, vertical: 23.h),
          radius: 27.h,
          height: 54.h,
          text: "立即充值",
          onTap: () {
            logic.openMember();
          },
          alignment: Alignment.center,
          textColor: kAppTextColor,
          fontSize: 16.sp,
          fontWeight: FontWeight.bold,
          gradientStartColor: kAppColor("#FEC987"),
          gradientEndColor: kAppColor("#FB807B"),
        ),
      );
    });
  }

  Widget priceGridView(MyVipModel myVipModel) {
    List<VipPriceModel> vipsPrice = logic.vipPriceList
        .where((v) => v.vipType == myVipModel.vipType)
        .toList();
    return UIContainer(
      margin: EdgeInsets.symmetric(horizontal: 16.w),
      padding: EdgeInsets.symmetric(horizontal: 14.w),
      height: 105.w,
      child: ListView.separated(
        scrollDirection: Axis.horizontal,
        separatorBuilder: (context, int index) {
          return SizedBox(width: 11.w);
        },
        itemBuilder: (context, index) {
          VipPriceModel model = vipsPrice[index];
          bool selected = model.id == logic.currentSelectModel?.id;
          return UIContainer(
            padding: EdgeInsets.symmetric(horizontal: 15.w),
            clipBehavior: Clip.hardEdge,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.r),
              // radiu: 8.r,
              color: selected
                  ? kAppColor("#FF5858").withOpacity(0.06)
                  : kAppWhiteColor,
              border: Border.all(
                color: selected ? kAppColor("#FB7E7B") : kAppLineColor,
                width: 0.5,
              ),
            ),
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 0, right: 0, top: 12.h),
                  child: Text(
                    model.title!,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 14.sp,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                UIText(
                  margin: EdgeInsets.only(top: 6.h, bottom: 2.h),
                  text: "¥${model.originAmount}",
                  textColor: kAppSub2TextColor,
                  fontSize: 8.sp,
                  textDecoration: TextDecoration.lineThrough,
                ),
                Text.rich(
                  textAlign: TextAlign.center,
                  TextSpan(children: [
                    TextSpan(
                        text: "¥",
                        style: TextStyle(
                          color: kAppColor("#FF5858"),
                          fontSize: 11.sp,
                        )),
                    TextSpan(
                        text: "${model.amount}",
                        style: TextStyle(
                          color: kAppColor("#FF5858"),
                          fontSize: 21.sp,
                          fontWeight: FontWeight.bold,
                        ))
                  ]),
                ),
                if (model.monthlyAmount != null)
                  UIText(
                    text: "${model.monthlyAmount?.toStringAsFixed(1)}元/月",
                    margin: EdgeInsets.only(top: 2.w),
                    textColor: kAppSub2TextColor,
                    fontSize: 10.sp,
                  ),
              ],
            ),
            onTap: () {
              logic.selectVipPrice(model);
            },
          );
        },
        itemCount: vipsPrice.length,
        shrinkWrap: true,
      ),
    );
  }

  Widget payTypeView() {
    return UIContainer(
      margin: EdgeInsets.only(top: 6.h, bottom: 13.h),
      child: ListView.builder(
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) {
          Map dic = logic.payTypeList[index];
          return UIContainer(
            margin: EdgeInsets.only(top: 10.h, left: 16.w, right: 16.w),
            color: kAppWhiteColor,
            radius: 14.w,
            child: Row(
              children: [
                UIImage(
                  margin: EdgeInsets.only(
                      left: 15.w, top: 13.w, bottom: 13.w, right: 11.w),
                  assetImage: dic["image"],
                  width: 24.w,
                  height: 24.w,
                ),
                Expanded(
                    child: UIText(
                  text: dic["title"],
                  fontSize: 13.sp,
                  textColor: kAppBlackColor,
                )),
                UIImage(
                  padding: EdgeInsets.all(3.w),
                  margin:
                      EdgeInsets.symmetric(vertical: 13.w, horizontal: 11.w),
                  assetImage: logic.selectPayType == dic["key"]
                      ? "icon_select"
                      : "vip_select_no",
                  width: 24.w,
                  height: 24.w,
                ),
              ],
            ),
            onTap: () {
              logic.selectPayType = dic["key"];
              logic.update();
            },
          );
        },
        shrinkWrap: true,
        itemCount: logic.payTypeList.length,
      ),
    );
  }

  Widget blackVIPView(MyVipModel e) {
    return UIContainer(
      color: kAppColor("#060709"),
      padding: EdgeInsets.only(top: 15.w),
      child: ListView(
        children: [
          Text(
            "限量黑金用户",
            style: TextStyle(
                fontSize: 29.sp,
                letterSpacing: 3,
                foreground: Paint()
                  ..shader = UI.Gradient.linear(
                      const Offset(100, 20),
                      const Offset(250, 20),
                      <Color>[kAppColor("#E7942D"), kAppColor("#FFDFA6")]),
                fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
          UIContainer(
            margin: EdgeInsets.only(top: 10.w),
            child: Text(
              "珍稀身份象征，尽享尊贵特权",
              style: TextStyle(
                  fontSize: 13.sp,
                  letterSpacing: 6,
                  color: kAppColor("#F5DAA4")),
              textAlign: TextAlign.center,
            ),
          ),
          UIImage(
            margin: EdgeInsets.only(top: 20.w, right: 30.w),
            assetImage: "bvip_card",
            width: 265.w,
            height: 181.w,
          ),
          UIStack(
            alignment: Alignment.topCenter,
            children: [
              UIImage(
                assetImage: "bvip_bg",
                width: 180.w,
                height: 226.w,
              ),
              UIRow(
                margin: EdgeInsets.only(top: 70.w),
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  UIImage(
                    margin: EdgeInsets.only(right: 4.w),
                    assetImage: "bvip_left",
                    width: 9.w,
                    height: 16.w,
                  ),
                  Text(
                    "尊享66项特权",
                    style: TextStyle(
                        fontSize: 12.sp,
                        color: kAppColor("#FDD08B"),
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                  UIImage(
                    margin: EdgeInsets.only(left: 4.w),
                    assetImage: "bvip_right",
                    width: 9.w,
                    height: 16.w,
                  ),
                ],
              ),
              UIColumn(
                margin: EdgeInsets.only(top: 120.w),
                padding: EdgeInsets.symmetric(horizontal: 20.w),
                children: [
                  vipInfoGridView(e, margin: EdgeInsets.zero),
                  UIImage(
                    padding: EdgeInsets.all(5.w),
                    assetImage: "vip_down",
                    width: 30.w,
                    height: 30.w,
                    onTap: () {
                      logic.isShow = true;
                      logic.update();
                    },
                  ),
                  UIText(
                    margin: EdgeInsets.only(top: 25.w, bottom: 11.w),
                    height: 48.w,
                    width: 150.w,
                    radius: 24.w,
                    color: kAppColor("#1F180E"),
                    textColor: kAppWhiteColor,
                    text: "您还无法开通",
                    alignment: Alignment.center,
                    fontSize: 15.sp,
                  ),
                  UIText(
                    text: "如有疑问请致电咨询",
                    textColor: kAppSub3TextColor,
                    fontSize: 10.sp,
                  )
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}
