import 'package:cave_flutter/pages/whl_mine/privacy_setting/privacy_setting_logic.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/widget/basic_widget.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:cave_flutter/widgets/whl_row_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class PrivacySettingPage extends StatelessWidget {
  PrivacySettingPage({super.key});
  final logic = Get.put(PrivacySettingLogic());
  // ScreenUtil().statusBarHeight
  @override
  Widget build(BuildContext context) {
    EdgeInsets padding = EdgeInsets.symmetric(horizontal: 0.w, vertical: 13.w);
    return Scaffold(
      backgroundColor: kAppWhiteColor,
      appBar: WhlAppBar(
        centerTitle: "隐私管理",
        backgroundColor: kAppWhiteColor,
      ),
      body: GetBuilder<PrivacySettingLogic>(builder: (logic) {
        return SingleChildScrollView(
            child: UIColumn(
                crossAxisAlignment: CrossAxisAlignment.start,
                padding: EdgeInsets.only(
                    top: 0.w, left: 16.w, right: 16.w, bottom: 0.w),
                children: logic.privacyList
                    .map(
                      (e) => MyRowItem(
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                                color: kAppColor('#E5E5E5'), width: 0.5.w),
                          ),
                        ),
                        padding: padding,
                        title: e.configName,
                        isShowSVip: e.condition == 2,
                        isShowSwitch: true,
                        isSwitchOn: e.isOpen == 1,
                        onChanged: () {
                          logic.saveDate(e);
                        },
                      ),
                    )
                    .toList()));
      }),
    );
  }
}
