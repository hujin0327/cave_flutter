import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:get/get.dart';

class PrivacySettingLogic extends GetxController {
  List<PrivacySettingModel> privacyList = [];

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    loadData();
  }

  void loadData() {
    WhlApi.myPrivacy.get({"type": 1}).then((value) {
      if (value.isSuccess()) {
        List data = value.data;
        List<PrivacySettingModel> allModels = [];
        for (var element in data) {
          allModels.add(PrivacySettingModel.fromJson(element));
        }
        privacyList = allModels;
      } else {
        if ((value.msg ?? "").isNotEmpty) {
          MyToast.show(value.msg!);
        }
      }
      update();
    });
  }

  void saveDate(PrivacySettingModel item) {
    WhlApi.saveMyPrivacy
        .post({"configKey": item.configKey, "isOpen": item.isOpen == 0 ? 1 : 0, "privacyType": item.privacyType}).then((value) {
      if (value.isSuccess()) {
        item.isOpen = item.isOpen == 0 ? 1 : 0;
      } else {
        MyToast.show(value.msg ?? "编辑失败");
      }
      update();
    });
  }
}

class PrivacySettingModel {
  String? configKey;
  String? configName;
  int? isOpen;
  int? condition;
  int? privacyType;

  PrivacySettingModel({
    this.configKey,
    this.configName,
    this.isOpen,
    this.condition,
    this.privacyType,
  });

  PrivacySettingModel.fromJson(Map<String, dynamic> json) {
    configKey = json['configKey'];
    configName = json['configName'] ?? '';
    isOpen = json['isOpen'] ?? 0;
    condition = json['condition'] ?? 0;
    privacyType = json['privacyType'] ?? 1;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['configKey'] = configKey;
    data['configName'] = configName;
    data['isOpen'] = isOpen;
    data['condition'] = condition;
    data['privacyType'] = privacyType;
    return data;
  }
}
