import 'package:cave_flutter/model/user_model.dart';
import 'package:cave_flutter/network/whl_api.dart';
import 'package:get/get.dart';

class VisitorsLogic extends GetxController {
  List<String> tabList = ['访客', '访问记录'];
  int selectedIndex = 0;

  bool isOpen = false;

  List listData = [];

  @override
  void onInit(){
    super.onInit();

    loadFKData();
  }

  loadFKData(){
    WhlApi.visitClientList.get({}).then((value) {
      if (value.isSuccess()){
        if (value.data != null) {
          List data = value.data["records"];
          listData = data.map((e) => UserModel.fromJson(e)).toList();
          update();
        }
      }
    });
  }

  loadFWData(){
    WhlApi.accessList.get({}).then((value) {
      if (value.isSuccess()){
        if (value.data != null) {
          List data = value.data["records"];
          listData = data.map((e) => UserModel.fromJson(e)).toList();
          update();
        }
      }
    });
  }


  void selectIndexChange(int index) {
    selectedIndex = index;
    listData.clear();
    update();
    if (index == 0) {
      loadFKData();
    }else {
      loadFWData();
    }

  }

  void openChange(bool value) {
    isOpen = value;
    update();
  }
}
