import 'package:cave_flutter/model/user_model.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'visitors_logic.dart';

///访客记录
class VisitorsPage extends StatelessWidget {
  VisitorsPage({Key? key}) : super(key: key);

  final logic = Get.put(VisitorsLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: GetBuilder<VisitorsLogic>(builder: (logic) {
        return Column(
          children: [
            buildTopView(context),
            Expanded(
                child: CustomScrollView(
              slivers: [
                SliverToBoxAdapter(
                  child: logic.selectedIndex == 1
                      ? buildVisitTopView()
                      : UIRow(
                          margin: EdgeInsets.only(
                              left: 18.w, right: 18.w, top: 5.w),
                          children: [
                            Expanded(
                              child: UIText(
                                radius: 6.w,
                                height: 50.w,
                                alignment: Alignment.center,
                                color: kAppColor('#F9F9F9'),
                                text: '今日访客',
                                textColor: kAppTextColor,
                                fontSize: 10.sp,
                                fontWeight: FontWeight.bold,
                                bottomDrawable: UIText(
                                  text: '15',
                                  textColor: kAppTextColor,
                                  fontSize: 15.sp,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            SizedBox(width: 18.w),
                            Expanded(
                              child: UIText(
                                radius: 6.w,
                                height: 50.w,
                                alignment: Alignment.center,
                                color: kAppColor('#F9F9F9'),
                                text: '历史访客总数',
                                textColor: kAppTextColor,
                                fontSize: 10.sp,
                                fontWeight: FontWeight.bold,
                                bottomDrawable: UIText(
                                  text: '15',
                                  textColor: kAppTextColor,
                                  fontSize: 15.sp,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            )
                          ],
                        ),
                ),
                SliverList(
                    delegate: SliverChildBuilderDelegate((context, index) {
                  return buildItemView(logic.listData[index]);
                }, childCount: logic.listData.length))
              ],
            )),
          ],
        );
      }),
    );
  }

  buildItemView(UserModel model) {
    return UIRow(
      margin: EdgeInsets.only(left: 21.w, right: 18.w),
      padding: EdgeInsets.only(top: 13.w, bottom: 17.w),
      decoration: BoxDecoration(
          border: Border(
              bottom: BorderSide(color: kAppColor('#E7E7E7'), width: 0.5.w))),
      children: [
        UIImage(
          httpImage: model.avatar?.toImageUrl(),
          assetPlaceHolder: 'icon_app_logo.png',
          width: 42.w,
          height: 42.w,
          radius: 21.w,
          margin: EdgeInsets.only(right: 13.w),
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              UIText(
                text: model.nickName,
                textColor: kAppTextColor,
                fontSize: 14.sp,
                fontWeight: FontWeight.bold,
                marginDrawable: 3.w,
                endDrawable: UIImage(
                  assetImage: 'icon_level.png',
                  width: 32.w,
                ),
              ),
              UIRow(
                margin: EdgeInsets.only(top: 6.w),
                children: [
                  UIText(
                    width: 50.w,
                    height: 18.w,
                    alignment: Alignment.center,
                    text: '21岁',
                    color: kAppColor('#FCEDF2'),
                    textColor: kAppColor('#FF73A0'),
                    fontSize: 11.sp,
                    radius: 6.w,
                    marginDrawable: 3.w,
                    startDrawable: UIImage(
                      assetImage: 'icon_sex_woman_1.png',
                      height: 10.w,
                    ),
                  ),
                  UIText(
                    margin: EdgeInsets.only(left: 4.w, right: 4.w),
                    text: model.sexualOrientation,
                    textColor: kAppColor('#444444'),
                    constraints: BoxConstraints(minWidth: 50.w),
                    fontSize: 11.sp,
                    radius: 6.w,
                    alignment: Alignment.center,
                    color: kAppColor('#F6F6F6'),
                    height: 18.w,
                    padding: EdgeInsets.symmetric(horizontal: 8.w),
                  ),
                  UIText(
                    margin: EdgeInsets.only(left: 4.w, right: 4.w),
                    text: 'CD',
                    textColor: kAppColor('#444444'),
                    fontSize: 11.sp,
                    alignment: Alignment.center,
                    constraints: BoxConstraints(minWidth: 50.w),
                    radius: 6.w,
                    color: kAppColor('#F6F6F6'),
                    height: 18.w,
                    padding: EdgeInsets.symmetric(horizontal: 8.w),
                  ),
                  Expanded(child: Container()),
                  UIText(
                    text: '6.2km · ${model.visitTime}访问',
                    textColor: kAppColor('#C1C1C1'),
                    fontSize: 10.sp,
                  )
                ],
              )
            ],
          ),
        )
      ],
    );
  }

  buildVisitTopView() {
    return UIRow(
      color: kAppColor("#F9F9F9"),
      padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 13.h),
      children: [
        UIText(
          text: "隐藏访问",
          fontSize: 15.sp,
          textColor: kAppTwoTextColor,
          marginDrawable: 4.w,
          endDrawable: UIImage(
            assetImage: 'icon_vip1.png',
            height: 15.w,
          ),
        ),
        const Expanded(child: UIContainer()),
        CupertinoSwitch(
            value: logic.isOpen,
            activeColor: kAppBlackColor,
            onChanged: (value) {
              logic.openChange(value);
            })
      ],
    );
  }

  buildTopView(context) {
    return UIRow(
      margin: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
      children: [
        UIImage(
          padding:
              EdgeInsets.only(top: 12.w, bottom: 12.w, left: 15.w, right: 15.w),
          assetImage: 'icon_back_black.png',
          width: 50.w,
          onTap: () {
            Get.back();
          },
        ),
        Expanded(
          child: UIRow(
            mainAxisAlignment: MainAxisAlignment.center,
            children: logic.tabList.map((e) {
              int index = logic.tabList.indexOf(e);
              return UIText(
                margin: EdgeInsets.only(left: index != 0 ? 42.w : 0),
                text: e,
                fontSize: 16.sp,
                fontWeight: index == logic.selectedIndex
                    ? FontWeight.bold
                    : FontWeight.normal,
                textColor: index == logic.selectedIndex
                    ? kAppTextColor
                    : kAppColor('#9C9C9C'),
                textAlign: TextAlign.center,
                onTap: () {
                  logic.selectIndexChange(index);
                },
              );
            }).toList(),
          ),
        ),
        SizedBox(
          width: 50.w,
        ),
      ],
    );
  }
}
