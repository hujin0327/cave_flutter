import 'package:azlistview/azlistview.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../config/whl_global_config.dart';
import '../../../model/user_model.dart';
import '../../../widgets/search_widget.dart';
import 'blacklist_logic.dart';

class BlacklistPage extends StatelessWidget {
  BlacklistPage({Key? key}) : super(key: key);

  final logic = Get.put(BlacklistLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppWhiteColor,
      appBar: WhlAppBar(
        centerTitle: "小黑屋",
        backgroundColor: kAppWhiteColor,
      ),
      body: GetBuilder<BlacklistLogic>(builder: (logic) {
        return AzListView(
          data: logic.backList,
          itemCount: logic.backList.length,
          itemBuilder: (BuildContext context, int index) {
            UserModel model = logic.backList[index];
            if (index == 0) {
              return UIContainer(
                color: kAppColor("#F6F6F6"),
                padding: EdgeInsets.all(11.w),
                child: SearchWidget(
                  logic.editingController,
                  bgColor: kAppWhiteColor,
                ),
              );
            }
            return buildItemView(model);
          },
          susItemHeight: 36,
          susItemBuilder: (BuildContext context, int index) {
            UserModel model = logic.backList[index];
            if (index == 0) return Container();
            return UIText(
              width: Get.width,
              // margin: EdgeInsets.symmetric(horizontal: 15.w),
              padding: EdgeInsets.only(left: 15.w),
              // radius: 18.w,
              height: 40.h,
              // gradientStartColor: kAppColorModel().rightColor.withOpacity(0.1),
              // gradientEndColor: kAppColorModel().rightColor.withOpacity(0.03),
              alignment: Alignment.centerLeft,
              text: model.initial ?? '',
              textColor: Colors.black,
              fontSize: 13.sp,
              fontWeight: FontWeight.w500,
            );
          },
          indexBarData: SuspensionUtil.getTagIndexList(logic.backList),
          indexBarOptions: IndexBarOptions(
            needRebuild: true,
            color: Colors.transparent,
            downDecoration: BoxDecoration(
              color: kAppColor("#8F847A").withOpacity(0.1),
              borderRadius: BorderRadius.circular(20.w),
            ),
            // localImages: [logic.imgFavorite], //local images.
          ),
        );
      }),
    );
  }

  Column buildItemView(UserModel model) {
    return Column(
      children: [
        UIRow(
          padding: EdgeInsets.symmetric(horizontal: 18.w, vertical: 10.h),
          children: [
            UIImage(
              httpImage: (model.avatar ?? "").toImageUrl(),
              assetPlaceHolder: WHLGlobalConfig.userPlaceholderAvatar,
              width: 42.w,
              height: 42.w,
              radius: 24,
              fit: BoxFit.cover,
              margin: EdgeInsets.only(right: 14.w),
            ),
            UIText(
              text: model.nickName ?? '',
              fontSize: 14.sp,
              fontWeight: FontWeight.bold,
              textColor: kAppTextColor,
            ),
            Expanded(child: Container()),
          ],
          onTap: () {},
        ),
        Container(
          margin: EdgeInsets.only(left: 70.w),
          height: 0.5.w,
          color: kAppColor('#EBEBEB'),
        )
      ],
    );
  }
}
