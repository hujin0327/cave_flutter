import 'package:azlistview/azlistview.dart';
import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/utils/task_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:lpinyin/lpinyin.dart';

import '../../../model/user_model.dart';

class BlacklistLogic extends GetxController {
  TextEditingController editingController = TextEditingController();

  List<UserModel> backList = [];

  @override
  void onInit() {
    super.onInit();
    TaskUtils.complateTask(18, type: 2);
    loadData();
  }

  loadData() {
    WhlApi.myBlackList.get({}).then((value) {
      if (value.isSuccess()) {
        Map data = value.data;
        List<UserModel> allModels = [];
        data.forEach((key, value) {
          List subData = value;
          for (var element in subData) {
            allModels.add(UserModel.fromJson(element));
          }
        });
        backList = allModels;
        _handleList(backList);
      }
    });
  }

  void _handleList(List<UserModel> list) {
    if (list.isNotEmpty) {
      for (int i = 0, length = list.length; i < length; i++) {
        try {
          String pinyin = PinyinHelper.getPinyinE((list[i].nickName ?? ''));
          String tag = pinyin.substring(0, 1).toUpperCase();
          list[i].initial = pinyin;
          if (RegExp("[A-Z]").hasMatch(tag)) {
            list[i].initial = tag;
          } else {
            list[i].initial = "#";
          }
        } catch (e) {
          list[i].initial = "#";
        }
      }
      SuspensionUtil.sortListBySuspensionTag(list);
      // show sus tag.
      SuspensionUtil.setShowSuspensionStatus(backList);
      backList.insert(0, UserModel(accountId: '↑', initial: '↑', nickName: ''));
      update();
    } else {
      backList = [];
      update();
    }
  }
}
