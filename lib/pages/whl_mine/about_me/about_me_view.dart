import 'package:cave_flutter/pages/whl_mine/feedback/feedback_view.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../network/whl_api.dart';
import '../../../routes/whl_app_pages.dart';
import '../../../style/whl_style.dart';
import '../../../widgets/brick/widget/basic_widget.dart';
import '../../../widgets/brick/widget/image_widget.dart';
import '../../../widgets/brick/widget/text_widget.dart';
import 'about_me_logic.dart';

class About_mePage extends StatelessWidget {
  About_mePage({Key? key}) : super(key: key);

  final logic = Get.put(About_meLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppWhiteColor,
      appBar: WhlAppBar(
        backgroundColor: kAppWhiteColor,
        isShowBottomLine: false,
      ),
      body: Center(
        child: UIColumn(
          children: [
            UIImage(
              margin: EdgeInsets.only(top: 60.h),
              height: 70.w,
              width: 70.w,
              radius: 10.w,
              assetImage: "icon_app_logo",
            ),
            UIText(
              margin: EdgeInsets.only(top: 16.h),
              text: "Cave",
              fontSize: 19.sp,
              textAlign: TextAlign.center,
              textColor: kAppSubTextColor,
              fontWeight: FontWeight.bold,
            ),
            GetBuilder<About_meLogic>(builder: (logic) {
              return UIText(
                margin: EdgeInsets.only(top: 8.h),
                text: "V ${logic.version}",
                fontSize: 16.sp,
                textAlign: TextAlign.center,
                textColor: kAppSubTextColor,
              );
            }),
            UIRow(
              margin: EdgeInsets.only(top: 50.h),
              height: 58.h,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              onTap: () => {Get.to(FeedbackPage())},
              children: [
                Expanded(
                    child: UIText(
                  margin: EdgeInsets.only(left: 16.w),
                  text: "意见反馈",
                  fontSize: 16.sp,
                  textAlign: TextAlign.left,
                  textColor: kAppTwoTextColor,
                )),
                UIImage(
                  margin: EdgeInsets.only(right: 16.h),
                  height: 24.w,
                  width: 24.w,
                  assetImage: "home_right.png",
                )
              ],
            ),
            UIRow(
              onTapUp: ((details) {
                // WhlApi.getAppVersion.get({});
              }),
              height: 58.h,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                    child: UIText(
                  margin: EdgeInsets.only(left: 16.w),
                  text: "版本更新",
                  fontSize: 16.sp,
                  textAlign: TextAlign.left,
                  textColor: kAppTwoTextColor,
                )),
                UIImage(
                  margin: EdgeInsets.only(right: 16.h),
                  height: 24.w,
                  width: 24.w,
                  assetImage: "home_right.png",
                )
              ],
              onTap: () {},
            ),
            Expanded(child: UIContainer()),
            Text.rich(
                textAlign: TextAlign.center,
                TextSpan(
                    text: "《Cave服务协议》",
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        Get.toNamed(Routes.webView, arguments: {
                          'webUrl': WhlApi.termConditions,
                          'title': 'Cave服务协议'
                        });
                      },
                    style: TextStyle(
                        color: kAppColor("#547C99"),
                        fontSize: 11.sp,
                        height: 2),
                    children: [
                      TextSpan(
                          text: "《Cave隐私协议》",
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              Get.toNamed(Routes.webView, arguments: {
                                'webUrl': WhlApi.privacyPolicy,
                                'title': 'Cave隐私协议'
                              });
                            },
                          style: TextStyle(
                              color: kAppColor("#547C99"),
                              fontSize: 11.sp,
                              height: 2)),
                      TextSpan(
                          text: "\n《Cave用户行为规范》",
                          style: TextStyle(
                              color: kAppColor("#547C99"),
                              fontSize: 11.sp,
                              height: 2))
                    ])),
            UIContainer(
              margin: EdgeInsets.only(bottom: 34.h, top: 5.h),
              child: Text(
                "官方合作伙伴\n网易（杭州）网络有限公司\n阿里云计算有限公司\n杭州润达科技有限公司\ncopyrigh2019-2024",
                style: TextStyle(
                    color: kAppSub3TextColor, fontSize: 11.sp, height: 2),
                maxLines: 5,
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
