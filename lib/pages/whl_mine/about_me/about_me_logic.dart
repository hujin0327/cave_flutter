import 'package:get/get.dart';
import 'package:package_info_plus/package_info_plus.dart';

class About_meLogic extends GetxController {

  String version = "";

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();

    getVersion();
  }

  getVersion() async {
    PackageInfo pi = await PackageInfo.fromPlatform();
    version = pi.version;
    update();
  }
}
