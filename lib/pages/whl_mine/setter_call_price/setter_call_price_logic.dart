import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class Setter_call_priceLogic extends GetxController {
  TextEditingController videoEditingController = TextEditingController();
  TextEditingController audioEditingController = TextEditingController();

  int audioMin = 20;
  int audioMax = 200;
  int videoMin = 50;
  int videoMax = 300;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    loadPriceData();
  }

  loadPriceData(){
    WhlApi.myConnectionPrice.get({}).then((value) {
      if (value.isSuccess()){
        if (value.data != null) {
          int videoConnection = value.data["videoConnection"];
          int voiceConnection = value.data["voiceConnection"];
          audioEditingController.text = '$voiceConnection';
          videoEditingController.text = '$videoConnection';
        }
      }
    });
  }

  void onConfirm() {
    if (audioEditingController.text.isEmpty) {
      MyToast.show("请输入语音连线价格");
      return;
    }
    if (videoEditingController.text.isEmpty) {
      MyToast.show("请输入视频连线价格");
      return;
    }
    int audioPrice = int.parse(audioEditingController.text);
    int videoPrice = int.parse(videoEditingController.text);
    if (audioPrice < audioMin || audioPrice > audioMax) {
      MyToast.show("语音连线价格超出可设置范围");
      return;
    }
    if (videoPrice < videoMin || videoPrice > videoMax) {
      MyToast.show("视频连线价格超出可设置范围");
      return;
    }
    setPriceRequest();
  }

  setPriceRequest() {
    int audioPrice = int.parse(audioEditingController.text);
    int videoPrice = int.parse(videoEditingController.text);
    WhlApi.connectionPrice.post(
        {"videoConnection": videoPrice, "voiceConnection": audioPrice},
        withLoading: true).then((value) {
      if (value.isSuccess()) {
        MyToast.show("价格设置成功");
      }
    });
  }
}
