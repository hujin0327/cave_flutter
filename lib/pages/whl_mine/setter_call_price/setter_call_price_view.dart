import 'package:cave_flutter/common/common_widget.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'setter_call_price_logic.dart';

class Setter_call_pricePage extends StatelessWidget {
  Setter_call_pricePage({Key? key}) : super(key: key);

  final logic = Get.put(Setter_call_priceLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppWhiteColor,
      appBar: WhlAppBar(
        backgroundColor: kAppWhiteColor,
        centerTitle: "视频语音连线",
      ),
      body: UIColumn(
        children: [
          buildItemWidget("语音",logic.audioEditingController,logic.audioMin,logic.audioMax),
          buildItemWidget("视频",logic.videoEditingController,logic.videoMin,logic.videoMax),
          Expanded(child: UIContainer()),
          UIText(
            margin: EdgeInsets.symmetric(horizontal: 20.w, vertical: 60.h),
            text: "确定",
            textColor: kAppWhiteColor,
            fontSize: 16.sp,
            fontWeight: FontWeight.bold,
            color: kAppBlackColor,
            height: 52.h,
            radius: 26.h,
            alignment: Alignment.center,
            onTap: () {
              logic.onConfirm();
            },
          )
        ],
      ),
    );
  }

  buildItemWidget(String title, TextEditingController controller,int min,int max) {
    return UIRow(
      crossAxisAlignment: CrossAxisAlignment.start,
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      margin: EdgeInsets.only(top: 22.h),
      children: [
        UIText(
          margin: EdgeInsets.only(top: 20.h),
          text: title,
          textColor: kAppSubTextColor,
          fontSize: 13.sp,
          fontWeight: FontWeight.bold,
        ),
        Expanded(
          child: UIColumn(
            crossAxisAlignment: CrossAxisAlignment.start,
            margin: EdgeInsets.only(left: 14.w,right: 11.w),
            children: [
              UIContainer(
                height: 54.h,
                radius: 18.h,
                color: kAppColor("#F7F7F9"),
                child: noBorderCTextField(
                    controller: controller,
                    contentPadding: EdgeInsets.symmetric(vertical: 20.h,horizontal: 10.w),
                    fontSize: 14.sp,
                    // maxLength: 3,
                    keyboardType: TextInputType.number,
                    inputFormatters: [FilteringTextInputFormatter(RegExp("^[0-9]+"), allow: true)],
                    hint: "请输入"),
              ),
              UIText(
                margin: EdgeInsets.only(top: 10.h,left: 10.w),
                text: "最低${min}C币-最高${max}C币",
                textColor: kAppSub3TextColor,
                fontSize: 11.sp,
              ),
            ],
          ),
        ),
        UIText(
          margin: EdgeInsets.only(top: 20.h),
          text: "C币/min",
          textColor: kAppSubTextColor,
          fontSize: 11.sp,
        ),
      ],
    );
  }
}
