import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cave_flutter/common/common_widget.dart';
import 'package:cave_flutter/common/upload_photo_widget.dart';
import 'package:cave_flutter/model/dynamic_model.dart';
import 'package:cave_flutter/model/image_model.dart';
import 'package:cave_flutter/model/whl_user_info_model.dart';
import 'package:cave_flutter/pages/whl_home/whl_publish/push_top_setting/push_top_setting_sheet.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:cave_flutter/utils/whl_dialog_util.dart';
import 'package:cave_flutter/whl_app.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:extended_nested_scroll_view/extended_nested_scroll_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';

import '../../../common/common_action.dart';
import '../../../common/sliver_header_delegate.dart';
import '../../../routes/whl_app_pages.dart';
import '../../../utils/whl_toast_utils.dart';
import '../../../utils/whl_user_utils.dart';
import '../../../widgets/refresh_widget.dart';
import '../../square/comment/comment_view.dart';
import '../../square/square_more_widget.dart';
import 'add_friend_dialog/add_friend_dialog_view.dart';
import 'user_info_logic.dart';

class UserInfoPage extends StatelessWidget {
  UserInfoPage({Key? key}) : super(key: key);

  final logic = Get.put(UserInfoLogic());

  @override
  Widget build(BuildContext context) {
    final double statusBarHeight = MediaQuery.of(context).padding.top;
    // final double pinnedHeaderHeight = statusBarHeight + kToolbarHeight;
    // WhlUserInfoModel myUserInfo = WhlUserUtils.getUserInfo();

    return MediaQuery.removePadding(
      removeTop: true,
      removeBottom: true,
      context: context,
      child: Scaffold(
        backgroundColor: kAppColor('#323232'),
        body: GetBuilder<UserInfoLogic>(builder: (logic) {
          String? backgroundImageUrl = logic.userModel?.avatar?.toImageUrl();
          if ((logic.userModel?.albumList ?? []).isNotEmpty) {
            ImageModel model = logic.userModel!.albumList!.first;
            backgroundImageUrl = model.imageUrl?.toImageUrl();
          }

          String? vipPath =
              getVipIcon(logic.userModel?.vip, logic.userModel?.rechargeType);
          return Stack(
            children: [
              UIImage(
                httpImage: backgroundImageUrl,
                width: double.infinity,
                height: 733.h,
                fit: BoxFit.fitHeight,
              ),
              MyRefresh(
                controller: logic.refreshController,
                onLoad: logic.selectedIndex == 1
                    ? () {
                        logic.getUserDynamicsList();
                      }
                    : null,
                childBuilder: (context, physics) {
                  return CustomScrollView(
                    controller: logic.scrollController,
                    physics: physics,
                    slivers: [
                      SliverAppBar(
                        pinned: true,
                        expandedHeight: ScreenUtil().statusBarHeight,
                        floating: false,
                      ),
                      SliverAppBar(
                        pinned: true,
                        expandedHeight: 0,
                        floating: false,
                        // backgroundColor: Colors.transparent,
                        flexibleSpace: FlexibleSpaceBar(
                          // collapseMode: CollapseMode.pin,
                          background: Visibility(
                            visible: logic.isShowBar,
                            child: UIRow(
                              padding: EdgeInsets.only(
                                left: 18.w,
                              ),
                              crossAxisAlignment: CrossAxisAlignment.center,
                              // mainAxisAlignment: MainAxisAlignment.start,
                              color: kAppColor('#252525').withOpacity(0.21),
                              height: 40.h,
                              children: [
                                UIContainer(
                                  child: const Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.white,
                                  ),
                                  onTap: () {
                                    Get.back();
                                  },
                                ),
                                UIImage(
                                  httpImage:
                                      logic.userModel?.avatar?.toImageUrl(),
                                  assetPlaceHolder: 'icon_app_logo.png',
                                  width: 30.w,
                                  height: 30.w,
                                  radius: 30.w,
                                  margin: EdgeInsets.only(right: 4.w),
                                ),
                                UIText(
                                  text:
                                      '${logic.userModel?.nickName ?? ""} · ${logic.userModel?.age ?? 0}岁 · ${logic.userModel?.sexualOrientation ?? "未知"} · ${logic.userModel?.funRole ?? "未知"} · ${logic.isMine ? "" : "0.0km"}',
                                  textColor: Colors.white,
                                  fontSize: 12.sp,
                                  // fontWeight: FontWeight.bold,
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      SliverPersistentHeader(
                        delegate: MySliverHeaderDelegate(
                          maxHeight: 585.h,
                          minHeight: 585.h,
                          child: Stack(
                            alignment: Alignment.topCenter,
                            children: [
                              Column(
                                children: [
                                  Container(height: 284.h),
                                  GetBuilder<UserInfoLogic>(builder: (logic) {
                                    return Stack(
                                      clipBehavior: Clip.none,
                                      children: [
                                        UIContainer(
                                          margin: EdgeInsets.symmetric(
                                              horizontal: 15.w),
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(20.w),
                                            child: BackdropFilter(
                                              filter: ImageFilter.blur(
                                                  sigmaY: 44, sigmaX: 44),
                                              child: Stack(
                                                clipBehavior: Clip.none,
                                                children: [
                                                  buildFollow(),
                                                  UIColumn(
                                                    margin: EdgeInsets.only(
                                                      top: 60.w,
                                                      left: 15.w,
                                                    ),
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Row(
                                                        children: [
                                                          UIText(
                                                            text: logic
                                                                .userModel
                                                                ?.nickName,
                                                            textColor:
                                                                Colors.white,
                                                            fontSize: 22.sp,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                          ),
                                                          if (logic.userModel
                                                                  ?.levelImage !=
                                                              null)
                                                            UIImage(
                                                              disabledHttpBigImage:
                                                                  true,
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      left:
                                                                          4.w),
                                                              httpImage: logic
                                                                  .userModel
                                                                  ?.levelImage
                                                                  ?.toImageUrl(),
                                                              width: 30.w,
                                                            ),
                                                          if (vipPath != null)
                                                            UIImage(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      left:
                                                                          4.w),
                                                              assetImage:
                                                                  vipPath,
                                                              width: 51.w,
                                                            )
                                                        ],
                                                      ),
                                                      UIRow(
                                                        margin: EdgeInsets.only(
                                                            top: 4.w),
                                                        children: [
                                                          UIText(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 2.w,
                                                                    right: 5.w),
                                                            text:
                                                                'ID:${logic.accountId}',
                                                            textColor:
                                                                Colors.white,
                                                            fontSize: 10.sp,
                                                          ),
                                                          if (logic.hide_wealth)
                                                            UIText(
                                                              padding: EdgeInsets
                                                                  .symmetric(
                                                                      horizontal:
                                                                          6.w,
                                                                      vertical:
                                                                          2.w),
                                                              color: kAppColor(
                                                                      '#6A6000')
                                                                  .withOpacity(
                                                                      0.21),
                                                              text:
                                                                  '财富值${logic.userModel?.wealth ?? 0}',
                                                              textColor:
                                                                  kAppColor(
                                                                      '#FFE800'),
                                                              fontSize: 11.sp,
                                                              radius: 6.w,
                                                            ),
                                                          if (logic.hide_charm)
                                                            UIText(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      left:
                                                                          5.w),
                                                              padding: EdgeInsets
                                                                  .symmetric(
                                                                      horizontal:
                                                                          6.w,
                                                                      vertical:
                                                                          2.w),
                                                              color: kAppColor(
                                                                      '#7F2020')
                                                                  .withOpacity(
                                                                      0.24),
                                                              text:
                                                                  '魅力值${logic.userModel?.charm ?? 0}',
                                                              textColor:
                                                                  kAppColor(
                                                                      '#FF73A0'),
                                                              fontSize: 11.sp,
                                                              radius: 6.w,
                                                            ),
                                                        ],
                                                      ),
                                                      UIRow(
                                                        margin: EdgeInsets.only(
                                                            top: 16.w,
                                                            bottom: 14.w),
                                                        children: [
                                                          UIText(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    right: 5.w),
                                                            padding:
                                                                EdgeInsets.only(
                                                                    left: 8.w,
                                                                    right: 7.w,
                                                                    top: 3.w,
                                                                    bottom:
                                                                        3.w),
                                                            alignment: Alignment
                                                                .center,
                                                            text:
                                                                '${logic.userModel?.age ?? 0}岁',
                                                            color: kAppColor(
                                                                    '#A06075')
                                                                .withOpacity(
                                                                    0.16),
                                                            textColor:
                                                                getGenderColor(logic
                                                                    .userModel
                                                                    ?.gender),
                                                            fontSize: 10.sp,
                                                            strokeColor:
                                                                getGenderColor(logic
                                                                    .userModel
                                                                    ?.gender),
                                                            strokeWidth: 1.w,
                                                            radius: 18.w,
                                                            marginDrawable: 3.w,
                                                            startDrawable:
                                                                UIImage(
                                                              assetImage:
                                                                  getGenderIcon(logic
                                                                      .userModel
                                                                      ?.gender),
                                                              height: 10.w,
                                                            ),
                                                          ),
                                                          RichText(
                                                              text: TextSpan(
                                                                  text: '/ ',
                                                                  style:
                                                                      TextStyle(
                                                                    color: kAppColor(
                                                                        '#979797'),
                                                                    fontSize:
                                                                        10.sp,
                                                                  ),
                                                                  children: [
                                                                TextSpan(
                                                                  text: logic
                                                                          .userModel
                                                                          ?.sexualOrientation ??
                                                                      '未知',
                                                                  style:
                                                                      TextStyle(
                                                                    color: kAppColor(
                                                                            '#D9D9D9')
                                                                        .withOpacity(
                                                                            0.7),
                                                                    fontSize:
                                                                        10.sp,
                                                                  ),
                                                                ),
                                                                TextSpan(
                                                                  text: ' / ',
                                                                  style:
                                                                      TextStyle(
                                                                    color: kAppColor(
                                                                        '#979797'),
                                                                    fontSize:
                                                                        10.sp,
                                                                  ),
                                                                ),
                                                                TextSpan(
                                                                  text: logic
                                                                          .userModel
                                                                          ?.funRole ??
                                                                      "未知",
                                                                  style:
                                                                      TextStyle(
                                                                    color: kAppColor(
                                                                            '#D9D9D9')
                                                                        .withOpacity(
                                                                            0.7),
                                                                    fontSize:
                                                                        10.sp,
                                                                  ),
                                                                ),
                                                              ])),
                                                          UIText(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 8.w),
                                                            padding: EdgeInsets
                                                                .symmetric(
                                                                    horizontal:
                                                                        6.w,
                                                                    vertical:
                                                                        5.w),
                                                            radius: 6.w,
                                                            color: Colors.white
                                                                .withOpacity(
                                                                    0.25),
                                                            text: '查看安全词',
                                                            textColor:
                                                                Colors.white,
                                                            fontSize: 8.sp,
                                                          )
                                                        ],
                                                      ),
                                                      UIRow(
                                                        margin: EdgeInsets.only(
                                                          right: 16.w,
                                                        ),
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          UIText(
                                                            text:
                                                                '${logic.hide_location ? '未知' : logic.userModel?.cityName ?? '未知'} · ${logic.hide_online_time == true ? '未知' : logic.userModel?.onlineStatus == true ? '在线' : '离线'} · ${logic.hide_distance ? '未知' : logic.userModel?.distance ?? '未知'}km',
                                                            textColor:
                                                                Colors.white,
                                                            fontSize: 12.sp,
                                                            marginDrawable: 5.w,
                                                            startDrawable:
                                                                UIImage(
                                                              assetImage:
                                                                  'icon_location_white.png',
                                                              width: 12.w,
                                                            ),
                                                          ),
                                                          UIText(
                                                            text:
                                                                '加入Cave${logic.userModel?.joinCaveDays ?? 0}天',
                                                            textColor: kAppColor(
                                                                    '#D9D9D9')
                                                                .withOpacity(
                                                              0.7,
                                                            ),
                                                            fontSize: 10.sp,
                                                          )
                                                        ],
                                                      ),
                                                      buildMedalInfo(),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                        Positioned(
                                          left: 30.w,
                                          top: -24.h,
                                          child: UIImage(
                                            httpImage: logic.userModel?.avatar
                                                ?.toImageUrl(),
                                            assetPlaceHolder:
                                                'icon_app_logo.png',
                                            width: 76.w,
                                            height: 76.w,
                                            radius: 76.w,
                                          ),
                                        )
                                      ],
                                    );
                                  }),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      SliverPersistentHeader(
                        pinned: true,
                        delegate: MySliverHeaderDelegate(
                          maxHeight: 50.w,
                          minHeight: 50.w,
                          child: Container(
                            color: kAppColor('#252525'),
                            padding: EdgeInsets.symmetric(vertical: 10.w),
                            child: TabBar(
                              controller: logic.tabController,
                              labelColor: Colors.white,
                              labelStyle: TextStyle(
                                  fontSize: 16.sp,
                                  color: kAppColor('#151718'),
                                  fontWeight: FontWeight.bold),
                              unselectedLabelStyle: TextStyle(
                                  fontSize: 14.sp,
                                  color: Colors.white.withOpacity(0.7)),
                              indicatorColor: Colors.white,
                              indicatorSize: TabBarIndicatorSize.label,
                              indicatorWeight: 2.0,
                              isScrollable: false,
                              unselectedLabelColor:
                                  Colors.white.withOpacity(0.7),
                              tabs: const <Tab>[
                                Tab(text: '资料'),
                                Tab(text: '动态'),
                                Tab(text: '相册')
                              ],
                            ),
                          ),
                        ),
                      ),
                      buildContextWidget(),
                      SliverToBoxAdapter(
                        child: Container(
                            height: 110.w, color: kAppColor('#323232')),
                      )
                    ],
                  );
                },
              ),
              GetBuilder<UserInfoLogic>(builder: (logic) {
                return Visibility(
                  visible: !logic.isScrolling && !logic.isMine,
                  child: Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: ClipRRect(
                      child: BackdropFilter(
                        filter: ImageFilter.blur(sigmaY: 44, sigmaX: 44),
                        child: UIRow(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          padding: EdgeInsets.only(
                              left: 46.w, right: 46.w, top: 24.w),
                          height: 110.w,
                          children: [
                            Expanded(
                              child: UIStrokeButton(
                                textColor: Colors.white,
                                strokeColor: Colors.white,
                                text: '关注',
                                onTap: () {
                                  logic.attentionRequest();
                                },
                              ),
                            ),
                            SizedBox(width: 23.w),
                            Expanded(
                              child: UISolidButton(
                                text: '+ 加好友',
                                color: kAppColor('#FFF754'),
                                textColor: kAppTextColor,
                                onTap: () {
                                  print('加好友参数--------');
                                  print(logic.userModel?.id.toString());
                                  Get.dialog(AddFriendDialog(),
                                      arguments: logic.userModel);
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              }),
            ],
          );
        }),
      ),
    );
  }

  Widget buildFollow() {
    return UIColumn(
      color: kAppColor('#A1A1A1').withOpacity(0.12),
      width: double.infinity,
      height: 250.w,
      children: [
        UIRow(
          margin: EdgeInsets.only(right: 25.w, top: 12.w),
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            UIText(
              text: '关注',
              textColor: Colors.white.withOpacity(0.68),
              fontSize: 11.sp,
              marginDrawable: 5.w,
              topDrawable: UIText(
                text: logic.hide_follow
                    ? '-'
                    : logic.userModel?.followNum.toString(),
                textColor: Colors.white,
                fontSize: 17.sp,
                fontWeight: FontWeight.w500,
              ),
            ),
            UIText(
              margin: EdgeInsets.symmetric(horizontal: 20.w),
              text: '粉丝',
              textColor: Colors.white.withOpacity(0.68),
              fontSize: 11.sp,
              marginDrawable: 5.w,
              topDrawable: UIText(
                text:
                    logic.hide_fans ? '-' : logic.userModel?.fansNum.toString(),
                textColor: Colors.white,
                fontSize: 17.sp,
                fontWeight: FontWeight.w500,
              ),
            ),
            UIText(
              text: 'Cave',
              textColor: Colors.white.withOpacity(0.68),
              fontSize: 11.sp,
              marginDrawable: 5.w,
              topDrawable: UIText(
                text:
                    logic.hide_cave ? '-' : logic.userModel?.caveNum.toString(),
                textColor: Colors.white,
                fontSize: 17.sp,
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        )
      ],
    );
  }

  Widget buildMedalInfo() {
    if (logic.unLockMedal.isEmpty && logic.isMine) {
      return UIContainer(
        margin: EdgeInsets.only(top: 12.h),
        padding: EdgeInsets.symmetric(
          vertical: 18.h,
          horizontal: 17.w,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8.r),
          color: kAppColorOpacity(
            '#F6F7F9',
            0.18,
          ),
        ),
        child: UIRow(
          onTap: () {
            logic.toUnLockMedal();
          },
          mainAxisSize: MainAxisSize.min,
          children: [
            UIImage(
              assetImage: 'icon_medal.png',
              width: 24.w,
              height: 24.h,
              imageColor: kAppWhiteColor,
              padding: EdgeInsets.only(
                right: 8.w,
              ),
            ),
            UIText(
              text: '您有未解锁的勋章',
              textColor: kAppWhiteColor,
            ),
          ],
        ),
      );
    } else {
      return SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: UIRow(
          margin: EdgeInsets.only(
            top: 7.w,
            bottom: 7.w,
          ),
          children: logic.unLockMedal
              .map(
                (e) => UIImage(
                  width: 57.w,
                  height: 57.h,
                  httpImage: e.imageUrl?.toImageUrl(),
                ),
              )
              .toList(),
        ),
      );
    }
  }

  Widget buildContextWidget() {
    return GetBuilder<UserInfoLogic>(
      builder: (logic) {
        if (logic.selectedIndex == 1) {
          return SliverList(
            delegate: SliverChildBuilderDelegate(
              (context, index) {
                if (logic.listData.isEmpty) {
                  return UIContainer(
                    // margin: EdgeInsets.symmetric(horizontal: 15.w),
                    decoration: BoxDecoration(
                      // color: kAppColorOpacity('#F6F7F9', 0.18),
                      color: kAppColor('#323232'),
                    ),
                    child: UIRow(
                      padding:
                          EdgeInsets.symmetric(horizontal: 14.w, vertical: 15.h)
                              .copyWith(
                        left: 21.w,
                      ),
                      margin: EdgeInsets.symmetric(
                        horizontal: 15.w,
                        vertical: 15.h,
                      ),
                      decoration: BoxDecoration(
                        color: kAppColorOpacity('#F6F7F9', 0.18),
                        borderRadius: BorderRadius.circular(8.r),
                      ),
                      children: [
                        Expanded(
                          child: UIColumn(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              UIText(
                                text: '你还没有发布动态哦~',
                                textColor: kAppWhiteColor,
                                fontSize: 16.sp,
                                margin: EdgeInsets.only(bottom: 8.h),
                              ),
                              UIText(
                                text: '记录你的精彩生活，与有趣的人分享~',
                                textColor: kAppColor('#D5D5D5'),
                                fontSize: 13.sp,
                              ),
                            ],
                          ),
                        ),
                        UIContainer(
                          onTap: () {
                            logic.toPublish();
                          },
                          width: 82.w,
                          height: 37.h,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(26.r),
                            color: kAppColor('#9F9F9F'),
                          ),
                          child: UIRow(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              UIImage(
                                assetImage: 'icon_publish.png',
                                width: 16.w,
                                height: 16.w,
                                imageColor: kAppColor('#E3E3E3'),
                              ),
                              UIText(
                                text: '去发布',
                                textColor: kAppColor('#E3E3E3'),
                                strokeColor: kAppColor('#E3E3E3'),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  );
                }
                DynamicModel model = logic.listData[index];
                return buildDynamicItem(model);
              },
              childCount: logic.listData.length,
            ),
          );
        } else if (logic.selectedIndex == 0) {
          List<dynamic>? _receiveGift = logic.userModel?.giftList
              ?.where((g) => g.receive == true)
              .toList();
          int giftCount = logic.userModel?.giftList?.length ?? 0;
          int receiveGiftCount = _receiveGift?.length ?? 0;
          return SliverList(
              delegate: SliverChildListDelegate([
            SingleChildScrollView(
              physics: const NeverScrollableScrollPhysics(),
              child: UIColumn(
                color: kAppColor('#323232'),
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 15.w)
                        .copyWith(top: 15.h),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20.w),
                      child: BackdropFilter(
                        filter: ImageFilter.blur(sigmaY: 44, sigmaX: 44),
                        child: UIColumn(
                          radius: 20.w,
                          color: kAppColor('#E9E9E9').withOpacity(0.12),
                          // margin: EdgeInsets.symmetric(horizontal: 15.w),
                          width: double.infinity,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            UIRow(
                              margin: EdgeInsets.only(left: 14.w, top: 20.w),
                              children: [
                                UIText(
                                  text: '基础资料',
                                  textColor: kAppColor('#E9E9E9'),
                                  fontSize: 17.sp,
                                  fontWeight: FontWeight.bold,
                                ),
                                UIContainer(
                                  onTap: () {
                                    logic.toEditUserPage();
                                  },
                                  margin: EdgeInsets.only(left: 18.w),
                                  decoration: BoxDecoration(
                                    color: kAppColorOpacity('F6F7F9', 0.18),
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 7.w,
                                    vertical: 5.h,
                                  ),
                                  child: UIRow(
                                    children: [
                                      UIImage(
                                        assetImage: 'icon_edit.png',
                                        width: 13.w,
                                        height: 13.w,
                                        imageColor: kAppWhiteColor,
                                      ),
                                      UIText(
                                        text: ' 完善个人资料',
                                        fontSize: 9.sp,
                                        textColor: kAppWhiteColor,
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                            UIWrap(
                              runAlignment: WrapAlignment.start,
                              wrapAlignment: WrapAlignment.start,
                              crossAxisAlignment: WrapCrossAlignment.start,
                              margin: EdgeInsets.symmetric(
                                  horizontal: 14.w, vertical: 18.w),
                              runSpacing: 7.w,
                              spacing: 8.w,
                              children:
                                  (logic.userModel?.baseInfo ?? []).map((e) {
                                return e != null
                                    ? UIText(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 15.w, vertical: 7.w),
                                        text: e,
                                        textColor: Colors.white,
                                        fontSize: 12.sp,
                                        strokeColor: Colors.white,
                                        strokeWidth: 1.w,
                                        radius: 30.w,
                                      )
                                    : Container();
                              }).toList(),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin:
                        EdgeInsets.symmetric(horizontal: 15.w, vertical: 20.w),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20.w),
                      child: BackdropFilter(
                        filter: ImageFilter.blur(sigmaY: 44, sigmaX: 44),
                        child: UIColumn(
                          radius: 20.w,
                          color: kAppColor('#E9E9E9').withOpacity(0.12),
                          // margin: EdgeInsets.symmetric(horizontal: 15.w),
                          width: double.infinity,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            UIRow(
                              margin: EdgeInsets.only(left: 14.w, top: 20.w),
                              children: [
                                UIText(
                                  text: '关于我',
                                  textColor: kAppColor('#E9E9E9'),
                                  fontSize: 17.sp,
                                  fontWeight: FontWeight.bold,
                                ),
                                UIContainer(
                                  onTap: () {
                                    logic.toEditDesc();
                                  },
                                  margin: EdgeInsets.only(left: 18.w),
                                  decoration: BoxDecoration(
                                    color: kAppColorOpacity('F6F7F9', 0.18),
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 7.w,
                                    vertical: 5.h,
                                  ),
                                  child: UIRow(
                                    children: [
                                      UIImage(
                                        assetImage: 'icon_edit.png',
                                        width: 13.w,
                                        height: 13.w,
                                        imageColor: kAppWhiteColor,
                                      ),
                                      UIText(
                                        text: ' 自我介绍一下，展示你自己',
                                        fontSize: 9.sp,
                                        textColor: kAppWhiteColor,
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                            UIText(
                              margin: EdgeInsets.only(top: 18.w, bottom: 40.w),
                              padding: EdgeInsets.symmetric(horizontal: 15.w),
                              text: logic.userModel?.aboutMe ?? "",
                              textColor: kAppColor('#D5D5D5'),
                              shrinkWrap: false,
                              fontSize: 13.sp,
                              fontWeight: FontWeight.bold,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 15.w),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20.w),
                      child: BackdropFilter(
                        filter: ImageFilter.blur(sigmaY: 44, sigmaX: 44),
                        child: UIColumn(
                          radius: 20.w,
                          color: kAppColor('#E9E9E9').withOpacity(0.12),
                          // margin: EdgeInsets.symmetric(horizontal: 15.w),
                          width: double.infinity,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            UIRow(
                              margin: EdgeInsets.only(left: 14.w, top: 20.w),
                              children: [
                                UIText(
                                  text: '礼物展馆',
                                  textColor: kAppColor('#E9E9E9'),
                                  fontSize: 17.sp,
                                  fontWeight: FontWeight.bold,
                                  marginDrawable: 5.w,
                                  endDrawable: UIText(
                                    text: '已点亮$receiveGiftCount/$giftCount',
                                    textColor: kAppColor('#A7A7A7'),
                                    fontSize: 10.sp,
                                  ),
                                ),
                                UIContainer(
                                  onTap: () {
                                    logic.toGiftWallPage();
                                  },
                                  margin: EdgeInsets.only(left: 12.w),
                                  decoration: BoxDecoration(
                                    color: kAppColorOpacity('F6F7F9', 0.18),
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 7.w,
                                    vertical: 5.h,
                                  ),
                                  child: UIRow(
                                    children: [
                                      UIImage(
                                        assetImage: 'icon_chat_gift.png',
                                        width: 15.w,
                                        height: 15.w,
                                        imageColor: kAppWhiteColor,
                                      ),
                                      UIText(
                                        text: '你有未点亮礼物',
                                        fontSize: 9.sp,
                                        textColor: kAppWhiteColor,
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 16.w),
                              child: SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: UIRow(
                                  margin: EdgeInsets.symmetric(vertical: 15.w),
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    for (int i = 0; i < giftCount; i++)
                                      UIImage(
                                        assetImage: logic.userModel
                                                ?.giftList?[i]?.imageUrl ??
                                            'icon_gift_0.png',
                                        width: 54.w,
                                      )
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin:
                        EdgeInsets.symmetric(horizontal: 15.w, vertical: 24.w),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20.w),
                      child: BackdropFilter(
                        filter: ImageFilter.blur(sigmaY: 44, sigmaX: 44),
                        child: UIColumn(
                          radius: 20.w,
                          color: kAppColor('#E9E9E9').withOpacity(0.12),
                          // margin: EdgeInsets.symmetric(horizontal: 15.w),
                          width: double.infinity,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            UIRow(
                              margin: EdgeInsets.only(left: 14.w, top: 20.w),
                              children: [
                                UIText(
                                  text: '灵魂标签',
                                  textColor: kAppColor('#E9E9E9'),
                                  fontSize: 17.sp,
                                  fontWeight: FontWeight.bold,
                                ),
                                UIContainer(
                                  onTap: () {
                                    logic.toEditSoulTag();
                                  },
                                  margin: EdgeInsets.only(left: 12.w),
                                  decoration: BoxDecoration(
                                    color: kAppColorOpacity('F6F7F9', 0.18),
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 7.w,
                                    vertical: 5.h,
                                  ),
                                  child: UIRow(
                                    children: [
                                      Icon(
                                        Icons.add_circle_outline,
                                        size: 12.sp,
                                        color: kAppWhiteColor,
                                      ),
                                      UIText(
                                        text: ' 添加标签，让别人了解你',
                                        fontSize: 9.sp,
                                        textColor: kAppWhiteColor,
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                            UIWrap(
                              runAlignment: WrapAlignment.start,
                              wrapAlignment: WrapAlignment.start,
                              crossAxisAlignment: WrapCrossAlignment.start,
                              margin: EdgeInsets.symmetric(
                                  horizontal: 14.w, vertical: 18.w),
                              runSpacing: 7.w,
                              spacing: 8.w,
                              children:
                                  (logic.userModel?.soulTagList ?? []).map((e) {
                                return UIText(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 15.w, vertical: 7.w),
                                  text: e.label,
                                  textColor: Colors.white,
                                  fontSize: 12.sp,
                                  strokeColor: Colors.white,
                                  strokeWidth: 1.w,
                                  radius: 30.w,
                                );
                              }).toList(),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ]));
        } else {
          return SliverList(
            delegate: SliverChildListDelegate(
              [
                logic.userModel?.albumList?.isEmpty == true
                    ? UIContainer(
                        // margin: EdgeInsets.symmetric(horizontal: 15.w),
                        decoration: BoxDecoration(
                          // color: kAppColorOpacity('#F6F7F9', 0.18),
                          color: kAppColor('#323232'),
                        ),
                        child: UIRow(
                          padding:
                              EdgeInsets.symmetric(horizontal: 14.w).copyWith(
                            left: 21.w,
                          ),
                          margin: EdgeInsets.symmetric(
                            horizontal: 15.w,
                            vertical: 15.h,
                          ),
                          decoration: BoxDecoration(
                            color: kAppColorOpacity('#F6F7F9', 0.18),
                            borderRadius: BorderRadius.circular(8.r),
                          ),
                          children: [
                            Expanded(
                              child: UIColumn(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  UIText(
                                    text: '你还没有展示图片哦~',
                                    textColor: kAppWhiteColor,
                                    fontSize: 16.sp,
                                    margin: EdgeInsets.only(bottom: 8.h),
                                  ),
                                  UIText(
                                    text: '上传图片到相册，提高交友成功率！',
                                    textColor: kAppColor('#D5D5D5'),
                                    fontSize: 13.sp,
                                  ),
                                ],
                              ),
                            ),
                            UploadPhotoWidget(
                              columCount: 3,
                              photoData: logic.userModel?.albumList,
                              onlyAdd: true,
                              addButtonBuilder: (context) {
                                return UIText(
                                  margin: EdgeInsets.symmetric(vertical: 14.w),
                                  width: 62.w,
                                  height: 62.h,
                                  radius: 13.w,
                                  color: kAppColorOpacity("#9F9F9F", 0.33),
                                  text: "+",
                                  textColor: kAppColor('#BABABA'),
                                  fontSize: 35.sp,
                                  alignment: Alignment.center,
                                );
                              },
                              photoChangeCallBack: (value) {
                                logic.bindPhoto(value);
                              },
                              photoDeleteCallBack: (deleteModel) {
                                // logic.bindPhoto(deleteModel);
                              },
                            )
                          ],
                        ),
                      )
                    : Container(
                        color: kAppColor('#323232'),
                        child: SingleChildScrollView(
                          physics: ClampingScrollPhysics(),
                          child: Container(
                            padding: EdgeInsets.all(10),
                            child: MasonryGridView.count(
                              // 展示几列
                              crossAxisCount: 2,
                              // 元素总个数
                              itemCount: logic.userModel != null
                                  ? logic.userModel!.albumList?.length
                                  : 0,
                              // 单个子元素
                              itemBuilder: (BuildContext context, int index) {
                                ImageModel? imageModel =
                                    logic.userModel!.albumList?[index];
                                return ClipRRect(
                                  borderRadius: BorderRadius.circular(22.w),
                                  child: CachedNetworkImage(
                                    imageUrl: (imageModel?.imageUrl ?? "")
                                        .toImageUrl(),
                                    fit: BoxFit.fitWidth,
                                  ),
                                );
                              },
                              // 纵向元素间距
                              mainAxisSpacing: 10,
                              // 横向元素间距
                              crossAxisSpacing: 10,
                              //本身不滚动，让外面的singlescrollview来滚动
                              physics: const NeverScrollableScrollPhysics(),
                              shrinkWrap: true, //收缩，让元素宽度自适应
                            ),
                          ),
                        ),
                      ),
              ],
            ),
          );
        }
      },
    );
  }

  TabBar buildTabBarView(UserInfoLogic logic) {
    return TabBar(
      controller: logic.tabController,
      labelColor: Colors.white,
      labelStyle: TextStyle(
          fontSize: 16.sp,
          color: kAppColor('#151718'),
          fontWeight: FontWeight.bold),
      unselectedLabelStyle:
          TextStyle(fontSize: 14.sp, color: Colors.white.withOpacity(0.7)),
      indicatorColor: Colors.white,
      indicatorSize: TabBarIndicatorSize.label,
      indicatorWeight: 2.0,
      isScrollable: false,
      unselectedLabelColor: Colors.white.withOpacity(0.7),
      tabs: const <Tab>[Tab(text: '资料'), Tab(text: '动态'), Tab(text: '相册')],
    );
  }

  Widget buildDynamicItem(DynamicModel model) {
    String? vipPath = getVipIcon(model.user?.vip, model.user?.rechargeType);
    String? positionStr;
    if (logic.hide_location) {
      positionStr = model.user?.time;
    } else {
      positionStr =
          '${(model.province ?? '') + (model.city ?? '')} ·${model.user?.time ?? ''} ';
    }
    return Container(
        color: kAppColor('#323232'),
        child: UIRow(
          clipBehavior: Clip.hardEdge,
          margin: EdgeInsets.only(top: 15.h, left: 15.w, right: 15.w),
          padding:
              EdgeInsets.only(left: 16.w, right: 14.w, top: 20.w, bottom: 25.w),
          gradientStartColor: kAppColor('#473D3E'),
          gradientEndColor: kAppColor('#584848'),
          radius: 20.w,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UIImage(
              margin: EdgeInsets.only(right: 9.w),
              httpImage: model.avatar?.toImageUrl(),
              assetPlaceHolder: 'icon_app_logo.png',
              width: 45.w,
              height: 45.w,
              radius: 22.5.w,
            ),
            Expanded(
                child: Stack(
              clipBehavior: Clip.none,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    UIRow(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            UIRow(
                              margin: EdgeInsets.only(bottom: 4.h),
                              children: [
                                UIText(
                                  text: model.user?.nickName ?? "",
                                  textColor: Colors.white,
                                  fontSize: 15.sp,
                                  fontWeight: FontWeight.bold,
                                ),
                                UIImage(
                                  margin: EdgeInsets.only(left: 4.w),
                                  httpImage:
                                      logic.userModel?.levelImage?.toImageUrl(),
                                  disabledHttpBigImage: true,
                                  width: 18.w,
                                  height: 18.w,
                                ),
                                if (vipPath != null)
                                  UIImage(
                                    margin: EdgeInsets.only(left: 4.w),
                                    assetImage: vipPath,
                                    width: 51.w,
                                  )
                              ],
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                UIText(
                                  padding:
                                      EdgeInsets.only(left: 8.w, right: 7.w),
                                  height: 18.w,
                                  alignment: Alignment.center,
                                  text: '${model.user?.age ?? 0}岁',
                                  textColor: getGenderColor(model.user?.gender),
                                  fontSize: 11.sp,
                                  color: kAppColor('#F6F6F6').withOpacity(0.11),
                                  radius: 6.w,
                                  marginDrawable: 3.w,
                                  startDrawable: UIImage(
                                    assetImage:
                                        getGenderIcon(model.user?.gender),
                                    height: 10.w,
                                  ),
                                ),
                                UIText(
                                  margin: EdgeInsets.only(left: 4.w),
                                  padding:
                                      EdgeInsets.only(left: 8.w, right: 7.w),
                                  height: 18.w,
                                  alignment: Alignment.center,
                                  text: model.user?.sexualOrientation ?? "未知",
                                  textColor: Colors.white,
                                  fontSize: 11.sp,
                                  color: kAppColor('#F6F6F6').withOpacity(0.11),
                                  radius: 6.w,
                                ),
                                UIText(
                                  margin: EdgeInsets.only(left: 4.w),
                                  padding:
                                      EdgeInsets.only(left: 8.w, right: 7.w),
                                  height: 18.w,
                                  alignment: Alignment.center,
                                  text: model.user?.funRole ?? "未知",
                                  textColor: Colors.white,
                                  fontSize: 11.sp,
                                  color: kAppColor('#F6F6F6').withOpacity(0.11),
                                  radius: 6.w,
                                ),
                              ],
                            ),
                          ],
                        ),
                        Expanded(child: Container()),
                        if (model.topOrder == 4)
                          UIImage(
                            assetImage: 'icon_square_top.gif',
                            width: 20.w,
                            height: 20.w,
                          ),
                        if (model.hot == 1)
                          UIImage(
                            margin: EdgeInsets.only(left: 9.w, right: 4.w),
                            assetImage: 'icon_square_hot.png',
                            width: 20.w,
                            height: 20.w,
                          ),
                        UIContainer(
                          child: Icon(Icons.more_vert,
                              color: kAppColor('#9E9E9E')),
                          onTap: () {
                            Get.bottomSheet(square_more_widget(
                              dynamicId: model.id ?? "",
                              isMe: model.accountId ==
                                  WhlUserUtils.getUserInfo().accountId,
                              callBack: (type) {
                                if (type == 2) {
                                  logic.getUserDynamicsList(isRefresh: true);
                                } else if (type == 3) {
                                  logic.toEditSquarePage(model);
                                } else if (type == 4) {
                                  logic.listData.remove(model);
                                  logic.update();
                                }
                              },
                            ));
                          },
                        )
                      ],
                    ),
                    UIText(
                      margin: EdgeInsets.only(top: 10.w),
                      text: model.content ?? "",
                      textColor: Colors.white,
                      fontSize: 15.sp,
                      shrinkWrap: false,
                    ),
                    if (model?.urls?.first.imageUrl?.isNotEmpty ?? false)
                      UIImage(
                        margin: EdgeInsets.only(top: 13.w),
                        httpImage: model?.urls?.first.imageUrl?.toImageUrl(),
                        assetPlaceHolder: 'icon_app_logo.png',
                        width: 166.w,
                        alignment: Alignment.centerLeft,
                        height: 166.w,
                        fit: BoxFit.cover,
                        onTap: () {
                          Get.toNamed(Routes.userInfo, arguments: {
                            "accountId": model?.accountId,
                          });
                        },
                        radius: 6.w,
                      ),
                    Container(
                      margin: EdgeInsets.only(top: 11.w),
                      child: RichText(
                        text: TextSpan(
                            text: positionStr,
                            style: TextStyle(
                                fontSize: 10.sp, color: kAppColor('#C1C1C1')),
                            children: [
                              TextSpan(
                                  text: model.caveId != null
                                      ? '· 来自 ${model.caveName ?? ''}'
                                      : '',
                                  style: TextStyle(
                                      fontSize: 10.sp, color: kAppTextColor))
                            ]),
                      ),
                    ),
                    UIRow(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      margin: EdgeInsets.only(top: 21.w),
                      children: [
                        // UIImage(
                        //   assetImage: 'icon_square_share.png',
                        //   width: 20.w,
                        //   imageColor: model.setting?.notForward == 1
                        //       ? kAppSub3TextColor.withOpacity(0.5)
                        //       : kAppColor("#ADADAD"),
                        //   onTap: () {
                        //     if (model.setting?.notComment == 1) {
                        //       MyToast.show("该动态禁止转发");
                        //       return;
                        //     }
                        //     Get.toNamed(Routes.publish,
                        //         arguments: {"model": model, "isForward": true});
                        //   },
                        // ),
                        UIImage(
                          assetImage: 'icon_square_fly_grey.png',
                          width: 20.w,
                          onTap: () {
                            Get.bottomSheet(
                              PushTopSettingSheet(
                                id: model.id,
                              ),
                              isScrollControlled: true,
                            );
                          },
                        ),
                        UIImage(
                          assetImage: 'icon_square_gift.png',
                          onTap: () {
                            sendGiftDialog(model.accountId!);
                          },
                          width: 20.w,
                        ),
                        UIText(
                          text: '${model.commentNum ?? 0}',
                          textColor: model.setting?.notComment == 1
                              ? kAppSub3TextColor.withOpacity(0.5)
                              : kAppColor("#ADADAD"),
                          fontSize: 14.sp,
                          marginDrawable: 3.w,
                          startDrawable: UIImage(
                            assetImage: 'icon_square_comment.png',
                            width: 20.w,
                            imageColor: model.setting?.notComment == 1
                                ? kAppSub3TextColor.withOpacity(0.5)
                                : kAppColor("#ADADAD"),
                          ),
                          onTap: () {
                            if (model.setting?.notComment == 1) {
                              MyToast.show("该动态禁止评论");
                              return;
                            }
                            Get.bottomSheet(
                              CommentWidget(
                                dynamicId: model.id ?? "",
                              ),
                              isScrollControlled: true,
                            ).then((value) {
                              if (model.commentNum != value['commentNum']) {
                                model.commentNum = value['commentNum'];
                                logic.update();
                              }
                            });
                          },
                        ),
                        UIText(
                          text: '${model.goodNum ?? 0}',
                          textColor: kAppColor('#ADADAD'),
                          fontSize: 14.sp,
                          marginDrawable: 3.w,
                          startDrawable: UIImage(
                            assetImage: model?.currentZan == true
                                ? 'icon_square_isLike.png'
                                : 'icon_square_like.png',
                            width: 20.w,
                          ),
                          onTap: () {
                            dianZanDynamicRequest(model!,
                                callback: (isSuccess) {
                              if (isSuccess && logic != null) {
                                logic.update();
                              }
                            });
                          },
                        ),
                      ],
                    )
                  ],
                ),
                if (model.topOrder == 4)
                  Positioned(
                    right: -22.w,
                    top: -30.h,
                    child: Transform.rotate(
                      angle: -3.14 * 270,
                      child: UIText(
                        alignment: Alignment.center,
                        width: 60.w,
                        padding:
                            EdgeInsets.only(left: 12.w, top: 6.h, bottom: 2.h),
                        // height: 30.h,
                        text: '置顶',
                        fontSize: 11.sp,
                        textColor: kAppWhiteColor,
                        color: kAppColor('#DA4456'),
                      ),
                    ),
                  ),
              ],
            ))
          ],
        ));
  }
}
