import 'package:cave_flutter/model/user_model.dart';
import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:get/get.dart';

class AddFriendDialogLogic extends GetxController {
  int selectedIndex = 0;
  bool isShowGift = false;

  UserModel? userModel;

  @override
  void onInit() {
    super.onInit();

    userModel = Get.arguments;
    update();
    print("add friend init");
  }


  doClickGift() {
    if (!isShowGift){
      isShowGift = true;
      update();
    }
  }

  addFriendRequest(){
    if (userModel == null) {
      return;
    }

    '${WhlApi.applyFriend}${userModel!.id ?? userModel!.userId}'.get({},withLoading: true).then((value) {
      if (value.isSuccess()) {
        MyToast.show("提交好友申请成功");
        Get.back();
      }
    });
  }
}
