import 'package:cave_flutter/routes/whl_app_pages.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:cave_flutter/config/whl_global_config.dart';

import 'share_logic.dart';

class SharePage extends StatelessWidget {
  SharePage({Key? key}) : super(key: key);

  final logic = Get.put(ShareLogic());

  String getImageBaseUrl = WHLGlobalConfig.getInstance().getImageBaseUrl();

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: GetBuilder<ShareLogic>(
      builder: (logic) {
        return Stack(
          children: [
            UIContainer(
              gradientStartColor: kAppColor('#FFFCDF'),
              gradientEndColor: kAppColor('#F4F6E9'),
              gradientBegin: Alignment.topCenter,
              gradientEnd: Alignment.bottomCenter,
              width: double.infinity,
              height: double.infinity,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                UIImage(
                  margin: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
                  padding: EdgeInsets.only(
                      top: 12.w, bottom: 12.w, left: 15.w, right: 15.w),
                  assetImage: 'icon_back_black.png',
                  width: 50.w,
                  onTap: () {
                    Get.back();
                  },
                ),
                Expanded(
                    child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      UIText(
                        margin: EdgeInsets.only(top: 14.w, left: 23.w),
                        text: '邀请好友',
                        textColor: kAppTextColor,
                        fontSize: 18.sp,
                        fontWeight: FontWeight.bold,
                      ),
                      UIText(
                        margin: EdgeInsets.only(top: 4.w, left: 23.w),
                        text: '获得永久奖励',
                        textColor: kAppTextColor,
                        fontSize: 32.sp,
                        fontWeight: FontWeight.bold,
                      ),
                      UIContainer(
                        margin: EdgeInsets.only(top: 10.w, left: 21.w),
                        padding: EdgeInsets.only(
                            left: 14.w, top: 6.w, bottom: 6.w, right: 12.w),
                        radius: 24.w,
                        strokeColor: kAppColor('#000000').withOpacity(0.39),
                        strokeWidth: 0.5.w,
                        child: RichText(
                          text: TextSpan(
                              text: '高达',
                              style: TextStyle(
                                  fontSize: 12.sp,
                                  color: kAppTextColor,
                                  fontWeight: FontWeight.bold),
                              children: [
                                TextSpan(
                                    text: ' 20% ',
                                    style: TextStyle(
                                        fontSize: 12.sp,
                                        color: kAppColor('#EB5070'))),
                                TextSpan(text: '平台利润奖励')
                              ]),
                        ),
                      ),
                      UIColumn(
                        radius: 16.w,
                        margin:
                            EdgeInsets.only(top: 45.w, left: 17.w, right: 18.w),
                        padding: EdgeInsets.only(
                            top: 21.w, left: 15.w, right: 19.w, bottom: 34.w),
                        color: Colors.white,
                        width: double.infinity,
                        children: [
                          UIText(
                            text: '共计收入',
                            textColor: kAppTextColor,
                            fontSize: 16.sp,
                            fontWeight: FontWeight.bold,
                          ),
                          UIRow(
                            margin: EdgeInsets.only(top: 7.w, bottom: 10.w),
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              UIText(
                                text: logic.shareState['coinCount'].toString(),
                                textColor: kAppColor('#E93323'),
                                fontSize: 37.sp,
                                fontWeight: FontWeight.bold,
                              ),
                              UIText(
                                margin: EdgeInsets.only(bottom: 3.w),
                                text: 'C币',
                                textColor: kAppColor('#E93323'),
                                fontSize: 18.sp,
                                fontWeight: FontWeight.bold,
                              ),
                            ],
                          ),
                          UIRow(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              UIText(
                                text: logic.shareState['shareCount'].toString(),
                                textColor: kAppTextColor,
                                fontSize: 19.sp,
                                fontWeight: FontWeight.bold,
                                marginDrawable: 5.w,
                                bottomDrawable: UIText(
                                  text: '手机验证人数',
                                  textColor: kAppColor('#9C9C9C'),
                                  fontSize: 11.sp,
                                ),
                              ),
                              Container(
                                height: 15.w,
                                width: 0.5.w,
                                color: kAppColor('#DCDCDC'),
                              ),
                              UIText(
                                text: logic.shareState['registerCount']
                                    .toString(),
                                textColor: kAppTextColor,
                                fontSize: 19.sp,
                                fontWeight: FontWeight.bold,
                                marginDrawable: 5.w,
                                bottomDrawable: UIText(
                                  text: '注册人数',
                                  textColor: kAppColor('#9C9C9C'),
                                  fontSize: 11.sp,
                                ),
                              ),
                              Container(
                                height: 15.w,
                                width: 0.5.w,
                                color: kAppColor('#DCDCDC'),
                              ),
                              UIText(
                                text: logic.shareState['userRechargeCount']
                                    .toString(),
                                textColor: kAppTextColor,
                                fontSize: 19.sp,
                                fontWeight: FontWeight.bold,
                                marginDrawable: 5.w,
                                bottomDrawable: UIText(
                                  text: '充值人数',
                                  textColor: kAppColor('#9C9C9C'),
                                  fontSize: 11.sp,
                                ),
                              ),
                            ],
                          ),
                          UISolidButton(
                            margin: EdgeInsets.only(top: 27.w),
                            radius: 16.w,
                            text: '立即提现',
                            color: kAppColor('#FF5858'),
                            onTap: () {
                              Get.toNamed(Routes.withDrawIndexPage);
                            },
                          )
                        ],
                      ),
                      logic.inviteRankList.isNotEmpty
                          ? UIColumn(
                              margin: EdgeInsets.only(
                                  top: 21.w, left: 18.w, right: 17.w),
                              height: 265.w,
                              width: double.infinity,
                              color: Colors.white,
                              radius: 16.w,
                              children: [
                                UIText(
                                  margin: EdgeInsets.only(top: 18.w),
                                  text: '邀请排行榜',
                                  textColor: kAppTextColor,
                                  fontSize: 16.sp,
                                  fontWeight: FontWeight.bold,
                                ),
                                Expanded(
                                    child: CustomScrollView(
                                  slivers: [
                                    SliverList(
                                        delegate: SliverChildBuilderDelegate(
                                            (context, index) {
                                      return UIRow(
                                        margin: EdgeInsets.only(top: 14.w),
                                        padding: EdgeInsets.only(
                                            left: 14.w, right: 14.w),
                                        children: [
                                          if (index < 3)
                                            UIImage(
                                              assetImage:
                                                  'icon_invite_rank_${index}.png',
                                              width: 29.w,
                                            )
                                          else
                                            UIText(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: 10.w),
                                              text: '${index + 1}',
                                              textColor: kAppColor('#9C7004'),
                                              fontSize: 11.sp,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          logic.inviteRankList[index]
                                                      ['inviterAvatar'] !=
                                                  null
                                              ? UIImage(
                                                  httpImage: !logic
                                                          .inviteRankList[index]
                                                              ['inviterAvatar']
                                                          .toString()
                                                          .startsWith('http')
                                                      ? '$getImageBaseUrl${logic.inviteRankList[index]['inviterAvatar']}'
                                                      : logic.inviteRankList[
                                                              index]
                                                          ['inviterAvatar'],
                                                  width: 35.w,
                                                  height: 35.w,
                                                  radius: 35.w,
                                                  margin: EdgeInsets.only(
                                                      left: 2.w, right: 7.w),
                                                )
                                              : UIImage(
                                                  assetImage:
                                                      'icon_app_logo.png',
                                                  width: 35.w,
                                                  height: 35.w,
                                                  radius: 35.w,
                                                  margin: EdgeInsets.only(
                                                      left: 2.w, right: 7.w),
                                                ),
                                          UIText(
                                            text: logic.inviteRankList[index]
                                                ['inviterName'],
                                            textColor: kAppTextColor,
                                            fontSize: 13.sp,
                                            fontWeight: FontWeight.bold,
                                          ),
                                          Expanded(child: Container()),
                                          RichText(
                                              text: TextSpan(
                                            text:
                                                '邀请${logic.inviteRankList[index]['inviteCount']}名',
                                            style: TextStyle(
                                                fontSize: 13.sp,
                                                color: kAppColor('#73766F')),
                                            // children: [
                                            // TextSpan(
                                            //     text: '9999.99',
                                            //     style: TextStyle(
                                            //         fontSize: 13.sp,
                                            //         color: kAppColor(
                                            //             '#FF5858'),
                                            //         fontWeight:
                                            //             FontWeight.bold)),
                                            // TextSpan(
                                            //     text: '元',
                                            //     style: TextStyle(
                                            //         fontSize: 13.sp,
                                            //         color: kAppColor(
                                            //             '#73766F'))),
                                            // ]
                                          ))
                                        ],
                                      );
                                    }, childCount: logic.inviteRankList.length))
                                  ],
                                ))
                              ],
                            )
                          : Container(),
                      UIRow(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        margin: EdgeInsets.only(top: 25.w, bottom: 15.w),
                        children: logic.tabList.map((e) {
                          return GetBuilder<ShareLogic>(builder: (logic) {
                            return UIText(
                              text: e.title,
                              textColor: e == logic.selectedModel
                                  ? kAppTextColor
                                  : kAppColor('#ADADAD'),
                              fontSize: 13.sp,
                              fontWeight: e == logic.selectedModel
                                  ? FontWeight.bold
                                  : FontWeight.normal,
                              onTap: () {
                                logic.selectedModel = e;
                                logic.update();
                              },
                            );
                          });
                        }).toList(),
                      ),
                      GetBuilder<ShareLogic>(builder: (logic) {
                        if (logic.selectedModel.shareType ==
                            ShareType.shareSkill) {
                          return UIColumn(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            margin: EdgeInsets.only(left: 18.w, right: 17.w),
                            padding: EdgeInsets.only(
                                left: 15.w, right: 15.w, bottom: 15.w),
                            radius: 16.w,
                            color: Colors.white,
                            width: double.infinity,
                            // height: 432.w,
                            children: [
                              UIText(
                                margin: EdgeInsets.only(top: 16.w),
                                text: '分享技巧：',
                                textColor: kAppColor('#FF5858'),
                                fontSize: 12.sp,
                              ),
                              UIText(
                                margin: EdgeInsets.only(top: 7.w),
                                text: '·通过复制邀请链接、生成分享二维码保存到相册、在社区转发动态到第三方平台。 \n'
                                    '·找到更多同兴趣好友，例如各大社交平台、自媒体平台、常用聊天软件，通过发朋友圈、私聊、好友、兴趣群组等等，向用户进行推广。 \n'
                                    '·自行创建的同兴趣交友圈子，有自己的垂直用户群体，例如：游戏群、粉丝群、交友群、Coser群、同学群、公益群、同性群、相亲群、艺术交流群、人文交流群等等。 \n'
                                    '·可以自己进行第三方付费推广、广告宣传等，仅作为建议，根据自己实际情况酌情操作。',
                                textColor: kAppColor('#979797'),
                                fontSize: 11.sp,
                                shrinkWrap: false,
                              ),
                              UIText(
                                margin: EdgeInsets.only(top: 11.w),
                                text: '可阐述Cave优点：',
                                textColor: kAppColor('#FF5858'),
                                fontSize: 12.sp,
                              ),
                              UIText(
                                margin: EdgeInsets.only(top: 7.w),
                                text:
                                    '·CaveApp有强大的隐私保护功能，平台有完善的《互联网信息安全管理准则》，不保存任何敏感信息，可为用户提供更好的隐私保护。 \n'
                                    '·CaveApp拥有强大的社交隔离系统，多种添加方式避免用户受到不必要的骚扰，可沉浸在属于自己的海洋。 \n'
                                    '·CaveApp拥有强大的交友功能，不管你是想同城交友（附近人），还是想找个情感陪伴（密聊）、还是颜控（灵魂交互）、或者声控（心动电台）、甚至是想找个网友聊聊天（灵魂速配）、又或者说你想创建一个属于自己的社交圈子（Cave系统），那么Cave是你的不二之选。 \n'
                                    '·CaveApp拥有强大的动态发布功能，简洁易操作，并且社区内容更丰富，可以更好的表达自己，受到关注，让自己成为当之无愧的小网红。',
                                textColor: kAppColor('#979797'),
                                fontSize: 11.sp,
                                shrinkWrap: false,
                              ),
                            ],
                          );
                        } else if (logic.selectedModel.shareType ==
                            ShareType.rewardRule) {
                          return UIColumn(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            margin: EdgeInsets.only(left: 18.w, right: 17.w),
                            padding: EdgeInsets.only(
                                left: 15.w, right: 15.w, bottom: 15.w),
                            radius: 16.w,
                            color: Colors.white,
                            width: double.infinity,
                            // height: 432.w,
                            children: [
                              UIText(
                                margin: EdgeInsets.only(top: 16.w),
                                text: '奖励规则：',
                                textColor: kAppColor('#FF5858'),
                                fontSize: 12.sp,
                              ),
                              UIText(
                                margin: EdgeInsets.only(top: 7.w),
                                text: '·平台用户均可参与分享计划，无邀请人数限制。 \n'
                                    '·分享人与被分享人一旦绑定，则为永久收益。 \n'
                                    '·仅限分享人通过生成的分享链接、分享二维码（包含分享到第三方平台的链接），并且被分享人成功注册、登录，则记为有效分享。 \n'
                                    '·佣模式为一级分佣，不可进行二级以上分佣，例如：A分享了B、C、D，则A可获得B、C、D充值分佣，如果B、C、D继续分享其他人，则A不获得其他人收益。 \n'
                                    '·分佣比例为平台实际收益的20%，苹果手机因App Store政策问题，需要扣除相应手续费，详情可在iPhone App Store查询相关协议 \n'
                                    '·分享人的账号注销、违反社区规定被封禁，则视为主动放弃分佣奖励，详见《Cave用户协议》 \n'
                                    '·本规则最终解释权归Cave所有。',
                                textColor: kAppColor('#979797'),
                                fontSize: 11.sp,
                                shrinkWrap: false,
                              )
                            ],
                          );
                        } else {
                          return UIContainer(
                            margin: EdgeInsets.only(left: 18.w, right: 17.w),
                            padding: EdgeInsets.only(
                                left: 15.w, right: 15.w, bottom: 15.w),
                            radius: 16.w,
                            color: Colors.white,
                            width: double.infinity,
                            height: 432.w,
                            child: logic.inviteRecord.length > 0
                                ? CustomScrollView(
                                    slivers: [
                                      SliverPadding(
                                        padding:
                                            EdgeInsets.symmetric(vertical: 6.w),
                                        sliver: SliverList(
                                            delegate:
                                                SliverChildBuilderDelegate(
                                                    (context, index) {
                                          return UIColumn(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            padding: EdgeInsets.only(
                                                top: 13.w, bottom: 7.w),
                                            decoration: BoxDecoration(
                                              border: Border(
                                                  bottom: BorderSide(
                                                      width: 1.w,
                                                      color: kAppColor(
                                                          '#F6F6F6'))),
                                            ),
                                            children: [
                                              Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Expanded(
                                                    child: UIText(
                                                      text:
                                                          '邀请的手机尾号${logic.inviteRecord[index]['mobile']}用户注册',
                                                      textColor:
                                                          kAppColor('#979797'),
                                                      fontSize: 12.sp,
                                                      shrinkWrap: false,
                                                    ),
                                                  ),
                                                  // UIText(
                                                  //   text: '+64.9元',
                                                  //   textColor:
                                                  //       kAppColor('#FF5858'),
                                                  //   fontSize: 12.sp,
                                                  //   fontWeight: FontWeight.bold,
                                                  // )
                                                  UIText(
                                                    margin: EdgeInsets.only(
                                                        top: 2.w),
                                                    text: logic.getDateString(logic
                                                            .inviteRecord[index]
                                                        ['inviteTime']),
                                                    textColor:
                                                        kAppColor('#D9D9D9'),
                                                    fontSize: 11.sp,
                                                  )
                                                ],
                                              ),
                                            ],
                                          );
                                        },
                                                    childCount: logic
                                                        .inviteRecord.length)),
                                      )
                                    ],
                                  )
                                : Container(
                                    child: Container(
                                    margin: EdgeInsets.only(top: 10.w),
                                    child: UIText(
                                      textAlign: TextAlign.center,
                                      text: '暂无邀请记录',
                                      textColor: kAppColor('#979797'),
                                      fontSize: 12.sp,
                                      shrinkWrap: false,
                                    ),
                                  )),
                          );
                        }
                      }),
                      UIRow(
                        margin: EdgeInsets.only(
                            top: 26.w, bottom: 37.w, left: 46.w, right: 36.w),
                        children: [
                          Expanded(
                              child: UISolidButton(
                            text: '复制链接',
                            onTap: () {
                              logic.copyLink();
                            },
                          )),
                          SizedBox(width: 23.w),
                          Expanded(
                              child: UISolidButton(
                            text: '下载二维码',
                            textColor: kAppTextColor,
                            color: kAppColor('#FFE800'),
                            onTap: () {
                              Get.toNamed(Routes.qrCode);
                            },
                          ))
                        ],
                      )
                    ],
                  ),
                ))
              ],
            )
          ],
        );
      },
    ));
  }
}
