import 'package:cave_flutter/common/common_widget.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:cave_flutter/config/whl_global_config.dart';

import '../../../../widgets/brick/brick.dart';
import 'qr_code_logic.dart';
import 'package:cave_flutter/common/common_action.dart';

class QrCodePage extends StatelessWidget {
  QrCodePage({Key? key}) : super(key: key);

  final logic = Get.put(QrCodeLogic());

  String getImageBaseUrl = WHLGlobalConfig.getInstance().getImageBaseUrl();

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: GetBuilder<QrCodeLogic>(
      builder: (logic) {
        return Stack(
          children: [
            UIContainer(
              gradientBegin: Alignment.topCenter,
              gradientEnd: Alignment.bottomCenter,
              gradientStartColor: kAppColor('#FFFCDF'),
              gradientEndColor: kAppColor('#F4F6E9'),
              width: double.infinity,
              height: double.infinity,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                UIContainer(
                  margin: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
                  padding: EdgeInsets.only(top: 18.h, left: 20.w, right: 20.w),
                  child: const Icon(Icons.arrow_back_ios),
                  onTap: () {
                    Get.back();
                  },
                ),
                UIText(
                  margin: EdgeInsets.only(top: 22.w, bottom: 7.w),
                  text: '邀请好友加入Cave',
                  textColor: kAppTextColor,
                  alignment: Alignment.center,
                  fontSize: 25.sp,
                  fontWeight: FontWeight.bold,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                            text: '高达',
                            style: TextStyle(
                              color: kAppTextColor,
                              fontSize: 12.sp,
                              fontWeight: FontWeight.w500,
                            ),
                            children: [
                              TextSpan(
                                text: '20%',
                                style: TextStyle(
                                  color: kAppColor('#EB5070'),
                                  fontSize: 12.sp,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              const TextSpan(
                                text: '平台利润奖励',
                              )
                            ])),
                  ],
                ),
                RepaintBoundary(
                  key: logic.repaintWidgetKey,
                  child: Stack(
                    alignment: Alignment.topCenter,
                    children: [
                      UIColumn(
                        color: Colors.white,
                        margin:
                            EdgeInsets.only(top: 63.w, left: 32.w, right: 32.w),
                        padding: EdgeInsets.only(top: 57.w),
                        radius: 22.w,
                        children: [
                          UIText(
                            text: logic.userModel?.nickName ?? 'Cave',
                            textColor: kAppTextColor,
                            fontSize: 22.sp,
                            fontWeight: FontWeight.bold,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              UIText(
                                margin: EdgeInsets.only(right: 5.w),
                                padding: EdgeInsets.only(left: 8.w, right: 7.w),
                                height: 18.w,
                                alignment: Alignment.center,
                                text: '${logic.userModel?.age ?? 0}岁',
                                textColor:
                                    getGenderColor(logic.userModel?.gender),
                                fontSize: 8.sp,
                                strokeColor:
                                    getGenderColor(logic.userModel?.gender),
                                strokeWidth: 1.w,
                                radius: 18.w,
                                marginDrawable: 3.w,
                                startDrawable: UIImage(
                                  assetImage:
                                      getGenderIcon(logic.userModel?.gender),
                                  height: 10.w,
                                ),
                              ),
                              RichText(
                                  text: TextSpan(
                                      text: '/ ',
                                      style: TextStyle(
                                        color: kAppColor('#979797'),
                                        fontSize: 9.sp,
                                      ),
                                      children: [
                                    TextSpan(
                                      text:
                                          logic.userModel?.sexualOrientation ??
                                              '',
                                      style: TextStyle(
                                        color: kAppTextColor,
                                        fontSize: 9.sp,
                                      ),
                                    ),
                                    TextSpan(
                                      text: ' / ',
                                      style: TextStyle(
                                        color: kAppColor('#979797'),
                                        fontSize: 9.sp,
                                      ),
                                    ),
                                    TextSpan(
                                      text: logic.userModel?.funRole ?? "",
                                      style: TextStyle(
                                        color: kAppTextColor,
                                        fontSize: 9.sp,
                                      ),
                                    ),
                                  ]))
                            ],
                          ),
                          UIText(
                            margin: EdgeInsets.only(top: 20.w),
                            text: '这是一句个性签名的内容暂时没写...',
                            textColor: kAppColor('#474747'),
                            fontSize: 13.sp,
                          ),
                          UIStack(
                            margin: EdgeInsets.only(top: 36.w, bottom: 55.w),
                            alignment: Alignment.center,
                            children: [
                              UIImage(
                                assetImage: 'img_bg_qrcode.png',
                                width: 167.w,
                              ),
                              Center(
                                  child: CustomPaint(
                                size: Size.square(120.w),
                                painter: QrPainter(
                                  data: logic.QrUrl,
                                  version: QrVersions.auto,
                                  eyeStyle: QrEyeStyle(
                                    eyeShape: QrEyeShape.square,
                                    color: kAppTextColor,
                                  ),
                                  dataModuleStyle: QrDataModuleStyle(
                                    dataModuleShape: QrDataModuleShape.circle,
                                    color: kAppTextColor,
                                  ),
                                  // embeddedImage: logic.avatarImage,
                                  // embeddedImageStyle: const QrEmbeddedImageStyle(
                                  //   size: Size.square(60),
                                  // ),
                                  // size: 320.0,
                                ),
                              ))
                            ],
                          )
                        ],
                      ),
                      logic.userModel != null
                          ? UIImage(
                              margin: EdgeInsets.only(top: 25.w),
                              httpImage: !logic.userModel!.avatar
                                      .toString()
                                      .startsWith('http')
                                  ? '$getImageBaseUrl${logic.userModel!.avatar}'
                                  : logic.userModel!.avatar,
                              width: 93.w,
                              height: 93.w,
                              radius: 46.5.w,
                              strokeWidth: 3.w,
                              strokeColor: Colors.white,
                            )
                          : UIImage(
                              margin: EdgeInsets.only(top: 25.w),
                              assetPlaceHolder: 'icon_app_logo.png',
                              width: 93.w,
                              height: 93.w,
                              radius: 46.5.w,
                              strokeWidth: 3.w,
                              strokeColor: Colors.white,
                            )
                    ],
                  ),
                ),
                UISolidButton(
                  margin: EdgeInsets.only(top: 57.w, left: 110.w, right: 110.w),
                  text: '保存到相册',
                  fontSize: 16.sp,
                  textColor: kAppTextColor,
                  color: kAppColor('#FFE800'),
                  onTap: () {
                    logic.capturePngAndSaveImage();
                  },
                )
              ],
            )
          ],
        );
      },
    ));
  }
}
