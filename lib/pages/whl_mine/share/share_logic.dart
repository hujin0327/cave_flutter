import 'package:get/get.dart';
import 'package:cave_flutter/network/whl_api.dart';
import 'package:flutter/services.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';

import 'package:cave_flutter/model/base_model.dart';
enum ShareType {
  shareSkill,
  inviteRecord,
  rewardRule,
}
class ShareLogic extends GetxController {
  List<BaseModel> tabList = [
    BaseModel(title: '分享技巧',shareType: ShareType.shareSkill),
    BaseModel(title: '邀请记录',shareType: ShareType.inviteRecord),
    BaseModel(title: '奖励规则',shareType: ShareType.rewardRule),
  ];
  late BaseModel selectedModel;

  Map shareState = {
    'coinCount':0,
    'registerCount':0,
    'shareCount':0,
    'userRechargeCount':0
  };

  int userId = 0;

  List<dynamic> inviteRankList = [];
  List<dynamic> inviteRecord = [];

  @override
  void onInit() {
    super.onInit();
    selectedModel = tabList[0];
    getShareState();
    getInviteRankList();
    getInviteRecord();
  }

  getShareState() async {
    ResponseData responseData = await WhlApi.getShareState.get({});
    if (responseData.isSuccess()) {
      shareState = responseData.data;
      userId = responseData.data['userId'];
      update();
    }
  }

  getInviteRankList() async {
    ResponseData responseData = await WhlApi.getInviteRankList.get({});
    if (responseData.isSuccess()) {
      inviteRankList = responseData.data['records'];
      update();
    }
  }

  getInviteRecord() async {
    ResponseData responseData = await WhlApi.getMyInviteRecord.get({});
    if (responseData.isSuccess()) {
      inviteRecord = responseData.data['records'];
      print(inviteRecord.toString());
      update();
    }
  }

  copyLink() async {
      // ignore: prefer_interpolation_to_compose_strings
      ResponseData responseData = await WhlApi.saveShareCount.get({});
      if (responseData.isSuccess()) {
          String url = 'http://app.cave-meta.com:8888/register?inviteCode=' + userId.toString();
          Clipboard.setData( ClipboardData(text: url));
        MyToast.show('复制成功');
      } 
  }

  getDateString (String text) {
    DateTime dateTime = DateTime.parse(text);

    String formattedDateTime = '${dateTime.year}-${dateTime.month}-${dateTime.day}';

    return formattedDateTime;
  }
}
