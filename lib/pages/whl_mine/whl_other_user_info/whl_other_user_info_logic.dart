import 'package:cave_flutter/config/whl_base_controller.dart';
import 'package:cave_flutter/db_manager/bean/whl_product_bean.dart';
import 'package:cave_flutter/model/whl_user_info_model.dart';
import 'package:cave_flutter/pages/whl_chat/whl_chat_logic.dart';
import 'package:cave_flutter/pages/whl_explore/whl_explore_logic.dart';
import 'package:cave_flutter/pages/whl_home/whl_home_logic.dart';
import 'package:cave_flutter/pages/whl_home/whl_product_detail/whl_product_detail_logic.dart';
import 'package:cave_flutter/routes/whl_app_pages.dart';
import 'package:cave_flutter/utils/whl_db_utils.dart';
import 'package:get/get.dart';

import '../../../network/whl_api.dart';

class WhlOtherUserInfoLogic extends WhlBaseController {
  WhlProductBean productBean = Get.arguments;
  List<WhlProductBean> productList = [];

  @override
  void onInit() {
    super.onInit();
    onGetProductList();
  }

  onGetProductList() async {
    List<WhlProductBean>? modelList = await WhlDBUtils.instance.productDao.findProductByUserId(productBean.userId ?? '');
    if (modelList?.isNotEmpty == true) {
      productList = modelList!;
      update();
    }
  }

  doClickItem(WhlProductBean model) {
    if (Get.isRegistered<WhlProductDetailLogic>()) {
      WhlProductDetailLogic logic = Get.find();
      logic.productBean = model;
      logic.update();
      Get.until((route) => Get.currentRoute == Routes.productDetail);
    } else {
      Get.toNamed(Routes.productDetail, arguments: model)?.then((value) {
        onGetProductList();
      });
    }
  }

  doClickLike(WhlProductBean model) {
    if (Get.isRegistered<WhlProductDetailLogic>()) {
      WhlProductDetailLogic logic = Get.find();
      logic.doClickProductLike(model);
      update();
    } else {
      WhlExploreLogic logic = Get.find();
      logic.doClickLike(model);
      update();
    }
  }

  doClickFollow() {
    if (Get.isRegistered<WhlProductDetailLogic>()) {
      WhlProductDetailLogic logic = Get.find();
      logic.doClickFollow();
      update();
    } else {
      WhlHomeLogic logic = Get.find();
      logic.doClickFollow(productBean);
      update();
    }
  }

  doClickCancelFollow() {
    if (Get.isRegistered<WhlProductDetailLogic>()) {
      WhlProductDetailLogic logic = Get.find();
      logic.doClickCancelFollow();
      update();
    } else {
      WhlHomeLogic logic = Get.find();
      logic.doClickCancelFollow(productBean);
      update();
    }
  }

  doClickChat() {
    WhlChatLogic logic = Get.find();

  }
  doClickMore(){
    onShowReportBottomSheet(productBean.userId ?? '');
  }
}
