import 'dart:io';

import 'package:cave_flutter/db_manager/bean/whl_product_bean.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'whl_other_user_info_logic.dart';

class WhlOtherUserInfoPage extends StatelessWidget {
  WhlOtherUserInfoPage({Key? key}) : super(key: key);

  final logic = Get.put(WhlOtherUserInfoLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            UIImage(
              assetImage: 'img_top_long.png',
              width: double.infinity,
              height: 194.h,
              fit: BoxFit.fill,
            ),
            Column(
              children: [
                buildTopView(),
                buildUserInfoView(),
                UIText(
                  margin: EdgeInsets.only(left: 20.w, right: 20.w, top: 12.h),
                  text: logic.productBean.about ?? '',
                  textColor: kAppTextColor,
                  fontSize: 12.sp,
                  shrinkWrap: false,
                  // fontWeight: FontWeight.bold,
                ),
                UIText(
                  margin: EdgeInsets.only(left: 20.w, top: 20.h, bottom: 10.h),
                  text: 'Posts',
                  textColor: kAppTextColor,
                  alignment: Alignment.centerLeft,
                  fontSize: 18.sp,
                  fontWeight: FontWeight.w500,
                ),
                Container(
                  color: kAppColor('#CCCCCC'),
                  height: 0.5,
                ),
                Expanded(
                  child: GetBuilder<WhlOtherUserInfoLogic>(builder: (logic) {
                    if (logic.productList.isEmpty) return Container();
                    return CustomScrollView(
                      slivers: [
                        SliverPadding(
                          padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 10.h),
                          sliver: SliverGrid(
                              delegate: SliverChildBuilderDelegate((context, index) {
                                WhlProductBean model = logic.productList[index];
                                return buildListItemView(model);
                              }, childCount: logic.productList.length),
                              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2, crossAxisSpacing: 10.w, mainAxisSpacing: 10.w, childAspectRatio: 166 / 202)),
                        ),
                      ],
                    );
                  }),
                )
              ],
            )
          ],
        ));
  }

  UIRow buildTopView() {
    return UIRow(
      margin: EdgeInsets.only(left: 20.w, top: ScreenUtil().statusBarHeight + 8.h, right: 20.w),
      children: [
        UIImage(
          assetImage: 'icon_back_circle.png',
          width: 28.w,
          onTap: () {
            Get.back();
          },
        ),
        Expanded(
          child: UIText(
            margin: EdgeInsets.symmetric(horizontal: 10.w),
            text: logic.productBean.nickname,
            textColor: kAppTextColor,
            fontSize: 18.sp,
            alignment: Alignment.center,
            fontWeight: FontWeight.bold,
            shrinkWrap: false,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        UIImage(
          assetImage: 'icon_more_cicle.png',
          width: 28.w,
          onTap: () {
            logic.doClickMore();
          },
        )
      ],
    );
  }

  buildUserInfoView() {
    return UIRow(
      margin: EdgeInsets.only(top: 50.h, left: 24.w, right: 24.w),
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        UIImage(
          httpImage: logic.productBean.avatar,
          width: 108.w,
          height: 108.w,
          radius: 100.w,
          color: Colors.grey.withOpacity(0.8),
          strokeColor: Colors.white,
          strokeWidth: 2.w,
        ),
        Expanded(child: Container()),
        GetBuilder<WhlOtherUserInfoLogic>(builder: (logic) {
          if (logic.productBean.isFollowed == true) {
            return UIText(
                margin: EdgeInsets.only(top: 15.h),
                padding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 8.h),
                text: 'Followed',
                textColor: Colors.white,
                fontSize: 12.sp,
                color: Colors.black.withOpacity(0.4),
                radius: 4.w,
                onTap: () {
                  logic.doClickCancelFollow();
                });
          } else {
            return UIContainer(
              margin: EdgeInsets.only(top: 15.h),
              padding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 6.h),
              gradientStartColor: kAppLeftColor,
              gradientEndColor: kAppRightColor,
              radius: 4.w,
              child: UIRow(
                children: [
                  UIImage(
                    assetImage: 'icon_add.png',
                    width: 18.w,
                    height: 18.w,
                  ),
                  UIText(
                    margin: EdgeInsets.only(left: 5.w),
                    text: 'Follow',
                    textColor: Colors.white,
                    fontSize: 12.sp,
                  )
                ],
              ),
              onTap: () {
                logic.doClickFollow();
              },
            );
          }
        }),
        UIText(
          margin: EdgeInsets.only(top: 15.h, left: 24.w),
          padding: EdgeInsets.symmetric(horizontal: 8.w, vertical: 6.h),
          text: 'Chat',
          textColor: Colors.white,
          fontSize: 12.sp,
          color: Colors.black.withOpacity(0.4),
          radius: 4.w,
          marginDrawable: 4.w,
          startDrawable: UIImage(
            assetImage: 'icon_chat.png',
            width: 18.w,
            height: 18.w,
          ),
          onTap: () {
            logic.doClickChat();
          },
        )
      ],
    );
  }

  UIContainer buildListItemView(WhlProductBean model) {
    return UIContainer(
      color: Colors.white,
      radius: 4.w,
      child: UIColumn(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (model.cover?.contains('img_art') == true)
            UIImage(
              assetImage: model.cover,
              height: 148.w,
              width: double.maxFinite,
              fit: BoxFit.cover,
              radius: 4.w,
            )
          else
            UIContainer(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(4.w),
                child: Image.file(
                  File(model.cover ?? ''),
                  fit: BoxFit.cover,
                  height: 148.w,
                  width: double.maxFinite,
                ),
              ),
            ),
          UIText(
            margin: EdgeInsets.only(left: 10.w, top: 8.h, right: 10.w),
            text: model.title,
            textColor: kAppTextColor,
            fontSize: 12.sp,
            shrinkWrap: false,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            fontWeight: FontWeight.bold,
          ),
          UIRow(
            margin: EdgeInsets.only(top: 4.h, left: 10.w, right: 10.w),
            children: [
              UIImage(
                httpImage: model.avatar,
                width: 16.w,
                height: 16.w,
                radius: 16.w,
              ),
              Expanded(
                child: UIText(
                  margin: EdgeInsets.only(left: 5.w, right: 5.w),
                  text: model.nickname,
                  textColor: kAppSub2TextColor,
                  fontSize: 10.sp,
                  shrinkWrap: false,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              UIImage(
                assetImage: model.isLike == false ? 'icon_like_no.png' : 'icon_like.png',
                width: 16.w,
                height: 16.w,
                onTap: () {
                  logic.doClickLike(model);
                },
              )
            ],
          )
        ],
      ),
      onTap: () {
        logic.doClickItem(model);
      },
    );
  }
}
