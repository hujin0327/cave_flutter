import 'package:cave_flutter/config/whl_global_config.dart';
import 'package:cave_flutter/network/whl_network_utils.dart';
import 'package:cave_flutter/routes/whl_app_pages.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:cave_flutter/utils/whl_user_utils.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'switch_account_logic.dart';

class Switch_accountPage extends StatelessWidget {
  final logic = Get.put(Switch_accountLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: WhlAppBar(
        centerTitle: "切换账号",
      ),
      body: GetBuilder<Switch_accountLogic>(builder: (logic) {
        List keys = logic.locAccountData.keys.toList();
        return ListView.builder(itemBuilder: (context, index) {
          if (index >= keys.length) {
            return buildAddWidget();
          }
          String accountId = keys[index];
          Map info = logic.locAccountData[accountId];
          return buildAccountItemWidget(info);
        }, itemCount: keys.length+1,);
      }),
    );
  }

  buildAddWidget() {
    return UIRow(
      margin: EdgeInsets.all(18.w),
      padding: EdgeInsets.all(14.w),
      color: kAppWhiteColor,
      radius: 12.w,
      children: [
        UIText(
          strokeColor: kAppColor("#D9D9D9"),
          strokeWidth: 1.w,
          text: "+",
          fontSize: 30.sp,
          textColor: kAppSub3TextColor,
          width: 60.w,
          height: 60.w,
          radius: 6.w,
          alignment: Alignment.center,
        ),
        Expanded(
          child: UIText(
            margin: EdgeInsets.only(left: 15.w),
            text: "添加账号",
            textColor: kAppTwoTextColor,
            fontSize: 16.sp,
          ),
        )
      ],
      onTap: (){
        logic.toLoginPage();
      },
    );
  }

  buildAccountItemWidget(Map info) {
    return UIRow(
      margin: EdgeInsets.only(left: 18.w,right: 18.w,top: 18.w),
      padding: EdgeInsets.all(14.w),
      radius: 12.w,
      color: kAppWhiteColor,
      children: [
        UIImage(
          httpImage: info["avatar"].toString().toImageUrl(),
          assetPlaceHolder: WHLGlobalConfig.userPlaceholderAvatar,
          width: 60.w,
          height: 60.w,
          radius: 6.w,
        ),
        Expanded(
          child: UIColumn(
            margin: EdgeInsets.only(left: 15.w),
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              UIText(
                text: info["nickName"],
                fontSize: 16.sp,
                textColor: kAppBlackColor,
              ),
              UIText(
                margin: EdgeInsets.only(top: 9.w),
                text: info["accountId"],
                fontSize: 14.sp,
                textColor: kAppTwoTextColor,
              ),
            ],
          ),
        ),
        if (info["accountId"] == WhlUserUtils.getUserInfo().accountId)
        UIText(
          margin: EdgeInsets.only(right: 15.w,bottom: 30.w),
          text: "当前登录",
          textColor: kAppColor("#4DC18D"),
          fontSize: 14.sp,
        )

      ],
      onTap: (){
        if (info["accountId"] != WhlUserUtils.getUserInfo().accountId){
          String token = info["token"]??"";
          WhlUserUtils.switchAccount();
          WhlNetWorkUtils.setAccessToken(token);
          Get.offAllNamed(Routes.main);
        }
      },
    );
  }
}
