import 'dart:convert';

import 'package:cave_flutter/utils/whl_user_utils.dart';
import 'package:get/get.dart';

import '../../../routes/whl_app_pages.dart';

class Switch_accountLogic extends GetxController {

  Map locAccountData = {};

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();

    locAccountData = WhlUserUtils().getLocAccount()??{};
    update();
  }

  void toLoginPage() {
    Get.toNamed(Routes.login);
  }
}
