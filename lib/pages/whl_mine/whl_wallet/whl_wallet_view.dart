import 'dart:io';

import 'package:cave_flutter/pages/whl_mine/whl_wallet/model/whl_wallet_model.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/widget/text_widget.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../common/common_widget.dart';
import '../../../widgets/brick/widget/basic_widget.dart';
import '../../../widgets/brick/widget/image_widget.dart';
import 'whl_wallet_logic.dart';

class WhlWalletPage extends StatelessWidget {
  WhlWalletPage({Key? key}) : super(key: key);

  final logic = Get.put(WhlWalletLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppColor('#ECEDF0'),
      appBar: WhlAppBar(
        backgroundColor: kAppColor('#1B1B1B'),
        centerTitle: '我的钱包',
        isShowBottomLine: false,
      ),
      body: SingleChildScrollView(child: GetBuilder<WhlWalletLogic>(
        builder: (s) {
          return UIColumn(
            padding: EdgeInsets.zero,
            children: [_topWidget(), _bottomWidget()],
          );
        },
      )),
    );
  }

  ///顶部
  _topWidget() {
    return Container(
      height: logic.topItemMinHeight * 2 + 20.h,
      decoration: BoxDecoration(
          color: kAppColor('#1B1B1B'),
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(20.w),
            bottomLeft: Radius.circular(20.w),
          )),
      child: GetBuilder<WhlWalletLogic>(
        builder: (logic) {
          return Stack(
            children: [
              //右侧图标
              Positioned(
                  right: 0,
                  top: 0,
                  child: UIImage(
                    assetImage: 'wallet_right_icon.png',
                    width: 90.w,
                  )),
              Positioned(
                left: logic.topItemMarginLr,
                right: logic.topItemMarginLr,
                bottom: logic.topItemMinHeight,
                child: _topWidgetItem(
                  isWithDraw: false,
                  title: '账户余额',
                  money: '${logic.walletModel?.balance}',
                ),
              ),
              Positioned(
                  left: logic.topItemMarginLr - 5.w,
                  right: logic.topItemMarginLr - 5.w,
                  bottom: logic.topItemMinHeight - 33.h,
                  child: UIContainer(
                    height: 40.h,
                    radius: logic.topItemRadius,
                    color: kAppColor('#1B1B1B'),
                  )),
              Positioned(
                left: logic.topItemMarginLr,
                right: logic.topItemMarginLr,
                bottom: 0,
                child: _topWidgetItem(
                    isWithDraw: true,
                    title: '预计可提现金额',
                    money: '${logic.walletModel?.withdrawalAmount}'),
              )
            ],
          );
        },
      ),
    );
  }

  //顶部item
  _topWidgetItem({
    required bool isWithDraw,
    required String title,
    required String money,
  }) {
    return UIContainer(
      padding: EdgeInsets.only(left: 18.w, right: 18.w),
      width: double.infinity,
      height: logic.topItemMinHeight,
      gradientBegin: Alignment.bottomLeft,
      gradientEnd: Alignment.topRight,
      gradientStartColor: kAppColor('#4D2C2B22'),
      gradientEndColor: kAppColor('#4DB3B296'),
      topLeftRadius: logic.topItemRadius,
      topRightRadius: logic.topItemRadius,
      strokeWidth: 1.w,
      strokeColor: kAppColor("#4D9F9F97"),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              //标题和余额
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 12.h,
                    ),
                    UIText(
                      text: title,
                      textColor: Colors.white.withOpacity(0.6),
                      fontSize: 11.sp,
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    RichText(
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: money,
                              style: TextStyle(
                                  fontSize: 25.sp,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                            WidgetSpan(
                                child: SizedBox(
                              width: 1.5.w,
                            )),
                            TextSpan(
                                style: TextStyle(
                                    fontSize: 13.sp,
                                    color: Colors.white.withOpacity(0.7)),
                                text: isWithDraw ? "元" : "C币"),
                          ],
                        )),
                  ],
                ),
              ),
              //提现按钮
              if (isWithDraw) _withDrawButton()
            ],
          ),
          SizedBox(
            height: 2.h,
          ),
          //底部文案
          if (isWithDraw)
            UIText(
              padding: EdgeInsets.symmetric(horizontal: 8.w, vertical: 5.h),
              text: "已充值的C币、收到的C币红包等，不可提现",
              textColor: Colors.white.withOpacity(0.6),
              color: kAppColor("#4D4B0F"),
              fontSize: 10.sp,
              radius: 99.h,
            )
          else
            Text(
              "可提现 ${logic.walletModel?.withdrawal ?? ""}c币 | 不可提现 ${logic.walletModel?.unCanWithDrawCoin ?? ""}c币",
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontSize: 10.sp,
                color: Colors.white.withOpacity(0.5),
              ),
            )
        ],
      ),
    );
  }

  ///提现按钮
  _withDrawButton() {
    return InkWell(
      onTap: logic.onPushToWithDraw,
      child: SizedBox(
        height: 53.h,
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            UIText(
              alignment: Alignment.center,
              fontWeight: FontWeight.w600,
              width: 75.w,
              height: 36.w,
              text: "提现",
              textColor: Colors.black,
              color: kAppColor('#FFF754'),
              fontSize: 12.sp,
              radius: 99.h,
            ),
            Positioned(
                bottom: 23.h,
                child: UIContainer(
                  backgroundDrawable: UIImage(
                    assetImage: 'with_draw_tip.png',
                    width: 68.w,
                  ),
                  child: UIText(
                    padding: EdgeInsets.only(bottom: 4.h),
                    alignment: Alignment.center,
                    fontWeight: FontWeight.w600,
                    text: "绑定支付宝",
                    textColor: Colors.white,
                    color: Colors.transparent,
                    fontSize: 10.sp,
                  ),
                ))
          ],
        ),
      ),
    );
  }

  ///底部
  _bottomWidget() {
    return Container(
      padding: EdgeInsets.only(left: 20.w, right: 20.w),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(20.w), topLeft: Radius.circular(20.w)),
          color: Colors.white),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _rechargeListWidget(),
          //  暂时去掉支付宝和微信的支付方式
          // _payTypeListWidget(),
          _confirmWidget()
        ],
      ),
    );
  }

  ///充值金额
  _rechargeListWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 26.h,
        ),
        UIRow(
          children: [
            Expanded(
              child: UIText(
                text: "选择充值金额",
                textColor: kAppColor('#9E9E9E'),
                color: Colors.transparent,
                fontSize: 14.sp,
              ),
            ),
            if (logic.selRechargeModel?.endTime != null)
              UIText(
                text: '距离活动时间：${logic.selRechargeModel?.endTime}',
                textColor: Colors.red,
                fontSize: 12.sp,
              )
          ],
        ),
        SizedBox(
          height: 17.h,
        ),
        GridView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            padding: EdgeInsets.zero,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              //水平子Widget之间间距
              crossAxisSpacing: 15.w,
              //垂直子Widget之间间距
              mainAxisSpacing: 10.h,
              //一行的Widget数量
              crossAxisCount: 3,
              //子Widget宽高比例
              childAspectRatio: 102 / 50,
            ),
            itemCount: logic.walletModel?.rechargeList?.length ?? 0,
            itemBuilder: (BuildContext context, int index) {
              WhlWalletRechargeItemModel model =
                  logic.walletModel!.rechargeList![index];
              bool selected = model.id == logic.selRechargeModel?.id;
              String? rightTag;
              if (model.nowFirstCharge == 1) {
                rightTag = '首冲200%';
              } else if (model.discount != null) {
                rightTag = '${model.discount}折';
              }
              return UIContainer(
                onTap: () {
                  logic.onSelRechargeItem(model);
                },
                padding: EdgeInsets.only(left: 2.w, right: 0.w),
                width: double.infinity,
                color: selected ? Colors.white : kAppColor('#F5F7F9'),
                height: 50.h,
                radius: 12.w,
                strokeWidth: 1.w,
                alignment: Alignment.center,
                strokeColor: selected ? Colors.black : Colors.transparent,
                child: Stack(
                  fit: StackFit.expand,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        // Text(
                        //   "${model.nowMoney}C币",
                        //   style: TextStyle(
                        //     fontSize: 17.sp,
                        //     fontWeight: FontWeight.bold,
                        //     color: Colors.black,
                        //   ),
                        //   textAlign: TextAlign.center,
                        // ),

                        RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                            text: "${model.nowMoney}",
                            style: TextStyle(
                              fontSize: 17.sp,
                              color: kAppColor('#000000'),
                              fontWeight: FontWeight.bold,
                            ),
                            children: <TextSpan>[
                              TextSpan(
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12.sp,
                                  color: kAppColor('#000000'),
                                ),
                                text: " C币",
                              ),
                            ],
                          ),
                        ),
                        if (model.nowGiveMoney != null &&
                            model.nowGiveMoney! > 0)
                          Text(
                            '送${model.nowGiveMoney}C币',
                            style: TextStyle(
                              color: kAppColor('#1D90E3'),
                              fontSize: 8.sp,
                            ),
                          ),
                        RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                            text: "¥",
                            style: TextStyle(
                                fontSize: 8.sp, color: kAppColor('#ADADAD')),
                            children: <TextSpan>[
                              TextSpan(
                                style: TextStyle(
                                  fontSize: 8.sp,
                                  color: kAppColor('#ADADAD'),
                                ),
                                text: "${model.nowPrice}",
                              ),
                              if (model.originalPrice != null)
                                TextSpan(
                                  style: TextStyle(
                                    fontSize: 6.sp,
                                    color: kAppColor('#D0D0D0'),
                                    decoration: TextDecoration.lineThrough,
                                  ),
                                  text: " ￥${model.originalPrice}",
                                ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    if (rightTag != null)
                      Positioned(
                        right: 0,
                        child: UIContainer(
                          alignment: Alignment.center,
                          width: 40.w,
                          height: 14.h,
                          decoration: BoxDecoration(
                            color: kAppColor('#EC5450'),
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(12.w),
                              bottomLeft: Radius.circular(6.w),
                            ),
                          ),
                          child: UIText(
                            text: rightTag,
                            fontSize: 8.sp,
                            textColor: Colors.white,
                          ),
                        ),
                      )
                  ],
                ),
              );
            }),
        SizedBox(
          height: 10.h,
        ),
        UIContainer(
          height: 50.h,
          radius: 12.w,
          color: kAppColor('#F5F7F9'),
          padding: EdgeInsets.only(left: 18.w, right: 18.w),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              UIText(
                text: "其他金额",
                fontWeight: FontWeight.bold,
                textColor: Colors.black,
                color: Colors.transparent,
                fontSize: 14.sp,
              ),
              SizedBox(
                width: 18.w,
              ),
              Expanded(
                  child: noBorderCTextField(
                controller: logic.customController,
                keyboardType: TextInputType.number,
                textAlign: TextAlign.left,
                hint: "请输入充值金额",
                textColor: kAppSubTextColor,
                hintTextColor: kAppSub3TextColor,
                fontSize: 14.sp,
                contentPadding: EdgeInsets.only(top: 4.h),
              ))
            ],
          ),
        )
      ],
    );
  }

  ///支付方式
  _payTypeListWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 23.h,
        ),
        UIText(
          text: "支付方式",
          textColor: kAppColor('#9E9E9E'),
          color: Colors.transparent,
          fontSize: 14.sp,
        ),
        SizedBox(
          height: 17.h,
        ),
        ListView.separated(
            physics: const NeverScrollableScrollPhysics(),
            padding: EdgeInsets.zero,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              WhlWalletPayTypeModel model =
                  logic.walletModel!.payTypeList![index];
              return UIContainer(
                onTap: () {
                  logic.onSelPayTypeItem(model);
                },
                height: 50.h,
                padding: EdgeInsets.only(left: 12.w, right: 12.w),
                strokeColor: kAppColor('#EFEFEF'),
                strokeWidth: 1.sp,
                radius: 12.w,
                child: Row(
                  children: [
                    UIImage(
                      assetImage: model.image,
                      width: 24.w,
                    ),
                    SizedBox(
                      width: 11.w,
                    ),
                    Expanded(
                      child: UIText(
                        text: model.title,
                        textColor: Colors.black,
                        fontWeight: FontWeight.bold,
                        color: Colors.transparent,
                        fontSize: 13.sp,
                      ),
                    ),
                    UIImage(
                      assetImage: model == logic.selPayModel
                          ? "icon_select.png"
                          : "icon_select_nor_gray.png",
                      width: 18.w,
                    ),
                  ],
                ),
              );
            },
            separatorBuilder: (context, index) {
              return SizedBox(
                height: 10.h,
              );
            },
            itemCount: logic.walletModel?.payTypeList?.length ?? 0)
      ],
    );
  }

  ///底部按钮
  _confirmWidget() {
    return SizedBox(
      width: double.infinity,
      child: Column(
        children: [
          SizedBox(
            height: 28.h,
          ),
          InkWell(
            onTap: logic.onCheckProtocol,
            child: RichText(
                text: TextSpan(
              text: "查看",
              style: TextStyle(
                  fontSize: 10.sp,
                  color: kAppColor('#9E9E9E'),
                  decoration: TextDecoration.underline),
              children: <TextSpan>[
                TextSpan(
                    style: TextStyle(
                        fontSize: 10.sp,
                        color: Colors.black,
                        decoration: TextDecoration.underline),
                    text: "《Cave用户充值协议》"),
              ],
            )),
          ),
          UIText(
            onTap: logic.onRechargeNow,
            width: double.infinity,
            alignment: Alignment.center,
            margin: EdgeInsets.only(top: 12.h, bottom: 12.h),
            height: 53.h,
            text: '立即充值',
            fontWeight: FontWeight.bold,
            fontSize: 16.sp,
            color: Colors.black,
            textColor: Colors.white,
            radius: 99.h,
          )
        ],
      ),
    );
  }
}
