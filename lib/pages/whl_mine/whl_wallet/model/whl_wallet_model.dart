class WhlWalletModel {
  /// 钱包ID
  int? walletId;

  ///账户余额
  int? balance;

  ///可提现c币
  int? withdrawal;

  ///不可提现c币
  int? unCanWithDrawCoin;

  ///可提现金额
  double? withdrawalAmount;

  List<WhlWalletRechargeItemModel>? rechargeList;
  List<WhlWalletPayTypeModel>? payTypeList;
  WhlWalletModel.fromJson(Map<String, dynamic> json) {
    balance = json['balance'] ?? 0;
    withdrawal = json['withdrawal'] ?? 0;
    unCanWithDrawCoin = (json['balance'] ?? 0) - (json['withdrawal'] ?? 0);
    withdrawalAmount = json['withdrawalAmount'] ?? 0;
    walletId = json['walletId'];
  }
  WhlWalletModel(
      {this.balance,
      this.withdrawal,
      this.unCanWithDrawCoin,
      this.withdrawalAmount});
}

//充值金额
class WhlWalletRechargeItemModel {
  WhlWalletRechargeItemModel({
    this.id,
    this.nowPrice,
    this.originalPrice,
    this.nowMoney,
    this.nowGiveMoney,
    this.nowFirstCharge,
    this.effectType,
    this.endTime,
    this.discount,
  });
  String? id;
  //现价金额
  int? nowPrice;

  //原价金额
  int? originalPrice;

  //得到的C币
  int? nowMoney;
  //得到的C币
  int? nowGiveMoney;
  //得到的C币
  int? nowFirstCharge;
  //得到的C币
  int? effectType;
  String? endTime;
  String? discount;

  WhlWalletRechargeItemModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nowPrice = json['nowPrice'];
    originalPrice = json['originalPrice'];
    nowMoney = json['nowMoney'];
    nowGiveMoney = json['nowGiveMoney'];
    nowFirstCharge = json['nowFirstCharge'];
    effectType = json['effectType'];
    endTime = json['endTime'];
    discount = json['discount'];
  }
}

//支付方式
class WhlWalletPayTypeModel {
  WhlWalletPayTypeModel(
      {required this.key, required this.image, required this.title});

  final String key; //1支付宝 2微信
  final String image;
  final String title;
}
