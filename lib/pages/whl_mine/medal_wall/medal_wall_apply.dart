import 'package:cave_flutter/common/common_widget.dart';
import 'package:cave_flutter/common/upload_photo_widget.dart';
import 'package:cave_flutter/model/base_source_model.dart';
import 'package:cave_flutter/pages/whl_mine/medal_wall/medal_wall_apply_logic.dart';
import 'package:cave_flutter/pages/whl_mine/medal_wall/medal_wall_logic.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:cave_flutter/widgets/brick/widget/basic_widget.dart';
import 'package:cave_flutter/widgets/brick/widget/image_widget.dart';
import 'package:cave_flutter/widgets/brick/widget/text_widget.dart';
import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

class MedalWallApplyPage extends StatefulWidget {
  final MedalModel data;
  MedalWallApplyPage({Key? key, required this.data}) : super(key: key);

  @override
  State<MedalWallApplyPage> createState() => _MedalWallApplyPageState();
}

class _MedalWallApplyPageState extends State<MedalWallApplyPage> {
  final logic = Get.put(MedalWallApplyLogic());

  @override
  void initState() {
    super.initState();
    logic.onInit();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppWhiteColor,
      body: UIColumn(crossAxisAlignment: CrossAxisAlignment.start, children: [
        UIRow(
          margin: EdgeInsets.only(
            top: ScreenUtil().statusBarHeight + 10.w,
            left: 10.w,
            right: 10.w,
          ),
          children: [
            UIImage(
              padding: EdgeInsets.all(5.w),
              assetImage: "icon_back_black",
              width: 30.w,
              height: 30.w,
              onTap: () {
                Get.back();
              },
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(right: 30.w),
                child: Center(
                  child: Text(
                    "成就认证系统",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 16.sp,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
        Expanded(
            child: UIWrap(
          runSpacing: 20.h,
          padding: EdgeInsets.symmetric(horizontal: 20.w).copyWith(top: 34.h),
          children: [
            UIRow(
              children: [
                UIText(
                  margin: EdgeInsets.only(right: 20.w),
                  text: '申请内容',
                  fontSize: 13.sp,
                  fontWeight: FontWeight.bold,
                ),
                Expanded(
                  child: UIContainer(
                    height: 68.h,
                    width: 68.w,
                    decoration: BoxDecoration(color: kAppColor('#FAFAFA')),
                    child: UIRow(
                      children: [
                        UIImage(
                          margin: EdgeInsets.symmetric(
                            // vertical: 20.h,
                            horizontal: 10.w,
                          ),
                          httpImage: widget.data.imageUrl?.toImageUrl(),
                          width: 30.w,
                          height: 30.h,
                        ),
                        UIText(
                          text: widget.data.medalName,
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
            UIRow(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                UIText(
                  margin: EdgeInsets.only(right: 20.w),
                  text: '申请理由',
                  fontSize: 13.sp,
                  fontWeight: FontWeight.bold,
                ),
                Expanded(
                  child: UIContainer(
                    decoration: BoxDecoration(color: kAppColor('#FAFAFA')),
                    // height: 105,
                    child: noBorderCTextField(
                      hint: "请详细说说你的申请理由",
                      hintTextColor: kAppSub3TextColor,
                      controller: logic.textEditingController,
                      maxLine: 5,
                      maxLength: 512,
                      contentPadding: EdgeInsets.symmetric(
                        vertical: 10.h,
                        horizontal: 15.w,
                      ),
                      onChanged: (value) {
                        if (value.length > 512) {
                          MyToast.show('最多512个字符');
                        }
                      },
                    ),
                  ),
                ),
              ],
            ),
            UIRow(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                UIText(
                  margin: EdgeInsets.only(right: 20.w),
                  text: '证明材料',
                  fontSize: 13.sp,
                  fontWeight: FontWeight.bold,
                ),
                Expanded(
                    child: UIColumn(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      '根据认证勋章不同，认证要求也不同，需要有视频上传功能',
                      style: TextStyle(
                        fontSize: 10.sp,
                        color: kAppColor('#C1C1C1'),
                      ),
                    ),
                    createUploadPhotoView(widget.data)
                  ],
                )),
              ],
            ),
          ],
        ))
      ]),
      bottomNavigationBar: UISolidButton(
        margin: EdgeInsets.symmetric(horizontal: 20.w).copyWith(bottom: 20.h),
        color: kAppColor('#000000'),
        onTap: () {
          logic.onApply(widget.data);
        },
        textColor: kAppWhiteColor,
        text: '提交',
      ),
    );
  }

  Widget createUploadPhotoView(MedalModel model) {
    return UploadPhotoWidget(
      maxPhotoCount: 6,
      photoData: logic.materials,
      photoChangeCallBack: (value) {
        // logic.bindPhoto(value);
      },
      photoDeleteCallBack: (deleteModel) {
        // logic.removePhoto(deleteModel);
      },
    );
  }
}
