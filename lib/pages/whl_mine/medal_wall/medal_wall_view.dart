import 'package:cave_flutter/config/whl_global_config.dart';
import 'package:cave_flutter/model/base_source_model.dart';
import 'package:cave_flutter/model/gift_model.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:cave_flutter/utils/whl_user_utils.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/empty_widget.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'medal_wall_logic.dart';

class MedalWallPage extends StatelessWidget {
  MedalWallPage({Key? key}) : super(key: key);

  final logic = Get.put(MedalWallLogic());

  @override
  Widget build(BuildContext context) {
    int rowItemCount = 4;
    double itemWidth =
        (ScreenUtil().screenWidth - 5.w * (rowItemCount - 1) - 20.w) /
            rowItemCount;
    return Scaffold(
      backgroundColor: kAppColor('#000000'),
      // appBar: WhlAppBar(
      //   centerTitle: "勋章墙",
      // ),
      body: GetBuilder<MedalWallLogic>(
        builder: (logic) {
          return UIColumn(
            children: [
              UIRow(
                mainAxisAlignment: MainAxisAlignment.center,
                margin: EdgeInsets.only(
                  top: ScreenUtil().statusBarHeight + 10.w,
                  left: 10.w,
                  right: 10.w,
                ),
                children: [
                  Expanded(
                      child: Stack(
                    children: [
                      UIImage(
                        padding: EdgeInsets.all(5.w),
                        assetImage: "icon_back_black",
                        imageColor: kAppWhiteColor,
                        width: 30.w,
                        height: 30.w,
                        onTap: () {
                          Get.back();
                        },
                      ),
                      Container(
                        // margin: EdgeInsets.only(right: 30.w),
                        child: Center(
                          child: Text(
                            "勋章墙",
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontSize: 16.sp,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ))
                ],
              ),
              SizedBox(
                height: 30.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.w),
                child: Stack(
                  clipBehavior: Clip.none,
                  alignment: Alignment.topCenter,
                  children: [
                    // Positioned(
                    //   top: 20.h,
                    //   width: double.infinity,
                    //   height: 136.h,
                    //   child: UIImage(
                    //     // height: 136.h,
                    //     // padding: EdgeInsets.symmetric(horizontal: 20.w),
                    //     assetImage: 'medal_top_bg.png',
                    //     width: double.infinity,
                    //   ),
                    // ),
                    UIImage(
                      assetImage: 'medal_top_bg.png',
                      width: double.infinity,
                    ),
                    UIText(
                      text: WhlUserUtils.getNickName(),
                      textColor: kAppWhiteColor,
                      fontSize: 15.sp,
                    ),

                    UIContainer(
                      margin: EdgeInsets.only(top: 84.h),
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          UIImage(
                            width: 120.w,
                            assetImage: 'medal_top_t.png',
                          ),
                          UIText(
                            text:
                                '已超越${logic.ranking}用户', // WhlUserUtils.getNickName(),
                            textColor: kAppColor('#FEC645'),
                            fontSize: 13.sp,
                            textAlign: TextAlign.center,
                          )
                        ],
                      ),
                    ),

                    Positioned(
                      top: -16.h,
                      child: UIColumn(
                        children: [
                          UIImage(
                            margin: EdgeInsets.only(bottom: 2.h),
                            alignment: Alignment.center,
                            httpImage: WhlUserUtils.getAvatar().toImageUrl(),
                            assetPlaceHolder:
                                WHLGlobalConfig.userPlaceholderAvatar,
                            width: 64.w,
                            height: 64.w,
                            radius: 32.w,
                            fit: BoxFit.cover,
                            clipBehavior: Clip.hardEdge,
                          ),
                          UIText(
                            text: WhlUserUtils.getNickName(),
                            textColor: kAppWhiteColor,
                            fontSize: 15.sp,
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                      top: -80.h,
                      child: UIImage(
                        padding: EdgeInsets.only(left: 20.w),
                        height: 320.h,
                        assetImage: 'medal_light.png',
                      ),
                    ),
                  ],
                ),
              ),
              GetBuilder<MedalWallLogic>(builder: (logic) {
                return UIContainer(
                    margin: EdgeInsets.only(top: 8.h, bottom: 10.h),
                    child: UIRow(
                      children: [
                        const Expanded(
                          child: UIImage(
                            assetImage: 'medal_star_2.png',
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 4.w),
                          child: Text.rich(
                            TextSpan(
                              text: "已点亮",
                              style: TextStyle(
                                fontWeight: FontWeight.w700,
                                color: kAppColor("#F7D08D"),
                                fontSize: 12.sp,
                                fontFamily: 'D-DIN-Bold',
                              ),
                              children: [
                                TextSpan(
                                    text: "  ${logic.haveMedalCount}",
                                    style: TextStyle(
                                        color: kAppColor("#F7D08D"),
                                        fontSize: 12.sp)),
                                TextSpan(
                                    text: "/${logic.medalCount}",
                                    style: TextStyle(
                                        color: kAppWhiteColor.withOpacity(0.66),
                                        fontSize: 12.sp)),
                              ],
                            ),
                          ),
                        ),
                        const Expanded(
                          child: UIImage(
                            assetImage: 'medal_star_1.png',
                          ),
                        ),
                      ],
                    ));
              }),
              TabBar(
                controller: logic.tabController,
                labelColor: Colors.white,
                labelStyle: TextStyle(
                  fontSize: 15.sp,
                  color: kAppColor('#151718'),
                  fontWeight: FontWeight.bold,
                ),
                unselectedLabelStyle: TextStyle(
                  fontSize: 15.sp,
                  color: kAppColor('#979797'),
                ),
                indicatorColor: kAppColor('#FFE800'),
                indicatorSize: TabBarIndicatorSize.label,
                indicatorWeight: 3.0,
                isScrollable: false,
                indicatorPadding: EdgeInsets.symmetric(horizontal: 12.w),
                unselectedLabelColor: kAppColor('#979797'),
                tabs: const <Tab>[
                  Tab(text: '已解锁'),
                  Tab(text: '达成自动解锁'),
                  Tab(text: '手动申请勋章')
                ],
              ),
              Expanded(
                child: TabBarView(controller: logic.tabController, children: [
                  if (logic.unlockWall.isEmpty)
                    UIContainer(
                      alignment: Alignment.center,
                      child: UIText(
                        text: '你目前还没有解锁勋章',
                        textColor: kAppWhiteColor,
                        fontSize: 16.sp,
                      ),
                    )
                  else
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 25.w)
                          .copyWith(bottom: 20.h),
                      child: GridView.builder(
                        shrinkWrap: true,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3,
                          mainAxisSpacing: 25.w,
                          crossAxisSpacing: 25.w,
                          childAspectRatio: itemWidth / (itemWidth + 20),
                        ),
                        itemBuilder: (context, index) {
                          return getMedalItem(logic.unlockWall[index]);
                        },
                        itemCount: logic.unlockWall.length,
                      ),
                    ),
                  if (logic.automaticWall.isEmpty)
                    UIContainer(
                      alignment: Alignment.center,
                      child: UIText(
                        text: '目前没有达成自动解锁的勋章',
                        textColor: kAppWhiteColor,
                        fontSize: 16.sp,
                      ),
                    )
                  else
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 25.w)
                          .copyWith(bottom: 20.h),
                      child: GridView.builder(
                        shrinkWrap: true,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3,
                          mainAxisSpacing: 25.w,
                          crossAxisSpacing: 25.w,
                          childAspectRatio: itemWidth / (itemWidth + 20),
                        ),
                        itemBuilder: (context, index) {
                          return getMedalItem(logic.automaticWall[index]);
                        },
                        itemCount: logic.automaticWall.length,
                      ),
                    ),
                  if (logic.handWall.isEmpty)
                    UIContainer(
                      alignment: Alignment.center,
                      child: UIText(
                        text: '目前没有手动申请的勋章',
                        textColor: kAppWhiteColor,
                        fontSize: 16.sp,
                      ),
                    )
                  else
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 25.w)
                          .copyWith(bottom: 20.h),
                      child: GridView.builder(
                        shrinkWrap: true,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3,
                          mainAxisSpacing: 25.w,
                          crossAxisSpacing: 25.w,
                          childAspectRatio: itemWidth / (itemWidth + 20),
                        ),
                        itemBuilder: (context, index) {
                          return getMedalItem(logic.handWall[index]);
                        },
                        itemCount: logic.handWall.length,
                      ),
                    ),
                ]),
              ),
            ],
          );
        },
      ),
      bottomNavigationBar: GetBuilder<MedalWallLogic>(builder: (logic) {
        if (logic.selectedMedal != null) {
          if (logic.selectedMedal!.haveTime == null) {
            return UISolidButton(
              margin:
                  EdgeInsets.symmetric(horizontal: 20.w).copyWith(bottom: 20.h),
              color: kAppColor('#D9D9D9'),
              onTap: () {
                logic.goDetail(logic.selectedMedal!);
              },
              textColor: kAppBlackColor,
              text: '点击查看解锁要求',
            );
          } else if (logic.selectedMedal!.haveTime != null) {
            return UISolidButton(
              margin:
                  EdgeInsets.symmetric(horizontal: 20.w).copyWith(bottom: 20.h),
              color: kAppColor('#FFE800'),
              onTap: () {
                logic.useMedal(logic.selectedMedal!);
              },
              textColor: kAppBlackColor,
              text: logic.selectedMedal!.wearing == true ? '摘下' : '佩戴',
            );
          }
        }
        return SizedBox();
      }),
    );
  }

  getMedalItem(MedalModel medal) {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        UIContainer(
          onTap: () {
            logic.selectMedal(medal);
          },
          alignment: Alignment.center,
          // width: double.infinity,
          padding: EdgeInsets.symmetric(
            vertical: 15.h,
            horizontal: 12.w,
          ),
          decoration: BoxDecoration(
            border: Border.all(
              color: medal.id == logic.selectedMedal?.id
                  ? kAppColor('#FFE800')
                  : Colors.transparent,
            ),
            color: kAppColor('#18181F'),
            borderRadius: BorderRadius.circular(
              8.r,
            ),
          ),
          child: UIColumn(
            children: [
              Expanded(
                child: ColorFiltered(
                  colorFilter: ColorFilter.mode(
                    medal.haveTime != null ? Colors.transparent : Colors.grey,
                    BlendMode.color,
                  ),
                  child: UIImage(
                    httpImage: medal.imageUrl?.toImageUrl(),
                    disabledHttpBigImage: true,
                    width: 60.w,
                    height: 60.h,
                    imageColor: Colors.transparent,
                    color: Colors.transparent,
                  ),
                ),
              ),
              UIText(
                padding: EdgeInsets.only(top: 6.h, bottom: 8.h),
                text: medal.medalName,
                textColor: kAppColor('#FEC645'),
                fontSize: 12.sp,
              ),
              if (medal.haveTime != null)
                UIText(
                  text: '${medal.haveTime}点亮',
                  fontSize: 8.sp,
                  textColor: kAppColor('#979797'),
                )
              else
                UIText(
                  text: medal.describe ?? '未解锁',
                  fontSize: 8.sp,
                  textColor: kAppColor('#979797'),
                ),
            ],
          ),
        ),
        if (medal.wearing == true)
          Positioned(
            top: 4.h,
            right: -4.w,
            child: UIContainer(
              decoration: BoxDecoration(
                border: Border.all(
                  color: kAppColor('#FEC645'),
                  width: 0.5,
                ),
              ),
              child: UIText(
                text: '佩戴中',
                textColor: kAppColor('#FEC645'),
                fontSize: 9.sp,
              ),
            ),
          )
      ],
    );
  }
}
