import 'package:cave_flutter/model/base_source_model.dart';
import 'package:cave_flutter/pages/whl_mine/medal_wall/medal_wall_apply.dart';
import 'package:cave_flutter/pages/whl_mine/medal_wall/medal_wall_logic.dart';
import 'package:cave_flutter/routes/whl_app_pages.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:cave_flutter/widgets/brick/widget/basic_widget.dart';
import 'package:cave_flutter/widgets/brick/widget/image_widget.dart';
import 'package:cave_flutter/widgets/brick/widget/text_widget.dart';
import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

class MedalWallDetailPage extends StatelessWidget {
  final logic = Get.put(MedalWallLogic());

  final MedalModel data;
  MedalWallDetailPage({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppColor('#000000'),
      body: UIColumn(children: [
        UIRow(
          margin: EdgeInsets.only(
            top: ScreenUtil().statusBarHeight + 10.w,
            left: 10.w,
            right: 10.w,
          ),
          children: [
            UIImage(
              padding: EdgeInsets.all(5.w),
              assetImage: "icon_back_black",
              imageColor: kAppWhiteColor,
              width: 30.w,
              height: 30.w,
              onTap: () {
                Get.back();
              },
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(right: 30.w),
                child: Center(
                  child: Text("成就认证系统",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontSize: 16.sp,
                          color: Colors.white,
                          fontWeight: FontWeight.bold)),
                ),
              ),
            )
          ],
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 20.w,
            ),
            child: UIColumn(
              children: [
                // UIImage(
                //   httpImage: data.imageUrl?.toImageUrl(),
                //   width: 159.w,
                //   height: 159.h,
                //   fit: BoxFit.fitWidth,
                //   margin: EdgeInsets.only(top: 50.h, bottom: 55.h),
                // ),
                ColorFiltered(
                  colorFilter: ColorFilter.mode(
                    data.haveTime != null ? Colors.transparent : Colors.grey,
                    BlendMode.color,
                  ),
                  child: UIImage(
                    disabledHttpBigImage: true,
                    margin: EdgeInsets.only(top: 50.h, bottom: 55.h),
                    httpImage: data.imageUrl?.toImageUrl(),
                    width: 159.w,
                    height: 159.h,
                    imageColor: Colors.transparent,
                    color: Colors.transparent,
                  ),
                ),
                UIText(
                  text: data.medalName,
                  fontSize: 20.sp,
                  textColor: kAppWhiteColor,
                  margin: EdgeInsets.only(bottom: 10.h),
                ),
                UIText(
                  text: data.haveTime != null ? '有效期：${data.haveTime}' : '未解锁',
                  fontSize: 11.sp,
                  textColor: kAppColor('#979797').withOpacity(0.7),
                ),
                if (data.describe != null)
                  Padding(
                    padding: EdgeInsets.only(top: 50.h),
                    child: Text(
                      '点亮条件：${data.describe}',
                      style: TextStyle(
                        color: kAppColor('#979797'),
                        fontSize: 12.sp,
                      ),
                      maxLines: 5,
                    ),
                  ),
              ],
            ),
          ),
        ),
      ]),
      bottomNavigationBar: data.haveTime != null
          ? UISolidButton(
              margin: EdgeInsets.symmetric(horizontal: 20.w).copyWith(
                bottom: 20.h,
              ),
              color: kAppColor('#FFE800'),
              onTap: () {
                logic.useMedal(logic.selectedMedal!);
              },
              textColor: kAppBlackColor,
              text: data.wearing == true ? '摘下' : '佩戴',
            )
          : data.unlockType == "1"
              ? UISolidButton(
                  margin: EdgeInsets.symmetric(horizontal: 20.w).copyWith(
                    top: 20.h,
                  ),
                  color: kAppColor('#1A1B1F'),
                  textColor: kAppColor('#484848'),
                  text: '未完成',
                )
              : UISolidButton(
                  margin: EdgeInsets.symmetric(horizontal: 20.w).copyWith(
                    bottom: 20.h,
                  ),
                  color: kAppColor('#FFE800'),
                  onTap: () {
                    Get.to(MedalWallApplyPage(
                      data: data,
                    ))?.then((value) {
                      print(value);
                    });
                  },
                  textColor: kAppBlackColor,
                  text: '立即申请',
                ),
    );
  }
}
