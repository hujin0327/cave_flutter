import 'package:cave_flutter/model/base_source_model.dart';
import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/pages/whl_mine/medal_wall/medal_wall_detail_view.dart';
import 'package:cave_flutter/pages/whl_mine/medal_wall/medal_wall_logic.dart';
import 'package:cave_flutter/routes/whl_app_pages.dart';
import 'package:cave_flutter/utils/task_utils.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MedalWallApplyLogic extends GetxController {
  List<dynamic> materials = List<dynamic>.from([]);
  TextEditingController textEditingController = TextEditingController();
  @override
  void onInit() {
    super.onInit();
    textEditingController.clear();
    materials.clear();
  }

  onApply(MedalModel data) {
    WhlApi.applyForMedal.post({
      'medalId': data.id,
      'content': textEditingController.text,
      'images': materials.map((e) => e.imagePath).toList()
    }).then((value) {
      if (value.isSuccess()) {
        MyToast.show('已提交申请');
        Get.back(result: value.data);
      }
    });
  }
}
