import 'dart:io';
import 'package:cave_flutter/config/whl_global_config.dart';
import 'package:cave_flutter/db_manager/bean/whl_product_bean.dart';
import 'package:cave_flutter/model/base_model.dart';
import 'package:cave_flutter/model/whl_user_info_model.dart';
import 'package:cave_flutter/network/dio_log/overlay_draggable_button.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:cave_flutter/utils/whl_user_utils.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/refresh_widget.dart';
import 'package:cave_flutter/widgets/whl_row_item.dart';
import 'package:floor/floor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../routes/whl_app_pages.dart';
import 'attention_fans/attention_fans_view.dart';
import 'edit_userinfo/edit_userinfo_logic.dart';
import 'whl_mine_logic.dart';

class WhlMinePage extends StatelessWidget {
  WhlMinePage({Key? key}) : super(key: key);

  final WhlMineLogic logic = Get.put(WhlMineLogic());
  final userInfoLogic = Get.put(Edit_userinfoLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // backgroundColor: kAppColor('#F8F8F8'),
        body: Stack(
      children: [
        UIContainer(
          height: double.maxFinite,
          width: double.maxFinite,
          gradientStartColor: kAppColor('#EEEDED'),
          gradientEndColor: kAppColor('#F9F9F9'),
          gradientBegin: Alignment.topCenter,
          gradientEnd: Alignment.bottomCenter,
        ),
        UIImage(
          assetImage: 'img_bg_mine_top.png',
          width: double.maxFinite,
          height: 236.h,
        ),
        GetBuilder<WhlMineLogic>(builder: (logic) {
          return Column(
            children: [
              buildTopView(context),
              Expanded(
                  child: MyRefresh(
                controller: logic.easyRefreshController,
                onRefresh: () {
                  logic.refreshAction();
                },
                child: CustomScrollView(
                  slivers: [
                    SliverToBoxAdapter(
                      child: Column(
                        children: [
                          buildUserInfoView(),
                          buildMyStatisticsView(),
                          buildTabListView(),
                          buildCenterBanner(),
                        ],
                      ),
                    ),
                    buildListView(),
                  ],
                ),
              ))
            ],
          );
        })
      ],
    ));
  }

  UIRow buildTopView(context) {
    return UIRow(
      margin: EdgeInsets.only(
          left: 20.w, top: ScreenUtil().statusBarHeight + 8.h, right: 20.w),
      children: [
        Expanded(child: Container()),
        GetBuilder<Edit_userinfoLogic>(builder: (userInfoLogic) {
          if (userInfoLogic.progress < 100) {
            return UIStack(
              alignment: Alignment.center,
              margin: EdgeInsets.only(right: 4.w),
              children: [
                UIImage(
                  assetImage: 'img_bg_mine_completion.png',
                  height: 33.w,
                ),
                UIText(
                  text: '资料完成${(userInfoLogic.progress * 100).ceil()}%',
                  textColor: kAppTextColor,
                  fontSize: 11.sp,
                )
              ],
            );
          } else {
            return const SizedBox();
          }
        }),
        UIImage(
          assetImage: 'icon_mine_edit.png',
          width: 33.w,
          onTap: () {
            userInfoLogic.onInit();
            logic.toEditUserInfoPage();
          },
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.07),
              offset: const Offset(0, 8),
              blurRadius: 30,
            )
          ]),
        ),
        UIImage(
          margin: EdgeInsets.only(left: 7.w),
          assetImage: 'icon_mine_setting.png',
          width: 33.w,
          onTap: () {
            logic.doClickSetting();
          },
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.07),
              offset: const Offset(0, 8),
              blurRadius: 30,
            )
          ]),
        )
      ],
    );
  }

  buildUserInfoView() {
    // WhlUserInfoModel model = WhlUserUtils.getUserInfo();
    return UIRow(
      margin: EdgeInsets.only(top: 12.h, left: 24.w, right: 24.w),
      children: [
        UIImage(
          onTap: () {
            logic.doClickListViewItem(BaseModel(mineType: MineType.myProduct));
          },
          margin: EdgeInsets.only(right: 10.w),
          disabledHttpBigImage: true,
          httpImage: WhlUserUtils.getUserInfo().avatar?.toImageUrl(),
          assetPlaceHolder: WHLGlobalConfig.userPlaceholderAvatar,
          width: 67.w,
          height: 67.w,
          radius: 34.w,
          strokeColor: Colors.white,
          strokeWidth: 2.w,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UIRow(
              margin: EdgeInsets.only(bottom: 4.h),
              children: [
                UIText(
                  text: logic.userInfoModel.nickname,
                  textColor: kAppTextColor,
                  fontSize: 22.sp,
                  fontWeight: FontWeight.bold,
                ),
                UIImage(
                  margin: EdgeInsets.only(left: 4.w),
                  httpImage: logic.userInfoModel.levelImage?.toImageUrl(),
                  disabledHttpBigImage: true,
                  width: 18.w,
                  height: 18.w,
                ),
                // UIImage(
                //   margin: EdgeInsets.only(left: 4.w),
                //   assetImage: 'icon_svip.png',
                //   width: 51.w,
                // )
              ],
            ),
            Row(
              children: [
                UIText(
                  padding: EdgeInsets.symmetric(horizontal: 6.w, vertical: 2.w),
                  color: kAppColor('#F6F6F6'),
                  text: '财富值${logic.userInfoModel.wealth ?? 0}',
                  textColor: kAppTextColor,
                  fontSize: 11.sp,
                  radius: 6.w,
                ),
                UIText(
                  margin: EdgeInsets.only(left: 5.w),
                  padding: EdgeInsets.symmetric(horizontal: 6.w, vertical: 2.w),
                  color: kAppColor('#F6F6F6'),
                  text: '魅力值${logic.userInfoModel.charm ?? 0}',
                  textColor: kAppTextColor,
                  fontSize: 11.sp,
                  radius: 6.w,
                ),
              ],
            ),
            UIText(
              margin: EdgeInsets.only(left: 7.w, top: 7.h),
              text: 'ID:${logic.userInfoModel.accountId}',
              textColor: kAppTextColor.withOpacity(0.3),
              fontSize: 10.sp,
            )
          ],
        )
      ],
    );
  }

  buildMyStatisticsView() {
    return UIRow(
      margin: EdgeInsets.only(top: 30.w),
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        UIText(
          text: '关注',
          textColor: kAppColor('#484A49'),
          fontSize: 11.sp,
          marginDrawable: 7.w,
          topDrawable: UIText(
            text: '${logic.userInfoModel.followCount ?? 0}',
            textColor: kAppTextColor,
            fontSize: 15.sp,
            fontWeight: FontWeight.bold,
          ),
          onTap: () {
            Get.toNamed(Routes.attentionFansPage, arguments: 0);
          },
        ),
        UIText(
          text: '粉丝',
          textColor: kAppColor('#484A49'),
          fontSize: 11.sp,
          marginDrawable: 7.w,
          topDrawable: UIText(
            text: '${logic.userInfoModel.fansCount ?? 0}',
            textColor: kAppTextColor,
            fontSize: 15.sp,
            fontWeight: FontWeight.bold,
          ),
          onTap: () {
            Get.toNamed(Routes.attentionFansPage, arguments: 1);
          },
        ),
        UIText(
          text: '访客',
          textColor: kAppColor('#484A49'),
          fontSize: 11.sp,
          marginDrawable: 7.w,
          topDrawable: UIText(
            text: '${logic.userInfoModel.visitCount ?? 0}',
            textColor: kAppTextColor,
            fontSize: 15.sp,
            fontWeight: FontWeight.bold,
          ),
          onTap: () {
            Get.toNamed(Routes.visitors);
          },
        ),
        UIText(
          text: '我的Cave',
          textColor: kAppColor('#484A49'),
          marginDrawable: 7.w,
          fontSize: 11.sp,
          // topDrawable: Stack(
          //   clipBehavior: Clip.none,
          //   children: [
          //     for (int i = 0; i < 2; i++)
          //       UIImage(
          //         assetImage: 'icon_app_logo.png',
          //         margin: EdgeInsets.only(left: i * 20.w),
          //         strokeColor: Colors.white,
          //         strokeWidth: 1.w,
          //         width: 29.w,
          //         height: 29.w,
          //         radius: 22.w,
          //       )
          //   ],
          // ),

          topDrawable: UIText(
            text: '${logic.userInfoModel.caveCount ?? 0}',
            textColor: kAppTextColor,
            fontSize: 15.sp,
            fontWeight: FontWeight.bold,
          ),
          onTap: () {
            Get.toNamed(Routes.attentionFansPage, arguments: 2);
          },
        )
      ],
    );
  }

  buildTabListView() {
    return UIRow(
      margin: EdgeInsets.only(left: 18.w, right: 18.w, top: 20.h),
      padding: EdgeInsets.only(top: 9.w, bottom: 12.w),
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      color: Colors.white,
      radius: 12.w,
      children: logic.tabList.map((e) {
        return UIText(
          text: e.title,
          textColor: kAppColor('#2D2D2D'),
          fontSize: 12.sp,
          topDrawable: UIImage(
            assetImage: e.icon,
            width: 43.w,
            height: 43.w,
          ),
          onTap: () {
            logic.toNextTabPage(e.tabType);
          },
        );
      }).toList(),
    );
  }

  buildCenterBanner() {
    return UIImage(
      margin: EdgeInsets.only(top: 13.h),
      assetImage: 'img_bg_mine_banner.png',
      height: 63.w,
      fit: BoxFit.fill,
      onTap: () {
        Get.toNamed(Routes.share);
      },
    );
  }

  buildListView() {
    return SliverList(
        delegate: SliverChildListDelegate([
      UIContainer(
        margin:
            EdgeInsets.only(top: 14.h, left: 18.w, right: 18.w, bottom: 14.h),
        radius: 12.w,
        color: Colors.white,
        child: ListView.builder(
          shrinkWrap: true,
          padding: EdgeInsets.zero,
          physics: const NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            BaseModel model = logic.mineList[index];
            return MyRowItem(
              padding: EdgeInsets.symmetric(vertical: 15.h),
              margin: EdgeInsets.symmetric(horizontal: 17.w),
              titleColor: kAppColor('#2D2D2D'),
              title: model.title,
              // value: logic.mineList[index].value,
              onTap: () {
                logic.doClickListViewItem(model);
              },
            );
          },
          itemCount: logic.mineList.length,
        ),
      )
    ]));
  }
}
