import 'package:cave_flutter/pages/whl_home/rank_list_widget.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/task_utils.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'my_ranking_logic.dart';

class My_rankingPage extends StatelessWidget {
  My_rankingPage({Key? key}) : super(key: key);

  final logic = Get.put(My_rankingLogic());

  @override
  Widget build(BuildContext context) {
    TaskUtils.complateTask(16, type: 2);
    return Scaffold(
      backgroundColor: kAppWhiteColor,
      appBar: WhlAppBar(
        backgroundColor: kAppWhiteColor,
        isShowBottomLine: false,
      ),
      body: RankListWidget(),
    );
  }
}
