import 'package:cave_flutter/pages/whl_mine/personality_test/personality_test_logic.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/widget/basic_widget.dart';
import 'package:cave_flutter/widgets/brick/widget/image_widget.dart';
import 'package:cave_flutter/widgets/brick/widget/text_widget.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class PersonalityTestPage extends StatefulWidget {
  const PersonalityTestPage({super.key});

  @override
  State<PersonalityTestPage> createState() => _PersonalityTestPageState();
}

class _PersonalityTestPageState extends State<PersonalityTestPage> {
  late PersonalityTestLogic logic;
  @override
  void initState() {
    super.initState();
    logic = Get.put(PersonalityTestLogic());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppBlackColor,
      appBar: WhlAppBar(
        centerTitle: '人格测试',
        backgroundColor: kAppBlackColor,
        isShowBottomLine: false,
      ),
      body: GetBuilder<PersonalityTestLogic>(builder: (logic) {
        if (logic.answerList.length < logic.questionList.length) {
          PersonalityQuestionModel questionModel =
              logic.questionList[logic.currentIndex];
          return UIColumn(
            margin: EdgeInsets.symmetric(
              horizontal: 20.w,
              vertical: 20.h,
            ),
            children: [
              UIContainer(
                constraints: const BoxConstraints(
                  minHeight: 120,
                ),
                width: double.infinity,
                padding: EdgeInsets.symmetric(horizontal: 12.w, vertical: 12.h),
                decoration: BoxDecoration(
                  color: kAppColor('#33a474'),
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Text(
                  questionModel.title ?? '',
                  style: TextStyle(color: kAppWhiteColor),
                  overflow: TextOverflow.visible,
                  softWrap: true,
                ),
              ),
              UIContainer(
                width: double.infinity,
                padding: EdgeInsets.only(bottom: 20.h),
                child: Text(
                  '${logic.currentIndex + 1}/${logic.questionList.length}',
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    color: kAppWhiteColor,
                  ),
                ),
              ),
              ...logic.questionList[logic.currentIndex].options!.map((option) {
                return UISolidButton(
                  onTap: () {
                    logic.answerOption(questionModel.id!, option);
                  },
                  text: option.title,
                  color: kAppWhiteColor,
                  textColor: kAppBlackColor,
                  fontSize: 12.sp,
                  margin: EdgeInsets.only(bottom: 8.h),
                );
              }).toList(),
              UISolidButton(
                margin: EdgeInsets.only(top: 20.h),
                text: '返回上一项',
                onTap: () {
                  logic.backLast();
                },
              )
            ],
          );
        } else {
          return UIColumn(
            margin: EdgeInsets.symmetric(
              horizontal: 20.w,
            ),
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              UIText(
                text: '人格测试完毕',
                textColor: kAppWhiteColor,
                margin: EdgeInsets.only(bottom: 20.h),
                fontSize: 24.sp,
              ),
              UISolidButton(
                text: '重新测试',
                onTap: () {
                  logic.resetAnswer();
                },
              )
            ],
          );
        }
      }),
    );
  }
}
