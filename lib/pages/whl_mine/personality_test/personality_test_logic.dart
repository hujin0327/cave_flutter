import 'package:cave_flutter/model/base_source_model.dart';
import 'package:get/get.dart';

import 'questions.dart';

class PersonalityTestLogic extends GetxController {
  int currentIndex = 0;
  late List<PersonalityQuestionModel> questionList;
  late List<PersonalityAnswrModel> answerList;
  @override
  void onInit() {
    super.onInit();
    questionList = questions.map((e) {
      return PersonalityQuestionModel(
        id: e['assessment_question_id'],
        title: e['question_description'],
        options: List<BaseItemModel>.from(
          e['options'].map((op) {
            return BaseItemModel(title: op['option']);
          }).toList(),
        ),
      );
    }).toList();
    answerList = [];
    currentIndex = answerList.length;
  }

  backLast() {
    answerList.removeLast();
    currentIndex = answerList.length;
    update();
  }

  resetAnswer() {
    answerList.clear();
    currentIndex = 0;
    update();
  }

  answerOption(int questionId, BaseItemModel option) {
    if (answerList.length < questionList.length) {
      answerList
          .add(PersonalityAnswrModel(id: questionId, answer: option.title));
      currentIndex = answerList.length;
      update();
    } else {}
  }
}

class PersonalityAnswrModel {
  int? id;
  String? answer;
  PersonalityAnswrModel({this.id, this.answer});
}

class PersonalityQuestionModel {
  int? id;
  String? title;
  String? description;

  List<BaseItemModel>? options;

  PersonalityQuestionModel({
    this.id,
    this.title,
    this.description,
    this.options,
  });
}
