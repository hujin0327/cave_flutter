const List<Map<String, dynamic>> questions = [
  {
    "assessment_question_id": 3879942438,
    "question_id_str": "qp_1467",
    "question_description": "你常看到感人的电视或电影情景看到泪流不止",
    "options": [
      {"option": "是的"},
      {"option": "我很少因为这样的事情哭，只有特别伤心的事发生在自己身上时才会哭"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/1.mp3"
  },
  {
    "assessment_question_id": 3879942440,
    "question_id_str": "qp_1468",
    "question_description": "准备搭车远行时，哪种更符合你",
    "options": [
      {"option": "临行前才匆匆忙忙收拾，赶到车站车已经差不多要出发了……谁叫我是个慢性子呢"},
      {"option": "早早收拾好行李，第二天提前很早就去到车站，发现我是第一个来候车的人"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/2.mp3"
  },
  {
    "assessment_question_id": 3879942442,
    "question_id_str": "qp_1469",
    "question_description": "马上能叫出五个朋友的名字",
    "options": [
      {"option": "不要说是5个，让我说出10个人也没问题呀"},
      {"option": "突然让我叫出5个人的名字，想不起来呀！天啊，难道我朋友这么少吗"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/3.mp3"
  },
  {
    "assessment_question_id": 3879942444,
    "question_id_str": "qp_1470",
    "question_description": "只要有爱，什么都可以原谅",
    "options": [
      {"option": "不认同，就算相爱，凡事都是有分寸的"},
      {"option": "认同，爱情能冲淡一切不快"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/4.mp3"
  },
  {
    "assessment_question_id": 3879942446,
    "question_id_str": "qp_1471",
    "question_description": "下面哪个更符合你",
    "options": [
      {"option": "在不与人接触的场合，我充满了生命的愉悦"},
      {"option": "哪怕喧嚣嘈杂一点，也比一个人孤伶伶呆着强"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/5.mp3"
  },
  {
    "assessment_question_id": 3879942448,
    "question_id_str": "qp_1472",
    "question_description": "喜欢装饰",
    "options": [
      {"option": "是，喜欢漂亮的，可爱的，华丽的"},
      {"option": "不，服装嘛，只要有一件T恤，一条牛仔裤就足够了"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/6.mp3"
  },
  {
    "assessment_question_id": 3879942450,
    "question_id_str": "qp_1473",
    "question_description": "很容易与他人成为朋友",
    "options": [
      {"option": "是的，不管对方是谁，我都能和他和睦相处"},
      {"option": "没有必要和所有人都成为朋友，有几个真正的知己就可以"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/7.mp3"
  },
  {
    "assessment_question_id": 3879942452,
    "question_id_str": "qp_1474",
    "question_description": "哪种更接近朋友对你的评价",
    "options": [
      {"option": "外表平静，大家都说我闷骚"},
      {"option": "开朗活泼，大家都说我太聒噪了"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/8.mp3"
  },
  {
    "assessment_question_id": 3879942454,
    "question_id_str": "qp_1475",
    "question_description": "你是此类型的人",
    "options": [
      {"option": "喜欢在一段时间里专心于一件事情直到完成"},
      {"option": "享受同时进行好几件事情"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/9.mp3"
  },
  {
    "assessment_question_id": 3879942456,
    "question_id_str": "qp_1476",
    "question_description": "与朋友交往时，你倾向于看重",
    "options": [
      {"option": "情感上的相容性：对对方情感上的需求很敏感"},
      {"option": "智慧上的相容性：沟通重要的想法；客观地讨论和辩论事情"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/10.mp3"
  },
  {
    "assessment_question_id": 3879942458,
    "question_id_str": "qp_1477",
    "question_description": "认识你的人倾向于形容你为",
    "options": [
      {"option": "逻辑和明确"},
      {"option": "热情而敏感"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/11.mp3"
  },
  {
    "assessment_question_id": 3879942460,
    "question_id_str": "qp_1478",
    "question_description": "你喜欢",
    "options": [
      {"option": "有部署、有节奏的工作"},
      {"option": "有灵活性、较为松散的工作"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/12.mp3"
  },
  {
    "assessment_question_id": 3879942462,
    "question_id_str": "qp_1479",
    "question_description": "你乐于拥有广泛的人际圈",
    "options": [
      {"option": "是的"},
      {"option": "如果可以选择的话，我更愿意一个人静静呆着"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/13.mp3"
  },
  {
    "assessment_question_id": 3879942464,
    "question_id_str": "qp_1480",
    "question_description": "你看肥皂剧的时候会很投入",
    "options": [
      {"option": "是的"},
      {"option": "不会"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/14.mp3"
  },
  {
    "assessment_question_id": 3879942466,
    "question_id_str": "qp_1481",
    "question_description": "电话铃声突然响起或类似这样的突发性事件，你习惯最先做出回应",
    "options": [
      {"option": "是的"},
      {"option": "不会"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/15.mp3"
  },
  {
    "assessment_question_id": 3879942468,
    "question_id_str": "qp_1482",
    "question_description": "热衷于猎奇和掌握新概念",
    "options": [
      {"option": "是的"},
      {"option": "不会"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/16.mp3"
  },
  {
    "assessment_question_id": 3879942470,
    "question_id_str": "qp_1483",
    "question_description": "当受到情感上的伤害或挫折时",
    "options": [
      {"option": "虽然我觉得受伤，但一旦想通，就会很快从阴影中走出来"},
      {"option": "我通常让自己的情绪深陷其中，很难走出来"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/17.mp3"
  },
  {
    "assessment_question_id": 3879942472,
    "question_id_str": "qp_1484",
    "question_description": "你选择的生活充满着",
    "options": [
      {"option": "自然发生和弹性"},
      {"option": "日程表和组织"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/18.mp3"
  },
  {
    "assessment_question_id": 3879942474,
    "question_id_str": "qp_1485",
    "question_description": "在同学聚会中，你通常",
    "options": [
      {"option": "较安静并保留，直到我觉得舒服"},
      {"option": "整体来说很健谈"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/19.mp3"
  },
  {
    "assessment_question_id": 3879942476,
    "question_id_str": "qp_1486",
    "question_description": "当你对一个朋友放松聊天时，你偏向谈论",
    "options": [
      {"option": "实际的、具体的、关于“此时此地”的事物。例如，我也许会谈论即将要参加的旅程"},
      {"option": "未来，关于改进或发明事物和生活的种种可能性。例如，我也许会谈论一个新的科学发明"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/20.mp3"
  },
  {
    "assessment_question_id": 3879942478,
    "question_id_str": "qp_1487",
    "question_description": "你经常考虑人类及其命运问题",
    "options": [
      {"option": "是的"},
      {"option": "不会"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/21.mp3"
  },
  {
    "assessment_question_id": 3879942480,
    "question_id_str": "qp_1488",
    "question_description": "你有时候会心血来潮做一件事情",
    "options": [
      {"option": "是的"},
      {"option": "极少会这样"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/22.mp3"
  },
  {
    "assessment_question_id": 3879942482,
    "question_id_str": "qp_1489",
    "question_description": "你倾向比较能够察觉到",
    "options": [
      {"option": "当人们不合逻辑时"},
      {"option": "当人们需要情感上的支持时"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/23.mp3"
  },
  {
    "assessment_question_id": 3879942484,
    "question_id_str": "qp_1490",
    "question_description": "你喜欢立刻行动而不习惯对各种选择进行反复斟酌",
    "options": [
      {"option": "是的"},
      {"option": "不是"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/24.mp3"
  },
  {
    "assessment_question_id": 3879942487,
    "question_id_str": "qp_1491",
    "question_description": "你更容易被认为是一个",
    "options": [
      {"option": "很理性的人"},
      {"option": "很重感情的人"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/25.mp3"
  },
  {
    "assessment_question_id": 3879942489,
    "question_id_str": "qp_1492",
    "question_description": "如你有一个约见，你是习惯临场发挥还是事先周密准备",
    "options": [
      {"option": "随机应变，临场发挥"},
      {"option": "不周密准备我不安心"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/26.mp3"
  },
  {
    "assessment_question_id": 3879942491,
    "question_id_str": "qp_1493",
    "question_description": "业余时间里你常与一群人积极交往，参加聚会、一起购物",
    "options": [
      {"option": "是的"},
      {"option": "不，我独自呆着比较舒服"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/27.mp3"
  },
  {
    "assessment_question_id": 3879942493,
    "question_id_str": "qp_1494",
    "question_description": "若你有时间和金钱，你的朋友邀请你到国外度假，并且在前一天才通知，你会",
    "options": [
      {"option": "必须先坚持我的时间表"},
      {"option": "立刻收拾行装"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/28.mp3"
  },
  {
    "assessment_question_id": 3879942495,
    "question_id_str": "qp_1495",
    "question_description": "哪种更符合你",
    "options": [
      {"option": "表面传统，其实内心是开放而热烈的，渴望挑战自己"},
      {"option": "表面很开放，但只是想做自己，内心其实是保守而传统的"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/29.mp3"
  },
  {
    "assessment_question_id": 3879942497,
    "question_id_str": "qp_1496",
    "question_description": "当遇到聚会中第一次见面的人对你很热情时，你会",
    "options": [
      {"option": "很高兴，情绪会受到感染，并对其产生好感"},
      {"option": "热情应付，但心存一丝疑虑：对方为什么要这样热情，是否有其他目的"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/30.mp3"
  },
  {
    "assessment_question_id": 3879942499,
    "question_id_str": "qp_1497",
    "question_description": "你懂得如何把每分每秒都花得有意义",
    "options": [
      {"option": "是的"},
      {"option": "不习惯计划时间，喜欢随心所欲"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/31.mp3"
  },
  {
    "assessment_question_id": 3879942501,
    "question_id_str": "qp_1498",
    "question_description": "总体而言，你是一个在交流时有些保守和有距离感的人",
    "options": [
      {"option": "是的"},
      {"option": "不是"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/32.mp3"
  },
  {
    "assessment_question_id": 3879942503,
    "question_id_str": "qp_1499",
    "question_description": "你时常容易流露和说出你的感觉和感情",
    "options": [
      {"option": "是的"},
      {"option": "不易流露感情"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/33.mp3"
  },
  {
    "assessment_question_id": 3879942505,
    "question_id_str": "qp_1500",
    "question_description": "经过冗长的社交之后，你会想要离开，独处一下",
    "options": [
      {"option": "是的，我常有这样的感觉"},
      {"option": "没有这种体会"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/34.mp3"
  },
  {
    "assessment_question_id": 3879942507,
    "question_id_str": "qp_1501",
    "question_description": "你常会站在对方角度去思考而模糊了自己的立场",
    "options": [
      {"option": "是的"},
      {"option": "不会"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/35.mp3"
  },
  {
    "assessment_question_id": 3879942509,
    "question_id_str": "qp_1502",
    "question_description": "你容易看出具体事件背后的普遍原理",
    "options": [
      {"option": "是的"},
      {"option": "不是"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/36.mp3"
  },
  {
    "assessment_question_id": 3879942511,
    "question_id_str": "qp_1503",
    "question_description": "当要去见陌生人时，你通常",
    "options": [
      {"option": "多少有些不情愿"},
      {"option": "不感到难为情"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/37.mp3"
  },
  {
    "assessment_question_id": 3879942513,
    "question_id_str": "qp_1504",
    "question_description": "你倾向通过以下哪种方式收集信息",
    "options": [
      {"option": "我对目前状况的实际认知"},
      {"option": "我对有可能发生之事的想象和期望"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/38.mp3"
  },
  {
    "assessment_question_id": 3879942515,
    "question_id_str": "qp_1505",
    "question_description": "对于应酬中逢场作戏、聪明圆融的人，你的看法是",
    "options": [
      {"option": "不理解，也无法接受人们快速地相熟热络，而又骨底淡薄"},
      {"option": "看应酬的目的是什么，只要目的是有益的，这些也都无所谓了"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/39.mp3"
  },
  {
    "assessment_question_id": 3879942517,
    "question_id_str": "qp_1506",
    "question_description": "你倾向如此做决定",
    "options": [
      {"option": "首先依我的逻辑，然后依我的心意"},
      {"option": "首先依我的心意，然后依我的逻辑"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/40.mp3"
  },
  {
    "assessment_question_id": 3879942519,
    "question_id_str": "qp_1507",
    "question_description": "大多数时候，你宁可看一本书，也不愿意出去参加聚会",
    "options": [
      {"option": "是的"},
      {"option": "我喜欢参加聚会"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/41.mp3"
  },
  {
    "assessment_question_id": 3879942521,
    "question_id_str": "qp_1508",
    "question_description": "大多数时候，你倾向于",
    "options": [
      {"option": "随机应变"},
      {"option": "事先计划"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/42.mp3"
  },
  {
    "assessment_question_id": 3879942522,
    "question_id_str": "qp_1509",
    "question_description": "你倾向从何处得到力量",
    "options": [
      {"option": "朋友"},
      {"option": "自己的想法"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/43.mp3"
  },
  {
    "assessment_question_id": 3879942524,
    "question_id_str": "qp_1510",
    "question_description": "当你不同意朋友的想法时",
    "options": [
      {"option": "我尽可能地避免伤害对方的感情；若是会对对方造成伤害的话，我就不会说"},
      {"option": "我通常毫无保留地说话，并且对朋友直言不讳，因为对的就是对的"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/44.mp3"
  },
  {
    "assessment_question_id": 3879942526,
    "question_id_str": "qp_1511",
    "question_description": "你更喜欢远离外界的喧嚣",
    "options": [
      {"option": "是的"},
      {"option": "不，我喜欢热闹"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/45.mp3"
  },
  {
    "assessment_question_id": 3879942528,
    "question_id_str": "qp_1512",
    "question_description": "当不得不主动同生人打交道时，你",
    "options": [
      {"option": "有点儿担心别人不愿被打搅"},
      {"option": "感到自信并且轻松"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/46.mp3"
  },
  {
    "assessment_question_id": 3879942530,
    "question_id_str": "qp_1513",
    "question_description": "你是一个时间观念很强的人",
    "options": [
      {"option": "是的"},
      {"option": "不是"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/47.mp3"
  },
  {
    "assessment_question_id": 3879942532,
    "question_id_str": "qp_1514",
    "question_description": "脑海里常冒出一些新点子",
    "options": [
      {"option": "是的"},
      {"option": "不是"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/48.mp3"
  },
  {
    "assessment_question_id": 3879942534,
    "question_id_str": "qp_1515",
    "question_description": "当和朋友约定好见面",
    "options": [
      {"option": "若我所约的人来迟了，我常会不高兴"},
      {"option": "一点儿都不在乎，因为我自己常常迟到"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/49.mp3"
  },
  {
    "assessment_question_id": 3879942536,
    "question_id_str": "qp_1516",
    "question_description": "把事物收拾得有条理，你就会感到高兴",
    "options": [
      {"option": "是的"},
      {"option": "不，我习惯自由自在和随意"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/50.mp3"
  },
  {
    "assessment_question_id": 3879942538,
    "question_id_str": "qp_1517",
    "question_description": "你倾向拥有",
    "options": [
      {"option": "很多认识的人和很亲密的朋友"},
      {"option": "一些很亲密的朋友和一些认识的人"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/51.mp3"
  },
  {
    "assessment_question_id": 3879942540,
    "question_id_str": "qp_1518",
    "question_description": "你能够很好的控制自己的欲望和外部诱惑",
    "options": [
      {"option": "是的"},
      {"option": "更多时候我会不由自主"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/52.mp3"
  },
  {
    "assessment_question_id": 3879942542,
    "question_id_str": "qp_1519",
    "question_description": "哪种更符合你",
    "options": [
      {"option": "喜欢发掘新点子"},
      {"option": "不善于此"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/53.mp3"
  },
  {
    "assessment_question_id": 3879942544,
    "question_id_str": "qp_1520",
    "question_description": "相对解决问题的结果，你更注重解决的过程和方法",
    "options": [
      {"option": "是的"},
      {"option": "不是"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/54.mp3"
  },
  {
    "assessment_question_id": 3879942546,
    "question_id_str": "qp_1521",
    "question_description": "碰到熟人，下意识躲避",
    "options": [
      {"option": "常常这样"},
      {"option": "不常这样"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/55.mp3"
  },
  {
    "assessment_question_id": 3879942548,
    "question_id_str": "qp_1522",
    "question_description": "哪种更符合你",
    "options": [
      {"option": "我很少质询和指责他人，更愿意做一个随和包容的人"},
      {"option": "我不想充当老好人，发现他人犯错就会直言不讳指出来"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/56.mp3"
  },
  {
    "assessment_question_id": 3879942550,
    "question_id_str": "qp_1523",
    "question_description": "解决问题的时候，比起用新的方法，你更愿意用熟悉的方法解决",
    "options": [
      {"option": "是的"},
      {"option": "不是"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/57.mp3"
  },
  {
    "assessment_question_id": 3879942552,
    "question_id_str": "qp_1524",
    "question_description": "你的内心渴望一次冒险",
    "options": [
      {"option": "是的"},
      {"option": "不是"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/58.mp3"
  },
  {
    "assessment_question_id": 3879942554,
    "question_id_str": "qp_1525",
    "question_description": "你发现很难谈论自己的情感",
    "options": [
      {"option": "是这样"},
      {"option": "不是"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/59.mp3"
  },
  {
    "assessment_question_id": 3879942556,
    "question_id_str": "qp_1526",
    "question_description": "对你更具吸引力的人是",
    "options": [
      {"option": "聪明而反应敏捷的人"},
      {"option": "稳重而脚踏实地的人"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/60.mp3"
  },
  {
    "assessment_question_id": 3879942558,
    "question_id_str": "qp_1527",
    "question_description": "你倾向于凡事都持科学的态度",
    "options": [
      {"option": "是的"},
      {"option": "不是，我比较感性"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/61.mp3"
  },
  {
    "assessment_question_id": 3879942561,
    "question_id_str": "qp_1528",
    "question_description": "当你考虑一件事情时，你很少想到事情可能的发展，而是更多关注目的",
    "options": [
      {"option": "是这样"},
      {"option": "不是，我更容易陷入对未来可能性的推测中"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/62.mp3"
  },
  {
    "assessment_question_id": 3879942563,
    "question_id_str": "qp_1529",
    "question_description": "你容易被强烈的情感影响",
    "options": [
      {"option": "是这样"},
      {"option": "不会"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/63.mp3"
  },
  {
    "assessment_question_id": 3879942565,
    "question_id_str": "qp_1530",
    "question_description": "比起事前的计划，你的决定大多基于瞬间的感觉",
    "options": [
      {"option": "是的"},
      {"option": "不是"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/64.mp3"
  },
  {
    "assessment_question_id": 3879942567,
    "question_id_str": "qp_1531",
    "question_description": "你喜欢一个人独自享受空闲时光，或者在安静的家庭环境中放松",
    "options": [
      {"option": "是这样"},
      {"option": "不是"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/65.mp3"
  },
  {
    "assessment_question_id": 3879942569,
    "question_id_str": "qp_1532",
    "question_description": "传统的生活方式让你觉得更自在",
    "options": [
      {"option": "是的"},
      {"option": "不是，我渴望新的体验"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/66.mp3"
  },
  {
    "assessment_question_id": 3879942571,
    "question_id_str": "qp_1533",
    "question_description": "大多数时候，你是这种人",
    "options": [
      {"option": "喜欢先纵观全局"},
      {"option": "喜欢先掌握细节"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/67.mp3"
  },
  {
    "assessment_question_id": 3879942572,
    "question_id_str": "qp_1534",
    "question_description": "你的桌子、办公台总是整洁而规律",
    "options": [
      {"option": "是的"},
      {"option": "不是"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/68.mp3"
  },
  {
    "assessment_question_id": 3879942573,
    "question_id_str": "qp_1535",
    "question_description": "独自行走你会觉得舒服",
    "options": [
      {"option": "是的"},
      {"option": "我更愿意找人聊天、参加娱乐活动"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/69.mp3"
  },
  {
    "assessment_question_id": 3879942574,
    "question_id_str": "qp_1536",
    "question_description": "你倾向相信",
    "options": [
      {"option": "我直接的观察和现成的经验"},
      {"option": "我的直觉"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/70.mp3"
  },
  {
    "assessment_question_id": 3879942575,
    "question_id_str": "qp_1537",
    "question_description": "好朋友倾向对你说",
    "options": [
      {"option": "你难道不可以安静一会儿吗？"},
      {"option": "可以请你从你的世界中出来一下吗？"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/71.mp3"
  },
  {
    "assessment_question_id": 3879942576,
    "question_id_str": "qp_1538",
    "question_description": "喜欢受到他人的关心",
    "options": [
      {"option": "是的，感觉旁边人在关心我的话，心情当然好咯"},
      {"option": "不，不要过分在意我"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/72.mp3"
  },
  {
    "assessment_question_id": 3879942577,
    "question_id_str": "qp_1539",
    "question_description": "小时候，你更热衷于",
    "options": [
      {"option": "问为什么，或自己动手拆玩具找到其原理"},
      {"option": "绘画和表述，描述和描绘所看到的"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/73.mp3"
  },
  {
    "assessment_question_id": 3879942578,
    "question_id_str": "qp_1540",
    "question_description": "你常常会急急忙忙赶一件事",
    "options": [
      {"option": "是的"},
      {"option": "不是"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/74.mp3"
  },
  {
    "assessment_question_id": 3879942579,
    "question_id_str": "qp_1541",
    "question_description": "相比之下，你更欣赏",
    "options": [
      {"option": "实业家洛克菲勒"},
      {"option": "爱因斯坦"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/75.mp3"
  },
  {
    "assessment_question_id": 3879942580,
    "question_id_str": "qp_1542",
    "question_description": "你偏好",
    "options": [
      {"option": "事先知道约会的行程：要去哪里、有谁参加、我会在那里多久"},
      {"option": "让约会自然地发生，不做太多事先的计划"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/76.mp3"
  },
  {
    "assessment_question_id": 3879942581,
    "question_id_str": "qp_1543",
    "question_description": "哪一项较常见",
    "options": [
      {"option": "我准时出席而其他人都迟到"},
      {"option": "其他人都准时出席而我迟到"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/77.mp3"
  },
  {
    "assessment_question_id": 3879942582,
    "question_id_str": "qp_1544",
    "question_description": "你更愿意被看作是",
    "options": [
      {"option": "务实的人"},
      {"option": "有想象力、有灵感的人"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/78.mp3"
  },
  {
    "assessment_question_id": 3879942583,
    "question_id_str": "qp_1545",
    "question_description": "闲暇时候，你更愿意看哪方面的书",
    "options": [
      {"option": "摄影"},
      {"option": "哲学思辨"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/79.mp3"
  },
  {
    "assessment_question_id": 3879942584,
    "question_id_str": "qp_1546",
    "question_description": "选择较符合你的词",
    "options": [
      {"option": "准时"},
      {"option": "闲适、悠然"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/80.mp3"
  },
  {
    "assessment_question_id": 3879942585,
    "question_id_str": "qp_1547",
    "question_description": "哪种描述较符合你",
    "options": [
      {"option": "如果对于一个东西没有一套合理的系统的解释，我就感觉搞不清楚，一片模糊"},
      {"option": "享受生活和现实，给什么东西都进行抽象和解释，太累了"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/81.mp3"
  },
  {
    "assessment_question_id": 3879942586,
    "question_id_str": "qp_1548",
    "question_description": "疲惫地下班回家时，哪种描述更像你",
    "options": [
      {"option": "再累也收拾整齐了再休息"},
      {"option": "扔下包倒床就睡"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/82.mp3"
  },
  {
    "assessment_question_id": 3879942587,
    "question_id_str": "qp_1549",
    "question_description": "喜欢清闲的生活",
    "options": [
      {"option": "是的，我喜欢轻轻松松的人生"},
      {"option": "我喜欢有挑战的人生，不断给自己更高的目标"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/83.mp3"
  },
  {
    "assessment_question_id": 3879942588,
    "question_id_str": "qp_1550",
    "question_description": "你喜欢一个人行动",
    "options": [
      {"option": "是的"},
      {"option": "不是，我不喜欢孤苦伶仃"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/84.mp3"
  },
  {
    "assessment_question_id": 3879942589,
    "question_id_str": "qp_1551",
    "question_description": "哪种意境更符合你",
    "options": [
      {"option": "人生就像一场旅行，不必在意目的地，在乎的是沿途的风景和看风景的心情"},
      {"option": "人生需要树立目标并为之奋斗，专注人生目标的人往往无暇欣赏路途美景"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/85.mp3"
  },
  {
    "assessment_question_id": 3879942590,
    "question_id_str": "qp_1552",
    "question_description": "对你而言，解决问题本身就是一种乐趣",
    "options": [
      {"option": "是的"},
      {"option": "我更喜欢发现和体验美好的东西"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/86.mp3"
  },
  {
    "assessment_question_id": 3879942591,
    "question_id_str": "qp_1553",
    "question_description": "喜欢推测、预测和分析",
    "options": [
      {"option": "是的"},
      {"option": "这些对我而言都是浮云，我只想把握好当下"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/87.mp3"
  },
  {
    "assessment_question_id": 3879942592,
    "question_id_str": "qp_1554",
    "question_description": "周围的朋友们都说你是很冷静的人",
    "options": [
      {"option": "我说话做事比较冲动，并非一个冷静的人"},
      {"option": "是，我的性格是孤言寡语，非常冷静"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/88.mp3"
  },
  {
    "assessment_question_id": 3879942593,
    "question_id_str": "qp_1555",
    "question_description": "你更擅长",
    "options": [
      {"option": "运用逻辑、分析及批判思维来解决问题"},
      {"option": "运用人际关系解决问题"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/89.mp3"
  },
  {
    "assessment_question_id": 3879942594,
    "question_id_str": "qp_1556",
    "question_description": "对周围人的变化很敏感",
    "options": [
      {"option": "是的，连朋友换个戒指也瞒不了我"},
      {"option": "不，有时候对方换什么衣服否察觉不到"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/90.mp3"
  },
  {
    "assessment_question_id": 3879942595,
    "question_id_str": "qp_1557",
    "question_description": "哪种更符合你",
    "options": [
      {"option": "凡事追求个性，完全凭自己的兴趣，没有固定的程式"},
      {"option": "传统而保守，不喜欢标新立异"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/91.mp3"
  },
  {
    "assessment_question_id": 3879942596,
    "question_id_str": "qp_1558",
    "question_description": "哪种更符合你",
    "options": [
      {"option": "过于抽象，因此在必要的后续行动上不切实际"},
      {"option": "沉浸于细节，导致视野不开阔"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/92.mp3"
  },
  {
    "assessment_question_id": 3879942597,
    "question_id_str": "qp_1559",
    "question_description": "哪种更符合你",
    "options": [
      {"option": "喜欢惊喜并适应最后的变化"},
      {"option": "不喜欢变化，追求稳定与踏实"}
    ],
    "user_option_choice": 0,
    "user_response_time": 0,
    "sound_url": "https://static.jzmbti.com/sound/sound_93/93.mp3"
  }
];
