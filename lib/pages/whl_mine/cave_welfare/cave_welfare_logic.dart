import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/pages/whl_login/complete_user_info/selected_sex/selected_sex_logic.dart';
import 'package:cave_flutter/utils/task_utils.dart';
import 'package:get/get.dart';

class Cave_welfareLogic extends GetxController {
  List<SexModel> sexList = [];
  int selectedSex = 0;

  @override
  void onInit() {
    super.onInit();
    TaskUtils.complateTask(13, type: 2);
    // getSexData();
  }

  getSexData() {
    WhlApi.getGenderList.get({"type": 1}, withLoading: true).then((value) {
      List data = value.data;
      sexList = data.map((e) {
        SexModel model = SexModel(
            defaultIcon: e["imageUrl"],
            selectedIcon: e["imageUrl"],
            name: e["lable"],
            value: e["id"],
            describe: e["note"]);
        return model;
      }).toList();
      update();
    });
  }
}
