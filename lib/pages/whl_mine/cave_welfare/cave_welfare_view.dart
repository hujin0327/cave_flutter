import 'package:cave_flutter/model/whl_user_info_model.dart';
import 'package:cave_flutter/utils/whl_user_utils.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'cave_welfare_logic.dart';

class Cave_welfarePage extends StatelessWidget {
  Cave_welfarePage({Key? key}) : super(key: key);

  final logic = Get.put(Cave_welfareLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<Cave_welfareLogic>(
        builder: (logic) {
          String assetImage = "";
          WhlUserInfoModel userInfoModel = WhlUserUtils.getUserInfo();
          switch (userInfoModel.gender) {
            case "0":
              assetImage = "mine_cave_w_man.png";
              break;
            case "1":
              assetImage = "mine_cave_w_wuman.png";
              break;
            default:
              assetImage = "mine_cave_w_more.png";
          }
          return ListView(
            children: [
              UIImage(
                width: ScreenUtil().screenWidth,
                fit: BoxFit.fitWidth,
                assetImage: assetImage,
              )
            ],
          );
        },
      ),
    );
  }
}
