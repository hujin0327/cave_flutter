import 'package:cave_flutter/common/common_widget.dart';
import 'package:cave_flutter/common/list_view_group.dart';
import 'package:cave_flutter/config/whl_global_config.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/extension/string_extension.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../common/upload_photo_widget.dart';
import '../../../model/base_source_model.dart';
import 'edit_userinfo_logic.dart';
import 'my_progress_widget.dart';

class Edit_userinfoPage extends StatelessWidget {
  final logic = Get.put(Edit_userinfoLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppWhiteColor,
      appBar: WhlAppBar(
        backgroundColor: kAppWhiteColor,
        centerTitle: "个人资料",
      ),
      body: GetBuilder<Edit_userinfoLogic>(builder: (logic) {
        ListViewGroupHandler handler = getGroupHandler();
        return UIColumn(
          children: [
            buildProgressWidget(),
            Expanded(
              child: ListView.builder(
                itemBuilder: (context, index) {
                  return handler.cellAtIndex(index);
                },
                itemCount: handler.allItemCount,
              ),
            )
          ],
        );
      }),
    );
  }

  Widget buildProgressWidget() {
    return UIRow(
      margin: EdgeInsets.only(left: 29.w, right: 19.w, top: 30.h, bottom: 15.h),
      children: [
        UIText(
          margin: EdgeInsets.only(right: 7.w),
          text: "完成度",
          fontSize: 12.sp,
          textColor: kAppSubTextColor,
        ),
        MyProgressWidget(
          progress: logic.progress,
        )
      ],
    );
  }

  ListViewGroupHandler getGroupHandler() {
    return ListViewGroupHandler(
      numberOfSections: logic.listData.length,
      numberOfRowsInSection: (section) {
        BaseItemSectionModel sectionModel = logic.listData[section];
        return sectionModel.itemDatas?.length ?? 0;
      },
      headerForSection: (section) {
        BaseItemSectionModel sectionModel = logic.listData[section];

        return buildDefHeaderView(sectionModel);
      },
      cellForRowAtIndexPath: (indexPath) {
        BaseItemSectionModel sectionModel = logic.listData[indexPath.section];
        BaseItemModel model = sectionModel.itemDatas?[indexPath.row];

        return UIContainer(
          margin: EdgeInsets.only(
              bottom: model == sectionModel.itemDatas?.last ? 15.h : 0),
          child: createListItem(model, sectionModel),
        );
      },
    );
  }

  Widget buildDefHeaderView(BaseItemSectionModel sectionModel) {
    return UIRow(
      margin: EdgeInsets.symmetric(horizontal: 28.w, vertical: 14.w),
      children: [
        Expanded(
            child: UIContainer(
          child: Text.rich(TextSpan(
            text: sectionModel.title,
            style: TextStyle(
                color: kAppBlackColor,
                fontSize: 16.sp,
                fontWeight: FontWeight.bold),
            children: [
              TextSpan(
                text: "  (完成度+${(sectionModel.otherDatas * 100).toInt()}%）",
                style: TextStyle(
                  color: kAppColor("#B0B0B0"),
                  fontSize: 12.sp,
                ),
              )
            ],
          )),
        )),
        if (sectionModel.modelType == BaseModelTypeENUM.MODELTYPE_Avatar)
          UIImage(
            margin: EdgeInsets.only(right: 13.w),
            width: 28.w,
            height: 28.w,
            radius: 14.w,
            fit: BoxFit.cover,
            httpImage: sectionModel.value?.toImageUrl(),
            assetPlaceHolder: WHLGlobalConfig.userPlaceholderAvatar,
            clipBehavior: Clip.hardEdge,
          ),
        if (sectionModel.modelType != BaseModelTypeENUM.MODELTYPE_Avatar)
          UIText(
            text: sectionModel.otherValue,
            fontSize: 14.sp,
            textColor: kAppSub2TextColor,
            onTap: () {
              logic.toSectionEditPage(sectionModel);
            },
          ),
        UIImage(
          assetImage: "home_right",
          width: 15.w,
          height: 15.w,
          imageColor: kAppColor("#BEBEBE"),
        )
      ],
      onTap: () {
        if (sectionModel.modelType == BaseModelTypeENUM.MODELTYPE_Avatar) {
          logic.selectPhoto(sectionModel);
        }
      },
    );
  }

  Widget createListItem(
      BaseItemModel model, BaseItemSectionModel sectionModel) {
    if (model.itemType == BaseItemTypeENUM.ENUM_AddCell) {
      return createUploadPhotoView(model);
    } else if (model.itemType == BaseItemTypeENUM.ENUM_SelectCell) {
      return createSelectItemView(model, sectionModel);
    } else if (model.itemType == BaseItemTypeENUM.ENUM_MoreInputCell) {
      return UIContainer(
        margin: EdgeInsets.symmetric(horizontal: 18.w),
        padding: EdgeInsets.symmetric(horizontal: 10.w),
        height: 110.h,
        color: kAppColor("#F9F9F9"),
        radius: 15.w,
        child: Center(
          child: UIText(
            text:
                (model.value ?? "").isNotEmpty ? model.value : model.hitString,
            textColor: (model.value ?? "").isNotEmpty
                ? kAppSubTextColor
                : kAppSub3TextColor,
            fontSize: 14.sp,
            maxLines: 20,
            shrinkWrap: false,
          ),
        ),
      );
    } else if (model.itemType == BaseItemTypeENUM.ENUM_SelectTypeCell) {
      return UIContainer(
        margin: EdgeInsets.symmetric(horizontal: 18.w),
        // height: 110.h,
        radius: 15.w,
        color: kAppColor("#F9F9F9"),
        child: UIWrap(
          runAlignment: WrapAlignment.start,
          wrapAlignment: WrapAlignment.start,
          crossAxisAlignment: WrapCrossAlignment.start,
          margin: EdgeInsets.symmetric(horizontal: 14.w, vertical: 18.w),
          runSpacing: 7.w,
          spacing: 8.w,
          children: (model.datas ?? []).map((e) {
            return UIText(
              padding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 7.w),
              text: e.label,
              textColor: Colors.white,
              fontSize: 12.sp,
              color: kAppColor("#808080"),
              radius: 30.w,
            );
          }).toList(),
        ),
      );
    }
    return Container();
  }

  Widget createSelectItemView(
      BaseItemModel model, BaseItemSectionModel sectionModel) {
    return UIRow(
      margin: EdgeInsets.symmetric(horizontal: 18.w),
      padding: EdgeInsets.only(
          left: 14.w,
          right: 7.w,
          top: 18.h,
          bottom: model == sectionModel.itemDatas?.last ? 18.h : 5.h),
      topLeftRadius: model == sectionModel.itemDatas?.first ? 15.w : 0,
      topRightRadius: model == sectionModel.itemDatas?.first ? 15.w : 0,
      bottomLeftRadius: model == sectionModel.itemDatas?.last ? 15.w : 0,
      bottomRightRadius: model == sectionModel.itemDatas?.last ? 15.w : 0,
      color: kAppColor("#F9F9F9"),
      children: [
        Expanded(
          child: UIText(
            text: model.title,
            fontSize: 13.sp,
            textColor: kAppColor("#212121"),
          ),
        ),
        UIText(
          margin: EdgeInsets.only(right: 12.w),
          text: model.value,
          fontSize: 12.sp,
          textColor: kAppSub3TextColor,
        ),
        UIImage(
          assetImage: "home_right",
          width: 15.w,
          height: 15.w,
          imageColor: kAppColor("#BEBEBE"),
        )
      ],
      onTap: () {
        logic.selectViewAction(model);
        // logic.toEditItemPage(model);
      },
    );
  }

  Widget createUploadPhotoView(BaseItemModel model) {
    return UIContainer(
      key: Key("uploadPhoto_${model.datas?.length}"),
      padding: EdgeInsets.symmetric(horizontal: 29.w),
      margin: EdgeInsets.only(top: 11.h),
      child: UploadPhotoWidget(
        photoData: model.datas,
        photoChangeCallBack: (value) {
          logic.bindPhoto(value);
        },
        photoDeleteCallBack: (deleteModel) {
          if (model.datas != null) {
            logic.bindPhoto(model.datas!);
          }
        },
      ),
    );
  }
}
