import 'package:cave_flutter/model/base_source_model.dart';
import 'package:cave_flutter/model/whl_user_info_model.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:cave_flutter/utils/whl_user_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../../../../network/whl_api.dart';
import '../../whl_mine_logic.dart';

class Edit_userinfo_itemLogic extends GetxController {
  TextEditingController textEditingController = TextEditingController();
  late BaseItemModel editModel;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    var a = Get.arguments;
    if (a != null) {
      editModel = a["model"];
      textEditingController.text = editModel.value??"";
      update();
    }
  }

  saveUserInfo(BaseItemModel model){
    Map<String,dynamic> param = {model.requestKey??"":model.requestValue ??model.value};
    WhlApi.saveUserBaseInfo.post(param,withLoading: true).then((value) {
      if (value.isSuccess()) {
        MyToast.show("保存成功");
        WhlUserInfoModel infoModel = WhlUserUtils.getUserInfo();
        infoModel.nickname = textEditingController.text;
        WhlUserUtils.saveUserInfo(infoModel);
        Get.back(result: true);
        final mineLogic = Get.find<WhlMineLogic>();
        mineLogic.update();
      }
    });
  }

  saveUserConfig(BaseItemModel model) {
    Map<String,dynamic> param = {model.requestKey??"":model.requestValue ??model.value};
    WhlApi.saveUserInformation.post(param, withLoading: true).then((value) {
      if (value.isSuccess()) {
        MyToast.show("保存成功");
        Get.back(result: true);
      }
    });
  }

  void onConfirm() {
    if (textEditingController.text.isNotEmpty) {
      // editModel.requestValue = textEditingController.text;
      editModel.value = textEditingController.text;
      if (editModel.modelType == BaseModelTypeENUM.MODELTYPE_NameModel){
        saveUserInfo(editModel);
      }else {
        saveUserConfig(editModel);
      }

    }else {
      MyToast.show("请输入");
    }
  }
  
}
