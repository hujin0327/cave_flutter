import 'package:cave_flutter/common/common_widget.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../style/whl_style.dart';
import '../../../../widgets/brick/widget/basic_widget.dart';
import '../../../../widgets/brick/widget/text_widget.dart';
import 'edit_userinfo_item_logic.dart';

class Edit_userinfo_itemPage extends StatelessWidget {
  Edit_userinfo_itemPage({Key? key}) : super(key: key);

  final logic = Get.put(Edit_userinfo_itemLogic());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppWhiteColor,
      appBar: WhlAppBar(
        backgroundColor: kAppWhiteColor,
        centerTitle: '编辑${logic.editModel.title ?? ""}',
        isShowBottomLine: true,
      ),
      body: UIColumn(
          crossAxisAlignment: CrossAxisAlignment.start,
          color: kAppWhiteColor,
          padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 25.w),
          children: [
            noBorderCTextField(
              controller: logic.textEditingController,
              hint: "请输入",
              fontSize: 16.sp,
            ),
            Expanded(child: UIContainer()),
            UIText(
              margin: EdgeInsets.symmetric(horizontal: 20.w, vertical: 60.h),
              text: "确定",
              textColor: kAppWhiteColor,
              fontSize: 16.sp,
              fontWeight: FontWeight.bold,
              color: kAppBlackColor,
              height: 52.h,
              radius: 26.h,
              alignment: Alignment.center,
              onTap: () {
                logic.onConfirm();
              },
            )
          ]),
    );
  }
}
