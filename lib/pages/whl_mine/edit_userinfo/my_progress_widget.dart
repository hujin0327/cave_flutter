
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../style/whl_style.dart';
import '../../../widgets/brick/widget/basic_widget.dart';

class MyProgressWidget extends StatefulWidget {

  double? progress = 0;
  MyProgressWidget({super.key,this.progress});

  @override
  State<MyProgressWidget> createState() => _MyProgressWidgetState();
}

class _MyProgressWidgetState extends State<MyProgressWidget> {
  @override
  Widget build(BuildContext context) {
    double width = ScreenUtil().screenWidth-110.w;
    double progressWidth = width*(widget.progress??0);
    print('width=== $width ---- $progressWidth');
    return UIStack(
      alignment: Alignment.centerLeft,
      children: [
        UIContainer(
          color: kAppColor("#F9F9F9"),
          width: width,
          height: 6.h,
          radius: 3.h,
        ),
        UIContainer(
          color: kAppBlackColor,
          width: progressWidth,
          height: 6.h,
          radius: 3.h,
        ),
        UIText(
          margin: EdgeInsets.only(left: progressWidth-35.w > 0?progressWidth-35.w :0),
          width: 35.w,
          height: 18.w,
          radius: 9.w,
          text: "${((widget.progress??0)*100).ceil()}%",
          textColor: kAppBlackColor,
          fontSize: 11.sp,
          color: kAppColor("#FFE800"),
          alignment: Alignment.center,
        )
      ],
    );
  }
}