import 'package:cave_flutter/model/image_model.dart';
import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FeedbackLogic extends GetxController {
  List<dynamic> materials = List<dynamic>.from([]);
  TextEditingController textEditingController = TextEditingController();
  @override
  void onInit() {
    super.onInit();
    textEditingController.clear();
    materials.clear();
  }

  onFeedBack() {
    WhlApi.inCaveFeedback.post({
      'content': textEditingController.text,
      'matrials': materials.map((e) => e.imagePath).join(',')
    }).then((value) {
      MyToast.show(value.msg ?? '感谢您的建议');
      Get.back();
    });
  }
}
