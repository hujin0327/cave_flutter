import 'package:cave_flutter/common/common_widget.dart';
import 'package:cave_flutter/common/upload_photo_widget.dart';
import 'package:cave_flutter/model/base_source_model.dart';
import 'package:cave_flutter/pages/whl_mine/feedback/feedback_logic.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:cave_flutter/widgets/whl_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class FeedbackPage extends StatefulWidget {
  const FeedbackPage({super.key});

  @override
  State<FeedbackPage> createState() => _FeedbackPageState();
}

class _FeedbackPageState extends State<FeedbackPage> {
  final logic = Get.put(FeedbackLogic());

  @override
  void initState() {
    super.initState();
    logic.onInit();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppWhiteColor,
      appBar: WhlAppBar(
        centerTitle: "意见反馈",
        backgroundColor: kAppWhiteColor,
      ),
      body: GetBuilder<FeedbackLogic>(
        builder: (logic) {
          return UIColumn(
            padding: EdgeInsets.symmetric(horizontal: 20.w).copyWith(top: 34.h),
            children: [
              UIRow(
                margin: EdgeInsets.only(bottom: 20.h),
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  UIText(
                    margin: EdgeInsets.only(right: 20.w),
                    text: '申请内容',
                    fontSize: 13.sp,
                    fontWeight: FontWeight.bold,
                  ),
                  Expanded(
                    child: UIContainer(
                      decoration: BoxDecoration(color: kAppColor('#FAFAFA')),
                      child: noBorderCTextField(
                        hint: "你的反馈理由",
                        hintTextColor: kAppSub3TextColor,
                        maxLine: 5,
                        controller: logic.textEditingController,
                        maxLength: 512,
                        contentPadding: EdgeInsets.symmetric(
                          vertical: 10.h,
                          horizontal: 15.w,
                        ),
                        onChanged: (value) {
                          if (value.length > 512) {
                            MyToast.show('最多512个字符');
                          }
                        },
                      ),
                    ),
                  ),
                ],
              ),
              UIRow(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  UIText(
                    margin: EdgeInsets.only(right: 20.w),
                    text: '证明材料',
                    fontSize: 13.sp,
                    fontWeight: FontWeight.bold,
                  ),
                  Expanded(
                    child: createUploadPhotoView(),
                  ),
                ],
              ),
            ],
          );
        },
      ),
      bottomNavigationBar: UISolidButton(
        onTap: () {
          logic.onFeedBack();
        },
        text: '提交',
        margin: EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.h),
      ),
    );
  }

  Widget createUploadPhotoView() {
    return UploadPhotoWidget(
      maxPhotoCount: 6,
      photoData: logic.materials,
      photoChangeCallBack: (value) {
        // logic.bindPhoto(value);
        print(logic.materials);
      },
      photoDeleteCallBack: (deleteModel) {
        // logic.removePhoto(deleteModel);
        print(logic.materials);
      },
    );
  }
}
