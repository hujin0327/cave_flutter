import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../common/common_widget.dart';
import '../../../../style/whl_style.dart';
import '../../../../widgets/brick/widget/basic_widget.dart';
import '../../../../widgets/brick/widget/text_widget.dart';
import '../../../../widgets/whl_app_bar.dart';
import 'with_draw_bind_logic.dart';

class WithDrawBindPage extends StatelessWidget {
  WithDrawBindPage({Key? key}) : super(key: key);

  final logic = Get.find<WithDrawBindLogic>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: WhlAppBar(
          backgroundColor: Colors.white,
          centerTitle: '绑定支付宝',
          isShowBottomLine: false,
        ),
        body: UIColumn(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          padding: EdgeInsets.only(left: 17.w, right: 17.w),
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _topTipWidget(),
                _inputItem(
                    title: "支付宝账户",
                    hint: '点击输入',
                    controller: logic.accountController),
                SizedBox(
                  height: 12.h,
                ),
                _inputItem(
                    title: "真实姓名",
                    isTransMoney: true,
                    hint: "点击输入",
                    controller: logic.nameController)
              ],
            ),
            UIText(
              onTap: () {
                logic.onAccountBind(context);
              },
              width: double.infinity,
              alignment: Alignment.center,
              margin: EdgeInsets.only(top: 12.h, bottom: 12.h),
              height: 53.h,
              text: '立即绑定',
              fontWeight: FontWeight.bold,
              fontSize: 16.sp,
              color: Colors.black,
              textColor: Colors.white,
              radius: 99.h,
            )
          ],
        ));
  }

  ///顶部点击绑定
  _topTipWidget() {
    return Container(
      alignment: Alignment.centerLeft,
      margin: EdgeInsets.symmetric(vertical: 5.h),
      height: 40.h,
      child: Text(
        '支付宝账户需与实名认证账户一致',
        style: TextStyle(fontSize: 12.sp, color: kAppColor('#9E9E9E')),
      ),
    );
  }

  ///输入item
  _inputItem(
      {bool isTransMoney = false,
      required String title,
      required String hint,
      required TextEditingController controller}) {
    return UIContainer(
      height: 61.h,
      radius: 15.w,
      color: kAppColor('#F5F7F9'),
      padding: EdgeInsets.only(left: 18.w, right: 18.w),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          UIText(
            text: title,
            fontWeight: FontWeight.bold,
            textColor: Colors.black,
            color: Colors.transparent,
            fontSize: 14.sp,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              ConstrainedBox(
                  constraints: BoxConstraints(minWidth: 170.w, maxWidth: 170.w),
                  child: IntrinsicWidth(
                    child: noBorderCTextField(
                      controller: controller,
                      textAlign: TextAlign.right,
                      hint: hint,
                      textColor: kAppSubTextColor,
                      hintTextColor: kAppSub3TextColor,
                      fontSize: 14.sp,
                      fontWeight: FontWeight.bold,
                      hintFontWeight: FontWeight.normal,
                      contentPadding: EdgeInsets.only(top: 4.h),
                    ),
                  ))
            ],
          )
        ],
      ),
    );
  }
}
