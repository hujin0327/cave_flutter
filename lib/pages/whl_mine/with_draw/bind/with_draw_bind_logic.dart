import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:cave_flutter/utils/whl_toast_utils.dart';

import 'package:cave_flutter/network/whl_api.dart';

class WithDrawBindLogic extends GetxController {

  //账号
  TextEditingController accountController = TextEditingController();
  //姓名
  TextEditingController nameController = TextEditingController();



  ///立即绑定
  onAccountBind(BuildContext context) async {
    if (accountController.text.isEmpty) {
      BotToast.showText(text: "请输入支付宝账号");
      return;
    }
    if (nameController.text.isEmpty) {
      BotToast.showText(text: "请输入真实姓名");
      return;
    }

    ResponseData responseData = await WhlApi.bindAlipay.post({
      'payAccount':accountController.text,
      'realName': nameController.text
    });

    if (responseData.isSuccess()) {
        MyToast.show('绑定成功');
        Navigator.pop(context);  
    }
  }

}
