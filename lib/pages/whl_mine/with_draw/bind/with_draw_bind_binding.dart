import 'package:get/get.dart';

import 'with_draw_bind_logic.dart';

class WithDrawBindBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => WithDrawBindLogic());
  }
}
