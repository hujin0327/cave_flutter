import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../common/common_widget.dart';
import '../../../../widgets/whl_app_bar.dart';
import 'with_draw_index_logic.dart';

class WithDrawIndexPage extends StatelessWidget {
  WithDrawIndexPage({Key? key}) : super(key: key);

  final logic = Get.find<WithDrawIndexLogic>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: WhlAppBar(
          backgroundColor: Colors.white,
          centerTitle: '提现',
          isShowBottomLine: false,
        ),
        body: GetBuilder<WithDrawIndexLogic>(builder: (s) {
          return UIColumn(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            padding: EdgeInsets.only(left: 17.w, right: 17.w),
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _topTipWidget(),
                  _inputItem(
                      title: "提现到",
                      hint: '请输入账号',
                      controller: logic.accountController),
                  SizedBox(
                    height: 12.h,
                  ),
                  _inputItem(
                      title: "转出金额",
                      isTransMoney: true,
                      hint: logic.moneyHint,
                      controller: logic.moneyController)
                ],
              ),
              UIText(
                onTap: () {
                  logic.onWithDraw();
                },
                width: double.infinity,
                alignment: Alignment.center,
                margin: EdgeInsets.only(top: 12.h, bottom: 12.h),
                height: 53.h,
                text: '提现',
                fontWeight: FontWeight.bold,
                fontSize: 16.sp,
                color: Colors.black,
                textColor: Colors.white,
                radius: 99.h,
              )
            ],
          );
        }));
  }

  ///顶部点击绑定
  _topTipWidget() {
    return InkWell(
        onTap: logic.onPushToBind,
        child: Container(
          alignment: Alignment.centerLeft,
          margin: EdgeInsets.symmetric(vertical: 5.h),
          height: 40.h,
          child: RichText(
              text: TextSpan(
            text: "如需修改提现支付宝账号",
            style: TextStyle(fontSize: 12.sp, color: kAppColor('#9E9E9E')),
            children: [
              WidgetSpan(
                  child: SizedBox(
                width: 7.w,
              )),
              TextSpan(
                style: TextStyle(
                    fontSize: 12.sp,
                    color: kAppColor('#1D53BD'),
                    decoration: TextDecoration.underline),
                text: "请点击此处",
              ),
            ],
          )),
        ));
  }

  ///输入item
  _inputItem(
      {bool isTransMoney = false,
      required String title,
      required String hint,
      required TextEditingController controller}) {
    return UIContainer(
      height: 61.h,
      radius: 15.w,
      color: kAppColor('#F5F7F9'),
      padding: EdgeInsets.only(left: 18.w, right: 18.w),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          UIText(
            text: title,
            fontWeight: FontWeight.bold,
            textColor: Colors.black,
            color: Colors.transparent,
            fontSize: 14.sp,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              if (isTransMoney)
                UIText(
                  padding: EdgeInsets.only(top: 2.h),
                  text: "¥",
                  textColor: Colors.black,
                  fontSize: 14.sp,
                ),
              SizedBox(
                width: 5.w,
              ),
              ConstrainedBox(
                  constraints: BoxConstraints(minWidth: 18.sp, maxWidth: 170.w),
                  child: IntrinsicWidth(
                    child: noBorderCTextField(
                      controller: controller,
                      textAlign: TextAlign.right,
                      hint: hint,
                      textColor: kAppSubTextColor,
                      hintTextColor: kAppSub3TextColor,
                      fontSize: 14.sp,
                      fontWeight: FontWeight.bold,
                      hintFontWeight: FontWeight.normal,
                      contentPadding: EdgeInsets.only(top: 4.h),
                    ),
                  ))
            ],
          )
        ],
      ),
    );
  }
}
