import 'package:get/get.dart';

import 'with_draw_index_logic.dart';

class WithDrawIndexBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => WithDrawIndexLogic());
  }
}
