import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import 'package:cave_flutter/utils/whl_toast_utils.dart';

import 'package:cave_flutter/network/whl_api.dart';

import '../../../../routes/whl_app_pages.dart';

class WithDrawIndexLogic extends GetxController {

  //提现到
  TextEditingController accountController = TextEditingController();
  //转出金额
  TextEditingController moneyController = TextEditingController();
  String moneyHint = '可提现金额100元';

  //是否输入了
  bool isHasEdit = false;

  @override
  void onInit() {
    super.onInit();
    getBindAlipay();
    moneyController.addListener(() {
      bool isEdit = false;
      if (moneyController.text.isNotEmpty) {
        isEdit = true;
        moneyHint = '';
      } else {
        isEdit = false;
        moneyHint = '可提现金额100元';
      }
      if (isHasEdit != isEdit) {
        update();
      }
      isHasEdit = isEdit;
    });
  }
  
  ///提现
  onWithDraw() async {
    if (accountController.text.isEmpty) {
      BotToast.showText(text: "请输入提现账号");
      return;
    }
    if (moneyController.text.isEmpty) {
      BotToast.showText(text: "请输入转出金额");
      return;
    }

    ResponseData responseData = await WhlApi.withdraw.post({
      'withdrawalAmount': moneyController.text
    });

    if (responseData.isSuccess()) {
      MyToast.show('申请成功');
      Get.toNamed(Routes.withDrawSucPage);

    }
    //提现
  }

  ///进入绑定页面
  onPushToBind() {
    Get.toNamed(Routes.withDrawBindPage);
  }

  getBindAlipay() async {
    ResponseData responseData = await WhlApi.getBindAlipay.get({});

    if (responseData.isSuccess()) {
      accountController.text = responseData.data['payAccount'];
    }
  }
  
}
