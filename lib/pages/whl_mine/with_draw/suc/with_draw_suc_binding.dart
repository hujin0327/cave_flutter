import 'package:get/get.dart';

import 'with_draw_suc_logic.dart';

class WithDrawSucBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => WithDrawSucLogic());
  }
}
