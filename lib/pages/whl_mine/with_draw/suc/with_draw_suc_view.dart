import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../widgets/brick/widget/basic_widget.dart';
import '../../../../widgets/brick/widget/text_widget.dart';
import '../../../../widgets/whl_app_bar.dart';
import 'with_draw_suc_logic.dart';

class WithDrawSucPage extends StatelessWidget {
  WithDrawSucPage({Key? key}) : super(key: key);

  final logic = Get.find<WithDrawSucLogic>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: WhlAppBar(
          backgroundColor: Colors.white,
          centerTitle: '提现成功',
          isShowBottomLine: false,
        ),
        body: UIColumn(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          padding: EdgeInsets.only(left: 17.w, right: 17.w),
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 20.h,
                ),
                UIImage(
                  assetImage: 'ic_suc.png',
                  width: 56.w,
                  height: 56.w,
                ),
                SizedBox(
                  height: 20.h,
                ),
                UIText(
                  text: '提现成功',
                  color: Colors.transparent,
                  textColor: Colors.black,
                  fontSize: 16.sp,
                  fontWeight: FontWeight.bold,
                ),
                SizedBox(
                  height: 10.h,
                ),
                UIText(
                  text: '我们会在预计72小时内完成审核并到账',
                  color: Colors.transparent,
                  textColor: kAppColor('#ADADAD'),
                  fontSize: 13.sp,
                ),
              ],
            ),
            UIText(
              onTap: logic.onFinish,
              width: double.infinity,
              alignment: Alignment.center,
              margin: EdgeInsets.only(top: 12.h, bottom: 12.h),
              height: 53.h,
              text: '知道了',
              fontWeight: FontWeight.bold,
              fontSize: 16.sp,
              color: Colors.black,
              textColor: Colors.white,
              radius: 99.h,
            )
          ],
        ));
  }
}
