class WhlUserInfoModel {
  String? phone;
  String? accountId;
  int? age; // 年龄
  String? avatar;
  String? birthDate;
  int? charm;

  String? createTime;
  String? gender;
  String? id;

  int? level;
  String? levelImage;
  String? nickname; // 昵称

  String? mobile;
  String? updateTime;
  int? wealth;

  int? fansCount;
  int? followCount;
  bool? isSoul;
  bool? isTask;
  int? memberGrade;
  int? userId;
  int? visitCount;
  int? caveCount;
  String? funRole;
  int? schedule;

  WhlUserInfoModel({
    this.phone,
    this.accountId,
    this.age,
    this.avatar,
    this.birthDate,
    this.charm,
    this.createTime,
    this.gender,
    this.nickname,
    this.id,
    this.levelImage,
    this.level,
    this.mobile,
    this.updateTime,
    this.wealth,
    this.fansCount,
    this.followCount,
    this.isSoul,
    this.isTask,
    this.memberGrade,
    this.userId,
    this.visitCount,
    this.funRole,
    this.schedule,
    this.caveCount,
  });

  WhlUserInfoModel.fromJson(dynamic json) {
    accountId = json['accountId'];
    age = json['age'];
    avatar = json['avatar'];
    birthDate = json['birthDate'];
    charm = json['charm'];
    gender = json['gender'];
    nickname = json['nickname'] ?? json['nickName'];
    age = json['age'];
    id = json['id'].toString();
    level = json['level'];
    levelImage = json['levelImage'];
    mobile = json['mobile'];
    phone = json['phone'];
    updateTime = json['updateTime'];
    wealth = json['wealth'];

    fansCount = json['fansCount'];
    followCount = json['followCount'];
    isSoul = json['isSoul'];
    isTask = json['isTask'];
    memberGrade = json['memberGrade'];
    userId = json['userId'];
    visitCount = json['visitCount'];
    funRole = json['funRole'];
    schedule = json['schedule'];
    caveCount = json['caveCount'];
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['accountId'] = accountId;
    map['age'] = age;
    map['avatar'] = avatar;
    map['birthDate'] = birthDate;
    map['charm'] = charm;
    map['gender'] = gender;
    map['nickname'] = nickname;
    map['age'] = age;
    map['id'] = id;
    map['level'] = level;
    map['mobile'] = mobile;
    map['phone'] = phone;
    map['updateTime'] = updateTime;
    map['wealth'] = wealth;
    map['funRole'] = funRole;

    map['fansCount'] = fansCount;
    map['followCount'] = followCount;
    map['isSoul'] = isSoul;
    map['isTask'] = isTask;
    map['memberGrade'] = memberGrade;
    map['userId'] = userId;
    map['visitCount'] = visitCount;
    map['schedule'] = schedule;
    map['caveCount'] = caveCount;

    return map;
  }
}

/// 用户媒体信息模型
class MediaModel {
  String? mediaId; // 媒体资源ID
  String? mediaPath; // 媒体资源文件路径
  String? mediaType; // 媒体资源类型
  String? mediaUrl; // 媒体资源地址
  String? middleThumbUrl; // 中等尺寸媒体资源地址
  dynamic? sort; // 资源排序
  String? thumbUrl; // 媒体资源缩略图地址
  String? userId; // 归属的用户ID

  MediaModel({
    this.mediaId,
    this.mediaPath,
    this.mediaType,
    this.mediaUrl,
    this.middleThumbUrl,
    this.sort,
    this.thumbUrl,
    this.userId,
  });

  MediaModel.fromJson(dynamic json) {
    mediaId = json['mediaId'];
    mediaPath = json['mediaPath'];
    mediaType = json['mediaType'];
    mediaUrl = json['mediaUrl'];
    middleThumbUrl = json['middleThumbUrl'];
    sort = json['sort'];
    thumbUrl = json['thumbUrl'];
    userId = json['userId'];
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['mediaId'] = mediaId;
    map['mediaPath'] = mediaPath;
    map['mediaType'] = mediaType;
    map['mediaUrl'] = mediaUrl;
    map['middleThumbUrl'] = middleThumbUrl;
    map['sort'] = sort;
    map['thumbUrl'] = thumbUrl;
    map['userId'] = userId;
    return map;
  }
}
