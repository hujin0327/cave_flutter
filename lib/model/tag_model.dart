class TagModel {
  String? label;
  List<TagModel>? child;
  String? id;

  TagModel({
    this.id,
    this.label,
    this.child,
  });

  TagModel.fromJson(Map<String, dynamic> json) {
    id = json['id'] ?? "";
    label = json['label'] ?? json['lable'] ?? "";

    child = List<TagModel>.from(
        ((json['child'] ?? []).map((c) => TagModel.fromJson(c))).toList());
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['child'] = List<TagModel>.from(
      child != null ? child!.map((c) => c.toJson()).toList() : [],
    );
    data['id'] = id;
    data['label'] = label;
    return data;
  }
}
