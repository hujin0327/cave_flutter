import 'package:azlistview/azlistview.dart';

class FriendModel extends ISuspensionBean {
  String? uid;
  String? nickName;
  String? avatar;
  String? initial;
  bool isVip = false;
  bool isLiang = false;
  FriendModel({this.uid, this.nickName, this.avatar, this.initial});

  FriendModel.fromJson(Map json) {
    uid = json['uid'];
    isVip = json['isVip']??false;
    isLiang = json['isLiang']??false;

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['uid'] = uid;
    data['isVip'] = isVip;
    data['isLiang'] = isLiang;
    return data;
  }

  @override
  String getSuspensionTag() {
    return initial ?? '#';
  }
}
