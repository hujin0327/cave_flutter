import 'package:azlistview/azlistview.dart';
import 'package:cave_flutter/model/image_model.dart';
import 'package:cave_flutter/model/tag_model.dart';
import 'package:cave_flutter/model/user_im_model.dart';
import 'package:cave_flutter/pages/whl_mine/privacy_setting/privacy_setting_logic.dart';

class UserModel extends ISuspensionBean {
  String? accountId;
  String? nickName;
  String? avatar;
  String? gender;
  String? sexualOrientation;
  String? funRole;
  List? album;
  num? distance;
  bool? onlineStatus;

  int? id;
  String? mobile;
  String? birthDate;
  int? age;
  String? level;
  String? levelImage;
  int? charm;
  int? wealth;
  String? createTime;
  String? updateTime;
  String? visitTime;

  String? remark;
  int? block;
  String? acid;

  String? value;
  int? userId;
  String? time;

  String? cityName;
  List? baseInfo;
  List<TagModel>? soulTagList;
  String? aboutMe;
  int? joinCaveDays;
  int? memberGrade; //会员等级
  List? giftList;
  List<ImageModel>? albumList;
  bool? medal; // 是否首测
  String? medalImage;
  int? followNum; // 关注数量
  int? fansNum; // 粉丝数量
  int? caveNum;
  bool? follow; // 是否关注
  bool? friend; // 是否好友

  String? initial;
  UserImModel? userNetim;
  int? rechargeType;
  int? vip;
  int? status; //实名认证状态
  String? province;
  String? city;

  List<PrivacySettingModel>? vacies;

  UserModel(
      {this.accountId,
      this.nickName,
      this.avatar,
      this.gender,
      this.sexualOrientation,
      this.funRole,
      this.album,
      this.distance,
      this.onlineStatus,
      this.id,
      this.mobile,
      this.birthDate,
      this.age,
      this.level,
      this.levelImage,
      this.charm,
      this.wealth,
      this.createTime,
      this.updateTime,
      this.remark,
      this.block,
      this.acid,
      this.value,
      this.userId,
      this.cityName,
      this.baseInfo,
      this.soulTagList,
      this.aboutMe,
      this.joinCaveDays,
      this.memberGrade,
      this.giftList,
      this.albumList,
      this.initial,
      this.medal,
      this.medalImage,
      this.time,
      this.follow,
      this.friend,
      this.followNum,
      this.fansNum,
      this.caveNum,
      this.vip,
      this.rechargeType,
      this.userNetim,
      this.vacies,
      this.status,
      this.province,
      this.city});

  UserModel.fromJson(Map<String, dynamic> json) {
    accountId = json['accountId'];
    nickName = json['nickName'] ?? json['nickname'];
    avatar = json['avatar'];
    gender = json['gender'];
    sexualOrientation = json['sexualOrientation'] ?? json["sexual_orientation"];
    funRole = json['funRole'] ?? json["fun_role"];
    album = json['album'];
    distance = json['distance'];
    onlineStatus = json['onlineStatus'];

    id = json['id'];
    mobile = json['mobile'];
    birthDate = json['birthDate'];
    age = json['age'];
    level = json['level'].toString();
    levelImage = json['levelImage'];
    charm = json['charm'];
    wealth = json['wealth'];
    createTime = json['createTime'];
    updateTime = json['updateTime'];

    block = json['block'];
    remark = json['remark'];
    acid = json['acid'];
    userId = json["userId"];
    value = json['value'];

    medal = json['medal'];
    medalImage = json['medalImage'];
    time = json['time'];

    follow = json['follow'];
    friend = json['friend'];
    fansNum = json['fansNum'];
    followNum = json['followNum'];
    caveNum = json['caveNum'];
    vip = json['vip'];
    rechargeType = json['rechargeType'];

    cityName = json['cityName'];
    baseInfo = json['baseInfo'];
    if (json['soulTagList'] != null) {
      soulTagList = <TagModel>[];
      json['soulTagList'].forEach((v) {
        if (v is String) {
          soulTagList!.add(TagModel(id: v, label: v));
        } else {
          soulTagList!.add(TagModel.fromJson(v));
        }
      });
    }
    if (json['vacies'] != null) {
      vacies = List<PrivacySettingModel>.from(
        json['vacies'].map(
          (item) => PrivacySettingModel.fromJson(item),
        ),
      ).toList();
    }
    aboutMe = json['aboutMe'];
    joinCaveDays = json['joinCaveDays'];
    memberGrade = json['memberGrade'];
    giftList = json['giftList'];
    albumList = <ImageModel>[];
    if (json['albumList'] != null) {
      json['albumList'].forEach((v) {
        albumList!.add(ImageModel.fromJson(v));
      });
    }
    if (json['userNetim'] != null) {
      userNetim = UserImModel.fromJson(json['userNetim']);
    }
    status = json['status'];
    province = json['province'];
    city = json['city'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['accountId'] = this.accountId;
    data['nickName'] = this.nickName;
    data['avatar'] = this.avatar;
    data['gender'] = this.gender;
    data['sexualOrientation'] = this.sexualOrientation;
    data['funRole'] = this.funRole;
    data['album'] = this.album;
    data['distance'] = this.distance;
    data['onlineStatus'] = this.onlineStatus;
    data['medal'] = this.medal;
    data['medalImage'] = medalImage;
    data['time'] = this.time;
    data['vacies'] = vacies?.map((e) => e.toJson()).toList();

    data['id'] = this.id;
    data['mobile'] = this.mobile;
    data['birthDate'] = this.birthDate;
    data['age'] = this.age;
    data['level'] = this.level;
    data['levelImage'] = this.levelImage;
    data['charm'] = this.charm;
    data['wealth'] = this.wealth;
    data['createTime'] = this.createTime;
    data['updateTime'] = this.updateTime;

    data['block'] = this.block;
    data['remark'] = this.remark;
    data['acid'] = this.acid;
    data['userId'] = this.userId;
    data['value'] = this.value;
    data['follow'] = this.follow;
    data['friend'] = this.friend;
    data['fansNum'] = this.fansNum;
    data['followNum'] = this.followNum;
    data['caveNum'] = this.caveNum;
    data['vip'] = this.vip;
    data['rechargeType'] = this.rechargeType;

    data['cityName'] = this.cityName;
    data['baseInfo'] = this.baseInfo;
    if (this.soulTagList != null) {
      data['soulTagList'] = this.soulTagList!.map((v) => v.toJson()).toList();
    }
    data['aboutMe'] = this.aboutMe;
    data['joinCaveDays'] = this.joinCaveDays;
    data['memberGrade'] = this.memberGrade;
    data['giftList'] = this.giftList;
    if (albumList != null) {
      data['albumList'] = albumList!.map((v) => v.toJson()).toList();
    }
    data['userNetim'] = this.userNetim;
    data['status'] = this.status;
    data['province'] = this.province;
    data['city'] = this.city;
    return data;
  }

  @override
  String getSuspensionTag() {
    return initial ?? '#';
  }
}
