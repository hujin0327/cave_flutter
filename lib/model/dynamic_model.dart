import 'package:cave_flutter/model/image_model.dart';
import 'package:cave_flutter/model/topic_model.dart';
import 'package:cave_flutter/model/user_model.dart';
import 'package:cave_flutter/model/whl_user_info_model.dart';

class DynamicModel {
  String? pubTime;
  String? avatar;
  String? accountId;
  String? caveId;
  String? caveName;
  String? mobile;
  dynamic authStatus;
  String? audioUrl;
  int? audioTime;
  String? content;
  dynamic approveStatus;
  int? hot;
  int? topOrder;
  int? goodNum;
  int? commentNum;
  int? giftNum;
  int? fansNum;
  int? readNum;
  bool? currentZan;
  String? id;
  List<ImageModel>? urls;
  List<TopicModel>? talks;
  dynamic browsePer;
  Setting? setting;
  UserModel? user;
  String? adCode;
  String? cityCode;
  String? address;
  String? province;
  String? city;
  String? district;
  bool? essence;

  DynamicModel({
    this.pubTime,
    this.avatar,
    this.accountId,
    this.caveId,
    this.caveName,
    this.mobile,
    this.authStatus,
    this.audioUrl,
    this.audioTime,
    this.content,
    this.approveStatus,
    this.hot,
    this.topOrder,
    this.goodNum,
    this.commentNum,
    this.giftNum,
    this.fansNum,
    this.readNum,
    this.currentZan,
    this.id,
    this.urls,
    this.talks,
    this.browsePer,
    this.setting,
    this.user,
    this.adCode,
    this.cityCode,
    this.province,
    this.city,
    this.district,
    this.address,
    this.essence,
  });

  factory DynamicModel.fromJson(Map<String, dynamic> json) => DynamicModel(
        pubTime: json["pubTime"],
        avatar: json["avatar"],
        accountId: json["accountId"],
        caveId: json["caveId"]?.toString(),
        caveName: json["caveName"],
        mobile: json["mobile"],
        authStatus: json["authStatus"],
        audioUrl: json["audioUrl"],
        audioTime: json["audioTime"],
        content: json["content"],
        approveStatus: json["approveStatus"],
        hot: json["hot"],
        topOrder: json["topOrder"],
        goodNum: json["goodNum"],
        commentNum: json["commentNum"],
        giftNum: json["giftNum"],
        fansNum: json["fansNum"],
        readNum: json["readNum"],
        currentZan: json["currentZan"],
        adCode: json["adCode"],
        cityCode: json["cityCode"],
        province: json["province"],
        city: json['city'],
        district: json['district'],
        address: json['address'],
        id: json["id"],
        urls: json["urls"] == null
            ? []
            : List<ImageModel>.from(
                json["urls"]!.map((x) => ImageModel.fromJson(x))),
        talks: json["talks"] == null
            ? []
            : List<TopicModel>.from(
                json["talks"]!.map((x) => TopicModel.fromJson(x))),
        browsePer: json["browsePer"],
        setting:
            json["setting"] == null ? null : Setting.fromJson(json["setting"]),
        user: json["user"] == null ? null : UserModel.fromJson(json["user"]),
        essence: json['essence'],
      );

  Map<String, dynamic> toJson() => {
        "pubTime": pubTime,
        "avatar": avatar,
        "accountId": accountId,
        "caveId": caveId,
        "caveName": caveName,
        "mobile": mobile,
        "authStatus": authStatus,
        "audioUrl": audioUrl,
        "audioTime": audioTime,
        "content": content,
        "approveStatus": approveStatus,
        "hot": hot,
        "topOrder": topOrder,
        "goodNum": goodNum,
        "commentNum": commentNum,
        "giftNum": giftNum,
        "fansNum": fansNum,
        "readNum": readNum,
        "currentZan": currentZan,
        "id": id,
        "urls": urls == null
            ? []
            : List<dynamic>.from(urls!.map((x) => x.toJson())),
        "talks": talks == null
            ? []
            : List<dynamic>.from(talks!.map((x) => x.toJson())),
        "browsePer": browsePer,
        "setting": setting?.toJson(),
        "user": user?.toJson(),
        "province": province,
        "city": city,
        "district": district,
        "cityCode": cityCode,
        "adCode": adCode,
        "address": address,
        'essence': essence,
      };
}

class Setting {
  String? id;
  String? squareId;
  String? accountId;
  int? recommend;
  int? send;
  int? notAnonymousComment;
  int? hideComment;
  int? notComment;
  int? notDownload;
  int? notForward;
  int? watermark;

  Setting({
    this.id,
    this.squareId,
    this.accountId,
    this.recommend,
    this.send,
    this.notAnonymousComment,
    this.hideComment,
    this.notComment,
    this.notDownload,
    this.notForward,
    this.watermark,
  });

  factory Setting.fromJson(Map<String, dynamic> json) => Setting(
        id: json["id"],
        squareId: json["squareId"],
        accountId: json["accountId"],
        recommend: json["recommend"],
        send: json["send"],
        notAnonymousComment: json["notAnonymousComment"],
        hideComment: json["hideComment"],
        notComment: json["notComment"],
        notDownload: json["notDownload"],
        notForward: json["notForward"],
        watermark: json["watermark"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "squareId": squareId,
        "accountId": accountId,
        "recommend": recommend,
        "send": send,
        "notAnonymousComment": notAnonymousComment,
        "hideComment": hideComment,
        "notComment": notComment,
        "notDownload": notDownload,
        "notForward": notForward,
        "watermark": watermark,
      };
}
