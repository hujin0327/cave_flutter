import 'package:cave_flutter/model/topic_model.dart';

class DynamicDraftModel {
  List<TopicModel>? selectTopicList = [];
  String? content;
  String? audioUrl;
  String? audioPath;
  String? imagePath;
  String? url;
  String? perId;
  String? perName;
  String? userIds;
  Map<String, dynamic>? setting;

  DynamicDraftModel({this.selectTopicList, this.content, this.audioUrl ,this.audioPath, this.imagePath, this.url,
      this.perId, this.perName, this.userIds, this.setting});


  DynamicDraftModel.fromJson(Map json) {
    selectTopicList = <TopicModel>[];
    if (json['selectTopicList'] != null) {

      json['selectTopicList'].forEach((v) {
        selectTopicList!.add(TopicModel.fromJson(v));
      });
    }
    content = json['content'];
    audioUrl = json['audioUrl'];
    audioPath = json['audioPath'];
    imagePath = json['imagePath'];
    url = json['url'];
    perId = json['perId'];
    perName = json['perName'];
    userIds = json['userIds'];
    setting = json['setting'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['selectTopicList'] = selectTopicList;
    data['content'] = content;
    data['audioUrl'] = audioUrl;
    data['audioPath'] = audioPath;
    data['imagePath'] = imagePath;
    data['url'] = url;
    data['perId'] = perId;
    data['perName'] = perName;
    data['userIds'] = userIds;
    data['setting'] = setting;
    if (selectTopicList != null) {
      data['selectTopicList'] = selectTopicList!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
