import 'package:cave_flutter/model/image_model.dart';
import 'package:cave_flutter/model/tag_model.dart';

class OtherUserModel {
  FunRole? funRole;
  List<FunRole>? addFriendTarget;
  FunRole? education;
  FunRole? gender;
  FunRole? emotionalState;
  String? weight;
  String? secureDesc;
  String? avatar;
  String? birthDate;
  int? userId;
  FunRole? sexualOrientation;
  int? schedule;
  String? constellation;
  String? school;
  List<ImageModel>? uploadUserAlbumBO;
  String? nickname;
  String? monthlyPay;
  String? worker;
  String? introduction;
  int? age;
  List<TagModel>? soulTagList;
  String? height;

  OtherUserModel(
      {this.funRole,
        this.addFriendTarget,
        this.education,
        this.gender,
        this.emotionalState,
        this.weight,
        this.secureDesc,
        this.avatar,
        this.birthDate,
        this.userId,
        this.sexualOrientation,
        this.schedule,
        this.constellation,
        this.school,
        this.uploadUserAlbumBO,
        this.nickname,
        this.monthlyPay,
        this.worker,
        this.introduction,
        this.age,
        this.soulTagList,
        this.height});

  OtherUserModel.fromJson(Map<String, dynamic> json) {
    funRole = json['fun_role'] != null
        ? new FunRole.fromJson(json['fun_role'])
        : null;
    if (json['addFriendTarget'] != null) {
      addFriendTarget = <FunRole>[];
      json['addFriendTarget'].forEach((v) {
        addFriendTarget!.add(FunRole.fromJson(v));
      });
    }
    education = json['education'] != null
        ? new FunRole.fromJson(json['education'])
        : null;
    gender =
    json['gender'] != null ? new FunRole.fromJson(json['gender']) : null;
    emotionalState = json['emotionalState'] != null
        ? new FunRole.fromJson(json['emotionalState'])
        : null;
    weight = json['weight'];
    secureDesc = json['secureDesc'];
    avatar = json['avatar'];
    birthDate = json['birthDate'];
    userId = json['userId'];
    sexualOrientation = json['sexual_orientation'] != null
        ? new FunRole.fromJson(json['sexual_orientation'])
        : null;
    schedule = json['schedule'];
    constellation = json['constellation'];
    school = json['school'];
    uploadUserAlbumBO = <ImageModel>[];
    if (json['uploadUserAlbumBO'] != null) {
      json['uploadUserAlbumBO'].forEach((v) {
        uploadUserAlbumBO!.add(ImageModel.fromJson(v));
      });
    }
    nickname = json['nickname'];
    monthlyPay = json['monthlyPay'];
    worker = json['worker'];
    introduction = json['introduction'];
    age = json['age'];
    if (json['soulTagList'] != null) {
      soulTagList = <TagModel>[];
      json['soulTagList'].forEach((v) {
        soulTagList!.add(TagModel.fromJson(v));
      });
    }
    height = json['height'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.funRole != null) {
      data['fun_role'] = this.funRole!.toJson();
    }
    if (this.addFriendTarget != null) {
      data['addFriendTarget'] =
          this.addFriendTarget!.map((v) => v.toJson()).toList();
    }
    if (this.education != null) {
      data['education'] = this.education!.toJson();
    }
    if (this.gender != null) {
      data['gender'] = this.gender!.toJson();
    }
    if (this.emotionalState != null) {
      data['emotionalState'] = this.emotionalState!.toJson();
    }
    data['weight'] = this.weight;
    data['secureDesc'] = this.secureDesc;
    data['avatar'] = this.avatar;
    data['birthDate'] = this.birthDate;
    data['userId'] = this.userId;
    if (this.sexualOrientation != null) {
      data['sexual_orientation'] = this.sexualOrientation!.toJson();
    }
    data['schedule'] = this.schedule;
    data['constellation'] = this.constellation;
    data['school'] = this.school;
    if (this.uploadUserAlbumBO != null) {
      data['uploadUserAlbumBO'] =
          this.uploadUserAlbumBO!.map((v) => v.toJson()).toList();
    }
    data['nickname'] = this.nickname;
    data['monthlyPay'] = this.monthlyPay;
    data['worker'] = this.worker;
    data['introduction'] = this.introduction;
    data['age'] = this.age;
    if (this.soulTagList != null) {
      data['soulTagList'] = this.soulTagList!.map((v) => v.toJson()).toList();
    }
    data['height'] = this.height;
    return data;
  }
}

class FunRole {
  String? lable;
  String? id;

  FunRole({this.lable, this.id});

  FunRole.fromJson(Map<String, dynamic> json) {
    lable = json['lable'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['lable'] = this.lable;
    data['id'] = this.id;
    return data;
  }
}




