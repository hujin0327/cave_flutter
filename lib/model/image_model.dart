import 'package:flutter/cupertino.dart';
import 'package:photo_manager/photo_manager.dart';

class ImageModel {
  String? imageUrl;
  String? id;
  String? squareId;
  String? name;
  String? contentType;
  // int? size;
  String? createBy;
  String? createTime;
  int? type;

  String? coverUrl;
  bool? canDelete;
  String? description;

  /*------------custom field------------*/
  ImageProvider? image;
  ImageProvider? originalImage;
  AssetEntity? assetEntity;
  String? imagePath = "";
  bool? isVideo = false;
  bool? isDocument = false;
  int? imageStatus =
      0; //上传状态： 0 未上传 1 上传成功 -1 上传失败

  ImageModel({
    this.id,
    this.imageUrl,
    this.squareId,
    this.name,
    this.contentType,
    // this.size,
    this.createBy,
    this.createTime,
    this.type,

    this.coverUrl,
    this.canDelete,
    this.description,
    this.image,
    this.originalImage,
    this.assetEntity,
    this.imagePath = "",
    this.isDocument = false,
    this.imageStatus = 0,
  });

  ImageModel.fromJson(Map<String, dynamic> json) {
    id = json['id'] ?? "";
    imageUrl = json['imageUrl'] ?? json["url"]??json["photoUrl"]??"";
    squareId = json['squareId'] ?? "";
    name = json['name'] ?? "";
    contentType = json['contentType'] ?? "";
    // size = json['size']??0;
    createBy = json['createBy'] ?? "";
    createTime = json['createTime'] ?? "";
    type = json['type'] ?? 0;

    coverUrl = json['coverUrl'] ?? "";
    canDelete = json['canDelete'] ?? false;
    description = json['description'] ?? "";

    if (contentType!.contains("video")) {
      isVideo = true;
      isDocument = true;
    } else if (!contentType!.contains("image")) {
      isDocument = true;
    }
    imageStatus = 1;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = id;
    data['imageUrl'] = imageUrl;
    data['squareId'] = squareId;
    data['name'] = name;
    data['contentType'] = contentType;
    // data['size'] = this.size;
    data['createBy'] = createBy;
    data['createTime'] = createTime;
    data['type'] = type;

    data['coverUrl'] = coverUrl;
    data['canDelete'] = canDelete;
    data['description'] = description;
    return data;
  }
}
