class WhlCoinGoodsModel {
  String? goodsId;
  String? code;
  String? icon;
  String? type;
  int? subType;
  String? tags;
  double? discount;
  double? originalPrice;
  double? price;
  int? exchangeCoin;
  double? originalPriceRupee;
  double? priceRupee;
  double? localPaymentPriceRupee;
  bool? isPromotion;
  int? localPayOriginalPrice;
  int? localPayPrice;

  WhlCoinGoodsModel(
      {this.goodsId,
        this.code,
        this.icon,
        this.type,
        this.subType,
        this.tags,
        this.discount,
        this.originalPrice,
        this.price,
        this.exchangeCoin,
        this.originalPriceRupee,
        this.priceRupee,
        this.localPaymentPriceRupee,
        this.isPromotion,
        this.localPayOriginalPrice,
        this.localPayPrice});

  WhlCoinGoodsModel.fromJson(Map<String, dynamic> json) {
    goodsId = json['goodsId'];
    code = json['code'];
    icon = json['icon'];
    type = json['type'];
    subType = json['subType'];
    tags = json['tags'];
    discount = json['discount'];
    originalPrice = json['originalPrice'];
    price = json['price'];
    exchangeCoin = json['exchangeCoin'];
    originalPriceRupee = json['originalPriceRupee'];
    priceRupee = json['priceRupee'];
    localPaymentPriceRupee = json['localPaymentPriceRupee'];
    isPromotion = json['isPromotion'];
    localPayOriginalPrice = json['localPayOriginalPrice'];
    localPayPrice = json['localPayPrice'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['goodsId'] = goodsId;
    data['code'] = code;
    data['icon'] = icon;
    data['type'] = type;
    data['subType'] = subType;
    data['tags'] = tags;
    data['discount'] = discount;
    data['originalPrice'] = originalPrice;
    data['price'] = price;
    data['exchangeCoin'] = exchangeCoin;
    data['originalPriceRupee'] = originalPriceRupee;
    data['priceRupee'] = priceRupee;
    data['localPaymentPriceRupee'] = localPaymentPriceRupee;
    data['isPromotion'] = isPromotion;
    data['localPayOriginalPrice'] = localPayOriginalPrice;
    data['localPayPrice'] = localPayPrice;
    return data;
  }
}