enum BaseItemTypeENUM {
  ENUM_InputCell, //Input
  ENUM_MoreInputCell, //more Input
  ENUM_SelectCell, //select
  ENUM_SelectTypeCell,
  ENUM_headerCell,
  ENUM_AddCell,
  ENUM_visitCell,
  ENUM_subVisitCell,
  ENUM_statusCell,
}

enum BaseModelTypeENUM {
  MODELTYPE_DefaultModel,
  MODELTYPE_AddressModel,
  MODELTYPE_educationModel,
  MODELTYPE_NameModel,
  MODELTYPE_PhoneModel,
  MODELTYPE_genderModel,
  MODELTYPE_funRoleModel,
  MODELTYPE_sexualOrientation,
  MODELTYPE_qgzt,
  MODELTYPE_friend,
  MODELTYPE_ID,
  MODELTYPE_Title,
  MODELTYPE_Tag,
  MODELTYPE_Avatar,
  MODELTYPE_Photo,
}

class BaseItemModel {
  BaseItemTypeENUM? itemType;
  BaseModelTypeENUM? modelType;
  String? title = "";
  String? value = "";
  String? imageName = "";
  String? hitString = "";
  bool? isRequired = false;
  List? datas = [];
  dynamic otherDatas;
  dynamic otherModel;
  String? otherValue = "";
  dynamic requestValue;
  String? requestKey = "";
  bool? canEdit = true;
  int? maxLength = 0;
  bool? showPhotoName = true;
  bool? otherBool = false;
  String pageTitle;
  String? status;
  bool? hasRedView = false;
  String clickText;
  int? targetPageType = 0;
  int? pageType = 0;
  bool? isShow = true;
  bool? isSelect = false;
  bool? needUpload = false;

  BaseItemModel({
    required this.title,
    this.itemType,
    this.modelType,
    this.value = "",
    this.imageName = "",
    this.hitString = "",
    this.isRequired = false,
    this.datas,
    this.otherDatas,
    this.otherModel,
    this.otherValue = "",
    this.requestValue,
    this.requestKey = "",
    this.canEdit = true,
    this.maxLength = 0,
    this.showPhotoName = true,
    this.otherBool = false,
    this.pageTitle = "",
    this.status,
    this.hasRedView = false,
    this.clickText = "CONFIRM",
    this.targetPageType,
    this.pageType,
    this.isShow = true,
    this.isSelect = false,
  });
}

class BaseItemSectionModel {
  BaseModelTypeENUM? modelType;
  String? title;
  String? value;
  String? imageName;
  String? footerTitle = "";
  List? itemDatas = [];
  dynamic otherDatas;
  String? otherValue;
  bool? isRequired = false;
  bool? isShow = true;
  bool? isSelect = false;
  bool? hasMore = false;
  bool? canAdd = false;
  int? maxLength = 0;

  BaseItemSectionModel(
      {required this.title,
      this.modelType,
      this.value,
      this.imageName,
      this.itemDatas,
      this.otherDatas,
      this.otherValue,
      this.isRequired = false,
      this.isShow = true,
      this.isSelect = false,
      this.hasMore = false,
      this.canAdd = false,
      this.maxLength = 0,
      this.footerTitle = ""}) {
    itemDatas ??= [];
  }
}
