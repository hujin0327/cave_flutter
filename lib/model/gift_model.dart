class GiftModel {
  String? id;
  String? giftName;
  String? imageUrl;
  int? giftPrice;
  bool? isReceive;
  int? count;

  GiftModel(
      {this.id,
        this.giftName,
        this.imageUrl,
        this.giftPrice,
        this.isReceive,
        this.count});

  GiftModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    giftName = json['giftName'];
    imageUrl = json['imageUrl'];
    giftPrice = json['giftPrice'];
    isReceive = json['isReceive'];
    count = json['count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['giftName'] = this.giftName;
    data['imageUrl'] = this.imageUrl;
    data['giftPrice'] = this.giftPrice;
    data['isReceive'] = this.isReceive;
    data['count'] = this.count;
    return data;
  }
}
