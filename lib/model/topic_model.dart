

class TopicModel {
  String? id;
  String? talkName;
  String? imageUrl;
  String? talkBrief;
  String? squareId;

  TopicModel({
    this.id,
    this.talkName,
    this.imageUrl,
    this.talkBrief,
    this.squareId,
  });

  factory TopicModel.fromJson(Map<String, dynamic> json) => TopicModel(
    id: json["id"],
    talkName: json["talkName"],
    imageUrl: json["imageUrl"],
    talkBrief: json["talkBrief"],
    squareId: json["squareId"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "talkName": talkName,
    "imageUrl": imageUrl,
    "talkBrief": talkBrief,
    "squareId": squareId,
  };
}
