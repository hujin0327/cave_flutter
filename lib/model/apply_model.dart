class ApplyModel {
  String? accountId;
  int? applyId;
  String? avatar;
  bool? mutualFollow;
  String? nickName;

  ApplyModel(
      {this.accountId,
        this.applyId,
        this.avatar,
        this.mutualFollow,
        this.nickName});

  ApplyModel.fromJson(Map<String, dynamic> json) {
    accountId = json['accountId'];
    applyId = json['applyId'];
    avatar = json['avatar'];
    mutualFollow = json['mutualFollow'];
    nickName = json['nickName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['accountId'] = this.accountId;
    data['applyId'] = this.applyId;
    data['avatar'] = this.avatar;
    data['mutualFollow'] = this.mutualFollow;
    data['nickName'] = this.nickName;
    return data;
  }
}
