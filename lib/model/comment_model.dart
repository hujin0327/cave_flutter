class CommentModel {
  String? id;
  String? avatar;
  String? accountId;
  String? nickName;
  String? squareId;
  String? audioUrl;
  String? content;
  int? goodNum;
  String? pubTime;
  int? anonymous;
  List<ReplyCommentModel>? messages;
  bool? isGood = false;

  /**-本地自定义字段--*/
  bool? isShow = false;

  CommentModel(
      {this.id,
        this.avatar,
        this.accountId,
        this.nickName,
        this.squareId,
        this.audioUrl,
        this.content,
        this.goodNum,
        this.pubTime,
        this.anonymous,
        this.messages});

  CommentModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    avatar = json['avatar'];
    accountId = json['accountId'];
    nickName = json['nickName'];
    squareId = json['squareId'];
    audioUrl = json['audioUrl'];
    content = json['content'];
    goodNum = json['goodNum'];
    pubTime = json['pubTime'];
    anonymous = json['anonymous'];
    if (json['messages'] != null) {
      messages = <ReplyCommentModel>[];
      json['messages'].forEach((v) {
        messages!.add(new ReplyCommentModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['avatar'] = this.avatar;
    data['accountId'] = this.accountId;
    data['nickName'] = this.nickName;
    data['squareId'] = this.squareId;
    data['audioUrl'] = this.audioUrl;
    data['content'] = this.content;
    data['goodNum'] = this.goodNum;
    data['pubTime'] = this.pubTime;
    data['anonymous'] = this.anonymous;
    if (this.messages != null) {
      data['messages'] = this.messages!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ReplyCommentModel {
  String? id;
  String? sourceAccountId;
  String? sourceNickName;
  String? targetAccountId;
  String? targetNickName;
  String? squareId;
  String? commentId;
  String? content;
  String? pubTime;

  ReplyCommentModel(
      {this.id,
        this.sourceAccountId,
        this.sourceNickName,
        this.targetAccountId,
        this.targetNickName,
        this.squareId,
        this.commentId,
        this.content,
        this.pubTime});

  ReplyCommentModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    sourceAccountId = json['sourceAccountId'];
    sourceNickName = json['sourceNickName'];
    targetAccountId = json['targetAccountId'];
    targetNickName = json['targetNickName'];
    squareId = json['squareId'];
    commentId = json['commentId'];
    content = json['content'];
    pubTime = json['pubTime'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['sourceAccountId'] = this.sourceAccountId;
    data['sourceNickName'] = this.sourceNickName;
    data['targetAccountId'] = this.targetAccountId;
    data['targetNickName'] = this.targetNickName;
    data['squareId'] = this.squareId;
    data['commentId'] = this.commentId;
    data['content'] = this.content;
    data['pubTime'] = this.pubTime;
    return data;
  }
}
