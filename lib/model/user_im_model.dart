
class UserImModel {
  String? token;
  String? netimId;
  int? userId;

  UserImModel({
    this.token,
    this.netimId,
    this.userId,
  });

  UserImModel.fromJson(Map<String, dynamic> json) {
    token = json['token'] ?? "";
    netimId = json['netimId'] ?? "";
    userId = json['userId'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['token'] = token;
    data['netimId'] = netimId;
    data['tagId'] = userId;
    return data;
  }
}