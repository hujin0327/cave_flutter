import 'package:cave_flutter/pages/whl_mine/setting/setting_logic.dart';
import 'package:cave_flutter/pages/whl_mine/whl_mine_logic.dart';
import 'package:easy_refresh/easy_refresh.dart';

import '../pages/whl_mine/setting/account_safe/account_safe_logic.dart';
import '../pages/whl_mine/share/share_logic.dart';

enum LoginType {
  phone,
  password,
  wechat,
  qq,
  weibo,
}

class BaseModel {
  String? title;
  String? subTitle;
  String? content;
  String? icon;
  String? id;
  LoginType? loginType;
  SettingType? settingType;
  AccountSafeType? accountSafeType;
  MineType? mineType;
  TabType? tabType;
  ShareType? shareType;
  List? data;
  EasyRefreshController? refreshController;
  BaseModel({
    this.title,
    this.subTitle,
    this.content,
    this.icon,
    this.id,
    this.loginType,
    this.settingType,
    this.accountSafeType,
    this.mineType,
    this.tabType,
    this.shareType,
    this.data,
    this.refreshController,
  });
}
