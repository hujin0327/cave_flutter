import 'package:cave_flutter/utils/whl_user_utils.dart';
import 'package:sp_util/sp_util.dart';

import 'whl_network_config.dart';

class WhlNetWorkUtils {
  //单例
  factory WhlNetWorkUtils() => _getInstance();

  static WhlNetWorkUtils get instance => _getInstance();
  static WhlNetWorkUtils? _instance;
  static late WhlNetWorkConfig config;
  static bool isShowUpdateDialog = false;
  WhlNetWorkUtils._internal();

  static WhlNetWorkUtils _getInstance() {
    _instance ??= WhlNetWorkUtils._internal();
    return _instance!;
  }

  initConfig(WhlNetWorkConfig config) {
    WhlNetWorkUtils.config = config;
  }

  static Future<bool> setCookie(String cookie) async {
    return await SpUtil.putString('cookie', cookie) ?? false;
  }
  static String getCookie() {
    return SpUtil.getString('cookie', defValue: "") ?? "";
  }

  static Future<bool> setAccessToken(String accessToken) async {
    if (getAccessToken().isNotEmpty) {
      WhlUserUtils.switchAccount();
    }
    return await SpUtil.putString('accessToken', accessToken) ?? false;
  }
  static String getAccessToken() {
    return SpUtil.getString('accessToken', defValue: "") ?? "";
  }
}