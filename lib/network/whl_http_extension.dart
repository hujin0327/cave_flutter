import 'package:cave_flutter/config/whl_global_config.dart';

import 'whl_http_manager.dart';
import 'whl_result_data.dart';

extension WhlHttpExtension on String {
  ///Get请求
  Future<ResponseData> get(
    Map<String, dynamic> params, {
    withLoading = false,
    isJson = true,
    hideMsg = false,
    isNeedCommonParams = true,
  }) async {
    String baseUrl = WHLGlobalConfig.getInstance().getBaseUrl();
    String url = this;
    if (!contains("http")) {
      url = baseUrl + this;
    }
    dynamic result = await WhlHttpManager()
        .get(url, params: params, hideMsg: hideMsg, withLoading: withLoading);
    if (result is ResponseData) {
      return result;
    }
    return ResponseData(code: "-1", data: {}, msg: "数据出现点问题");
  }

  ///Post请求
  Future<ResponseData> post(
    params, {
    withLoading = false,
    isJson = true,
    hideMsg = false,
    isNeedCommonParams = true,
  }) async {
    String baseUrl = WHLGlobalConfig.getInstance().getBaseUrl();
    String url = this;
    if (!contains("http")) {
      url = baseUrl + this;
    }
    dynamic result = await WhlHttpManager().post(url,
        params: params,
        isJson: isJson,
        hideMsg: hideMsg,
        isNeedCommonParams: isNeedCommonParams,
        withLoading: withLoading);
    if (result is ResponseData) {
      return result;
    }
    // if (GlobalConfig.isDebug) {
    //   throw UnsupportedError("统一返回值类型应该为ResponseData,当前类型为:${result.runtimeType},${result.toString()},需处理下");
    // }
    return ResponseData(code: "-1", data: {}, msg: "数据出现点问题");
  }

  ///Delete请求
  Future<ResponseData> delete(
    Map<String, dynamic> params, {
    withLoading = false,
    hideMsg = false,
  }) async {
    String url = this;
    dynamic result = await WhlHttpManager().delete(url,
        params: params, hideMsg: hideMsg, withLoading: withLoading);
    if (result is ResponseData) {
      return result;
    }
    // if (GlobalConfig.isDebug) {
    //   throw UnsupportedError("统一返回值类型应该为ResponseData,当前类型为:${result.runtimeType},${result.toString()},需处理下");
    // }
    return ResponseData(code: "-1", data: {}, msg: "数据出现点问题");
  }
}
