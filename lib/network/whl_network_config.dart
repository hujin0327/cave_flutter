class WhlNetWorkConfig {
  final String packageName;
  final String versionName;
  final String deviceId;
  final String platform;
  final int appVersionCode;
  final String successCode;
  final String noLoginCode;
  final String? requestErrorTip;
  final String? requestTimeoutTip;
  final String? model;
  WhlNetWorkConfig({
    required this.packageName,
    required this.versionName,
    required this.deviceId,
    required this.platform,
    required this.model,
    required this.appVersionCode,
    required this.successCode,
    required this.noLoginCode,
    this.requestErrorTip = '网络请求失败',
    this.requestTimeoutTip = '网络请求超时',
  });
}
