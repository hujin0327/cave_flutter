export 'whl_http_extension.dart';
export 'whl_result_data.dart';

class WhlApi {
  //host
  // static String baseUrl = 'http://app.cave-meta.com:7600/';
  //上报
  static String reportLog = 'http://log.heeru.xyz/log/live-chat';
  //翻译
  static String translate =
      'https://translation.googleapis.com/language/translate/v2';
  //隐私政策URL
  static String privacyPolicy =
      'http://app.cave-meta.com:8888/agreement/privacy.html';
  //用户协议URL
  static String termConditions =
      'http://app.cave-meta.com:8888/agreement/service-agreement.html';
  //  获取app版本
  static String getAppVersion = '/app/cave/parameter/config/getVersion';

  /*-----登录相关------*/
  //获取验证码
  static String getAuthCode = '/app/phone-message/na/message';
  //验证码登录
  static String loginForCode = '/app/user/na/authCodeLogin';
  //忘记密码
  static String forgetPassword = '/app/verify/na/code';
  //保存密码
  static String savePassword = '/app/user/savePassword';
  //保存密码
  static String saveNaPassword = '/app/user/na/savePassword';
  //用户登录
  static String passwordLogin = '/app/user/na/passwordLogin';
  //保存用户基本信息
  static String saveUserBaseInfo = '/app/user/saveUserBaseInfo';
  //获取IM token
  static String getIMToken = '/app/net-im/token';
  //获取rtc token
  static String getRTCToken = '/app/net-rtc/token';
  //获取im acid
  static String getChatAcid = '/app/net-im/getUserAcid';
  //快捷登录
  static String fastLogin = '/app/user/na/oneClickLogin';
  //获取性别数据
  static String getGenderList = '/app/gender/list';
  //获取情趣角色数据
  static String getQQJSList = '/app/role/list';
  //获取情感状态数据
  static String getQGZTList = '/app/user-emotional-state/list';
  //获取学历数据
  static String getEducationList = '/app/user-education/list';
  //获取交友目的数据
  static String getFriendMDList = '/app/add-friend-target/list';
  //获取性取向数据
  static String getSexQXList = '/app/sex/list';

  //登录用户信息
  static String getCurrentUserInfo = '/app/user/currentUser';
  //获取灵魂代码
  // static String getLHTag = '/app/tag/pageQuery';
  //绑定灵魂代码
  static String bindLHTag = '/app/user-soul/saveSoul';

  //更换头像
  static String updateAvatar = '/user/updateAvatar';
  //保存用户配置资料
  static String saveUserConfig = '/app/user-config/saveConfig';
  //保存用户资料
  static String saveUserInformation = '/app/user/saveUserInformation';

  //保存用户当前经纬度
  static String saveUserLocation = '/app/user-location/saveLocation';

  /// 我的-绑定QQ、微信、微博
  static String userBind = '/app/my-home/userBind';

  /// 我的-绑定的支付账户
  static String myBindBindPayment = '/app/my-home/myBindBindPayment';

  /// 我的-绑定的支付账户
  static String bindBindPayment = '/app/my-home/bindBindPayment';

  /// 我的-收货地址
  static String getUserReceiveAddress = '/app/my-home/getUserReceiveAddress';

  /// 我的-保存收货地址
  static String saveUserReceiveAddress = '/app/my-home/saveUserReceiveAddress';

  //所有套餐
  // static String coinShopList = '${baseUrl}coin/goods/search';
  //购买套餐
  // static String createOrder = '${baseUrl}coin/recharge/create';
  //校验订单
  // static String checkOrder = '${baseUrl}coin/recharge/payment/ipa';
  //
  // static String consumeCoin = '${baseUrl}coin/reviewModeConsume';

  static String uploadFile = '/app/file/upload';

  /*---------交友-------*/
  //附近的人
  static String getNearbyPeople = '/app/nearbyPeople/pageQuery1';
  //用户详情
  // static String getUserDetail = '/app/user/searchByAccountId/';
  static String getPersonDetail = '/app/user/personPage';
  //添加好友
  static String applyFriend = '/app/user-friend/applyFriend/';
  //处理好友申请
  static String applyFriendHandle = '/app/user-friend/applyFriendHandle';
  //好友申请列表
  static String applyFriendList = '/app/user-friend/applyFriendList';
  //好友列表
  static String friendList = '/app/user-friend/friendList';
  //用户动态列表
  static String getUserDynamicsList = '/app/cave/square/dynamics';
  //好友申请设置

  static String getSoulPeoples = '/app/nearbyPeople/soulPeoples';

  /*----广场----*/
  //广场列表
  static String getSquareList = '/app/cave/square/list';
  //广场详情
  static String getSquareDetail = '/app/cave/square/getById/';
  //广场点赞
  static String squareDianzan = '/app/cave/square/dianzan';
  //  Cave点赞
  static String caveDianzan = '/app/cave/post/dianzan';
  //广场置顶
  static String squareToTop = '/app/cave/square/top';
  static String caveToTop = '/app/cave/post/top';
  static String caveEssence = '/app/cave/post/essence';

  static String editTopOrder = '/app/cave/square/editTopOrder';
  static String editCavTopOrder = '/app/cave/post/editTopOrder';
  //  修改访问量
  static String editReadNum = '/app/cave/square/editReadNum';
  //  道具推顶卡
  static String getTopNum = '/app/user-prop/getTopNum';

  //广场收藏
  static String squareCollect = '/app/cave/square/collect';
  //Cave收藏
  static String caveCollect = '/app/cave/post/collect';

  //广场删除
  static String squareDelete = '/app/cave/square/delete';
  static String caveDelete = '/app/cave/post/delete';

  //广场获取举报类型
  static String getCaveAppReportType = '/app/cave/report/getCaveAppReportType';
  //发布动态
  static String sendSquare = '/app/cave/square/add';
  //编辑动态
  static String editSquare = '/app/cave/square/edit';
  //获取话题列表
  static String getTopicList = '/app/cave/square/getTalks';
  //获取权限列表
  static String getBrowseList = '/app/cave/square/getBrowse';
  //获取高级设置列表
  static String getHighSetList = '/app/cave/square/getSetting';
  //获取评论列表
  static String getCommentList = '/app/cave/square/comment/list';
  static String getCaveCommentList = '/app/cave/post/comment/list';
  //新增评论
  static String addComment = '/app/cave/square/comment/add';
  static String addCaveComment = '/app/cave/post/comment/add';
  //点赞评论
  static String commentDianzan = '/app/cave/square/comment/dianzan';
  static String commentCaveDianzan = '/app/cave/post/comment/dianzan';
  //回复评论
  static String replyComment = '/app/cave/square/comment/message/add';
  static String replyCaveComment = '/app/cave/post/comment/message/add';
  /*-----IM----*/
  //发送单聊消息
  static String sendChatSingleMessage = '/app/net-im/sendSingleMessage';
  //批量转化用户信息ACID->用户数据
  static String batchReplaceUserInfo = '/app/net-im/batchReplaceUserInfo';
  //转化cave
  static String batchCaveInfo = '/app/cave/batchReplaceCave';
  //设置备注
  static String saveRemark = '/app/userRemark/saveRemark';

  /*-------我的-------*/
  // 我的灵魂标签
  static String getMySoul = '/app/user-soul/soul';
  // 分享次数保存
  static String saveShareCount = '/app/user-share/saveShare';
  // 分享详情
  static String getShareState = '/app/my-home/shareInfo';
  // 邀请排行榜
  static String getInviteRankList = '/app/my-home/inviteRanking';
  static String homeRankg = '/app/user/ranking';
  // 邀请记录
  static String getMyInviteRecord = '/app/my-home/myInviteRecord';
  // 关注列表
  static String getFollowList = '/app/user-follow/followList';
  // 粉丝列表
  static String getFansList = '/app/user-follow/fansList';
  // 我的cave
  static String myCave = '/app/cave/myCaveList';
  // 关注
  static String userFollow = '/app/user-follow/follow/';
  // 取消关注
  static String userUnfollow = '/app/user-follow/unFollow/';
  //我的礼物列表
  static String myGiftList = '/app/my-home/giftList';
  //赠送礼物
  static String giveGift = '/app/user-gift/giveGift';

  //修改密码
  static String updatePassword = '/app/user/updatePassword';
  // 绑定支付宝
  static String bindAlipay = '/app/my-home/bindBindPayment';
  // 获取绑定支付宝信息
  static String getBindAlipay = '/app/my-home/myBindBindPayment';
  // 提现
  static String withdraw = '/app/user-withdraw/saveWithdraw';

  // 我的认证
  static String myUserVerification = '/app/my-home/myUserVerification';
  // 用户认证
  static String userVerification = '/app/user/verification';

  // 注销账号
  static String userLogout = '/app/my-home/userLogout';

  /*-------Cave-------*/
  // 热门top5
  static String getHotCave = '/app/cave/caveTopFive';
  // 获取分类
  static String getCaveCategory = '/app/cave-categories/queryCategories';
  // 首页获取个人标签下的cave
  static String getPersonalCategory = '/app/cave/indexPagePersonalClass';
  // 创建 Cave
  static String createCave = '/app/cave/applyCave';
  // 申请加入Cave
  static String applyJoinCave = '/app/cave/applyJoinCave';
  // 设置公告
  static String setCaveNotice = '/app/cave/saveCaveAnnouncements';
  // Cave 详情
  static String getCaveDetail = '/app/cave/caveDetail';
  // 搜索 Cave
  static String searchCave = '/app/cave/search';
  // 发cave 红包
  static String sendCaveRedPacket = '/app/cave/sendCaveRedWarp';
  // 获取cave中身份
  static String getRoleInCave = '/app/cave/getUserCaveIdInfo';
  // 获取cave 中帖子
  static String getCaveSquareList = '/app/cave/post/list';
  // 发布cave 帖子
  static String addCavePost = '/app/cave/post/add';
  // 发送群聊消息
  static String sendChatGroupMessage = '/app/net-im/sendGroupMessage';
  // 申请创建/修改Cave
  static String applyCave = '/app/cave/applyCave';
  // 设置Cave公告
  static String saveCaveAnnouncements = '/app/cave/saveCaveAnnouncements';
  // 查询cave成员
  static String pageCaveUser = '/app/cave/pageCaveUser';
  // 查询加入条件
  static String getCaveJoinCondition = '/app/cave/getJoinCondition';
  // 设置加入条件
  static String saveCaveJoinCondition = '/app/cave/saveJoinCondition';
  // 查询群主设置
  static String getMasterPerDetail = '/app/cave/getMasterPerDetail';
  // 保存群主设置
  static String saveCavePer = '/app/cave/saveCavePer';
  // 获取管理员配置
  static String getAdminPerDetail = '/app/cave/getAdminPerDetail';
  // 退出Cave
  static String exitCave = '/app/cave/delCave';
  // 解散Cave
  static String delCave = '/app/cave/delCave';
  // 成员管理-设置管理员
  static String caveHandleAdmin = '/app/cave/handleAdmin';
  // 成员管理-设置嘉宾
  static String caveHandleGuest = '/app/cave/handleGuest';
  // 成员管理-设置禁言/解除禁令
  static String caveHandleProhibit = '/app/cave/handleProhibit';
  // 成员管理-移除成员
  static String caveRemoveMember = '/app/cave/removeMember';

  /// 新增举报
  static String caveReport = '/app/cave/square/report';

  /*-----我的模块----*/
  // 我的首页
  static String myHome = '/app/my-home/data';
  // 我的收藏
  static String myCollect = '/app/my-home/myCollect';
  // 我的黑名单
  static String myBlackList = '/app/my-home/myBlack';
  // 加入/删除黑名单
  static String editBlack = '/app/black/editBlack';
  // 排行榜
  static String myRanking = '/app/my-home/myRanking';
  // 我的-语音连线价格
  static String myConnectionPrice = '/app/my-home/myConnectionPrice';
  // 我的-设置语音连线价格
  static String connectionPrice = '/app/my-home/connectionPrice';
  // 我的-个人资料
  static String myUserInformation = '/app/my-home/myUserInformation';
  // 访客列表
  static String visitClientList = '/app/user-visit_log/visitList';
  // 访问列表
  static String accessList = '/app/user-visit_log/accessList';
  // 添加访问记录
  static String addVisit = '/app/user-visit_log/saveLog';
  // 获取用户配置
  static String getCurrentUserConfig = '/app/user-config/currentUserConfig';
  // 配置用户
  static String saveConfig = '/app/user-config/saveConfig';
  // 隐私管理
  static String myPrivacy = '/app/my-home/myPrivacy';
  static String saveMyPrivacy = '/app/my-home/saveMyPrivacy';
  //  勋章墙
  static String getMedalList = '/app/user-medal/getMedalList';
  //  穿戴/取下勋章
  static String wearingMedal = '/app/user-medal/wearingMedal';

  /// 申请勋章
  static String applyForMedal = '/app/user-medal/applyForMedal';

  //  任务列表
  static String getTask = '/app/user-task/getTask';
  //  获取签到信息
  static String getSignInfo = '/app/user-sign/getSignInfo';
  //  签到
  static String saveSign = '/app/user-sign/saveSign';
  //  用户等级
  static String userLevel = '/app/my-home/userLevel';

  //  钱包-充值
  static String saveRecharge = '/app/user-recharge/saveRecharge';
  //  我的钱包-充值列表
  static String rechargeList = '/app/cave/recharge/list';
  //  我的钱包-可提现金额
  static String withdrawAmount = '/app/user-withdraw/withdrawAmount';
  //  我的钱包-提现
  static String saveWithdraw = '/app/user-withdraw/saveWithdraw';
  //  意见反馈
  static String inCaveFeedback = '/feedback/inCaveFeedback';

  //  我的会员列表
  static String myMember = '/app/user-member/myMember';
  //  开通会员
  static String openMember = '/app/user-member/openMember';
  //  可购买会员列表
  static String vipList = '/app/cave-vip/list';

  //  完成任务
  static String completeTask = '/app/user-task/completeTask';
  // 灵魂速配
  static String soulFriend = '/app/user-friend/soulFriend';
  // 灵魂交互列表
  static String soulPeoples = '/app/nearbyPeople/soulPeoples';
  // 灵魂速配/灵魂交互 绑定好友关系
  static String setFriendStart = '/app/user-friend/soulFriendStart';
}
