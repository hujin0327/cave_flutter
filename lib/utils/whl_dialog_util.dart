import 'package:cave_flutter/pages/whl_login/complete_user_info/selected_sex/selected_sex_logic.dart';
import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class WhlDialogUtil {
  ///提示信息弹框|输入框弹框
  static void showConfirmDialog(
    String title, {
    String description = "",
    String? cancelString,
    bool isShowCancel = true,
    String? sureString,
    Function? sureAction,
    Function? cancelAction,
    bool isInput = false,
    TextEditingController? textEditingController,
    String? placeHolder,
    TextInputType? keyboardType = TextInputType.text,
    Widget? customWidget,
    bool isDelete = false,
  }) {
    Get.dialog(Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(
        child: WillPopScope(
          onWillPop: () async {
            return false;
          },
          child: UIWrap(
            color: Colors.white,
            margin: EdgeInsets.symmetric(horizontal: 30.w),
            radius: 13.w,
            wrapAlignment: WrapAlignment.center,
            children: [
              if (title.isNotEmpty)
                UIText(
                  margin: EdgeInsets.only(top: 16.w),
                  text: title,
                  textAlign: TextAlign.center,
                  fontSize: 15.sp,
                  shrinkWrap: false,
                  textColor: kAppTextColor,
                  fontWeight: FontWeight.w500,
                ),
              customWidget ??
                  (isInput
                      ? UIContainer(
                          padding: EdgeInsets.symmetric(horizontal: 18.w),
                          margin: EdgeInsets.only(
                              left: 22.w, right: 22.w, top: 12.w),
                          color: kAppColor('#F7F7F7'),
                          radius: 12.w,
                          height: 44.w,
                          alignment: Alignment.center,
                          strokeWidth: 1.w,
                          strokeColor: Colors.black,
                          child: TextFormField(
                            cursorColor: kAppThemeColor,
                            //光标颜色
                            keyboardType: keyboardType,
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(12)
                            ],
                            controller: textEditingController,
                            maxLines: 1,
                            autofocus: false,
                            style: TextStyle(
                                fontSize: 14.sp,
                                color: kAppTextColor,
                                fontWeight: FontWeight.w500),
                            decoration: InputDecoration(
                                border: const OutlineInputBorder(
                                    borderSide: BorderSide.none),
                                hintText: placeHolder ?? '',
                                hintStyle: TextStyle(
                                    color: kAppColor('#AAAAAA'),
                                    fontWeight: FontWeight.normal,
                                    fontSize: 14.sp),
                                contentPadding: EdgeInsets.zero),
                            onChanged: (value) {
                              // action(value);
                            },
                          ),
                        )
                      : Padding(
                          padding: EdgeInsets.only(
                              left: 15.w, right: 15.w, top: 18.w, bottom: 5.w),
                          child: Text(
                            description,
                            style: TextStyle(
                              fontSize: 14.sp,
                              color: kAppTextColor,
                            ),
                          ),
                        )),
              UIRow(
                margin: EdgeInsets.all(18.w),
                children: [
                  if (isShowCancel)
                    Expanded(
                      child: UIText(
                        height: 44.w,
                        radius: 12.w,
                        alignment: Alignment.center,
                        color: kAppColor('#F7F7F7'),
                        text: cancelString ?? 'Cancel',
                        fontSize: 14.sp,
                        textColor: kAppTextColor,
                        fontWeight: FontWeight.w500,
                        onTap: () {
                          Get.back();
                          cancelAction?.call();
                        },
                      ),
                    ),
                  SizedBox(
                    width: 36.w,
                  ),
                  // if (userCenterLogic == null)
                  Expanded(
                      child: UIText(
                    text: sureString ?? 'Sure',
                    height: 44.w,
                    radius: 12.w,
                    textColor: isDelete ? kAppColor('#FF5140') : kAppThemeColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 14.sp,
                    color: isDelete
                        ? kAppColor('#FFF4F3')
                        : kAppThemeColor.withOpacity(0.1),
                    textAlign: TextAlign.center,
                    alignment: Alignment.center,
                    onTap: () {
                      Get.back();
                      sureAction?.call();
                    },
                  ))
                ],
              ),
            ],
          ),
        ),
      ),
    ));
  }

  static void showBottomSheet(BuildContext context, List data,
      {String title = '',
      callBack(int)?,
      Color? titleColor,
      double? titleFont,
      Color? itemColor,
      double? itemFont}) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return SafeArea(
            child: UIContainer(
              // color: const Color.fromRGBO(255, 255, 255, 0.3),
              decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(14),
                      topRight: Radius.circular(14))),
              clipBehavior: Clip.hardEdge,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  if (title.isNotEmpty)
                    Container(
                      decoration: bottomLineBoxDecoration,
                      alignment: Alignment.center,
                      child: UIText(
                        margin: EdgeInsets.symmetric(
                            horizontal: 16.w, vertical: 16.w),
                        text: title,
                        textAlign: TextAlign.center,
                        shrinkWrap: false,
                        fontSize: titleFont ?? 14,
                        textColor: titleColor ?? kAppSubTextColor,
                      ),
                    ),
                  //为了防止控件溢出
                  Flexible(
                      child: Container(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Flexible(
                            child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: data.length,
                          itemBuilder: (context, index) {
                            return Column(
                              children: <Widget>[
                                ListTile(
                                    onTap: () {
                                      Navigator.pop(context);
                                      if (callBack != null) {
                                        callBack!(index);
                                      }
                                    },
                                    title: Text(data[index],
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontSize: itemFont ?? 16,
                                            color: itemColor ??
                                                kAppSubTextColor))),
                                index == data.length - 1
                                    ? Container(
                                        color: kAppStrokeColor,
                                        height: 12,
                                      )
                                    : Divider(
                                        height: 1.w,
                                        color: kAppLineColor,
                                      ),
                              ],
                            );
                          },
                        )),
                      ],
                    ),
                  )),
                  GestureDetector(
                    child: Container(
                      height: 54,
                      width: double.infinity,
                      alignment: Alignment.center,
                      decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(0)),
                      ),
                      child: Text('取消',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 16, color: kAppSub3TextColor)),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  )
                ],
              ),
            ),
          );
        });
  }

  static void showSexModelBottomSheet(BuildContext context, List<SexModel> data,
      {String title = '',
        Function(SexModel model)? callBack,
        Color? titleColor,
        double? titleFont,
        Color? itemColor,
        double? itemFont}) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return SafeArea(
            child: UIContainer(
              // color: const Color.fromRGBO(255, 255, 255, 0.3),
              decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(14),
                      topRight: Radius.circular(14))),
              clipBehavior: Clip.hardEdge,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  if (title.isNotEmpty)
                    Container(
                      decoration: bottomLineBoxDecoration,
                      alignment: Alignment.center,
                      child: UIText(
                        margin: EdgeInsets.symmetric(
                            horizontal: 16.w, vertical: 16.w),
                        text: title,
                        textAlign: TextAlign.center,
                        shrinkWrap: false,
                        fontSize: titleFont ?? 14,
                        textColor: titleColor ?? kAppSubTextColor,
                      ),
                    ),
                  //为了防止控件溢出
                  Flexible(
                      child: Container(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Flexible(
                                child: ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: data.length,
                                  itemBuilder: (context, index) {
                                    return Column(
                                      children: <Widget>[
                                        ListTile(
                                            onTap: () {
                                              Navigator.pop(context);
                                              if (callBack != null) {
                                                callBack(data[index]);
                                              }
                                            },
                                            title: Text(data[index].name??"",
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontSize: itemFont ?? 16,
                                                    color: itemColor ??
                                                        kAppSubTextColor))),
                                        index == data.length - 1
                                            ? Container(
                                          color: kAppStrokeColor,
                                          height: 12,
                                        )
                                            : Divider(
                                          height: 1.w,
                                          color: kAppLineColor,
                                        ),
                                      ],
                                    );
                                  },
                                )),
                          ],
                        ),
                      )),
                  GestureDetector(
                    child: Container(
                      height: 54,
                      width: double.infinity,
                      alignment: Alignment.center,
                      decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(0)),
                      ),
                      child: Text('取消',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 16, color: kAppSub3TextColor)),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  )
                ],
              ),
            ),
          );
        });
  }
}
