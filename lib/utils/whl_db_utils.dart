import 'package:cave_flutter/db_manager/dao/whl_product_dao.dart';
import 'package:cave_flutter/db_manager/db/app_database.g.dart';
import 'package:cave_flutter/utils/whl_user_utils.dart';
import 'package:floor/floor.dart';
import '../db_manager/db/app_database.dart';

class WhlDBUtils {
  //单例
  factory WhlDBUtils() => _getInstance();

  static WhlDBUtils get instance => _getInstance();
  static WhlDBUtils? _instance;
  static String? databasesPath;

  WhlDBUtils._internal() {}

  static WhlDBUtils _getInstance() {
    _instance ??= WhlDBUtils._internal();
    return _instance!;
  }

  //定义数据库变量
  AppDatabase? _db;

  WhlProductDao get productDao => _db!.productDao;

  initDB() async {
    _db = await $FloorAppDatabase.databaseBuilder('app_database_${WhlUserUtils.getId()}.db_manager').addMigrations(
      [],
    ).build();
    print('数据库初始化完成');
  }

  //迁移策略,数据库版本1->2  (1.4.0)
  final migration1to2 = Migration(1, 2, (database) async {
    await database.execute('ALTER TABLE MessageBean ADD COLUMN isLimit TEXT');
    await database.execute('ALTER TABLE MessageBean ADD COLUMN type INTEGER');
    await database.update('MessageBean', {'isLimit': '0', 'type': 0});
  });
}
