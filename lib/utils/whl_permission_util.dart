import 'dart:io';
import 'package:cave_flutter/utils/whl_dialog_util.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';

class WhlPermissionUtil {
  static Future reqStoragePermission({Function? onSuccessAction}) async {
    PermissionStatus _permissionStatus;// = await Permission.photos.status;
    if (Platform.isAndroid){
      _permissionStatus = await Permission.storage.status;
    }else{
      _permissionStatus = await Permission.photos.status;
    }
    if (_permissionStatus == PermissionStatus.denied ||Platform.isAndroid){
      Map<Permission, PermissionStatus> statuses = await [
        Permission.photos,
        // Permission.camera,
        Permission.storage
      ].request();
      if (statuses[Permission.storage] != PermissionStatus.granted && Platform.isAndroid) {
        WhlDialogUtil.showConfirmDialog('Permission settings', description: 'The album permissions have been turned off. Do you want to go to Settings to open it?',
            sureAction: () async {
              bool isOpened = await openAppSettings();
            });
      }
      if (statuses[Permission.storage] == PermissionStatus.granted || statuses[Permission.photos] == PermissionStatus.granted){
        onSuccessAction?.call();
      }
    }else if (_permissionStatus == PermissionStatus.permanentlyDenied){
      WhlDialogUtil.showConfirmDialog('Permission settings', description: 'The album permissions have been turned off. Do you want to go to Settings to open it?',
          sureAction: () async {
            bool isOpened = await openAppSettings();
          });
    }else if (_permissionStatus == PermissionStatus.granted){
      onSuccessAction?.call();
    }
  }

  static reqCameraPermission ({Function? onSuccessAction}) async {
    PermissionStatus _permissionStatus = await Permission.camera.status;
    if (_permissionStatus == PermissionStatus.denied ||Platform.isAndroid){
      Map<Permission, PermissionStatus> statuses = await [
        Permission.camera,
      ].request();
      if (statuses[Permission.camera] != PermissionStatus.granted && Platform.isAndroid) {
        WhlDialogUtil.showConfirmDialog('Permission settings', description: 'The camera permissions have been turned off. Do you want to go to Settings to turn them on?',
            sureAction: () async {
              bool isOpened = await openAppSettings();
            });
      }
      if (statuses[Permission.camera] == PermissionStatus.granted){
        onSuccessAction?.call();
      }
    }else if (_permissionStatus == PermissionStatus.permanentlyDenied){
      WhlDialogUtil.showConfirmDialog('Permission settings', description: 'The camera permissions have been turned off. Do you want to go to Settings to turn them on?',
          sureAction: () async {
            bool isOpened = await openAppSettings();
          });
    }else if (_permissionStatus == PermissionStatus.granted){
      onSuccessAction?.call();
    }
  }

  static reqMicrophonePermission ({Function? onSuccessAction}) async {
    PermissionStatus _permissionStatus = await Permission.microphone.status;
    if (_permissionStatus == PermissionStatus.denied ||Platform.isAndroid){
      Map<Permission, PermissionStatus> statuses = await [
        Permission.microphone,
      ].request();
      if (statuses[Permission.microphone] != PermissionStatus.granted && Platform.isAndroid) {
        WhlDialogUtil.showConfirmDialog('Permission settings', description: 'The microphone permission is turned off. Do you want to go to Settings to turn it on?',
            sureAction: () async {
              bool isOpened = await openAppSettings();
            });
      }
      if (statuses[Permission.microphone] == PermissionStatus.granted) {
        onSuccessAction?.call();
      }
    }else if (_permissionStatus == PermissionStatus.permanentlyDenied){
      WhlDialogUtil.showConfirmDialog('Permission settings', description: 'The microphone permission is turned off. Do you want to go to Settings to turn it on?',
          sureAction: () async {
            bool isOpened = await openAppSettings();
          });
    }else if (_permissionStatus == PermissionStatus.granted){
      onSuccessAction?.call();
    }
  }

  static Future reqLocationWhenInUsePermission({Function? onSuccessAction}) async {
    PermissionStatus _permissionStatus;// = await Permission.photos.status;
    _permissionStatus = await Permission.locationWhenInUse.status;
    if (_permissionStatus == PermissionStatus.denied ||Platform.isAndroid){
      Map<Permission, PermissionStatus> statuses = await [
        Permission.locationWhenInUse,
      ].request();
      if (statuses[Permission.locationWhenInUse] != PermissionStatus.granted && Platform.isAndroid) {
        WhlDialogUtil.showConfirmDialog('权限设置', description: '需要打开定位权限，才能发送位置相关信息',
            sureAction: () async {
              bool isOpened = await openAppSettings();
            });
      }
      if (statuses[Permission.locationWhenInUse] == PermissionStatus.granted){
        onSuccessAction?.call();
      }
    }else if (_permissionStatus == PermissionStatus.permanentlyDenied){
      WhlDialogUtil.showConfirmDialog('权限设置', description: '需要打开定位权限，才能发送位置相关信息',
          sureAction: () async {
            bool isOpened = await openAppSettings();
          });
    }else if (_permissionStatus == PermissionStatus.granted){
      onSuccessAction?.call();
    }
  }
}