import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class MyToast {
  static show(String text) {
    BotToast.showText(text: text ?? '');
  }
}
