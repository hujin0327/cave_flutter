import 'dart:convert';

import 'package:cave_flutter/common/chat_manager.dart';
import 'package:cave_flutter/config/app_keyname_const.dart';
import 'package:cave_flutter/model/dynamic_draft_model.dart';
import 'package:cave_flutter/model/whl_user_info_model.dart';
import 'package:cave_flutter/network/whl_network_utils.dart';
import 'package:cave_flutter/utils/whl_md5_utils.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:sp_util/sp_util.dart';

import '../routes/whl_app_pages.dart';

class WhlUserUtils {
  ///是否登录
  static bool isLogin() {
    return WhlNetWorkUtils.getAccessToken().isNotEmpty;
  }

  static Future<bool> saveUserInfo(WhlUserInfoModel userInfo) async {
    return await SpUtil.putObject('userInfo', userInfo) ?? false;
  }

  static void removeUserModel() {
    SpUtil.remove('userInfo');
  }

  static void loginOut() {
    WhlUserUtils()
        .removeAccountWithAccountId(WhlUserUtils.getUserInfo().accountId ?? "");
    removeUserModel();
    ChatManager.getInstance().logoutIM();
    setUserInfoComplete(false);
    WhlNetWorkUtils.setAccessToken("");
    Get.offAllNamed(Routes.login);
  }

  static void switchAccount() {
    removeUserModel();
    ChatManager.getInstance().logoutIM();
  }

  static WhlUserInfoModel getUserInfo() {
    return SpUtil.getObj('userInfo', (v) => WhlUserInfoModel.fromJson(v)) ??
        WhlUserInfoModel();
  }

  static String getId() {
    return getUserInfo().id ?? '';
  }

  static String accountId() {
    return getUserInfo().accountId ?? '';
  }

  static String getNickName() {
    return getUserInfo().nickname ?? '';
  }

  static String getAvatar() {
    return getUserInfo().avatar ?? '';
  }

  static Future<bool> setAppConfig(Map appConfig) async {
    return await SpUtil.putString('appConfig', jsonEncode(appConfig)) ?? false;
  }

  static Map getAppConfig() {
    Map map = jsonDecode(SpUtil.getString('appConfig', defValue: '{}') ?? '{}');
    return map;
  }

  static Future setTranslatedText(String key, String translatedText) async {
    key = WhlMd5Util.generateMd5(key);
    Map map = getTranslatedMap();
    map[key] = translatedText;
    return await SpUtil.putString('translatedTextMap', jsonEncode(map)) ??
        false;
  }

  static Map getTranslatedMap() {
    Map map = jsonDecode(
        SpUtil.getString('translatedTextMap', defValue: '{}') ?? '{}');
    return map;
  }

  static String? getTranslatedText(String key) {
    key = WhlMd5Util.generateMd5(key);
    Map map = getTranslatedMap();
    return map[key];
  }

  static Future setIsAutoTranslate(bool isTranslate) async {
    return await SpUtil.putBool('isAutoTranslate${getId()}', isTranslate);
  }

  static bool getIsAutoTranslate() {
    return SpUtil.getBool('isAutoTranslate${getId()}', defValue: true) ?? true;
  }

  static Future setBlockList(List<String> blockList) async {
    return await SpUtil.putStringList('blockList${getId()}', blockList);
  }

  static List<String> getBlockList() {
    return SpUtil.getStringList('blockList${getId()}', defValue: []) ?? [];
  }

  static Future setUserInfoComplete(bool isComplete) async {
    return await SpUtil.putBool(APPKeyNameConst.infoComplete, isComplete);
  }

  static bool getUserInfoComplete() {
    return SpUtil.getBool(APPKeyNameConst.infoComplete) ?? false;
  }

  static Future saveDynamicDraftModel(DynamicDraftModel draftModel) async {
    return await SpUtil.putObject(APPKeyNameConst.dynamicDraft, draftModel);
  }

  static DynamicDraftModel? getDynamicDraftModel() {
    return SpUtil.getObj(
        APPKeyNameConst.dynamicDraft, (v) => DynamicDraftModel.fromJson(v));
  }

  static void removeDynamicDraftModel() {
    SpUtil.remove(APPKeyNameConst.dynamicDraft);
  }

  addAccountToLoc(WhlUserInfoModel model, String token) {
    Map oldData = getLocAccount() ?? {};
    Map userInfo = {
      "nickName": model.nickname,
      "token": token,
      "avatar": model.avatar,
      "accountId": model.accountId
    };
    oldData[model.accountId ?? ""] = userInfo;
    SpUtil.putObject(APPKeyNameConst.allLoginAccount, oldData);
  }

  getLocAccount() {
    return json
        .decode(SpUtil.getDynamic(APPKeyNameConst.allLoginAccount) ?? "{}");
  }

  removeAccountWithAccountId(String accountId) {
    Map oldData = getLocAccount();
    oldData.remove(accountId);
    SpUtil.putObject(APPKeyNameConst.allLoginAccount, oldData);
  }
}
