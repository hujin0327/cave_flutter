import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class WhlPickerUtil {
  static showPickerDateTime(
      BuildContext context, int type, PickerConfirmCallback confirmCallback,
      {String? title,
      PickerSelectedCallback? selectedCallback,
      DateTime? chooseDateTime,
      DateTime? maxDateTime,
      DateTime? minDateTime}) {
    Picker picker = Picker(
        height: 200.w,
        itemExtent: 40.w,
        backgroundColor: Colors.transparent,
        headerDecoration: const BoxDecoration(
            border:
                Border(bottom: BorderSide(color: Colors.black12, width: 0.5))),
        adapter: DateTimePickerAdapter(
          type: type,
          isNumberMonth: true,
          strAMPM: const ["上午", "下午"],
          yearSuffix: "年",
          monthSuffix: "月",
          daySuffix: "日",
          value: chooseDateTime ?? DateTime.now(),
          minValue: minDateTime,
          maxValue: maxDateTime,
          minuteInterval: 1,
          minHour: 0,
          maxHour: 23,
          // twoDigitYear: true,
        ),
        title: Text(title ?? '', style: TextStyle(fontSize: 15.sp)),
        textAlign: TextAlign.right,
        confirmText: '确定',
        cancelText: '取消',
        confirmTextStyle: TextStyle(
            color: kAppColor('#000000'),
            fontSize: 14.sp,
            fontWeight: FontWeight.bold),
        cancelTextStyle: TextStyle(
            color: kAppTextColor, fontSize: 14.sp, fontWeight: FontWeight.bold),
        textStyle: TextStyle(color: kAppColor('#666666'), fontSize: 14.sp),
        selectedTextStyle: TextStyle(
            color: kAppTextColor, fontSize: 16.sp, fontWeight: FontWeight.bold),
//        delimiter: [
//          PickerDelimiter(
//              column: 5,
//              child: Container(
//                width: 16.0,
//                alignment: Alignment.center,
//                child: Text(':', style: TextStyle(fontWeight: FontWeight.bold)),
//                color: Colors.white,
//              ))
//        ],
        onConfirm: confirmCallback,
        onSelect: selectedCallback);
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (context) {
        return Material(
          color: kAppColor('#FFF7F6'),
          borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(10), topRight: Radius.circular(10)),
          child: Container(
            padding: const EdgeInsets.only(top: 4),
            child: picker.makePicker(null, true),
          ),
        );
      },
    );
  }

  ///常用Picker
  static Widget commonPiker<T>(
      {String title = "",
      List? pickerData,
      List<PickerItem<T>>? data,
      PickerConfirmCallback? onConfirm,
      PickerSelectedCallback? onSelect,
      Color? selectedTextColor,
      Color? selectedBgColor,
      List<int>? selectedList,
      bool hideHeader = false}) {
    return Picker(
      adapter: PickerDataAdapter<T>(pickerData: pickerData, data: data),
      cancelText: 'Cancel',
      cancelTextStyle: TextStyle(color: kAppColor("#333333"), fontSize: 14.sp),
      title: UIText(
        text: title,
        textColor: kAppColor("#333333"),
        fontSize: 14.sp,
        alignment: Alignment.center,
        height: 44.w,
        fontWeight: FontWeight.w500,
      ),
      confirmText: 'Confirm',
      confirmTextStyle: TextStyle(color: kAppColor("#333333"), fontSize: 14.sp),
      onConfirm: (Picker picker, List<int> selected) async {
        onConfirm?.call(picker, selected);
      },
      onSelect: (Picker picker, int index, List<int> selected) async {
        onSelect?.call(picker, index, selected);
      },
      headerDecoration: const BoxDecoration(color: Colors.transparent),
      height: 35.w * data!.length + 50.w > 227.w
          ? 227.w
          : 35.w * data.length + 50.w,
      itemExtent: 35.w,
      backgroundColor: Colors.transparent,
      containerColor: Colors.transparent,
      textAlign: TextAlign.center,
      columnPadding: EdgeInsets.only(top: 10.w, bottom: 10.w),
      selectedTextStyle: TextStyle(
          fontSize: 16.sp,
          color: selectedTextColor ?? Colors.black,
          fontWeight: FontWeight.w500),
      selectionOverlay: UIContainer(
        radius: 4.w,
        margin: EdgeInsets.only(left: 20.w, right: 20.w),
        // width: 345.w,
        color: selectedBgColor ?? kAppColor("#333333").withOpacity(0.1),
      ),
      textStyle: TextStyle(fontSize: 16.sp, color: Colors.black),
      selecteds: selectedList,
    ).makePicker();
  }

  ///返回日期选择器
  static Widget renderDatePiker({
    String title = "",
    int type = PickerDateTimeType.kYMD,
    List data = const [],
    PickerConfirmCallback? onConfirm,
    PickerSelectedCallback? onSelect,
    List<int>? selectedList,
    bool hideHeader = false,
    DateTime? startDateTime,
    DateTime? endDateTime,
    DateTime? selectDateTime,
  }) {
    return Picker(
      hideHeader: hideHeader,
      adapter: DateTimePickerAdapter(
        type: type,
        value: selectDateTime ?? DateTime.now(),
        yearSuffix: "年",
        monthSuffix: "月",
        daySuffix: "日",
        isNumberMonth: true,
        minHour: 0,
        maxHour: 23,
        // minValue: startDateTime ?? DateTime(GlobalConfig.exportDateStartYear),
        // maxValue: endDateTime ?? DateTime(GlobalConfig.exportDateEndYear),
      ),
      cancelText: "取消",
      cancelTextStyle: TextStyle(color: kAppColor("#898989"), fontSize: 14.sp),
      title: UIText(
        text: title,
        textColor: kAppColor("#000000"),
        fontSize: 15.sp,
        alignment: Alignment.center,
        height: 44.w,
        fontWeight: FontWeight.bold,
      ),
      confirmText: "确定",
      confirmTextStyle: TextStyle(color: kAppColor("#FDD428"), fontSize: 14.sp),
      onConfirm: (Picker picker, List<int> selected) async {
        onConfirm?.call(picker, selected);
      },
      onSelect: (Picker picker, int index, List<int> selected) async {
        onSelect?.call(picker, index, selected);
      },
      headerDecoration: const BoxDecoration(color: Colors.transparent),
      height: 227.w,
      itemExtent: 46.w,
      backgroundColor: Colors.transparent,
      containerColor: Colors.transparent,
      selectedTextStyle: TextStyle(
          fontSize: 16.sp, color: Colors.black, fontWeight: FontWeight.bold),
      selectionOverlay: UIColumn(
        children: [
          Divider(color: kAppColor("#F0F0F0"), height: 1.w),
          SizedBox(height: 44.w),
          Divider(color: kAppColor("#F0F0F0"), height: 1.w),
        ],
      ),
      textStyle: TextStyle(fontSize: 16.sp, color: Colors.black),
      selecteds: selectedList,
    ).makePicker();
  }

  static showChooseSinglePicker(
      BuildContext context, PickerConfirmCallback confirmCallback,
      {PickerSelectedCallback? selectedCallback,
      required List dataList,
      required List<int> selectedIndex,
      required String title}) {
    Picker picker = Picker(
        height: 200.w,
        itemExtent: 33.w,
        backgroundColor: Colors.transparent,
        selecteds: selectedIndex,
        headerDecoration: BoxDecoration(
            border:
                Border(bottom: BorderSide(color: Colors.black12, width: 0.5))),
        adapter: PickerDataAdapter<String>(pickerData: dataList),
        title: Container(
          margin: EdgeInsets.only(left: 15.w),
          child: Text(
            title == null ? '' : '请选择$title',
            style: TextStyle(
                fontSize: 18.sp,
                fontWeight: FontWeight.bold,
                color: kAppColor('#333333')),
            textAlign: TextAlign.left,
          ),
        ),
        textAlign: TextAlign.right,
        confirmText: 'X',
        cancelText: '',
        confirmTextStyle: TextStyle(
          color: kAppColor('#626166'),
          fontSize: 20.sp,
        ),
        cancelTextStyle: TextStyle(
          color: kAppColor('#626166'),
          fontSize: 17.5.sp,
        ),
        textStyle: TextStyle(color: kAppColor('#626166'), fontSize: 20.sp),
        selectedTextStyle: TextStyle(
            color: kAppColor('#333333'),
            fontSize: 19.sp,
            fontWeight: FontWeight.bold),
        onSelect: selectedCallback);
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        builder: (context) {
          return Material(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10), topRight: Radius.circular(10)),
              child: Container(
                height: 327.w,
                // padding: const EdgeInsets.only(top: 4),
                child: Column(
                  children: [
                    picker.makePicker(null, true),
                    UIText(
                      text: "确定",
                      onTap: () {
                        confirmCallback(picker, picker.selecteds);
                      },
                      fontSize: 16.sp,
                      alignment: Alignment.center,
                      textColor: Colors.white,
                      color: kAppThemeColor,
                      height: 44.w,
                      width: double.maxFinite,
                      radius: 9.w,
                      gradientStartColor: kAppLeftColor,
                      gradientEndColor: kAppRightColor,
                      margin:
                          EdgeInsets.only(top: 15.w, left: 30.w, right: 30.w),
                    )
                  ],
                ),
              ));
        });
  }
}
