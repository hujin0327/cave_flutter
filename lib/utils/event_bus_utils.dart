import 'package:event_bus/event_bus.dart';

class EventBusUtils {
  static EventBusUtils? _instance;

  EventBusUtils._internal();

  final EventBus eventBus = EventBus();

  static EventBusUtils getInstance() {
    if (_instance == null) {
      _instance = EventBusUtils._internal();
    }
    return _instance!;
  }

}