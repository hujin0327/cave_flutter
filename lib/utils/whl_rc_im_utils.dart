// import 'dart:convert';
// import 'dart:isolate';
// import 'dart:ui';
//
// import 'package:cave_flutter/network/whl_api.dart';
// import 'package:cave_flutter/network/whl_network_config.dart';
// import 'package:cave_flutter/network/whl_network_utils.dart';
// import 'package:cave_flutter/pages/whl_chat/whl_chat_logic.dart';
// import 'package:cave_flutter/pages/whl_chat/whl_chat_room/whl_chat_room_logic.dart';
// import 'package:cave_flutter/utils/whl_md5_utils.dart';
// import 'package:cave_flutter/utils/whl_record_voice_utils.dart';
// import 'package:cave_flutter/utils/whl_toast_utils.dart';
// import 'package:cave_flutter/utils/whl_user_utils.dart';
// import 'package:get/get.dart';
// import 'package:rongcloud_im_wrapper_plugin/rongcloud_im_wrapper_plugin.dart';
//
// class WhlRCIMUtils {
//   //单例
//   factory WhlRCIMUtils() => _getInstance();
//
//   static WhlRCIMUtils get instance => _getInstance();
//   static WhlRCIMUtils? _instance;
//
//   WhlRCIMUtils._internal() {}
//
//   static WhlRCIMUtils _getInstance() {
//     _instance ??= WhlRCIMUtils._internal();
//     return _instance!;
//   }
//
//   static late RCIMIWEngine engine;
//   static RCIMIWSendMessageCallback? sendMessageCallback;
//
//   static onInit(String appKey) async {
//     RCIMIWEngineOptions options = RCIMIWEngineOptions.create();
//     options.logLevel = RCIMIWLogLevel.verbose;
//     engine = await RCIMIWEngine.create(appKey, options);
//     print(engine);
//   }
//
//   static onConnect(String token) async {
//     await engine.connect(token, 30,
//         callback: RCIMIWConnectCallback(
//             onConnected: (code, userId) {
//               print('onConnected $code $userId');
//             },
//             onDatabaseOpened: (code) {}));
//     engine.onMessageReceived = (message, left, hasPackage, offline) {
//       if (message == null) {
//         return;
//       }
//       onReceiveNewMessage(message);
//     };
//     sendMessageCallback = RCIMIWSendMessageCallback(
//       onMessageSent: (int? code, RCIMIWMessage? message) {
//         if (message == null) {
//           return;
//         }
//         onReceiveNewMessage(message);
//       },
//     );
//   }
//
//   static onReceiveNewMessage(RCIMIWMessage message) {
//     WhlChatLogic logic = Get.find();
//     RCIMIWConversation? conversation = logic.conversationList.firstWhereOrNull((element) => element.targetId == message?.targetId);
//     if (conversation != null) {
//       conversation.lastMessage = message;
//       conversation.unreadCount = (conversation.unreadCount ?? 0) + 1;
//       logic.conversationList.removeWhere((element) => element.targetId == message?.targetId);
//       logic.conversationList.insert(0, conversation);
//       logic.update();
//     } else {
//       logic.onGetConversationList();
//     }
//     if (Get.isRegistered<WhlChatRoomLogic>()) {
//       WhlChatRoomLogic logic = Get.find();
//       // logic.onGetMessageList(oldMessage: message);
//       logic.messageList.insert(0, message);
//       logic.update();
//       if (message.messageType == RCIMIWMessageType.text && message.senderUserId != WhlUserUtils.getId()) {
//         //收到对方消息后 自动翻译
//         logic.onTranslateForChatRoom(message);
//       }
//     }
//   }
//
//   static onGetConversations({IRCIMIWGetConversationsCallback? callback}) {
//     engine.getConversations([RCIMIWConversationType.private], null, 0, 50, callback: callback);
//   }
//
//   static onGetMessageList(RCIMIWConversation conversation, int sentTime, {IRCIMIWGetMessagesCallback? callback}) {
//     engine.getMessages(conversation.conversationType ?? RCIMIWConversationType.private, conversation.targetId ?? '', conversation.channelId, sentTime,
//         RCIMIWTimeOrder.before, RCIMIWMessageOperationPolicy.localRemote, 20,
//         callback: callback);
//   }
//
//   static onSendTextMessage(String targetId, String text, {RCIMIWSendMessageCallback? callback}) async {
//     RCIMIWTextMessage? message = await engine.createTextMessage(RCIMIWConversationType.private, targetId, null, text);
//     _sendMessage(message, callback: callback);
//   }
//
//   static onSendVoiceMessage(RCIMIWConversation conversation, RecordVoiceModel model) async {
//     RCIMIWVoiceMessage? message = await engine.createVoiceMessage(
//         conversation.conversationType ?? RCIMIWConversationType.private, conversation.targetId ?? '', null, model.path, model.time);
//     _sendMessage(message);
//   }
//
//   static onSendImageMessage(RCIMIWConversation conversation, String imagePath) async {
//     RCIMIWImageMessage? message = await engine.createImageMessage(
//         conversation.conversationType ?? RCIMIWConversationType.private, conversation.targetId ?? '', null, imagePath);
//     _sendMessage(message);
//   }
//
//   static onSendFileMessage(RCIMIWConversation conversation, String filePath) async {
//     RCIMIWFileMessage? message =
//         await engine.createFileMessage(conversation.conversationType ?? RCIMIWConversationType.private, conversation.targetId ?? '', null, filePath);
//     _sendMessage(message);
//   }
//
//   static _sendMessage(RCIMIWMessage? localMessage, {RCIMIWSendMessageCallback? callback}) async {
//     if (localMessage == null) {
//       return;
//     }
//     localMessage.senderUserId = WhlUserUtils.getId();
//     localMessage.sentTime = DateTime.now().millisecondsSinceEpoch;
//     localMessage.userInfo = RCIMIWUserInfo.create(userId: WhlUserUtils.getId(), name: WhlUserUtils.getNickName(), portrait: WhlUserUtils.getAvatar());
//     localMessage.sentStatus = RCIMIWSentStatus.sending;
//     if (Get.isRegistered<WhlChatRoomLogic>()) {
//       WhlChatRoomLogic logic = Get.find();
//       // logic.onGetMessageList(oldMessage: message);
//       logic.messageList.insert(0, localMessage);
//       logic.update();
//     }
//     // await Future.delayed(Duration(seconds: 1));
//     engine.sendMessage(localMessage, callback: RCIMIWSendMessageCallback(
//       onMessageSent: (int? code, RCIMIWMessage? message) {
//         if (message == null) {
//           return;
//         }
//         localMessage.sentStatus = RCIMIWSentStatus.sent;
//         if (Get.isRegistered<WhlChatRoomLogic>()) {
//           WhlChatRoomLogic logic = Get.find();
//           logic.update();
//         }
//         if (code != 0) {
//           MyToast.show('Send message failed');
//         }
//         onSendMessageSuccess(message);
//       },
//     ));
//   }
//
//   static onSendMessageSuccess(RCIMIWMessage message) {
//     WhlChatLogic logic = Get.find();
//     RCIMIWConversation? conversation = logic.conversationList.firstWhereOrNull((element) => element.targetId == message?.targetId);
//     if (conversation != null) {
//       conversation.lastMessage = message;
//       conversation.unreadCount = (conversation.unreadCount ?? 0) + 1;
//       logic.conversationList.removeWhere((element) => element.targetId == message?.targetId);
//       logic.conversationList.insert(0, conversation);
//       logic.update();
//     } else {
//       logic.onGetConversationList();
//     }
//   }
// }
