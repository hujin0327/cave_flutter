import 'package:cave_flutter/network/whl_api.dart';
import 'package:cave_flutter/pages/whl_mine/task/model/task_model.dart';
import 'package:get/get.dart';

class TaskUtils {
  static TaskUtils? _instance;
  static List<TaskModel> allTasks = [];
  TaskUtils._internal();

  static TaskUtils getInstance() {
    if (_instance == null) {
      _instance = TaskUtils._internal();
      WhlApi.getTask.get({}).then((responseData) {
        if (responseData.isSuccess()) {
          allTasks = [
            ...List<TaskModel>.from(responseData.data['dailyTasks']
                .map((c) => TaskModel.fromJson(c))
                .toList()),
            ...List<TaskModel>.from(responseData.data['randomTasks']
                .map((c) => TaskModel.fromJson(c))
                .toList()),
            ...List<TaskModel>.from(responseData.data['fixedTasks']
                .map((c) => TaskModel.fromJson(c))
                .toList()),
          ];
        }
      });
    }
    return _instance!;
  }

/**
 * 
 * 
  1	资料完成100%
  2	完成人格测试 
  3	完成实名认证
  4	绑定灵魂契约
  5	发布一条文字动态
  6	发布一条语音动态
  7	发布一条视频动态
  8	获得一位粉丝
  9	获得一位好友
  10	参加一次心动电台
  11	参加一次密聊
  12	逛一逛Cave
  13	逛一逛Cave公益
  14	逛一逛勋章墙
  15	逛一逛礼物墙
  16	逛一逛排行榜
  17	逛一逛我的装扮
  18	逛一逛小黑屋
  19	逛一逛别人的契约空间
  20	点赞5条动态
  21	评论3条动态
  22	转发一条动态到第三方平台
  23	完成一次灵魂交互
  24	发起一次聊天
  25	送出一次礼物  
 */
  static complateTask(int taskId, {int? type}) {
    //  这么做是防止后台的type发生变化导致type传错误
    TaskModel? task = allTasks.firstWhereOrNull((e) => e.taskId == taskId);

    WhlApi.completeTask.post({
      "taskId": taskId,
      "type": task?.type ?? type,
    }, hideMsg: true).then((value) {});
  }
}
