import 'dart:convert';

import 'package:crypto/crypto.dart';
class WhlMd5Util {

  /// md5加密
  static String generateMd5(String data) {
    var content = const Utf8Encoder().convert(data);
    var digest = md5.convert(content);
    return digest.toString();
  }

  /// sha1加密
  static String generateSHA1(String data){
    var bytes = utf8.encode(data);
    var content = sha1.convert(bytes);
    return "$content";
  }
}

class Sha256Utils {
  static const key = 'Jkcz3Epv~1yhSOpBpqJ^Y_@GP@FL!';
  static encryptKeyWithHMAC (String data){
    var hmacSha256 = Hmac(sha256, utf8.encode(key));
    var digest = hmacSha256.convert(utf8.encode(data));
    return digest.toString();
  }
}