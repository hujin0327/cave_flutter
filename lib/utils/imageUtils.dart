import 'dart:io';
import 'dart:typed_data';
import 'dart:async';
import 'dart:ui' as ui;
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:image_editor/image_editor.dart';

import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:image_size_getter/file_input.dart';
import 'package:image_size_getter/image_size_getter.dart';
import 'package:photo_manager/photo_manager.dart';

//customSize： Unit MB
Future<List<int>> compressImageToCustomSize(String filePath,
    {double? customSize = 0.7}) async {
  try {
    List<int> firstImageBytes = await File(filePath).readAsBytes();
    int quality = 95;
    var imageBytes = firstImageBytes;
    double allSize = imageBytes.length / (1024 * 1024);
    if (allSize >= 5) {
      customSize = allSize / 4.0;
    }
    while (imageBytes.length > 1024 * 1024 * customSize!) {
      imageBytes = await FlutterImageCompress.compressWithList(
        Uint8List.fromList(firstImageBytes),
        quality: quality,
        rotate: 0,
        // format: CompressFormat.png,
        // minHeight: 0,
        // minWidth: 0,
      );
      print("result image size ${imageBytes!.length / 1024 / 1024} MB");
      quality -= 5;
    }
    print("return image size ${imageBytes.length / 1024 / 1024} MB");
    return imageBytes;
  } catch (e) {
    print(e);
    return [];
  }
}

//customSize： Unit MB
Future<List<int>> compressImageByteToCustomSize(Uint8List imageBytes,
    {double? customSize = 0.7}) async {
  try {
    List<int> firstImageBytes = imageBytes;
    int quality = 95;
    double allSize = imageBytes.length / (1024 * 1024);
    if (allSize >= 5) {
      customSize = allSize / 4.0;
    }
    while (imageBytes.length > 1024 * 1024 * customSize!) {
      imageBytes = await FlutterImageCompress.compressWithList(
        Uint8List.fromList(firstImageBytes),
        quality: quality,
        rotate: 0,
        // format: CompressFormat.png,
        // minHeight: 0,
        // minWidth: 0,
      );
      print("result image size ${imageBytes!.length / 1024 / 1024} MB");
      quality -= 5;
    }
    print("return image size ${imageBytes.length / 1024 / 1024} MB");
    return imageBytes;
  } catch (e) {
    print(e);
    return [];
  }
}

//制造字符串水印图片
Future<ui.Image> _makeStrMarkImage(String str, double fontSize,
    {double fontScale = 1}) async {
  final picRecorder = ui.PictureRecorder();
  final paragraphBuilder = ui.ParagraphBuilder(
    ui.ParagraphStyle(
      textAlign: TextAlign.right,
      fontSize: fontSize,
    ),
  );
  paragraphBuilder.addText(str);
  final paragraph = paragraphBuilder.build()
    ..layout(
      ui.ParagraphConstraints(
        width: fontSize * 16.0 * fontScale,
      ),
    );
  final lineMetrics = paragraph.computeLineMetrics();
  double width = 0;
  lineMetrics.forEach((element) {
    if (element.width > width) {
      width = element.width;
    }
  });
  double height = paragraph.height * fontScale;
  final cvs = Canvas(
    picRecorder,
    Rect.fromLTRB(
      0,
      0,
      width,
      height,
    ),
  );
  cvs.drawParagraph(paragraph, Offset(0, 0));
  final pic = picRecorder.endRecording();
  final waterMark = await pic.toImage(
    width.toInt(),
    height.toInt(),
  );
  return waterMark;
}

Future<File?> imageAddWaterMark({
  required AssetEntity assetEntity,
  String? text,
  //  Color color,
  //  double fontSize,
  //  Offset offset,
}) async {
  // 加载图片
  File file = (await assetEntity.file)!;
  final fileSize = assetEntity.width == 0
      ? ImageSizeGetter.getSize(FileInput(file))
      : Size(
          assetEntity.width,
          assetEntity.height,
        );
  ImageEditorOption option = ImageEditorOption();
  AddTextOption textOption = AddTextOption();
  String waterStr = text ?? '【Cave】';
  double height = fileSize.height.toDouble();
  double width = fileSize.width.toDouble();
  double screentWidth = 375;
  double fontScale = width < screentWidth ? width / screentWidth : 1;
  ui.Image fontImg = await _makeStrMarkImage(
    waterStr,
    24,
    fontScale: fontScale,
  );
  if (fileSize.width > fileSize.height) {
    height = fileSize.width.toDouble();
    width = fileSize.height.toDouble();
  }
  Offset offset =
      Offset((width - fontImg.width - 40), (height - fontImg.height));
  EditorText editorText = EditorText(
    text: waterStr,
    offset: offset,
    fontSizePx: (24 * fontScale).floor(),
    textColor: Colors.grey.withOpacity(0.9),
  );
  textOption.addText(editorText);
  option.addOption(textOption);
  return ImageEditor.editFileImageAndGetFile(
    file: file,
    imageEditorOption: option,
  ).then((value) {
    var path = value!.path;
    var lastSeparator = path.lastIndexOf(Platform.pathSeparator);
    var newPath = assetEntity.title != null
        ? path.substring(0, lastSeparator + 1) + assetEntity.title!
        : path;
    return value.copy(newPath).then((newFile) {
      value.deleteSync();
      return newFile;
    });
  });
}
