import 'package:flutter/material.dart';

import 'whl_notification.dart';
enum GestureType {
  translate, // 平移
  scale, // 缩放
}
/// 拖拽的widget
class WhlDragGestureDetector extends StatelessWidget {
  WhlDragGestureDetector({
    Key? key,
    required this.child,
    this.onPanStart,
    this.onPanUpdate,
    this.onPanEnd,
    this.onPanCancel,
    this.onTap,
    this.onDoubleTap,
    this.onScaleStart,
    this.onScaleEnd,
    this.onScaleUpdate,
  }) : super(key: key);
  final Widget child;
  final Function? onPanStart;
  final Function? onPanUpdate;
  final Function? onPanEnd;
  final Function? onPanCancel;
  final Function? onTap;
  final Function? onDoubleTap;
  final Function? onScaleStart;
  final Function? onScaleEnd;
  final Function? onScaleUpdate;
  GestureType _gestureType = GestureType.translate;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      // onPanStart: (DragStartDetails details) {
      //   DragOnPanStartNotification().dispatch(context);
      //   onPanStart?.call();
      // },
      // onPanUpdate: (DragUpdateDetails details) {
      //   DragOnPanUpdateNotification(details).dispatch(context);
      //   onPanUpdate?.call();
      // },
      // onPanEnd: (DragEndDetails details) {
      //   DragOnPanEndNotification().dispatch(context);
      //   onPanEnd?.call();
      // },
      // onPanCancel: (){
      //   const DragOnPanCancelNotification().dispatch(context);
      //   onPanCancel?.call();
      // },
      onTap: () {
        const DragOnTapNotification().dispatch(context);
        onTap?.call();
      },
      onDoubleTap: () {
        onDoubleTap?.call();
      },
      onScaleStart: (ScaleStartDetails details) {
        if (details.pointerCount > 1) {  // 双指
          _gestureType = GestureType.scale;
          onScaleStart?.call(details);
        } else { // 单指
          _gestureType = GestureType.translate;
          DragOnPanStartNotification().dispatch(context);
          onPanStart?.call();
        }
      },
      onScaleEnd: (ScaleEndDetails details) {
        if (details.pointerCount > 1) {
          onScaleEnd?.call(details);
        }else{
          DragOnPanEndNotification().dispatch(context);
          onPanEnd?.call();
        }
      },
      onScaleUpdate: (ScaleUpdateDetails details) {
        if (details.pointerCount > 1) {
          onScaleUpdate?.call(details);
        }else{
          DragOnPanUpdateNotification(details).dispatch(context);
          onPanUpdate?.call();
        }
      },
      child: child,

      /// 只有[child]会被响应拖拽事件
    );
  }
}
