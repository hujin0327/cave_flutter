import 'package:flutter/material.dart';

import 'whl_drag_gesture_animation.dart';
import 'whl_drag_gesture_detector.dart';

/// 自定义路由动画
class WhlDragGesturePageRoute<T> extends PageRoute<T> with MaterialRouteTransitionMixin {
  WhlDragGesturePageRoute({
    required this.builder,
    RouteSettings? settings,
    this.maintainState = true,
  }) : super(settings: settings, fullscreenDialog: true);

  final WidgetBuilder builder;

  @override
  final bool maintainState;


  AnimationController? _pushAnimationController;
  AnimationController? _popAnimationController;

  @override
  Widget buildContent(BuildContext context) {
    return WhlDragGestureAnimation( // DragGestureAnimation：计算和处理动画的widget
      pushAnimationController: _pushAnimationController!,
      popAnimationController: _popAnimationController!,
      child: builder(context),
    );
  }

  // 重写动画
  @override
  AnimationController createAnimationController() {
    if (_pushAnimationController == null) {
      _pushAnimationController = AnimationController(
        vsync: navigator!.overlay!,
        duration: Duration(milliseconds: 300),
        reverseDuration: transitionDuration,
      );
      _popAnimationController = AnimationController(
        vsync: navigator!.overlay!,
        duration: Duration(milliseconds: 300),
        reverseDuration: transitionDuration,
        value: 1,
      );
    }
    return _pushAnimationController!;
  }

  // 背景，添加淡入淡出的动画
  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    return Stack(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: Colors.black.withOpacity(0.5),
        ),
        FadeTransition(
          opacity: isActive ? _pushAnimationController! : _popAnimationController!,// 判断该路由是否在导航上，来使用不同的控制器
          child: Container(
            color: Colors.black,
          ),
        ),
        WhlDragGestureDetector(child: child),
      ],
    );
  }

  String? get title => null;
}