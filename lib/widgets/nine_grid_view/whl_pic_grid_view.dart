import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'whl_crop_image.dart';

const int maxPicGridViewCount = 9;

/// Grid view to show picture
class WhlPicGridView extends StatelessWidget {
  const WhlPicGridView({
    required this.imagesList, required this.heroTags,
  });

  final List<String> imagesList;
  final List<String> heroTags;
  @override
  Widget build(BuildContext context) {
    if (imagesList.isEmpty) {
      return Container();
    }

    Widget widget = LayoutBuilder(builder: (BuildContext c, BoxConstraints b) {
      const double margin = 11;
      final double size = b.maxWidth;
      int rowCount = 3;
      //single image
      if (imagesList.length == 1) {
        return SizedBox(
          width: 185.w,
          height: 185.w,
          child: WhlCropImage(
            index: 0,
            knowImageSize: true,
            imagesList: imagesList,
            heroTags: heroTags,
          ),
        );
      }

      double totalWidth = size;
      if (imagesList.length == 4) {
        totalWidth = size - 75;
        rowCount = 2;
      }
      return SizedBox(
        // margin: const EdgeInsets.all(margin),
        width: totalWidth,
        child: GridView.builder(
          shrinkWrap: true,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: rowCount, crossAxisSpacing: 6, mainAxisSpacing: 6),
          itemBuilder: (BuildContext s, int index) {
            return WhlCropImage(
              index: index,
              imagesList: imagesList,
              knowImageSize: true,
              heroTags: heroTags,
            );
          },
          physics: const NeverScrollableScrollPhysics(),
          itemCount: imagesList.length.clamp(1, maxPicGridViewCount),
          padding: const EdgeInsets.all(0.0),
        ),
      );
    });
    // if (margin != null) {
    //   widget = Padding(
    //     padding: margin,
    //     child: widget,
    //   );
    // }
    widget = Align(
      child: widget,
      alignment: Alignment.centerLeft,
    );
    return widget;
  }
}
