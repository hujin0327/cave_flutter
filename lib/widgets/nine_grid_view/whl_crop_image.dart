import 'dart:math';
import 'dart:ui' as ui show Image;
import 'package:flutter/material.dart';

import '../brick/brick.dart';
import 'drag_widgets/whl_drag_gesture_page_route.dart';
import 'whl_image_browser_page.dart';

class WhlCropImage extends StatelessWidget {
  const WhlCropImage({
    required this.index,
    required this.imagesList,
    this.knowImageSize, required this.heroTags,
  });

  final List<String> imagesList;
  final bool? knowImageSize;
  final int index;
  final List<String> heroTags;
  @override
  Widget build(BuildContext context) {
    if (imagesList.isEmpty) {
      return Container();
    }

    const double num300 = 150;
    const double num400 = 200;
    double height = num300;
    double width = num400;
    final String imageUrl = imagesList[index];
    if (knowImageSize!) {
      final double n = height / width;
      if (n >= 4 / 3) {
        width = num300;
        height = num400;
      } else if (4 / 3 > n && n > 3 / 4) {
        final double maxValue = max(width, height);
        height = num400 * height / maxValue;
        width = num400 * width / maxValue;
      } else if (n <= 3 / 4) {
        width = num400;
        height = num300;
      }
    }
    Widget widget = Hero(
      tag: heroTags[index],
      child: UIImage(
        httpImage: imageUrl,
        width: width,
        height: height,
        fit: BoxFit.cover,
      ),
    );
    if (index == 8 && imagesList.length > 9) {
      widget = Stack(children: <Widget>[
        widget,
        Container(
          color: Colors.grey.withOpacity(0.6),
          alignment: Alignment.center,
          child: Text(
            '+${imagesList.length - 9}',
            style: const TextStyle(fontSize: 18.0, color: Colors.white),
          ),
        )
      ]);
    }
    return GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            WhlDragGesturePageRoute(
              builder: (context) {
                return WhlImageBrowserPage(
                  tags: heroTags,
                  messageList: const [],
                  index: index,
                );
              },
            ),
          );
        },
        child: widget);
  }
}
