
import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../style/whl_style.dart';
import 'brick/widget/text_widget.dart';

class MyRefresh extends StatelessWidget {
  final EasyRefreshController controller;
  final Widget? child;
  final VoidCallback? onRefresh;
  final VoidCallback? onLoad;
  final ERChildBuilder? childBuilder;

  const MyRefresh(
      {super.key,
        required this.controller,
        this.child,
        this.onRefresh,
        this.onLoad,
        this.childBuilder});

  @override
  Widget build(BuildContext context) {
    if (childBuilder != null) {
      return EasyRefresh.builder(
        controller: controller,
        // noMoreRefresh: onRefresh != null,
        // noMoreLoad: onLoad != null,
        header: MaterialHeader(
            backgroundColor: kAppWhiteColor,
            color: kAppMainColor
        ),
        footer: CupertinoFooter(
            emptyWidget: Center(child: UIText(text: "已经到底了!",textColor: kAppTwoTextColor,fontSize: 13.sp,),)
        ),
        onRefresh: onRefresh,
        onLoad: onLoad,
        childBuilder: childBuilder,
      );
    }else{
      return EasyRefresh(
        controller: controller,
        // noMoreRefresh: onRefresh != null,
        // noMoreLoad: onLoad != null,
        header: MaterialHeader(
            backgroundColor: kAppWhiteColor,
            color: kAppMainColor
        ),
        footer: CupertinoFooter(
            emptyWidget: Center(child: UIText(text: "已经到底了!",textColor: kAppTwoTextColor,fontSize: 13.sp,),)
        ),
        onRefresh: onRefresh,
        onLoad: onLoad,
        child: child,
      );
    }

  }


}
