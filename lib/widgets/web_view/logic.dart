import 'dart:convert';

// import 'package:fluwx/fluwx.dart';
import 'package:get/get.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WhlWebViewLogic extends GetxController {
  String? webUrl;
  String? title;
  bool isLoading = false;
  RxDouble webProgress = 0.0.obs;
  RxString rxTitle = "".obs;
  Map<String, dynamic>? webParams;
  late final WebViewController webController;


  @override
  void onInit() {
    Map argument = Get.arguments;
    webUrl = argument['webUrl'] ?? '';
    title = argument['title'] ?? '';
    webParams = argument['params'] ?? {};
    rxTitle.value = title ?? "";
    final WebViewController controller = WebViewController();
    controller
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            webProgress.value = progress / 100;
          },
          onPageStarted: (String url) {},
          onPageFinished: (String url) async {
            webProgress.value = 1;
            // String title = await webController.getTitle() ?? "";
            // rxTitle.value = title;
          },
          onWebResourceError: (WebResourceError error) {},
          onNavigationRequest: (NavigationRequest request) {
            return NavigationDecision.navigate;
          },
        ),
      )
      ..loadRequest(Uri.parse(webUrl ?? ""));
    // controller.addJavaScriptChannel('openWeChat', onMessageReceived: (message){
    //   Map data = jsonDecode(message.message);
    //   print('message: $message');
    //   fluwx.open(target: MiniProgram(username: 'gh_8d6a4d2d2c3e', path: data['path'], miniProgramType: WXMiniProgramType.release));
    // });
    webController = controller;
    super.onInit();
  }
}
