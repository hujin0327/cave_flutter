import 'dart:math';

import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/whl_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'logic.dart';

class WhlWebViewPage extends StatelessWidget {
  WhlWebViewPage({super.key});

  final WhlWebViewLogic logic = Get.put(WhlWebViewLogic(), tag: Random().nextInt(9999).toString());

  @override
  Widget build(BuildContext context) {
    progressBar(double progress, BuildContext context) {
      return LinearProgressIndicator(
        minHeight: 2.0.w,
        backgroundColor: Colors.white.withOpacity(0),
        value: progress == 1.0 ? 0 : progress,
        valueColor: const AlwaysStoppedAnimation<Color>(Colors.greenAccent),
      );
    }

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      child: Scaffold(
          backgroundColor: Colors.white,
          body: SafeArea(
            child: UIColumn(
              children: [
                Obx(() {
                  return WhlAppBar(
                    centerTitle: logic.rxTitle.value,
                    backgroundColor: Colors.white,
                    bottom: Obx(() => PreferredSize(
                          preferredSize: const Size.fromHeight(3.0),
                          child: progressBar(logic.webProgress.value, context),
                        )),
                  );
                }),
                Expanded(
                  child: UIContainer(
                    margin: const EdgeInsets.only(bottom: 0),
                    child: Builder(builder: (BuildContext context) {
                      return WebViewWidget(controller: logic.webController);
                    }),
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
