import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../style/whl_style.dart';
import 'brick/brick.dart';

class TestView extends StatelessWidget {
  const TestView({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Stack(
        alignment: Alignment.center,
        children: [
          Opacity(
            opacity: 0.3,
            child: UIContainer(
                decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      kAppColor('#000000').withOpacity(0.3),
                      kAppColor('#00C69B').withOpacity(0.3),
                      kAppColor('#02DBFF').withOpacity(0.3),
                      kAppColor('#00C69B').withOpacity(0.3),
                      kAppColor('#000000').withOpacity(0.3),
                    ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
                    borderRadius: BorderRadius.circular(400)),
                width: 350.w,
                height: 350.w,
                child: BlackHoleAnimation()),
          ),
          for (int i = 0; i < 18; i++)
            UIContainer(
              // margin: EdgeInsets.only(bottom: i*5.0),
              radius: 1000.w,
              width: 30.0 * i,
              height: 30.0 * i,
              strokeColor: Colors.white.withOpacity(0.01 * (18 - i)),
              strokeWidth: 1,
            ),
          UIContainer(
            width: 50,
            height: 50,
            color: Colors.black,
            radius: 40,
          ),
          BlackHoleAnimation1(),
        ],
      ),
    );
  }
}

class BlackHoleAnimation extends StatefulWidget {
  @override
  _BlackHoleAnimationState createState() => _BlackHoleAnimationState();
}

class _BlackHoleAnimationState extends State<BlackHoleAnimation> with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: Duration(seconds: 10),
      vsync: this,
    )..repeat(reverse: true);
    _animation = CurvedAnimation(parent: _controller, curve: Curves.easeInCirc);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // return CustomPaint(
    //   painter: BlackHolePainter(_animation.value),
    //   child: Container(),
    // );
    return AnimatedBuilder(
      animation: _animation,
      builder: (context, child) {
        return CustomPaint(
          painter: BlackHolePainter(_animation.value),
          child: Container(),
        );
      },
    );
  }
}

class BlackHolePainter extends CustomPainter {
  final double animationValue;

  BlackHolePainter(this.animationValue);

  @override
  void paint(Canvas canvas, Size size) {
    final center = Offset(size.width / 2, size.height / 2);
    final radius = min(size.width, size.height) / 2;

    final paint = Paint()
      ..color = Colors.white.withOpacity(0.1)
      ..style = PaintingStyle.fill;

    final numRings = 2;
    final numSpokes = 30;

    for (int i = 0; i < numRings; i++) {
      final ringRadius = radius * (i + 1) / numRings;
      final strokeWidth = ringRadius * 0.1;
      paint.strokeWidth = 1;

      for (int j = 0; j < numSpokes; j++) {
        final angle = (pi * 2 / numSpokes) * j + animationValue * 2 * pi;
        final startPoint = Offset(center.dx + ringRadius * cos(angle), center.dy + ringRadius * sin(angle));
        final endPoint = Offset(center.dx, center.dy);

        canvas.drawLine(startPoint, endPoint, paint);
      }
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

class BlackHoleAnimation1 extends StatefulWidget {
  @override
  _BlackHoleAnimation1State createState() => _BlackHoleAnimation1State();
}

class _BlackHoleAnimation1State extends State<BlackHoleAnimation1> with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _scaleAnimation;
  late Animation<double> _opacityAnimation;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: Duration(seconds: 2),
      vsync: this,
    )..repeat(reverse: true);
    _scaleAnimation = Tween<double>(begin: 1, end: 2).animate(_controller);
    _opacityAnimation = Tween<double>(begin: 1, end: 0).animate(_controller);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _controller,
      builder: (context, child) {
        return Transform.scale(
          scale: _scaleAnimation.value,
          child: Opacity(
            opacity: _opacityAnimation.value,
            child: child,
          ),
        );
      },
      child: Container(
        width: 350.w,
        height: 350.w,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          gradient: RadialGradient(
            colors: [Colors.black, Colors.transparent],
            stops: [0, 1],
          ),
        ),
      ),
    );
  }
}
