import 'package:flutter/material.dart';
import 'dart:math';


class RippleAnimatedWidget extends StatefulWidget {

  final Color color;
  final AnimationController controller;

  const RippleAnimatedWidget({super.key, this.color = Colors.white, required this.controller});

  @override
  State<StatefulWidget> createState() => _RippleAnimatedWidgetState();
}

class _RippleAnimatedWidgetState extends State<RippleAnimatedWidget>
    with SingleTickerProviderStateMixin {

  late Animation<double> animation;

  @override
  void initState() {
    super.initState();
    //增加一个缓出的效果
    animation = CurvedAnimation(
        parent: widget.controller,
        curve: const Interval(0.5, 1, curve: Curves.easeOut));
  }


  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: widget.controller,
      builder: (context, child) {
        return CustomPaint(
          painter: RipplePainter(widget.controller.value, color: widget.color, count: 4),
        );
      },
    );
  }
}

class RipplePainter extends CustomPainter {

  final int count;
  final double progress;
  final Color color;
  Paint painter = Paint()..style = PaintingStyle.stroke..strokeWidth = 2;
  RipplePainter(this.progress, {this.color = Colors.white, this.count = 1});

  @override
  void paint(Canvas canvas, Size size) {
    double width = min(size.width / 2, size.height / 2);
    for (int i = count; i >= 0; i--) {
      //改变透明度
      final double opacity = (1.0 - ((i + progress) / (count + 1)));
      final Color tmpColor = color.withOpacity(opacity);
      painter.color = tmpColor;
      //宽度递增
      double radius = width * ((i + progress) / (count + 1));
      canvas.drawCircle(
          Offset(size.width / 2, size.height / 2), radius, painter);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
