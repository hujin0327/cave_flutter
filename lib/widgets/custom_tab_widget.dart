import 'package:cave_flutter/style/whl_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomTabWidget extends StatefulWidget {
  final List<dynamic> tabs;
  int selectedIndex;
  final ValueChanged<int> onTabSelected;

  CustomTabWidget({
    required this.tabs,
    required this.selectedIndex,
    required this.onTabSelected,
  });

  @override
  _CustomTabWidgetState createState() => _CustomTabWidgetState();
}

class _CustomTabWidgetState extends State<CustomTabWidget> {
  final ScrollController _scrollController = ScrollController();

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      controller: _scrollController,
      child: Row(
        children: List.generate(
          widget.tabs.length,
              (index) {
            return GestureDetector(
              onTap: () {
                setState(() {
                  widget.selectedIndex = index;
                });
                widget.onTabSelected(index);
                scrollToCenter(index);
              },
              child: Container(
                margin: EdgeInsets.only(right: index == widget.tabs.length - 1 ? 12.w : 0.w, left: 12.w),
                padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 4.h),
                // decoration: BoxDecoration(
                //   borderRadius: BorderRadius.circular(16.w),
                //   color: widget.selectedIndex == index
                //       ? kAppColor("#09849F")
                //       : kAppColorOpacity("#09849F", 0.15),
                // ),
                child: Text(
                  widget.tabs[index],
                  style: TextStyle(
                    color: widget.selectedIndex == index
                        ? kAppBlackColor
                        : kAppSub3TextColor,
                    fontSize: 15.sp,
                    fontWeight: widget.selectedIndex == index?FontWeight.bold:null,
                  ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  void scrollToCenter(int index) {
    final double screenWidth = MediaQuery.of(context).size.width;
    final double itemWidth = _calculateItemWidth(index);

    double scrollToOffset =
        (itemWidth * index) - (screenWidth / 2) + (itemWidth / 2);
    if (scrollToOffset < 0) {
      scrollToOffset = 0;
    }
    if (scrollToOffset > _scrollController.position.maxScrollExtent || index == widget.tabs.length - 1) {
      scrollToOffset = _scrollController.position.maxScrollExtent;
    }

    _scrollController.animateTo(
      scrollToOffset,
      duration: Duration(milliseconds: 300),
      curve: Curves.easeInOut,
    );
  }

  double _calculateItemWidth(int index) {
    final String text = widget.tabs[index];
    final TextPainter textPainter = TextPainter(
      text: TextSpan(text: text, style: TextStyle(fontSize: 16.sp)),
      maxLines: 1,
      textDirection: TextDirection.ltr,
    )..layout();

    return textPainter.width + 32.w;
  }
}
