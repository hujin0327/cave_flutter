import 'dart:io';

import 'package:cave_flutter/style/whl_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/// 自定义AppBar
class WhlAppBar extends StatelessWidget implements PreferredSizeWidget {
  WhlAppBar(
      {Key? key,
      this.backgroundColor,
      this.title = "",
      this.centerTitle = "",
      this.centerWidget,
      this.actionName = "",
      this.actionName2 = "",
      this.actionName3 = '',
      this.backImg = "icon_back_black.png",
      this.leftImg = "",
      this.onPressed,
      this.onPressed2,
      this.onPressed3,
      this.isBack = true,
      this.isSearch = false,
      this.isShowBottomLine,
      this.rightWidget,
      this.isEdit = false,
      this.textEditingController,
      this.onValueChangeAction,
      this.onClickBackAction,
      this.onClickLeftAction,
      this.hintString = '搜索',
      this.bottom,
      this.isStatusBarDark,
      this.onClickTitleAction})
      : super(key: key);

  final Color? backgroundColor;
  final bool? isStatusBarDark;
  final String title;
  final String centerTitle;
  final Widget? centerWidget;
  final String backImg, leftImg;
  final String actionName, actionName2, actionName3;
  final VoidCallback? onPressed, onPressed2, onPressed3;
  final Function? onValueChangeAction;
  final Function? onClickBackAction;
  final Function? onClickLeftAction;
  final Function()? onClickTitleAction;
  final bool isBack;
  final bool isSearch;
  final Widget? rightWidget;
  bool? isShowBottomLine;
  final bool isEdit;
  final TextEditingController? textEditingController;
  final String hintString;
  final Widget? bottom;

  @override
  Widget build(BuildContext context) {
    Color backgroundColorTemp =
        backgroundColor ?? Theme.of(context).scaffoldBackgroundColor;
    SystemUiOverlayStyle overlayStyle = SystemUiOverlayStyle.light;
    bool? isStatusBarDarkTemp = isStatusBarDark;
    if (null == isStatusBarDarkTemp) {
      overlayStyle =
          ThemeData.estimateBrightnessForColor(backgroundColorTemp) ==
                  Brightness.dark
              ? SystemUiOverlayStyle.light
              : SystemUiOverlayStyle.dark;
    } else {
      overlayStyle = isStatusBarDarkTemp
          ? SystemUiOverlayStyle.dark
          : SystemUiOverlayStyle.light;
    }
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: overlayStyle,
      child: Material(
        color: backgroundColorTemp,
        child: SafeArea(
          child: bottom != null
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    buildAppBar(context, overlayStyle),
                    bottom!,
                  ],
                )
              : buildAppBar(context, overlayStyle),
        ),
      ),
    );
  }

  Container buildAppBar(
      BuildContext context, SystemUiOverlayStyle overlayStyle) {
    isShowBottomLine ??= true;
    return Container(
      decoration: BoxDecoration(
//              gradient: LinearGradient(
//                colors: [MyColors.themeColor, ColorUtil.fromHex('#FF8F8F')],
//                begin: Alignment.topCenter,
//                end: Alignment.bottomCenter,
//              ),
          border: Border(
              bottom: BorderSide(
                  width: 0.5,
                  color: isShowBottomLine == true
                      ? kAppColor('#EEEEEE')
                      : Colors.transparent))),
      child: Stack(
        alignment: Alignment.centerLeft,
        children: <Widget>[
          Semantics(
            namesRoute: true,
            header: true,
            child: GestureDetector(
              onTap: onClickTitleAction,
              child: Container(
                alignment: centerTitle.isEmpty
                    ? Alignment.centerLeft
                    : Alignment.center,
                width: double.infinity,
                child: Container(
                  margin: centerWidget != null?EdgeInsets.only(left: isBack==true?48.w:15.w, right: 15.w):const EdgeInsets.symmetric(horizontal: 48.0),
                  child: centerWidget ??Text(title.isEmpty ? centerTitle : title,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontSize: 16.sp,
                          color: overlayStyle == SystemUiOverlayStyle.light
                              ? Colors.white
                              : kAppTextColor,
                          fontWeight: FontWeight.bold)),
                ),
              ),
            ),
          ),
          isBack
              ? IconButton(
                  onPressed: () {
                    FocusScope.of(context).unfocus();
                    if (onClickBackAction != null) {
                      onClickBackAction!();
                    } else {
                      Navigator.maybePop(context);
                    }
                  },
                  tooltip: '返回',
                  padding: const EdgeInsets.all(12.0),
                  // icon: const Icon(
                  //   Icons.arrow_back_ios,
                  //   size: 25,
                  // ),
                  icon: Image.asset(
                    "assets/images/$backImg",
                    color: overlayStyle == SystemUiOverlayStyle.light
                        ? Colors.white
                        : kAppTextColor,
                    width: 20.w,
                    height: 20.w,
                  ),
                )
              : const SizedBox(height: 49),
          leftImg != ''
              ? IconButton(
                  onPressed: () {
                    FocusScope.of(context).unfocus();
                    if (onClickLeftAction != null) {
                      onClickLeftAction!();
                    }
                  },
                  tooltip: '',
                  padding: const EdgeInsets.all(12.0),
                  icon: Image.asset(
                    leftImg,
                    color: overlayStyle == SystemUiOverlayStyle.light
                        ? Colors.white
                        : kAppTextColor,
                    width: 21,
                    height: 20,
                  ),
                )
              : const SizedBox(),
          Positioned(
            right: 0.0,
            child: Theme(
              data: Theme.of(context).copyWith(
                  buttonTheme: const ButtonThemeData(
                padding: EdgeInsets.symmetric(horizontal: 16.0),
                minWidth: 60.0,
              )),
              child: actionName.isEmpty
                  ? rightWidget ?? Container()
                  : InkWell(
                      highlightColor: Colors.transparent,
                      onTap: onPressed,
                      child: Text(
                        actionName,
                        key: const Key('actionName'),
                        style: TextStyle(
                            fontSize: 14.sp,
                            color: overlayStyle == SystemUiOverlayStyle.light
                                ? Colors.white
                                : kAppTextColor,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
            ),
          ),
          if (actionName2.isNotEmpty)
            Positioned(
              right: 47,
              child: Theme(
                data: Theme.of(context).copyWith(
                    buttonTheme: const ButtonThemeData(
                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                  minWidth: 60.0,
                )),
                child: actionName2.isEmpty
                    ? rightWidget ?? Container()
                    : InkWell(
                        highlightColor: Colors.transparent,
                        onTap: onPressed2,
                        child: Text(
                          actionName2,
                          key: const Key('actionName2'),
                          style: TextStyle(
                              fontSize: 14.sp,
                              color: overlayStyle == SystemUiOverlayStyle.light
                                  ? Colors.white
                                  : kAppTextColor,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
              ),
            ),
          if (actionName3.isNotEmpty)
            Positioned(
              right: 12.w,
              child: Theme(
                data: Theme.of(context).copyWith(
                    buttonTheme: const ButtonThemeData(
//                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
//                      minWidth: 60.0,
                        )),
                child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: onPressed3, // 空白地方也可以点击
                  child: Container(
                    padding: EdgeInsets.all(8.w),
                    child: Text(
                      actionName3,
                      key: const Key('actionName3'),
                      style: TextStyle(
                          fontSize: 14.sp,
                          color: overlayStyle == SystemUiOverlayStyle.light
                              ? Colors.white
                              : kAppTextColor,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(Platform.isAndroid
      ? 56
      : bottom != null
          ? 52
          : 44);
}
