import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../whl_app.dart';
import '../main.dart';

class WhlWhiteAppBar extends StatelessWidget {
  const WhlWhiteAppBar(this.title,
      {Key? key, this.color, this.hasBackBtn = true, this.actions, this.onTapBack, this.backgroundColor, this.systemOverlayStyle})
      : super(key: key);
  final String title;
  final Color? color;
  final Color? backgroundColor;
  final bool hasBackBtn;
  final List<Widget>? actions;
  final Function()? onTapBack;
  final SystemUiOverlayStyle? systemOverlayStyle;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: backgroundColor ?? Colors.transparent,
      elevation: 0,
      centerTitle: true,
      title: Text(title ?? '',
          style: TextStyle(
            color: color ?? Colors.white,
            fontSize: 16.sp,
            fontWeight: FontWeight.bold,
          )),
      automaticallyImplyLeading: true,
      leading: hasBackBtn
          ? UIImage(
              margin: EdgeInsets.symmetric(horizontal: 15.w),
              assetImage: 'icon_back_black.png',
              width: 20.w,
              imageColor: color ?? Colors.white,
              onTap: onTapBack ??
                  () {
                    hideKeyboard(context);
                    if (Navigator.canPop(context)) {
                      Navigator.pop(context);
                    }
                  })
          : null,
      actions: actions,
      systemOverlayStyle: systemOverlayStyle ?? SystemUiOverlayStyle.dark,
    );
  }
}
