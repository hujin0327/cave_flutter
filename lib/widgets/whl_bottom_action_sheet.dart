import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../style/whl_style.dart';
import 'brick/widget/basic_widget.dart';

class WhlBottomActionSheet {
  static show(List data, {String title = '标题', callBack(int)?}) {
    Get.bottomSheet(UIContainer(
      padding: EdgeInsets.only(bottom: ScreenUtil().bottomBarHeight),
      color: Colors.white,
      topRightRadius: 30.w,
      topLeftRadius: 30.w,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          //为了防止控件溢出
          Flexible(
              child: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Flexible(
                        child: ListView.builder(
                          padding: EdgeInsets.zero,
                          shrinkWrap: true,
                          itemCount: data.length,
                          physics: const NeverScrollableScrollPhysics(),
                          itemBuilder: (context, index) {
                            return Column(
                              children: <Widget>[
                                ListTile(
                                    onTap: () {
                                      Navigator.pop(context);
                                      if (callBack != null) {
                                        callBack!(index);
                                      }
                                    },
                                    title: Text(data[index], textAlign: TextAlign.center, style: TextStyle(fontSize: 14.sp, color: kAppTextColor))),
                                index == data.length - 1
                                    ? Container()
                                    : const Divider(
                                  height: 1,
                                  color: Color(0xFFF0F0F0),
                                ),
                              ],
                            );
                          },
                        )),
                  ],
                ),
              )),
          Container(
            height: 9,
            color: kAppBcgColor,
          ),
          GestureDetector(
            child: Container(
              height: 54,
              width: double.infinity,
              alignment: Alignment.center,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(0)),
              ),
              child:
              Text('取消', textAlign: TextAlign.center, style: TextStyle(fontSize: 16.sp, color: kAppTextColor)),
            ),
            onTap: () {
              Get.back();
            },
          )
        ],
      ),
    ));
  }

  static showDelete(BuildContext context, List data, {String title = '标题', callBack(int)?}) {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        context: context,
        builder: (BuildContext context) {
          return SafeArea(
            child: Container(
              color: Colors.transparent,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  //为了防止控件溢出
                  Flexible(
                      child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Flexible(
                          child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: data.length,
                        itemBuilder: (context, index) {
                          return Column(
                            children: <Widget>[
                              UIContainer(
                                margin: EdgeInsets.only(left: 25.w, right: 25.w, top: 7.w),
                                height: 44.w,
                                width: double.infinity,
                                alignment: Alignment.center,
                                color: Colors.white,
                                radius: 12.w,
                                child: Text(data[index],
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontSize: 14.sp, color: kAppColor('#FF5140'), fontWeight: FontWeight.w500)),
                                onTap: () {
                                  Navigator.pop(context);
                                  if (callBack != null) {
                                    callBack(index);
                                  }
                                },
                              ),
                            ],
                          );
                        },
                      )),
                    ],
                  )),
                  SizedBox(
                    height: 7.w,
                  ),
                  GestureDetector(
                    child: UIContainer(
                      margin: EdgeInsets.only(left: 25.w, right: 25.w, bottom: 25.w),
                      height: 44.w,
                      width: double.infinity,
                      alignment: Alignment.center,
                      color: Colors.white,
                      radius: 12.w,
                      child: Text('取消',
                          textAlign: TextAlign.center, style: TextStyle(fontSize: 14.sp, color: kAppColor('#333333'), fontWeight: FontWeight.w500)),
                    ),
                    onTap: () {
                      //点击取消 弹层消失
                      Navigator.pop(context);
                    },
                  )
                ],
              ),
            ),
          );
        });
  }
}
