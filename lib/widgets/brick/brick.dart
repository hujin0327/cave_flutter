library brick;

export '../brick/widget/basic_widget.dart';
export '../brick/widget/image_widget.dart';
export '../brick/widget/text_widget.dart';
class WhlBrickKit {
  WhlBrickKit._internal();

  factory WhlBrickKit() => _instance;

  static final WhlBrickKit _instance = WhlBrickKit._internal();

  String? _assetPackage;
  String? _assetImagePath;

  void init({String? assetPackage, String? assetImagePath}) {
    _assetPackage = assetPackage;
    _assetImagePath = assetImagePath;
  }

  String? getAssetPackage() => _assetPackage;

  String? getAssetImagePath() => _assetImagePath;
}
