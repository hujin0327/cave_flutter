import 'package:flutter/material.dart';

import 'image_widget.dart';

///点击
class UIClick extends StatelessWidget {
  final Widget? child;

  ///GestureDetector
  final GestureTapDownCallback? onTapDown;
  final GestureTapUpCallback? onTapUp;
  final GestureTapCallback? onTap;
  final GestureLongPressCallback? onLongPress;
  final GestureLongPressEndCallback? onLongPressEnd;
  final GestureLongPressMoveUpdateCallback? onLongPressMoveUpdate;
  final GestureTapCancelCallback? onTapCancel;

  const UIClick({
    super.key,
    this.onTapDown,
    this.onTapUp,
    this.onTap,
    this.onTapCancel,
    this.onLongPress,
    this.child,
    this.onLongPressEnd,
    this.onLongPressMoveUpdate,
  }) : super();

  Widget buildRealChild({List<Widget>? supportChildList}) {
    List<Widget> supportChildListTemp = supportChildList ?? [];
    if (supportChildListTemp.length > 1) throw UnsupportedError("仅支持1个child");
    return supportChildListTemp.isEmpty ? (child ?? const UIEmptyWidget()) : supportChildListTemp.first;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: onTapDown,
      onTapUp: onTapUp,
      onTap: onTap,
      onLongPress: onLongPress,
      onLongPressEnd: onLongPressEnd,
      onLongPressMoveUpdate: onLongPressMoveUpdate,
      onTapCancel: onTapCancel,
      child: buildRealChild(),
    );
  }
}

///容器
class UIContainer extends UIClick {
  ///快捷属性
  final double radius;
  final double? topLeftRadius;
  final double? topRightRadius;
  final double? bottomRightRadius;
  final double? bottomLeftRadius;
  final Color solidColor;
  final Color strokeColor;
  final double? strokeWidth;
  final Color? gradientStartColor;
  final Color? gradientEndColor;
  final AlignmentGeometry gradientBegin;
  final AlignmentGeometry gradientEnd;
  final UIImage? backgroundDrawable;

  ///Container
  final AlignmentGeometry? alignment;
  final EdgeInsetsGeometry? padding;
  final Color? color;
  final Decoration? decoration;
  final Decoration? foregroundDecoration;
  final double? width;
  final double? height;
  final BoxConstraints? constraints;
  final EdgeInsetsGeometry? margin;
  final Matrix4? transform;
  final AlignmentGeometry? transformAlignment;
  final Clip clipBehavior;
  final List<BoxShadow>? boxShadow;

  const UIContainer({
    super.key,
    //快捷属性
    this.radius = 0,
    this.topLeftRadius,
    this.topRightRadius,
    this.bottomRightRadius,
    this.bottomLeftRadius,
    this.solidColor = Colors.transparent,
    this.strokeColor = Colors.transparent,
    this.strokeWidth,
    this.gradientStartColor,
    this.gradientEndColor,
    this.gradientBegin = Alignment.centerLeft,
    this.gradientEnd = Alignment.centerRight,
    this.backgroundDrawable,
    //Container
    this.margin,
    this.padding,
    this.color,
    this.decoration,
    this.width,
    this.height,
    this.constraints,
    this.alignment,
    this.foregroundDecoration,
    this.transform,
    this.transformAlignment,
    super.child,
    this.clipBehavior = Clip.none,
    this.boxShadow,
    //GestureDetector
    super.onTapDown,
    super.onTapUp,
    super.onTap,
    super.onTapCancel,
    super.onLongPress,
    super.onLongPressEnd,
    super.onLongPressMoveUpdate,
  }) : super();

  @override
  Widget buildRealChild({List<Widget>? supportChildList}) {
    List<Widget> supportChildListTemp = supportChildList ?? [];
    if (supportChildListTemp.length > 1) throw UnsupportedError("仅支持1个child");
    Widget supportChildTemp = supportChildListTemp.isEmpty ? (child ?? const UIEmptyWidget()) : supportChildListTemp.first;
    Widget realChild = Container(
      alignment: alignment,
      padding: padding,
      decoration: buildDecoration(),
      foregroundDecoration: foregroundDecoration,
      width: width,
      height: height,
      constraints: constraints,
      margin: margin,
      transform: transform,
      transformAlignment: transformAlignment,
      clipBehavior: clipBehavior,
      child: (null == backgroundDrawable)
          ? supportChildTemp
          : Stack(
              alignment: Alignment.center,
              children: [
                backgroundDrawable ?? const UIEmptyWidget(),
                supportChildTemp,
              ],
            ),
    );
    return super.buildRealChild(supportChildList: [realChild]);
  }

  ///创建Decoration逻辑
  Decoration buildDecoration() {
    return decoration ??
        BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(topLeftRadius ?? radius),
              topRight: Radius.circular(topRightRadius ?? radius),
              bottomRight: Radius.circular(bottomRightRadius ?? radius),
              bottomLeft: Radius.circular(bottomLeftRadius ?? radius),
            ),
            color: (null == color) ? (null != gradientStartColor ? null : solidColor) : color,
            gradient: (null == gradientStartColor)
                ? null
                : LinearGradient(
                    colors: [gradientStartColor ?? solidColor, gradientEndColor ?? solidColor],
                    begin: gradientBegin,
                    end: gradientEnd,
                  ),
            border: (null == strokeWidth) ? null : Border.all(color: strokeColor, width: strokeWidth ?? 0),
            boxShadow: boxShadow);
  }
}

///弹性
class UIFlex extends UIContainer {
  ///Flex
  final MainAxisAlignment mainAxisAlignment;
  final MainAxisSize mainAxisSize;
  final CrossAxisAlignment crossAxisAlignment;
  final TextDirection? textDirection;
  final VerticalDirection verticalDirection;
  final TextBaseline? textBaseline;
  final List<Widget> children;
  final Axis direction;

  const UIFlex({
    super.key,
    //Flex
    required this.direction,
    this.mainAxisAlignment = MainAxisAlignment.start,
    this.mainAxisSize = MainAxisSize.max,
    this.crossAxisAlignment = CrossAxisAlignment.center,
    this.textDirection,
    this.verticalDirection = VerticalDirection.down,
    this.textBaseline,
    this.children = const <Widget>[],
    //快捷属性
    super.radius = 0,
    super.topLeftRadius,
    super.topRightRadius,
    super.bottomRightRadius,
    super.bottomLeftRadius,
    super.solidColor = Colors.transparent,
    super.strokeColor = Colors.transparent,
    super.strokeWidth,
    super.gradientStartColor,
    super.gradientEndColor,
    super.gradientBegin = Alignment.centerLeft,
    super.gradientEnd = Alignment.centerRight,
    //Container
    super.margin,
    super.padding,
    super.color,
    super.decoration,
    super.width,
    super.height,
    super.constraints,
    super.alignment,
    super.foregroundDecoration,
    super.transform,
    super.transformAlignment,
    super.clipBehavior,
    //GestureDetector
    super.onTapDown,
    super.onTapUp,
    super.onTap,
    super.onTapCancel,
    super.onLongPress,
    super.boxShadow
  });

  @override
  Widget buildRealChild({List<Widget>? supportChildList}) {
    List<Widget> supportChildListTemp = supportChildList ?? [];
    supportChildListTemp = supportChildListTemp.isEmpty ? children : supportChildListTemp;
    Widget realChild = Flex(
      direction: direction,
      mainAxisAlignment: mainAxisAlignment,
      mainAxisSize: mainAxisSize,
      crossAxisAlignment: crossAxisAlignment,
      textDirection: textDirection,
      verticalDirection: verticalDirection,
      textBaseline: textBaseline,
      clipBehavior: Clip.none,
      children: children,
    );
    return super.buildRealChild(supportChildList: [realChild]);
  }
}

///行
class UIRow extends UIFlex {
  const UIRow({
    super.key,
    //Flex
    super.mainAxisAlignment = MainAxisAlignment.start,
    super.mainAxisSize = MainAxisSize.max,
    super.crossAxisAlignment = CrossAxisAlignment.center,
    super.textDirection,
    super.verticalDirection = VerticalDirection.down,
    super.textBaseline,
    super.children = const <Widget>[],
    //快捷属性
    super.radius = 0,
    super.topLeftRadius,
    super.topRightRadius,
    super.bottomRightRadius,
    super.bottomLeftRadius,
    super.solidColor = Colors.transparent,
    super.strokeColor = Colors.transparent,
    super.strokeWidth,
    super.gradientStartColor,
    super.gradientEndColor,
    super.gradientBegin = Alignment.centerLeft,
    super.gradientEnd = Alignment.centerRight,
    //Container
    super.margin,
    super.padding,
    super.color,
    super.decoration,
    super.width,
    super.height,
    super.constraints,
    super.alignment,
    super.foregroundDecoration,
    super.transform,
    super.transformAlignment,
    super.clipBehavior,
    //GestureDetector
    super.onTapDown,
    super.onTapUp,
    super.onTap,
    super.onTapCancel,
    super.onLongPress,
    super.boxShadow
  }) : super(direction: Axis.horizontal);
}

///列
class UIColumn extends UIFlex {
  const UIColumn({
    super.key,
    //Flex
    super.mainAxisAlignment = MainAxisAlignment.start,
    super.mainAxisSize = MainAxisSize.max,
    super.crossAxisAlignment = CrossAxisAlignment.center,
    super.textDirection,
    super.verticalDirection = VerticalDirection.down,
    super.textBaseline,
    super.children = const <Widget>[],
    //快捷属性
    super.radius = 0,
    super.topLeftRadius,
    super.topRightRadius,
    super.bottomRightRadius,
    super.bottomLeftRadius,
    super.solidColor = Colors.transparent,
    super.strokeColor = Colors.transparent,
    super.strokeWidth,
    super.gradientStartColor,
    super.gradientEndColor,
    super.gradientBegin = Alignment.centerLeft,
    super.gradientEnd = Alignment.centerRight,
    //Container
    super.margin,
    super.padding,
    super.color,
    super.decoration,
    super.width,
    super.height,
    super.constraints,
    super.alignment,
    super.foregroundDecoration,
    super.transform,
    super.transformAlignment,
    super.clipBehavior,
    //GestureDetector
    super.onTapDown,
    super.onTapUp,
    super.onTap,
    super.onTapCancel,
    super.onLongPress,
    super.boxShadow
  }) : super(direction: Axis.vertical);
}

///帧布局
class UIStack extends UIContainer {
  ///Stack
  final TextDirection? textDirection;
  final StackFit? fit;
  final List<Widget> children;

  const UIStack(
      {
      //Stack
      Key? key,
      super.alignment,
      this.textDirection,
      this.fit,
      super.clipBehavior = Clip.hardEdge,
      this.children = const <Widget>[],
      //快捷属性
      super.radius = 0,
      super.topLeftRadius,
      super.topRightRadius,
      super.bottomRightRadius,
      super.bottomLeftRadius,
      super.solidColor = Colors.transparent,
      super.strokeColor = Colors.transparent,
      super.strokeWidth,
      super.gradientStartColor,
      super.gradientEndColor,
      super.gradientBegin = Alignment.centerLeft,
      super.gradientEnd = Alignment.centerRight,
      //Container
      super.margin,
      super.padding,
      super.color,
      super.decoration,
      super.constraints,
      super.transform,
      //GestureDetector
      super.onTapDown,
      super.onTapUp,
      super.onTap,
      super.onTapCancel,
      super.onLongPress,
      super.onLongPressEnd,
      super.onLongPressMoveUpdate})
      : super(key: key);

  @override
  Widget buildRealChild({List<Widget>? supportChildList}) {
    List<Widget> supportChildListTemp = supportChildList ?? [];
    supportChildListTemp = supportChildListTemp.isEmpty ? children : supportChildListTemp;
    Widget realWidget = Stack(
      alignment: alignment ?? AlignmentDirectional.topStart,
      textDirection: textDirection,
      fit: fit ?? StackFit.loose,
      clipBehavior: clipBehavior,
      children: supportChildListTemp,
    );
    return super.buildRealChild(supportChildList: [realWidget]);
  }
}

///包裹内容
class UIWrap extends UIContainer {
  final Axis direction;
  final WrapAlignment wrapAlignment;
  final double spacing;
  final WrapAlignment runAlignment;
  final double runSpacing;
  final WrapCrossAlignment crossAxisAlignment;
  final TextDirection? textDirection;
  final VerticalDirection verticalDirection;
  final List<Widget> children;

  const UIWrap({
    super.key,
    this.direction = Axis.horizontal,
    this.wrapAlignment = WrapAlignment.start,
    this.spacing = 0.0,
    this.runAlignment = WrapAlignment.start,
    this.runSpacing = 0.0,
    this.crossAxisAlignment = WrapCrossAlignment.start,
    this.textDirection,
    this.verticalDirection = VerticalDirection.down,
    super.clipBehavior = Clip.none,
    this.children = const <Widget>[],
    //快捷属性
    super.radius = 0,
    super.topLeftRadius,
    super.topRightRadius,
    super.bottomRightRadius,
    super.bottomLeftRadius,
    super.solidColor = Colors.transparent,
    super.strokeColor = Colors.transparent,
    super.strokeWidth,
    super.gradientStartColor,
    super.gradientEndColor,
    super.gradientBegin = Alignment.centerLeft,
    super.gradientEnd = Alignment.centerRight,
    //Container
    super.margin,
    super.padding,
    super.color,
    super.decoration,
    super.constraints,
    super.transform,
    //GestureDetector
    super.onTapDown,
    super.onTapUp,
    super.onTap,
    super.onTapCancel,
    super.onLongPress,
  });

  @override
  Widget buildRealChild({List<Widget>? supportChildList}) {
    List<Widget> supportChildListTemp = supportChildList ?? [];
    supportChildListTemp = supportChildListTemp.isEmpty ? children : supportChildListTemp;
    Widget realWidget = Wrap(
      direction: direction,
      alignment: wrapAlignment,
      spacing: spacing,
      runAlignment: runAlignment,
      runSpacing: runSpacing,
      crossAxisAlignment: crossAxisAlignment,
      textDirection: textDirection,
      verticalDirection: verticalDirection,
      clipBehavior: clipBehavior,
      children: children,
    );
    return super.buildRealChild(supportChildList: [realWidget]);
  }
}

///空视图
class UIEmptyWidget extends StatelessWidget {
  const UIEmptyWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return const SizedBox.shrink();
  }
}
