import 'package:cave_flutter/style/whl_style.dart';
import 'package:flutter/material.dart';
import '../brick.dart';
import 'dart:ui' as UI;

///文字Widget
///参考[UIContainer]
class UIText extends UIContainer {
  //快捷属性
  final Widget? startDrawable;
  final Widget? topDrawable;
  final Widget? endDrawable;
  final Widget? bottomDrawable;
  final double marginDrawable;
  final bool shrinkWrap;
  final TextAlign? textAlign;
  final int? maxLines;
  final TextOverflow? overflow;

  ///Text
  final Text? customText;
  final String? text;
  final Color? textColor;
  final Offset? from;
  final Offset? to;
  final Color? textStartColor;
  final Color? textEndColor;
  final double? letterSpacing;
  final double? fontSize;
  final FontWeight? fontWeight;
  final double? textStyleHeight;
  final TextStyle? textStyle;
  final String? fontFamily;
  final String? package;
  final TextDecoration? textDecoration;

  const UIText({
    super.key,
    this.text,
    this.customText,
    this.textColor,
    this.fontSize,
    this.fontWeight,
    this.fontFamily,
    this.textStyleHeight,
    this.textStyle,
    this.package,
    //自身快捷属性
    this.startDrawable,
    this.topDrawable,
    this.endDrawable,
    this.bottomDrawable,
    this.marginDrawable = 0,
    this.shrinkWrap = true,
    this.textAlign,
    this.maxLines,
    this.overflow,
    this.textDecoration,
    this.from,
    this.to,
    this.textStartColor,
    this.textEndColor,
    this.letterSpacing,
    //快捷属性
    super.radius = 0,
    super.topLeftRadius,
    super.topRightRadius,
    super.bottomRightRadius,
    super.bottomLeftRadius,
    super.solidColor = Colors.transparent,
    super.strokeColor = Colors.transparent,
    super.strokeWidth,
    super.gradientStartColor,
    super.gradientEndColor,
    super.gradientBegin = Alignment.centerLeft,
    super.gradientEnd = Alignment.centerRight,
    //UIContainer
    super.margin,
    super.padding,
    super.color,
    super.decoration,
    super.width,
    super.height,
    super.constraints,
    super.alignment,
    super.foregroundDecoration,
    super.transform,
    super.transformAlignment,
    super.clipBehavior = Clip.none,
    super.backgroundDrawable,
    //GestureDetector
    super.onTapDown,
    super.onTapUp,
    super.onTap,
    super.onTapCancel,
    super.onLongPress,
    super.onLongPressEnd,
    super.onLongPressMoveUpdate,
  });

  @override
  Widget buildRealChild({List<Widget>? supportChildList}) {
    List<Widget> supportChildListTemp = supportChildList ?? [];
    if (supportChildListTemp.isNotEmpty) throw UnsupportedError("不支持重写");
    Widget realChild = (null == backgroundDrawable)
        ? (shrinkWrap ? _renderWrapComboText() : _renderComboText())
        : Stack(
            alignment: Alignment.center,
            children: [
              backgroundDrawable ?? const UIEmptyWidget(),
              (shrinkWrap ? _renderWrapComboText() : _renderComboText()),
            ],
          );
    return super.buildRealChild(supportChildList: [realChild]);
  }

  ///ComboText
  Widget _renderWrapComboText() {
    return UIWrap(
      direction: Axis.vertical,
      crossAxisAlignment: WrapCrossAlignment.center,
      children: [
        topDrawable ?? const UIEmptyWidget(),
        SizedBox(height: (null == topDrawable) ? 0 : marginDrawable),
        UIWrap(
          direction: Axis.horizontal,
          crossAxisAlignment: WrapCrossAlignment.center,
          children: [
            startDrawable ?? const UIEmptyWidget(),
            SizedBox(width: (null == startDrawable) ? 0 : marginDrawable),
            _renderText(),
            SizedBox(width: (null == endDrawable) ? 0 : marginDrawable),
            endDrawable ?? const UIEmptyWidget(),
          ],
        ),
        SizedBox(height: (null == bottomDrawable) ? 0 : marginDrawable),
        bottomDrawable ?? const UIEmptyWidget(),
      ],
    );
  }

  ///ComboText
  Widget _renderComboText() {
    return UIColumn(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        topDrawable ?? const UIEmptyWidget(),
        SizedBox(height: (null == topDrawable) ? 0 : marginDrawable),
        UIRow(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            startDrawable ?? const UIEmptyWidget(),
            SizedBox(width: (null == startDrawable) ? 0 : marginDrawable),
            Expanded(child: _renderText()),
            SizedBox(width: (null == endDrawable) ? 0 : marginDrawable),
            endDrawable ?? const UIEmptyWidget(),
          ],
        ),
        SizedBox(height: (null == bottomDrawable) ? 0 : marginDrawable),
        bottomDrawable ?? const UIEmptyWidget(),
      ],
    );
  }

  ///TextWidget
  Widget _renderText() {
    return customText ??
        Text(
          text ?? "",
          textAlign: textAlign,
          maxLines: maxLines,
          style: textStyle ??
              TextStyle(
                  color: textColor,
                  fontSize: fontSize,
                  fontWeight: fontWeight,
                  height: textStyleHeight,
                  fontFamily: fontFamily,
                  package: package,
                  decoration: textDecoration,
                  letterSpacing: letterSpacing,
                  foreground: textStartColor != null
                      ? (Paint()
                        ..shader = UI.Gradient.linear(
                            from ?? const Offset(100, 20),
                            to ?? const Offset(250, 20), <Color>[
                          textStartColor ?? (textColor ?? kAppBlackColor),
                          textEndColor ?? (textColor ?? kAppBlackColor)
                        ]))
                      : null),
          overflow: overflow,
        );
  }
}
