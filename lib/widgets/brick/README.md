# 通用组件,勿添加业务代码

#### 目录
```
/widget 基础视图
```

### widget/basic_widget.dart
| 组件 |
:-: |
| UIFlex |
| UIRow |
| UIColumn |
| UIContainer |
| UIStack |
| UIEmptyWidget |

### widget/text_widget.dart
| 组件 |
:-: |
| UIText |