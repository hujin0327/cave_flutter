import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MyEmptyWidget extends StatelessWidget {
  final String title;
  final String image;
  final Color? bgColor;
  final Widget? bottomButton;
  EdgeInsetsGeometry? margin;

  MyEmptyWidget(
      {Key? key,
      this.image = "mine_noData.png",
      this.title = "目前什么都没有",
      this.bottomButton,
      this.bgColor,
      this.margin})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: bgColor ?? Colors.white,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            UIImage(
              assetImage: image,
              // width: 150.w,
              // height: 150.w,
              margin: margin,
            ),
            UIText(
              text: title,
              textColor: kAppSubTextColor,
              fontSize: 14.sp,
              margin:
                  bottomButton != null ? EdgeInsets.only(bottom: 20.h) : null,
              // fontWeight: FontWeight.bold,
            ),
            if (bottomButton != null) bottomButton!,
          ],
        ),
      ),
    );
  }
}
