import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../style/whl_style.dart';

class WhlBottomBar extends StatelessWidget {
  WhlBottomBar({Key? key, this.index = 0, required this.items}) : super(key: key);
  final int index;
  final List<WhlBottomBarItem> items;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: ScreenUtil().bottomBarHeight),
      height: 50.h + ScreenUtil().bottomBarHeight,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20.w),
          topRight: Radius.circular(20.w),
        ),
        // border: Border(top: BorderSide(color: flavorISBAIBIAN() ? kAppColor('#EEEEEE') : kAppColor('#292929').withOpacity(0.5), width: 1.w))
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.05),
            offset: Offset(0, -5),
            blurRadius: 4,
            spreadRadius: 0,
          ),
        ],
      ),
      child: Row(
        children: List.generate(
            items.length,
            (index) => _buildItemView(
                  i: index,
                  item: items.elementAt(index),
                )).toList(),
      ),
    );
  }

  Widget _buildItemView({required int i, required WhlBottomBarItem item}) => Expanded(
        child: GestureDetector(
          onTap: () {
            if (item.onClick != null) item.onClick!(i);
          },
          behavior: HitTestBehavior.translucent,
          child: Stack(
            alignment: Alignment.center,
            children: [
              Positioned(
                top: 13.h,
                child: UIImage(
                  assetImage: i == index ? item.selectedImgRes : item.unselectedImgRes,
                  fit: BoxFit.contain,
                  width: item.imgWidth,
                  height: item.imgHeight,
                ),
              ),
              // Positioned(
              //   top: 3.h,
              //   child: Container(
              //     padding: EdgeInsets.only(left: 15.w),
              //     child: UnreadCountView(
              //       count: item.count ?? 0,
              //     ),
              //   ),
              // ),
            ],
          ),
        ),
      );
}

class WhlBottomBarItem {
  final String selectedImgRes;
  final String unselectedImgRes;
  final String label;
  final TextStyle? selectedStyle;
  final TextStyle? unselectedStyle;
  final double imgWidth;
  final double imgHeight;
  final Function(int index)? onClick;
  final Stream<int>? steam;
  final int? count;

  WhlBottomBarItem(
      {required this.selectedImgRes,
      required this.unselectedImgRes,
      required this.label,
      this.selectedStyle,
      this.unselectedStyle,
      required this.imgWidth,
      required this.imgHeight,
      this.onClick,
      this.steam,
      this.count});
}
