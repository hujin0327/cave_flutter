import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../style/whl_style.dart';
import 'brick/widget/text_widget.dart';

///业务实心Button
class UISolidButton extends StatelessWidget {
  final String? text;
  final EdgeInsets? margin;
  final Function()? onTap;
  final double? width;
  final double? height;
  final double? radius;
  final double? fontSize;
  final bool? shrinkWrap;
  final Color? color;
  final Color? textColor;

  UISolidButton({
    super.key,
    this.text,
    this.margin,
    this.onTap,
    this.width,
    this.height,
    this.radius,
    this.fontSize,
    this.shrinkWrap,
    this.color,
    this.textColor,
  });

  @override
  Widget build(BuildContext context) {
    return UIText(
      onTap: onTap,
      text: text ?? "",
      textColor: textColor ?? Colors.white,
      fontSize: fontSize ?? 14.sp,
      shrinkWrap: shrinkWrap ?? false,
      width: width,
      height: height ?? 48.h,
      fontWeight: FontWeight.bold,
      margin: margin,
      textAlign: TextAlign.center,
      radius: radius ?? 24.h,
      gradientBegin: Alignment.centerLeft,
      gradientEnd: Alignment.centerRight,
      gradientStartColor: color ?? kAppTextColor,
      gradientEndColor: color ?? kAppTextColor,
      alignment: Alignment.center,
    );
  }
}

///业务空心Button
class UIStrokeButton extends UISolidButton {
  final Color? strokeColor;

  UIStrokeButton({
    this.strokeColor,
    super.textColor,
    super.key,
    super.text,
    super.margin,
    super.onTap,
    super.width,
    super.height,
    super.radius,
    super.fontSize,
    super.shrinkWrap,
  });

  @override
  Widget build(BuildContext context) {
    return UIText(
      onTap: onTap,
      text: text ?? "",
      textColor: textColor ?? kAppThemeColor,
      fontSize: fontSize ?? 14.sp,
      shrinkWrap: shrinkWrap ?? false,
      width: width,
      height: height ?? 48.w,
      fontWeight: FontWeight.normal,
      margin: margin,
      textAlign: TextAlign.center,
      radius: radius ?? 24.w,
      strokeColor: strokeColor ?? kAppThemeColor,
      strokeWidth: 1.w,
      alignment: Alignment.center,
    );
  }
}
