import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class NavCustomPainter extends CustomPainter {
  late double loc;
  late double s;
  Color color;
  TextDirection textDirection;

  NavCustomPainter(
      double startingLoc, int itemsLength, this.color, this.textDirection) {
    final span = 1.0 / itemsLength;
    s = 0.2;
    double l = startingLoc + (span - s) / 2;
    loc = textDirection == TextDirection.rtl ? 0.8 - l : l;
  }

  @override
  void paint(Canvas canvas, Size size) {
    double width = size.width;
    final paint = Paint()
      ..color = Colors.black
      ..style = PaintingStyle.fill;

    final path = Path()
      ..moveTo(0, 0)
      ..lineTo((loc - 0.1) * width, 0)
      ..cubicTo(
        (loc + s * 0.20) * width,
        size.height * 0.05,
        loc * width,
        size.height * 0.60,
        (loc + s * 0.50) * width,
        size.height * 0.60,
      )
      ..cubicTo(
        (loc + s) * width,
        size.height * 0.60,
        (loc + s - s * 0.20) * width,
        size.height * 0.05,
        (loc + s + 0.1) * width,
        0,
      )
      ..lineTo(width, 0)
      ..lineTo(width, size.height)
      ..lineTo(0, size.height)
      ..close();
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return this != oldDelegate;
  }
}
