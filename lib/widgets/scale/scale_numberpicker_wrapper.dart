import 'package:cave_flutter/widgets/scale/scale_numberpicker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

///对ScaleNumberPicker进行简单包装，添加顶部的选中值显示和两边的半透明遮罩
// ignore: must_be_immutable
class ScaleNumberPickerWrapper extends StatefulWidget {
  final int initialValue;
  final int minValue;
  final int maxValue;
  final int step;
  final String unit;

  ///控件的宽度
  final int widgetWidth;

  ///一大格中有多少个小格
  final int subGridCountPerGrid;

  ///每一小格的宽度
  final int subGridWidth;

  final void Function(int) onSelectedChanged;

  ///返回标尺刻度所展示的数值字符串
  final String Function(int) scaleTransformer;

  ///刻度颜色
  final Color scaleColor;

  final int numberPickerHeight;

  ///刻度文字颜色
  final Color scaleTextColor;

  ScaleNumberPickerWrapper({
    Key? key,
    this.initialValue = 500,
    this.minValue = 10,
    this.maxValue = 100,
    this.step = 1,
    this.unit = "",
    this.widgetWidth = 200,
    this.subGridCountPerGrid = 10,
    this.subGridWidth = 10,
    required this.onSelectedChanged,
    required this.scaleTransformer,
    this.scaleColor = Colors.black,
    this.scaleTextColor = Colors.black,
    required this.numberPickerHeight,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ScaleNumberPickerWrapperState();
  }
}

class ScaleNumberPickerWrapperState extends State<ScaleNumberPickerWrapper> {
  final GlobalKey<ScaleNumberPickerState> pickerKey =
      GlobalKey<ScaleNumberPickerState>();

  @override
  void initState() {
    super.initState();
  }

  ///处理state的复用
  void didUpdateWidget(ScaleNumberPickerWrapper oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        //可滚动标尺
        Listener(
          child: Stack(
            children: <Widget>[
              ScaleNumberPicker(
                key: pickerKey,
                initialValue: widget.initialValue,
                minValue: widget.minValue,
                maxValue: widget.maxValue,
                step: widget.step,
                widgetWidth: widget.widgetWidth,
                widgetHeight: widget.numberPickerHeight,
                subGridCountPerGrid: widget.subGridCountPerGrid,
                subGridWidth: widget.subGridWidth,
                onSelectedChanged: (value) {
                  widget.onSelectedChanged(value);
                },
                scaleTransformer: widget.scaleTransformer,
                scaleColor: widget.scaleColor,
                scaleTextColor: widget.scaleTextColor,
              ),
              //左右半透明遮罩
              Positioned(
                left: 0,
                child: IgnorePointer(
                  child: Container(
                    width: MediaQuery.of(context).size.width / 2 - 30.w,
                    height: widget.numberPickerHeight.toDouble(),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(colors: [
                        Colors.white.withOpacity(0.8),
                        Colors.white.withOpacity(0)
                      ]),
                    ),
                  ),
                ),
              ),
              Positioned(
                right: 0,
                child: IgnorePointer(
                  child: Container(
                    width: MediaQuery.of(context).size.width / 2 - 30.w,
                    height: widget.numberPickerHeight.toDouble(),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(colors: [
                        Colors.white.withOpacity(0),
                        Colors.white.withOpacity(0.8)
                      ]),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  select(double valueToSelect) {
    pickerKey.currentState?.select((valueToSelect * 10).toInt());
  }
}
