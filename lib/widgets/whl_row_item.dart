import 'package:cave_flutter/style/whl_style.dart';
import 'package:cave_flutter/widgets/brick/brick.dart';
import 'package:cave_flutter/widgets/brick/widget/basic_widget.dart';
import 'package:cave_flutter/widgets/whl_switch_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MyRowItem extends UIContainer {
  final String? title;
  final String? value;
  final String? subTitle;
  final bool? showArrow;
  final bool isShowSwitch;
  final bool isSwitchOn;
  final Function? onChanged;
  final String? icon;
  final Widget? valueWidget;
  final Color? titleColor;
  final FontWeight? titleFontWeight;
  final bool isShowSVip;

  const MyRowItem(
      {super.key,
      this.title,
      this.value,
      this.showArrow,
      this.icon,
      this.valueWidget,
      super.onTap,
      this.isShowSwitch = false,
      this.isSwitchOn = false,
      this.isShowSVip = false,
      this.subTitle,
      this.onChanged,
      super.padding,
      super.margin,
      super.color,
      super.decoration,
      super.radius,
      this.titleColor,
      this.titleFontWeight});

  @override
  Widget build(BuildContext context) {
    return UIRow(
      decoration: decoration,
      margin: margin,
      color: color,
      padding: padding,
      radius: radius,
      onTap: onTap,
      children: [
        if (icon != null)
          UIImage(
            assetImage: icon,
            width: 22.w,
            height: 22.w,
            margin: EdgeInsets.only(right: 6.w),
          ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UIText(
                text: title ?? '',
                fontSize: 15.sp,
                textColor: titleColor ?? kAppColor('#444444'),
                fontWeight: titleFontWeight ?? FontWeight.normal,
                endDrawable: isShowSVip
                    ? UIImage(
                        margin: EdgeInsets.only(left: 7.w),
                        assetImage: 'icon_vip1.png',
                        height: 15.w,
                      )
                    : null),
            if (subTitle != null)
              UIText(
                margin: EdgeInsets.only(top: 4.h),
                text: subTitle,
                textColor: kAppSubTextColor,
                fontSize: 12.sp,
              )
          ],
        ),
        Expanded(child: Container()),
        valueWidget ??
            UIText(
              text: value ?? '',
              fontSize: 16.sp,
              textColor: kAppColor('#666666'),
              maxLines: 1,
              width: value != null && value!.isNotEmpty ? 180.w : 10.w,
              shrinkWrap: false,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.right,
            ),
        if (isShowSwitch)
          WhlSwitch(
              openColor: Colors.white,
              bgColor: kAppColor('#EDEDED'),
              color: Colors.white,
              openBgColor: kAppThemeColor,
              // inactiveTrackColor: Color.fromRGBO(237, 237, 237, 1),
              value: isSwitchOn,
              width: 50.w,
              height: 26.w,
              onChanged: (val) {
                onChanged?.call();
              })
        else if (showArrow ?? true)
          Icon(Icons.keyboard_arrow_right, color: kAppSub2TextColor),
      ],
    );
  }
}
