import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../style/whl_style.dart';
import 'brick/widget/basic_widget.dart';
import 'brick/widget/image_widget.dart';

typedef SearchCallBack = Function(String value);

class SearchWidget extends StatefulWidget {
  TextEditingController textEditingController = TextEditingController();
  bool? showClearIcon = false;
  Function? onClear;
  SearchCallBack? searchCallback;
  String? strHint;
  Color? bgColor;
  bool? readOnly = false;

  SearchWidget(this.textEditingController,
      {this.onClear,
      this.searchCallback,
      this.strHint,
      this.showClearIcon,
      this.bgColor,
      this.readOnly,
      Key? key})
      : super(key: key);

  @override
  State<SearchWidget> createState() => _SearchWidgetState();
}

class _SearchWidgetState extends State<SearchWidget> {
  bool? showClearIcon = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    showClearIcon = widget.showClearIcon ?? false;
    addMyListener();
  }

  @override
  Widget build(BuildContext context) {
    return UIRow(
      // padding:
      // EdgeInsets.only(left: 16.w, right: 16.w, top: 12.h, bottom: 12.h),
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
          child: UIRow(
            // margin: EdgeInsets.only(left: 0.w, right: 0.w),
            color: widget.bgColor,
            radius: 19.w,
            height: 38.w,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              UIImage(
                margin: EdgeInsets.only(left: 12.w, right: 6.w),
                assetImage: "cave_search.png",
                width: 20.w,
                height: 20.w,
              ),
              Expanded(
                child: TextField(
                  controller: widget.textEditingController,
                  // keyboardType: TextInputType.visiblePassword,
                  textInputAction: TextInputAction.search,
                  onSubmitted: widget.searchCallback,
                  readOnly: widget.readOnly??false,
                  enabled: !(widget.readOnly??false),
                  decoration: InputDecoration(
                    isDense: true,
                    hintText: widget.strHint ?? "搜索",
                    border: InputBorder.none,
                    hintStyle:
                        TextStyle(fontSize: 14.sp, color: kAppSub3TextColor),
                  ),
                  style: TextStyle(fontSize: 14.sp, color: kAppSubTextColor),
                ),
              ),
              if (showClearIcon!)
                UIImage(
                  margin: EdgeInsets.only(left: 6.w, right: 12.w),
                  assetImage: "search_delete.png",
                  width: 20.w,
                  height: 20.h,
                  onTap: () {
                    widget.textEditingController.text = "";
                    if (widget.onClear != null) widget.onClear!();
                  },
                )
            ],
          ),
        ),
        // UIText(
        //   alignment: Alignment.center,
        //   text: "Clear",
        //   textColor: kAppMainColor,
        //   width: 50.w,
        //   height: 31.w,
        //   radius: 4.w,
        //   strokeColor: kAppMainColor,
        //   strokeWidth: 0.5,
        //   fontSize: 14.sp,
        //   onTap: () {
        //     controller.text = "";
        //     if (onClear != null) onClear();
        //   },
        // ),
      ],
    );
  }

  addMyListener() {
    widget.textEditingController.addListener(() {
      setState(() {
        showClearIcon = widget.textEditingController.text.isNotEmpty;
      });
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    widget.textEditingController.removeListener(() {});
  }
}
