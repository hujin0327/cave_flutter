import "package:cave_flutter/style/whl_style.dart";
import "package:flutter/material.dart";

class AudioVisualizerWidget extends StatelessWidget {
  final List<Color>? colors;
  final List<int>? duration;
  final int? barCount;
  final Curve? curve;
  List? allBars;

  AudioVisualizerWidget({
    Key? key,
    @required this.colors,
    @required this.duration,
    @required this.barCount,
    this.allBars,
    this.curve = Curves.easeInQuad,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    allBars ??= [];
    for (int i = 0; i < barCount!; i++) {
      VisualComponent visual = VisualComponent(
        curve: curve!,
        duration: duration![i % 5],
        color: (colors ?? []).length >= 4 ? colors![i % 4] : kAppThemeColor,
      );
      allBars!.add(visual);
    }
    return Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: List<Widget>.generate(barCount!, (index) => allBars![index]));
  }
}

class VisualComponent extends StatefulWidget {
  final int? duration;
  final Color? color;
  final Curve? curve;
  final double animationLevel;

  const VisualComponent({Key? key, @required this.duration, @required this.color, @required this.curve, this.animationLevel = 1.0}) : super(key: key);

  @override
  _VisualComponentState createState() => _VisualComponentState();

  startAnimation() {
    _VisualComponentState().startAnimation();
  }

  stopAnimation() {
    _VisualComponentState().stopAnimation();
  }
}

class _VisualComponentState extends State<VisualComponent> with SingleTickerProviderStateMixin {
  Animation<double>? animation;
  AnimationController? animationController;

  @override
  void initState() {
    super.initState();
    animate();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    print('didChangeDependencies');
  }

  @override
  void dispose() {
    animation!.removeListener(() {});
    animation!.removeStatusListener((status) {});
    animation = null;
    if (animationController!.isAnimating) {
      animationController!.stop();
    }
    animationController!.dispose();
    super.dispose();
  }

  startAnimation() {
    animationController!.repeat(reverse: true);
  }

  stopAnimation() {
    animationController!.stop();
  }

  void animate() {
    animationController = AnimationController(duration: Duration(milliseconds: widget.duration!), vsync: this);
    final curvedAnimation = CurvedAnimation(parent: animationController!, curve: widget.curve!);
    animation = Tween<double>(begin: 2 * widget.animationLevel, end: 15).animate(curvedAnimation)
      ..addListener(() {
        update();
      });
    startAnimation();
  }

  void update() {
    if (mounted) setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 3,
      height: animation!.value,
      decoration: BoxDecoration(color: widget.color, borderRadius: BorderRadius.circular(5)),
    );
  }
}
