import 'package:flutter/material.dart';

/// 保持状态的包裹类
class WhlKeepAliveWidget extends StatefulWidget {
  final Widget child;

  const WhlKeepAliveWidget({required this.child});

  @override
  State<StatefulWidget> createState() => _KeepAliveState();
}

class _KeepAliveState extends State<WhlKeepAliveWidget>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return widget.child;
  }

  @override
  bool get wantKeepAlive => true;
}

Widget keepAliveWrapper(Widget child) => WhlKeepAliveWidget(child: child,);
