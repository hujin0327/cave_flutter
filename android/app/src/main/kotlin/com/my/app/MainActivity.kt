package com.my.app
import io.flutter.embedding.android.FlutterFragmentActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant


class MainActivity : FlutterFragmentActivity() {
    ///插件名称
    private val channelName = "my_flutter_plugin"
    var methodChannel: MethodChannel? = null

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine);
        methodChannel = MethodChannel(flutterEngine.dartExecutor, channelName)
        methodChannel?.setMethodCallHandler { call: MethodCall, result: MethodChannel.Result ->
            when (call.method) {
                "getProductFlavor" -> {
                    result.success(BuildConfig.FLAVOR_app)
                }

                else -> {
                    result.notImplemented()
                }
            }
        }
    }
}
