package com.my.app

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import com.my.app.BuildConfig
class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

}