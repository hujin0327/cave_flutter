# cave_flutter

## 数据库生成
```
1. 删除'app_database.g.dart'文件
2. 在项目根目录执行 flutter packages pub run build_runner build
3. 生成的文件在 .dart_tool/build/generated/cave_flutter/lib/db_manager/app_database.floor.g.part
4. 将文件改名并复制到 lib/db_manager/db/app_database.g.dart
5. 更新顶部导包：
import 'dart:async';
import 'package:cave_flutter/db_manager/dao/whl_product_dao.dart';
import 'package:floor/floor.dart';
import '../bean/whl_product_bean.dart';
import 'app_database.dart';
import 'package:sqflite/sqflite.dart' as sqflite;
``` 
